$(document).ready(function(){

    // Hide Header on on scroll down
    var didScroll;
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = $('.header').outerHeight();

    $(window).scroll(function(event){
    	didScroll = true;
    });

    setInterval(function() {
    	if (didScroll) {
    		hasScrolled();
    		didScroll = false;
    	}
    }, 250);

    function hasScrolled() {
    	var st = $(this).scrollTop();

        // Make sure they scroll more than delta
        if(Math.abs(lastScrollTop - st) <= delta)
        	return;

        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.
        if (st > lastScrollTop && st > navbarHeight){
            // Scroll Down
            $('.header').removeClass('nav-down').addClass('nav-up');
        } else {
            // Scroll Up
            if(st + $(window).height() < $(document).height()) {
            	$('.header').removeClass('nav-up').addClass('nav-down');
            }
        }

        lastScrollTop = st;
    }


    $('.slider-1').slick({
    	dots: false,
    	infinite: true,
    	speed: 300,
    	slidesToShow: 3,
    	slidesToScroll: 3,
    	prevArrow:'.slider-1-setas .seta-esquerda',
    	nextArrow:'.slider-1-setas .seta-direita',
    	responsive: [
    	{
    		breakpoint: 1024,
    		settings: {
    			slidesToShow: 3,
    			slidesToScroll: 3,
    			infinite: true,
    		}
    	},
    	{
    		breakpoint: 600,
    		settings: {
    			slidesToShow: 2,
    			slidesToScroll: 2,
    		}
    	},
    	{
    		breakpoint: 480,
    		settings: {
    			slidesToShow: 1,
    			slidesToScroll: 1
    		}
    	}]
    });


    $('.box-ofertas-hover-2 a[data-toggle="tab"]').click(function() {
    	$('.box-ofertas-hover-2 a[data-toggle="tab"]').removeClass('active')
    	$(this).addClass('active')
    })

    $(".zoom").fancybox({
    	prevEffect	: 'none',
    	nextEffect	: 'none',
    	helpers	: {
    		title	: {
    			type: 'outside'
    		},
    		thumbs	: {
    			width	: 50,
    			height	: 50
    		}
    	}
    });


    $('.slider-fotos-produtos-imagem-grande').slick({
    	slidesToShow: 1,
    	slidesToScroll: 1,
    	arrows: false,
    	responsive: true,
    	fade: true
    });

    $('.slider-fotos-produtos-lista').slick({
    	slidesToShow: 3,
    	slidesToScroll: 1,
    	asNavFor: '.slider-fotos-produtos-imagem-grande',
    	prevArrow: '<span type="button" class="bt-prev">‹</span>',
    	nextArrow: '<span type="button" class="bt-next">›</span>',
    	dots: false,
    	centerMode: true,
    	responsive: true,
    	focusOnSelect: true,
    	infinite: false,
    	responsive: [
    	{
    		breakpoint: 1024,
    		settings: {
    			slidesToShow: 4,
    			slidesToScroll: 4,
    			infinite: false,
    		}
    	},
    	{
    		breakpoint: 600,
    		settings: {
    			slidesToShow: 3,
    			slidesToScroll: 3,
    		}
    	},
    	{
    		breakpoint: 480,
    		settings: {
    			slidesToShow: 2,
    			slidesToScroll: 2
    		}
    	}] 					
    });

 		$(".slider-fotos-produtos-imagem-grande").on('init', function () {
 			setTimeout(function () {
 				$('.slider-fotos-produtos-imagem-grande .img-item').zoom({ magnify : 2 }); 
 				$('.msgZoom').show()
 			},500)
			
		}); 



});
