﻿<%
dim sCampoPreco, sCampoAcrescimoLogin, sCampoDescricao
dim sCampoQtedMinima, sCampoQtedMaxima, Idioma

Idioma = "br"
if(	InSTr(lcase(Request.ServerVariables("Script_Name")),"/japones/") > 0) then Idioma = "jp"
    
Const ValorImposto              = 0.5
Const MaxShoppingCartItems      = 500
Const ShoppingCartAttributes    = 31
Const sCdCliente                = 1
Const sParcelaMinima            = 7
Const sValorParcelaMinima       = 10
Const sTipoNewLetter            = 2
Const sidTextoContato           = 1

' SEQUENCIA DO ARRAY, CARACTERISTICA DO PRODUTO
Const C_ID = 1
Const C_Titulo = 2
Const C_Qtd = 3
Const C_Preco = 4
Const C_Imagem = 5
Const C_Estoque = 6
Const C_Moeda = 7
Const C_Ref = 8
Const C_Tamanho = 9
Const C_Cor = 10
Const C_Sabor = 11
Const C_Tipo = 12
Const C_Fragrancia = 13
Const C_TamanhoAcres = 14
Const C_CorAcres = 15
Const C_SaborAcres = 16
Const C_TipoAcres = 17
Const C_FragranciaAcres = 18
Const C_Pacote = 19
Const C_PacoteAcres = 20
Const C_Peso = 21
Const C_Manuseio = 22
Const C_Embrulho = 23
Const C_QteParcelamento = 24
Const C_PrecoPrazo = 25
Const C_GE = 26
Const C_GEAcres = 27
Const C_PrecoCesta = 28
Const C_NomeP = 29
Const C_EmailP = 30
Const C_MensagemP = 31

Const sPorcentagemCartaoCredito = 4

sCampoDescricao 		= "Descricao"
sCampoPreco 			= "Preco_Consumidor"
sCampoAcrescimoLogin 	= "Acrescimo_Consumidor"
sCampoQtedMinima 		= "Qte_Minima_Consumidor"
sCampoQtedMaxima 		= "Qte_Maxima_Consumidor"
%>