<%
'***********************
function checa_pasta(v1)
'***********************
' verifica se existe a pasta v1
Set ScriptObject = Server.CreateObject("Scripting.FileSystemObject")
checa_pasta=ScriptObject.FolderExists(v1)
set ScriptObject=nothing
end function

'**********************
function cria_pasta(v1)
'**********************
' fun��o que cria pasta no diret�rio v1
Set ScriptObject = Server.CreateObject("Scripting.FileSystemObject")
existe=ScriptObject.FolderExists(v1)
if not existe then
   ' n�o existe. Criar
   set criar = ScriptObject.CreateFolder(v1)
end if
set ScriptObject=nothing
end function

'**************************
function remove_arquivo(v1)
'**************************
' fun��o que remove o arquivo no diret�rio v1
Set ScriptObject = Server.CreateObject("Scripting.FileSystemObject")
existe=ScriptObject.FileExists(v1)
if existe then
   ' existe. Remover
   on error resume next
   remover = ScriptObject.DeleteFile(v1, 1)
   if err.number<>0 then
      remove_arquivo=err.descritiion
   else
      remove_arquivo="1"
   end if
   on error goto 0
else
   remove_arquivo="Arquivo n�o encontrado!"
end if
set ScriptObject=nothing
end function

'****************************
function move_arquivo(v1, v2)
'****************************
' move o arquivo de v1 para v2
on error resume next
set fs=Server.CreateObject("Scripting.FileSystemObject")
fs.MoveFile v1, v2
if err.number <> 0 then
   move_arquivo = 0
else
   move_arquivo = 1
end if
set fs=nothing
on error goto 0
end function

'*************************
function checa_arquivo(v1)
'*************************
' verifica se existe o arquivo na a pasta v1
Set ScriptObject = Server.CreateObject("Scripting.FileSystemObject")
checa_arquivo=ScriptObject.FileExists(v1)
set ScriptObject=nothing
end function

'************************
function pega_tamanho(v1)
'************************
' fun��o que pega o tamanho do arquivo (size)
Set fs = CreateObject("Scripting.FileSystemObject")
Set f = fs.GetFile(v1)
pega_tamanho = f.size
set fs=nothing
end function

%>
