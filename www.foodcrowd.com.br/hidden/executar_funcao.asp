﻿<!--#include virtual="/hidden/funcoes.asp"-->
<%

dim sAcaoFuncao, sMsgJsFun
dim sOpcNewsletter, sEmailNEws
dim sLogin, sSenha, sDir

if(Replace_Caracteres_Especiais(request("idiomajp")) <> "") then Idioma = "japones"

call Recupera_Variaveis()

if(Idioma = "br" or Idioma = "") then
	sDir = "portugues"
else
	sDir = "japones"
end if

' =======================================================================
sub Recupera_Variaveis()
	dim sSql, Rs
	dim sData, sTipoNew

	sAcaoFuncao = Replace_Caracteres_Especiais(request("acao"))
	sData = (day(now) & "/" & month(now) & "/" & year(now))
	
	Select case sAcaoFuncao
		case "enviarlinkproduto"
			nomee           = Replace_Caracteres_Especiais(request("nomee"))
            emailnp         = Replace_Caracteres_Especiais(request("emailnp"))
            idprodutolink   = Replace_Caracteres_Especiais(request("idprodutolink"))

            call EnviarLinkProdutos(idprodutolink, nomee, emailnp)

            sMsgJsFun = "E-mail enviado com sucesso!"

		case "lista"
			sProduto = Replace_Caracteres_Especiais(request("produto"))

			if(sProduto = "0") then 
				response.write "ERRO NO SISTEMA!"
				response.end
			end if
			if(not isnumeric(sProduto)) then 
				response.write "ERRO NO SISTEMA!"
				response.end
			end if
			if(not isnumeric(Session("Id")) or Session("Id") = "") then 
				response.write "ERRO NO SISTEMA!"
				response.end
			end if
			
			call Abre_Conexao()

			' VERIFICA SE O EMAIL JA ESTA CADASTRADO
			sSql = "Select * from TB_LISTA_DESEJOS where Id_Cliente = "& Session("Id") &" and Id_Produto = " & sProduto
			set Rs = oConn.execute(sSql)
			if(Rs.eof) then
				sSql = "Insert into TB_LISTA_DESEJOS (Id_Cliente, Id_Produto, DataCad) values ("& Session("Id") &", "& sProduto &", '"& sData &"')"
				oConn.execute(sSql)
				sMsgJsFun = "Produto adicionado com sucesso a Lista de Desejos. Obrigado!"
			else
				sMsgJsFun = "Produto já esta na Lista de Desejos. Obrigado!"
			end if
			call Fecha_Conexao()

		case "gnews"
			sOpcNewsletter = Replace_Caracteres_Especiais(request("Newsletter"))
			sEmailNEws = Replace_Caracteres_Especiais(replace(trim(request("emailn")),"'",""))
			
			call Abre_Conexao()

			' VERIFICA SE O EMAIL JA ESTA CADASTRADO
			sSql = "Select id, descricao from TB_NEWSLETTER_EMAILS (nolock) where descricao = '"& sEmailNEws &"' and Id_Tipo_Newsletter = "& sTipoNewLetter &" order by descricao"
			set Rs = oConn.execute(sSql)

			if(Rs.eof) then
				Select case sOpcNewsletter
					case "a"
						' GRAVA EMAIL NEWSLETTER
						sSql = "Insert into TB_NEWSLETTER_EMAILS (Id_Tipo_Newsletter, Descricao, Ativo, DataCad) values ("& sTipoNewLetter &", '"& sEmailNEws &"' , 1, '"& sData &"')"
						oConn.execute(sSql)
                        
						sMsgJsFun = "E-mail cadastrado com sucesso!"
                        'response.write sMsgJsFun
                        'response.End
					case "c"
						sMsgJsFun = "E-mail não cadastrado, não pode ser cancelado!"

                        'response.write sMsgJsFun
                        'response.End
				end select
			else
				Select case sOpcNewsletter
					case "a"
						sMsgJsFun = "E-mail existente!"

                        'response.write sMsgJsFun
                        'response.End
					case "c"
						' GRAVA EMAIL NEWSLETTER
						sSql = "Delete from TB_NEWSLETTER_EMAILS where descricao = '"& sEmailNEws &"'"
						oConn.execute(sSql)

						sMsgJsFun = "E-mail cancelado com sucesso!"

                        'response.write sMsgJsFun
                        'response.End
				end select
			end if
			set Rs = nothing
			call Fecha_Conexao()

		case "login"
			sLogin = Replace_Caracteres_Especiais(replace(trim(request("login")),"'",""))
			sSenha = Replace_Caracteres_Especiais(replace(trim(request("senha")),"'",""))
			sMsgJsFun = "Login e Senha inválidos!"

			if(Efetua_Login(sLogin, sSenha)) then 
				sMsgJsFun = "Login efetuado com sucesso!"
			end if

		case "logout"
			Session.Abandon
	end select
end sub
%>
<script>
	<%if(sMsgJsFun <> "" and (sAcaoFuncao = "gnews" OR sAcaoFuncao = "lista" OR sAcaoFuncao = "enviarlinkproduto")) then %>
		alert('<%= sMsgJsFun %>'); 

		<%if(sAcaoFuncao = "gnews") then %>
			window.close();
		<%end if%>
	<%end if%>

	<%if(sAcaoFuncao = "login") then%>
			parent.top.location.href = '<%= sLinkhttp %>AreaCadastral.asp';
	<%elseif (sAcaoFuncao = "logout") then%>
			parent.top.location.href = '<%= sLinkhttp %>';
	<%elseif (sAcaoFuncao = "lista") then%>
			parent.top.location.href = '<%= sLinkhttp %>detalhes.asp?produto=<%= Replace_Caracteres_Especiais(request("produto")) %>';
	<%else%>
		history.back(-1);
	<%end if%>
</script>