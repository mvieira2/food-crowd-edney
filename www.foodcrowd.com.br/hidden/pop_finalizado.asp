﻿<!--#include virtual="/hidden/funcoes.asp"-->
<%
dim sIdCateg, sTipo

sIdCateg 	= cdbl(Replace_Caracteres_Especiais(Request("pedido")))
sTipo 		= Replace_Caracteres_Especiais(Request("tipo"))
Idioma 		= "br"


call Abre_Conexao()
%>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><%=Application("NomeLoja")%></title>
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
<link rel="stylesheet" href="//css/style.css"/>
<script src="/js/jquery-1.5.2.min.js"></script>
<!-- Add Carrousel -->
<script src="/js/jquery.easing.js"></script>
<script src="/js/jquery.carousel.min.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="/js/fbox/jquery.fancybox.js?v=2.0.6"></script>
<link rel="stylesheet" type="text/css" href="//js/fbox/jquery.fancybox.css?v=2.0.6" media="screen" />
<script type="text/javascript" src="/js/funcoes_ui.js"></script>
<script type="text/javascript" src="/js/etalege/jquery.etalage.min.js"></script>
<link rel="stylesheet" href="//js/etalege/etalage.css"/>
</head>
<body  onLoad="javascript: window.print();"> 
    <div id="main" style="width:auto">
        <section class="container">
            <hr>
            <h3>DETALHES DO PEDIDO Nº <%= right(("00000000" & sIdCateg), 8) %></h3>
            <hr>
            <p>Este Pedido foi solicitado através de <%= trim(Recupera_Campos_Db_oConn("TB_FORMAS_PAGAMENTO", trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_FrmPagamento")), "id", "Descricao")) %> </p>
            <table style="margin:15px" border="0" cellpadding="" cellspacing="0" width="98%">
                 <tbody>
                    <!--#include virtual="/hidden/campos_finalizado_pt.asp"-->
	            </tbody>
            </table>      
        </section>
    </div>


    <%call Fecha_Conexao()%>
</body>
</html>
