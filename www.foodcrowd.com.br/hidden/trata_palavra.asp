<%
' =======================================================================
function Recupera_Result_Cliente(prTipo, prCliente)
	if(prTipo = "" OR prCliente = "") then exit function
	
	dim sSQl, RS
	
	call Abre_Conexao()
	
	Select case prTipo
		case "1"
			sSql = "Select top 1 DataCad from TB_PEDIDOS where id_cliente = " & prCliente & " order by id desc"
			Set Rs = oConn.execute(sSql)
			if(not rs.eof) then Recupera_Result_Cliente = rs("DataCad")
			Set Rs = nothing
		case "2"
			sSql = "Select sum(Total) as total, count(*) as qtd from TB_PEDIDOS where id_cliente = " & prCliente
			Set Rs = oConn.execute(sSql)
			if(not rs.eof) then Recupera_Result_Cliente = (rs("total") / rs("qtd"))
			Set Rs = nothing
		case "3"
			sSql = "Select count(*) as qtd from TB_PEDIDOS where id_cliente = " & prCliente
			Set Rs = oConn.execute(sSql)
			if(not rs.eof) then Recupera_Result_Cliente = (rs("qtd"))
			Set Rs = nothing
	end select
	
end function


' ================================================================
Function Trata_Palavra(Palavra)
	Dim Letra, sPalavraC
	
	if(Palavra = "" OR isnull(Palavra)) then exit function
	
	Trata_Palavra = ""
	sPalavraC = ""
	
	for i = 1 to len(Palavra)
		Letra = mid(Palavra, i, 1)

		Select Case lcase(Letra)
			Case "�","�","�","�","�"
				Letra = "a"
			Case "�","�","�"
				Letra = "e"
			Case "�","�","�"
				Letra = "i"
			Case "�","�","�","�"
				Letra = "o"
			Case "�","�"
				Letra = "u"
			Case "�"
				Letra = "c"
			Case "�"
				Letra = ""
			Case "-"
				Letra = ""
			Case "&"
				Letra = ""
		End Select

		sPalavraC = (sPalavraC & ucase(Letra))
	next

	Trata_Palavra = sPalavraC
End Function
%>