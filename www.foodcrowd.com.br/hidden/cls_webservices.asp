<%

'**************************************************** 
' Classe para WebService
'****************************************************
Class WebService
  Public Url
  Public Method
  Public Response
  Public Parameters
 
  ' Funcao para Invokar o WebService
  Public Function Invoke()
    Dim xmlhttp
    Set xmlhttp = Server.CreateObject("MSXML2.XMLHTTP")
    xmlhttp.open "POST", Url & "/" & Method, false
    xmlhttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
    xmlhttp.Send Parameters.toString
    response = xmlhttp.responseText
    set xmlhttp = nothing
  End Function
  
  
  Private Sub Class_Initialize()
    Set Parameters = New wsParameters
  End Sub
  
  Private Sub Class_Terminate()
    Set Parameters = Nothing
  End Sub
  
End class

'**************************************************** 
' Classe para wsParameters
'****************************************************
Class wsParameters
  Public mCol
  Public Function toString()
    Dim nItem
    Dim buffer
    buffer = ""
    For nItem = 1 to Count
      buffer = buffer & Item(nItem).toString & "&"
    Next
    If right(buffer,1)="&" then
      buffer = left(buffer,len(buffer)-1)
    End if
    toString = buffer 
  End Function
  
  Public Sub Clear
    set mcol = nothing 
    Set mCol = Server.CreateObject("Scripting.Dictionary") 
  End Sub
  
  Public Sub Add(pKey,pValue)
    Dim NewParameter
  
    Set NewParameter = New wsParameter
    NewParameter.Key = pKey
    NewParameter.Value = pValue
    mCol.Add mCol.count+1, NewParameter
  
    Set NewParameter = Nothing
  End Sub
  
  Public Function Item(nKey)
    Set Item=mCol.Item(nKey)
  End Function
  
  Public Function ExistsXKey(pKey)
    Dim nItem
  
    For nItem = 1 to mcol.count
      If mCol.Item(nItem).key = pKey Then
        ExistsXKeyword = True
        Exit For
      End if
    Next
  End Function
  
  Public Sub Remove(nKey)
    mCol.Remove(nKey)
  End sub
  
  Public Function Count()
    Count=mCol.count
  End Function
  
  Private Sub Class_Initialize()
    Set mCol = Server.CreateObject("Scripting.Dictionary")
  End Sub
  
  Private Sub Class_Terminate()
    Set mCol = Nothing
  End Sub
  
End class

'**************************************************** 
' Classe para wsParameter
'****************************************************
Class wsParameter
   Public Key
   Public Value
   Public Function toString()
     toString = Key & "=" & Value
   End Function
End Class
%>