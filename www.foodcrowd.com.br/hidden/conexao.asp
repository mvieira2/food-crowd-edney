<%
Session.LCID = 1046 ' define o aplicativo para regiao do brasil
Server.ScriptTimeOut = 65500

Dim sStringConexao, oConn, sOrigemSite
sStringConexao = Application("StringConexaoODBC")


' =======================================================================
sOrigemSite = Replace_Caracteres_Especiais(trim(Request("OrigemSite")))

if(sOrigemSite <> "") then
	if(isnumeric(sOrigemSite)) then
 		if(Recupera_Campos_Db("tb_pedidos_origem", sOrigemSite, "Id", "Descricao") <> "") then
 			Session("OrigemSite") = sOrigemSite
 		end if
 	else
 		Session("OrigemSite") = ""
 	end if
end if
' =======================================================================


' =======================================================================
sub Subtrai_Estoque(IdProduto, nQtde)

	if(IdProduto = "") then exit sub
	
	dim sSql, Rs, Rs1, sEstoqueAtual

	call Abre_conexao()
	
	sSql = "Select Qte_Estoque from TB_PRODUTOS (nolock) where id = " & IdProduto
	set Rs = oConn.execute(sSql)
	
	if(not Rs.eof) then
		sEstoqueAtual = rs("Qte_Estoque")
		
		if(sEstoqueAtual <> "0" and sEstoqueAtual <> "" and not isnull(sEstoqueAtual)) then 
			sEstoqueAtual = (cdbl(sEstoqueAtual) - cdbl(nQtde))
			if(sEstoqueAtual < 0) then sEstoqueAtual = 0
			
			oConn.execute("Update TB_PRODUTOS set Qte_Estoque = "& sEstoqueAtual &" where id = " & IdProduto)
			
			if(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", IdProduto, "id", "BoolBundle")) = "1") then
				sSql = "Select id_produto_bundle from TB_PRODUTOS_BUNDLE (nolock) where id_produto = " & IdProduto
				set Rs1 = oConn.execute(sSql)
				if(not Rs1.eof) then
					do while not Rs1.eof
						sEstoqueAtual = rs("Qte_Estoque")
						sEstoqueAtual = (cdbl(sEstoqueAtual) - cdbl(nQtde))
						oConn.execute("Update TB_PRODUTOS set Qte_Estoque = "& sEstoqueAtual &" where id = " & Rs1("id_produto_bundle"))
					Rs1.movenext
					loop
				end if
				set Rs1 = nothing
			end if
		end if
	end if
	set Rs = nothing
end sub

' =======================================================================
sub Subtrai_Estoque_Variacao_Abacos(IdProduto, nQtde)

	if(IdProduto = "") then exit sub
	
	dim sSql, Rs, Rs1, sEstoqueAtual

	call Abre_conexao()
	
	sSql = "Select Qtd_Estoque from TB_IMAGENS_AUXILIARES (nolock) where CodAbacos = '"& IdProduto & "'"
	set Rs = oConn.execute(sSql)
	
	if(not Rs.eof) then
		sEstoqueAtual = rs("Qtd_Estoque")
		
		if(sEstoqueAtual <> "0" and sEstoqueAtual <> "" and not isnull(sEstoqueAtual)) then 
			sEstoqueAtual = (cdbl(sEstoqueAtual) - cdbl(nQtde))
			if(sEstoqueAtual < 0) then sEstoqueAtual = 0
			oConn.execute("Update TB_IMAGENS_AUXILIARES set Qtd_Estoque = "& sEstoqueAtual &" where CodAbacos = '"& IdProduto & "'")
		end if
	end if
	set Rs = nothing
end sub

' =======================================================================
sub Somar_Estoque(IdProduto, nQtde)

	if(IdProduto = "") then exit sub

	call Abre_conexao()
	
	dim sSql, Rs, sEstoqueAtual

	sSql = "Select Qte_Estoque from TB_PRODUTOS (nolock) where id = " & IdProduto
	set Rs = oConn.execute(sSql)
	
	if(not Rs.eof) then
		sEstoqueAtual = rs("Qte_Estoque")
		
		if(sEstoqueAtual <> "" and not isnull(sEstoqueAtual)) then 
			sEstoqueAtual = (cdbl(sEstoqueAtual) + cdbl(nQtde))
			oConn.execute("Update TB_PRODUTOS set Qte_Estoque = "& sEstoqueAtual &" where id = " & IdProduto)
		end if
	end if
	set Rs = nothing
end sub


' =======================================================================
function Recupera_Id_Provincia(sDescProvincia)
	dim Rs1, sSql1, sCampoIf
	
	call Abre_Conexao()
	
	sSql1 = "Select top 1 id from TB_ESTADOS (nolock) where Sigla = '"& sDescProvincia &"' "
	Set Rs1 = oConn.execute(sSql1)
	if(not rs1.eof) then Recupera_Id_Provincia = trim(rs1("id"))
	Set Rs1 = nothing
end function


' =======================================================================
function Replace_Caracteres_Especiais(prConteudo)
	
	if(trim(prConteudo) <> "" and (InStr(lcase(trim(prConteudo)),"cript") > 0 OR InStr(lcase(trim(prConteudo)),"alert") > 0 OR InStr(lcase(trim(prConteudo)),"<") > 0  OR InStr(lcase(trim(prConteudo)),">") > 0) ) then
		response.write "<P><div align=""center"">ERRO NO SISTEMA!</div></P>"
		response.end
	end if
	
	Replace_Caracteres_Especiais = replace(replace(replace(replace(replace(replace(replace(replace(prConteudo, "'", ""), """", ""), ")", ""), "(", ""), ";", ""), "|", ""), "%", ""), "`", "")
end function
'========================================================================
function Recupera_Campos_Db(sTabela, sValorWhere, sCampoWhere, sCampoRetorno)
	Dim sSQl, oRs
	
	call Abre_Conexao()
	
	sSQL = "Select "& sCampoRetorno &" from "& sTabela &" (nolock) where "& sCampoWhere &" = " & sValorWhere

	if((sValorWhere = "") and (sCampoWhere = "")) then 
		sSQL = "Select "& sCampoRetorno &" from "& sTabela
	end if
	if((sValorWhere <> "") and (not isNumeric(sValorWhere))) then 
		sSQL = "Select "& sCampoRetorno &" from "& sTabela &"  where convert(varchar(50),"& sCampoWhere &") = '" & replace(sValorWhere, "'", "") & "'"
	end if
	if((sValorWhere = "") OR (Isnull(sValorWhere))) then 
		sSQL = "Select "& sCampoRetorno &" from "& sTabela
	end if
	
'	response.write ssql
'	response.end
	
	set oRs = oConn.execute(sSQL)

	if(not oRs.eof) then 
		Recupera_Campos_Db = oRs(sCampoRetorno)
	else
		Recupera_Campos_Db = ""
	end if

	oRs.close
	
	call Fecha_Conexao()
end function


'========================================================================
function Recupera_Campos_Db_oConn(sTabela, sValorWhere, sCampoWhere, sCampoRetorno)
	Dim sSQl, oRs
	
	If(oConn.State = "0") then
		call Abre_Conexao()
	end if
	
	sSQL = "Select "& sCampoRetorno &" from "& sTabela &" (nolock) where "& sCampoWhere &" = " & sValorWhere

	if((sValorWhere = "") and (sCampoWhere = "")) then 
		sSQL = "Select "& sCampoRetorno &" from "& sTabela
	end if
	if((sValorWhere <> "") and (not isNumeric(sValorWhere))) then 
		sSQL = "Select "& sCampoRetorno &" from "& sTabela &"  where convert(varchar(50),"& sCampoWhere &") = '" & replace(sValorWhere, "'", "") & "'"
	end if
	if((sValorWhere = "") OR (Isnull(sValorWhere))) then 
		sSQL = "Select "& sCampoRetorno &" from "& sTabela
	end if
	
'	response.write ssql & "<br>"
'	response.end
	
	set oRs = oConn.execute(sSQL)

	if(not oRs.eof) then 
		Recupera_Campos_Db_oConn = oRs(sCampoRetorno)
	else
		Recupera_Campos_Db_oConn = ""
	end if

	oRs.close
end function
' =========================================================================
sub Abre_Conexao()
'	response.write sStringConexao
'	response.end
	Set oConn = Server.CreateObject("ADODB.Connection")
	oConn.Open sStringConexao
end sub

' =========================================================================
sub Fecha_Conexao()
	if(oConn.state = 1) then oConn.close
end sub

%>