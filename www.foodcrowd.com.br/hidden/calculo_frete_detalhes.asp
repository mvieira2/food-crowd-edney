﻿<!--#include virtual="/hidden/funcoes.asp"-->

<%
'response.Charset = "ISO-8859-1"

dim sCepAgendamento, ValorCompra, sTotalPeso, sTipoPagina, nValorManuseio, sTotalQuantidade, sProduto, sValorFreteTotal, prProduto
dim prsDataPreVendaIni, prsDataPreVendaFim, prboolPreVenda, sTipoPaginaSaida
dim EstadoFrete, CidadeFrete

prProduto					= Replace_Caracteres_Especiais(request("produto"))
sCepAgendamento				= Replace_Caracteres_Especiais(request("cepAG"))
ValorCompra					= Replace_Caracteres_Especiais(request("valor"))
sTotalPeso					= Replace_Caracteres_Especiais(request("peso"))
sTipoPagina					= Replace_Caracteres_Especiais(request("tipo"))
sTipoPaginaSaida			= Replace_Caracteres_Especiais(request("tiposaida"))
CidadeFrete			        = Replace_Caracteres_Especiais(request("CidadeFrete"))
EstadoFrete                 = Replace_Caracteres_Especiais(request("EstadoFrete"))
nValorManuseio 				= 0
sPaginaPedido				= false

'response.write "1-" & prProduto & "<br>"
'response.write "1-" & sCepAgendamento & "<br>"
'response.write "1-" & ValorCompra & "<br>"
'response.write "1-" & sTotalPeso & "<br>"
'response.write "1-" & sTipoPagina & "<br>"
'response.End

if (Replace_Caracteres_Especiais(trim(request("PaginaPedido"))) = "1") then 
	sPaginaPedido			= true
end if

if(trim(Recupera_Campos_Db("TB_PRODUTOS", prProduto, "id", "Prevenda")) = "1" and prProduto <> "") then 
	prsDataPreVendaIni	= trim(Recupera_Campos_Db("TB_PRODUTOS", prProduto, "id", "DataIniPreVenda"))
	prsDataPreVendaFim	= trim(Recupera_Campos_Db("TB_PRODUTOS", prProduto, "id", "DataFimPreVenda"))
	
	if((prsDataPreVendaIni) <> "" and not isnull(prsDataPreVendaIni)) then prsDataPreVendaIni = cdate(prsDataPreVendaIni)
	if((prsDataPreVendaFim) <> "" and not isnull(prsDataPreVendaFim)) then prsDataPreVendaFim = cdate(prsDataPreVendaFim)
	
	prboolPreVenda 		    = (date >= prsDataPreVendaIni AND date <= prsDataPreVendaFim)
    session("boolPreVenda") = trim(Recupera_Campos_Db("TB_PRODUTOS", prProduto, "id", "Prazo_Garantia"))
end if

if (ValorCompra <> "") 	then ValorCompra	= replace(ValorCompra,".",",")
if (sTotalPeso <> "") 	then sTotalPeso		= replace(sTotalPeso,".",",")

if (ucase(sTipoPagina) = "DETALHES") THEN
	sProduto					= Replace_Caracteres_Especiais(request("produto"))
	nValorManuseio 				= Recupera_Campos_Db("TB_PRODUTOS", sProduto, "id", "Preco_Manuseio")
	sTotalQuantidade 			= 1
end if

boolDataCampanhaFrete 	    = Monta_Campanha_Frete()
sValorFreteTotal 		    = RetornaCasasDecimais(Calcula_Frete("1", sCepAgendamento, ValorCompra),2)

if(ValorCompra = "") then 				ValorCompra 			= 0
if(sValorFreteTotal = "") then 			sValorFreteTotal 		= 0
if(Session("sMontaTotal") = "") then 	Session("sMontaTotal") 	= 0

' ==================================================================================
if (sAgendamento <> 1) then
    sConteudo = sConteudo & "<table width=""80%"" border=0>"
    sConteudo = sConteudo & "    <tr>"
    sConteudo = sConteudo & "        <td width=""100%"" class=""TextoMaior_2"">"
    
    'sConteudo = sConteudo & "            <span >ENTREGA EXPRESSA </span>"
    'sConteudo = sConteudo & "            <br>"
    'sConteudo = sConteudo & "            <b>PRAZO DE ENTREGA: </b>"'2 dias úteis

	if(session("boolPreVenda") <> "" ) then 
		sConteudo = sConteudo & "a partir de "& session("boolPreVenda") &""
	else
		'sConteudo = sConteudo & ""& Session("DiaPrazoTotal") &" dias &uacute;teis"

	    if(InStr(ucase("," & CidadesAtendidas & ","), ucase("," & Session("RetornaCidade") & ",") ) > 0 ) then 
            sConteudo = sConteudo & Application("DadosEntrega")
        end if
	end if

    if(InStr(ucase("," & CidadesAtendidas & ","), ucase("," & Session("RetornaCidade") & ",") ) = 0 ) then 
        sConteudo = sConteudo & "<span style=""color: rgb(240, 0, 0); font-size: 14px; font-weight: bold;"">CIDADE NÃO ATENDIDA!</span>"
        sConteudo = sConteudo & "<script>$('#labelmudarcep').show(); $('#btcontinuar1').hide(); </script>"
        sConteudo = sConteudo & "<script>$('#finalizar_final').hide(); </script>"
    else
        sConteudo = sConteudo & "            <br/><br/>"
        sConteudo = sConteudo & "   <b>Cidade: </b>"
        sConteudo = sConteudo & "   <span style=""color: rgb(240, 0, 0); font-size: 14px; font-weight: bold;""> "& Session("RetornaCidade") &"</span>"
        sConteudo = sConteudo & "   <br><b>Valor do Frete: </b>"

	    if (sValorFreteTotal = 0) then 
		    sConteudo = sConteudo & "<span style=""color: rgb(240, 0, 0); font-size: 14px; font-weight: bold;""> FRETE GRÁTIS</span>"
	    else
		    sConteudo = sConteudo & "<span style=""color: rgb(240, 0, 0); font-size: 12px; font-weight: bold;""> R$"& sValorFreteTotal &"</span>"
	    end if

        sConteudo = sConteudo & "<script>$('#labelmudarcep').hide(); $('#btcontinuar1').show(); </script>"
        sConteudo = sConteudo & "<script>$('#finalizar_final').show(); </script>"
    end if

    'sConteudo = sConteudo & "            <b>VALOR DO FRETE (1): </b>"


    sConteudo = sConteudo & "        </td>" 
    
    'sConteudo = sConteudo & "        <td width=""404"" class=""TextoMaior_2 desktop2"">"
    'sConteudo = sConteudo & "            <div >"
    'sConteudo = sConteudo & "                <span >ENTREGA AGENDADA</span><br>"

    'if (sCepAgendamento < "19999999") then
    '    sConteudo = sConteudo & "            A entrega para este CEP é efetuada pelos Correios. Portanto não é possível agendar a entrega."
    'else
    '    sConteudo = sConteudo & "            Consulte datas disponíveis e custo da entrega<br>na página de finalização do seu pedido."
    'end if

    'sConteudo = sConteudo & "            </div>"
    'sConteudo = sConteudo & "        </td>"
    sConteudo = sConteudo & "    </tr>"
    'sConteudo = sConteudo & "   <tr>"
    'sConteudo = sConteudo & "       <td class=""TextoMaior_2"" colspan=""3"">"
    'sConteudo = sConteudo & "           <div class=""divisao_carrinho""></div>"
    'sConteudo = sConteudo & "       </td>"
    'sConteudo = sConteudo & "    </tr>"
    sConteudo = sConteudo & "</table>"
' ==================================================================
else
' ==================================================================
    sConteudo = sConteudo & "<br><table width=""90%"">"
    sConteudo = sConteudo & "    <tr>"
    sConteudo = sConteudo & "        <td width=""371"" class=""TextoMaior_2"">"
    sConteudo = sConteudo & "            <br>"
    if (sPaginaPedido AND ucase(sTipoPagina) <> "DETALHES" AND ucase(sTipoPagina) <> "CESTA") then
		sConteudo = sConteudo & "<input type=""radio"" name=""radio"" id=""eagenda"" "
		if (not session("sCheckedAgendamento")) then 
			sConteudo = sConteudo & "checked"
		end if
		sConteudo = sConteudo & " value="& sValorFreteTotal &">"
	End If
    
    'sConteudo = sConteudo & "            <span >ENTREGA EXPRESSA </span>"
    
    sConteudo = sConteudo & "<br> "

    'sConteudo = sConteudo & "            <b>PRAZO DE ENTREGA: </b>"'2 dias úteis

	if(session("boolPreVenda") <> "" ) then 
		sConteudo = sConteudo & "a partir de "& session("boolPreVenda") &""
	else
		'sConteudo = sConteudo & ""& Session("DiaPrazoTotal") &" dias &uacute;teis"
		sConteudo = sConteudo & Application("DadosEntrega")
	end if

    sConteudo = sConteudo & "            <br>"
    sConteudo = sConteudo & "            <b>VALOR DO FRETE  (2): </b>"
	if (sValorFreteTotal = 0) then 
		sConteudo = sConteudo & "<span style=""color: rgb(240, 0, 0); font-size: 14px; font-weight: bold;""> FRETE GRÁTIS</span>"
	else
		sConteudo = sConteudo & "<span style=""color: rgb(240, 0, 0); font-size: 12px; font-weight: bold;""> R$"& sValorFreteTotal &"</span>"
	end if
    sConteudo = sConteudo & "        </td>"
    sConteudo = sConteudo & "        <td width=""30"">"
    sConteudo = sConteudo & "            <div style=""height: 40px; width: 1px; background-color: #CCC;""></div>"
    sConteudo = sConteudo & "        </td>"
    sConteudo = sConteudo & "        <td width=""404"" class=""TextoMaior_2 desktop2"">"
    sConteudo = sConteudo & "            <br>"
    sConteudo = sConteudo & "            <div style=""text-transform: uppercase;"">"
	if (sPaginaPedido AND ucase(sTipoPagina) <> "DETALHES" AND ucase(sTipoPagina) <> "CESTA") then
		sConteudo = sConteudo & "			<input type=""radio"" name=""radio"" id=""ragenda"" "
		if (session("sCheckedAgendamento")) then 
			sConteudo = sConteudo & "checked"
		end if
		sConteudo = sConteudo & " value="& sValorFreteTotal &">"
	end if	
    sConteudo = sConteudo & "                <span >ENTREGA AGENDADA</span>"
    sConteudo = sConteudo & "<br> "
	if (session("sDatadaentregaAgendamento") <> "" and session("sPeriodoAgendamento") <> "") then
		if (ucase(sTipoPagina) <> "DETALHES" AND ucase(sTipoPagina) <> "CESTA") then
			sConteudo = sConteudo & "<b>DATA DA ENTREGA: </b> "& Session("sDatadaentregaAgendamento") &"<br>"
			sConteudo = sConteudo & "<b>PERÍODO DE ENTREGA: </b> "& Session("sPeriodoAgendamento") &"<br>"
		end if
	end if
    if (sPaginaPedido AND ucase(sTipoPagina) <> "DETALHES" AND ucase(sTipoPagina) <> "CESTA") then
        sConteudo = sConteudo & "            <b>VALOR DO FRETE  (3): </b>"
	    if (FormatNumber((Cdbl(sValorFreteTotal) + Cdbl(Session("sAcrescimoAgendamento"))),2) = 0) then 
		    sConteudo = sConteudo & "<span style=""color: rgb(240, 0, 0); font-size: 12px; font-weight: bold;""> FRETE GRÁTIS</span>"
	    else
		    sConteudo = sConteudo & "<span style=""color: rgb(240, 0, 0); font-size: 12px; font-weight: bold;""> R$"& FormatNumber((Cdbl(sValorFreteTotal) + Cdbl(Session("sAcrescimoAgendamento"))),2) &"</span>"
	    end if
    end if
	if (ucase(sTipoPagina) = "DETALHES" OR ucase(sTipoPagina) = "CESTA") then
		sConteudo = sConteudo & " Consulte datas disponíveis e custo da entrega na página de finalização do seu pedido."
	end if


    sConteudo = sConteudo & "            </div>"
    sConteudo = sConteudo & "        </td>"
    sConteudo = sConteudo & "    </tr>"
    sConteudo = sConteudo & "   <tr>"
    sConteudo = sConteudo & "       <td class=""TextoMaior_2"" colspan=""3"">"
    sConteudo = sConteudo & "           <div class=""divisao_carrinho""></div>"
    sConteudo = sConteudo & "       </td>"
    sConteudo = sConteudo & "    </tr>"
    sConteudo = sConteudo & "</table>"
end if


' =====================================================================
Select case sTipoPaginaSaida
    Case ""
        response.write sConteudo        
    Case "frete"
        response.write "R$" & FormatNumber(Cdbl(sValorFreteTotal),2)        
    Case "total"
        response.write "R$" & FormatNumber(Cdbl(sValorFreteTotal) + cdbl(ValorCompra),2)        
    Case "subfrete"
        response.write "R$" & FormatNumber((Cdbl(sValorFreteTotal) + Cdbl(ValorCompra)+ cdbl(Session("ValorPresente"))),2)           
end select 


'response.write "<script type=""text/javascript"">" & vbcrlf
'response.write "	$(""#freteajax"").asp('R$ "& FormatNumber(Cdbl(sValorFreteTotal),2) &"');" & vbcrlf
'response.write "	$(""#totalajax"").asp('R$ "& FormatNumber(Cdbl(sValorFreteTotal) + cdbl(ValorCompra),2) &"');" & vbcrlf
'response.write "	$(""#subfrete"").asp('R$ "& FormatNumber((Cdbl(sValorFreteTotal) + Cdbl(ValorCompra)+ cdbl(Session("ValorPresente"))),2) &"');" & vbcrlf
'response.write "	$(""#capital"").asp('"& sConteudo &"');" & vbcrlf
'response.write "  alert('"& sConteudo &"')" & vbcrlf
'response.write "</script>" & vbcrlf
' =======================================================================
%>

<script type="text/javascript">
   // $(document).ready(function(){

	<%'if (session("sCheckedAgendamento")) then %>
		// $('#freteajax').asp('R$&nbsp;<%= FormatNumber((Cdbl(sValorFreteTotal) + Cdbl(Session("sAcrescimoAgendamento"))),2)%>');
        // $('#totalajax').asp('R$&nbsp;<%= FormatNumber((Cdbl(sValorFreteTotal) + Cdbl(ValorCompra)),2)%>');
        // $('#subfrete').asp('R$&nbsp;<%= FormatNumber((Cdbl(sValorFreteTotal) + Cdbl(ValorCompra) + cdbl(Session("ValorPresente"))),2)%>');
	<%'else %>
       // $('#freteajax').asp('R$ <%= FormatNumber(Cdbl(sValorFreteTotal),2)%>');
       // $('#totalajax').asp('R$ <%= FormatNumber(Cdbl(sValorFreteTotal + cdbl(ValorCompra)),2)%>');
        // $('#subfrete').asp('R$ <%= FormatNumber((Cdbl(sValorFreteTotal) + Cdbl(ValorCompra)+ cdbl(Session("ValorPresente"))),2)%>');
	<%'end if %>

    // });

</script>
