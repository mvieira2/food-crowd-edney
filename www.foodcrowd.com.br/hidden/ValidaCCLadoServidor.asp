<%
'Para validar o cart�o e a operadora
'	IsCartaoCredito("NUMERO DO CART�O", "OPERADORA")
'
'Para validar apenas o numero do cart�o
'	IsCartaoCredito("NUMERO DO CART�O", "")



Function IsCartaoCredito(ByVal strNumeroCartao, strTipoCartao)
    'Verificando se o valor passado � todo numerico
    If Not IsNumeric(strNumeroCartao) Then
        Retorno = False
    Else
        Retorno = True
    End If
    
    'Valor � num�rico
    If Retorno Then
        'Selecionando o prefixo do cart�o
        strTipoCartao    =    Ucase(strTipoCartao)
        Select Case strTipoCartao
            Case "MASTERCARD"
                strExpressaoRegular = "^5[1-5]\d{14}$"
            Case "VISA"
                strExpressaoRegular = "^4(\d{12}|\d{15})$"
            Case "AMEX"
                strExpressaoRegular = "^3(3|7)\d{14}$"
            Case "DINERSCLUB"
                strExpressaoRegular = "^3((6|8)\d{12})|(00|01|02|03|04|05)\d{11})$"
        End Select
            
        'Validando o formato do cart�o de cr�dito
        Set regEx = New RegExp                            ' Cria o Objeto Express�o
        regEx.Pattern = strExpressaoRegular                  ' Express�o Regular
        regEx.IgnoreCase = True                           ' Sensitivo ou n�o
        regEx.Global = True                               
        Retorno = RegEx.Test(strNumeroCartao)
        Set regEx = Nothing
        if strTipoCartao	=	"" then Retorno = true
        'Formato correto
        If Retorno Then
            '-----------------------------------------
            'Processo de valida��o do numero do cart�o    
            '-----------------------------------------
            intVerificaSoma        = 0
            blnFlagDigito        = False 
            For Cont = Len(strNumeroCartao) To 1 Step -1
                Digito = Asc(Mid(strNumeroCartao, Cont, 1))        'Isola o caracter da vez
                If (Digito > 47) And (Digito < 58) Then            'Somente se for inteiro
                    Digito = Digito - 48                        'Converte novamente para numero (-48)
                    If blnFlagDigito Then                  
                        Digito = Digito + Digito                'Primeiro duplica-o
                        If Digito > 9 Then                        'Verifica se o Digito � maior que 9
                            Digito = Digito - 9                    'For�a ser somente um n�mero
                        End If
                    End If
                    blnFlagDigito = Not blnFlagDigito      
                    intVerificaSoma = intVerificaSoma + Digito   
                    If intVerificaSoma > 9 Then                
                        intVerificaSoma = intVerificaSoma - 10   'Mesmo que MOD 10 s� que mais rapido
                    End If
                End If
            Next
            If intVerificaSoma <> 0 Then ' Deve totalizar zero
                Retorno = False
            Else
                Retorno = True
            End If
            '-----------------------------------------
        End If
    End If
    'Retornando a fun��o
    IsCartaoCredito = Retorno
End Function

%>
