﻿<!--#include virtual="/hidden/funcoes.asp"-->
<%
'response.Charset = "ISO-8859-1"

dim sCepAgendamento, ValorCompra, sTotalPeso, sTipoPagina, nValorManuseio, sTotalQuantidade, sProduto, sValorFreteTotal, prProduto
dim prsDataPreVendaIni, prsDataPreVendaFim, prboolPreVenda, sTipoPaginaSaida

ValorCompra		= trim(Replace_Caracteres_Especiais(request("valor")))
cupom			= trim(request("cupom"))
sTipoPagina		= trim(Replace_Caracteres_Especiais(request("tipo")))

SeSsion("cupomdesconto")        = ""
SeSsion("cupomdescontovalor")   = 0

Select case lcase(cupom)

    case "crowd1000"
        sTotalCarrinho                  = cdbl(replace(ValorCompra,".",","))
        SeSsion("cupomdesconto")        = ""
        SeSsion("cupomdescontovalor")   = 0
        boolValidoCArrinho              = true

        if(boolValidoCArrinho) then 
            sDesconto10acess                = RetornaCasasDecimais(cdbl(sTotalCarrinho) * 0.1, 2)
            SeSsion("cupomdesconto")        = "crowd1000"
            SeSsion("cupomdescontovalor")   = sDesconto10acess

            if(sTipoPagina = "1") then        
                response.write RetornaCasasDecimais(sDesconto10acess, 2)
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(cdbl(sTotalCarrinho) - cdbl(sDesconto10acess), 2)
                response.End
            end if
        else
            if(sTipoPagina = "1") then        
                response.write "0,00"
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(replace(ValorCompra,".",","), 2)
                response.End
            end if
        end if

    case "maisde1000"
        sTotalCarrinho                  = cdbl(replace(ValorCompra,".",","))
        SeSsion("cupomdesconto")        = ""
        SeSsion("cupomdescontovalor")   = 0
        boolValidoCArrinho              = true

        if(boolValidoCArrinho) then 
            sDesconto10acess                = RetornaCasasDecimais(cdbl(sTotalCarrinho) * 0.2, 2)
            SeSsion("cupomdesconto")        = "maisde1000"
            SeSsion("cupomdescontovalor")   = sDesconto10acess

            if(sTipoPagina = "1") then        
                response.write RetornaCasasDecimais(sDesconto10acess, 2)
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(cdbl(sTotalCarrinho) - cdbl(sDesconto10acess), 2)
                response.End
            end if
        else
            if(sTipoPagina = "1") then        
                response.write "0,00"
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(replace(ValorCompra,".",","), 2)
                response.End
            end if
        end if

    case "a5led"
        sTotalCarrinho                  = cdbl(replace(ValorCompra,".",","))
        SeSsion("cupomdesconto")        = ""
        SeSsion("cupomdescontovalor")   = 0

        For icar = 1 to Session("ItemCount")               
            if(ARYShoppingcart(C_iD, icar)  = "500566") then 
                sDesconto10acess        = 40.10
                boolValidoCArrinho      = true
            end if
        next

        if(boolValidoCArrinho) then 
            SeSsion("cupomdesconto")        = "a5led"
            SeSsion("cupomdescontovalor")   = sDesconto10acess

            if(sTipoPagina = "1") then        
                response.write RetornaCasasDecimais(sDesconto10acess, 2)
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(cdbl(sTotalCarrinho) - cdbl(sDesconto10acess), 2)
                response.End
            end if
        else
            if(sTipoPagina = "1") then        
                response.write "0,00"
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(replace(ValorCompra,".",","), 2)
                response.End
            end if
        end if

    case "idol4"
        sTotalCarrinho                  = cdbl(replace(ValorCompra,".",","))
        SeSsion("cupomdesconto")        = ""
        SeSsion("cupomdescontovalor")   = 0

        For icar = 1 to Session("ItemCount")               
            if(ARYShoppingcart(C_iD, icar)  = "500574") then 
                sDesconto10acess        = 27.10
                boolValidoCArrinho      = true
            end if
        next

        if(boolValidoCArrinho) then 
            SeSsion("cupomdesconto")        = "idol4"
            SeSsion("cupomdescontovalor")   = sDesconto10acess

            if(sTipoPagina = "1") then        
                response.write RetornaCasasDecimais(sDesconto10acess, 2)
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(cdbl(sTotalCarrinho) - cdbl(sDesconto10acess), 2)
                response.End
            end if
        else
            if(sTipoPagina = "1") then        
                response.write "0,00"
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(replace(ValorCompra,".",","), 2)
                response.End
            end if
        end if

    case "a7"
        sTotalCarrinho                  = cdbl(replace(ValorCompra,".",","))
        SeSsion("cupomdesconto")        = ""
        SeSsion("cupomdescontovalor")   = 0

        For icar = 1 to Session("ItemCount")               
            if(ARYShoppingcart(C_iD, icar)  = "683703") then 
                sDesconto10acess        = 60.10
                boolValidoCArrinho      = true
            end if
        next

        if(boolValidoCArrinho) then 
            SeSsion("cupomdesconto")        = "a7"
            SeSsion("cupomdescontovalor")   = sDesconto10acess

            if(sTipoPagina = "1") then        
                response.write RetornaCasasDecimais(sDesconto10acess, 2)
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(cdbl(sTotalCarrinho) - cdbl(sDesconto10acess), 2)
                response.End
            end if
        else
            if(sTipoPagina = "1") then        
                response.write "0,00"
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(replace(ValorCompra,".",","), 2)
                response.End
            end if
        end if

    case "alcatelsemp"
        sTotalCarrinho                  = cdbl(replace(ValorCompra,".",","))
        SeSsion("cupomdesconto")        = ""
        SeSsion("cupomdescontovalor")   = 0
        boolValidoCArrinho              = true

        if(boolValidoCArrinho) then 
            sDesconto10acess                = RetornaCasasDecimais(cdbl(sTotalCarrinho) * 0.30, 2)
            SeSsion("cupomdesconto")        = "alcatelsemp"
            SeSsion("cupomdescontovalor")   = sDesconto10acess

            if(sTipoPagina = "1") then        
                response.write RetornaCasasDecimais(sDesconto10acess, 2)
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(cdbl(sTotalCarrinho) - cdbl(sDesconto10acess), 2)
                response.End
            end if
        else
            if(sTipoPagina = "1") then        
                response.write "0,00"
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(replace(ValorCompra,".",","), 2)
                response.End
            end if
        end if

    case "heptafriday" ' 32,10
        sTotalCarrinho                  = cdbl(replace(ValorCompra,".",","))
        SeSsion("cupomdesconto")        = ""
        SeSsion("cupomdescontovalor")   = 0

        For icar = 1 to Session("ItemCount")               
            if(ARYShoppingcart(C_iD, icar)  = "683703") then 
                sDesconto10acess        = 32.10
                boolValidoCArrinho      = true
            end if
        next

        if(boolValidoCArrinho) then 
            SeSsion("cupomdesconto")        = "heptafriday"
            SeSsion("cupomdescontovalor")   = sDesconto10acess

            if(sTipoPagina = "1") then        
                response.write RetornaCasasDecimais(sDesconto10acess, 2)
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(cdbl(sTotalCarrinho) - cdbl(sDesconto10acess), 2)
                response.End
            end if
        else
            if(sTipoPagina = "1") then        
                response.write "0,00"
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(replace(ValorCompra,".",","), 2)
                response.End
            end if
        end if

    case "selfiealcatel" ' 15% A3 xl max
        sTotalCarrinho                  = cdbl(replace(ValorCompra,".",","))
        SeSsion("cupomdesconto")        = ""
        SeSsion("cupomdescontovalor")   = 0

        For icar = 1 to Session("ItemCount")               
            if(ARYShoppingcart(C_iD, icar)  = "681931" OR ARYShoppingcart(C_iD, icar)  = "681932") then 
                sDesconto10acess        = RetornaCasasDecimais(cdbl(sTotalCarrinho) * 0.15, 2)
                boolValidoCArrinho      = true
            end if
        next

        if(boolValidoCArrinho) then 
            sDesconto10acess                = RetornaCasasDecimais(cdbl(sTotalCarrinho) * 0.15, 2)
            SeSsion("cupomdesconto")        = "selfiealcatel"
            SeSsion("cupomdescontovalor")   = sDesconto10acess

            if(sTipoPagina = "1") then        
                response.write RetornaCasasDecimais(sDesconto10acess, 2)
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(cdbl(sTotalCarrinho) - cdbl(sDesconto10acess), 2)
                response.End
            end if
        else
            if(sTipoPagina = "1") then        
                response.write "0,00"
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(replace(ValorCompra,".",","), 2)
                response.End
            end if
        end if

    case "proxymediaacessorios"
        SeSsion("cupomdesconto")        = ""
        SeSsion("cupomdescontovalor")   = 0

        if(true) then 
            sTotalCarrinho                      = cdbl(replace(ValorCompra,".",","))
            boolValidoCArrinho                  = false
            sTotalCarrinhoProdutosAcessorios    = 0

            call Abre_Conexao()

            For icar = 1 to Session("ItemCount")
                IdCategoriaCarrinho = Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYShoppingcart(C_ID, icar), "id", "Id_Categoria")
                
                if(IdCategoriaCarrinho = "196238") then 
                    sValorCarrinho          = cdbl(ARYShoppingcart(C_Preco, icar)) 
                    sDescontoValorProduto   = trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYShoppingcart(C_ID, icar), "id", "descontovalor"))

                    if (sDescontoValorProduto <> "" and not isnull(sDescontoValorProduto)) then
                        if (cdbl(sDescontoValorProduto) > 0) then
                            sValorCarrinho      = cdbl(sValorCarrinho) - cdbl(sDescontoValorProduto)
                        end if
                    end if

                    sTotalCarrinhoProdutosAcessorios    = (cdbl(sTotalCarrinhoProdutosAcessorios) + (sValorCarrinho * cdbl(ARYShoppingcart(C_Qtd, icar))) )
                    boolValidoCArrinho                  = true
                end if
            next

            call Fecha_Conexao()

            if(boolValidoCArrinho) then 
                sDesconto10acess                = (SeSsion("cupomdescontovalorBruto") * sTotalCarrinhoProdutosAcessorios )  
                SeSsion("cupomdesconto")        = "proxymediaacessorios"
                SeSsion("cupomdescontovalor")   = sDesconto10acess

                if(sTipoPagina = "1") then        
                    response.write RetornaCasasDecimais(sDesconto10acess, 2)
                    response.End
                else
                    response.write "R$" & RetornaCasasDecimais(cdbl(sTotalCarrinho) - cdbl(sDesconto10acess), 2)
                    response.End
                end if
            else
                if(sTipoPagina = "1") then        
                    response.write "0,00"
                    response.End
                else
                    response.write "R$" & RetornaCasasDecimais(replace(ValorCompra,".",","), 2)
                    response.End
                end if
            end if
        else
            if(sTipoPagina = "1") then        
                response.write "0,00"
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(replace(ValorCompra,".",","), 2)
                response.End
            end if
        end if


    case "proxymedia5"
        sTotalCarrinho                  = cdbl(replace(ValorCompra,".",","))
        SeSsion("cupomdesconto")        = ""
        SeSsion("cupomdescontovalor")   = 0

        if(InStr(Session("bannerid"), "proxymedia") > 0) then 
            boolValidoCArrinho      = true
        else
            boolValidoCArrinho      = false
        end if

        if(boolValidoCArrinho) then 
            sDesconto10acess                = RetornaCasasDecimais(cdbl(sTotalCarrinho) * 0.05, 2)
            SeSsion("cupomdesconto")        = "proxymedia5"
            SeSsion("cupomdescontovalor")   = sDesconto10acess

            if(sTipoPagina = "1") then        
                response.write RetornaCasasDecimais(sDesconto10acess, 2)
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(cdbl(sTotalCarrinho) - cdbl(sDesconto10acess), 2)
                response.End
            end if
        else
            if(sTipoPagina = "1") then        
                response.write "0,00"
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(replace(ValorCompra,".",","), 2)
                response.End
            end if
        end if


    case "timaocampeao"
        sTotalCarrinho                  = cdbl(replace(ValorCompra,".",","))
        SeSsion("cupomdesconto")        = ""
        SeSsion("cupomdescontovalor")   = 0

        For icar = 1 to Session("ItemCount")               
            if(ARYShoppingcart(C_iD, icar)  = "681931" OR ARYShoppingcart(C_iD, icar)  = "681932") then 
                sDesconto10acess        = "109,90"
                boolValidoCArrinho      = true
            end if
        next

        if(boolValidoCArrinho) then 
            sDesconto10acess                = "109,90"
            SeSsion("cupomdesconto")        = "timaocampeao"
            SeSsion("cupomdescontovalor")   = sDesconto10acess

            if(sTipoPagina = "1") then        
                response.write RetornaCasasDecimais(sDesconto10acess, 2)
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(cdbl(sTotalCarrinho) - cdbl(sDesconto10acess), 2)
                response.End
            end if
        else
            if(sTipoPagina = "1") then        
                response.write "0,00"
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(replace(ValorCompra,".",","), 2)
                response.End
            end if
        end if

    case "10off"
        SeSsion("cupomdesconto")        = ""
        SeSsion("cupomdescontovalor")   = 0

        if(Month(date) <= 7 and Year(date) = 2017) then 
            sTotalCarrinho                      = cdbl(replace(ValorCompra,".",","))
            boolValidoCArrinho                  = true

            call Abre_Conexao()

            For icar = 1 to Session("ItemCount")
                sValorCarrinho          = cdbl(ARYShoppingcart(C_Preco, icar)) 
                sDescontoValorProduto   = trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYShoppingcart(C_ID, icar), "id", "descontovalor"))

                if (sDescontoValorProduto <> "" and not isnull(sDescontoValorProduto)) then
                    if (cdbl(sDescontoValorProduto) > 0) then
                        sValorCarrinho      = cdbl(sValorCarrinho) - cdbl(sDescontoValorProduto)
                    end if
                end if

                sTotalCarrinhoProdutosAcessorios    = (cdbl(sTotalCarrinhoProdutosAcessorios) + (sValorCarrinho * cdbl(ARYShoppingcart(C_Qtd, icar))) )
                boolValidoCArrinho                  = true
            next

            call Fecha_Conexao()

            if(boolValidoCArrinho) then 
                sDesconto10acess            = (0.1 * sTotalCarrinhoProdutosAcessorios )  

                SeSsion("cupomdesconto")        = "10off"
                SeSsion("cupomdescontovalor")   = sDesconto10acess

                if(sTipoPagina = "1") then        
                    response.write RetornaCasasDecimais(sDesconto10acess, 2)
                    response.End
                else
                    response.write "R$" & RetornaCasasDecimais(cdbl(sTotalCarrinho) - cdbl(sDesconto10acess), 2)
                    response.End
                end if
            else
                if(sTipoPagina = "1") then        
                    response.write "0,00"
                    response.End
                else
                    response.write "R$" & RetornaCasasDecimais(replace(ValorCompra,".",","), 2)
                    response.End
                end if
            end if
        else
            if(sTipoPagina = "1") then        
                response.write "0,00"
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(replace(ValorCompra,".",","), 2)
                response.End
            end if
        end if

    case "10acessorio"
        SeSsion("cupomdesconto")        = ""
        SeSsion("cupomdescontovalor")   = 0

        if(Month(date) <= 7 and Year(date) = 2017) then 
            sTotalCarrinho                      = cdbl(replace(ValorCompra,".",","))
            boolValidoCArrinho                  = false
            sTotalCarrinhoProdutosAcessorios    = 0

            call Abre_Conexao()

            For icar = 1 to Session("ItemCount")
                IdCategoriaCarrinho = Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYShoppingcart(C_ID, icar), "id", "Id_Categoria")
                
                if(IdCategoriaCarrinho = "196238") then 

                    sValorCarrinho          = cdbl(ARYShoppingcart(C_Preco, icar)) 
                    sDescontoValorProduto   = trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYShoppingcart(C_ID, icar), "id", "descontovalor"))

                    if (sDescontoValorProduto <> "" and not isnull(sDescontoValorProduto)) then
                        if (cdbl(sDescontoValorProduto) > 0) then
                            sValorCarrinho      = cdbl(sValorCarrinho) - cdbl(sDescontoValorProduto)
                        end if
                    end if

                    sTotalCarrinhoProdutosAcessorios    = (cdbl(sTotalCarrinhoProdutosAcessorios) + (sValorCarrinho * cdbl(ARYShoppingcart(C_Qtd, icar))) )
                    boolValidoCArrinho                  = true
                end if
            next

            call Fecha_Conexao()

            if(boolValidoCArrinho) then 
                sDesconto10acess            = (0.1 * sTotalCarrinhoProdutosAcessorios )  

                SeSsion("cupomdesconto")        = "10acessorio"
                SeSsion("cupomdescontovalor")   = sDesconto10acess

                if(sTipoPagina = "1") then        
                    response.write RetornaCasasDecimais(sDesconto10acess, 2)
                    response.End
                else
                    response.write "R$" & RetornaCasasDecimais(cdbl(sTotalCarrinho) - cdbl(sDesconto10acess), 2)
                    response.End
                end if
            else
                if(sTipoPagina = "1") then        
                    response.write "0,00"
                    response.End
                else
                    response.write "R$" & RetornaCasasDecimais(replace(ValorCompra,".",","), 2)
                    response.End
                end if
            end if
        else
            if(sTipoPagina = "1") then        
                response.write "0,00"
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(replace(ValorCompra,".",","), 2)
                response.End
            end if
        end if

    case "fieltorcedor"
        ' PRODUTO 500242
        ' DESCONTO DO FERETE ATUAL

        sTotalCarrinho                  = cdbl(replace(ValorCompra,".",","))
        SeSsion("cupomdesconto")        = ""
        SeSsion("cupomdescontovalor")   = 0

        For icar = 1 to Session("ItemCount")               
            if(ARYShoppingcart(C_iD, icar)  = "500242" OR ARYShoppingcart(C_iD, icar)  = "500550") then 
                sDesconto10acess        = "9,90"    
                boolValidoCArrinho      = true
            end if
        next

        if(boolValidoCArrinho) then 
            sDesconto10acess                = "9,90"
            SeSsion("cupomdesconto")        = "fieltorcedor"
            SeSsion("cupomdescontovalor")   = sDesconto10acess

            if(sTipoPagina = "1") then        
                response.write RetornaCasasDecimais(sDesconto10acess, 2)
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(cdbl(sTotalCarrinho) - cdbl(sDesconto10acess), 2)
                response.End
            end if
        else
            if(sTipoPagina = "1") then        
                response.write "0,00"
                response.End
            else
                response.write "R$" & RetornaCasasDecimais(replace(ValorCompra,".",","), 2)
                response.End
            end if
        end if

    case else
        if(sTipoPagina = "1") then        
            response.write "0,00"
            response.End
        else
            response.write "R$" & RetornaCasasDecimais(replace(ValorCompra,".",","), 2)
            response.End
        end if
end select 


'response.write "<script type=""text/javascript"">" & vbcrlf
'response.write "	$(""#freteajax"").asp('R$ "& FormatNumber(Cdbl(sValorFreteTotal),2) &"');" & vbcrlf
'response.write "	$(""#totalajax"").asp('R$ "& FormatNumber(Cdbl(sValorFreteTotal) + cdbl(ValorCompra),2) &"');" & vbcrlf
'response.write "	$(""#subfrete"").asp('R$ "& FormatNumber((Cdbl(sValorFreteTotal) + Cdbl(ValorCompra)+ cdbl(Session("ValorPresente"))),2) &"');" & vbcrlf
'response.write "	$(""#capital"").asp('"& sConteudo &"');" & vbcrlf
'response.write "  alert('"& sConteudo &"')" & vbcrlf
'response.write "</script>" & vbcrlf
' =======================================================================
%>

