﻿<%
dim boolDataCampanhaFrete

' =======================================================================
function Calcula_Frete(prTipo, sProvinciaUser, ValorCompra)

	dim Rs, sSQl, lCont, sCampoWhere
	dim sDe, sAte, nValorManuseio
	dim boolCategoriaILUMINA, boolBIBLIASSBB, boolBIBLIASSBBEstados
	dim nkgAdicional, BoolCalculo
	dim boolDatapromocao, boolDataPromocaoCincoProdutos, Calcula_FreteAux, Calcula_FreteFinal, lContaLinha
	dim AdValoremGris, AdValoremGrisP
	dim VerificaVirgula, sValorFixo

	if (sProvinciaUser = "" or sProvinciaUser = "," or lcase(sProvinciaUser) = "undefined" OR isnull(sProvinciaUser) ) then 
        Calcula_Frete                   = 0
        Session("sCepAgendamento")		= ""
        exit function
    end if

	Calcula_Frete 		= 0
	Calcula_FreteAux	= 0 
	nValorManuseio 		= 0
	nkgAdicional 		= 0
	AdValoremGris		= 0
	AdValoremGrisP		= 1     ' Valor antigo: 0.60 (alterado 31/10 por Edney)
	lContaLinha			= 1
    sValorFixo          = 25

	Session("sValorPeso")					= 0
	Session("sAcrescimoAgendamento")		= 0
	Session("sAcrescimoAgendamentoReal")	= 0
	Session("DiaPrazoTotal")				= 7
	Session("sCepAgendamento")				= ""
    Session("RetornaCidade")                = ""
	
	boolCategoriaILUMINA 	= false
	boolBIBLIASSBB 			= false
	boolBIBLIASSBBEstados 	= false
	sCampoWhere 			= "descricao"

    boolDataCampanhaFrete = Monta_Campanha_Frete()
	
	' SE O PARAMENTRO CEP ESTIVER VAZIO ----------------------------------------
	if(sProvinciaUser = "") then 
		exit function
	end if

    'On Error Resume Next

	' INSERIR 0,20 % GRIS e 0,40% ADV = 0,60% SOBRE O VALOR DO PEDIDO FECHADO ==
	if(cdbl(ValorCompra) > 0) then 
        AdValoremGris = ( cdbl((AdValoremGrisP / 100)) * cdbl(ValorCompra) )
    end if

    if(Err.number <> 0) then 
        ValorCompra = Session("SubTotalCompra")

	    if(cdbl(ValorCompra) > 0) then 
            AdValoremGris = ( cdbl((AdValoremGrisP / 100)) * cdbl(ValorCompra) )
        end if
    end if

    'on error goto 0


	' RECUPERA O VALOR DO PESO TOTAL DA COMPRA ---------------------------------
	if (sTotalPeso = "") then 
		sTotalPeso = 0
	else
		sTotalPeso = Cdbl(sTotalPeso)
	end if
	
	if (sTotalQuantidade = "") then 
		sTotalQuantidade = 0
	else
		sTotalQuantidade = Cdbl(sTotalQuantidade)
	end if
	
	call Abre_Conexao()
	
	if (sTipoPagina <> "DETALHES") THEN
		for lCont = 1 to Session("ItemCount")
			sTotalPeso 			= (cdbl(sTotalPeso) + (cdbl(ARYshoppingcart(C_Peso, lCont)) * cdbl(ARYshoppingcart(C_Qtd, lCont))) )
			nValorManuseio 		= (cdbl(nValorManuseio) + cdbl(ARYshoppingcart(C_Manuseio, lCont)))
			sTotalQuantidade 	= (cdbl(sTotalQuantidade) + cdbl(ARYshoppingcart(C_Qtd, lCont)))
		next
	end if

	Session("sCepAgendamento")		= sProvinciaUser
	Session("sValorPeso") 			= sTotalPeso 
        	
    IF(InStr(sProvinciaUser, ",") > 0) THEN
        sCepArray = Split(sProvinciaUser, ",")
        if(UBound(sCepArray) >= 0) then sProvinciaUser = sCepArray(0) 
    END IF

	' sSQl = "Select * from TB_TIPO_ENTREGA (nolock) where (ativo = 1 and cdcliente = "& sCdCliente &") order by ordem "
	sSQl = "SELECT TOP 1																			" & _
                "TEC.Descricao as Descricao_Cep,                                                    " & _
				"TE.*																				" & _
			"FROM 																					" & _
			"	TB_TIPO_ENTREGA TE (NOLOCK)															" & _
			"	INNER JOIN TB_TIPOENTREGA_CEP TEC (NOLOCK) ON TE.ID = TEC.ID_TIPOENTREGA			" & _
			"WHERE																					" & _
			"	TE.CDCLIENTE			= "& sCdCliente &"														" & _
			"	AND TEC.ATIVO			= 1															" & _
			"	AND TE.ATIVO			= 1															" & _
			"	AND "& sProvinciaUser &" BETWEEN CEPINI AND CEPFIM									" & _
			"	AND "& replace(replace(formatNumber(Session("sValorPeso"),1),".",""),",",".") &" BETWEEN FRETEDE AND FRETEATE	" & _
			"ORDER BY 																				" & _
			"	TEC.VALOR 											"
    
'response.write sSQl & "<br><br>"
'response.end

    set Rs = oConn.execute(sSQl)
	
	if(not rs.eof) then
		BoolCalculo 	= false
		
		do while not rs.eof
			sId 			            = cdbl(rs("id"))
			sTransportadora             = trim(rs("trasportadora"))
			sTipoEntrega	            = trim(rs("Descricao"))
            Session("RetornaCidade")    = trim(rs("Descricao_cep"))

            'response.Write "Descricao_cep: " & trim(rs("Descricao_cep"))
            'response.End

			sSQl = " Select isnull(ValorKgAd, 0) as ValorKgAd1, * from TB_TIPOENTREGA_CEP (nolock) " & _ 
					" where Id_TipoEntrega = "& sId &" and " & _ 
					" ("& sProvinciaUser &" between CepIni and CepFim) and (ativo = 1 and cdcliente = "& sCdCliente &") order by descricao"

            'response.Write sSQl
            'response.end
			set Rs1 = oConn.execute(sSQl)

			if(not rs1.eof) then
				' BoolCalculo = false
				
				do while not rs1.eof
					sIdCep		 			= rs1("id")
					sDe 					= cdbl(rs1("fretede"))
					sAte 		 			= cdbl(rs1("freteate"))
					nkgAdicional 			= cdbl(rs1("ValorKgAd1"))
					sAgendamento			= cdbl(rs1("Agendamento"))

					Session("sAcrescimoAgendamento")		= cdbl(rs1("AcrescimoAgendamento"))
					Session("sAcrescimoAgendamentoReal")	= cdbl(rs1("AcrescimoRealAgendamento"))
					Session("DiaPrazoTotal")				= cInt(rs1("DiaPrazoTotal"))

					if((Session("sValorPeso") >= sDe) and (Session("sValorPeso") <= sAte)) then
						if(nkgAdicional > 0) then
							if((Session("sValorPeso") - sAte) > 0) then 
								nkgAdicional = ((Session("sValorPeso") - sAte) * nkgAdicional)
								Calcula_Frete = (RetornaCasasDecimais(cdbl(rs1("Valor")),2) + nkgAdicional + nValorManuseio)
								Session("IdTipoEntrega") = sIdCep

                                if(sValorFixo <> "" and sValorFixo <> "0") then Calcula_Frete = sValorFixo

								exit do
							else
								Calcula_Frete = (RetornaCasasDecimais(cdbl(rs1("Valor")),2) + nValorManuseio)
								Session("IdTipoEntrega") = sIdCep

                                if(sValorFixo <> "" and sValorFixo <> "0") then Calcula_Frete = sValorFixo

								exit do
							end if
						else
							Calcula_Frete = (RetornaCasasDecimais(cdbl(rs1("Valor")),2) + nValorManuseio)
							Session("IdTipoEntrega") = sIdCep

                            if(sValorFixo <> "" and sValorFixo <> "0") then Calcula_Frete = sValorFixo

							exit do
						end if
					else
						if((Session("sValorPeso") - sAte) > 0) then 
							nkgAdicional = ((Session("sValorPeso") - sAte) * nkgAdicional)
							Calcula_Frete = (RetornaCasasDecimais(cdbl(rs1("Valor")),2) + nkgAdicional + nValorManuseio)

                            if(sValorFixo <> "" and sValorFixo <> "0") then Calcula_Frete = sValorFixo

						end if
					end if
					
				rs1.movenext		' WHILE CEP TIPO DE ENTREGA *****************
				loop
				set Rs1 = nothing
			end if
			
			if(Calcula_Frete > 0) then 
				if((cdbl(Calcula_Frete) <= cdbl(Calcula_FreteAux)) OR lContaLinha = 1 OR Calcula_FreteFinal = "") then 
					Calcula_FreteFinal 	= Calcula_Frete + AdValoremGris

                    if(sValorFixo <> "" and sValorFixo <> "0") then Calcula_Frete = sValorFixo

					BoolCalculo 		= true
				end if
				
				Calcula_FreteAux = Calcula_Frete
			end if

			lContaLinha 		= (lContaLinha + 1)
		rs.movenext					' WHILE TIPO DE ENTREGA *********************
		loop
		set Rs = nothing
	end if
	
	'if (not session("sCheckedAgendamento")) then
		Session("Transportadora") 	= sTransportadora
		Session("TipoEntrega") 		= sTipoEntrega
	'else
	'	Session("Transportadora") 	= "Direct - Agendamento"
	'end if
	
	if (sTotalPeso = 0) then
		Calcula_Frete = 0
		exit function
	end if
	
	' ---------------------------------------------------------------------------------
	if(BoolCalculo and Calcula_FreteFinal > 0) then 
		if ((boolDataCampanhaFrete) AND (cdbl(ValorCompra) >= cdbl(sPrecoPromocaoFreteGratis))) then
			 if(prTipo = "1") then
		 		Calcula_FreteFinal =  Calcula_FreteFinal - (sDescontoPromocaoFreteGratis/ 100)* Calcula_FreteFinal
			 end if
			 
			 if(prTipo = "2") then
		 		Calcula_FreteFinal =  ((sDescontoPromocaoFreteGratis/ 100) * Calcula_FreteFinal)
			 end if
		end if
	end if

	if(Calcula_FreteFinal > 0) then 
		Calcula_Frete = (Calcula_Frete + AdValoremGris + nValorManuseio)

        if(sValorFixo <> "" and sValorFixo <> "0") then Calcula_Frete = sValorFixo

		exit function
	end if

    ' COMENTADO 
	if ((boolDataCampanhaFrete) and (cdbl(ValorCompra) >= cdbl(sPrecoPromocaoFreteGratis))) then 
		boolDatapromocaoFreteGratis = true
		 if(prTipo = "1") then
	 		Calcula_Frete = 0
	 		call fecha_Conexao()
	 		exit function
		 end if
	end if

	
	' ================================================================================================
	
	' =================================================================================================
	' SE CHEGOU ATE AQUI EH PQ NAO CALCULOU O FRETE PQ NAO TINHA A FAIXA DE PESO
	' PEGAR A ULTIMA FAIXA DE PESO DO TIPO DE ENTREGA TOP 1 PRIORIDADE 01 =====
	if(not BoolCalculo AND Session("sValorPeso") > 0) then
	    
		sSQl = " Select top 1 TB_TIPO_ENTREGA.* " & _
               " from TB_TIPO_ENTREGA (nolock) " & _ 
               " INNER JOIN TB_TIPOENTREGA_CEP (NOLOCK) ON TB_TIPO_ENTREGA.ID = TB_TIPOENTREGA_CEP.Id_TipoEntrega " & _
			    " WHERE  ("& sProvinciaUser &" between TB_TIPOENTREGA_CEP.CepIni and TB_TIPOENTREGA_CEP.CepFim) and (TB_TIPO_ENTREGA.ativo = 1 and TB_TIPO_ENTREGA.cdcliente = "& sCdCliente &")  order by TB_TIPO_ENTREGA.ordem "
        ' response.write ssql
        ' response.End
		set Rs = oConn.execute(sSQl)
		
		if(not rs.eof) then
			do while not rs.eof
				sId = cdbl(rs("id"))
				
				sSQl = " Select top 1 * from TB_TIPOENTREGA_CEP (nolock) " & _ 
						" where Id_TipoEntrega = "& sId &" and " & _ 
						" ("& sProvinciaUser &" between CepIni and CepFim) and (ativo = 1 and cdcliente = "& sCdCliente &") order by freteate desc "
			    ' response.write ssql
                ' response.End
            	set Rs1 = oConn.execute(sSQl)
		
				if(not rs1.eof) then
					BoolCalculo = false
					
					do while not rs1.eof
						sDe 		 = cdbl(rs1("fretede"))
						sAte 		 = cdbl(rs1("freteate"))

                        if(trim(rs1("ValorKgAd")) <> "" and not isnull(rs1("ValorKgAd"))) then 
                            nkgAdicional = cdbl(rs1("ValorKgAd"))						
                        else
                            nkgAdicional = 0  						
                        end if
						
						sAgendamento			                = cdbl(rs1("Agendamento"))
						Session("sAcrescimoAgendamento")		= cdbl(rs1("AcrescimoAgendamento"))
						Session("sAcrescimoAgendamentoReal")	= cdbl(rs1("AcrescimoRealAgendamento"))
						Session("DiaPrazoTotal")				= cInt(rs1("DiaPrazoTotal"))

						'if((Session("sValorPeso") >= sDe) and (Session("sValorPeso") <= sAte)) then
							if(nkgAdicional > 0) then
								if((Session("sValorPeso") - sAte) > 0) then 
									nkgAdicional = ((Session("sValorPeso") - sAte) * nkgAdicional)
								end if
							end if
							
							Calcula_Frete 	= (RetornaCasasDecimais(cdbl(rs1("Valor")),2) + nkgAdicional + nValorManuseio + AdValoremGris)
							BoolCalculo 	= true
							exit function
						'end if
					rs1.movenext		' WHILE CEP TIPO DE ENTREGA *****************
					loop
				end if
				
			rs.movenext					' WHILE TIPO DE ENTREGA *********************
			loop

			' SISTEMA NAO ACHOU CEP - INSERIR
			IF(Calcula_Frete = 0 OR Calcula_Frete = "") THEN 
                Calcula_Frete = (8 * sTotalQuantidade)
            END IF
			set Rs = nothing
			
		end if
		
	end if
	call fecha_Conexao()
end function



' =======================================================================
function Monta_Campanha_Frete()
	dim sSQl, Rs, sConteudo
	dim sid_provincia, slocalidade, sDescricao, sDataIni, sDataFim, sFaixaCepIni, sFaixaCepFim, sCidade, sPedido_Total, sData, sId_Produto, sDesconto, sidFrete
	dim sCep, RsCep, sSqlCep, sSqlCidade, RsCidade
	
	call Abre_Conexao()
	
	Monta_Campanha_Frete = false
	
    sSql = "Select * from TB_FRETE_GRATIS (NOLOCK)" & _
			" where " &_
					" Ativo	= 1	AND " &_
					" getdate() between DataIni and DataFim AND " &_
					" CdCliente =" & sCdCliente
	set Rs = oConn.execute(sSql)
	
	if(not Rs.eof) then
		do while not Rs.eof
			sidFrete						= trim(Rs("id"))
			sIdCampanhaAtiva				= sidFrete
			sid_provincia					= trim(Rs("id_provincia"))
			sCidade							= trim(Rs("Cidade"))
			slocalidade						= trim(Rs("localidade")) ' nome1, nome2
			sDescricao						= trim(Rs("Descricao"))
			sDataIni						= trim(Rs("DataIni"))
			sDataFim						= trim(Rs("DataFim"))
			sFaixaCepIni					= trim(Rs("FaixaCepIni"))
			sFaixaCepFim					= trim(Rs("FaixaCepFim"))
			sPedido_Total					= trim(Rs("Pedido_Total")) 	' PRECO DO PRODUTO
			sDesconto						= trim(Rs("Desconto"))
			Session("CampanhaFreteGratis") 	= trim(Rs("Descricao"))
			sImgSeloFreteGratis				= trim(Rs("Imagem"))
			sImgSeloFreteGratisG			= trim(Rs("Imagem2"))
			
			sPrecoPromocaoFreteGratis 	= cdbl(sPedido_Total) ' VARIAVEL USADA PARA MOSTRAR O SELO DENTRO DOS PRODUTOS
			boolDatapromocaoFreteGratis = true 
			if(sDesconto > 0) then sDescontoPromocaoFreteGratis = sDesconto

			if(Session("Id") <> "") then
				if (sCidade <> "" and not isnull(sCidade)) then
					sSqlCidade = "Select * from TB_FRETE_GRATIS (nolock) where Cdcliente = "& sCdCliente &" and cidade collate Latin1_General_CI_AI  like '%"& trim(Recupera_Campos_Db_oConn("TB_CLIENTES", Session("Id"), "id", "Cidade")) &"%' and id =" & sidFrete
					set RsCidade = oConn.execute(sSqlCidade)
					if (not RsCidade.eof) then
						boolDatapromocaoFreteGratis = true
						Monta_Campanha_Frete 		= true
					end if
					set RsCidade = nothing
				else

					if((trim(request("entrega")) <> "") and (replace(trim(request("cep")),"'","") <> "") or (Replace_Caracteres_Especiais(trim(request("cepd"))) <> "")  ) then 
						if ((Replace_Caracteres_Especiais(trim(request("cepd"))) <> "")) then
							sProvinciaCad = replace(trim(request("cepd")),"'","")
						else
							sProvinciaCad = replace(trim(request("cep")),"'","")
						end if
					else
						sProvinciaCad = Recupera_Campos_Db_oConn("TB_CLIENTES", Session("Id"), "id", "cep")
					end if

					Session("ProvinciaCep") = sProvinciaCad
					
	                if (sProvinciaCad = "" or lcase(sProvinciaCad) = "undefined" OR isnull(sProvinciaCad) ) then 
                        exit function
                    else
                        IF(InStr(Session("ProvinciaCep"), ",") > 0) THEN
                            sCepArray = Split(Session("ProvinciaCep"), ",")
                            if(UBound(sCepArray) >= 0) then Session("ProvinciaCep") = sCepArray(0) 
                        END IF
                    end if
                    
					sSqlCep = "SELECT " & _
								" TEC.* " & _
							" FROM  " & _
								" TB_TIPO_ENTREGA TE 				(NOLOCK) " & _
								" INNER JOIN TB_TIPOENTREGA_CEP TEC (NOLOCK) ON TE.ID = TEC.ID_TIPOENTREGA " & _
							" WHERE " & _
								" TE.CDCLIENTE	= "& sCdCliente & _
								" /*AND TE.ATIVO= 1*/ " & _
								" AND TEC.ATIVO	= 1 " & _
								" AND "& Session("ProvinciaCep") &" BETWEEN CEPINI AND CEPFIM "
					set RsCep = oConn.execute(sSqlCep)
					
					if(not RsCep.eof) then
						if (Instr(Ucase(slocalidade), Ucase(RsCep("Localidade"))) > 0) then
							boolDatapromocaoFreteGratis = true
							Monta_Campanha_Frete 		= true
							exit function
						else
							if (sid_provincia = Session("Id_provincia")) then
								boolDatapromocaoFreteGratis = true
								Monta_Campanha_Frete 		= true
								exit function
							else
								if ((sCidade = "" or isnull(sCidade)) and (slocalidade = "" or isnull(slocalidade) ) and (sid_provincia = "0" or isnull(sid_provincia) or sid_provincia = "")) then
									boolDatapromocaoFreteGratis = true
									Monta_Campanha_Frete 		= true
									exit function
								end if
							
							'	boolDatapromocaoFreteGratis = false
							'	Monta_Campanha_Frete 		= false
							'	exit function
							end if
						end if 
					else
						if ((sCidade = "" or isnull(sCidade)) and (slocalidade = "" or isnull(slocalidade) ) and (sid_provincia = "0" or isnull(sid_provincia) or sid_provincia = "")) then
							Monta_Campanha_Frete 		= true
						end if
					end if
					
'					Monta_Campanha_Frete 		= true
'					response.write Monta_Campanha_Frete
				end if
			else

				if(Session("Id") = "" AND ( (Replace_Caracteres_Especiais(trim(request("cepd"))) <> "") OR (Replace_Caracteres_Especiais(trim(request("cepAG"))) <> "")) AND (InStr(trim(request("cepd")), ",") = 0)  ) then

'response.write Replace_Caracteres_Especiais(trim(request("cepd")))
'response.End
					sCep = Replace_Caracteres_Especiais(trim(request("cepd")))

					IF(sCep = "") THEN 
						sCep = Replace_Caracteres_Especiais(trim(request("cepAG")))
                    ELSE
                        IF(InStr(sCep, ",") > 0) THEN
                            sCepArray = Split(sCep, ",")

                            if(UBound(sCepArray) >= 0) then sCep = sCepArray(0) 
                        END IF
					END IF
                   
					sSqlCep = "SELECT " & _
								" TEC.* " & _
							" FROM  " & _
								" TB_TIPO_ENTREGA TE(NOLOCK) " & _
								" INNER JOIN TB_TIPOENTREGA_CEP TEC (NOLOCK) ON TE.ID = TEC.ID_TIPOENTREGA " & _
							" WHERE " & _
								" TE.CDCLIENTE	= "& sCdCliente &" " & _
								" AND TEC.ATIVO	= 1 " & _
								" AND "& sCep &" BETWEEN CEPINI AND CEPFIM "
    'response.write sCep
    'response.End
					set RsCep = oConn.execute(sSqlCep)

					if(not RsCep.eof) then

						if (Ucase(sCidade) = Ucase(RsCep("DESCRICAO"))) then
								boolDatapromocaoFreteGratis = true
								Monta_Campanha_Frete 		= true
								exit function
						else
							if (Instr(Ucase(slocalidade),Ucase(RsCep("localidade"))) > 0) then
								boolDatapromocaoFreteGratis = true
								Monta_Campanha_Frete 		= true
								exit function
							else

'response.write trim(Recupera_Campos_Db_oConn("TB_ESTADOS", trim(sid_provincia), "id", "SIGLA")) & "<Br>"
'response.write Ucase(trim(RsCep("SiglaEst")))
'response.End

								if (trim(Recupera_Campos_Db_oConn("TB_ESTADOS", trim(sid_provincia), "id", "SIGLA")) =   Ucase(trim(RsCep("SiglaEst")))) then
									boolDatapromocaoFreteGratis = true
									Monta_Campanha_Frete 		= true
									exit function
								else
									if ((sCidade = "" or isnull(sCidade)) and (slocalidade = "" or isnull(slocalidade) ) and (sid_provincia = "0" or isnull(sid_provincia) or sid_provincia = "")) then
										boolDatapromocaoFreteGratis = true
										Monta_Campanha_Frete 		= true
										exit function
									end if
								end if
							end if
						end if
					else
						if ((sCidade = "" or isnull(sCidade)) and (slocalidade = "" or isnull(slocalidade) ) and (sid_provincia = "0" or isnull(sid_provincia) or sid_provincia = "")) then
							Monta_Campanha_Frete 		= true
						end if
					end if
					set RsCep = nothing
				else
					'if(Replace_Caracteres_Especiais(trim(request("cepd"))) <> "") then 
						if ((sCidade = "" or isnull(sCidade)) and (sid_provincia = "0" or isnull(sid_provincia) or sid_provincia = "")) then
							Monta_Campanha_Frete 		= true
						end if
					'else
						'Monta_Campanha_Frete 		= false
					'end if
					
				end if
			end if
		rs.movenext
		loop
	else
		Monta_Campanha_Frete 		= false
		boolDatapromocaoFreteGratis	= false
		sDescontoPromocaoFreteGratis = 0
	end if

	set Rs = nothing
	call Fecha_Conexao()
end function

%>