<%
'-----------------------------------------------------
'Funcao: IsCNPJ(ByVal intNumero)
'Sinopse: Verifica se o valor passado � um CNPJ v�lido
'          Formatos aceitos: XX.XXX.XXX/XXXX-XX ou
'                            XXXXXXXXXXXXXX
'Parametro: intNumero
'Retorno: Booleano
'Autor: Gabriel Fr�es - www.codigofonte.com.br
'-----------------------------------------------------
Function IsCNPJ(ByVal intNumero)
	' Fun��o COPIADA do site CodigoFonte.uol.com.br  Marcelo Faria
    'Validando o formato do CNPJ com express�o regular
    Set regEx = New RegExp                                'Cria o Objeto Express�o
    regEx.Pattern = "\d{2}.?\d{3}.?\d{3}/?\d{4}-?\d{2}"    ' Express�o Regular
    regEx.IgnoreCase = True                                ' Sensitivo ou n�o
    regEx.Global = True
    Retorno = RegEx.Test(intNumero)
    Set regEx = Nothing
    
    'Caso seja verdadeiro posso validar se o CNPJ � v�lido
    If Retorno = True Then
        'Validando a sequencia n�meros
        Dim CNPJ_temp
        CNPJ_temp            = intNumero
        CNPJ_temp            = Replace(CNPJ_temp, ".", "")
        CNPJ_temp            = Replace(CNPJ_temp, "/", "")
        CNPJ_temp            = Replace(CNPJ_temp, "-", "")
        CNPJ_Digito_temp    = Right(CNPJ_temp, 2)
        
        'Somando os 12 primeiros digitos do CNPJ 
        Soma    = (Clng(Mid(CNPJ_temp,1,1)) * 5) + (Clng(Mid(CNPJ_temp,2,1)) * 4) + (Clng(Mid(CNPJ_temp,3,1)) * 3) + (Clng(Mid(CNPJ_temp,4,1)) * 2) + (Clng(Mid(CNPJ_temp,5,1)) * 9) + (Clng(Mid(CNPJ_temp,6,1)) * 8)+ (Clng(Mid(CNPJ_temp,7,1)) * 7) + (Clng(Mid(CNPJ_temp,8,1)) * 6) + (Clng(Mid(CNPJ_temp,9,1)) * 5) + (Clng(Mid(CNPJ_temp,10,1)) * 4) + (Clng(Mid(CNPJ_temp,11,1)) * 3) + (Clng(Mid(CNPJ_temp,12,1)) * 2)
        '----------------------------------
        'Calculando o 1� d�gito verificador
        '----------------------------------
        'Pegando o resto da divis�o por 11
        Resto    = (Soma Mod 11)
        If Resto < 2 Then
            DigitoHum = 0
        Else
            DigitoHum = Cstr(11-Resto)
        End If
        '----------------------------------
        '----------------------------------
        'Calculando o 2� d�gito verificador
        '----------------------------------
        'Somando os 12 primeiros digitos do CNPJ mais o 1� d�gito
        Soma    = (Clng(Mid(CNPJ_temp,1,1)) * 6) + (Clng(Mid(CNPJ_temp,2,1)) * 5) + (Clng(Mid(CNPJ_temp,3,1)) * 4) + (Clng(Mid(CNPJ_temp,4,1)) * 3) + (Clng(Mid(CNPJ_temp,5,1)) * 2) + (Clng(Mid(CNPJ_temp,6,1)) * 9) + (Clng(Mid(CNPJ_temp,7,1)) * 8) + (Clng(Mid(CNPJ_temp,8,1)) * 7) + (Clng(Mid(CNPJ_temp,9,1)) * 6) + (Clng(Mid(CNPJ_temp,10,1)) * 5) + (Clng(Mid(CNPJ_temp,11,1)) * 4) + (Clng(Mid(CNPJ_temp,12,1)) * 3) + (DigitoHum * 2)
        'Pegando o resto da divis�o por 11
        Resto    = (Soma Mod 11)
        
        If Resto < 2 Then
            DigitoDois = 0
        Else
            DigitoDois = Cstr(11-Resto)
        End If
        '----------------------------------
        'Verificando se os digitos s�o iguais aos dig�tados.
        DigitoCNPJ = Cstr(DigitoHum) & Cstr(DigitoDois)
        If Cstr(CNPJ_Digito_temp) = Cstr(DigitoCNPJ) Then
            Retorno = True
        Else
            Retorno = False
        End If
    End If
    IsCNPJ = Retorno
	IsCNPJ = true	' Valida��o desativada at� encontrar nova valida��o que funcione com a nova regra de CNPJ
End Function
%>

