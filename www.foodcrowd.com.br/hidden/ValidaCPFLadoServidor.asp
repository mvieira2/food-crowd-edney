<%
Function IsCPF(ByVal intNumero)
	' Fun��o COPIADA do site CodigoFonte.uol.com.br  Marcelo Faria
    'Validando o formato do CPF com express�o regular
    Set regEx = New RegExp                           			' Cria o Objeto Express�o
    regEx.Pattern = "^(\d{3}\.\d{3}\.\d{3}-\d{2})|(\d{11})$"    ' Express�o Regular
    regEx.IgnoreCase = True                           			' Sensitivo ou n�o
    regEx.Global = True
    Retorno = RegEx.Test(intNumero)
    Set regEx = Nothing

    'Caso seja verdadeiro posso validar se o CPF � v�lido
    If Retorno = True Then
        'Validando a sequencia n�meros
        Dim CPF_temp
        CPF_temp            = intNumero
        CPF_temp            = Replace(CPF_temp, ".", "")
        CPF_temp            = Replace(CPF_temp, "-", "")
        CPF_Digito_temp     = Right(CPF_temp, 2)
        
        'Somando os nove primeiros digitos do CPF
        Soma    = (Clng(Mid(CPF_temp,1,1)) * 10) + (Clng(Mid(CPF_temp,2,1)) * 9) + (Clng(Mid(CPF_temp,3,1)) * 8) + (Clng(Mid(CPF_temp,4,1)) * 7) + (Clng(Mid(CPF_temp,5,1)) * 6) + (Clng(Mid(CPF_temp,6,1)) * 5) + (Clng(Mid(CPF_temp,7,1)) * 4) + (Clng(Mid(CPF_temp,8,1)) * 3) + (Clng(Mid(CPF_temp,9,1)) * 2)
        '----------------------------------
        'Calculando o 1� d�gito verificador
        '----------------------------------
        'Pegando o resto da divis�o por 11
        Resto    = (Soma Mod 11)
        
        If Resto = 1 Or Resto = 0 Then
            DigitoHum = 0
        Else
            DigitoHum = Cstr(11-Resto)
        End If
        '----------------------------------
        '----------------------------------
        'Calculando o 2� d�gito verificador
        '----------------------------------
        'Somando os 9 primeiros digitos do CPF mais o 1� d�gito
        Soma    = (Clng(Mid(CPF_temp,1,1)) * 11) + (Clng(Mid(CPF_temp,2,1)) * 10) + (Clng(Mid(CPF_temp,3,1)) * 9) + (Clng(Mid(CPF_temp,4,1)) * 8) + (Clng(Mid(CPF_temp,5,1)) * 7) + (Clng(Mid(CPF_temp,6,1)) * 6) + (Clng(Mid(CPF_temp,7,1)) * 5) + (Clng(Mid(CPF_temp,8,1)) * 4) + (Clng(Mid(CPF_temp,9,1)) * 3) + (DigitoHum * 2)
        'Pegando o resto da divis�o por 11
        Resto    = (Soma Mod 11)
        
        If Resto = 1 Or Resto = 0 Then
            DigitoDois = 0
        Else
            DigitoDois = Cstr(11-Resto)
        End If
        '----------------------------------
        'Verificando se os digitos s�o iguais aos dig�tados.
        DigitoCPF = Cstr(DigitoHum) & Cstr(DigitoDois)
        If Cstr(CPF_Digito_temp) = Cstr(DigitoCPF) Then
            Retorno = True
        Else
            Retorno = False
        End If
    End If
    IsCPF = Retorno
End Function

Sub Validacao_Lado_Servidor(Dado,Tipo)
	Dim Valida
	Valida	=	True
	Select case Tipo
		case "CPF"
			if not IsCPF (Dado) then
				Msg = "Cpf inv�lido\n"
				Valida	=	False
			end if
		case "CNPJ"
			if not IsCNPJ (Dado) then
				Msg = "Cnpj inv�lido\n"
				Valida	=	False
			end if
		case "Vazio"
			if replace(trim(Dado)," ","") = "" then
				Msg = "Verifique os campos obrigat�rios\n"
				Valida	=	False
			end if
		' Incluir outros Case para outras valida��es......
	End Select
	if (Not Valida) then
		Response.write "<script>alert('"&Msg&"');history.back();</script>"
		Response.end
	end if
End Sub


%>