<!--#include file="constantes.asp"-->
<!--#include file="conexao.asp"-->
<!--#include file="trata_palavra.asp"-->
<!--#include file="calculo_frete.asp"-->
<!--#include file="hacksafe.asp"-->
<!--#include file="ValidaCPFLadoServidor.asp"-->
<!--#include file="ValidaCNPJLadoServidor.asp"-->
<!--#include file="ValidaCCLadoServidor.asp"-->
<!--#include file="encripta.asp"-->
<!--#include file="moip_dados.asp"-->

<%
' VARIAVEIS GLOBAIS! =======================================================
Dim sMsgJs, iCount, sNumerosPaginasPadrao, sNumeroPaginacao
Dim ARYshoppingcart, sPathHttps, sLinkhttp, sCupomGet, sBannerChave, sFreteGratis, boolDatapromocaoFreteGratis, sDescontoPromocaoFreteGratis, sIdCampanhaAtiva
dim sPrecoPromocaoFreteGratis, sImgSeloFreteGratis, sImgSeloFreteGratisG, sMidiaGA, sProdutosCupomFuncoes, sNotIdSugestao, nDescontoBoleto, nDescontoBoletoLeng, sTitleProduto
Dim sAgendamento, CidadesAtendidas

sNumerosPaginasPadrao 	= 21
sNumeroPaginacao 	    = 5

DescontoBoletoBD        = trim(Recupera_Campos_Db("TBCLIENTE", sCdCliente, "CDCLIENTE", "DescontoBoleto"))
if(DescontoBoletoBD <> "" and not isnull(DescontoBoletoBD)) then 
    nDescontoBoleto         = (cdbl(DescontoBoletoBD)  / 100)
    nDescontoBoletoLeng     = DescontoBoletoBD
end if

iCount 			        = Session("ItemCount")
ARYshoppingcart         = Session("MyShoppingCart")

sPathHttps              = "https://www.foodcrowd.com.br/"
sPathHttp  	            = "https://www.foodcrowd.com.br/"
sLinkhttp 	            = "https://www.foodcrowd.com.br/"

'sPathHttps             = "http://foodcrowd.localhost/"
'sPathHttp              = "http://foodcrowd.localhost/"
'sLinkhttp 	            = "http://foodcrowd.localhost/"

If (Request.ServerVariables("HTTPS") = "off") Then
    'response.redirect(sPathHttps)
end if

if(Request.ServerVariables("https") = "off") then 
	sLinkImagem = "https://imagensproduto.foodcrowd.com.br/"
else
	sLinkImagem = "https://imagensproduto.foodcrowd.com.br/"
end if

call HackSafe()

sCupomGet	 	= Replace_Caracteres_Especiais(replace(trim(request("cupom")),"'",""))
sMidiaGA 		= Replace_Caracteres_Especiais(replace(trim(request("mi")),"'",""))
sBannerChave 	= Replace_Caracteres_Especiais(replace(trim(request("bannerid")),"'",""))
sBannerChaveAux = Replace_Caracteres_Especiais(replace(trim(request("&bannerid")),"'",""))
sFreteGratis 	= Replace_Caracteres_Especiais(replace(trim(request("fg")),"'",""))
CidadesAtendidas= "São Paulo,Sao Paulo,Barueri,Santana de parnaiba,Santana de Parnaíba,Osasco,Taboao da Serra,Taboão da Serra,Sao Bernardo,São Bernardo,Sao Caetano,Diadema,Santo Andre,Santo André,Cotia,Carapicuiba,Itapevi,Jandira,Guarulhos,Cajamar"


if(sFreteGratis <> "") 		then Session("fretegratis") = sFreteGratis

if(sBannerChave <> "") 		then 
	Session("bannerid") 	= sBannerChave
else
	if(sBannerChaveAux <> "") 		then 
		Session("bannerid") 	= sBannerChaveAux
	end if
end if
if (sMidiaGA <> "")  then
	Session("MidiaGA") 	=  sMidiaGA
end if

boolDataCampanhaFrete = Monta_Campanha_Frete()

' OPERACAO DE INVERNO *********************
'if (Session("fretegratis") = 1 and ((month(date) = 12 and year(date) = 2009) and (day(date) >= 18 and day(date) <= 31) or (month(date) = 2 and year(date) = 2010) and (day(date) >= 1 and day(date) <= 5)) ) then
if (Session("fretegratis") = 1 and ((month(date) = 1 and year(date) = 2010) and (day(date) >= 1 and day(date) <= 31) or (month(date) = 2 and year(date) = 2010) and (day(date) >= 1 and day(date) <= 5)) ) then
	boolDatapromocaoFreteGratis 	= true
	boolDataCampanhaFrete			= true
	sDescontoPromocaoFreteGratis 	= 100
	sPrecoPromocaoFreteGratis		=  0 ' PARA NAO MOSTRAR O SELO QDO A CAMPANHA NAO ESTIVER ATIVA
end if
' OPERACAO DE INVERNO *********************

if(sCupomGet <> "") then 
	call Valida_Desconto_Cupom(sCupomGet)
else
	if(InSTr(lcase(Request.ServerVariables("script_name")), "cesta.asp") > 0 OR _ 
		InSTr(lcase(Request.ServerVariables("script_name")), "pedido.asp") > 0 ) then 
		if(Replace_Caracteres_Especiais(replace(trim(request("presente")),"'","")) <>"") then 
		'	call Valida_Desconto_Cupom(Replace_Caracteres_Especiais(replace(trim(request("presente")),"'","")))
		end if
	end if
end if


' =========================================================================
function bVerificaCupom(prCupom, prTotal)

    bVerificaCupom = false
    
    if(prCupom = "") then exit function

    call Abre_Conexao()

    dim sSQl, Rs

	sSQl = "Select count(*) as TotalPedidos from TB_PEDIDOS (nolock) where cdcliente = "& sCdCliente &" and CupomDesconto = '"& prCupom &"' and id_status in (1, 2, 4, 6, 56)"
	Set Rs = oConn.execute(sSQl)

	if(not Rs.eof) then 
        if(cdbl(trim(rs("TotalPedidos"))) <= cint(prTotal) ) then
            bVerificaCupom = true
	    end if
	end if
	Set Rs = nothing

end function



' =========================================================================
function bVerificaLaudo(prProduto)
    bVerificaLaudo = false
    if(prProduto = "") then exit function

    call Abre_Conexao()

    dim sSqllaudo, rssSqllaudo

    sSqllaudo = "EXEC Sp_ListarRespostasAPP " & prProduto
    set rssSqllaudo = oConn.execute(sSqllaudo)
	if(not rssSqllaudo.eof) then bVerificaLaudo = true
	set rssSqllaudo = nothing     
end function



' =========================================================================
function Montar_Links_Consulta(PrTipo, prCategora, prPrFabricante, prprIdCat, prIdSubCat, prPrTIpo)
	dim sSQlop, sLinkMonta

    if(prCategora <> "") then 
        if(Right(trim(prCategora), 1) = ",") then prCategora = left(trim(prCategora), len(trim(prCategora))-1)
    'else
        'exit Function	    
    end if

	call Abre_Conexao()

    Select case PrTipo
        case "0", "7"    ' Categorias

	        sSQlop = "SELECT ID, CATEGORIA AS DESCRICAO, 'descricaoSEO' =  CASE WHEN isnull(Categoria_Jp, '') <> '' THEN Categoria_Jp ELSE Categoria END, capa FROM TB_CATEGORIAS (NOLOCK) WHERE CDCLIENTE in (" & sCdCliente & ") and ativo = 1 AND EXISTS(SELECT TOP 1 P.ID FROM TB_PRODUTOS P (NOLOCK) WHERE P.ID_CATEGORIA = TB_CATEGORIAS.ID AND P.ATIVO = 1) " 
            if(prCategora <> "") then sSQlop = sSQlop & "AND ID IN ("& prCategora &")" 
            sSQlop = sSQlop & "Order by CATEGORIA "

            sLinkMonta  = "categoria"

        case "4", "6"    ' Categorias
	        sSQlop = "SELECT ID, CATEGORIA AS DESCRICAO, 'descricaoSEO' =  CASE WHEN isnull(Categoria_Jp, '') <> '' THEN Categoria_Jp ELSE Categoria END, capa FROM TB_CATEGORIAS (NOLOCK) WHERE CDCLIENTE in (" & sCdCliente & ") and ativo = 1 AND EXISTS(SELECT TOP 1 P.ID FROM TB_PRODUTOS P (NOLOCK) WHERE P.ID_CATEGORIA = TB_CATEGORIAS.ID AND P.ATIVO = 1) " 
            if(prCategora <> "") then sSQlop = sSQlop & "AND ID IN ("& prCategora &")" 
            sSQlop = sSQlop & "Order by CATEGORIA "

            sLinkMonta  = "categoria"

        case "5"    ' Categorias
	        sSQlop = "SELECT ID, CATEGORIA AS DESCRICAO, 'descricaoSEO' =  CASE WHEN isnull(Categoria_Jp, '') <> '' THEN Categoria_Jp ELSE Categoria END, capa FROM TB_CATEGORIAS (NOLOCK) WHERE CDCLIENTE in (" & sCdCliente & ") and isnull(Capa, '') <> ''  and ativo = 1 " 
            if(prCategora <> "") then sSQlop = sSQlop & "AND ID IN ("& prCategora &")" 
            sSQlop = sSQlop & "Order by CATEGORIA "

            ' AND EXISTS(SELECT TOP 1 P.ID FROM TB_PRODUTOS P (NOLOCK) WHERE P.ID_CATEGORIA = TB_CATEGORIAS.ID AND P.ATIVO = 1 AND lower(P.CampoAux8) <> 'vendido' ) 

            sLinkMonta  = "categoria"

        case "1"    ' Subcategorias
	        sSQlop      = "SELECT C.CATEGORIA, S.ID, S.SUBCATEGORIA AS DESCRICAO FROM TB_CATEGORIAS C (NOLOCK) INNER JOIN TB_SUBCATEGORIA S (NOLOCK) ON C.ID = S.ID_CATEGORIA WHERE C.CDCLIENTE in (" & sCdCliente & ") AND S.ID IN ("& prCategora &") ORDER BY C.CATEGORIA, S.SUBCATEGORIA "
            sLinkMonta  = "subcategorias"

        case "2"    ' Marca
	        sSQlop = "SELECT distinct ID, FABRICANTE as descricao FROM TB_FABRICANTE (NOLOCK) WHERE CDCLIENTE in (" & sCdCliente & ") AND ID IN ("& prCategora &")  Order by fabricante "
            sLinkMonta  = "marca"

        case "3"    ' Modelo
            sLinkMonta  = "modelo"
	        sSQlop = "SELECT distinct ID, DESCRICAO FROM TB_TIPOS (NOLOCK) WHERE CDCLIENTE in (" & sCdCliente & ") AND ID IN ("& prCategora &")  Order by descricao "
    end select
    ' RESPONSE.WRITE sSQlop & "<br>"

	set RsOption = oConn.execute(sSQlop)
	
	if(Not RsOption.Eof) then
        lContLi = 1

		do while Not RsOption.Eof
            capa = trim(RsOption("capa"))
    
            Select case PrTipo
                case "0"    ' Categorias
                    Montar_Links_Consulta = Montar_Links_Consulta & "<li class=""mt-root"">" & vbcrlf
                    Montar_Links_Consulta = Montar_Links_Consulta & "   <div class=""mt-root-item""><a href=""/categoria/"& TrataSEO(trim(RsOption("descricaoSEO"))) &"/"& trim(RsOption("id")) &""">" & vbcrlf
                    Montar_Links_Consulta = Montar_Links_Consulta & "   <div class=""title title_font""><span class=""title-text"">"& FormataNome(trim(RsOption("descricao"))) &"</span></div></a></div>" & vbcrlf
                    Montar_Links_Consulta = Montar_Links_Consulta & "</li>" & vbcrlf

                case "1"    ' Subcategorias
                    if(lContLi > 20) then 
                        sDisplay = "none"
                    else
                        sDisplay = "block"
                    end if

    			    Montar_Links_Consulta = Montar_Links_Consulta & "<li id=""itemsub-"& lContLi &""" style=""display:"& sDisplay &"""><a href=""/subcategoria/"& TrataSEO(trim(RsOption("descricao"))) &"/"& prIdSubCat &"""><label for=""c1"" style=""cursor: auto;"">"& trim(RsOption("descricao")) &" ["& trim(RsOption("CATEGORIA")) &"]</label></a></li>" 

                case "2"    ' Marca
	                Montar_Links_Consulta = Montar_Links_Consulta & "<li ><a href=""/categorias.asp?IdFabricante="& prPrFabricante &"""><label for=""c1"" style=""cursor: auto;"">"& trim(RsOption("descricao")) &"</label></a></li>" 
                
                case "3"    ' Modelo
                    if(lContLi > 10) then 
                        sDisplay = "none"
                    else
                        sDisplay = "block"
                    end if

                    ' Montar_Links_Consulta = Montar_Links_Consulta & "<li id=""itemmod-"& lContLi &""" style=""display:"& sDisplay &"""><a href=""/categorias.asp?IdTipo="& trim(RsOption("id")) &"&idcat="& prprIdCat &"&idsubcat="& prIdSubCat &"&IdFabricante="& prPrFabricante &"""><label for=""c1"" style=""cursor: auto;"">"& trim(RsOption("descricao")) &"</label></a></li>" 
                    Montar_Links_Consulta = Montar_Links_Consulta & "<li id=""itemmod-"& lContLi &""" style=""display:"& sDisplay &"""><a href=""/subcategoria/"& TrataSEO(trim(RsOption("descricao"))) &"/"& prIdSubCat &"""><label for=""c1"" style=""cursor: auto;"">"& trim(RsOption("descricao")) &"</label></a></li>" 

                case "4", "6"    ' Categorias

                    Montar_Links_Consulta = Montar_Links_Consulta & "<li><a href=""/categoria/"& TrataSEO(trim(RsOption("descricaoSEO"))) &"/"& trim(RsOption("id")) &""">"& FormataNome(trim(RsOption("descricao"))) &"</a>"

                    if(PrTipo = "4") then 
                        Montar_Links_Consulta = Montar_Links_Consulta & "  <div class=""wrap-popup"">"
                        Montar_Links_Consulta = Montar_Links_Consulta & "    <div class=""popup"">"
                        Montar_Links_Consulta = Montar_Links_Consulta & "      <div class=""row"">"
                        Montar_Links_Consulta = Montar_Links_Consulta & "        <div class=""col-md-4 col-sm-6"">"
                    end if

                    Montar_Links_Consulta = Montar_Links_Consulta & "          <ul class=""nav"">"

                    Montar_Links_Consulta = Montar_Links_Consulta & Monta_Vitrine_Secao(trim(RsOption("id")))

                    Montar_Links_Consulta = Montar_Links_Consulta & "          </ul>"

                    if(PrTipo = "4") then 
                        Montar_Links_Consulta = Montar_Links_Consulta & "        </div>"
                    end if

                    if(PrTipo = "4") then 
                        if(capa <> "" and not isnull(capa)) then 
                            Montar_Links_Consulta = Montar_Links_Consulta & "        <div class=""col-md-4 has-sep hidden-sm"">"
                            Montar_Links_Consulta = Montar_Links_Consulta & "          <div class=""custom-menu-right"">"
                            Montar_Links_Consulta = Montar_Links_Consulta & "            <div class=""box-banner menu-banner"">"
                            Montar_Links_Consulta = Montar_Links_Consulta & "              <div class=""add-right""><a href=""#""><img src="""& sLinkImagem &"/EcommerceNew/upload/categoria/"& capa &""" alt=""responsive""></a></div>"
                            Montar_Links_Consulta = Montar_Links_Consulta & "            </div>"
                            Montar_Links_Consulta = Montar_Links_Consulta & "          </div>"
                            Montar_Links_Consulta = Montar_Links_Consulta & "        </div>"
                        end if

                        Montar_Links_Consulta = Montar_Links_Consulta & "      </div>"
                        Montar_Links_Consulta = Montar_Links_Consulta & "    </div>"
                        Montar_Links_Consulta = Montar_Links_Consulta & "  </div>"
                    end if

                    Montar_Links_Consulta = Montar_Links_Consulta & "</li>"


                case "5"    ' Categorias

                    Montar_Links_Consulta = Montar_Links_Consulta & "<div class=""boxes-categorias-home"">"
	                    Montar_Links_Consulta = Montar_Links_Consulta & "<a href=""/categorias.asp?idcat="& trim(RsOption("id")) &""" class=""boxes-categorias-home"">"
		                    Montar_Links_Consulta = Montar_Links_Consulta & "<img src="""& sLinkImagem &"/EcommerceNew/upload/categoria/"& capa &""" alt="""">"
		                    Montar_Links_Consulta = Montar_Links_Consulta & "<p class=""padding-top-10"">"& FormataNome(trim(RsOption("descricao"))) &"</p>"
	                    Montar_Links_Consulta = Montar_Links_Consulta & "</a>"
                    Montar_Links_Consulta = Montar_Links_Consulta & "</div>"

                case "7"    ' Categorias - HOME

                    sSubMenu = Monta_Vitrine_Secao(trim(RsOption("id")))

                    if(sSubMenu <> "") then 
                        sClassSub = " class=""dropdown-submenu""  "
                    else
                        sClassSub = ""
                    end if

                    Montar_Links_Consulta = Montar_Links_Consulta & "<li "& sClassSub &">" & vbcrlf
                    Montar_Links_Consulta = Montar_Links_Consulta & "   <a href=""/categoria/"& TrataSEO(trim(RsOption("descricaoSEO"))) &"/"& trim(RsOption("id")) &""">" & FormataNome(trim(RsOption("descricao"))) &" <span class=""caret""></span></a>" & vbcrlf

                    if(sSubMenu <> "") then 
                        Montar_Links_Consulta = Montar_Links_Consulta & "<ul class=""dropdown-menu"">" & vbcrlf
                        Montar_Links_Consulta = Montar_Links_Consulta &     sSubMenu
                        Montar_Links_Consulta = Montar_Links_Consulta & "</ul>" & vbcrlf
                    end if

                    Montar_Links_Consulta = Montar_Links_Consulta & "</li>" & vbcrlf
            end select

	        lContLi = lContLi + 1	
            RsOption.movenext
		loop

        'if(PrTipo = "1" and lContLi > 30) then 
            'Montar_Links_Consulta = Montar_Links_Consulta & "<li><a href=""javascript:void(0);""><label for=""c1"" style=""cursor: auto; color:red"" id=""itemsub-mais"">mostrar mais...</label></a></li>" 
        'end if

        'if(PrTipo = "3" and lContLi > 10) then 
            'Montar_Links_Consulta = Montar_Links_Consulta & "<li><a href=""javascript:void(0);""><label for=""c1"" style=""cursor: auto; color:red"" id=""itemmod-mais"">mostrar mais...</label></a></li>" 
        'end if
	end if

	set RsOption = nothing
end function


' ====================================================================
function Monta_Lista_Produtos(prBusca, PrCat, PrboolPromocao, PrVendidos)
	dim sSQl, Rs, sConteudo, sCliente, sCidade, sNomeProduto, sData, sTitulo, sComentario, sAvaliacao, sDescricaoStyle, spalavraschaves, lContPro

	call Abre_Conexao()
	
    sSQl = "Select "

    sSQl = sSQl & "   TB_CATEGORIAS.Id,  " & _
                    " TB_CATEGORIAS.Categoria, " & _ 
                    " TB_CATEGORIAS.Categoria_Jp, " & _
                    " TB_PRODUTOS.ID AS IDP,  " & _
                    " TB_PRODUTOS.DESCRICAO,  " & _
                    " TB_PRODUTOS.DESTAQUE,  " & _
                    " TB_PRODUTOS.imagem, " & _
                    " TB_PRODUTOS.promocao,                 " & _
                    " TB_PRODUTOS.DescontoValor,                " & _
                    " TB_PRODUTOS.MsgCartao,                " & _
                    " TB_PRODUTOS.DescontoTipo,  " & _
                    " TB_PRODUTOS.Preco_Consumidor as Preco,  " & _
                    " TB_PRODUTOS.Preco_Aprazo,  " & _
                    " TB_PRODUTOS.CodReferencia,  " & _
                    " isnull(TB_PRODUTOS.Qte_Parcelamento,0) as Qte_Parcelamento,  " & _
                    " TB_PRODUTOS.Qte_Estoque,  " & _
                    " TB_PRODUTOS.Id_Cor,  " & _
                    " TB_PRODUTOS.DataIniPromo,  " & _
                    " TB_PRODUTOS.DataFimPromo,  " & _
                    " REPLACE(Destaque,CHAR(160),'') as MODELO, " & _
                    " (SELECT TOP 1 Descricao FROM TB_CORES WHERE id = Id_Cor) as CIDADE, " & _
                    " (SELECT TOP 1 Descricao FROM TB_TAMANHOS WHERE id = Id_Tamanho) as ESTADO, " & _
                    " (SELECT top 1 Descricao FROM TB_FRAGRANCIAS WHERE ID = Id_Fragrancia) as ANO,  " & _
                    " ISNULL((SELECT TOP 1 Descricao FROM TB_TIPOS WHERE ID = Id_Tipos),0) as HORAS,  " & _
                    " (SELECT TOP 100 Categoria FROM TB_CATEGORIAS WHERE id = Id_Categoria) as CATEGORIA,  " & _
                    " isnull(MsgCartao,'') as MsgCartao, " & _
                    " (SELECT top 1 Fabricante FROM TB_FABRICANTE WHERE id = Id_Fabricante) as Fabricante, " & _
                    " NumeroSerie, " & _
                    " CampoAux8, " & _
                    " CampoAux21 " & _
		        " from  " & _
				    " TB_CATEGORIAS (NOLOCK)"  & _
                    " INNER JOIN TB_PRODUTOS (NOLOCK) ON TB_CATEGORIAS.ID = TB_PRODUTOS.ID_CATEGORIA "  & _
			    "where " & _
				    " TB_CATEGORIAS.cdcliente       = "& sCdCliente     & _
				    " AND TB_CATEGORIAS.Ativo      = 1             "   & _
                    " AND TB_PRODUTOS.Ativo         = 1             "   & _
                    " AND isnull(TB_PRODUTOS.Imagem,'') <> ''       "

    if(prBusca <> "")       then 
        sSQl = sSQl & " AND TB_PRODUTOS.descricao like '%"& prBusca &"%' "
    end if

    if(PrCat <> "")       then 
        sSQl = sSQl & " AND TB_CATEGORIAS.id = " & PrCat
    else
        sSQl = sSQl & " AND TB_CATEGORIAS.Id <> 196200 "
    end if

    if(PrboolPromocao) then sSQl = sSQl & " AND TB_PRODUTOS.Promocao = 1"

    if(PrVendidos) then 
        sSQl = sSQl & " AND lower(TB_PRODUTOS.CampoAux8) = 'vendido' "
    else
        sSQl = sSQl & " AND lower(TB_PRODUTOS.CampoAux8) = 'disponível' OR lower(TB_PRODUTOS.CampoAux8) = 'em negociação' "
    end if

    'sSQl = sSQl & " order by newId() "
    sSQl = sSQl & " order by newId() "

    'Response.WRITE sSQl
    'Response.End
    set Rs = oConn.execute(sSQl)

    sConteudo   = ""
    lContPro    = 0
	
	if(not Rs.Eof) then
        'lContPro    = 1

		Do While (Not Rs.Eof)
            sIdCat	        = trim(Rs("id"))
            sIdProd	        = trim(Rs("IDP"))
			sCategoria	    = trim(Rs("Categoria"))
            sCategoriaJp	= trim(Rs("Categoria_Jp"))
            sImagem	        = trim(Rs("Imagem"))
            sPreco          = formatNumber(trim(Rs("Preco")),2)
            sPrecoPrazo     = formatNumber(trim(Rs("Preco_Aprazo")),2)
            sParcelamento   = trim(Rs("Qte_Parcelamento"))
            sDescontoTipo   = trim(Rs("DescontoTipo"))
            CodReferencia   = trim(Rs("CodReferencia"))
            MsgCartao       = trim(Rs("MsgCartao"))
			sImagem         = (Alterar_Tamanho_Imagem("m", "", "", trim(Rs("Imagem")), 242, 125))

			if(trim(Rs("DataIniPromo")) <> "" and not isnull(trim(Rs("DataIniPromo"))) )  then 
				BoolDataPromocao = ( (cdate(trim(Rs("DataIniPromo"))) <= Date) and (cDate(trim(Rs("DataFimPromo"))) >= Date) )
			end if

            if(BoolDataPromocao) then 
                Session("d" & sIdProd) = cdbl(trim(Rs("DescontoValor")))
                
                if(sDescontoTipo = "R") then 
                    sPrecofinal         = formatNumber((cdbl(sPreco) - cdbl(Session("d" & sIdProd)) ),2)   
                    sPrecofinalPrazo    = formatNumber((cdbl(sPrecoPrazo) - cdbl(Session("d" & sIdProd)) ),2) 
                else
                    sPrecofinal         = formatNumber((cdbl(sPreco) - ((cdbl(Session("d" & sIdProd))/100) * cdbl(sPreco))),2)   
                    sPrecofinalPrazo    = formatNumber((cdbl(sPrecofinalPrazo) - ((cdbl(Session("d" & sIdProd))/100) * cdbl(sPrecofinalPrazo))),2)   
                end if
            else
                if(Session("dc" & sIdProd) > 0) then                
                    sPrecofinal         = formatNumber((cdbl(sPreco) - cdbl(Session("dc" & sIdProd)) ),2)   
                    sPrecofinalPrazo    = formatNumber((cdbl(sPrecoPrazo) - cdbl(Session("dc" & sIdProd)) ),2) 
                    BoolDataPromocao    = true
                else
                    sPrecofinal             = sPreco
                    sPrecofinalPrazo        = sPrecoPrazo
                end if
            end if 

            if(cint(sParcelamento) > 0) then
                sParcela     = FormatNumber((cdbl(sPrecofinalPrazo) / cint(sParcelamento)),2)
            end if

            if(MsgCartao = "" or isnull(MsgCartao)) then 
                if(BoolDataPromocao) then     
                    MsgCartao = "<span style=""text-decoration: line-through;"">R$ "& formatNumber(sPreco, 2) &"</span> R$ " & sPrecofinal
                else
                    MsgCartao = "R$ " & sPrecofinal
                end if
            end if

			sConteudo = sConteudo & "<div class=""text-center boxes"">" & vbcrlf

				sConteudo = sConteudo & "<div class=""imagem"" style=""width: 238px;    height: 236px;     margin: 0 auto;"">" & vbcrlf

                    if(BoolDataPromocao) then sConteudo = sConteudo & "<img src=""/images/selo_oferta_semana.png"" alt="""" class=""selo-oferta"">" & vbcrlf

                    if(bVerificaLaudo(sIdProd)) then sConteudo = sConteudo & "<img src=""/images/selo.png"" alt="""" class=""tag-garantia-de-peso"">" & vbcrlf

					sConteudo = sConteudo & "<a href=""/detalhe-produto/"& TrataSEO(trim(Rs("DESCRICAO"))) &"/"& sIdProd &"""><img src="""& sImagem &""" alt="""& trim(Rs("DESCRICAO")) &""" class=""imagem-single-produto"" style=""width: 238px;height: auto;""></a>" & vbcrlf
				sConteudo = sConteudo & "</div>" & vbcrlf

				sConteudo = sConteudo & "<p class=""sub-title"" style=""height: 42px;"">"& trim(Rs("DESCRICAO")) &"</p>" & vbcrlf
				sConteudo = sConteudo & "<p class=""sub-details"">Ano: "& trim(Rs("ANO")) &"<br>Cidade: "& trim(Rs("CIDADE")) &"</p>" & vbcrlf
				sConteudo = sConteudo & "<div class=""price"">" & vbcrlf
					sConteudo = sConteudo & "<hr>" & vbcrlf
					sConteudo = sConteudo & "<span class=""valor-price"" style=""font-size: 19px;"">"& MsgCartao &"</span>" & vbcrlf
				sConteudo = sConteudo & "</div>" & vbcrlf
				sConteudo = sConteudo & "<p class=""campo-btn-comprar""><a href=""javascript: EnviarComparacao('"& sIdProd &"');"" class=""btn btn-comprar bg-yellow"">COMPARAR</a></p>" & vbcrlf
				sConteudo = sConteudo & "<p class=""select-comparar""><input type=""checkbox"" id=""comparar1"" > <label for=""comparar1"">Comparar</label></p>" & vbcrlf
			sConteudo = sConteudo & "</div>" & vbcrlf


            BoolDataPromocao = false
            lContPro         = lContPro + 1
		rs.movenext
		loop
	end if
	set Rs = nothing

	call Fecha_Conexao()
	
    sResultado2Count     = lContPro
    Monta_Lista_Produtos = sConteudo
end function   

' ====================================================================
function Monta_Carrossel(PrCat)
	dim sSQl, Rs, sConteudo, sCliente, sCidade, sNomeProduto, sData, sTitulo, sComentario, sAvaliacao, sDescricaoStyle, spalavraschaves

	call Abre_Conexao()
	
	sSQl = "Select Id, Categoria, Categoria_Jp, Categoria_Desc, PalavrasChaves " & _
		    " from  " & _
				" TB_CATEGORIAS (NOLOCK)"  & _
			"where " & _
				" cdcliente = "& sCdCliente             & _
				" AND Ativo     = 1                     "  & _
                " AND EXISTS(SELECT TOP 1 P.ID FROM TB_PRODUTOS P (NOLOCK) WHERE P.ID_CATEGORIA = TB_CATEGORIAS.ID)  "

    if(PrCat <> "") then 
        sSQl = sSQl & " AND id =  " & PrCat
    end if
    sSQl = sSQl & " order by isnull(TB_CATEGORIAS.Ordem,99) "

    'response.write sSQl
    'response.end
    set Rs = oConn.execute(sSQl)

    sConteudo = ""
	
	if(not Rs.Eof) then
		Do While (Not Rs.Eof)
            sIdCat	        = trim(Rs("id"))
			sCategoria	    = trim(Rs("Categoria"))
            sCategoriaJp	= trim(Rs("Categoria_Jp"))
            sDescricaoStyle = trim(Rs("Categoria_Desc"))
            spalavraschaves = trim(Rs("palavraschaves"))

            if(sDescricaoStyle <> "" and not isnull(sDescricaoStyle)) then sDescricaoStyle = replace(sDescricaoStyle,"*","""")

            sConteudo = sConteudo & "<div id="""& spalavraschaves &""">" & vbcrlf
	        sConteudo = sConteudo & "    <a class=""buttons prev"" href=""#"">&#60;</a>" & vbcrlf
            sConteudo = sConteudo & "    <div class=""viewport"">" & vbcrlf
		    sConteudo = sConteudo & "        <ul class=""overview"">" & vbcrlf
            sConteudo = sConteudo &             Monta_Carrossel_Produtos(sIdCat, false)
		    sConteudo = sConteudo & "        </ul>" & vbcrlf
            sConteudo = sConteudo & "    </div>" & vbcrlf
            sConteudo = sConteudo & "    <a class=""buttons next"" href=""#"">&#62;</a>" & vbcrlf
            sConteudo = sConteudo & "</div>" & vbcrlf

            sDescricaoStyle = ""
		rs.movenext
		loop
	end if
	set Rs = nothing

	call Fecha_Conexao()
	
    Monta_Carrossel = sConteudo
end function   



' ====================================================================
function Monta_Carrossel_Produtos(prCat, prPromocao, prId, prIdnot)
	dim sSQl, Rs, sConteudo, sCliente, sCidade, sNomeProduto, sData, sTitulo, sComentario, sAvaliacao, sclass

	call Abre_Conexao()
	
    sTopSelec = ""

    if(prId <> "") then 
        sTopSelec       = " TOP 1 "
        sStyleImagem    = "style=""width: 653px;height: 338px;"" "
    else
        sTopSelec       = " TOP 4 "
        sStyleImagem    = "style=""width: 263px;height: 340px;"" "
    end if

	sSQl = "Select "& sTopSelec &" " & _
                " TB_CATEGORIAS.Id, TB_CATEGORIAS.Categoria, TB_CATEGORIAS.Categoria_Jp, " & _
                " TB_PRODUTOS.ID AS IDP, TB_PRODUTOS.Preco_Consumidor as Preco, TB_PRODUTOS.* " & _
		    " from  " & _
				" TB_CATEGORIAS (NOLOCK)"  & _
                " INNER JOIN TB_PRODUTOS (NOLOCK) ON TB_CATEGORIAS.ID = TB_PRODUTOS.ID_CATEGORIA "  & _
			"where " & _
				" TB_CATEGORIAS.cdcliente       = "& sCdCliente     & _
				" AND TB_CATEGORIAS.Ativo       = 1             "   & _
                " AND TB_PRODUTOS.Ativo         = 1             "   & _
                " AND TB_PRODUTOS.Qte_Estoque   > 0             "   & _
                " AND isnull(TB_PRODUTOS.Imagem2,'') <> ''       "

    if(prCat <> "") then 
        sSQl = sSQl & " AND TB_PRODUTOS.ID_CATEGORIA  = "& prCat
    else
        if(prPromocao) then
            sSQl = sSQl & " AND TB_PRODUTOS.promocao = 1"
        end if    
    end if

    if(prId <> "") then 
        sSQl = sSQl & " AND TB_PRODUTOS.ID  = "& prId
    end if

    if(prIdnot <> "") then 
        sSQl = sSQl & " AND TB_PRODUTOS.ID <> "& prIdnot
    end if

    'sSQl = sSQl & " order by TB_PRODUTOS.OrdemVitrine, TB_PRODUTOS.Preco_Consumidor "
    sSQl = sSQl & " order by newId() "

    'response.Write sSQl
    'response.End
    set Rs = oConn.execute(sSQl)
	
    sclass      = ""
    sConteudo   = ""

	if(not Rs.Eof) then
        lCont = 1

		Do While (Not Rs.Eof)
            sIdP	            = trim(Rs("IDP"))
			sDescricao          = CortaLen(trim(Rs("DESCRICAO")), 50)
            sDestaque           = trim(Rs("destaque"))
            sPreco              = formatNumber(trim(Rs("Preco")),2)
            sPrecoPrazo         = formatNumber(trim(Rs("Preco_Aprazo")),2)
            sParcelamento       = trim(Rs("Qte_Parcelamento"))
            sDescontoTipo       = trim(Rs("DescontoTipo"))
			sImagem             = (Alterar_Tamanho_Imagem("m", "", "", trim(Rs("Imagem8")), 500, 500))
            BoolDataPromocao    = false

			if(trim(Rs("DataIniPromo")) <> "" and not isnull(trim(Rs("DataIniPromo"))) )  then 
				BoolDataPromocao = ( (cdate(trim(Rs("DataIniPromo"))) <= Date) and (cDate(trim(Rs("DataFimPromo"))) >= Date) )
			end if

            if(BoolDataPromocao) then 
                Session("d" & sIdProd) = cdbl(trim(Rs("DescontoValor")))
                
                if(sDescontoTipo = "R") then 
                    sPrecofinal         = formatNumber((cdbl(sPreco) - cdbl(Session("d" & sIdProd)) ),2)   
                    sPrecofinalPrazo    = formatNumber((cdbl(sPrecoPrazo) - cdbl(Session("d" & sIdProd)) ),2) 
                else
                    sPrecofinal         = formatNumber((cdbl(sPreco) - ((cdbl(Session("d" & sIdProd))/100) * cdbl(sPreco))),2)   
                    sPrecofinalPrazo    = formatNumber((cdbl(sPrecofinalPrazo) - ((cdbl(Session("d" & sIdProd))/100) * cdbl(sPrecofinalPrazo))),2)   
                end if
            else
                if(Session("dc" & sIdProd) > 0) then                
                    sPrecofinal         = formatNumber((cdbl(sPreco) - cdbl(Session("dc" & sIdProd)) ),2)   
                    sPrecofinalPrazo    = formatNumber((cdbl(sPrecoPrazo) - cdbl(Session("dc" & sIdProd)) ),2) 
                    BoolDataPromocao    = true
                else
                    sPrecofinal             = sPreco
                    sPrecofinalPrazo        = sPrecoPrazo
                end if
            end if 

            boolPRodutoNovo = false
            if((day(trim(Rs("datacad"))) = day(date)) and (month(trim(Rs("datacad"))) = month(date)) and (year(trim(Rs("datacad"))) = year(date))) then boolPRodutoNovo = true

   
            if(prId <> "") then 
                sConteudo = sConteudo & "<div class=""col-sm-7""><a href=""/detalhe-produto/"& TrataSEO(trim(Rs("DESCRICAO"))) &"/"& sIdP &"""><div class=""sombra""></div><img src="""& sImagem &""" alt="""& sDescricao &""" "& sStyleImagem &"><strong class=""nome-da-oferta"">"& sDescricao &"</strong></a></div>"
            else
                sConteudo = sConteudo & "<div class=""col-sm-3""><a href=""/detalhe-produto/"& TrataSEO(trim(Rs("DESCRICAO"))) &"/"& sIdP &"""><div class=""sombra""></div><img src="""& sImagem &""" alt="""& sDescricao &""" "& sStyleImagem &"><strong class=""nome-da-oferta"">"& sDescricao &"</strong></a></div>"
            end if

            lCont   = (lCont + 1)
		rs.movenext
		loop
	end if
	set Rs = nothing

	call Fecha_Conexao()
	
    Monta_Carrossel_Produtos = sConteudo
end function



' ====================================================================
function Monta_Carrossel_Produtos_Duplo(prCat)
	dim sSQl, Rs, RsCat, sConteudo, sCliente, sCidade, sNomeProduto, sData, sTitulo, sComentario, sAvaliacao, sclass

	call Abre_Conexao()
	
	sSQl = "Select top 1 " & _
                " * " & _
		    " from  " & _
				" TB_CATEGORIAS (NOLOCK) "  & _
			" where " & _
				" TB_CATEGORIAS.cdcliente       = "& sCdCliente     & _
				" AND TB_CATEGORIAS.Ativo       = 1             "   & _
                " AND TB_CATEGORIAS.Especial    = 1             "   & _
                " AND EXISTS (SELECT top 1 ID FROM TB_PRODUTOS PS (nolock) WHERE PS.ID_CATEGORIA = TB_CATEGORIAS.id and PS.ATIVO = 1) "

    if(prCat <> "") then 
        sSQl = sSQl & " AND ID = "& prCat
    end if

    sSQl = sSQl & " order by isnull(TB_CATEGORIAS.Ordem, 99) "

    'response.Write sSQl
    'response.End
    set RsCat = oConn.execute(sSQl)
	
	if(not RsCat.Eof) then
        lCont = 1

		Do While (Not RsCat.Eof)
            sIdPCat	            = trim(RsCat("id"))
			sDescricaoCat       = trim(RsCat("Categoria"))

            sConteudo = sConteudo &  "<ul class=""products-grid"">"

	        sSQl = "Select top 4 " & _
                        " TB_PRODUTOS.* " & _
		            " from  " & _
				        " TB_PRODUTOS (NOLOCK)"  & _
			        "where " & _
				        " cdcliente                     = "& sCdCliente     & _
                        " AND TB_PRODUTOS.id_categoria  = "& sIdPCat &"             "   & _
                        " AND TB_PRODUTOS.Ativo         = 1             "   & _
                        " AND isnull(TB_PRODUTOS.Imagem,'') <> ''       "

            sSQl = sSQl & " order by newId() "

            set Rs = oConn.execute(sSQl)
	
	        if(not Rs.Eof) then
                lCont = 1

		        Do While (Not Rs.Eof)
                    sIdP	            = trim(Rs("id"))
			        sDescricao          = CortaLen(trim(Rs("DESCRICAO")), 25)
                    sDestaque           = trim(Rs("destaque"))
                    sImagem	            = trim(Rs("Imagem"))
                    sPreco              = formatNumber(trim(Rs("Preco_Consumidor")),2)
                    sPrecoPrazo         = formatNumber(trim(Rs("Preco_Aprazo")),2)
                    sParcelamento       = trim(Rs("Qte_Parcelamento"))
                    sDescontoTipo       = trim(Rs("DescontoTipo"))
			        sImagem             = (Alterar_Tamanho_Imagem("mm", "", "", trim(Rs("Imagem")), 135, 135))
                    BoolDataPromocao    = false

			        if(trim(Rs("DataIniPromo")) <> "" and not isnull(trim(Rs("DataIniPromo"))) )  then 
				        BoolDataPromocao = ( (cdate(trim(Rs("DataIniPromo"))) <= Date) and (cDate(trim(Rs("DataFimPromo"))) >= Date) )
			        end if

                    if(BoolDataPromocao) then 
                        Session("d" & sIdProd) = cdbl(trim(Rs("DescontoValor")))
                
                        if(sDescontoTipo = "R") then 
                            sPrecofinal         = formatNumber((cdbl(sPreco) - cdbl(Session("d" & sIdProd)) ),2)   
                            sPrecofinalPrazo    = formatNumber((cdbl(sPrecoPrazo) - cdbl(Session("d" & sIdProd)) ),2) 
                        else
                            sPrecofinal         = formatNumber((cdbl(sPreco) - ((cdbl(Session("d" & sIdProd))/100) * cdbl(sPreco))),2)   
                            sPrecofinalPrazo    = formatNumber((cdbl(sPrecofinalPrazo) - ((cdbl(Session("d" & sIdProd))/100) * cdbl(sPrecofinalPrazo))),2)   
                        end if
                    else
                        if(Session("dc" & sIdProd) > 0) then                
                            sPrecofinal         = formatNumber((cdbl(sPreco) - cdbl(Session("dc" & sIdProd)) ),2)   
                            sPrecofinalPrazo    = formatNumber((cdbl(sPrecoPrazo) - cdbl(Session("dc" & sIdProd)) ),2) 
                            BoolDataPromocao    = true
                        else
                            sPrecofinal             = sPreco
                            sPrecofinalPrazo        = sPrecoPrazo
                        end if
                    end if 

                    boolPRodutoNovo = false
                    if((day(trim(Rs("datacad"))) = day(date)) and (month(trim(Rs("datacad"))) = month(date)) and (year(trim(Rs("datacad"))) = year(date))) then boolPRodutoNovo = true


                    sConteudo = sConteudo &  "  <li class=""item col-lg-6 col-md-6 col-sm-6 col-xs-12"">"
                    sConteudo = sConteudo &  "    <div class=""item-inner"">"
                    sConteudo = sConteudo &  "      <div class=""item-img""> <a class=""product-image"" title=""Retis lapen casen"" href=""/detalhe-produto/"& TrataSEO(trim(Rs("DESCRICAO"))) &"/"& sIdP &"""> <img alt=""HTML template"" src="""& sImagem &""" alt="""& sDescricao &"""> </a> </div>"
                    sConteudo = sConteudo &  "      <div class=""item-info"">"
                    sConteudo = sConteudo &  "        <div class=""info-inner"">"
                    sConteudo = sConteudo &  "          <div class=""item-title""> <a title="""& sDescricao &""" href=""/detalhe-produto/"& TrataSEO(trim(Rs("DESCRICAO"))) &"/"& sIdP &""">"& sDescricao &"</a> </div>"

                    if(BoolDataPromocao and Session("d" & sIdProd) > 0) then
                        sConteudo = sConteudo & "<div class=""price-box""> <span class=""regular-price""><span class=""price"" style=""text-decoration: line-through;    font-size: 14px;"">R$ "& sPreco &"<br/></span><span class=""price"">R$ "& sPrecofinal &"</span> </span> </div>"
                    else
                        sConteudo = sConteudo & "<div class=""price-box""> <span class=""regular-price""><span class=""price"">R$ "& sPrecofinal &"</span> </span> </div>"
                    end if


                    sConteudo = sConteudo &  "          <div class=""pro-action""><button type=""button"" class=""add-to-cart"" onclick=""javascript: location.href='/detalhe-produto/"& TrataSEO(trim(Rs("DESCRICAO"))) &"/"& sIdP &"'""> <i class=""fa fa-shopping-basket""></i><span>Comprar</span></button></div>"
                    sConteudo = sConteudo &  "          <div class=""pr-button-hover"">"
                    sConteudo = sConteudo & "              <div class=""mt-button quick-view""> <a href=""/quick_view.asp?i="& sIdP &"""><i class=""fa fa-search""></i> </a> </div>"
                    sConteudo = sConteudo &  "          </div>"
                    sConteudo = sConteudo &  "        </div>"
                    sConteudo = sConteudo &  "      </div>"
                    sConteudo = sConteudo &  "    </div>"
                    sConteudo = sConteudo &  "  </li>"

		        Rs.movenext
		        loop
	        end if
	        set Rs = nothing

            sConteudo = sConteudo &  "</ul>"

            lCont   = (lCont + 1)
		RsCat.movenext
		loop
	end if
	set RsCat = nothing

	call Fecha_Conexao()
	
    Monta_Carrossel_Produtos_Duplo = sConteudo
end function



' ====================================================================
function Monta_Carrossel_Home(prTipo, booldestaque)
	dim sSQl, Rs, sConteudo, sCliente, sCidade, sNomeProduto, sData, sTitulo, sComentario, sAvaliacao, sclass

	call Abre_Conexao()
	
    sSQl = "Select  "

    sSQl = sSQl & " TB_CATEGORIAS.Id, TB_CATEGORIAS.Categoria, TB_CATEGORIAS.Categoria_Jp, " & _
                " TB_PRODUTOS.ID AS IDP, TB_PRODUTOS.DESCRICAO, TB_PRODUTOS.DESTAQUE, TB_PRODUTOS.imagem, " & _
                " TB_PRODUTOS.promocao,                 " & _
                " TB_PRODUTOS.DescontoValor,                " & _
                " TB_PRODUTOS.DescontoTipo,  " & _
                " TB_PRODUTOS.Preco_Consumidor as Preco,  " & _
                " TB_PRODUTOS.Preco_Aprazo,  " & _
                " TB_PRODUTOS.CodReferencia,  " & _
                " isnull(TB_PRODUTOS.Qte_Parcelamento,0) as Qte_Parcelamento,  " & _
                " TB_PRODUTOS.Qte_Estoque,  " & _
                " TB_PRODUTOS.Id_Cor,  " & _
                " TB_PRODUTOS.DataIniPromo,  " & _
                " TB_PRODUTOS.DataFimPromo  " & _
		    " from  " & _
				" TB_CATEGORIAS (NOLOCK)"  & _
                " INNER JOIN TB_PRODUTOS (NOLOCK) ON TB_CATEGORIAS.ID = TB_PRODUTOS.ID_CATEGORIA "  & _
			"where " & _
				" TB_CATEGORIAS.cdcliente       = "& sCdCliente     & _
				" AND TB_CATEGORIAS.Ativo       = 1             "   & _
                " AND TB_PRODUTOS.Ativo         = 1             "   & _
                " AND isnull(TB_PRODUTOS.Imagem,'') <> ''       "

    if(prTipo = "1")                        then sSQl = sSQl & " AND TB_PRODUTOS.Promocao      =  1"
    if(prTipo = "2")                        then sSQl = sSQl & " AND TB_PRODUTOS.Promocao      =  1"
    if(prTipo = "3")                        then sSQl = sSQl & " AND TB_PRODUTOS.lancamento    =  1"
    if(prTipo = "4")                        then sSQl = sSQl & " AND EXISTS (SELECT TOP 1 TB_PRODUTOS_SUGESTOES.ID FROM TB_PRODUTOS_SUGESTOES (NOLOCK) WHERE TB_PRODUTOS_SUGESTOES.Id_Produto_Sugestao = TB_PRODUTOS.ID) "
    if(cdbl(prTipo) > 10)                   then sSQl = sSQl & " AND TB_PRODUTOS.ID_CATEGORIA = "& prTipo
    if(cdbl(prTipo) > 10 and booldestaque)  then sSQl = sSQl & " AND TB_PRODUTOS.ID_CATEGORIA = "& prTipo &" AND TB_PRODUTOS.lancamento = 1"

    ' sSQl = sSQl & " order by isnull(TB_PRODUTOS.DESCRICAO, 99) "
    sSQl = sSQl & " order by TB_PRODUTOS.Preco_Consumidor "

    ' response.Write sSQl
    ' response.End
    set Rs = oConn.execute(sSQl)
	
    sclass      = ""
    sConteudo   = ""

	if(not Rs.Eof) then
        lCont = 1

		Do While (Not Rs.Eof)
            sIdP	        = trim(Rs("IDP"))
            sDestaque       = trim(Rs("destaque"))
            sPreco          = formatNumber(trim(Rs("Preco")),2)
            sPrecoPrazo     = formatNumber(trim(Rs("Preco_Aprazo")),2)
            sParcelamento   = trim(Rs("Qte_Parcelamento"))
            sDescontoTipo   = trim(Rs("DescontoTipo"))

            if(prTipo = "1") then 
    			sImagem   = (Alterar_Tamanho_Imagem("carrossel", "", "", trim(Rs("Imagem")), 130, 130))
                sCssImg   = "style=""width:130px; height:130px;"""
			    sDescricao= CortaLen(trim(Rs("DESCRICAO")),25)
            else
                sImagem   = (Alterar_Tamanho_Imagem("m", "", "", trim(Rs("Imagem")), 210, 210))
                sCssImg   = "style=""width:210px; height:210px;"""
    			sDescricao= trim(Rs("DESCRICAO"))
            end if

			if(trim(Rs("DataIniPromo")) <> "" and not isnull(trim(Rs("DataIniPromo"))) )  then 
				BoolDataPromocao = ( (cdate(trim(Rs("DataIniPromo"))) <= Date) and (cDate(trim(Rs("DataFimPromo"))) >= Date) )
			end if

            if(BoolDataPromocao) then 
                Session("d" & sIdProd) = cdbl(trim(Rs("DescontoValor")))
                
                if(sDescontoTipo = "R") then 
                    sPrecofinal         = formatNumber((cdbl(sPreco) - cdbl(Session("d" & sIdProd)) ),2)   
                    sPrecofinalPrazo    = formatNumber((cdbl(sPrecoPrazo) - cdbl(Session("d" & sIdProd)) ),2) 
                else
                    sPrecofinal         = formatNumber((cdbl(sPreco) - ((cdbl(Session("d" & sIdProd))/100) * cdbl(sPreco))),2)   
                    sPrecofinalPrazo    = formatNumber((cdbl(sPrecofinalPrazo) - ((cdbl(Session("d" & sIdProd))/100) * cdbl(sPrecofinalPrazo))),2)   
                end if
            else
                if(Session("dc" & sIdProd) > 0) then                
                    sPrecofinal         = formatNumber((cdbl(sPreco) - cdbl(Session("dc" & sIdProd)) ),2)   
                    sPrecofinalPrazo    = formatNumber((cdbl(sPrecoPrazo) - cdbl(Session("dc" & sIdProd)) ),2) 
                    BoolDataPromocao    = true
                else
                    sPrecofinal             = sPreco
                    sPrecofinalPrazo        = sPrecoPrazo
                end if
            end if 

            if(cint(sParcelamento) > 0) then
                sParcela     = FormatNumber((cdbl(sPrecofinalPrazo) / cint(sParcelamento)),2)
            end if

            sConteudo = sConteudo & "<a href=""/detalhe-produto/"& TrataSEO(trim(Rs("DESCRICAO"))) &"/"& sIdP &""">" & vbcrlf

            'if(cdbl(prTipo) < 10) then
            '    sConteudo = sConteudo & "   <div class=""hover_bt_comprar""></div>" & vbcrlf
            'end if

            sConteudo = sConteudo & "   <img src="""& sImagem &"""  alt="""& trim(Rs("DESCRICAO")) &""" "& sCssImg &" >" & vbcrlf

            if(prTipo <> "1") then
                sConteudo = sConteudo & "<p><span class=""nome_produto"">"& sDescricao &"<br/><br/></span>" & vbcrlf
                sConteudo = sConteudo & "<span class=""nome_produto_destaque"">"& sDestaque &"</span>" & vbcrlf
            else
                sConteudo = sConteudo & "<p><span class=""nome_produto_lista"">"& CortaLen(sDescricao,15) &"<br/><br/></span>" & vbcrlf
            end if

            if(cint(sParcelamento) > 0) then
                if(prTipo <> "1") then
                    sConteudo = sConteudo & "<span class=""parcelas_produto"">"& sParcelamento &"x "& sParcela
                    sConteudo = sConteudo & " sem juros</span>" & vbcrlf
                else
                    sConteudo = sConteudo & "<span class=""parcelas_produto"">"& sParcelamento &"x "& sParcela & "<br/>"
                end if
            end if

            if(nDescontoBoleto <> "") then 
                if(nDescontoBoleto > 0) then 
                    sPrecofinal = FormatNumber((cdbl(sPrecofinal) - (cdbl(sPrecofinal) * cdbl(nDescontoBoleto)) ), 2)
                end if
            end if

            if(BoolDataPromocao and Session("d" & sIdProd) > 0) then 
                sConteudo = sConteudo & "<span class=""valor_de_produto"">De: R$ "& sPreco &"</span><br/>" & vbcrlf
                sConteudo = sConteudo & "<span class=""total_produto"">R$ "& sPrecofinal &"</span>" & vbcrlf
            else
                sConteudo = sConteudo & " <span class=""total_produto"">R$ "& sPrecofinal &"</span>" & vbcrlf
            end if
    
            sConteudo = sConteudo & " <br/><span class=""nome_produto_boleto"">no boleto "& nDescontoBoletoLeng &"%</span>" & vbcrlf
            sConteudo = sConteudo & "   </p>" & vbcrlf

            if(prTipo <> "1" and prTipo <> "2" and prTipo <> "3") then 
                sConteudo = sConteudo & "<br/><img src=""/img/botao_comprar.png""  alt="""& trim(Rs("DESCRICAO")) &""" title="""& trim(Rs("DESCRICAO")) &"""  style=""float: initial;"" >" & vbcrlf
            end if

            sConteudo = sConteudo & "</a>" & vbcrlf

            prTipo1FiltrosIdProdutos = (prTipo1FiltrosIdProdutos & sIdP & ",")
		rs.movenext
		loop
	end if
	set Rs = nothing

	call Fecha_Conexao()
	
    Monta_Carrossel_Home = sConteudo
end function



'======================================================
'Clear url name. URL Rewriting for SEO implementations
'======================================================
Function TrataSEO(palavra)

    if(palavra = "" or isnull(palavra)) then exit function

	palavra = TRIM(palavra)
	palavra = LCASE(palavra)
	palavra = Replace(palavra,"ä","a")
	palavra = Replace(palavra,"á","a")
	palavra = Replace(palavra,"à","a")
	palavra = Replace(palavra,"â","a")
	palavra = Replace(palavra,"ë","e")
	palavra = Replace(palavra,"é","e")
	palavra = Replace(palavra,"è","e")
	palavra = Replace(palavra,"ê","e")
	palavra = Replace(palavra,"ï","i")
	palavra = Replace(palavra,"í","i")
	palavra = Replace(palavra,"ì","i")
	palavra = Replace(palavra,"î","i")
	palavra = Replace(palavra,"ö","o")
	palavra = Replace(palavra,"ó","o")
	palavra = Replace(palavra,"ò","o")
	palavra = Replace(palavra,"ô","o")
	palavra = Replace(palavra,"ú","u")
	palavra = Replace(palavra,"ù","u")
	palavra = Replace(palavra,"û","u")
	palavra = Replace(palavra,"ü","u")
	palavra = Replace(palavra,"ç","c")
	palavra = Replace(palavra,"ã","a")
	palavra = Replace(palavra,"õ","o")
	palavra = Replace(palavra,"î","i")
	palavra = Replace(palavra,"ô","o")
	palavra = Replace(palavra,"û","u")
	palavra = Replace(palavra,"'","")
	palavra = Replace(palavra,"""","")
	palavra = Replace(palavra,";","")
	palavra = Replace(palavra,"^","")
	palavra = Replace(palavra,".","")
	palavra = Replace(palavra,",","")
	palavra = Replace(palavra,"`","")
	palavra = Replace(palavra,"´","")
	palavra = Replace(palavra,"!","")
	palavra = Replace(palavra,"?","")
	palavra = Replace(palavra,":","")
	palavra = Replace(palavra,"(","")
	palavra = Replace(palavra,")","")
	palavra = Replace(palavra,"/","e")
	palavra = Replace(palavra,"\","e")
	palavra = Replace(palavra,">","")
	palavra = Replace(palavra,"<","")
	palavra = Replace(palavra,"&gt;","")
	palavra = Replace(palavra,"&lt;","")
	palavra = Replace(palavra,"@","")
	palavra = Replace(palavra,"#","")
	palavra = Replace(palavra,"$","")
	palavra = Replace(palavra,"%","")
	palavra = Replace(palavra,"¨","")
	palavra = Replace(palavra,"&","")
	palavra = Replace(palavra,"*","")
	palavra = Replace(palavra,"_","-")
	palavra = Replace(palavra,"=","-")
	palavra = Replace(palavra,"º","-")
	palavra = Replace(palavra,"ª","-")
	palavra = Replace(palavra," - ","_")
	palavra = Replace(palavra,"+","mais")
	palavra = Replace(palavra,"–","")
	
	palavra = Replace(palavra," ","-")
	palavra = Replace(palavra,hex(96),"a")
	
	if	palavra = "" then	palavra = "-" end if

    while instr(palavra,"---") > 0
		palavra = Replace(palavra,"---","-")
	wend

    while instr(palavra,"--") > 0
		palavra = Replace(palavra,"--","-")
	wend
   	TrataSEO = palavra
End Function
'======================================================
'Clear url name. URL Rewriting for SEO implementations
'======================================================
Function TrataInteresse(palavra)
	palavra = TRIM(palavra)
	palavra = LCASE(palavra)
	palavra = Replace(palavra,"ä","a")
	palavra = Replace(palavra,"á","a")
	palavra = Replace(palavra,"à","a")
	palavra = Replace(palavra,"â","a")
	palavra = Replace(palavra,"ë","e")
	palavra = Replace(palavra,"é","e")
	palavra = Replace(palavra,"è","e")
	palavra = Replace(palavra,"ê","e")
	palavra = Replace(palavra,"ï","i")
	palavra = Replace(palavra,"í","i")
	palavra = Replace(palavra,"ì","i")
	palavra = Replace(palavra,"î","i")
	palavra = Replace(palavra,"ö","o")
	palavra = Replace(palavra,"ó","o")
	palavra = Replace(palavra,"ò","o")
	palavra = Replace(palavra,"ô","o")
	palavra = Replace(palavra,"ú","u")
	palavra = Replace(palavra,"ù","u")
	palavra = Replace(palavra,"û","u")
	palavra = Replace(palavra,"ü","u")
	palavra = Replace(palavra,"ç","c")
	palavra = Replace(palavra,"ã","a")
	palavra = Replace(palavra,"õ","o")
	palavra = Replace(palavra,"î","i")
	palavra = Replace(palavra,"ô","o")
	palavra = Replace(palavra,"û","u")
	palavra = Replace(palavra,"'","")
	palavra = Replace(palavra,"""","")
	palavra = Replace(palavra,";","")
	palavra = Replace(palavra,"^","")
	palavra = Replace(palavra,".","")
	palavra = Replace(palavra,",","")
	palavra = Replace(palavra,"`","")
	palavra = Replace(palavra,"´","")
	palavra = Replace(palavra,"!","")
	palavra = Replace(palavra,"?","")
	palavra = Replace(palavra,":","")
	palavra = Replace(palavra,"(","")
	palavra = Replace(palavra,")","")
'	palavra = Replace(palavra,"/","e")
	palavra = Replace(palavra,"\","e")
	palavra = Replace(palavra,">","")
	palavra = Replace(palavra,"<","")
	palavra = Replace(palavra,"&gt;","")
	palavra = Replace(palavra,"&lt;","")
	palavra = Replace(palavra,"@","")
	palavra = Replace(palavra,"#","")
	palavra = Replace(palavra,"$","")
	palavra = Replace(palavra,"%","")
	palavra = Replace(palavra,"¨","")
	palavra = Replace(palavra,"&","")
	palavra = Replace(palavra,"*","")
	palavra = Replace(palavra,"_","-")
	palavra = Replace(palavra,"=","-")
	palavra = Replace(palavra,"º","-")
	palavra = Replace(palavra,"ª","-")
	palavra = Replace(palavra," - ","_")
	palavra = Replace(palavra,"+","mais")
	palavra = Replace(palavra," ","-")
	while instr(palavra,"--") > 0
		palavra = Replace(palavra,"--","-")
	wend
	
	palavra = Replace(palavra," ","-")
	palavra = Replace(palavra,hex(96),"a")
	
	if	palavra = "" then	palavra = "-" end if
   	TrataInteresse = palavra
End Function

' =======================================================================
' D B M  -  Hunter Fan -  VERIDICAR SITUAÇÃO DO PRODUTO NO ATO DA COMPRA *********************
' =======================================================================
function Verifica_Situacao_Produto(sIdProduto)
	dim RsSituacao, RsSituacao2
	
	'L = Lançamento,		= TB_PRODUTOS.Lancamento = 1 / TB_PRODUTOS.DataIniLan/ TB_PRODUTOS.DataFimLan
	'D = desconto até 50%	= TB_PRODUTOS. Promocao = 1 / TB_PRODUTOS.DataIniPromo / TB_PRODUTOS.DataFimPromo
	'M = mais  vendido		= *(select na TB_PEDIDOS_ITENS )
	'N = Venda normal		= tudo que for diferente dessas situações.
	
	if(sIdProduto = "") then 
		Verifica_Situacao_Produto = ""
		exit function
	end if
	
	Verifica_Situacao_Produto	=	"N"

	Call Abre_Conexao()

	sSql	=	"Select lancamento, DataIniLan, DataFimLan, Promocao, DataIniPromo, DataFimPromo from Tb_Produtos (nolock) where Id = " & sIdProduto
	set RsSituacao = oConn.execute(sSql)

	if(not RsSituacao.eof) Then 
		if (trim(RsSituacao("Promocao")) = "1" ) then
			if(cDate(RsSituacao("DataFimPromo")) > date() and cDate(RsSituacao("DataIniPromo")) < date()) then
				Verifica_Situacao_Produto	=	"D"
				exit function
			end if
		end if
		if (trim(RsSituacao("lancamento")) = "1" ) then
			if(cDate(RsSituacao("DataFimLan")) > date() and cDate(RsSituacao("DataIniLan")) < date()) then
				Verifica_Situacao_Produto	=	"L"
				exit function
			end if
		end if
		
		if(Session("ProdutosMV") <> "") then 
			if(InStr("," & Session("ProdutosMV") & ",", "," & sIdProduto & ",") > 0) then
				Verifica_Situacao_Produto	=	"M"
				exit function
			end if
		else
			Session("ProdutosMV") = ""
			
			sSql =	" Select " & _
				"       distinct top(10) " & _
				"       count(PI.Id_Produto) as total,  " & _
				"       PI.Id_Produto  " & _
				" from    " & _
				"       TB_PEDIDOS_ITENS PI (NOLOCK)   " & _
				"       INNER JOIN TB_PEDIDOS P1 (nolock) ON PI.Id_Pedido = P1.ID  " & _
				"       INNER JOIN TB_PRODUTOS P (nolock)  ON PI.Id_Produto = P.ID and p.ativo = 1 and P.cdcliente = "& sCdCliente &" " & _
				"       INNER JOIN TB_CATEGORIAS C (nolock) ON P.Id_Categoria = C.ID AND C.ATIVO = 1 AND C.ID_TIPO = 'S' AND C.cdcliente = "& sCdCliente &"  " & _
				" where   " & _
				"       P1.datacad between getdate()-30 and getdate() " & _
				" group by PI.Id_Produto   " & _
				" order by count(PI.Id_Produto) desc  "
			set RsSituacao2 = oConn.execute(sSql)
			
			if(not RsSituacao2.eof) Then 
					do while not RsSituacao2.eof

						if(trim(sIdProduto) = trim(trim(RsSituacao2("Id_Produto")))) then
							Verifica_Situacao_Produto	=	"M"
						end if

						Session("ProdutosMV") = ("," & Session("ProdutosMV") & trim(RsSituacao2("Id_Produto")) &",")
						
					RsSituacao2.movenext
					loop
			end if
			set RsSituacao2 = nothing
		end if
	end if
	set RsSituacao = nothing
	
end Function


' SEO MídiaClick ============================================
' =========================================================================
Function RemoveHTMLTags(ByVal strHTML)
    Dim objER
    Dim strTexto

    'Configurando o objeto de Expressão Regular
    Set objER            = New RegExp 
    objER.IgnoreCase    = True
    objER.Global        = True
    objER.Pattern        = "<[^>]*>"
    strTexto            = strHTML
    strTexto            = objER.Replace(strTexto, "")
    Set objER            = Nothing
   
	strTexto = replace(strTexto,"  "," ")
	strTexto = replace(strTexto,"&nbsp;"," ")
	strTexto = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(trim(strTexto),"-",""),"/",""),".",""),",",""),"""",""),"'",""),"<P>",""),"<SPAN",""),"style=",""),"</STRONG>","")
	strTexto = replace(replace(replace(replace(replace(replace(trim(strTexto),"<FONT>",""),"color=",""),"COLOR:",""),"<BR>",""),vbcrlf,""),"&quot;","")
	strTexto = replace(trim(strTexto),"*"," ")
	strTexto = replace(trim(strTexto),chr(13)," ")
	strTexto = replace(trim(strTexto),chr(10)," ")

	RemoveHTMLTags = strTexto
	
	' response.end
End Function
' =========================================================================



' 2 - META DADOS ============================================
Function MetaData(nProduto)
	if(nProduto = "") then  exit function
	
	dim BreadCrumb_Detalhes_produto, sMetaData, sPalavrasChaves, sDescricaoCat, sTitle

	sMetaData = sMetaData & ""

	if(not IsNumeric(nProduto) and Left(nProduto, 1) = "C") then 
		' PARA CATEGORIAS ****************************************************************************
		sPalavrasChaves = trim(Recupera_Campos_Db("TB_CATEGORIAS", right(nProduto,len(nProduto)-1), "id", "palavraschaves"))
		sDescricaoCat	= trim(Recupera_Campos_Db("TB_CATEGORIAS", right(nProduto,len(nProduto)-1), "id", "categoria_desc"))
		sTitle			= trim(Recupera_Campos_Db("TB_CATEGORIAS", right(nProduto,len(nProduto)-1), "id", "title"))
		
		if(sPalavrasChaves <> "" and not isnull(sPalavrasChaves)) then 
			BreadCrumb_Detalhes_produto = sPalavrasChaves
		else
			BreadCrumb_Detalhes_produto = "Produtos para você: CDs, DVDs, Revistas e Livros em lançamento e edições especiais. descontos e ofertas especiais na "& Application("NomeLoja") &"."
		end if
		
		if(sTitle <> "" and not isnull(sTitle)) then 
			sMetaData = sMetaData & "<title>" & sTitle & "</title>" & vbcrlf
		else
			sMetaData = sMetaData & "<title>" & Application("NomeLoja") & "</title>" & vbcrlf
		end if
		
		' sMetaData = sMetaData &	"	<meta http-equiv=""Content-Type"" content=""text/html; charset=iso-8859-1"">" & vbcrlf
        sMetaData = sMetaData &	"	<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">" & vbcrlf
		
		If(sDescricaoCat <> "" And Not IsNull(sDescricaoCat)) Then 
			sMetaData = sMetaData &	"<meta name=""description"" content="""& RemoveHTMLTags(sDescricaoCat) &""">" & vbcrlf
		else
			sMetaData = sMetaData &	"<meta name=""description"" content=""Clique aqui e veja todas as novidades que a "& Application("NomeLoja") &" traz para você!"">" & vbcrlf
		End if

		sMetaData =	sMetaData &	"	<meta name=""keywords"" content="""&	replace(BreadCrumb_Detalhes_produto," ,","") & """>" & vbcrlf

		if (sFabricantes <> "") then
			sMetaData =	sMetaData &	"<link rel=""canonical"" href="/""& sLinkhttp & "especiais/"& trim(replace(replace(Recupera_Campos_Db_oConn("TB_FABRICANTE", sFabricantes, "id", "Fabricante"),"/","-"), " ","-")) &"-"& sFabricantes &"""/>" & vbcrlf
		else
			sMetaData =	sMetaData &	"<link rel=""canonical"" href="/""& sLinkhttp & trim(lcase(replace(replace(sNomeCategoria,"/","-"), " ","-")) &"-"& replace(nProduto,"C","")) &"""/>" & vbcrlf		
		end if
		' sMetaData =	sMetaData &	"	<meta name=""google-site-verification"" content=""OBgPrCBtqESxywy8fW1vxDD6Ke4HKDK6HTC0uScTiGU"" />" & vbcrlf
	else
		if(not IsNumeric(nProduto) and Left(nProduto, 1) = "F") then 
			' PARA FABRICANTES ****************************************************************************
			sPalavrasChaves = trim(Recupera_Campos_Db("TB_FABRICANTE", right(nProduto,len(nProduto)-1), "id", "palavraschaves"))
			sDescricaoCat	= trim(Recupera_Campos_Db("TB_FABRICANTE", right(nProduto,len(nProduto)-1), "id", "Fabricante_desc"))
			sTitle			= trim(Recupera_Campos_Db("TB_FABRICANTE", right(nProduto,len(nProduto)-1), "id", "title"))
			
			if(sPalavrasChaves <> "" and not isnull(sPalavrasChaves)) then 
				BreadCrumb_Detalhes_produto = sPalavrasChaves
			'else
				'BreadCrumb_Detalhes_produto = "livros, novidades, dvds, cds, moda"
			end if
			
			if(sTitle <> "" and not isnull(sTitle)) then 
				sMetaData = sMetaData & "<title>" & sTitle & "</title>" & vbcrlf
			else
				sMetaData = sMetaData & "<title>" & Application("NomeLoja") & "</title>" & vbcrlf
			end if
			
			sMetaData = sMetaData &	"	<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">" & vbcrlf
			
			If(sDescricaoCat <> "" And Not IsNull(sDescricaoCat)) Then 
				sMetaData = sMetaData &	"<meta name=""description"" content="""& RemoveHTMLTags(sDescricaoCat) &""">" & vbcrlf
			else
				sMetaData = sMetaData &	"<meta name=""description"" content=""Clique aqui e veja todas as novidades que a "& Application("NomeLoja") &" traz para você!"">" & vbcrlf
			End if
	
			sMetaData =	sMetaData &	"	<meta name=""keywords"" content="""&	replace(BreadCrumb_Detalhes_produto," ,","") & """>" & vbcrlf

			if (sFabricantes <> "") then
				sMetaData =	sMetaData &	"<link rel=""canonical"" href="/""& sLinkhttp & "marcas/"& trim(replace(replace(Recupera_Campos_Db_oConn("TB_FABRICANTE", sFabricantes, "id", "Fabricante"),"/","-"), " ","-")) &"-"& sFabricantes &"""/>" & vbcrlf
			else
				sMetaData =	sMetaData &	"<link rel=""canonical"" href="/""& sLinkhttp & trim(lcase(replace(replace(sNomeCategoria,"/","-"), " ","-")) &"-"& replace(nProduto,"C","")) &"""/>" & vbcrlf		
			end if
			' sMetaData =	sMetaData &	"	<meta name=""google-site-verification"" content=""OBgPrCBtqESxywy8fW1vxDD6Ke4HKDK6HTC0uScTiGU"" />" & vbcrlf
		else
			if(not IsNumeric(nProduto) and Left(nProduto, 1) = "L") then 
				' PARA FABRICANTES ****************************************************************************
				sPalavrasChaves = trim(Recupera_Campos_Db("TB_FABRICANTE", right(nProduto,len(nProduto)-1), "id", "palavraschaves"))
				sDescricaoCat	= trim(Recupera_Campos_Db("TB_FABRICANTE", right(nProduto,len(nProduto)-1), "id", "Fabricante_desc"))
				sTitle			= trim(Recupera_Campos_Db("TB_FABRICANTE", right(nProduto,len(nProduto)-1), "id", "title"))
				
				if(sPalavrasChaves <> "" and not isnull(sPalavrasChaves)) then 
					BreadCrumb_Detalhes_produto = sPalavrasChaves
				else
					BreadCrumb_Detalhes_produto = "Produtos para você: CDs, DVDs, Revistas e Livros em lançamento e edições especiais. descontos e ofertas especiais na "& Application("NomeLoja") &"."
				end if
				
				if(sTitle <> "" and not isnull(sTitle)) then 
					sMetaData = sMetaData & "<title>" & sTitle & "</title>" & vbcrlf
				else
					sMetaData = sMetaData & "<title>" & Application("NomeLoja") & "</title>" & vbcrlf
				end if
				
				sMetaData = sMetaData &	"	<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">" & vbcrlf
				
				If(sDescricaoCat <> "" And Not IsNull(sDescricaoCat)) Then 
					sMetaData = sMetaData &	"<meta name=""description"" content="""& RemoveHTMLTags(sDescricaoCat) &""">" & vbcrlf
				else
					sMetaData = sMetaData &	"<meta name=""description"" content=""Clique aqui e veja todas as novidades que a "& Application("NomeLoja") &" traz para você!"">" & vbcrlf
				End if

				sMetaData =	sMetaData &	"	<meta name=""keywords"" content="""&	replace(BreadCrumb_Detalhes_produto," ,","") & """>" & vbcrlf
	
				if (sFabricantes <> "") then
					sMetaData =	sMetaData &	"<link rel=""canonical"" href="/""& sLinkhttp & "marcas/"& trim(replace(replace(Recupera_Campos_Db_oConn("TB_FABRICANTE", sFabricantes, "id", "Fabricante"),"/","-"), " ","-")) &"-"& sFabricantes &"""/>" & vbcrlf
				else
					sMetaData =	sMetaData &	"<link rel=""canonical"" href="/""& sLinkhttp & "lista/" & trim(lcase(replace(replace(sNomeCategoria,"/","-"), " ","-")) &"-"& replace(sIdSub,"C","")) &"""/>" & vbcrlf
				end if
				' sMetaData =	sMetaData &	"	<meta name=""google-site-verification"" content=""OBgPrCBtqESxywy8fW1vxDD6Ke4HKDK6HTC0uScTiGU"" />" & vbcrlf
			else
				if(not IsNumeric(nProduto) and Left(nProduto, 1) = "S") then 
					' PARA FABRICANTES ****************************************************************************
					sPalavrasChaves = trim(Recupera_Campos_Db("TB_SUBCATEGORIA", right(nProduto,len(nProduto)-1), "id", "palavraschaves"))
					sDescricaoCat	= trim(Recupera_Campos_Db("TB_SUBCATEGORIA", right(nProduto,len(nProduto)-1), "id", "SubCategoria_Desc"))
					sTitle			= trim(Recupera_Campos_Db("TB_SUBCATEGORIA", right(nProduto,len(nProduto)-1), "id", "title"))
					
					if(sPalavrasChaves <> "" and not isnull(sPalavrasChaves)) then 
						BreadCrumb_Detalhes_produto = sPalavrasChaves
					else
						BreadCrumb_Detalhes_produto = "Produtos para você: CDs, DVDs, Revistas e Livros em lançamento e edições especiais. Ddescontos e ofertas especiais na "& Application("NomeLoja") &"."
					end if
					
					if(sTitle <> "" and not isnull(sTitle)) then 
						sMetaData = sMetaData & "<title>" & sTitle & "</title>" & vbcrlf
					else
						sMetaData = sMetaData & "<title>" & Application("NomeLoja") & "</title>" & vbcrlf
					end if
					
					sMetaData = sMetaData &	"	<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">" & vbcrlf
					
					If(sDescricaoCat <> "" And Not IsNull(sDescricaoCat)) Then 
						sMetaData = sMetaData &	"<meta name=""description"" content="""& RemoveHTMLTags(sDescricaoCat) &""">" & vbcrlf
					else
						sMetaData = sMetaData &	"<meta name=""description"" content=""Clique aqui e veja todas as novidades que a "& Application("NomeLoja") &" traz para você!"">" & vbcrlf
					End if
	
					sMetaData =	sMetaData &	"	<meta name=""keywords"" content="""&	replace(BreadCrumb_Detalhes_produto," ,","") & """>" & vbcrlf
		
					if (sFabricantes <> "") then
						sMetaData =	sMetaData &	"<link rel=""canonical"" href="/""& sLinkhttp & "marcas/"&  TrataSEO(Recupera_Campos_Db_oConn("TB_FABRICANTE", sFabricantes, "id", "Fabricante")) &"-"& sFabricantes &"""/>" & vbcrlf
					else
						sMetaData =	sMetaData &	"<link rel=""canonical"" href="/""& sLinkhttp & "lista/" &  TrataSEO(sNomeSubCategoria) &"-"& replace(sIdSub,"C","") &"""/>" & vbcrlf
					end if
					' sMetaData =	sMetaData &	"	<meta name=""google-site-verification"" content=""OBgPrCBtqESxywy8fW1vxDD6Ke4HKDK6HTC0uScTiGU"" />" & vbcrlf
				else
					' PARA PRODUTOS ****************************************************************************
					sPalavrasChaves = trim(Recupera_Campos_Db("TB_PRODUTOS", nProduto, "id", "palavraschaves"))
					sPalavrasChaves = trim(Recupera_Campos_Db("TB_PRODUTOS", nProduto, "id", "palavraschaves"))
					sTitle			= trim(Recupera_Campos_Db("TB_PRODUTOS", nProduto, "id", "tituloseo"))
					
					if(trim(sTitle) = "" or isnull(sTitle) ) then 
						sTitle = sNomeProduto & " - R$" & sPreco
					end if
					
					if(sPalavrasChaves <> "" and not isnull(sPalavrasChaves)) then 
						BreadCrumb_Detalhes_produto = sPalavrasChaves
					else
						BreadCrumb_Detalhes_produto = sNomeCategoria & ", "  & sNomeSubCategoria & ", "  & sNomeSubCategoria2 & ", "  & sNomeSubCategoria3  & ", "  &  sNomeSubCategoria4 & ", "  &   sNomeSubCategoria5  & ", "  & sNomeSubCategoria6  & ", "  & sNomeSubCategoria7 &	sNomeProduto
					end if
					
					sMetaData = sMetaData & "<title>" & sTitle & "</title>" & vbcrlf
                    sMetaData = sMetaData &	"	<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">" & vbcrlf
					sMetaData = sMetaData &	"	<meta name=""description"" content=""" 	&	left(replace(replace(replace(RemoveHTMLTags(sDescricaoCompleta),"<",""),">",""),"</FONT>",""),157)	&	"..."">" & vbcrlf
					sMetaData =	sMetaData &	"	<meta name=""keywords"" content=""" 		&	replace(BreadCrumb_Detalhes_produto," ,","") & """>" & vbcrlf
					sMetaData =	sMetaData &	"	<link rel=""canonical"" href="/""& sLinkhttp & "detalhes/"&TrataSEO(trim(Recupera_Campos_Db_Oconn("TB_PRODUTOS", nProduto, "id", "Descricao")))&"-"& nProduto &"""/>" & vbcrlf
					' sMetaData =	sMetaData &	"	<meta name=""google-site-verification"" content=""OBgPrCBtqESxywy8fW1vxDD6Ke4HKDK6HTC0uScTiGU"" />" & vbcrlf
				end if
			end if
		end if
	end if
	
	sMetaData	= replace(replace(replace(replace(replace(replace(replace(replace(replace(sMetaData,  "<br>",  ""),"<p>",""),"</p>",""),"</br>",""),"<BR>",""),"<P>",""),"</P>",""),"",""),"","") 	
	MetaData  	= sMetaData
End Function
' ===========================================================================
function mostrar_desconto_produto(prId)
	if (prId = "" and isnull(prId)) then exit function
	
	dim sFSql, sFRs
	Dim sFDescontoIgnorar, sFPromocao, sFid_categoria, sFid_subcategoria, sFDataIniPromo, sFDataFimPromo, BoolDataPromocao
	
	mostrar_desconto_produto =  false
	
	call abre_conexao()
	
	sFSql 		= "SELECT DescontoIgnorar, Promocao, id_categoria, id_subcategoria, DataIniPromo, DataFimPromo FROM TB_PRODUTOS (NOLOCK) WHERE ID = " & prId
	
	set sFRs 	= oConn.execute(sFSql)
	
	if (not sFRs.eof) then
		sFDescontoIgnorar 	= sFRs("DescontoIgnorar")
		sFPromocao		  	= sFRs("Promocao")
		sFid_categoria		= sFRs("id_categoria")
		sFid_subcategoria	= sFRs("id_subcategoria")
		sFDataIniPromo		= sFRs("DataIniPromo")
		sFDataFimPromo		= sFRs("DataFimPromo")
		
		if(sFDataIniPromo <> "" and not isnull(sFDataIniPromo)) then    BoolDataPromocao    = ((cdate(sFDataIniPromo) <= Date) and (cDate(sFDataFimPromo) >= Date))
		if (not BoolDataPromocao) then                                  Session("d" & prId) = 0

		if (	((trim(sFPromocao) = 1 and BoolDataPromocao) or (Session("CupomGet") <> "") ) 				AND _
				(		(Session("Desconto") <> "" and Session("Desconto") <> "0")							OR _
						(Session("d" & prId) <> "" and Session("d" & prId) <> "0")							OR _
						(Session("dc" & prId) <> "" and Session("dc" & prId) <> "0")               			OR _
						(VerificaDesconto(sFid_categoria,sFid_subcategoria))									)) then
						mostrar_desconto_produto =  true
		end if
	end if
	set sFRs = nothing
	call fecha_conexao()
end function

' ===========================================================================
function Recupera_Codigo_Abacos(prProduto , PrTamanho, PrCor, PrSabor, PrTipo)
	dim sSQlVa, RsVa
	
		call Abre_Conexao()

		sSQlVa = "select " &_
					"AX.ID_TAMANHO, " &_
					"AX.ID_COR, " &_
					"AX.CODABACOS " &_
				"from " &_
					"TB_IMAGENS_AUXILIARES AX (NOLOCK) "
					
                if (PrTamanho <> "")    then sSQlVa = sSQlVa & "INNER JOIN TB_TAMANHOS  T (NOLOCK) 	ON T.ID = AX.ID_TAMANHO AND T.CDCLIENTE = "& sCdCliente &" AND T.DESCRICAO = '"& PrTamanho &"'"
				if (PrCor <> "")        then sSQlVa = sSQlVa & "INNER JOIN TB_CORES     C (NOLOCK) 	ON C.ID = AX.ID_COR 	AND C.CDCLIENTE = "& sCdCliente &" AND C.DESCRICAO = '"& PrCor &"'"
                if (PrSabor <> "")      then sSQlVa = sSQlVa & "INNER JOIN TB_SABORES   S (NOLOCK) 	ON S.ID = AX.ID_SABOR 	AND S.CDCLIENTE = "& sCdCliente &" AND S.DESCRICAO = '"& PrSabor &"'"
                if (PrTipo <> "")       then sSQlVa = sSQlVa & "INNER JOIN TB_TIPOS     P (NOLOCK) 	ON P.ID = AX.ID_TIPO 	AND P.CDCLIENTE = "& sCdCliente &" AND P.DESCRICAO = '"& PrTipo &"'"

				sSQlVa = sSQlVa & "WHERE AX.ID_PRODUTO = "& prProduto

        'response.write sSQlVa
        'response.end
		Set RsVa = oConn.execute(sSQlVa)
		
		if(not RsVa.eof) then Recupera_Codigo_Abacos = rsVa("CODABACOS")
		Set RsVA = nothing
		call fecha_conexao()	
end function


' ===========================================================================
Function Retorna_Parcelamento(prValor)
	Dim lContP, nDividido, nQuantidadeAux

	for lContP = sParcelaMinima to 1 Step -1
		nDividido = (prValor/ lContP)
		
		if(cdbl(nDividido) >= cdbl(sValorParcelaMinima)) then 
			nQuantidadeAux = lContP
			exit for
		end if
	next
	
	Retorna_Parcelamento = nQuantidadeAux
end function
'========================================================================
Function Mostra_Anuncio()
	dim sConteudo, sSecao,  sAcao
	
	call Abre_Conexao()
	
	sSQl = "Select " & _
				" C.Categoria,  " & _
				" C.Ativo,  " & _
				" C.Tipo, " &_
				" C.ID, " &_
				" I.Conteudo, " &_
				"NewID = NEWID()  " &_ 
			" from  " &_
				" TB_INSTITUCIONAL I (NOLOCK) " &_
				" INNER JOIN TB_INSTITUCIONAL_CATEGORIAS C (NOLOCK) ON C.ID = I.ID_CATEGORIA AND C.CDCLIENTE = "& sCdCliente &" AND TIPO = 'A' " &_
			" where   " &_
				"	C.ATIVO = 1 " &_
				
			" order by  NewId()   "
	
	set Rs = oConn.execute(sSQl)
	
	if(not RS.eof) then
		sAnucio = RS("Conteudo")
		
		do while not rs.eof
		
		if (sAnucio <> "") then
			sConteudo = " "& sAnucio &" "
		end if	
			
		rs.movenext
		loop
	end if
	
	set Rs = nothing
	call Fecha_Conexao()

	Mostra_Anuncio = sConteudo
	
end function


'========================================================================
function RetornaCasasDecimais(sCampo, nTamanho)
    dim Formatar, lCont, lContVirgula, sLetra
    
    if(sCampo = "") then 
		exit function
    end if

    if(sCampo = "0") then 
		RetornaCasasDecimais = FormatNumber(sCampo,2)
        exit function
    end if
    
	sCampo = formatNumber(sCampo, 3)
	
    lContVirgula = 0
    
    for lCont = 1 to len(sCampo)
          sLetra = mid(sCampo, lCont, 1)

          if(sLetra <> ",") then
                if(lContVirgula = 3) then 
					RetornaCasasDecimais = Formatar
                    exit function
                end if
                
                Formatar = (Formatar & sLetra)
                if(lContVirgula > 0) then lContVirgula = (lContVirgula + 1)
          else
                Formatar = (Formatar & sLetra)
                lContVirgula = (lContVirgula + 1)
          end if
    next
    
    RetornaCasasDecimais = Formatar
end function


' =======================================================================
function BoolChecaAba(prTipo)

	BoolChecaAba = false
	if(prTipo = "") then exit function
	
	dim sSQlBool, RsCheca
	
	select case prTipo
		case "1"
			sSQlBool = "Select count(*) as total " & _
					"from  " & _
						"TB_PRODUTOS (NOLOCK)"  & _
							" LEFT JOIN TB_MOEDAS (NOLOCK) on  TB_PRODUTOS.Id_Moeda = TB_MOEDAS.id " & _
							" INNER JOIN TB_CATEGORIAS (NOLOCK) on  TB_PRODUTOS.Id_Categoria = TB_CATEGORIAS.Id and TB_CATEGORIAS.ativo = 1 AND TB_CATEGORIAS.Id_Tipo = 'S' " & _
							" INNER JOIN TB_SUBCATEGORIA (NOLOCK) on TB_CATEGORIAS.Id = TB_SUBCATEGORIA.Id_Categoria and TB_SUBCATEGORIA.ativo = 1 and TB_PRODUTOS.Id_SubCategoria = TB_SUBCATEGORIA.Id " & _
					"where " & _
						"TB_PRODUTOS.cdcliente = "& sCdCliente &" and " & _
						"TB_PRODUTOS.Lancamento = 1 and  " & _
						"TB_PRODUTOS.Ativo = 1 and  " & _
						"(getdate() between TB_PRODUTOS.DataIniLan and TB_PRODUTOS.DataFimLan) and  " & _
						"(TB_PRODUTOS.boolMostra is null or TB_PRODUTOS.boolMostra = '' or TB_PRODUTOS.boolMostra = 'p') and  " & _
						"TB_MOEDAS.Ativo = 1 "
						if (InSTr(lcase(Request.ServerVariables("script_name")), "default.asp") <> 0) then sSQlBool = sSQlBool & " and TB_PRODUTOS.id_subcategoria not in (4495,4496) and TB_PRODUTOS.id_categoria not in (845)"
						if(Replace_Caracteres_Especiais(Request("cat")) <> "") then sSQlBool = sSQlBool & " AND TB_PRODUTOS.id_categoria = " & Replace_Caracteres_Especiais(Request("cat"))
						if(Replace_Caracteres_Especiais(Request("sub")) <> "") then sSQlBool = sSQlBool & " AND TB_PRODUTOS.id_subcategoria = " & Replace_Caracteres_Especiais(Request("sub"))
						if(Replace_Caracteres_Especiais(Request("Id_Fabricante")) <> "") then 
							if(Replace_Caracteres_Especiais(Request("Id_Fabricante")) = "740") then 
								sSQlBool = sSQlBool & " AND TB_PRODUTOS.Id_Fabricante in (704, 714, 725, 730, 732) "
							else
								sSQlBool = sSQlBool & " AND TB_PRODUTOS.Id_Fabricante = " & Replace_Caracteres_Especiais(Request("Id_Fabricante"))
							end if
						end if
						
						If(oConn.State = "0") then call Abre_Conexao()
						
						set RsCheca = oConn.execute(sSQlBool)
						
						if(not RsCheca.Eof) then
							if(RsCheca("total") = 0) then 
								if(Replace_Caracteres_Especiais(Request("Id_Fabricante")) <> "" and isnumeric(Replace_Caracteres_Especiais(Request("Id_Fabricante")))) then
									sSQlBool = " Select  count(*) as total " & _
															"from " & _
																"TB_PRODUTOS (nolock) " & _
																"INNER JOIN TB_MOEDAS (NOLOCK) on TB_PRODUTOS.Id_Moeda = TB_MOEDAS.id " & _
																"INNER JOIN TB_FABRICANTES_AUXILIARES (NOLOCK) on TB_FABRICANTES_AUXILIARES.Id_Fabricante = "& Replace_Caracteres_Especiais(Request("Id_Fabricante")) &" AND TB_PRODUTOS.Id = TB_FABRICANTES_AUXILIARES.Id_Produto " & _
															"where " & _
																"TB_PRODUTOS.cdcliente = "& sCdCliente &" and " & _
																"(TB_PRODUTOS.boolMostra = '' or TB_PRODUTOS.boolMostra is null or TB_PRODUTOS.boolMostra = 'p') and " & _
																"TB_PRODUTOS.Ativo = 1 and " & _
																"(isnull(TB_PRODUTOS.Imagem,'') <> '') and  " & _
																"TB_PRODUTOS.Lancamento = 1 and  " & _
																"(getdate() between TB_PRODUTOS.DataIniLan and TB_PRODUTOS.DataFimLan) and " & _
																"TB_MOEDAS.Ativo = 1 "
								end if
							end if
						end if
						' UNION PARA SELECIONAR PRODUTO DAS TABELAS AUXILIARES ===============================
						
						sSQlBool = sSQlBool & " group by  " & _
							" TB_PRODUTOS.Id, TB_PRODUTOS.Descricao, TB_PRODUTOS.Destaque, TB_PRODUTOS.Imagem, TB_PRODUTOS.DataIniLan, TB_PRODUTOS.DataFimLan, TB_PRODUTOS.DataIniLan, " & _
							" TB_PRODUTOS.DataFimLan, TB_PRODUTOS.DataIniDesa, TB_PRODUTOS.DataFimDesa, TB_PRODUTOS.Lancamento, TB_PRODUTOS.id_categoria, TB_PRODUTOS.id_subcategoria, TB_PRODUTOS.id_Fabricante, " & _
							" TB_PRODUTOS.DescontoValor, TB_PRODUTOS.DescontoTipo, TB_PRODUTOS.Preco_Consumidor, TB_PRODUTOS.Preco_Aprazo, TB_PRODUTOS.Qte_Parcelamento, TB_PRODUTOS.Qte_Estoque, TB_PRODUTOS.DescontoIgnorar, " & _
							" TB_PRODUTOS.peso, TB_MOEDAS.Moeda, TB_MOEDAS.Simbolo "
'				response.write sSQlBool
'				response.end
		case "2"
			sSQlBool = "Select count(*) as total " & _
				"from  " & _
					"TB_PRODUTOS (NOLOCK)"  & _
						" INNER JOIN TB_MOEDAS (NOLOCK) on  TB_PRODUTOS.Id_Moeda = TB_MOEDAS.id " & _
						" INNER JOIN TB_CATEGORIAS (NOLOCK) on  TB_PRODUTOS.Id_Categoria = TB_CATEGORIAS.Id and TB_CATEGORIAS.ativo = 1 AND TB_CATEGORIAS.Id_Tipo = 'S' " & _
						" INNER JOIN TB_SUBCATEGORIA (NOLOCK) on TB_CATEGORIAS.Id = TB_SUBCATEGORIA.Id_Categoria and TB_SUBCATEGORIA.ativo = 1 and TB_PRODUTOS.Id_SubCategoria = TB_SUBCATEGORIA.Id " & _
				"where " & _
					"TB_PRODUTOS.cdcliente = "& sCdCliente &" and   " & _
					"TB_PRODUTOS.Promocao = 1 and  " & _
					"TB_PRODUTOS.Ativo = 1 and  " & _
					"(getdate() between TB_PRODUTOS.DataIniPromo and TB_PRODUTOS.DataFimPromo) and  " & _
					"(TB_PRODUTOS.boolMostra is null or TB_PRODUTOS.boolMostra = '' or TB_PRODUTOS.boolMostra = 'p') and  " & _
					"(isnull(TB_PRODUTOS.Imagem,'') <> '') and  " & _
					"TB_MOEDAS.Ativo = 1 " 
				if(Replace_Caracteres_Especiais(Request("cat")) <> "") then sSQlBool = sSQlBool & " AND TB_PRODUTOS.id_categoria = " & Replace_Caracteres_Especiais(Request("cat"))
				if(Replace_Caracteres_Especiais(Request("sub")) <> "") then sSQlBool = sSQlBool & " AND TB_PRODUTOS.id_subcategoria = " & Replace_Caracteres_Especiais(Request("sub"))
				if(Replace_Caracteres_Especiais(Request("Id_Fabricante")) <> "") then 
					if(Replace_Caracteres_Especiais(Request("Id_Fabricante")) = "740") then 
						sSQlBool = sSQlBool & " AND TB_PRODUTOS.Id_Fabricante in (704, 714, 725, 730, 732) "
					else
						sSQlBool = sSQlBool & " AND TB_PRODUTOS.Id_Fabricante = " & Replace_Caracteres_Especiais(Request("Id_Fabricante"))
					end if
				end if
			
				If(oConn.State = "0") then call Abre_Conexao()

				set RsCheca = oConn.execute(sSQlBool)
				
				if(not RsCheca.Eof) then
					if(RsCheca("total") = 0) then 
						if(Replace_Caracteres_Especiais(Request("Id_Fabricante")) <> "" and isnumeric(Replace_Caracteres_Especiais(Request("Id_Fabricante")))) then
							sSql_P = sSql_P & " UNION ALL "
							sSql_P = sSql_P & "Select " & _
												"TB_PRODUTOS.Id,  " & _
												"TB_PRODUTOS."& sCampoNome &",  " & _
												"TB_PRODUTOS."& sCampoDestaque &",  " & _
												"TB_PRODUTOS.Imagem,  " & _
												"TB_PRODUTOS.DataIniLan,  " & _
												"TB_PRODUTOS.DataFimLan,  " & _
												"TB_PRODUTOS.DataIniPromo,  " & _
												"TB_PRODUTOS.DataFimPromo,  " & _
												"TB_PRODUTOS.DataIniDesa,  " & _
												"TB_PRODUTOS.DataFimDesa,  " & _
												"TB_PRODUTOS.DataIniPreVenda,  " & _
												"TB_PRODUTOS.DataFimPreVenda,  " & _
												"TB_PRODUTOS.PreVenda,  " & _
												"TB_PRODUTOS.UrlCompra,  " & _
												"TB_PRODUTOS.Promocao,  " & _
												"TB_PRODUTOS.id_categoria,  " & _
												"TB_PRODUTOS.id_subcategoria,  " & _
												"TB_PRODUTOS.id_Fabricante,  " & _
												"isnull(TB_PRODUTOS.id_tipos, '') id_tipos,  " & _
												"TB_PRODUTOS.DescontoValor,  " & _
												"TB_PRODUTOS.DescontoTipo,  " & _
												"TB_PRODUTOS."& sCampoPreco &" as Preco,  " & _
												"TB_PRODUTOS.Preco_Aprazo,  " & _
												"TB_PRODUTOS.Qte_Parcelamento,  " & _
												"TB_PRODUTOS.Qte_Estoque,  " & _
												"TB_PRODUTOS.DescontoIgnorar,  " & _
												"TB_PRODUTOS.peso,  " & _
												"TB_MOEDAS.Moeda,  " & _
												"TB_MOEDAS.Simbolo,  " & _
												"NewID = NEWID()  " 
							sSql_P = sSql_P & "from "
							sSql_P = sSql_P & "TB_PRODUTOS (nolock) "
								sSql_P = sSql_P & "INNER JOIN TB_MOEDAS (NOLOCK) on TB_PRODUTOS.Id_Moeda = TB_MOEDAS.id "
								sSql_P = sSql_P & "INNER JOIN TB_FABRICANTES_AUXILIARES (NOLOCK) on TB_FABRICANTES_AUXILIARES.Id_Fabricante = "& Replace_Caracteres_Especiais(Request("Id_Fabricante")) &" AND TB_PRODUTOS.Id = TB_FABRICANTES_AUXILIARES.Id_Produto "
							sSql_P = sSql_P & "where "
								sSql_P = sSql_P & "TB_PRODUTOS.cdcliente = "& sCdCliente &" and "
								sSql_P = sSql_P & "(TB_PRODUTOS.boolMostra = '' or TB_PRODUTOS.boolMostra is null or TB_PRODUTOS.boolMostra = 'p') and "
								sSql_P = sSql_P & "(getdate() between TB_PRODUTOS.DataIniPromo and TB_PRODUTOS.DataFimPromo) and "
								sSql_P = sSql_P & "TB_PRODUTOS.Ativo = 1 and "
								sSql_P = sSql_P & "TB_PRODUTOS.Promocao = 1 and  "
								sSql_P = sSql_P & "TB_MOEDAS.Ativo = 1 "
						end if
						' UNION PARA SELECIONAR PRODUTO DAS TABELAS AUXILIARES ===============================

					end if
				end if

				sSQlBool = sSQlBool & " group by  " & _
							" TB_PRODUTOS.Id, TB_PRODUTOS.Descricao, TB_PRODUTOS.Destaque,  TB_PRODUTOS.DataIniDesa, TB_PRODUTOS.DataFimDesa, TB_PRODUTOS.DescontoTipo, " & _
							" TB_PRODUTOS.Imagem, TB_PRODUTOS.DataIniLan, TB_PRODUTOS.DataFimLan, " & _ 
							" TB_PRODUTOS.DataIniPromo, TB_PRODUTOS.DataFimPromo, TB_PRODUTOS.Promocao,  " & _
							" TB_PRODUTOS.id_categoria, TB_PRODUTOS.id_subcategoria, TB_PRODUTOS.Id_Fabricante, TB_PRODUTOS.DescontoValor,  " & _
							" TB_PRODUTOS.Preco_Consumidor, TB_PRODUTOS.Preco_Aprazo, TB_PRODUTOS.Qte_Parcelamento, TB_PRODUTOS.Qte_Estoque, TB_MOEDAS.Moeda, TB_PRODUTOS.peso, TB_PRODUTOS.DescontoIgnorar,  " & _
							" TB_MOEDAS.Simbolo "
		case "3"
			sSQlBool = _
					" Select distinct " & _
					" count(PI.Id_Produto) as total, PI.Id_Produto " & _
				" from  " & _
					" TB_PEDIDOS_ITENS PI (NOLOCK) " & _
					" INNER JOIN TB_PEDIDOS P1 (nolock) ON PI.Id_Pedido = P1.ID "& _
					" INNER JOIN TB_PRODUTOS P (nolock) ON PI.Id_Produto = P.ID and p.ativo = 1 and P.cdcliente = "& sCdCliente & _
					" INNER JOIN TB_CATEGORIAS C (nolock) ON P.Id_Categoria = C.ID AND C.ATIVO = 1 AND C.ID_TIPO = 'S' AND C.cdcliente = "& sCdCliente  & _
				" where " & _
					" P1.datacad between getdate()-30 and getdate() "
					if(Replace_Caracteres_Especiais(Request("Id_Fabricante")) <> "") then 
						if(Replace_Caracteres_Especiais(Request("Id_Fabricante")) = "740") then 
							sSQlBool = sSQlBool & " AND P.Id_Fabricante in (704, 714, 725, 730, 732) "
						else
							sSQlBool = sSQlBool & " AND P.Id_Fabricante = " & Replace_Caracteres_Especiais(Request("Id_Fabricante"))
						end if
					end if
					if(Replace_Caracteres_Especiais(Request("cat")) <> "") then sSQlBool = sSQlBool & " AND p.id_categoria = " & Replace_Caracteres_Especiais(Request("cat"))
					if(Replace_Caracteres_Especiais(Request("sub")) <> "") then sSQlBool = sSQlBool & " AND p.id_subcategoria = " & Replace_Caracteres_Especiais(Request("sub"))
					
					sSQlBool = sSQlBool & " group by PI.Id_Produto"  & _
										" order by  " & _
										" count(PI.Id_Produto) desc "
	end select
	
	call Abre_Conexao()

	set RsCheca = oConn.execute(sSQlBool)

	if(not RsCheca.Eof) then
		if(RsCheca("total") > 0) then 
			BoolChecaAba = true
		end if
	end if
	
	set RsCheca = nothing
	call Fecha_Conexao()
end function

' =======================================================================
Sub Valida_Desconto_Cupom(prCupomGet)

	if(prCupomGet = "") then exit sub

	'if(InStr(Request.ServerVariables("script_name"),"lista.asp") > 0) then  exit sub
	'if(InStr(Request.ServerVariables("script_name"),"secao.asp") > 0) then  exit Sub
	'if(InStr(Lcase(Request.ServerVariables("script_name")),"resultado_busca.asp") > 0) then  exit sub
	
	dim RsVale, sSQlVale, boolValidado
	dim sPedidoUtilizado, sTipoDesconto, sTipoDescontoValor, sProdutosCupom, sArrayProdutosCupom, boolValidadoProduto, sArrayProdutosCupom2
	dim sPrecoProdutoComDesconto
	
	call Abre_Conexao()
	
    ' ZERAR PRODUTOS DE OUTROS CUPONS -------------------------------------------
    sSql = "Select id from TB_PRODUTOS (NOLOCK) where cdcliente = "& sCdCliente &" AND ATIVO = 1 order by id"
    set RsCombo = oConn.execute(sSQL)
	if(not RsCombo.eof) then 
		do while not RsCombo.eof
	        if(Session("dc" & trim(RsCombo("id"))) <> "" and not isnull(Session("dc" & trim(RsCombo("id"))))) then
                Session("dc" & trim(RsCombo("id"))) = 0		
            end if
		RsCombo.movenext
		loop
	end if
	set RsCombo = nothing
    ' ZERAR PRODUTOS DE OUTROS CUPONS -------------------------------------------

	sSQlVale = "SELECT NumeroPedidoUtilizado,TipoDesconto,ProdutosDesconto, Valor, TipoDescontoValor FROM TB_CUPONS (NOLOCK) WHERE Cupom = '"& prCupomGet &"' " & _ 
				" AND (getdate() between DataValIni and DataValFim) " & _ 
				" AND (SubString(Cupom, 1, 1) = 'C' OR SubString(Cupom, 1, 8) = 'CLJABRIL') " & _ 
				" AND  CDCLIENTE = " & sCdCliente
    set RsVale = oConn.execute(sSQlVale)

	sProdutosCupom = ""

	if(not RsVale.eof) then 
		sPedidoUtilizado        = trim(RsVale("NumeroPedidoUtilizado"))	' VERIFICAR SE O CUPOM JA TEM PEDIDO UTILIZADO
		sTipoDesconto           = trim(RsVale("TipoDesconto"))			' TIPO DO DESCONTO - MULTI COMPRAS OU UNITARIO
		sProdutosCupom          = trim(RsVale("ProdutosDesconto"))		' VERIFICAR SE O CUPOM SO FUNCIONA COM ALGUNS PRODUTOS
        sTipoDescontoValor      = trim(RsVale("TipoDescontoValor"))

		' PEGA TODOS OS PRODUTOS DA BASE PQ O CUPOM SERVIRA PARA TODOS OS PRODUTOS ATUAIS
		if(sProdutosCupom = "" or isnull(sProdutosCupom)) then
            sSql = "Select id from TB_PRODUTOS (NOLOCK) where cdcliente = "& sCdCliente &" AND ATIVO = 1 and isnull(descontoignorar,'') <> 1 and qte_estoque > 0 order by id"
            set RsCombo = oConn.execute(sSQL)

		    if(not RsCombo.eof) then 
			    do while not RsCombo.eof
				    sProdutosCupom = (sProdutosCupom & "," & trim(RsCombo("id")))
			   RsCombo.movenext
			   loop
		    end if

		    if (left(sProdutosCupom,1) = ",") then
			    sProdutosCupom = right(sProdutosCupom, len(sProdutosCupom)-1)
		    end if
		    if (right(sProdutosCupom,1) = ",") then
			    sProdutosCupom = left(sProdutosCupom, len(sProdutosCupom)-1)
		    end if
		    set RsCombo = nothing
		else
			 Session("ProdutosDesconto") = sProdutosCupom
		end if
		
		' UNICA COMPRA ********************
		if(sTipoDesconto = "1") then 
			if(sPedidoUtilizado = "" or isnull(sPedidoUtilizado)) then
				boolValidado = true
			else
				boolValidado = false
				exit sub
			end if
		else
			' MUILTI COMPRA ********************
			boolValidado = true
		end if

		if(sProdutosCupom <> "" AND not isnull(sProdutosCupom) and sProdutosCupom <> "0") then
			Session("Desconto") 	= 0
			Session("dc" & Replace_Caracteres_Especiais(request("produto"))) = 0
			
			sProdutosCupomFuncoes	= sProdutosCupom
			sProdutosCupom  		= (trim(sProdutosCupom) & ",")
			sArrayProdutosCupom 	= split(sIdProdutoCesta, ",")
			sArrayProdutosCupom2 	= split(sProdutosCupom, ",")
			boolValidado 			= false
			
			if(ubound(sArrayProdutosCupom) > 0) then 
				' LISTANDO TODOS OS PRODUTOS DO CUPOM ******************************
				for lContArray = 0 to ubound(sArrayProdutosCupom)
					if(sArrayProdutosCupom(lContArray) <> "") then 
						if(InStr(1, sProdutosCupom, sArrayProdutosCupom(lContArray)) > 0) then
							boolValidado = true
							exit for
						end if
					end if
				next
			end if
			
			' response.write boolValidado
			if(InStr(Lcase(Request.ServerVariables("script_name")),"detalhes.asp") > 0 				OR _ 
				InStr(Lcase(Request.ServerVariables("script_name")),"lista.asp") > 0 				OR _
				InStr(Lcase(Request.ServerVariables("script_name")),"default.asp") > 0 				OR _
				InStr(Lcase(Request.ServerVariables("script_name")),"resultado_busca.asp") > 0 		OR _
				InStr(Lcase(Request.ServerVariables("script_name")),"secao.asp") > 0) then

				if(InStr(1, sProdutosCupom, Replace_Caracteres_Especiais(request("produto"))) > 0 	OR _ 
					InStr(Lcase(Request.ServerVariables("script_name")),"lista.asp") > 0  			OR _
					InStr(Lcase(Request.ServerVariables("script_name")),"secao.asp") > 0 			OR _
					InStr(Lcase(Request.ServerVariables("script_name")),"resultado_busca.asp") > 0 	OR _
					InStr(Lcase(Request.ServerVariables("script_name")),"default.asp") > 0) then

					' boolValidado = true 
					Session("Desconto") 	= 0
					Session("CupomGet")		= prCupomGet
					sValorPresente 			= RetornaCasasDecimais(trim(RsVale("Valor")),2)

					if(ucase(sTipoDescontoValor) = "P") then
						dim sValorProdutoReal, BoolDataPromocao, sValorDescontoProduto
                        sSql = "Select id,promocao,DataIniPromo,DataFimPromo,DescontoValor,PRECO_CONSUMIDOR,descontoignorar from TB_PRODUTOS (NOLOCK) where cdcliente = "& sCdCliente &" AND ATIVO = 1 and isnull(descontoignorar,'') <> 1 and qte_estoque > 0 and id in ("& left(sProdutosCupom, len(sProdutosCupom)-1) &") order by id"
                        set RsCombo = oConn.execute(sSQL)

		                if(not RsCombo.eof) then 
			                do while not RsCombo.eof
								    sValorProdutoReal = trim(RsCombo("PRECO_CONSUMIDOR"))
                                    if(trim(RsCombo("promocao")) = "1") then 
									    if(trim(RsCombo("DataIniPromo")) <> "" and not isnull(trim(RsCombo("DataIniPromo"))) )  then 
										    BoolDataPromocao = ( (cdate(trim(RsCombo("DataIniPromo"))) <= Date) and (cDate(trim(RsCombo("DataFimPromo"))) >= Date) )
									    end if
									    if(BoolDataPromocao) then
										    sValorDescontoProduto 	= trim(RsCombo("DescontoValor"))
										    sValorProdutoReal 		= (cdbl(sValorProdutoReal) - cdbl(sValorDescontoProduto))
									    end if
								    end if
								    if(trim(RsCombo("descontoignorar")) = "0" OR isnull(trim(RsCombo("descontoignorar")))) then
									    Session("dc" & trim(RsCombo("id"))) = cdbl(sValorProdutoReal) * (cdbl(sValorPresente) / 100)
								    end if
			                RsCombo.movenext
			               loop
		                end if
					else
						for lContArray = 0 to ubound(sArrayProdutosCupom2)
							if(sArrayProdutosCupom2(lContArray) <> "") then 
								Session("dc" & trim(sArrayProdutosCupom2(lContArray))) = cdbl(sValorPresente)
							end if
						next
					end if
					Session("CupomGet")		= prCupomGet
	                set RsVale = nothing
	                call Fecha_Conexao()
					exit sub
					
				end if
			end if
		else
            boolValidado        = false
            Session("Desconto") = 0
        end if

		if(boolValidado) then
			sValorPresente 	     = RetornaCasasDecimais(cdbl(trim(Recupera_Campos_Db("TB_CUPONS", "'" & prCupomGet & "'", "Cupom", "valor"))),2)
			Session("Desconto") = sValorPresente
			Session("CupomGet")	= prCupomGet
			' response.write Session("Desconto")
		end if
    end if

	set RsVale = nothing
	call Fecha_Conexao()
end sub
' =======================================================================
function Monta_Avaliacao_Produto(prProduto)
	if(prProduto = "") then exit function
	
	dim sSql, Rs, sTotal, sTotalGeral, sAvaliacao, sTotalP, sDescricao
	dim sSqlA, RsA
	
	call Abre_Conexao()
	
	
	sSQl = "Select " & _
				" count(*) as totalGeral  " & _
			" from  " & _
				" TB_PRODUTOS_AVALIACAO (nolock) " & _ 
			" where   " & _
				" ID_PRODUTO = "& prProduto & _ 
				" AND CDCLIENTE = "& sCdCliente & _ 
				" AND ATIVO = 1 "
	set Rs = oConn.execute(sSQl)
	

	if(not Rs.eof and Rs("totalGeral")  > 0 ) then
		
		sTotalGeral	= Rs("totalGeral") 

		sConteudo = sConteudo & "<table cellpadding=""3"" cellspacing=""3"" border=""1"" width=""350"">" & vbcrlf
		sConteudo = sConteudo & "	<tr>" & vbcrlf
		sConteudo = sConteudo & "		<td colspan=""3"">Avaliação geral dos clientes: <img src=""/css/imagem/logos/logo_abril_a"&Monta_Total_Arvores(prProduto)&".gif"" width=""50"">&nbsp;<strong>"& sTotalGeral &"</strong> "
		if (sTotalGeral > 1) then
			sConteudo = sConteudo & "avaliações </td>" & vbcrlf
		else
			sConteudo = sConteudo & "avaliação </td>" & vbcrlf
		end if
		sConteudo = sConteudo & "	</tr>" & vbcrlf
		
		call Abre_Conexao()
		sSqlA = "Select " & _
				"  PA.Avaliacao,  " & _
				"  count(*) as total  " & _
			" from  " & _
				"  TB_PRODUTOS_AVALIACAO PA (nolock)  " & _ 
			" where   " & _
				"  PA.ID_PRODUTO = "& prProduto & _ 
				"  AND PA.ATIVO = 1 " & _ 				
			" group by " &_
				" PA.Avaliacao " & _
			"  order by PA.Avaliacao desc"
			set RsA = oConn.execute(sSqlA)

				if(not RsA.eof) then
	
					sTotalP = 0
					do while (not RsA.eof)
		
						sTotal			= RsA("total") 
						sAvaliacao 		= RsA("Avaliacao") 
						sTotalP			= cInt(sTotal / sTotalGeral * 100)

						Select Case sAvaliacao
							Case "5"
								sDescricao = "Excelente"
							Case "4"
								sDescricao = "Ótimo"
							Case "3"
								sDescricao = "Bom"
							Case "2"
								sDescricao = "Regular"
							Case "1"
								sDescricao = "Ruim"
						end select

						
						sConteudo = sConteudo & "	<tr>" & vbcrlf
						sConteudo = sConteudo & "		<td width=""70"" >"& sTotal &" / "&  sTotalP  &"%</td>" & vbcrlf
						sConteudo = sConteudo & "		<td width=""100"" style=""float:left;margin:0px;"" ><div class=""AvaliacaoOn"" style=""width:"&  sTotalP  &"px;background:#efefef;"">&nbsp;&nbsp;<span style=""position:absolute;"">"&sDescricao&"</span></div></td>" & vbcrlf
						sConteudo = sConteudo & "		<td width=""200"" ><img src=""/css/imagem/logos/logo_abril_a"& sAvaliacao &".gif"" alt=""Abril"" ></td>" & vbcrlf
						sConteudo = sConteudo & "	</tr>" & vbcrlf		

					RsA.movenext
					loop
				end if
		sConteudo = sConteudo & "</table>"
	end if
	
	set Rs = nothing
	call Fecha_Conexao()

	Monta_Avaliacao_Produto = sConteudo
end function
' ==========================================================
function Monta_Avaliacao_Cientes(prProduto)
	if(prProduto = "") then exit function
	
	dim sSQl, Rs, sConteudo, sCliente, sCidade, sNomeProduto, sData, sTitulo, sComentario, sAvaliacao

	call Abre_Conexao()
	
	sSQl = "Select " & _
				" C.id,  " & _
				" C.Nome,  " & _
				" C.Cidade,  " & _
				" A.ID_CLIENTE,  " & _
				" A.ID_PRODUTO,  " & _
				" A.CDCLIENTE,  " & _
				" A.Titulo,  " & _
				" A.DataCad,  " & _
				" A.Comentario,  " & _
				" A.Avaliacao  " & _
			" from  " & _
				" TB_PRODUTOS_AVALIACAO A(nolock) " & _ 
				" INNER JOIN TB_CLIENTES C (nolock) ON C.ID = A.ID_CLIENTE " & _ 
			" where   " & _
				" A.ID_PRODUTO = "& prProduto & _ 
				" AND A.CDCLIENTE = "& sCdCliente & _ 
				" AND A.ATIVO = 1 " & _
			" order by A.DataCad desc"

	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(not Rs.Eof) then
		Rs.PageSize 	= 5
		Rs.CacheSize 	= Rs.PageSize
		iPageCount 		= Rs.PageCount
		iRecordCount 	= Rs.RecordCount
		Rs.AbsolutePage = iQualPagina
		iExibe 			= 1
		
		Do While (Not Rs.Eof) and (iExibe <= 5)
			iExibe = iExibe + 1
			sCliente		=	Ucase(Rs("Nome"))
			sCidade			=	Ucase(Rs("Cidade"))
			sData			=	FormatDateTime(Rs("DataCad"),2)
			sTitulo			=	Rs("Titulo")
			sComentario		=	replace(Rs("Comentario"), vbcrlf , "<br>")
			sAvaliacao		=	cInt(Rs("Avaliacao"))

 		
			sConteudo = sConteudo & "<article>" & vbcrlf
			sConteudo = sConteudo & "		<p><span style=""float:left"">" & vbcrlf
            sConteudo = sConteudo & "		"& sCliente &"</span><span class=""right""> <time datetime="""& sData &""">"& sData &"</time></span></p>" & vbcrlf

			sConteudo = sConteudo & "			<span class=""ob"">"& sTitulo &"<br>"&sComentario&"</span>" & vbcrlf
			sConteudo = sConteudo & "		</article>" & vbcrlf
			'sConteudo = sConteudo & "<label><span class="subt">Achou este comentário útil?</span></label>"
		    'sConteudo = sConteudo & "		<label><a href="/#" class="subt">Sim</a> (10)</label> / <label><a href="/#" class="subt">Não</a> (1)</label>" & vbcrlf
            'sConteudo = sConteudo & "		</li>" & vbcrlf
            'sConteudo = sConteudo & "		<p><hr></p>" & vbcrlf

		rs.movenext
		loop
		
		' PAGINACAO ******************
		If (iQualPagina > 0) Then
			if(iPageCount > 1) then
				sConteudo = sConteudo & ("   <div id=""paginacaoRodape2"">")
				sConteudo = sConteudo & ("		<p> Página:")
				sConteudo = sConteudo & ("		"& Gera_Paginas() &"")
				sConteudo = sConteudo & ("		</p>")		
				sConteudo = sConteudo & ("   </div>	")
			end if
		End If		
		
	end if
	
	set Rs = nothing
	call Fecha_Conexao()

	Monta_Avaliacao_Cientes = sConteudo
end function
' ==========================================================
function Monta_Total_Avaliacao(prProduto)
	dim Rs, sSQl
	
	call Abre_Conexao()
	
	sSQL = "Select count(id) as total from TB_PRODUTOS_AVALIACAO (NOLOCK) WHERE CDCLIENTE = "&sCdCliente&" and  ID_PRODUTO = " & prProduto & " and ativo = 1"
	set Rs = oConn.execute(sSQL)
	if(not rs.eof) then Monta_Total_Avaliacao = rs("total")
	set Rs = nothing
	
	call Fecha_Conexao()
	
	set Rs = nothing
end function
' ==========================================================
function Monta_Total_Arvores(prProduto)
	if(prProduto = "") then exit function
	
	dim sSql, Rs, sTotal, sAvaliacao, sTotalP, sDescricao, sTotalEstrela, sTotalEstrelaT
	call Abre_Conexao()

	sSql = "Select sum(PA.avaliacao) as totalI from TB_PRODUTOS_AVALIACAO PA (nolock) where PA.ID_PRODUTO = "& prProduto &" AND PA.ATIVO = 1"
	set Rs = oConn.execute(sSql)
	if(not Rs.eof) then
		sTotalP = 0
		sSQL1 = "Select count(id) as total from TB_PRODUTOS_AVALIACAO (nolock) WHERE ID_PRODUTO = "& prProduto &" AND ATIVO = 1"
		set Rs1 = oConn.execute(sSQL1)
			if(not Rs1.eof) then
			sTotal			= Rs1("total") 
			sAvaliacao 		= Rs("totalI") 
			sTotalP			= cInt(sAvaliacao / sTotal )
			Rs.movenext
			end if
	end if
	Monta_Total_Arvores = sTotalP
	set Rs = nothing

end function
' ==========================================================
function Monta_Itens_Kit(prProduto)
	if(prProduto = "") then exit function
	
	call Abre_Conexao()
	
	dim sSQl, Rs, sConteudo, sDe, sAte
	
	sSQl = "Select " & _
				" PO.DESCRICAO,  " & _
				" P.* " & _
			" from  " & _
				" TB_PRODUTOS_BUNDLE P (nolock) " & _ 
				" INNER JOIN TB_PRODUTOS PO (nolock) ON PO.ID = P.ID_PRODUTO_BUNDLE " & _ 
			" where   " & _
				" P.ID_PRODUTO = "& prProduto & _
			" order by  " & _
				" PO.DESCRICAO "
	set Rs = oConn.execute(sSQl)
	
	if(not rs.eof) then
		do while not rs.eof
			sConteudo = sConteudo & "<li>" & trim(rs("descricao")) &" - Quantidade: "& trim(rs("quantidade")) &"<br>"
		rs.movenext
		loop
	end if
	
	set Rs = nothing
	call Fecha_Conexao()

	Monta_Itens_Kit = sConteudo
end function


' ==========================================================
function Verifica_Subs_Ativas(prProduto)
	
	Verifica_Subs_Ativas = false
	
	if(prProduto = "") then exit function

	if(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria2")) <> "" and trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria2")) <> "0" and not isnull(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria2")))) then 
		if(trim(Recupera_Campos_Db_oConn("TB_SUBCATEGORIA2", trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria2")), "id", "ativo")) = "0") then 
			exit function
		end if
	end if
	if(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria3")) <> "" and trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria3")) <> "0" and not isnull(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria3")))) then 
		if(trim(Recupera_Campos_Db_oConn("TB_SUBCATEGORIA3", trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria3")), "id", "ativo")) = "0") then 
			exit function
		end if
	end if
	if(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria4")) <> "" and trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria4")) <> "0" and not isnull(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria4")))) then 
		if(trim(Recupera_Campos_Db_oConn("TB_SUBCATEGORIA4", trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria4")), "id", "ativo")) = "0") then 
			exit function
		end if
	end if
	if(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria5")) <> "" and trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria5")) <> "0" and not isnull(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria5")))) then 
		if(trim(Recupera_Campos_Db_oConn("TB_SUBCATEGORIA5", trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria5")), "id", "ativo")) = "0") then 
			exit function
		end if
	end if
	if(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria6")) <> "" and trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria6")) <> "0" and not isnull(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria6")))) then 
		if(trim(Recupera_Campos_Db_oConn("TB_SUBCATEGORIA6", trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria6")), "id", "ativo")) = "0") then 
			exit function
		end if
	end if
	if(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria7")) <> "" and trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria7")) <> "0" and not isnull(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria7")))) then 
		if(trim(Recupera_Campos_Db_oConn("TB_SUBCATEGORIA7", trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Id_SubCategoria7")), "id", "ativo")) = "0") then 
			exit function
		end if
	end if
	
	Verifica_Subs_Ativas = true
end function

' =======================================================================
function Alterar_Tamanho_Imagem(prIni, PrPasta1, PrPasta2, PrImagem, width, height)
	dim sConteudo, sNomeNovo, sSqlI
	dim Jpeg, PathLer, Pathgravar, boolExecutar, nomeArquivo
	
	if(PrImagem = "" or isnull(PrImagem)) then 
		Alterar_Tamanho_Imagem = (sLinkImagem & "EcommerceNew/upload/produto/sem_imagem.jpg")
		exit function
	end if

	boolExecutar = false

    PathLer     = "\\BZNGNGJMCMIAV0R\EcommerceNew\upload\produto"
    Pathgravar  = "\\BZNGNGJMCMIAV0R\EcommerceNew\upload\produto"
	sNomeNovo   = lcase(("str_" & prIni & left(PrImagem, len(PrImagem)-4) & ".jpg"))

	' --------------------------------------------------------------------------
	Set ScriptObject = Server.CreateObject("Scripting.FileSystemObject")

	If(prIni = "") Then 
		boolExecutar = ScriptObject.FileExists(Pathgravar & "\" & sNomeNovo)
	Else
		boolExecutar = ScriptObject.FileExists(Pathgravar & "\" & prIni & "\" & sNomeNovo)
	End if
    Set ScriptObject = nothing


	if(not boolExecutar) then
	    On Error Resume Next

		'Response.Write (PathLer & "\" & PrImagem) & "<br>"
		'Response.End

        Set Jpeg = Server.CreateObject("Persits.Jpeg")
		Jpeg.Open (PathLer & "\" & PrImagem)
		Jpeg.PreserveAspectRatio = True 		' Verificando a proporção da imagem
		Jpeg.Width 		= width
		Jpeg.Height 	= height
		Jpeg.Quality 	= 100

		If(prIni = "") Then 
			Jpeg.Save (Pathgravar & "\" & sNomeNovo)
		Else
			Jpeg.Save (Pathgravar & "\" & prIni & "\" & sNomeNovo)
		End if
		Set Jpeg = nothing
    end if
	
	'if(Err.Number <> "0") then 
    if(true) then 
        ' call Envia_Email("edneyrulli@yahoo.com.br", "sistema01@foodcrowd.com.br", "sistema01@foodcrowd.com.br", "Erro Criar Imagem", Err.Description)
        ' Alterar_Tamanho_Imagem = (sLinkImagem & "EcommerceNew/upload/produto/sem_imagem.jpg")
        ' Alterar_Tamanho_Imagem = ""
        ' response.write PrImagem & "<br>"
        Alterar_Tamanho_Imagem = (sLinkImagem & "EcommerceNew/upload/produto/" & PrImagem)
	else
        'sConteudo = ("images/imgProd/" & prIni & "/" & sNomeNovo)

        sConteudo = (sLinkImagem & "EcommerceNew/upload/produto/"& prIni &"/" & sNomeNovo)
		Alterar_Tamanho_Imagem = sConteudo
	end if
	
	on error goto 0
end function

' ------------------------------------------------------------------------
function Mostra_Historico(prPedido)

	dim sConteudo, Rs, sSql, sCampoPrecoA
	dim sCampo(15), sCampoNomeA, sCampoDestaqueA
		
	call Abre_Conexao()

	sSql = "SELECT distinct " & _
				" P.DataCad as DataP, H.DATACAD AS DATAO, " & _
				" Isnull(P.Rastreamento, '') as Rastreamento, " & _
				" Isnull(P.RastreamentoColeta, '') as RastreamentoColeta, " & _
				" H.DESCRICAO, " & _
				" H.Id_Status as Id_StatusH, " & _
				" Volumes = (SELECT SUM(QUANTIDADE) FROM TB_PEDIDOS_ITENS PT WHERE PT.ID_PEDIDO = P.ID) " & _
			" from  " & _
				" TB_PEDIDOS P (NOLOCK) " & _
				" INNER JOIN TB_CLIENTES C ON C.ID = P.ID_CLIENTE " & _
				" INNER JOIN TB_STATUS S ON S.ID = P.ID_STATUS " & _
				" INNER JOIN TB_HISTORICOS H ON P.ID = H.ID_PEDIDO " & _
			" WHERE  1=1 and "
	if(prPedido <> "") then sSql = sSql & " P.ID = "& prPedido &" "
	sSql = sSql & " order by H.DATACAD DESC "
	
	Set Rs = oConn.execute(sSql)

	sConteudo = "<table width=""100%"" border=""0"" vspace=""0"" hspace=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"">"
	sConteudo = sConteudo & "  <tr> "
	sConteudo = sConteudo & "    <td valign=""top""> "
	sConteudo = sConteudo & "      <div align=""center""><b><font size=""2"" face=""Tahoma"">Pedido</b></div>"
	sConteudo = sConteudo & "    </td>"
	sConteudo = sConteudo & "    <td valign=""top""> "
	sConteudo = sConteudo & "      <div align=""center""><b><font size=""2"" face=""Tahoma"">Operação</b></div>"
	sConteudo = sConteudo & "    </td>"
	sConteudo = sConteudo & "    <td valign=""top""> "
	sConteudo = sConteudo & "      <div align=""center""><b><font size=""2"" face=""Tahoma"">Status</b></div>"
	sConteudo = sConteudo & "    </td>"
	sConteudo = sConteudo & "    <td valign=""top""> "
	sConteudo = sConteudo & "      <div align=""center""><b><font size=""2"" face=""Tahoma"">Rastreio</b></div>"
	sConteudo = sConteudo & "    </td>"
	sConteudo = sConteudo & "    <td valign=""top""> "
	sConteudo = sConteudo & "      <div align=""center""><b><font size=""2"" face=""Tahoma"">Volume</b></div>"
	sConteudo = sConteudo & "    </td>"
	sConteudo = sConteudo & "  </tr>"
	
	if(not rs.eof) then
		do while not rs.eof
			sCampo(0) = trim(rs("DataP"))
			sCampo(1) = trim(rs("DESCRICAO"))
			sCampo(2) = trim(rs("DATAO"))
			sCampo(3) = trim(rs("Volumes"))
			sCampo(4) = trim(rs("Rastreamento"))
			sCampo(5) = trim(rs("Id_StatusH"))
			sCampo(6) = trim(rs("RastreamentoColeta"))

			if(sCampo(5) <> "04" AND sCampo(5) <> "06") then sCampo(4) = ""
			if(sCampo(5) = "42") then sCampo(4) = sCampo(6)
			
			sConteudo = sConteudo & "  <tr> "
			sConteudo = sConteudo & "    <td class=""a11zul"">"& sCampo(0) &"</td>"
			sConteudo = sConteudo & "    <td class=""a11zul"">"& sCampo(2) &"</td>"
			sConteudo = sConteudo & "    <td class=""a11zul"">"& sCampo(1) &"</td>"
			sConteudo = sConteudo & "    <td class=""a11zul"">"& sCampo(4) &"</td>"
			sConteudo = sConteudo & "    <td class=""a11zul"" align=center>"& sCampo(3) &"</td>"
			sConteudo = sConteudo & "  </tr>"
		rs.movenext
		loop
	else
		sConteudo = sConteudo & "  <tr> "
		sConteudo = sConteudo & "    <td colspan=""5"" align=""center""><font size=""2"" face=""Tahoma"">Pedido nao encontrado!</td>"
		sConteudo = sConteudo & "  </tr>"
	end if
	Set Rs = nothing
	
	call Fecha_Conexao()

	sConteudo = sConteudo & "</table>"

	Mostra_Historico = sConteudo
end function


' ========================================================================
' MONTA COMBO TIPO DATA DIA, MES E ANO SEPARADOS =========================
function Monta_Data(sTipo, sName, sValor, sOnchange)
	dim sConteudo, lCont, Sel1, sTemp
	
	sConteudo = sConteudo &"<select name="""& sName &""" "& sOnchange &">"
	
	Select case sTipo
		case "d"
  			if(sValor <> "") then sTemp = day(sValor) 
			
			for lCont = 0 to 31
				if(sValor <> "") then 
					Sel1 = ""
					if(cint(sTemp) = cint(lCont)) then Sel1 = " selected "
				end if 

				if(lCont > 0) then sConteudo = sConteudo & "<option value="""& right("00" & lCont, 2) &""" "& Sel1 &">"& right("00" & lCont, 2) &"</option>" & vbcrlf
			next
		case "m"
			if(sValor <> "") then sTemp = month(sValor)
			
			for lCont = 1 to 12
				if(sValor <> "") then 
					Sel1 = ""
					if(cint(sTemp) = cint(lCont)) then Sel1 = " selected "
				end if 
				sConteudo = sConteudo &"<option value="""& right("00" & lCont, 2) &""" "& Sel1 &">"& right("00" & lCont, 2) &"</option>" & vbcrlf
			next

		case "a"
			if(sValor <> "") then sTemp = year(sValor)
			
			for lCont = 1900 to 2010
				if(sValor <> "") then 
					Sel1 = ""
					if(cint(sTemp) = cint(lCont)) then Sel1 = " selected "
				end if 
				sConteudo = sConteudo &"<option value="""& lCont &""" "& Sel1 &">"& lCont &"</option>" & vbcrlf
			next
	end select

	sConteudo = sConteudo &"</select>"
	Monta_Data = sConteudo
end function


' ==========================================================
function fBoolAI(sInt)
	if(sInt = "A") then
		fBoolAI = "<font color=""blue"">Ativo</font>"
	elseif(sInt = "I") then
		fBoolAI = "<font color=""#06528E"">Inativo</font>"
	end if
end function
' =======================================================================
function FormatData(v1)
	dim xdia, xmes, xano
	
	xdia = right("00" & day(v1),2)
	xmes = right("00" & month(v1),2)
	xano = right("0000" & year(v1),4)

	FormatData = xano & xmes & xdia
end function


' =======================================================================
sub Gera_Cupom(prPedido, prCliente, prProduto)
	if(prPedido = "" or prProduto = "") then exit sub
	
	if(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "ValePresente")) = "1") then
		dim sSQl, sValor, sCupons
		
		sValor = replace(replace(RetornaCasasDecimais(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Preco_Consumidor"),2),".",""),",",".")

		sCupons = ("C" & right(("00000" & prCliente),5) & right(("00000" & prPedido),5) & right(("0000000000" & prProduto), 10) & right(("00000" & replace(replace(RetornaCasasDecimais(Recupera_Campos_Db_oConn("TB_PRODUTOS", prProduto, "id", "Preco_Consumidor"),2),",",""),".","")),5))
		
		sSQl = "Insert into TB_CUPONS (CdCliente, Cupom, Valor, NumeroPedido, ClienteComprou) values " & _ 
				"("& sCdCliente &", '"& sCupons &"', '"& sValor &"', '"& prPedido &"', '"& prCliente &"')"
		oConn.execute(sSQl)
	end if
end sub


' =======================================================================
sub Grava_Estatisticas_Banners(IdCliente)

	if(IdCliente = "" or not isnumeric(IdCliente)) then exit sub
	
	dim sSql, sData, sIp
	
	call Abre_Conexao()

	sData = (right("00" & day(date),2) & "/" & right("00" & month(date),2) & "/" & year(date) &" "& time())
	sIp = Request.Servervariables("REMOTE_ADDR")
	
	sSQl = "Insert into TB_ESTAT_BANNER (Id_Banner, DataCad, Nm_Ip) values " & _ 
			"("& IdCliente &", '"& sData &"', '"& sIp &"')"
	oConn.execute(sSQl)

	call Fecha_Conexao()
end sub


' =======================================================================
function Retorna_Vitrine_Randomica(sTipo)
	dim sConteudo, Rs, sSql, sCampoDescIdioma, sComboPeriodo
	dim sCampo(15), sData, sPrecoPrazo, sParcelamento
	
	sData = (day(now) & "/" & month(now) & "/" & year(now))
	
	if(sTipo = "") then exit function
		
	Select case sTipo
		case "1" ' LANCAEMTNOS
			sSql = "Select top 1 p.id as idproduto, * from TB_PRODUTOS P (nolock) "  & _
					" INNER JOIN TB_CATEGORIAS C (nolock) ON P.Id_Categoria = C.ID AND C.ATIVO = 1 AND c.Id_Tipo = 'S' " & _
					" where P.Lancamento = 1 and P.ativo = 1 and p.cdcliente = "& sCdcliente &" order by newId() "
		case "2" ' PROMOCAO
			sSql = "Select top 1 p.id as idproduto,* from TB_PRODUTOS P (nolock) "  & _
					" INNER JOIN TB_CATEGORIAS C (nolock) ON P.Id_Categoria = C.ID AND C.ATIVO = 1 AND c.Id_Tipo = 'S' " & _
					" where P.Promocao = 1 and P.ativo = 1 and  p.cdcliente = "& sCdcliente &" order by newId() "
		case "3" ' MAIS VENDIDO
			sSql = _
				" Select distinct " & _
					" count(PI.Id_Produto) as total, PI.Id_Produto as idproduto " & _
				" from  " & _
					" TB_PEDIDOS_ITENS PI (nolock) " & _
					" INNER JOIN TB_PRODUTOS P (nolock) ON PI.Id_Produto = P.ID  and p.cdcliente = "& sCdcliente &" " & _
					" INNER JOIN TB_CATEGORIAS C (nolock) ON P.Id_Categoria = C.ID AND C.ATIVO = 1 AND c.Id_Tipo = 'S' " & _
				" group by  " & _
					" PI.Id_Produto"  & _
				" order by  " & _
					" count(PI.Id_Produto) desc "
	end select
	
	call Abre_Conexao()
	Set Rs = oConn.execute(sSql)
	
	if(not rs.eof) then
		if(sTipo <> "3") then 
			sCampo(0) = rs("idproduto")
			sCampo(1) = rs("Descricao")
			sCampo(2) = RetornaCasasDecimais(rs("Preco_Consumidor"),2)
			sCampo(3) = rs("Imagem")
			sCampo(4) = rs("id_categoria")
			sCampo(5) = rs("id_subcategoria")
			sCampo(6) = rs("promocao")
			sCampo(7) = rs("DescontoValor")
			sPrecoPrazo = RetornaCasasDecimais(rs("preco_aprazo"),2)
			sParcelamento = trim(rs("Qte_Parcelamento"))
			sMontaValor = cdbl(sCampo(2))
		else
			sCampo(0) = rs("IdProduto")
			sCampo(1) = Recupera_Campos_Db("TB_PRODUTOS", sCampo(0), "id", "Descricao")
			sCampo(2) = RetornaCasasDecimais(Recupera_Campos_Db("TB_PRODUTOS", sCampo(0), "id", "Preco_Consumidor"),2)
			sCampo(3) = Recupera_Campos_Db("TB_PRODUTOS", sCampo(0), "id", "Imagem")
			sCampo(4) = Recupera_Campos_Db("TB_PRODUTOS", sCampo(0), "id", "id_categoria")
			sCampo(5) = Recupera_Campos_Db("TB_PRODUTOS", sCampo(0), "id", "id_subcategoria")
			sCampo(6) = Recupera_Campos_Db("TB_PRODUTOS", sCampo(0), "id", "promocao")
			sCampo(7) = Recupera_Campos_Db("TB_PRODUTOS", sCampo(0), "id", "DescontoValor")
			sPrecoPrazo = RetornaCasasDecimais(Recupera_Campos_Db("TB_PRODUTOS", sCampo(0), "id", "Preco_APrazo"),2)
			sParcelamento = Recupera_Campos_Db("TB_PRODUTOS", sCampo(0), "id", "Qte_Parcelamento")
			sMontaValor = cdbl(sCampo(2))
		end if
		
		if(trim(sCampo(6)) = "1") then 
			if(trim(sCampo(7)) <> "") then 
				Session("d" & sCampo(0)) = cdbl(sCampo(7))
			end if
		else
			Session("d" & sCampo(0)) = 0
		end if
		
		if(sCampo(3) <> "") then 
			sCampo(3) = "<img src="""& sLinkImagem &"/EcommerceNew/upload/produto/"& sCampo(3) &""" alt=""Miniaturas 100x110"" width=""100"" height=""110"" border=""0"">"
		else
			sCampo(3) = "<img src="""& sLinkImagem &"/EcommerceNew/upload/produto/img_indisponivel.jpg"" alt=""Miniaturas 100x110"" width=""100"" height=""110"" border=""0"">"
		end if
			
		sConteudo = sConteudo & "<td rowspan=""2"" valign=""top"" nowrap><a href="/"/detalhes.asp?produto="& sCampo(0) &""">"& sCampo(3) &"</a></td>"
		sConteudo = sConteudo & "<td align=""left"" valign=""top"" nowrap>"
		
			sConteudo = sConteudo & "<table width=""100%"" height=""80%"" border=""0"" cellpadding=""0"" cellspacing=""0"">"
			sConteudo = sConteudo & "<tr>"
			sConteudo = sConteudo & "	<td valign=""top"" width=""100"" nowrap><a href="/"/detalhes.asp?produto="& sCampo(0) &""" class=""LinkCinza"">"& sCampo(1) &"</a></td>"
			sConteudo = sConteudo & "</tr>"
			sConteudo = sConteudo & "<tr>"
			sConteudo = sConteudo & "	<td valign=""top"" nowrap>"
			
					if((Session("Desconto") <> "" and Session("Desconto") <> "0") OR _
					   ((trim(sCampo(6)) = "1") and (Session("d" & sCampo(0)) <> "" and Session("d" & sCampo(0)) <> "0") ) OR _
					   (VerificaDesconto(trim(sCampo(4)), trim(sCampo(5))))) then
						
						sConteudo = sConteudo & "<a href="/"detalhes.asp?produto="& sCampo(0) &""" class=""LinkVermelho""><s>"& sSimbolo &" "& sCampo(2) &"</s></a><br>"
						sMontaValor = RetornaCasasDecimais((cdbl(sMontaValor) - ((cdbl(Session("Desconto") + Session("Cat" & sCampo(4)) + Session("Sub" & sCampo(5)) + Session("d" & sCampo(0))) / 100) * cdbl(sMontaValor))),2)
						
						sConteudo = sConteudo & "<span class=""a11zul"">Desconto de: " & RetornaCasasDecimais(Session("Desconto") + Session("Cat" & sCampo(4)) + Session("Sub" & sCampo(5)) + Session("d" & sCampo(0)), 2) &"% </span><br>"
						sConteudo = sConteudo & "<span class=""stylered""><strong>Preco final: "& sSimbolo &" "& RetornaCasasDecimais(sMontaValor,2) &"</strong></span><br>"

						if(sPrecoPrazo > 0 and sParcelamento > 1) then 
							sConteudo = sConteudo & " ou <a href="/"detalhes.asp?produto="& sCampo(0) &""" class=""LinkVermelho"">"& sSimbolo &" "& sPrecoPrazo &" em "& sParcelamento &"x s/ <br>juros de R$ "& RetornaCasasDecimais((sPrecoPrazo / sParcelamento),2) &"</a><br>"
							if(sPrecoPrazo <> sCampo(2)) then sConteudo = sConteudo & "Desconto <strong>R$ "& RetornaCasasDecimais(sPrecoPrazo - sCampo(2), 2) &"</strong> pagamento a vista<br>"
						end if
					else
						if(sPrecoPrazo > 0 and sParcelamento > 1) then 
							sConteudo = sConteudo & "<a href="/"detalhes.asp?produto="& sCampo(0) &""" class=""LinkVermelho"">"& sSimbolo &" "& sPrecoPrazo &" em "& sParcelamento &"x s/ <br>juros de R$ "& RetornaCasasDecimais((sPrecoPrazo / sParcelamento),2) &"</a><br>"

							if(sPrecoPrazo <> sCampo(2)) then 
								sConteudo = sConteudo & "À Vista Ganhe Mais <br>"& RetornaCasasDecimais(((RetornaCasasDecimais(sPrecoPrazo - sCampo(2), 2) * 100) / sPrecoPrazo),2) &"% de Desconto "
								sConteudo = sConteudo & "<a href="/"detalhes.asp?produto="& sCampo(0) &""" class=""LinkVermelho"">"& sSimbolo &" "& sCampo(2) &"</a>"
							end if
							
						else
							sConteudo = sConteudo & "<a href="/"detalhes.asp?produto="& sCampo(0) &""" class=""LinkVermelho"">"& sSimbolo &" "& sCampo(2) &"</a><br>"
						end if
					end if
						
			sConteudo = sConteudo & "</td>"
			sConteudo = sConteudo & "</tr>"
			sConteudo = sConteudo & "<tr>"
			sConteudo = sConteudo & "	<td valign=""bottom"" nowrap>"
					if(Verifica_Status_Loja()) then sConteudo = sConteudo & "<br><a href="/"/cesta.asp?id="& sCampo(0) &"""><img src=""/img/butcomprar.png"" alt=""Miniaturas 100x110"" border=""0""><br></a>"
			sConteudo = sConteudo & "</td>"
			sConteudo = sConteudo & "</tr>"
			sConteudo = sConteudo & "</table>"

		sConteudo = sConteudo & "</td>"
	else
		sConteudo = sConteudo & "<td rowspan=""2"" valign=""top"" nowrap>&nbsp;</td>"
		sConteudo = sConteudo & "<td align=""left"" valign=""top"" width=""100"" nowrap>&nbsp;</td>"
	end if

	call Fecha_Conexao()

	Retorna_Vitrine_Randomica = sConteudo
end function


' =========================================================================
sub Gravar_Palavra_Chave(TxtPalavra)

	if(TxtPalavra = "") then exit sub
	
	dim sSql, sData, sIp, RS
	
	call Abre_Conexao()

	sIp = Request.Servervariables("REMOTE_ADDR")
	
	sSQl = "Insert into TB_PALAVRA_CHAVE (CdCliente, Descricao, Nm_Ip, OrigemLoja) values ('"& sCdCliente &"', '"& TxtPalavra &"', '"& sIp &"', '"& Application("NomeLoja") &"')"
	oConn.execute(sSQl)
end sub


' =========================================================================
function Verifica_Produto_Auxiliar(nProduto)
	dim Rs, sSQl, bool1, bool2
	
	Verifica_Produto_Auxiliar = false
	bool1 = false
	bool2 = false
	if(trim(nProduto) = "" or isnull(nProduto)) then exit function
	
	sSQl = "Select * from TB_IMAGENS_AUXILIARES where id_produto = "& nProduto
	set Rs = oConn.execute(sSQl)
	if(not rs.eof) then bool1 = true
	set Rs = nothing
	
	sSQl = "Select * from TB_PRODUTOS where " & _
				" ((Id_Tamanho <> '' ) OR  " & _
				" (Id_Cor <> '' ) OR  " & _
				" (Id_Sabor <> '' ) OR  " & _
				" (Id_Fragrancia <> '' ) OR " & _
				" (Id_Tipos <> '' ) OR  " & _
				" (Id_Pacote <> '')) AND  " & _
				" id = "& nProduto
	set Rs = oConn.execute(sSQl)
	if(not rs.eof) then bool2 = true
	set Rs = nothing
	
	if(bool2) then Verifica_Produto_Auxiliar = true
end function


' ================================================================
function LegendaNacionalidade(Texto)
	if(Isnull(Texto)) then exit function
	
	if(trim(lcase(Texto)) = "pt") then LegendaNacionalidade = "Brasileiro"
	if(trim(lcase(Texto)) = "jp") then LegendaNacionalidade = "Japon&ecirc;s"
	if(trim(lcase(Texto)) = "esp") then LegendaNacionalidade = "Outro - Especificar"
end function

' ================================================================
function FormataImagem(Texto)
	if(Isnull(Texto)) then exit function
	
	FormataImagem = Trata_Palavra(replace(replace(Texto," ",""),"-",""))
end function

' =======================================================================
function MostraValePresente(Cliente)
	dim x, sValorDesconto

	MostraValePresente = false
	
	call Abre_Conexao()
	
	sSql = "Select * from TB_VALE_PRESENTES_ATIVOS where isnull(Id_Pedido,'') = '' AND isnull(Utilizado,'') = '' AND Id_Cliente = " & Cliente
	Set Rs = oConn.execute(sSql)
	if(not rs.eof) then MostraValePresente = true
	Set Rs = nothing
	
	call Fecha_Conexao()
end function


' =======================================================================
Sub Notifica_Clientes_Lista_Esgotado(Produto)
	dim sEstoqueatual, boolMostra, Rs, sIdCate, sCorpo_Msg, sData, sProduto
	
	boolMostra = false
	sData = (day(now) & "/" & month(now) & "/" & year(now))

	call Abre_Conexao()
	
	if(Recupera_Campos_Db_oConn("TB_PRODUTOS", Produto, "id", "ativo") = "1") then	
		if(cint(Recupera_Campos_Db_oConn("TB_PRODUTOS", Produto, "id", "Qte_Estoque")) > 0) then
			sEstoqueatual = cint(Recupera_Campos_Db_oConn("TB_PRODUTOS", Produto, "id", "Qte_Estoque"))
			
			sSQL = "Select top "& sEstoqueatual &" * from TB_LISTA_ESGOTADOS where IdProduto = "& Produto &" and Notificado = 0 order by Id "
			set Rs = oConn.execute(sSQL)

			if(not rs.eof) then 
				do while not rs.eof
					sIdCate = rs("IdCliente")
					sProduto = Recupera_Campos_Db_oConn("TB_PRODUTOS", Produto, "id", "Descricao")
							
					sSQL = "Update TB_LISTA_ESGOTADOS set Notificado = 1, DataNotificado = '"& sData &"' where Id = "& rs("Id")
					oConn.execute(sSQL)
					
					sCorpo_Msg = Recupera_Style_Para_Email()
					sCorpo_Msg = sCorpo_Msg & "<table width=""500"" border=""0"" align=""center"" cellpadding=""1"" cellspacing=""1"">"
					sCorpo_Msg = sCorpo_Msg & "  <tr>"
					sCorpo_Msg = sCorpo_Msg & "    <td align=""center"" valign=""top""><div align=""center"">"
					sCorpo_Msg = sCorpo_Msg & "      <p><a href="/""& Application("URLdaLoja") &""" target=""_blank""><img src="""& Application("URLdaLoja") &"/img/LogoTop.png"" alt="""& ucase(Application("Nome")) &""" border=""0""></a></p>"
					sCorpo_Msg = sCorpo_Msg & "      <p class=""Titulo""><strong>Produto indisponivel - "& ucase(Application("Nome")) &"</strong><br>"
					sCorpo_Msg = sCorpo_Msg & "      <p align=""left"" style=""font-weight: bold"">Ol&aacute; "& trim(Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCate, "id", "Nome")) &" , </p>"
					sCorpo_Msg = sCorpo_Msg & "      <p align=""center"" style=""font-weight: bold"">"
					sCorpo_Msg = sCorpo_Msg & "        O produto: "& sProduto &" ja esta disponível, <a href="/""& Application("URLdaLoja") &"/detalhes.asp?produto="& rs("IdProduto") &""">clique aqui</a> para compra-lo.<br>"
					sCorpo_Msg = sCorpo_Msg & "        Agradecemos o seu interesse em nossos Servi&ccedil;os/Produtos.</p>"
					sCorpo_Msg = sCorpo_Msg & "      <hr align=""center"" size=""1"" noshade style=""border:1px dashed #FFF402"">"
					sCorpo_Msg = sCorpo_Msg & "      <p align=""left""><span ><strong>Venha conferir tamb&eacute;m nossos :<br>"
					sCorpo_Msg = sCorpo_Msg & "            <a href="/""& Application("URLdaLoja") &"/lancamentos.asp"" target=""_blank"" >LAN&Ccedil;AMENTOS</a> | "
					sCorpo_Msg = sCorpo_Msg & "            <a href="/""& Application("URLdaLoja") &"/ofertas.asp"" target=""_blank"" >PROMO&Ccedil;&Otilde;ES</a> | <a href="/""& Application("URLdaLoja") &"/maisvendido.asp"" target=""_blank"">e o RANKING dos produtos mais vendidos</a> | </strong></span></p>"
					sCorpo_Msg = sCorpo_Msg & "      <p align=""justify""><span ><strong>Em caso de d&uacute;vidas,  ou mais informa&ccedil;&otilde;es, entre em contato conosco.</strong></span></p>"
					sCorpo_Msg = sCorpo_Msg & "      <p align=""left""><a href="/""& Application("URLdaLoja") &""" target=""_blank"">"& Application("nome") &"</a><br> "
					sCorpo_Msg = sCorpo_Msg & "        <span ><strong>Telefone: "& Application("telefoneloja") &" <br>"
					sCorpo_Msg = sCorpo_Msg & "        Fax: "& Application("faxloja") &" <br>"
					sCorpo_Msg = sCorpo_Msg & "        E-mail: <a href="/"mailto:"& Application("Mailcobranca") &""">"& Application("Mailcobranca") &"</a> </strong></span><br> "
					sCorpo_Msg = sCorpo_Msg & "        <br>"
					sCorpo_Msg = sCorpo_Msg & "        <br>"
					sCorpo_Msg = sCorpo_Msg & "      </p>"
					sCorpo_Msg = sCorpo_Msg & "    </div></td>"
					sCorpo_Msg = sCorpo_Msg & "  </tr>"
					sCorpo_Msg = sCorpo_Msg & "</table>"

					call Envia_Email(Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCate, "id", "Email"), Application("Mailpedidos"), Application("NomeLoja"), "Produto Disponível - ("& ucase(Application("Nome")) &")", sCorpo_Msg)
				rs.movenext
				sCorpo_Msg = ""
				loop
			end if
			set Rs = nothing
		end if
	end if

	call Fecha_Conexao()
end sub


' =======================================================================
Sub Notifica_Clientes_Lista_Desejos(Produto)
	dim sDataIni, sDataFim, boolMostra, Rs, sIdCate, sCorpo_Msg, sData, sProduto
	
	boolMostra = false
	sData = (day(now) & "/" & month(now) & "/" & year(now))

	call Abre_Conexao()
	
	if(Recupera_Campos_Db_oConn("TB_PRODUTOS", Produto, "id", "ativo") = "1") then	
		if(Recupera_Campos_Db_oConn("TB_PRODUTOS", Produto, "id", "Promocao") = "1") then
			sDataIni = cdate(Recupera_Campos_Db_oConn("TB_PRODUTOS", Produto, "id", "DataIniPromo"))
			sDataFim = cdate(Recupera_Campos_Db_oConn("TB_PRODUTOS", Produto, "id", "DataFimPromo"))
			boolMostra = (date >= sDataIni AND date <= sDataFim)
			
			if(boolMostra) then
				sSQL = "Select * from TB_LISTA_DESEJOS where Id_Produto = "& Produto &" and Notificado = 0 order by Id_Cliente"
				set Rs = oConn.execute(sSQL)

				if(not rs.eof) then 
					do while not rs.eof
						sIdCate = rs("Id_Cliente")
						sProduto = Recupera_Campos_Db_oConn("TB_PRODUTOS", Produto, "id", "Descricao")
								
						sSQL = "Update TB_LISTA_DESEJOS set Notificado = 1, DataNotificado = '"& sData &"' where Id = "& rs("Id")
						oConn.execute(sSQL)
						
						sCorpo_Msg = Recupera_Style_Para_Email()
						sCorpo_Msg = sCorpo_Msg & "<table width=""500"" border=""0"" align=""center"" cellpadding=""1"" cellspacing=""1"">"
						sCorpo_Msg = sCorpo_Msg & "  <tr>"
						sCorpo_Msg = sCorpo_Msg & "    <td align=""center"" valign=""top""><div align=""center"">"
						sCorpo_Msg = sCorpo_Msg & "      <p><a href="/""& Application("URLdaLoja") &""" target=""_blank""><img src="""& Application("URLdaLoja") &"/img/LogoTop.png"" alt="""& ucase(Application("Nome")) &""" border=""0""></a></p>"
						sCorpo_Msg = sCorpo_Msg & "      <p class=""Titulo""><strong>Produto em Promocao - Lista de Desejos - "& ucase(Application("Nome")) &"</strong><br>"
						sCorpo_Msg = sCorpo_Msg & "      <p align=""left"" style=""font-weight: bold"">Ol&aacute; "& trim(Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCate, "id", "Nome")) &" , </p>"
						sCorpo_Msg = sCorpo_Msg & "      <p align=""center"" style=""font-weight: bold"">"
						sCorpo_Msg = sCorpo_Msg & "        O produto: "& sProduto &" entrou em promoção, <a href="/""& Application("URLdaLoja") &"/detalhes.asp?produto="& rs("Id_Produto") &""">clique aqui</a> para compra-lo.<br>"
						sCorpo_Msg = sCorpo_Msg & "        Agradecemos o seu interesse em nossos Servi&ccedil;os/Produtos.</p>"
						sCorpo_Msg = sCorpo_Msg & "      <hr align=""center"" size=""1"" noshade style=""border:1px dashed #FFF402"">"
						sCorpo_Msg = sCorpo_Msg & "      <p align=""left""><span ><strong>Venha conferir tamb&eacute;m nossos :<br>"
						sCorpo_Msg = sCorpo_Msg & "            <a href="/""& Application("URLdaLoja") &"/lancamentos.asp"" target=""_blank"" >LAN&Ccedil;AMENTOS</a> | "
						sCorpo_Msg = sCorpo_Msg & "            <a href="/""& Application("URLdaLoja") &"/ofertas.asp"" target=""_blank"" >PROMO&Ccedil;&Otilde;ES</a> | <a href="/""& Application("URLdaLoja") &"/maisvendido.asp"" target=""_blank"" >e o RANKING dos produtos mais vendidos</a> | </strong></span></p>"
						sCorpo_Msg = sCorpo_Msg & "      <p align=""justify""><span ><strong>Em caso de d&uacute;vidas,  ou mais informa&ccedil;&otilde;es, entre em contato conosco.</strong></span></p>"
						sCorpo_Msg = sCorpo_Msg & "      <p align=""left""><a href="/""& Application("URLdaLoja") &""" target=""_blank"" >"& ucase(Application("Nome")) &"</a><br> "
						sCorpo_Msg = sCorpo_Msg & "        <span ><strong>Telefone: "& Application("telefoneloja") &" <br>"
						sCorpo_Msg = sCorpo_Msg & "        Fax: "& Application("faxloja") &" <br>"
						sCorpo_Msg = sCorpo_Msg & "        E-mail: <a href="/"mailto:"& Application("Mailcobranca") &""" >"& Application("Mailcobranca") &"</a> </strong></span><br> "
						sCorpo_Msg = sCorpo_Msg & "        <br>"
						sCorpo_Msg = sCorpo_Msg & "        <br>"
						sCorpo_Msg = sCorpo_Msg & "      </p>"
						sCorpo_Msg = sCorpo_Msg & "    </div></td>"
						sCorpo_Msg = sCorpo_Msg & "  </tr>"
						sCorpo_Msg = sCorpo_Msg & "</table>"

						call Envia_Email(Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCate, "id", "Email"), Application("Mailpedidos"), Application("NomeLoja"), "Produto em Promoção - Lista de Desejo (Styllus.Net)", sCorpo_Msg)
					rs.movenext
					sCorpo_Msg = ""
					loop
				end if
				set Rs = nothing
			end if
		end if
	end if

	call Fecha_Conexao()
end sub


' =======================================================================
function MostraTotalDesconto(Tipo)
	dim x, sValorDesconto

	sValorDesconto = 0
	
	For x = 1 to iCount
		if(Tipo = "C") then  sValorDesconto = (sValorDesconto + Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID, x), "id", "Id_Categoria")) )
		if(Tipo = "S") then  sValorDesconto = (sValorDesconto + Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID, x), "id", "Id_SubCategoria")) )
		if(Tipo = "P") then  sValorDesconto = (sValorDesconto + Session("d" & ARYShoppingcart(C_ID, x)) )
	next

	if(sValorDesconto > 0) then 
		MostraTotalDesconto = RetornaCasasDecimais(sValorDesconto, 2)
	else
		MostraTotalDesconto = sValorDesconto
	end if
end function


' =======================================================================
function VerificaDesconto(IdCategoria, IdSubCategoria)
	dim sValorDescontoCategoria, sValorDescontoSubCategoria
	dim BoolDescontoCategoria, BoolDescontoSubCategoria
	dim DataIniDescontoCategoria, DataFimDescontoCategoria
	dim DataIniDescontoSubCategoria, DataFimDescontoSubCategoria
	
	VerificaDesconto = false
	Session("Cat" & IdCategoria) = 0
	Session("Sub" & IdSubCategoria) = 0
	
	if(IdCategoria = "" and IdSubCategoria = "") then  
		VerificaDesconto = false
		exit function
	end if

	if(IdCategoria = "") then  
		VerificaDesconto = false
		exit function
	end if
	
	call Abre_Conexao()
	
	BoolDescontoCategoria = Recupera_Campos_Db_oConn("TB_CATEGORIAS", IdCategoria, "id", "Desconto")
	if(IdSubCategoria <> "") then 
		BoolDescontoSubCategoria = Recupera_Campos_Db_oConn("TB_SUBCATEGORIA", IdSubCategoria, "id", "Desconto")
	end if

	if(BoolDescontoCategoria = "1") then 
		VerificaDesconto = true
		DataIniDescontoCategoria  = cdate(Recupera_Campos_Db_oConn("TB_CATEGORIAS", IdCategoria, "id", "DataIni"))
		DataFimDescontoCategoria  = cdate(Recupera_Campos_Db_oConn("TB_CATEGORIAS", IdCategoria, "id", "DataFIm"))

		if(date => DataIniDescontoCategoria AND DataFimDescontoCategoria >= date) then
			sValorDescontoCategoria = Recupera_Campos_Db_oConn("TB_CATEGORIAS", IdCategoria, "id", "DescontoValor")
		else
			sValorDescontoCategoria = 0
		end if
	else
		sValorDescontoCategoria = 0
	end if
		
	if(BoolDescontoSubCategoria = "1") then 
		VerificaDesconto = true
		DataIniDescontoSubCategoria  = cdate(Recupera_Campos_Db_oConn("TB_SUBCATEGORIA", IdSubCategoria, "id", "DataIni"))
		DataFimDescontoSubCategoria  = cdate(Recupera_Campos_Db_oConn("TB_SUBCATEGORIA", IdSubCategoria, "id", "DataFIm"))

		if(date => DataIniDescontoSubCategoria AND DataFimDescontoSubCategoria >= date) then
			sValorDescontoSubCategoria = Recupera_Campos_Db_oConn("TB_SUBCATEGORIA", IdSubCategoria, "id", "DescontoValor")
		else
			sValorDescontoSubCategoria = 0
		end if
	else
		sValorDescontoSubCategoria = 0
	end if
	
	if(sValorDescontoCategoria > 0) then 
		Session("Cat" & IdCategoria) = cdbl(sValorDescontoCategoria)
	end if
	
	if(sValorDescontoSubCategoria > 0) then 
		Session("Sub" & IdSubCategoria) = cdbl(sValorDescontoSubCategoria)
	end if

	call Fecha_Conexao()
end function

'========================================================================
function Monta_Video()
	dim sSql, Rs, i, j
	dim sConteudo
    
	call Abre_Conexao()
	
	sSql = "select top 1 * from tb_publicacoes (nolock) where CdCliente  = 97 and Id_Tipo = 4 and Ativo = 1 "

	set Rs = oConn.execute(sSql)

    if(not rs.eof) then
		sImagemB   = rs("Imagem")
        sTitulo    = rs("nome")
        sTipoVideo = rs("nome_2")
            
        sConteudo =	sConteudo & "<video width=""280"" height=""180""  controls=""controls"">" & vbcrlf
		sConteudo =	sConteudo & "	  <source src="""&sLinkImagem&"/EcommerceNew/upload/publicacoes/"&sImagemB&" "" type=""video/"&sTipoVideo&"""/>" & vbcrlf
		sConteudo =	sConteudo & "	  Seu navegador não suporta este formato. " & vbcrlf
        sConteudo =	sConteudo & " </video></li>" & vbcrlf
			
        
        response.write  sConteudo
        response.Flush
        sConteudo = ""

	end if

	set Rs = nothing
	
	call Fecha_Conexao()
	
	'Monta_three_phases = sConteudo
end function
'========================================================================
function Monta_three_phases()
	dim sSql, Rs, i, j
	dim sConteudo
    
	call Abre_Conexao()
	
	sSql = "select id,imagem,link,link_jp,descricao from tb_banners (nolock) where  id_formato = 1 and cdcliente = 97 and ativo = 1 order by link_jp asc"

	set Rs = oConn.execute(sSql)


		if(not rs.eof) then
	    
		do while not rs.eof
			sImagemB 		= rs("Imagem")
			sLink           = rs("link")
            sTitulo 		= rs("descricao")
            
            sConteudo =	sConteudo & "	<li class=""item first"">"
			sConteudo =	sConteudo & "		<a href="/""&sLink&""" title="""&sTitulo&"""> "
			sConteudo =	sConteudo & "	 		<img src="""&sLinkImagem&"/EcommerceNew/upload/banner/"&sImagemB&""" alt="""&sTitulo&""" /> "
			sConteudo =	sConteudo & "		</a> "
			sConteudo =	sConteudo & "	</li>" & vbcrlf
			
            response.write  sConteudo
            response.Flush
            sConteudo = ""
            
  
		rs.movenext
		loop
        
        response.write  sConteudo
        response.Flush
        sConteudo = ""

	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	
	'Monta_three_phases = sConteudo
end function


' =======================================================================
function Mostra_Banner(sTipo)
	dim sConteudo, Rs, sSql, sCampoDescIdioma, sComboPeriodo
	dim sCampo(15), sData, sLink, sTamanho, Descricao, Empresa, lCont
	
	sData = (day(now) & "/" & month(now) & "/" & year(now))
	
	if(sTipo = "") then exit function

	call Abre_Conexao()
		
	Select case sTipo
		case "1" ' MOSTRA NA HOME 1 BANNER - CENTRAL
			sSql = "Select * from TB_BANNERS where cdcliente = "& sCdCliente &" and Id_Formato = 1 and ativo = 1 and DataIni <= '"& sData &" 00:00:00' and isnull(Imagem, '') <> '' AND NOT ID = 10 order by newId() "
			sTamanho = "width=""638"" height=""132"""

		case "2" 
			sSql = "Select TOP 1 * from TB_BANNERS where cdcliente = "& sCdCliente &" and Id_Formato = 1 and ativo = 1 and DataIni <= '"& sData &" 00:00:00' and isnull(Imagem, '') <> '' AND ID = 10 "

		'case "2" 
			'sSql = "Select top 1 * from TB_BANNERS where cdcliente = "& sCdCliente &" and Id_Formato = 4 and ativo = 1 and DataIni <= '"& sData &" 00:00:00'  and isnull(Imagem, '') <> '' order by newId() "

		case "3" 
			sSql = "Select top 1 * from TB_BANNERS where cdcliente = "& sCdCliente &" and Id_Formato = 6 and ativo = 1 and DataIni <= '"& sData &" 00:00:00'  and isnull(Imagem, '') <> '' order by newId() "

		case "4" 
			sSql = "Select * from TB_BANNERS where cdcliente = "& sCdCliente &" and Id_Formato = 2 and ativo = 1 and DataIni <= '"& sData &" 00:00:00'  and isnull(Imagem, '') <> '' "  
            if(sIdCat <> "") then sSql = sSql & " and id_categoria = " & sIdCat
            sSql = sSql & " order by newId() "

		case "5" 
			sSql = "Select top 1 * from TB_BANNERS where cdcliente = "& sCdCliente &" and Id_Formato = 3 and ativo = 1 and DataIni <= '"& sData &" 00:00:00'  and isnull(Imagem, '') <> '' order by newId() "

		case "1m" ' MOSTRA NA HOME 1 BANNER - CENTRAL
			sSql = "Select * from TB_BANNERS where cdcliente = "& sCdCliente &" and Id_Formato = 1 and ativo = 1 and DataIni <= '"& sData &" 00:00:00' and isnull(Imagem_Jp, '') <> '' order by newId() "
			sTamanho = "width=""638"" height=""132"""

		case "2m"
			sSql = "Select * from TB_BANNERS where cdcliente = "& sCdCliente &" and Id_Formato = 2 and ativo = 1 and DataIni <= '"& sData &" 00:00:00'  and isnull(Imagem_Jp, '') <> '' order by newId() "

	end select

    'response.write sSql
    'response.End
	Set Rs = oConn.execute(sSql)
	
	if(not rs.eof) then
        lCont = 1

		do while not rs.eof
			sCampo(0)    = rs("DataIni")
			sCampo(1)    = rs("Imagem")
			sCampo(2)    = rs("Link")
			sLink        = sCampo(2)
			sCampo(3)    = rs("DataFim")
			sCampo(4)    = rs("Id_FaixaHorario")
			sCampo(5)    = rs("NovaJanela")
			sCampo(6)    = rs("Largura")
			sCampo(7)    = rs("Altura")
			sCampo(8)    = rs("id")
            sCampo(9)    = rs("Imagem_Jp")
            Descricao    = replace(trim(rs("Descricao"))," ", "")
            Empresa      = trim(rs("Empresa"))
            Link_Jp     = trim(rs("Link_Jp"))
            link_7      = trim(rs("link_7"))

			Select case sCampo(4)
				case "1" ' 24 HORAS
					boolHorario = true
				case "2"  ' MANHA
					boolHorario = (time > 1 and time <= 12)
				case "3"  ' TARDE
					boolHorario = (time > 12 and time <= 18)
			end select
			
			if(sCampo(5) = "1") then sCampo(5) = " target=""_blank""" : else sCampo(5) = "": end if

			if(sCampo(2) <> "" and not isnull(sCampo(2))) then 
                sLink = " href=""/hidden/banner_estatistica.asp?id_cliente="& sCampo(8) &"&pag="& sCampo(2) &""" "
            end if
			
			if( (date >= cdate(sCampo(0))) and (date <= cdate(sCampo(3)))) then
				if(boolHorario) then
					Select case sTipo
						case "1"

                            active = ""
                            if(lCont = 1) then active = "active"

                            sConteudo = sConteudo & "<div class=""item "& active &""">"
                                sConteudo = sConteudo & "<img src="""& sLinkImagem &"EcommerceNew/upload/banner/"& sCampo(1) &""" alt="""& Descricao &""">"
                                sConteudo = sConteudo & "<div class=""carousel-caption"">"
                                    sConteudo = sConteudo & "<div class=""banner-titles""><h2 class=""extra-bold"" >BEM-VINDO AO <span class=""cor-1"">FOOD</span> <span class=""cor-2"">CROWD</span> </h2></div>"
                                    sConteudo = sConteudo & "<p class=""banner-descritions"">"& link_7 &"</p>"
                                    sConteudo = sConteudo & "<p class=""text-center""><a href=""/facaparte"" class=""btn botao-1"">FAÇA PARTE</a></p>"
                                sConteudo = sConteudo & "</div>"
                            sConteudo = sConteudo & "</div>"

						case "2", "3"
                            sConteudo = sConteudo & sLink & "<img src="""& sLinkImagem &"EcommerceNew/upload/banner/"& sCampo(1) &""" border=""0"" alt="""& Descricao &"""></a>"

						case "4"
                            sConteudo = sConteudo & "<div class=""item""><a "& sLink &"><img alt="""& Descricao &""" src="""& sLinkImagem &"EcommerceNew/upload/banner/"& sCampo(1) &"""></a></div>"     

						case "5"
                            sConteudo = sConteudo & "<div class=""item active""><a "& sLink &"><img alt="""& Descricao &""" src="""& sLinkImagem &"EcommerceNew/upload/banner/"& sCampo(1) &""" alt=""slide1""></a></div>"     

						case "1m", "2m"
                            sConteudo = sConteudo & sLink & "<img src="""& sLinkImagem &"EcommerceNew/upload/banner/"& sCampo(9) &""" border=""0""></a>"
					end select
				end if
			end if

    		sCampo(5) = ""
            lCont = lCont + 1 
		rs.movenext
		loop
	end if

	call Fecha_Conexao()

	Mostra_Banner = sConteudo
end function


' =======================================================================
function Limpa_Tag_Editor(texto)
	Limpa_Tag_Editor = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(lcase(texto),"<br>",""),"<p>",""),"</p>",""),"</div>",""),"</DIV>",""),"</font>",""),"</table>",""),"<table>",""),"<td ",""),"<font ",""),"</td>",""),"<br />","")
end function


' =======================================================================
function Calcula_Servico_CartaoCredito(sTotal)
	if(sTotal = "") then 
		exit function
		sTotal = 0
	end if
	
	Calcula_Servico_CartaoCredito = ((cdbl(sPorcentagemCartaoCredito) / 100) * cdbl(sTotal))
end function



' =======================================================================
function Mostra_Loja_Fechada()
	Mostra_Loja_Fechada = ""

	if(not Verifica_Status_Loja()) then 
		Mostra_Loja_Fechada = "<p class=""styleletra10branco"" align=""center"">" & Retorna_Status_Loja("2") & "</p>"
	end if
end function


' =======================================================================
function Verifica_Estoque_QuantidadeMinima(idProduto)
	dim sQte_Estoque, sQte_Minima_Consumidor, sQte_Minima_Lojista
	
	Verifica_Estoque_QuantidadeMinima = true
	
	sQte_Estoque = 				Recupera_Campos_Db_oConn("TB_PRODUTOS", idProduto, "id", "Qte_Estoque")
	sQte_Minima_Consumidor = 	Recupera_Campos_Db_oConn("TB_PRODUTOS", idProduto, "id", "Qte_Minima_Consumidor")
	sQte_Minima_Lojista =		Recupera_Campos_Db_oConn("TB_PRODUTOS", idProduto, "id", "Qte_Minima_Lojista")
	
	if(Session("TipoId") = "") then
		' CLIENTE NAO LOGADO
		if(sQte_Estoque < sQte_Minima_Consumidor) then Verifica_Estoque_QuantidadeMinima = false
	else
		if(sQte_Estoque < sQte_Minima_Consumidor) then Verifica_Estoque_QuantidadeMinima = false

		if(Session("TipoId") = "1") then
			if(sQte_Estoque < sQte_Minima_Lojista) then Verifica_Estoque_QuantidadeMinima = false
		end if
	end if
end function

' =======================================================================
function Monta_Combo_Provincia(NameCombo, sSelect, sOnchange)
	dim Rs, Rs1, sSql, sConteudo, sMarcaValor, sSelected, sCampo(1)
	dim sCampoIf, sOrdem
	
	call Abre_Conexao()

	if(Idioma = "br") then
		sCampoIf = "descricao"
		sOrdem = "ordem"
	else
		sCampoIf = "descricao_jp"
		sOrdem = "ordem_jp"
	end if
	
	sSql = "Select * from TB_ESTADOS (nolock) ORDER BY DESCRICAO"
	Set Rs = oConn.execute(sSql)
	
	sConteudo = sConteudo & "<select name="""& NameCombo &""" "& sOnchange &" >" & vbcrlf
	sConteudo = sConteudo & " <option value="""" >Estados</option>" & vbcrlf
		
	if(not rs.eof) then
		if(sSelect <> "") then sMarcaValor = trim(Recupera_Campos_Db_oConn("TB_ESTADOS", sSelect, "id", sCampoIf))
		
		do while not rs.eof
			sCampo(1) = trim(rs(sCampoIf))
			
			if(sMarcaValor = sCampo(1)) then 
				sSelected = " selected "
			else
				sSelected = ""
			end if
			sConteudo = sConteudo & " <option value="""& RS("ID") &""" "& sSelected &">"& sCampo(1) & "</option>" & vbcrlf
		rs.movenext
		loop
	end if
	Set Rs = nothing
	sConteudo = sConteudo & "</select>" & vbcrlf
	call Fecha_Conexao()

	Monta_Combo_Provincia = sConteudo
end function

' =======================================================================
function Monta_Galeria_home()
	dim sConteudo, Rs, sSql, sCampoPrecoA
	dim sCampo(15), sCampoNomeA, sCampoDestaqueA
		
	call Abre_Conexao()
		sSql = _
			"Select top 1 " & _
				"TB_GALERIA_FOTOS.*  " & _
			"from  " & _
				"TB_GALERIA_FOTOS "  & _
			"where " & _
				"TB_GALERIA_FOTOS.Ativo = 1 " & _
			"order by " & _
				" RND(INT(NOW*TB_GALERIA_FOTOS.id)-NOW*TB_GALERIA_FOTOS.id) "
	Set Rs = oConn.execute(sSql)
	if(not rs.eof) then
		do while not rs.eof
			sCampo(0) = trim(rs("id"))
			sCampo(1) = trim(rs("Imagem_Autor"))
			sCampo(2) = trim(rs("Imagem_Path"))
			
			if(Idioma = "br") then
				sConteudo = sConteudo & "<table width=""360"" height=""100%"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""2"">"
				sConteudo = sConteudo & "      <tr align=""center"" valign=""top"">"
				sConteudo = sConteudo & "        <td colspan=""2"" scope=""col"" align=""center""><img src=""/images/text_galeria.jpg"" width=""160"" height=""35"" border=""0"" alt=""""></td>"
				sConteudo = sConteudo & "      </tr>"
				sConteudo = sConteudo & "      <tr align=""center"" valign=""top"">"
				if(sCampo(2) <> "") then sConteudo = sConteudo & " <td><a href="/"javascript: AbrePopUp('/pt/galeria/galeria_popup.asp?produto="& sCampo(0) &"', '500','450');""><img src=""/images/images_galeria/"& sCampo(2) &""" width=""150"" height=""100"" border=""0"" alt=""""></a></td>"
				sConteudo = sConteudo & "        <td >"
				sConteudo = sConteudo & "			<p align=""justify"" class=""styleletra10branco""><a href="/"/pt/galeria/default.asp?categoria="& Recupera_Campos_Db_oConn("TB_GALERIA_FOTOS", sCampo(0), "id", "Id_tema") &""" class=""styleletra10branco"">"& Recupera_Campos_Db_oConn("TB_GALERIA_CATEGORIA", Recupera_Campos_Db_oConn("TB_GALERIA_FOTOS", sCampo(0), "id", "Id_tema"), "id", "Categoria")  &"</a></p><br>"
				sConteudo = sConteudo & "		</td>"
				sConteudo = sConteudo & "      </tr>"
				sConteudo = sConteudo & "      <tr align=""center"" valign=""bottom"">"
				sConteudo = sConteudo & "        <td nowrap class=""styleletra10branco"" colspan=""2"">"
				sConteudo = sConteudo & "			<div align=""center""><br><br>Confiram todos os eventos em nossa <br>Galeria de Fotos! "
				sConteudo = sConteudo & "            <a href="/"/pt/galeria"" class=""styleletra10branco"">&#8249;Clique Aqui!&#8250;</a></div>"
				sConteudo = sConteudo & "		</td>"
				sConteudo = sConteudo & "      </tr>"
				sConteudo = sConteudo & "</table>"
			else
				sConteudo = sConteudo & "<table width=""360"" height=""100%"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""2"">"
				sConteudo = sConteudo & "      <tr align=""center"" valign=""top"">"
				sConteudo = sConteudo & "        <td colspan=""2"" scope=""col"" align=""center""><img src=""/images/jp_galeria.jpg"" border=""0"" alt=""""></td>"
				sConteudo = sConteudo & "      </tr>"
				sConteudo = sConteudo & "      <tr align=""center"" valign=""top"">"
				if(sCampo(2) <> "") then sConteudo = sConteudo & " <td><a href="/"javascript: AbrePopUp('/jp/galeria/galeria_popup.asp?produto="& sCampo(0) &"', '500','450');""><img src=""/images/images_galeria/"& sCampo(2) &""" width=""150"" height=""100"" border=""0"" alt=""""></a></td>"
				sConteudo = sConteudo & "        <td >"
				sConteudo = sConteudo & "			<p align=""justify"" class=""styleletra10branco""><a href="/"/jp/galeria/default.asp?categoria="& Recupera_Campos_Db_oConn("TB_GALERIA_FOTOS", sCampo(0), "id", "Id_tema") &""" class=""styleletra10branco"">"& Recupera_Campos_Db_oConn("TB_GALERIA_CATEGORIA", Recupera_Campos_Db_oConn("TB_GALERIA_FOTOS", sCampo(0), "id", "Id_tema"), "id", "Categoria_Jp")  &"</a></p><br>"
				sConteudo = sConteudo & "		</td>"
				sConteudo = sConteudo & "      </tr>"
				sConteudo = sConteudo & "      <tr align=""center"" valign=""bottom"">"
				sConteudo = sConteudo & "        <td nowrap class=""styleletra10branco"" colspan=""2"">"
				sConteudo = sConteudo & "			<div align=""center""><br><br>&#12452;&#12505;&#12531;&#12488;&#31561;&#12398;&#12501;&#12457;&#12488;&#12462;&#12515;&#12521;&#12522;&#12540; "
				sConteudo = sConteudo & "            <a href="/"/jp/galeria"" class=""styleletra10branco""> &rarr;&#35443;&#12375;&#12367;&#12399;&#12371;&#12385;&#12425; </a></div>"
				sConteudo = sConteudo & "		</td>"
				sConteudo = sConteudo & "      </tr>"
				sConteudo = sConteudo & "</table>"
			end if
		rs.movenext
		loop
	end if
	Set Rs = nothing
	
	call Fecha_Conexao()

	Monta_Galeria_home = sConteudo
end function


' =======================================================================
function Monta_Resoluccoes_Home(Id)
	dim sConteudo, Rs, sSql, sCampoPrecoA, sCampoIf
	dim sCampo(15), sCampoNomeA, sCampoDestaqueA
		
	if(Idioma = "br") then
		sCampoIf = "Imagem_Desc"
	else
		sCampoIf = "Imagem_Desc_Jp"
	end if
		
	call Abre_Conexao()
		sSql = _
			"Select " & _
				"TB_WALLPAPERS.*  " & _
			"from  " & _
				"TB_WALLPAPERS "  & _
			"where " & _
				"TB_WALLPAPERS.Id_Tipo = "& Id & _
			" order by " & _
				" TB_WALLPAPERS.Imagem_Desc "
	Set Rs = oConn.execute(sSql)

	if(not rs.eof) then
		do while not rs.eof
			sCampo(0) = trim(rs("id"))
			sCampo(1) = trim(rs(sCampoIf))
 			sConteudo = sConteudo & "<a href="/"javascript: AbrePopUp('wallpapers/pop_ver.asp?id="& sCampo(0) &"','500','450');"" class=""styleletra10branco"">"& sCampo(1) &"</a><br>"
		rs.movenext
		loop
	end if
	Set Rs = nothing
	
	call Fecha_Conexao()

	Monta_Resoluccoes_Home = sConteudo
end function


' =======================================================================
function Monta_Wallpaper_home()
	dim sConteudo, Rs, sSql, sCampoPrecoA
	dim sCampo(15), sCampoNomeA, sCampoDestaqueA
		
	call Abre_Conexao()
		sSql = _
			"Select top 1 " & _
				"TB_WALLPAPERS_TIPO.*  " & _
			"from  " & _
				"TB_WALLPAPERS_TIPO "  & _
			"where " & _
				"TB_WALLPAPERS_TIPO.Ativo = 1 " & _
			"order by " & _
				" RND(INT(NOW*TB_WALLPAPERS_TIPO.id)-NOW*TB_WALLPAPERS_TIPO.id) "
	Set Rs = oConn.execute(sSql)
	if(not rs.eof) then
		do while not rs.eof
			sCampo(0) = trim(rs("id"))
			sCampo(1) = trim(rs("Imagem_Path"))
			
			if(Idioma = "br") then
				sConteudo = sConteudo & "<table width=""160"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""1"">"
				sConteudo = sConteudo & "      <tr>"
				sConteudo = sConteudo & "        <th align=""center"" valign=""top"" scope=""col""><img src=""/images/text_wallpaper.jpg"" width=""160"" height=""35"" alt=""""></th>"
				sConteudo = sConteudo & "      </tr>"
				sConteudo = sConteudo & "      <tr>"
				sConteudo = sConteudo & "        <td align=""center"" valign=""top""><img src=""/images/images_wallpapers/"& sCampo(1) &""" width=""135"" height=""95"" hspace=""0"" vspace=""0"" border=""0"" alt=""""></td>"
				sConteudo = sConteudo & "      </tr>"
				sConteudo = sConteudo & "      <tr>"
				sConteudo = sConteudo & "        <td align=""center"" valign=""top"" class=""styleletra10branco"">" & Monta_Resoluccoes_Home(sCAmpo(0))
				sConteudo = sConteudo & "		</td>"
				sConteudo = sConteudo & "      </tr>"
				sConteudo = sConteudo & "    </table>"
			else
				sConteudo = sConteudo & "<table width=""160"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""1"">"
				sConteudo = sConteudo & "      <tr>"
				sConteudo = sConteudo & "        <th align=""center"" valign=""top"" scope=""col""><img src=""/images/jp_wallpaper.jpg"" alt=""""></th>"
				sConteudo = sConteudo & "      </tr>"
				sConteudo = sConteudo & "      <tr>"
				sConteudo = sConteudo & "        <td align=""center"" valign=""top""><img src=""/images/images_wallpapers/"& sCampo(1) &""" width=""135"" height=""95"" hspace=""0"" vspace=""0"" border=""0"" alt=""""></td>"
				sConteudo = sConteudo & "      </tr>"
				sConteudo = sConteudo & "      <tr>"
				sConteudo = sConteudo & "        <td align=""center"" valign=""top"" class=""styleletra10branco"">" & Monta_Resoluccoes_Home(sCAmpo(0))
				sConteudo = sConteudo & "		</td>"
				sConteudo = sConteudo & "      </tr>"
				sConteudo = sConteudo & "    </table>"
			end if
		rs.movenext
		loop
	end if
	Set Rs = nothing
	
	call Fecha_Conexao()

	Monta_Wallpaper_home = sConteudo
end function



' FUNCOES GLOBAIS =====================================================================
' =======================================================================
function Monta_Colecion_Destaque()
	dim sConteudo, Rs, sSql, sCampoPrecoA
	dim sCampo(15), sCampoNomeA, sCampoDestaqueA
	
	if(Idioma = "br") then
		sCampoNomeA = "Descricao"
		sCampoDestaqueA = "Destaque"
	else
		sCampoNomeA = "Descricao_Jp"
		sCampoDestaqueA = "Destaque_Jp"
	end if
	
	sCampoPrecoA = "Preco_Lojista"
	if(Session("TipoId") = "3") then sCampoPrecoA = "Preco_Consumidor"
	
	call Abre_Conexao()
		sSql = _
			"Select top 3 " & _
				"TB_PRODUTOS.Id,  " & _
				"TB_PRODUTOS."& sCampoNomeA &",  " & _
				"TB_PRODUTOS."& sCampoDestaqueA &",  " & _
				"TB_PRODUTOS.Preco_Consumidor,  " & _
				"TB_PRODUTOS.Preco_Lojista,  " & _
				"TB_PRODUTOS.Imagem,  " & _
				"TB_MOEDAS.Moeda,  " & _
				"TB_MOEDAS.Simbolo  " & _
			"from  " & _
				"TB_PRODUTOS "  & _
					" INNER JOIN TB_MOEDAS on TB_PRODUTOS.Id_Moeda = TB_MOEDAS.id " & _
			"where " & _
				"TB_PRODUTOS.Promocao = 1 and  " & _
				"TB_PRODUTOS.Lancamento = 1 and  " & _
				"TB_PRODUTOS.Qte_Estoque > 0 and  " & _
				"TB_PRODUTOS.Ativo = 1 and  " & _
				"TB_MOEDAS.Ativo = 1 " & _
			"order by " & _
				" RND(INT(NOW*TB_PRODUTOS.id)-NOW*TB_PRODUTOS.id) "
	Set Rs = oConn.execute(sSql)
	if(not rs.eof) then
		sConteudo = sConteudo & "<table width=""90%""  border=""0"" cellspacing=""5"" cellpadding=""0"">"
		sConteudo = sConteudo & " <tr align=""center"" valign=""top"">"
			
		do while not rs.eof
			sCampo(0) = trim(rs("Imagem"))
			sCampo(1) = trim(rs(sCampoNomeA))
			sCampo(2) = RetornaCasasDecimais(cdbl(rs(sCampoPrecoA)),0)
			sCampo(3) = Server.aspEncode(trim(rs("Simbolo")))
			if(sCampo(0) = "") then sCampo(0) = "img_indisponivel.jpg"
			sCampo(4) = trim(rs("id"))
			
			if(Idioma = "br") then
				sConteudo = sConteudo & "	<td width=""30%"" scope=""col"">    "
				sConteudo = sConteudo & "  		<table width=""180"" border=""0"" cellpadding=""0"" cellspacing=""0"" bordercolor=""#5A0703"" bgcolor=""#5A0703"">"
				sConteudo = sConteudo & "  			 <tr align=""center"" valign=""top"">"
				sConteudo = sConteudo & "    			<td height=""15"" colspan=""3"" scope=""col""><div align=""center""><img src=""/images/quadro_prod_top.jpg"" width=""58"" height=""15"" hspace=""0"" vspace=""0"" border=""0"" alt=""""></div></td>"
				sConteudo = sConteudo & "     		</tr>"
				sConteudo = sConteudo & "       	<tr align=""center"" valign=""top"">"
				sConteudo = sConteudo & "        		<td width=""15"" valign=""middle""><div align=""center""><img src=""/images/quadro_prod_esq.jpg"" width=""15"" height=""58"" hspace=""0"" vspace=""0"" border=""0"" align=""absmiddle"" alt=""""></div></td>"
				sConteudo = sConteudo & "        		<td width=""150"" height=""150"" valign=""middle"" bgcolor=""""><a href="/"/pt/ecommerce/detalhes.asp?produto="& sCampo(4) &"""><img src=""/images/images_produto/"& sCampo(0) &"""  width=""101"" height=""152"" hspace=""0"" vspace=""0"" border=""0"" align=""absmiddle"" alt=""""></a></td>"
				sConteudo = sConteudo & "				<td width=""15"" valign=""middle""><div align=""center""><img src=""/images/quadro_prod_dir.jpg"" width=""15"" height=""58"" hspace=""0"" vspace=""0"" border=""0"" align=""absmiddle"" alt=""""></div></td>"
				sConteudo = sConteudo & "        	</tr>"
				sConteudo = sConteudo & "           <tr align=""center"" valign=""top"">"
				sConteudo = sConteudo & "          		<td height=""15"" colspan=""3""><div align=""center""><img src=""/images/quadro_prod_bot.jpg"" width=""58"" height=""15"" hspace=""0"" vspace=""0"" border=""0"" alt=""""></div></td>"
				sConteudo = sConteudo & "           </tr>"
				sConteudo = sConteudo & "		</table>"
				sConteudo = sConteudo & "		<div class=""styleletra10branco""><br><a href="/"/pt/ecommerce/detalhes.asp?produto="& sCampo(4) &""">"& sCampo(1) &"<br>"& sCampo(3) &"&nbsp;"& sCampo(2) &"</a></div>"
				sConteudo = sConteudo & "	</td>"
			else
				sConteudo = sConteudo & "	<td width=""30%"" scope=""col"">    "
				sConteudo = sConteudo & "  		<table width=""180"" border=""0"" cellpadding=""0"" cellspacing=""0"" bordercolor=""#5A0703"" bgcolor=""#5A0703"">"
				sConteudo = sConteudo & "  			 <tr align=""center"" valign=""top"">"
				sConteudo = sConteudo & "    			<td height=""15"" colspan=""3"" scope=""col""><div align=""center""><img src=""/images/quadro_prod_top.jpg"" width=""58"" height=""15"" hspace=""0"" vspace=""0"" border=""0"" alt=""""></div></td>"
				sConteudo = sConteudo & "     		</tr>"
				sConteudo = sConteudo & "       	<tr align=""center"" valign=""top"">"
				sConteudo = sConteudo & "        		<td width=""15"" valign=""middle""><div align=""center""><img src=""/images/quadro_prod_esq.jpg"" width=""15"" height=""58"" hspace=""0"" vspace=""0"" border=""0"" align=""absmiddle"" alt=""""></div></td>"
				sConteudo = sConteudo & "        		<td width=""150"" height=""150"" valign=""middle"" bgcolor=""""><a href="/"/jp/ecommerce/detalhes.asp?produto="& sCampo(4) &"""><img src=""/images/images_produto/"& sCampo(0) &"""  width=""101"" height=""152"" hspace=""0"" vspace=""0"" border=""0"" align=""absmiddle"" alt=""""></a></td>"
				sConteudo = sConteudo & "				<td width=""15"" valign=""middle""><div align=""center""><img src=""/images/quadro_prod_dir.jpg"" width=""15"" height=""58"" hspace=""0"" vspace=""0"" border=""0"" align=""absmiddle"" alt=""""></div></td>"
				sConteudo = sConteudo & "        	</tr>"
				sConteudo = sConteudo & "           <tr align=""center"" valign=""top"">"
				sConteudo = sConteudo & "          		<td height=""15"" colspan=""3""><div align=""center""><img src=""/images/quadro_prod_bot.jpg"" width=""58"" height=""15"" hspace=""0"" vspace=""0"" border=""0"" alt=""""></div></td>"
				sConteudo = sConteudo & "           </tr>"
				sConteudo = sConteudo & "		</table>"
				sConteudo = sConteudo & "		<div class=""styleletra10branco""><br><a href="/"/jp/ecommerce/detalhes.asp?produto="& sCampo(4) &""">"& sCampo(1) &"<br>"& sCampo(3) &"&nbsp;"& sCampo(2) &"</a></div>"
				sConteudo = sConteudo & "	</td>"
			end if
		rs.movenext
		loop
		sConteudo = sConteudo & "   </tr>"
		sConteudo = sConteudo & "</table>"
	end if
	Set Rs = nothing
	
	call Fecha_Conexao()

	Monta_Colecion_Destaque = sConteudo
end function



' =======================================================================
function Calcula_Tesuriyou(TotalCompra, IdOpcPagamento)
	dim Rs, sSQl, boolCobra
	
	Calcula_Tesuriyou = 0
	exit function
	
	
	if(IdOpcPagamento = "") then
		Calcula_Tesuriyou = "Opcao de pagamento nao escolhida!"
		exit function
	else
		call Abre_Conexao()
		boolCobra = Recupera_Campos_Db_oConn("TB_FORMAS_PAGAMENTO", IdOpcPagamento, "id", "cobrat")

		if(boolCobra) then 
			sSQl = "Select * from TB_TESURIYOU where Valor_Compra >= "& replace(cdbl(TotalCompra),",",".") &" and ativo = 1 order by Valor_Compra"
			set Rs = oConn.execute(sSQl)
			
			if(not rs.eof) then
' 				Calcula_Tesuriyou = RetornaCasasDecimais(rs("Valor_Frete"),0)
 				Calcula_Tesuriyou = cdbl(rs("Valor_Frete"))
			else
				Calcula_Tesuriyou = 0
			end if
			
			set Rs = nothing
		else
			Calcula_Tesuriyou = 0
		end if
		call fecha_Conexao()
	end if
end function


' =======================================================================
function Monta_Linhas_noticias()
	dim Rs, sSql, sCampoTitulo
	dim sCampo(10), d, m, y
	
	if(Idioma = "br") then
		sCampoTitulo = "Titulo"
	else
		sCampoTitulo = "Titulo_Jp"
	end if
	
	call Abre_Conexao()
	
	sSql = "Select * from TB_NOTICIAS where Ativo = 1 order by id desc"
	Set Rs = oConn.execute(sSql)
	if(not rs.eof) then
		do while not rs.eof
			sCampo(0) = rs("id")
			sCampo(1) = rs("dataCad")
			sCampo(2) = trim(rs(sCampoTitulo))
			d = day(sCampo(1))
			m = month(sCampo(1))
			y = year(sCampo(1))

			if(len(d)< 2) then d = ("0" & d)
			if(len(m)< 2) then m = ("0" & m)
			sCampo(1) = (d & "/" & m & "/" & y )
			
			Monta_Linhas_noticias = Monta_Linhas_noticias & sCampo(1) & "&nbsp;<a href="/"javascript:AbrePopUp('news_popup.asp?id="& sCampo(0) &"', '500','450')"" class=""styleletra10branco"">"& sCampo(2) &"</a><br>" & vbcrlf
		rs.movenext
		loop
	end if
	Set Rs = nothing
	
	call Fecha_Conexao()
end function




' =============================================================
sub Apaga_Arquivo_Pasta(sArquivoApagar)
	On Error Resume Next
	
	if(sArquivoApagar = "" ) then exit sub

	dim objFile, sContPath
	dim sPathFile
	
	Set objFile = Server.CreateObject("Scripting.FileSystemObject")
	sPathFile= (sArquivoApagar)
	if (objFile.FileExists(sPathFile) = true) then objFile.DeleteFile (sPathFile)
	Set objFile = nothing
end sub

' =======================================================================
function Monta_Linhas_Produtos()
	dim sConteudo, Rs, sSql
	dim sCampo(16), sNomepresenteado, sEmailPresenteado
	
	call Abre_Conexao()
	
	sSql = "Select * from TB_PEDIDOS_ITENS where id_pedido = " & sIdCateg
	Set Rs = oConn.execute(sSql)
	if(not rs.eof) then
	
		sTotalItens 				= 0
		Session("TotalItensRecibo") = 0
	
		do while not rs.eof
			
			sCampo(0) 		= trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", rs("id_produto"), "id", "Imagem"))
			sCampo(1) 		= trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", rs("id_produto"), "id", "Descricao"))
			sCampo(2) 		= cdbl(rs("quantidade"))
			sCampo(3) 		= RetornaCasasDecimais(cdbl(rs("Preco_Unitario_bruto")),2)
			sCampo(4) 		= Server.aspEncode(trim(Recupera_Campos_Db_oConn("TB_MOEDAS", rs("Id_Moeda"), "id", "Simbolo")))
			sCampo(5) 		= RetornaCasasDecimais((cdbl(rs("Preco_Unitario_bruto")) * sCampo(2)),2)
			sCampo(6) 		= trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", rs("id_produto"), "id", "CodReferencia"))
			sCampo(7) 		= trim(rs("Id_Tamanho"))
			sCampo(8) 		= trim(rs("Id_Cor"))
			sCampo(9) 		= trim(rs("Id_Sabor"))
			sCampo(10)		= trim(rs("Id_Fragrancia"))
			sCampo(11) 		= trim(rs("Id_Tipo"))
			sCampo(12) 		= trim(rs("Id_Pacote"))
			sCampo(13) 		= RetornaCasasDecimais(trim(rs("embrulho")*sCampo(2)),2)
			sCampo(14) 		= trim(rs("Id_Ge"))
			sCampo(15) 		= trim(rs("ValorGe"))
			sCampo(16) 		= trim(rs("CertificadoGE"))
			sNomepresenteado= trim (Recupera_Campos_Db_oConn("TB_PEDIDOS_DADOS_PRESENTEADOS", rs("id"), "id_pedido_itens", "Nome"))

			if(sCampo(0) = "" or isnull(sCampo(0))) then sCampo(0) = "img_indisponivel.jpg"
			
			sConteudo = sConteudo & "                <tr valign=""middle"">"
			sConteudo = sConteudo & "                  <td ><span class=""descriprod"">"
			if(InStr(sCampo(0), ".wmv") > 0 ) then
				sConteudo = sConteudo & "<img src="""& sLinkImagem &"/EcommerceNew/upload/produto/imagem_video.jpg"" width=""50"" height=""50"" hspace=""1"" vspace=""0"" border=""0"" align=""absmiddle"" alt="""">"
			else
				sConteudo = sConteudo & "<img src="""& sLinkImagem &"/EcommerceNew/upload/produto/"& sCampo(0) &""" width=""50"" height=""50"" hspace=""1"" vspace=""0"" border=""0"" align=""absmiddle"" alt="""">"
			end if
			sConteudo = sConteudo & "</td>"
			sConteudo = sConteudo & "                  <td ><span class=""descriprod"">"& rs("id") &"</div></td>"
			sConteudo = sConteudo & "                  <td><span class=""descriprod""><strong>"
			if (sNomepresenteado <> "") then
				sConteudo = sConteudo & "Nome do presentenado: "& sNomepresenteado &" <br> Tipo: <span style=""color:red"">Vale presente</span>"
			else
				sConteudo = sConteudo & ""& sCampo(1) &""
			end if
			sConteudo = sConteudo & "</strong></td>"
			sConteudo = sConteudo & "                  <td><span class=""descriprod""><div align=""center"">"& sCampo(2) &"</div></td>"
			sConteudo = sConteudo & "                  <td><span class=""descriprod""><div align=""center"">"& sCampo(4) &"&nbsp;"& sCampo(3) &"</div></td>"
			sConteudo = sConteudo & "                  <td><span class=""descriprod""><div align=""center"">"& sCampo(13) &"</div></td>"
			sConteudo = sConteudo & "                  <td><span class=""descriprod""><div align=""center"">"& sCampo(4) &"&nbsp;"& sCampo(5) &"</div></td>"
			sConteudo = sConteudo & "                </tr>"
			sConteudo = sConteudo & "		          <tr>"
			sConteudo = sConteudo & "		            <td colspan=""6""><span class=""descriprod"">"

			if(sNomepresenteado = "" ) then 
				sConteudo = sConteudo & "<strong>Ref. </strong>" & sCampo(6) & "<Br>"
				If(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", rs("id_produto"), "id", "valepresente")) = "1") then
					sConteudo = sConteudo & "<strong> Numero do Cupom: </strong>" & Recupera_Campos_Db_oConn("TB_CUPONS", "'" & sIdCateg & "'", "NumeroPedido", "Cupom") & " (nao utilizado)<Br>"
				end if
				
				if(sCampo(7) <> "") then sConteudo = sConteudo & "						<strong> Tamanho:</strong>    " & sCampo(7)  & "|"
				if(sCampo(8) <> "") then sConteudo = sConteudo & "						<strong> Cor: </strong>       " & sCampo(8) & " |<Br>"
				if(sCampo(9) <> "") then sConteudo = sConteudo & "						<strong> Sabor: </strong> 	  " & sCampo(9) & " |"
				if(sCampo(10) <> "") then sConteudo = sConteudo & "			            <strong> Fragrancia:</strong> " & sCampo(10) & " |"
				if(sCampo(11) <> "") then sConteudo = sConteudo & "			            <strong> Tipo:</strong>       " & sCampo(11) & " |"
				if(sCampo(12) <> "") then sConteudo = sConteudo & "			            <strong> Pacote:</strong>     " & sCampo(12) & " |"
				if(sCampo(15) > 0) then sConteudo = sConteudo & "Garantia Estendida - 12 meses - Mapfre - <strong>Valor R$ "& sCampo(15) &"</strong> - Número Certificado: " & sCampo(16) & " - <a href="/"/mapfredoc/certificado.asp?pedido="& sIdCateg &"&produto="& rs("id_produto") &""">Imprimir Certificado</a>"
			end if
			sConteudo = sConteudo & Monta_Itens_Kit(rs("id_produto"))
			sConteudo = sConteudo & "					</td>"
			sConteudo = sConteudo & "		          </tr>"
			sConteudo = sConteudo & "                <tr>"
			sConteudo = sConteudo & "                  <td colspan=""7""><span class=""descriprod""><hr align=""center"" width=""100%"" size=""1"" noshade class=""styleverd10"" style=""border:1px dashed #370401""></td>"
			sConteudo = sConteudo & "                </tr>"
			sTotalItens = sTotalItens + sCampo(5)
			
		rs.movenext
		loop
		
		Session("TotalItensRecibo") = RetornaCasasDecimais(sTotalItens,2)


	end if
	Set Rs = nothing

	Monta_Linhas_Produtos = sConteudo
end function


' =======================================================================
function Retorna_Status_Loja(Layout)
	dim sSql, RS, sDescricao

	call Abre_Conexao()
	sSql = "Select top 1 * from TB_FECHAR_LOJA order by id desc"
	set Rs = oConn.execute(sSql)

	if(Verifica_Status_Loja()) then 
		if(not Rs.Eof) then sDescricao = trim(rs("DescricaoAbre"))
		if(Layout = "1") then 
			Retorna_Status_Loja = sDescricao
			exit function
		end if
		Retorna_Status_Loja = "Aberta<br><br>"
		Retorna_Status_Loja = Retorna_Status_Loja & Server.aspEncode("Comentarios: ") & sDescricao
	else
		if(not Rs.Eof) then sDescricao = trim(rs("DescricaoFecha"))
		if(Layout = "1") then 
			Retorna_Status_Loja = sDescricao
			exit function
		end if
		Retorna_Status_Loja = "Fechada<br><br>"
		Retorna_Status_Loja = Retorna_Status_Loja & Server.aspEncode("Comentarios: ") & sDescricao
	end if

	set Rs = nothing
	call Fecha_Conexao()
end function


' =======================================================================
sub Encerra_Session()
	Session("Id")  = ""
	Session("Nome")  = ""
	Session("Login") = ""
	Session("Tipo")  = ""
	Session("TipoId")  = ""
	Session("Desconto")  = ""
	Session("TotalCompra") = ""
	Session("ItemCount") = ""
	Session("MyShoppingCart") = ""
	Session("SubTotalCompra") = ""
end sub

' =======================================================================
function Verifica_Produto_Tabela_Auxiliar(Produto)
	dim sSql, Rs
	
	Verifica_Produto_Tabela_Auxiliar = false
	if(Produto = "") then exit function
	
	call Abre_Conexao()
	
	sSql = "Select * from TB_PRODUTOS where id = " & Produto
	set Rs = oConn.execute(sSql)
	if(not Rs.eof) then 
		if((trim(rs("Id_Tamanho")) <> "") and (not isnull(trim(rs("Id_Tamanho"))))) then
			Verifica_Produto_Tabela_Auxiliar = true
			exit function
		end if
		if((trim(rs("Id_Cor")) <> "") and (not isnull(trim(rs("Id_Cor"))))) then
			Verifica_Produto_Tabela_Auxiliar = true
			exit function
		end if
		if((trim(rs("Id_Sabor")) <> "") and (not isnull(trim(rs("Id_Sabor"))))) then
			Verifica_Produto_Tabela_Auxiliar = true
			exit function
		end if
		if((trim(rs("Id_Fragrancia")) <> "") and (not isnull(trim(rs("Id_Fragrancia"))))) then
			Verifica_Produto_Tabela_Auxiliar = true
			exit function
		end if
		if((trim(rs("Id_Tipos")) <> "") and (not isnull(trim(rs("Id_Tipos"))))) then
			Verifica_Produto_Tabela_Auxiliar = true
			exit function
		end if
	end if
	set Rs = nothing

	call Fecha_Conexao()
end function


' =======================================================================
function Calcula_TotalGeral_Pedidos(IdCliente)
	dim sSql, Rs, sSoma
	
	if(IdCliente = "") then exit function
	
	sSoma = 0
	
	call Abre_Conexao()

	' VERIFICA SE O EMAIL JA ESTA CADASTRADO
	sSql = "Select total from TB_PEDIDOS where Id_Cliente = " & IdCliente
	set Rs = oConn.execute(sSql)
	
	if(not Rs.eof) then
		do while not Rs.eof
			sSoma = (cdbl(sSoma) + cdbl(rs("total")))
		Rs.movenext
		loop
	end if
	set Rs = nothing

	call Fecha_Conexao()
	Calcula_TotalGeral_Pedidos = RetornaCasasDecimais(cdbl(sSoma),2)
end function


' =======================================================================
function Monta_Linhas_TipoEntrega(sSel)
	dim sSql, Rs, sChecked, sImagem, sCampo, sCampo2
	
	call Abre_Conexao()

	' VERIFICA SE O EMAIL JA ESTA CADASTRADO
	sSql = "Select * from TB_TIPO_ENTREGA (nolock) where home = 1 and cdcliente = " & sCdCliente
	set Rs = oConn.execute(sSql)
	
	if(not Rs.eof) then
		Monta_Linhas_TipoEntrega = Monta_Linhas_TipoEntrega &  "<tr>"

		do while not Rs.eof
			sImagem = ""
			if((trim(sSel) = trim(rs("id"))) OR lContador = 0) then 
				sChecked = "checked"
			end if

			sCampo = trim(rs("descricao"))			
			
			Monta_Linhas_TipoEntrega = Monta_Linhas_TipoEntrega & "  <td align=""left"" valign=""middle"" nowrap class=descriprod><input type=""radio"" name=""tipo_entrega"" value="""& rs("id") &""" "& sChecked &"></td>"
			Monta_Linhas_TipoEntrega = Monta_Linhas_TipoEntrega & "  <td align=""LEFT"" valign=""middle"" nowrap class=descriprod>"& sCampo &"<br><font size=1>"& sCampo2 &"</font></td>"

		sChecked = ""
		Rs.movenext
		loop
		
		Monta_Linhas_TipoEntrega = Monta_Linhas_TipoEntrega & "</tr>"
	end if
	set Rs = nothing

	call Fecha_Conexao()
end function



' =======================================================================
function Monta_Linhas_Forma_Pagamento_detalhes()
	dim sSql, Rs, sChecked, sImagem, sCampo, sCampo2
	
	call Abre_Conexao()

	' VERIFICA SE O EMAIL JA ESTA CADASTRADO
	sSql = "Select * from TB_FORMAS_PAGAMENTO (nolock) where iD_TIPO in ('S') and id <> 26 AND ativo = 1"
	set Rs = oConn.execute(sSql)
	
	if(not Rs.eof) then
		Monta_Linhas_Forma_Pagamento_detalhes = Monta_Linhas_Forma_Pagamento_detalhes &  "<tr>"

		do while not Rs.eof
			sImagem = ""
			sCampo 	= trim(rs("descricao"))
			sCampo2 = trim(rs("obs"))
			id 		= trim(rs("id"))

			if(trim(rs("imagem")) <> "") then 
				sImagem = "<img src=""/img/logos/"& trim(rs("imagem")) &""" alt="""">"
				sCampo = ""
				sCampo2 = ""
			end if
			
			Monta_Linhas_Forma_Pagamento_detalhes = Monta_Linhas_Forma_Pagamento_detalhes & "<td align=""left"" valign=""middle"" nowrap class=descriprod>&nbsp;</td>"
			Monta_Linhas_Forma_Pagamento_detalhes = Monta_Linhas_Forma_Pagamento_detalhes & "<td align=""LEFT"" valign=""middle"" nowrap class=descriprod><span class=""descriprod"">"& sCampo &"<br>"& sCampo2 &"</font>"& sImagem &"</td>"
		Rs.movenext
		loop

		Monta_Linhas_Forma_Pagamento_detalhes = Monta_Linhas_Forma_Pagamento_detalhes & "</tr>"
	end if
	set Rs = nothing

	call Fecha_Conexao()
end function


' ================================================================================
function Efetua_Login(Login, Senha, prCpf, prCnpj)
	dim sSql, Rs

    Dim objCriptografia
    Set objCriptografia = New Criptografia

	Efetua_Login = false

	call Abre_Conexao()

	' VERIFICA SE O EMAIL JA ESTA CADASTRADO
	sSql = "Select top 1 id,Nome,CPF,CNPJ,SobreNome,Email,Id_Tipo,Id_Provincia,CEP,endereco,senha from TB_CLIENTES (nolock) where Email = '"& Login &"' and ativo = 1 and cdcliente = "& sCdCliente & " order by id"
	set Rs = oConn.execute(sSql)
	
	if(not Rs.eof) then

        if (objCriptografia.QuickDecrypt(trim(rs("senha"))) = Senha) then
		    Session("Chave")  		= trim(rs("CPF"))
		    Session("Id")  			= trim(rs("id"))
		    Session("Nome")  		= trim(rs("Nome"))
		    Session("SobreNome")	= trim(rs("SobreNome"))
		    Session("Login") 		= trim(rs("Email"))
		    Session("TipoId")  		= trim(rs("Id_Tipo"))
		    Session("Id_Provincia") = trim(rs("Id_Provincia"))
		    Session("ProvinciaCep") = trim(rs("CEP"))
		    Session("Endereco")	 	= trim(rs("endereco"))
		    Session("Tipo")  		= trim(Recupera_Campos_Db_oConn("TB_CLIENTES_TIPO", rs("Id_Tipo"), "id", "descricao"))
		    Session("Desconto") 	= 0

            if(trim(rs("CNPJ")) <> "" and not isnull(trim(rs("CNPJ")))) then 
                Session("Chave") = trim(rs("CNPJ"))
            end if

            Session("Chave")    = replace(replace(replace(Session("Chave"),".",""),"-",""),"/","")
		    Efetua_Login        = true
        else
            Efetua_Login = false
        end if
	end if
	set Rs = nothing
	Set objCriptografia = Nothing

	call Fecha_Conexao()
end function


' =======================================================================
Function GeraSenha(nNoChars, sValidChars)
    Const szDefault = "abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUVXYZ0123456789"
    Dim nCount, sRet, nNumber, nLength

    Randomize

    If sValidChars = "" Then
	    sValidChars = szDefault
    End If

    nLength = Len( sValidChars )

    For nCount = 1 To nNoChars
	    nNumber = Int((nLength * Rnd) + 1)
	    sRet = sRet & Mid( sValidChars, nNumber, 1 )
    Next
	Password_GenPass = sRet
	GeraSenha = Password_GenPass
End Function
'===Função Responsável por verificar se cadastro já existe=========================

sub Esqueceu_Acesso(sCpf_Cnpj)
    dim sSql, sSql1, Rs,sCampo,pode_executar
	pode_executar = true

    'Validando o cpf ou cnpj do usuário======================================//

    'Aqui verifico se o dado veiu preechido
    if(sCpf_Cnpj = "") then 
     pode_executar = false
     'exit sub
      else
         'verificando se é apenas número-----------------------------------//
         if (not isnumeric(sCpf_Cnpj))Then
         pode_executar = false
          else
            'Validando Tamanho Campo
	         if (cdbl(len(sCpf_Cnpj))<= 11) then
	         sCampo = "cpf"
             pode_executar = true
	          elseif(cdbl(len(sCpf_Cnpj)) > 11 and cdbl(len(sCpf_Cnpj)) <= 15) then
               pode_executar = true
	           sCampo = "cnpj"
             else
              pode_executar = false
	         end if 
           end if 
    end if 
    

	
	'==========================================================================//

     if pode_executar = true then
	  call abre_conexao()

       sSql = "Select * from TB_CLIENTES where " & sCampo & " = '"& sCpf_Cnpj &"' and ativo = 1 and cdcliente = " & scdcliente
	  ' response.Write sSql
       set Rs = oConn.execute(sSql)
	    
       if(not rs.eof) then
		
	    RetornarEmail = trim(Rs("email"))

       
        html_primeiro_contato = html_primeiro_contato  & "<div class=""TextoMedioVerde"" style=""margin-bottom:7px; margin-top:10px; padding-left:19px;"">MINHA PRIMEIRA COMPRA</div>"
        html_primeiro_contato = html_primeiro_contato  & "<div class=""TextoGrande"" style=""margin-bottom:7px; margin-top:10px; padding-left:20px;"">Este <strong>CPF</strong> ou <strong>CNPJ</strong> já esta cadastrado</div>"
        html_primeiro_contato = html_primeiro_contato  & "<div style=""padding-left:20px;"">Por favor, informe os dados abaixo e resgate o seu e-mail e senha de cadastro.</div>"
      
		
        else
        response.Redirect(PathHttps & "cadastro.asp")
        

        end if
        set Rs = nothing

	    call fecha_conexao()

        else'caso pode_executar seja falso então retorna mensagem de erro para o usuário

     

        html_primeiro_contato = html_primeiro_contato  & "<form action=""" & sPathHttps & "cadastro.asp"" id=""formularioContato2"" name=""form1"">"
        html_primeiro_contato = html_primeiro_contato  & "<div class=""TextoMedioVerde"" style=""margin-bottom:7px; margin-top:10px; padding-left:19px;"">MINHA PRIMEIRA COMPRA</div>"
        html_primeiro_contato = html_primeiro_contato  & "<input type=""hidden"" name=""enviado3"" value=""ok""/>"
        html_primeiro_contato = html_primeiro_contato  & "<div class=""TextoGrande"" style=""margin-bottom:7px; margin-top:10px; padding-left:20px;"">Indique seu <strong>CPF</strong> ou <strong>CNPJ</strong>:</div>"
        html_primeiro_contato = html_primeiro_contato  & "<div style=""color:#006600; margin-bottom: 7px;  margin-left:230px; padding-top:15px;""><span>(apenas números)</span></div>"
        html_primeiro_contato = html_primeiro_contato  & "<div class=""Texto"" style=""padding-top:15px; padding-left:130px;""><input name=""logar2"" type=""image"" class=""semborda"" id=""logar"" value=""Submit"" style=""border:none;"" src=""images/bot_ok_primeira_conta.gif"" alt=""Clique Aqui para Cadastrar!"" width=""74"" height=""25""></div>"
        html_primeiro_contato = html_primeiro_contato  & "<div class=""Texto"" style=""margin-top: -63px; padding-left:0px;""><input name=""cpfl"" type=""text"" size=""37"" maxlength=""14"" style=""border:1px solid; margin-left: 23px; margin-top: -02px;"" class=""label""> </div> "
        html_primeiro_contato = html_primeiro_contato  & "</form>"
        html_primeiro_contato = html_primeiro_contato  & "<div id=""resposta_cnpj_cpf"" style=""clear:both;color:red;font-size:10px;padding-left:23px;"">*CPF OU CNPJ INVÁLIDO.</div>"

      end if 

end sub

' =======================================================================
'========================================================================

sub Esqueceu_Email(sCpf_Cnpj)
    dim sSql, sSql1, Rs,sCampo,pode_executar
	pode_executar = true
	
    'Validando o cpf ou cnpj do usuário======================================//

    'Aqui verifico se o dado veiu preechido
    if(sCpf_Cnpj = "") then 
     pode_executar = false
     'exit sub
      else
         'verificando se é apenas número-----------------------------------//
         if (not isnumeric(sCpf_Cnpj))Then
         pode_executar = false
          else
            'Validando Tamanho Campo
	         if (cdbl(len(sCpf_Cnpj))<= 11) then
	         sCampo = "cpf"
             pode_executar = true
	          elseif(cdbl(len(sCpf_Cnpj)) > 11 and cdbl(len(sCpf_Cnpj)) <= 15) then
               pode_executar = true
	           sCampo = "cnpj"
             else
              pode_executar = false
	         end if 
           end if 
    end if 
    

	
	'==========================================================================//

     if pode_executar = true then
	  call abre_conexao()

       sSql = "Select * from TB_CLIENTES where " & sCampo & " = '"& sCpf_Cnpj &"' and ativo = 1 and cdcliente = " & scdcliente
	   'response.Write sSql
       set Rs = oConn.execute(sSql)
	
       if(not rs.eof) then
		
	    RetornarEmail = trim(Rs("email"))

        'sMsgJsEsqueceu1 = sMsgJsEsqueceu1 & "<div class=""EsqueciEmail2"" style=""border: 0px solid;""> "
        'sMsgJsEsqueceu1 = sMsgJsEsqueceu1 & "<form name=""form4""   method=""post"" action=""" &  Request.ServerVariables("Script_Name") & """ id=""FormularioEsqueciEmail"">"
        'sMsgJsEsqueceu1 = sMsgJsEsqueceu1 & "<input type=""hidden"" name=""enviado2"" value=""ok"">"
        'sMsgJsEsqueceu1 = sMsgJsEsqueceu1 & "<BR>"
       ' sMsgJsEsqueceu1 = sMsgJsEsqueceu1 & "<div style=""color:#007f51;"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>ESQUECI MEU E-MAIL</strong></div>"
       ' sMsgJsEsqueceu1 = sMsgJsEsqueceu1 & "<BR>"
       ' sMsgJsEsqueceu1 = sMsgJsEsqueceu1 & ""
       ' sMsgJsEsqueceu1 = sMsgJsEsqueceu1 & "<div style=""color:#000;"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font size=""2"">Seu <span style=""color:#007f71"">E-MAIL</span> cadastrado é:</font></div>"
       ' sMsgJsEsqueceu1 = sMsgJsEsqueceu1 & "<BR>"
       ' sMsgJsEsqueceu1 = sMsgJsEsqueceu1 & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font size=""2"">" & RetornarEmail & "</font>"
        'sMsgJsEsqueceu1 = sMsgJsEsqueceu1 & "</div><!--Fecha EsqueciEmail2-->"

         sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<div style=""color:#007f51;""><strong>ESQUECI MEU E-MAIL</strong></div>"
         sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<div style=""color:#000;padding-top:10px;"">Seu e-mail cadastrado é: </div>"
         sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<div style=""color:#000;padding-top:15px;font-weight:bold;font-size:16px;"">" & RetornarEmail & "</div>"
         'sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<div class=""BlocoCampoTexto"" style=""float:left;""><input onkeyup=""javascript:verifica_digito(2)"" onkeypress="return isNumeric(event);" name=""cpf_cnpj"" type=""text"" class=""seuemail"" size=""50"" validar=1 style=""border:1px solid #000;""></div>"
        ' sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<div class=""botaoenviar"" style=""margin-left:270px; margin-top:-5px;""><input type=""image"" id=""enviar"" name=""imageField2"" class=""submit"" src=""./css/imagem/bt_enviar.gif""></div>"
         sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "</form>"

        Classe = "EsqueciEmail" 

	   ' sMsgJsEsqueceu1 = RetornarEmail
	   'HtmlEsqueceu = HtmlEsqueceu & "<div style=""margin-left: 20px""><font color=""#007f51"">ESQUECI MEU E-MAIL</font></div><BR>"
	   'HtmlEsqueceu = HtmlEsqueceu & "<div style=""margin-top: 4px; margin-left: 20px color:#000;"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Seu e-mail cadastrado é:</div>"
	 	
		
	 
	    else
	     'sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<div style=""color:#007f51;""><strong>ESQUECI MEU E-MAIL</strong></div>"
         'sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<BR>"
         'sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<div style=""color:#000;""><strong>Seu CPF ou CNPJ:</strong>&nbsp;&nbsp;<font color=""#007f51"">(apenas números)</font></div>" 
         'sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<BR>"
         'sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<div class=""BlocoCampoTexto"" style=""float:left;""><input onkeyup=""javascript:verifica_digito(2)"" name=""cpf_cnpj"" type=""text"" class=""seuemail"" size=""50"" validar=1 style=""border:1px solid #000;""></div>"
         'sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<div class=""botaoenviar"" style=""margin-left:270px; margin-top:-5px;""><input type=""image"" id=""enviar"" name=""imageField2"" class=""submit"" src=""./css/imagem/bt_enviar.gif""></div>"        
		 'sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "</form>"	

         sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<div style=""color:#007f51;""><strong>ESQUECI MEU E-MAIL</strong></div>"
         sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<div style=""color:#000;padding-top:10px;"">Digite abaixo o <span style=""color:#007f71"">CPF</span> ou <span style=""color:#007f71"">CNPJ</span> que cadastrou na Loja Hunter Fan. </div>        "
         sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<div style=""color:#000;padding-top:15px;""><strong>Seu CPF ou CNPJ:</strong>&nbsp;&nbsp;<font color=""#007f51"">(apenas números)</font></div>"
         sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<div class=""BlocoCampoTexto"" style=""float:left;""><input onkeyup=""javascript:verifica_digito(2)"" name=""cpf_cnpj"" type=""text"" class=""seuemail"" size=""50"" validar=1 style=""border:1px solid #000;""></div>"
         sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<div class=""botaoenviar"" style=""margin-left:270px; margin-top:-5px;""><input type=""image"" id=""enviar"" name=""imageField2"" class=""submit"" src=""./css/imagem/bt_enviar.gif""></div>"
         sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "</form>"

	     
         sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<div id=""resposta_cnpj_cpf"" style=""float:left;clear:both;color:red;font-size:10px;""><br/><b>CPF OU CNPJ NÃO LOCALIZADO.</b><BR>Não localizamos seu CPF ou CNPJ em nosso sistema. Por favor tente novamente ou entre em contato conosco.</div>"
        ' sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "</div><!--Fecha EsqueciEmail2-->"        ' sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<BR>"
        ' sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font size=""2"">" & sMsgJsEsqueceu1 & "</font>"
         Classe = "EsqueciEmail"

        end if
        set Rs = nothing

	    call fecha_conexao()

        else'caso pode_executar seja falso então retorna mensagem de erro para o usuário


         
         sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<div style=""color:#007f51;""><strong>ESQUECI MEU E-MAIL</strong></div>"
         sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<BR>"
         sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<div style=""color:#000;""><strong>Seu CPF ou CNPJ:</strong>&nbsp;&nbsp;<font color=""#007f51"">(apenas números)</font></div>" 
         sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<BR>"
         sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<div class=""BlocoCampoTexto"" style=""float:left;""><input onkeyup=""javascript:verifica_digito(2)"" name=""cpf_cnpj"" type=""text"" class=""seuemail"" size=""50"" validar=1 style=""border:1px solid #000;""></div>"
         sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<div class=""botaoenviar"" style=""margin-left:270px; margin-top:-5px;""><input type=""image"" id=""enviar"" name=""imageField2"" class=""submit"" src=""./css/imagem/bt_enviar.gif""></div>"
         sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "</form>"	
	     
         sMsgJsEsqueceu1 = sMsgJsEsqueceu1 & "<div id=""resposta_cnpj_cpf"" style=""float:left;clear:both;color:red;font-size:10px;"">*CPF OU CNPJ INVÁLIDO.</div>"
         'sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "</div><!--Fecha EsqueciEmail2-->"
         Classe = "EsqueciEmail"


         'sMsgJsEsqueceu1	= "<div style=""float:left;clear:both;"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CPF OU CNPJ NÃO LOCALIZADO.<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Não localizamos seu CPF ou CNPJ em nosso sistema. Por favor <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tente novamente ou entre em contato conosco.</div>"
         'sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "div style=""color:#000;"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font size=""2"">Seu <span style=""color:#007f71"">E-MAIL</span> cadastrado é:</font></div>"
         'sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "<BR>"
         'sMsgJsEsqueceu1 = sMsgJsEsqueceu1 &  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font size=""2"">" & sMsgJsEsqueceu1 & "</font>"
         


      end if 


end sub


' =======================================================================
sub EnviarLinkProdutos(prProduto, prNome, prEmail)
    dim sSql, sSql1, Rs, sCorpo_Msg, sAssuntoEnvio, sSenhaNova

	if(prProduto = "") then exit sub 

	sCorpo_Msg = "<html>"
	sCorpo_Msg = sCorpo_Msg & "<head>"
	sCorpo_Msg = sCorpo_Msg & "<meta http-equiv=""Content-Type"" content=""text/html; charset=iso-8859-1"" >"
	sCorpo_Msg = sCorpo_Msg & "<title>"& Application("NomeLoja") &"</title>"
	sCorpo_Msg = sCorpo_Msg & "</head>"
	sCorpo_Msg = sCorpo_Msg & "<body>"
	sCorpo_Msg = sCorpo_Msg & "<div id=""wrap"">"
	sCorpo_Msg = sCorpo_Msg & "<div id=""header"">"
	sCorpo_Msg = sCorpo_Msg & "<h1>Produto em Destaque</h1>"
	sCorpo_Msg = sCorpo_Msg & "</div>"
	sCorpo_Msg = sCorpo_Msg & "    <div id=""content"" class=""indique"">"
	sCorpo_Msg = sCorpo_Msg & "    	<p><strong>"& prNome &",</strong></p>"
    sCorpo_Msg = sCorpo_Msg & "    	<p>Segue link desejado: <a href="""& sPathHttps &"detalhe-produto/produto/"& prProduto &""">"& Recupera_Campos_Db("TB_PRODUTOS", prProduto, "id", "Descricao") &"</a></p>"
	sCorpo_Msg = sCorpo_Msg & "     Equipe <b>"& Application("Nome") &"</b><br><br>"
	sCorpo_Msg = sCorpo_Msg & "     Este é um e-mail automático, não é necessário respondê-lo."
	sCorpo_Msg = sCorpo_Msg & "    </div>"
	sCorpo_Msg = sCorpo_Msg & "  </div>"	
	sCorpo_Msg = sCorpo_Msg & "</body>"
	sCorpo_Msg = sCorpo_Msg & "</html>"	

	sAssuntoEnvio   = Application("Nome") & " - Produto em Destaque" 
	
	call Envia_Email(prEmail, Application("Mailpedidos"), Application("NomeLoja"), sAssuntoEnvio, sCorpo_Msg)
end sub

' =======================================================================
sub Esqueceu_Senha(sEmail)
    dim sSql, sSql1, Rs, sCorpo_Msg, sAssuntoEnvio, sSenhaNova

    Dim objCriptografia
    Set objCriptografia = New Criptografia

	if(sEmail = "") then exit sub 

	call abre_conexao()

    sSql = "Select top 1 * from TB_CLIENTES (nolock) where email = '"& sEmail &"' and ativo = 1 and cdcliente = " & scdcliente
    set Rs = oConn.execute(sSql)
	
    if(not rs.eof) then
		sSenhaNova1 = GeraSenha(5,"")
        sSenhaNova = objCriptografia.QuickEncrypt(sSenhaNova1)

		call abre_conexao()

		sSql1 = "UPDATE TB_CLIENTES SET SENHA = '"& sSenhaNova &"' where id = "& trim(Rs("id")) & " and email = '" & sEmail & "' and cdcliente = "& scdcliente
		oConn.execute(sSql1)

		sCorpo_Msg = "<html>"
		sCorpo_Msg = sCorpo_Msg & "<head>"
		sCorpo_Msg = sCorpo_Msg & "<meta http-equiv=""Content-Type"" content=""text/html; charset=iso-8859-1"" >"
		sCorpo_Msg = sCorpo_Msg & "<title>"& Application("NomeLoja")&"</title>"
		sCorpo_Msg = sCorpo_Msg & "</head>"
		sCorpo_Msg = sCorpo_Msg & "<body>"
		sCorpo_Msg = sCorpo_Msg & "<div id=""wrap"">"
		sCorpo_Msg = sCorpo_Msg & "<div id=""header"">"
		sCorpo_Msg = sCorpo_Msg & "<h1><a href="""& Application("URLdaLoja") &"""><img src="""& Application("URLdaLoja") &"/img/logo.png"" alt="""& ucase(Application("Nome")) &""" border=""0""></a></h1>"
		sCorpo_Msg = sCorpo_Msg & "</div>"
		sCorpo_Msg = sCorpo_Msg & "    <div id=""content"" class=""indique"">"
		sCorpo_Msg = sCorpo_Msg & "    	<p>Olá, <strong>"& trim(rs("nome")) & "!</strong></p><br>"

        sCorpo_Msg = sCorpo_Msg & "    	<p>Recebemos sua solicitação para envio de uma nova senha de acesso à <strong>Loja "& Application("Nome") &"</strong>.</p>"
        sCorpo_Msg = sCorpo_Msg & "    	<p>Para a sua segurança, acesse <a href="""& sPathHttps &"arearestrita"" style=""color:#000;font-weight:bold;"">"& sPathHttps &"arearestrita</a>, efetue seu login com a senha informada abaixo e clique em<br> ""Alterar senha de acesso"" para substituir por uma combinação de sua preferência.</p>"
        sCorpo_Msg = sCorpo_Msg & "    	<p>Sua identificação: "& trim(rs("email")) &" <br>"
        sCorpo_Msg = sCorpo_Msg & "    	Senha temporária: &nbsp;"& objCriptografia.QuickDecrypt(sSenhaNova) &" </p>"

        sCorpo_Msg = sCorpo_Msg & "<a href=""https://www.foodcrowd.com.br/produtos"" >Conheça nossos produtos!</a><br><br>"
        sCorpo_Msg = sCorpo_Msg & "Um grande abraço.<br>"
        sCorpo_Msg = sCorpo_Msg & "Equipe <b>"& Application("Nome") &"</b><br><br>"

		sCorpo_Msg = sCorpo_Msg & "    </div>"
		sCorpo_Msg = sCorpo_Msg & "  </div>"	
		sCorpo_Msg = sCorpo_Msg & "</body>"
		sCorpo_Msg = sCorpo_Msg & "</html>"	

        'response.Write sCorpo_Msg
        'response.End

		sMsgJs 			= "E-mail enviado com Sucesso!\n\nPara: "& sEmail&"" 
		sAssuntoEnvio   = "Envio de Senha - Esqueceu sua Senha - " & Application("Nome") 
	
		call Envia_Email(sEmail, Application("Mailpedidos"), Application("NomeLoja"), sAssuntoEnvio, sCorpo_Msg)
    end if
    set Rs = nothing
    Set objCriptografia = Nothing
	
    call fecha_conexao()
end sub


' =======================================================================
function Calcula_TipoEntrega(sProvinciaUser, prOpcTipo)
	dim Rs, sSQl, lCont, sCampoWhere
	dim sTotalPeso, sDe, sAte, nValorManuseio
	
	Calcula_TipoEntrega = 0
	exit function
	
	Calcula_TipoEntrega = 0
	nValorManuseio = 0
	sCampoWhere = "descricao"
	
	' SE O PARAMENTRO CEP ESTIVER VAZIO ---------------------------------------
	if(sProvinciaUser = "" or prOpcTipo = "") then 
		exit function
	end if
	' --------------------------------------------------------------------------
		
	' --------------------------------------------------------------------------
	' RECUPERA O VALOR DO PESO TOTAL DA COMPRA ---------------------------------
	sTotalPeso = 0
	for lCont = 1 to Session("ItemCount")
		sTotalPeso = (cdbl(sTotalPeso) + cdbl(ARYshoppingcart(C_Peso, lCont)))
	next
	
	if(sProvinciaUser = "") then
		exit function
	else
		call Abre_Conexao()
		
		sSQl = "Select * from TB_TIPOENTREGA_CEP " & _ 
				"where Id_TipoEntrega = "& prOpcTipo &" AND ('"& sProvinciaUser &"' between CepIni and CepFim) and (ativo = 1) order by Id_TipoEntrega"
		set Rs = oConn.execute(sSQl)

		if(not rs.eof) then
			do while not rs.eof
				sDe = cdbl(rs("fretede"))
				sAte = cdbl(rs("freteate"))

				if((sTotalPeso >= sDe) and (sTotalPeso <= sAte)) then
					Calcula_TipoEntrega = (RetornaCasasDecimais(cdbl(rs("Valor")),2))
					exit function
				end if
			rs.movenext
			loop
		end if
		
		set Rs = nothing
		call fecha_Conexao()
	end if
end function

' =======================================================================
function Retorna_Estoque_Produto_Combo(NameCombo, Valores, sMarcaValor, IdProduto, CodAbacos, prMobile)
	dim sConteudo, x, sSelected
	dim nQteMinima, nQteMaxima
	
	sConteudo = sConteudo & "<select name="""& NameCombo &""" id="""& NameCombo &""" onchange=""javascript: Recalcular("& IdProduto &", '"& prMobile &"')"" style=""width: 51px;"">"
	
	call Abre_Conexao()

	if (CodAbacos <> "") then
		nQteMaxima = Recupera_Campos_Db_oConn("TB_IMAGENS_AUXILIARES", "'" & CodAbacos & "'", "CodAbacos", "Qtd_Estoque")
	else
		nQteMaxima = Recupera_Campos_Db_oConn("TB_PRODUTOS", "'" & IdProduto & "'", "id", sCampoQtedMaxima)
	end if
	
	nQteMinima = Recupera_Campos_Db_oConn("TB_PRODUTOS", "'" & IdProduto & "'", "id", sCampoQtedMinima)

	if(cint(nQteMaxima) > 0)  then
        if(cint(nQteMaxima) > Valores)  then nQteMaxima = Valores
    else
        nQteMaxima = Valores
    end if

    if(cint(nQteMinima) = 0)        then nQteMinima = 1

	for x = nQteMinima to nQteMaxima
		'if(cint(x) <= cint(Valores)) then 
			if(sMarcaValor = x) then 
				sSelected = " selected "
			else
				sSelected = ""
			end if
	
			sConteudo = sConteudo & " <option value="""& x &""" "& sSelected &">"& x & "</option>"
		'end if
	next
	sConteudo = sConteudo & "</select>"

	call Fecha_conexao()

	Retorna_Estoque_Produto_Combo = sConteudo


    'response.write "NameCombo: "    & NameCombo & "<br>" 
    'response.write "Valores: "      & Valores & "<br>" 
    'response.write "sMarcaValor: "  & sMarcaValor & "<br>" 
    'response.write "IdProduto: "    & IdProduto & "<br>" 
    'response.write "CodAbacos: "    & CodAbacos & "<br>" 
    'response.write "nQteMaxima: "   & nQteMaxima & "<br>" 
    'response.write "nQteMinima: "   & nQteMinima & "<br>" 
    'response.write "Valores: "      & Valores & "<br>" 
    'response.End
end function


' =======================================================================
sub Envia_Email(sTo, sDe, sFromName, sAssunto, sCorpo)
	dim Mail, sendmail, sErro, arrSto, lCont, MailCfg
	
    'On Error Resume Next

	if(sTo = "" or sDe = "" or sAssunto = "" or sCorpo = "") then exit sub
	
    Set objCDOSYSMail   = Server.CreateObject("CDO.Message")
    Set MailCfg         = Server.CreateObject ("CDO.Configuration")
    
    'MailCfg.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver")            = "smtp.foodcrowd.com.br"
    'MailCfg.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport")        = 587
    'MailCfg.Fields("http://schemas.microsoft.com/cdo/configuration/smtpusessl")            = false
    'MailCfg.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate")      = 1
    'MailCfg.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername")          = "sistema01@foodcrowd.com.br"
    'MailCfg.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword")          = "ominic@2016"
    'MailCfg.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing")             = 2
    'MailCfg.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30

    MailCfg.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver")            = "smtp.ominic.com.br"
    MailCfg.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport")        = 587
    MailCfg.Fields("http://schemas.microsoft.com/cdo/configuration/smtpusessl")            = false
    MailCfg.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate")      = 1
    MailCfg.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername")          = "sistema01@ominic.com.br"
    MailCfg.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword")          = "ominic@2016"
    MailCfg.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing")             = 2
    MailCfg.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30

    MailCfg.Fields.update

    Set objCDOSYSMail.Configuration = MailCfg
    objCDOSYSMail.From      = """atendimento@foodcrowd.com.br"" <sistema01@ominic.com.br>"
    objCDOSYSMail.To        = sTo
    objCDOSYSMail.Subject   = sAssunto
    objCDOSYSMail.HTMLBody  = sCorpo
    objCDOSYSMail.Send 
    Set objCDOSYSMail = Nothing 

    Set objCDOSYSCon = Nothing
end sub

'========================================================================	
function Verifica_Status_Loja()
	dim Rs, sSql
	
	Verifica_Status_Loja = false
	
	call Abre_Conexao()

	sSql = "Select top 1 * from TB_FECHAR_LOJA where cdcliente = "& sCdCliente &" order by id desc"
	set Rs = oConn.execute(sSql)
	if(not Rs.Eof) then
		if(trim(rs("DescricaoAbre")) <> "" and not isnull(rs("DescricaoAbre"))) then Verifica_Status_Loja = true
	end if
	set Rs = nothing

end function


'========================================================================
function Monta_Cat_Institucional()
	dim sSql, Rs, sCampo
	dim sConteudo, sCategoria, sIdCategoria
	
	if(Idioma = "br") then
		sCampo = "Categoria"
	else
		sCampo = "Categoria_Jp"
	end if

	call Abre_Conexao()
	
	sSql = "Select * from TB_INSTITUCIONAL_CATEGORIAS where cdcliente = "& sCdCliente &" and ativo = 1 order by categoria"
	set Rs = oConn.execute(sSql)

	if(not rs.eof) then
		sContaLinha = 1
		
		do while not rs.eof
			sCategoria = trim(rs(sCampo))
			sIdCategoria = rs("id")
			
			if(sCategoria <> "") then 
				
				sCategoria = "&bull; <a href="/"institucional.asp?cat="& sIdCategoria &"""  class=""a11preto"">"& sCategoria &"</a>"
				sConteudo = sConteudo & sCategoria &"<br>" & vbcrlf

				sContaLinha = sContaLinha + 1
			end if
		rs.movenext
		loop
	end if

	set Rs = nothing
	call Fecha_Conexao()

	Monta_Cat_Institucional = sConteudo
end function


'========================================================================
function Monta_Filtros(prCat, prprTipo1FiltrosIdProdutos)
    dim sIdSugestao, sSugestaoDescricao
	dim sSql, Rs, Rs2, sCampo
	dim sConteudo, sSubCategoria, sIdCategoria, iExibe, sContaLinha, boolEsgotado, sTipos, sVTamanho
	
    if(prprTipo1FiltrosIdProdutos <> "") then 
        prprTipo1FiltrosIdProdutos              = left(trim(prprTipo1FiltrosIdProdutos), len(trim(prprTipo1FiltrosIdProdutos))-1)
        Session("prprTipo1FiltrosIdProdutos")   = prprTipo1FiltrosIdProdutos
    else
        Monta_Filtros = ""
        exit function
    end if

	iExibe      = 1
	sContaLinha = 1
	
	call abre_conexao()

	sSql = "Select DISTINCT " & _
			    "P.Id_SubCategoria,  " & _
			    "P.id_categoria,  " & _
			    "C.categoria,  " & _
			    "SC.subcategoria  " & _
		"from  " & _
				"TB_PRODUTOS P (nolock) "  & _
				" INNER JOIN TB_MOEDAS M (NOLOCK) on  P.Id_Moeda = M.id " & _
				" INNER JOIN TB_CATEGORIAS C(NOLOCK) on P.Id_Categoria = C.id AND C.Id_Tipo = 'S' AND C.Ativo = 1  AND isnull(C.UrlEspecial, '') not like 'http%' " & _
				" INNER JOIN TB_SUBCATEGORIA SC (NOLOCK) on P.Id_SubCategoria = SC.id AND SC.Ativo = 1 " & _
		"where " &_
                " P.cdcliente = "& sCdCliente

    if(prCat <> "") then
        sSql = sSql & " AND P.id_categoria in ("& prCat &") "
    else
        if(prprTipo1FiltrosIdProdutos <> "") then   sSql = sSql & " AND P.id in ("& prprTipo1FiltrosIdProdutos &") "
    end if

    sSql = sSql & " AND P.ATIVO = 1 order by C.categoria "

    ' response.write sSql
    set Rs2 = oConn.execute(sSql)

	if(not rs2.eof) then
		do while not rs2.eof
			categoria 		= ucase(trim(rs2("categoria")))
			subcategoria 	= CortaLen(trim(rs2("subcategoria")),17)
			sCategoria		= rs2("id_categoria")
			sSubCategoria  	= rs2("id_subcategoria")
			
            if(trim(categoria) <> trim(categoriaAux)) then
                sConteudo = sConteudo & "<h2 class=""title-filtro"">"& categoria &"</h2>"
            end if
    
            sConteudo = sConteudo & "<a href=""/subcategoria/"& TrataSEO(trim(rs2("subcategoria"))) &"/"& sSubCategoria &"""> > "& subcategoria &"</a>"


            categoriaAux    = categoria
			iExibe          = iExibe + 1
			sContaLinha     = sContaLinha + 1
		rs2.movenext
		loop		
	end if 
	set Rs2 = nothing
	
	call fecha_conexao()

    Monta_Filtros = sConteudo
end function


'========================================================================
function Monta_Vitrine_Default()
    dim sIdSugestao, sSugestaoDescricao
	dim sSql, Rs, Rs2, sCampo
	dim sConteudo, sSubCategoria, sIdCategoria, iExibe, sContaLinha, boolEsgotado, sTipos, sVTamanho
	
	iExibe = 1
	sContaLinha = 1
	
	call abre_conexao()

	sSql = "Select " & _
			"P.Id,  " & _
			"P.Id_SubCategoria,  " & _
			"P.id_categoria,  " & _
			"P.Descricao,  " & _
			"P.Destaque,  " & _
			"P.Imagem,  " & _
			"P.promocao,  " & _
			"P.DataIniPromo,  " & _
			"P.DataFimPromo,  " & _
			"P."& sCampoPreco &" as Preco,  " & _
			"P.Preco_Aprazo,  " & _
			"P.DescontoValor,  " & _
			"P.DescontoIgnorar,  " & _
			"P.Qte_Parcelamento,  " & _
			"P.Qte_Estoque,  " & _
			"P.DataIniDesa,  " & _
			"P.DataFimDesa,  " & _
            "P.BoolDica,  " & _
			"P.DataIniPreVenda,  " & _
			"P.DataFimPreVenda,  " & _
			"P.DescontoTipo,  " & _
			"P.PreVenda,  " & _
			"P.id_tamanho,  " & _
			"P.UrlCompra,  " & _
			"C.categoria,  " & _
			"SC.subcategoria,  " & _
			"isnull(P.id_tipos, '') id_tipos,  " & _
			"M.Moeda,  " & _
			"M.Simbolo  " & _
		"from  " & _
				"TB_PRODUTOS P (nolock) "  & _
				" INNER JOIN TB_MOEDAS M (NOLOCK) on  P.Id_Moeda = M.id " & _
				" INNER JOIN TB_CATEGORIAS C(NOLOCK) on P.Id_Categoria = C.id AND C.Id_Tipo = 'S' AND C.Ativo = 1  AND isnull(C.UrlEspecial, '') not like 'http%' " & _
				" INNER JOIN TB_SUBCATEGORIA SC (NOLOCK) on P.Id_SubCategoria = SC.id AND SC.Ativo = 1 " & _
		"where " &_
                " P.cdcliente = "& sCdCliente &" " &_
                " AND P.ATIVO = 1" &_
                " AND P.id_tipos not in (126) 	" &_
                " AND isnull(P.Imagem, '') <> '' " & _

                
		" order by  P.ID desc "

    set Rs2 = oConn.execute(sSql)

	if(not rs2.eof) then
		do while not rs2.eof
			
            sIdProd 		= trim(rs2("id"))
			sDescricao 		= trim(rs2("Descricao"))
			sDestaque 		= trim(rs2("Destaque"))
			sSimbolo 		= Server.aspEncode(trim(rs2("simbolo")))
			sPreco 			= RetornaCasasDecimais(rs2("preco"),2)
			sPrecoPrazo 	= RetornaCasasDecimais(rs2("preco_aprazo"),2)
			sParcelamento 	= trim(rs2("Qte_Parcelamento"))
			sTipos 			= trim(rs2("id_tipos"))
			sVTamanho		= trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "id_tamanho"))
			sCategoria		= TrataSEO(rs2("id_categoria"))
			sSubCategoria  	= TrataSEO(rs2("id_subcategoria"))
			sDescricaoSEO	= TrataSEO(rs2("Descricao"))
			boolEsgotado 	= (Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "Qte_Estoque") = 0 or Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "Qte_Estoque") < 0)
			sUrlCompra		= trim(rs2("UrlCompra"))

			sImagem = trim(rs2("imagem"))
			sImagem = Alterar_Tamanho_Imagem("", "", "", trim(rs2("imagem")), 200, 200)
			if(sImagem = "" or isnull(sImagem)) then sImagem = "img_indisponivel.jpg"
			
			if(trim(rs2("DataIniPreVenda")) <> "" and not isnull(rs2("DataIniPreVenda"))) then sDataPreVendaIni = cdate(rs2("DataIniPreVenda"))
			if(trim(rs2("DataFimPreVenda")) <> "" and not isnull(rs2("DataFimPreVenda"))) then sDataPreVendaFim = cdate(rs2("DataFimPreVenda"))
			
			boolPreVenda = false
			
			if(trim(rs2("PreVenda")) = "1") then 
				boolPreVenda = (date >= sDataPreVendaIni AND date <= sDataPreVendaFim)
			end if
			
			Session(sIdProd) = sPreco
			
			if( trim(rs2("promocao")) = "1") then 
				if(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DescontoValor") <> "" and Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DescontoValor") <> "0") then 
					Session("d" & sIdProd) = cdbl(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DescontoValor"))
				end if
			'else
				'Session("d" & sIdProd) = 0
			end if
			
			if(trim(rs2("DataIniDesa")) <> "" and not isnull(rs2("DataIniDesa"))) then sDataDesaIni = cdate(rs2("DataIniDesa"))
			if(trim(rs2("DataFimDesa")) <> "" and not isnull(rs2("DataFimDesa"))) then sDataDesaFim = cdate(rs2("DataFimDesa"))
			if(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DataIniPromo")) <> "" and not isnull(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DataIniPromo"))) then 
				BoolDataPromocao = ((cdate(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DataIniPromo")) <= Date) and (cDate(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DataFimPromo")) >= Date))
			end if

                sConteudo = sConteudo & "<li>" & vbcrlf 

				if (sUrlCompra = "" or isnull(sUrlCompra)) then sConteudo = sConteudo & "<a href="/"/produto/"& sDescricaoSEO &"-"& sIdProd &""" title="""& sDescricao &""">" & vbcrlf 				

				if((right(lcase(sImagem),4) = ".wmv")) then 
					sConteudo = sConteudo & "<div class=""product-image""><EMBED SRC="""& sLinkImagem &"/EcommerceNew/upload/produto/"& sImagem &""" AUTOSTART=""true"""" VOLUME=""100"" LOOP=""true"" CONTROLLER=""true"" ></div>"
				else
					sConteudo = sConteudo & "<div class=""product-image""><img src="""& sLinkImagem &"/EcommerceNew/upload/produto/"& sImagem &""" width=""200"" height=""200"" alt="""& sDescricao &"""></div>" & vbcrlf 
				end if

                sConteudo = sConteudo & "<div class=""carousel-body"" >" 
                
                if(Verifica_Status_Loja()) then
					if(boolEsgotado) then
						sConteudo = sConteudo & "<div class=""esgotado"">Esgotado</div>" & vbcrlf 
					end if
				end if

			    sConteudo = sConteudo & "<div class=""product-name""><h3 class=""title"">"& sDescricao &"</h3></div>" & vbcrlf 
				
                sConteudo = sConteudo & "<div class=""price"">"
				'if (not boolEsgotado) then
					if(mostrar_desconto_produto(sIdProd)) then 
						IF(Session("d" & sIdProd) > 0 OR Session("dc" & sIdProd) > 0) THEN
							if(ucase(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DescontoTipo")) = "R") then 
								' DESCONTO EM REAIS -----------------
								if(Session("CupomGet") <> "") then 
									sPrecoPor = RetornaCasasDecimais((  _
												cdbl(Session(sIdProd)) -  _
												( ( cdbl(Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) +  _
													Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria"))) / 100) * cdbl(Session(sIdProd)))),2)
									sPrecoPor = RetornaCasasDecimais((sPrecoPor - Session("d" & sIdProd) - Session("dc" & sIdProd)),2)
									if(ucase(Recupera_Campos_Db_oConn("TB_CUPONS", Session("CupomGet"), "Cupom", "TipoDescontoValor")) = "P") then
										sPrecoPor = RetornaCasasDecimais((cdbl(sPrecoPor) - ((cdbl(Session("Desconto") + Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) + Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria"))) / 100) * cdbl(sPrecoPor))),2)
									else
										sPrecoPor = RetornaCasasDecimais((cdbl(sPrecoPor)- ((cdbl(Session("Desconto") + Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) + Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria")))))),2)
									end if
									if(sPrecoPor < 0) then sPrecoPor = RetornaCasasDecimais(0,2)
								else
									sPrecoPor = RetornaCasasDecimais((  _
												cdbl(Session(sIdProd)) -  _
												( ( cdbl(Session("Desconto") +  _
													Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) +  _
													Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria"))) / 100) * cdbl(Session(sIdProd)))),2)

									sPrecoPor = RetornaCasasDecimais((sPrecoPor - Session("d" & sIdProd) - Session("dc" & sIdProd)),2)
								end if
							else
								' DESCONTO EM PORCENTO -----------------
								sPrecoPor = RetornaCasasDecimais((cdbl(Session(sIdProd)) - ((cdbl(Session("Desconto") + Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) + Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria")) + Session("d" & sIdProd) + Session("dc" & sIdProd)) / 100) * cdbl(Session(sIdProd)))),2)
							end if
						ELSE
							if(ucase(Recupera_Campos_Db_oConn("TB_CUPONS", Session("CupomGet"), "Cupom", "TipoDescontoValor")) = "P") then
								sPrecoPor = RetornaCasasDecimais((cdbl(Session(sIdProd)) - ((cdbl(Session("Desconto") + Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) + Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria"))) / 100) * cdbl(Session(sIdProd)))),2)
							else
								sPrecoPor = RetornaCasasDecimais((cdbl(Session(sIdProd))- ((cdbl(Session("Desconto") + Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) + Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria")))))),2)
							end if
							if(sPrecoPor < 0) then sPrecoPor = RetornaCasasDecimais(0,2)
						END IF
						
						if (sUrlCompra = "" or isnull(sUrlCompra)) then
						
							if(RetornaCasasDecimais((sPreco - sPrecoPor), 2) > 0) then 

								sConteudo = sConteudo & "<p>de: <span>"& sSimbolo &""& sPreco &"</span></p>" & vbcrlf 
								sConteudo = sConteudo & "<div>por: R$ "& RetornaCasasDecimais(sPrecoPor,2) &"</strong></div>" & vbcrlf 
								
								if(sPrecoPrazo > 0 and sParcelamento > 1) then 
									if((RetornaCasasDecimais((sPrecoPor / sParcelamento) >= (sValorParcelaMinima),2))) then
										sConteudo = sConteudo & "<div> ou <span>"& sParcelamento &" </span> x "& sSimbolo &" <strong> "& RetornaCasasDecimais((sPrecoPor / sParcelamento),2) &"</strong></div>" & vbcrlf 
									else
										if (Retorna_Parcelamento(sPrecoPor) > 1) then
											if((RetornaCasasDecimais((sPrecoPor / Retorna_Parcelamento(sPrecoPor)) >= (sValorParcelaMinima),2))) then
												sConteudo = sConteudo & "<div> ou <span>"& Retorna_Parcelamento(sPrecoPor) &" </span> x "& sSimbolo &" <strong> "& RetornaCasasDecimais((sPrecoPor / Retorna_Parcelamento(sPrecoPor)),2) &"</strong></div>" & vbcrlf 
											end if
										end if
									end if
								end if
								
							else

								if (sUrlCompra = "" or isnull(sUrlCompra)) then
									if(sPrecoPrazo > 0 and sParcelamento > 1) then 
										sConteudo = sConteudo & "<div>por:" & vbcrlf 
										sConteudo = sConteudo & "<strong>"& sSimbolo &" "& sPrecoPrazo &"</strong></div>" & vbcrlf 
										
										if((RetornaCasasDecimais((sPrecoPrazo / sParcelamento) >= (sValorParcelaMinima),2))) then 
											sConteudo = sConteudo & "<span class=""compretambem-vezes"">"& sParcelamento &"x "& sSimbolo &""& RetornaCasasDecimais((sPrecoPrazo / sParcelamento),2) &"</span>" & vbcrlf 
										else
											if (Retorna_Parcelamento(sPrecoPrazo) > 1) then
												if((RetornaCasasDecimais((sPrecoPrazo / Retorna_Parcelamento(sPrecoPrazo)) >= (sValorParcelaMinima),2))) then
													sConteudo = sConteudo & "<span class=""compretambem-vezes"">"& Retorna_Parcelamento(sPrecoPrazo) &"x "& sSimbolo &""& RetornaCasasDecimais((sPrecoPrazo / Retorna_Parcelamento(sPrecoPrazo)),2) &"</span>" & vbcrlf 
												end if
											end if
										end if
			
										'if (sUrlCompra = "" or isnull(sUrlCompra)) then
											'if(cdbl(sPrecoPrazo) <> cdbl(sPreco)) then 
												'sConteudo = sConteudo & "<span class=""compretambem-eco"">Economize:"& sSimbolo &""& RetornaCasasDecimais(((RetornaCasasDecimais(sPrecoPrazo - sPreco, 2) * 100) / sPrecoPrazo),2) &"% de desconto</span>" & vbcrlf 
												'sConteudo = sConteudo & "<span class=""compretambem-vezes""><a href="/"/detalhes/"& sDescricaoSEO &"-"& sIdProd &""">"& sSimbolo &" "& sPreco &"</a></span></a>" & vbcrlf 
											'end if
										
											'if ( (boolDataCampanhaFrete) AND ( (cdbl(sPreco) >= cdbl(sPrecoPromocaoFreteGratis))  )) then
												'sConteudo = sConteudo & "<span class=""compretambem-por""><a href="/"javascript:AbrePopUp('/pops/pop_fretegratis.asp','716','520')""><img src="""&sLinkImagem&"/EcommerceNew/upload/selos/"&sImgSeloFreteGratis&""" alt="""" ></a></span>" & vbcrlf 
											'end if
										'end if
									else
										sConteudo = sConteudo & "<div>por: R$" & vbcrlf 						
										sConteudo = sConteudo & "<strong>"& sSimbolo &" "& sPreco &"</strong></div>" & vbcrlf 
									end if
								end if
								
							end if

						end if

					else
						if (sUrlCompra = "" or isnull(sUrlCompra)) then
							if(sPrecoPrazo > 0 and sParcelamento > 1) then 
								sConteudo = sConteudo & "<div>por: " & vbcrlf 
								sConteudo = sConteudo & "<strong>"& sSimbolo &" "& sPrecoPrazo &"</strong></div>" & vbcrlf 
								
								'if((RetornaCasasDecimais((sPrecoPrazo / sParcelamento) >= (sValorParcelaMinima),2))) then 
									'sConteudo = sConteudo & "<span class=""compretambem-vezes""><a href="/"/detalhes/"& sDescricaoSEO &"-"& sIdProd &""">"& sParcelamento &"x "& sSimbolo &""& RetornaCasasDecimais((sPrecoPrazo / sParcelamento),2) &"</a></span>" & vbcrlf 
								'else
									'if (Retorna_Parcelamento(sPrecoPrazo) > 1) then
										'if((RetornaCasasDecimais((sPrecoPrazo / Retorna_Parcelamento(sPrecoPrazo)) >= (sValorParcelaMinima),2))) then
										'	sConteudo = sConteudo & "<span class=""compretambem-vezes""><a href="/"/detalhes/"& sDescricaoSEO &"-"& sIdProd &""">"& Retorna_Parcelamento(sPrecoPrazo) &"x "& sSimbolo &""& RetornaCasasDecimais((sPrecoPrazo / Retorna_Parcelamento(sPrecoPrazo)),2) &"</a></span>"
										'end if
									'end if
								'end if
	
								'if (sUrlCompra = "" or isnull(sUrlCompra)) then
									'if(cdbl(sPrecoPrazo) <> cdbl(sPreco)) then 
										'sConteudo = sConteudo & "<span class=""compretambem-eco"">Economize:"& sSimbolo &""& RetornaCasasDecimais(((RetornaCasasDecimais(sPrecoPrazo - sPreco, 2) * 100) / sPrecoPrazo),2) &"% de desconto</span>"
										'sConteudo = sConteudo & "<span class=""compretambem-vezes""><a href="/"/detalhes/"& sDescricaoSEO &"-"& sIdProd &""">"& sSimbolo &" "& sPreco &"</a></span></a>"
									'end if
								
									'if ( (boolDataCampanhaFrete) AND ( (cdbl(sPreco) >= cdbl(sPrecoPromocaoFreteGratis))  )) then
										'sConteudo = sConteudo & "<span class=""compretambem-por""><a href="/"javascript:AbrePopUp('/pops/pop_fretegratis.asp','716','520')""><img src="""&sLinkImagem&"/EcommerceNew/upload/selos/"&sImgSeloFreteGratis&""" alt="""" ></a></span>"
									'end if
								'end if
							else
								sConteudo = sConteudo & "<div>por: "						
								sConteudo = sConteudo & "<strong>"& sSimbolo &" "& sPreco &"</strong></div>"

							end if
						end if
					end if
				'end if

                sConteudo = sConteudo & "	<div class=""detalhes"">+</div>"

				sConteudo = sConteudo & "		 </div>"
				sConteudo = sConteudo & "	  </div>"
				sConteudo = sConteudo & "	</a>"
				sConteudo = sConteudo & "</li>"

				iExibe = iExibe + 1
				sContaLinha = sContaLinha + 1
			
		rs2.movenext
		loop
			
	end if 
	set Rs2 = nothing
	
	call fecha_conexao()
    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    Monta_Vitrine_Default = sConteudo

end function
'========================================================================
function Monta_Sugestao_ProdutoV2(prTipo, prProduto,prsubcategoria)
    dim sIdSugestao, sSugestaoDescricao
	dim sSql, Rs, Rs2, sCampo
	dim sConteudo, sSubCategoria, sIdCategoria, iExibe, sContaLinha, boolEsgotado, sTipos, sVTamanho
	
	iExibe = 1
	sContaLinha = 1
	
	call abre_conexao()

'LEGENDA
'prTipo = 1 / DICAS LOJAHunter Fan
'prTipo = 2 / QUEM COMPROU, COMPROU TAMBÉM
'prTipo = 3 / ITENS RELACIONADO
'prTipo = 4 / ÚLTIMOS PRODUTOS VISITADOS
'prTipo = 6 / DICAS DE FABRICANTE
    Select Case prTipo
        Case "1"
            sIdSugestao         = monta_select_sugestao(prProduto)
            if (sIdSugestao ="") then exit function
        Case "2"
            sIdSugestao         = monta_select_maisvendidos(prProduto)
            if (sIdSugestao ="") then exit function
        Case "3"
            sIdSugestao         = monta_select_fabricante(prProduto,prsubcategoria)
            if (sIdSugestao ="") then exit function
        Case "4"
            sIdSugestao         = monta_select_ultimos_visitados(prProduto)
            if (sIdSugestao ="") then exit function
        Case "5"
            sIdSugestao         = monta_select_marcas(prProduto,prsubcategoria)
            if (sIdSugestao ="") then exit function
        Case "6"
            sIdSugestao         = monta_select_marcas2(prsubcategoria)
            if (sIdSugestao ="") then exit function
        Case "7"
            sIdSugestao         = monta_select_maisvendidos_2()
            if (sIdSugestao ="") then exit function
    End Select

    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

	sSql = "Select " & _
			"P.Id,  " & _
			"P.Id_SubCategoria,  " & _
			"P.id_categoria,  " & _
			"P.Descricao,  " & _
			"P.Destaque,  " & _
			"P.Imagem,  " & _
			"P.promocao,  " & _
			"P.DataIniPromo,  " & _
			"P.DataFimPromo,  " & _
			"P."& sCampoPreco &" as Preco,  " & _
			"P.Preco_Aprazo,  " & _
			"P.DescontoValor,  " & _
			"P.DescontoIgnorar,  " & _
			"P.Qte_Parcelamento,  " & _
			"P.Qte_Estoque,  " & _
			"P.DataIniDesa,  " & _
			"P.DataFimDesa,  " & _
			"P.DataIniPreVenda,  " & _
			"P.DataFimPreVenda,  " & _
			"P.DescontoTipo,  " & _
			"P.PreVenda,  " & _
			"P.id_tamanho,  " & _
			"P.UrlCompra,  " & _
			"C.categoria,  " & _
			"SC.subcategoria,  " & _
			"isnull(P.id_tipos, '') id_tipos,  " & _
			"M.Moeda,  " & _
			"M.Simbolo  " & _
		"from  " & _
				"TB_PRODUTOS P (nolock) "  & _
				" INNER JOIN TB_MOEDAS M (NOLOCK) on  P.Id_Moeda = M.id " & _
				" INNER JOIN TB_CATEGORIAS C(NOLOCK) on P.Id_Categoria = C.id AND C.Id_Tipo = 'S' AND C.Ativo = 1  AND isnull(C.UrlEspecial, '') not like 'http%' " & _
				" INNER JOIN TB_SUBCATEGORIA SC (NOLOCK) on P.Id_SubCategoria = SC.id AND SC.Ativo = 1 " & _
		"where " &_
                " P.cdcliente = "& sCdCliente &" " &_
                " AND P.ATIVO = 1"
       if (prTipo = "") then sSql = sSql & " and P.ID NOT IN ("& prProduto &") "
        sSql = sSql & " and P.ID IN ("& sIdSugestao &")" & _
		" order by  P.ID desc "

       ' response.write sSql
        'response.end

    set Rs2 = oConn.execute(sSql)

	if(not rs2.eof) then
		do while not rs2.eof
			
            sIdProd 		= trim(rs2("id"))
			sDescricao 		= trim(rs2("Descricao"))
			sDestaque 		= trim(rs2("Destaque"))
			sSimbolo 		= Server.aspEncode(trim(rs2("simbolo")))
			sPreco 			= RetornaCasasDecimais(rs2("preco"),2)
			sPrecoPrazo 	= RetornaCasasDecimais(rs2("preco_aprazo"),2)
			sParcelamento 	= trim(rs2("Qte_Parcelamento"))
			sTipos 			= trim(rs2("id_tipos"))
			sVTamanho		= trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "id_tamanho"))
			sCategoria		= TrataSEO(rs2("id_categoria"))
			sSubCategoria  	= TrataSEO(rs2("id_subcategoria"))
			sDescricaoSEO	= TrataSEO(rs2("Descricao"))
			boolEsgotado 	= (Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "Qte_Estoque") = 0 or Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "Qte_Estoque") < 0)
			sUrlCompra		= trim(rs2("UrlCompra"))

			sImagem = trim(rs2("imagem"))
			sImagem = Alterar_Tamanho_Imagem("", "", "", trim(rs2("imagem")), 140, 140)
			if(sImagem = "" or isnull(sImagem)) then sImagem = "img_indisponivel.jpg"
			
			if(trim(rs2("DataIniPreVenda")) <> "" and not isnull(rs2("DataIniPreVenda"))) then sDataPreVendaIni = cdate(rs2("DataIniPreVenda"))
			if(trim(rs2("DataFimPreVenda")) <> "" and not isnull(rs2("DataFimPreVenda"))) then sDataPreVendaFim = cdate(rs2("DataFimPreVenda"))
			
			boolPreVenda = false
			
			if(trim(rs2("PreVenda")) = "1") then 
				boolPreVenda = (date >= sDataPreVendaIni AND date <= sDataPreVendaFim)
			end if
			
			Session(sIdProd) = sPreco
			
			if( trim(rs2("promocao")) = "1") then 
				if(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DescontoValor") <> "" and Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DescontoValor") <> "0") then 
					Session("d" & sIdProd) = cdbl(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DescontoValor"))
				end if
			'else
				'Session("d" & sIdProd) = 0
			end if
			
			if(trim(rs2("DataIniDesa")) <> "" and not isnull(rs2("DataIniDesa"))) then sDataDesaIni = cdate(rs2("DataIniDesa"))
			if(trim(rs2("DataFimDesa")) <> "" and not isnull(rs2("DataFimDesa"))) then sDataDesaFim = cdate(rs2("DataFimDesa"))
			if(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DataIniPromo")) <> "" and not isnull(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DataIniPromo"))) then 
				BoolDataPromocao = ((cdate(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DataIniPromo")) <= Date) and (cDate(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DataFimPromo")) >= Date))
			end if
 
                
			     sConteudo = sConteudo & "<li style=""display:list-item;"">" & vbcrlf 
				

				if (sUrlCompra = "" or isnull(sUrlCompra)) then sConteudo = sConteudo & "<a href="/"/produto/"& sDescricaoSEO &"-"& sIdProd &""" title="""& sDescricao &""">" & vbcrlf 				

				if((right(lcase(sImagem),4) = ".wmv")) then 
					sConteudo = sConteudo & "<div class=""product-image""><EMBED SRC="""& sLinkImagem &"/EcommerceNew/upload/produto/"& sImagem &""" AUTOSTART=""true"""" VOLUME=""100"" LOOP=""true"" CONTROLLER=""true"" ></div>"
				else
					sConteudo = sConteudo & "<div class=""product-image""><img src="""& sLinkImagem &"/EcommerceNew/upload/produto/"& sImagem &""" width=""200"" height=""200"" alt="""& sDescricao &"""></div>" & vbcrlf 
				end if
				sConteudo = sConteudo & "<div class=""carousel-body"" > " & vbcrlf 

                if(Verifica_Status_Loja()) then
					if(boolEsgotado) then
						sConteudo = sConteudo & "<div class=""esgotado"">Esgotado</div>" & vbcrlf 
					end if
				end if

				if (sUrlCompra = "" or isnull(sUrlCompra)) then
					sConteudo = sConteudo & "<div class=""product-name"" > <h3 class=""title"">"& sDescricao &"" & vbcrlf 
                    if (boolPreVenda) then sConteudo = sConteudo & " - Pr&eacute;-venda" & vbcrlf 	
                     sConteudo = sConteudo & "</h3> </div>" & vbcrlf 			
				else
					sConteudo = sConteudo & "<div class=""product-name"" > <h3 class=""title"">"& sDescricao &"" & vbcrlf 
                    if (boolPreVenda) then sConteudo = sConteudo & " - Pr&eacute;-venda" & vbcrlf 
                     	
				end if
				
                sConteudo = sConteudo & "<div class=""price"">" & vbcrlf 
                
				'if (not boolEsgotado) then
					if(mostrar_desconto_produto(sIdProd)) then 
						IF(Session("d" & sIdProd) > 0 OR Session("dc" & sIdProd) > 0) THEN
							if(ucase(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DescontoTipo")) = "R") then 
								' DESCONTO EM REAIS -----------------
								if(Session("CupomGet") <> "") then 
									sPrecoPor = RetornaCasasDecimais((  _
												cdbl(Session(sIdProd)) -  _
												( ( cdbl(Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) +  _
													Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria"))) / 100) * cdbl(Session(sIdProd)))),2)
									sPrecoPor = RetornaCasasDecimais((sPrecoPor - Session("d" & sIdProd) - Session("dc" & sIdProd)),2)
									if(ucase(Recupera_Campos_Db_oConn("TB_CUPONS", Session("CupomGet"), "Cupom", "TipoDescontoValor")) = "P") then
										sPrecoPor = RetornaCasasDecimais((cdbl(sPrecoPor) - ((cdbl(Session("Desconto") + Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) + Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria"))) / 100) * cdbl(sPrecoPor))),2)
									else
										sPrecoPor = RetornaCasasDecimais((cdbl(sPrecoPor)- ((cdbl(Session("Desconto") + Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) + Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria")))))),2)
									end if
									if(sPrecoPor < 0) then sPrecoPor = RetornaCasasDecimais(0,2)
								else
									sPrecoPor = RetornaCasasDecimais((  _
												cdbl(Session(sIdProd)) -  _
												( ( cdbl(Session("Desconto") +  _
													Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) +  _
													Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria"))) / 100) * cdbl(Session(sIdProd)))),2)

									sPrecoPor = RetornaCasasDecimais((sPrecoPor - Session("d" & sIdProd) - Session("dc" & sIdProd)),2)
								end if
							else
								' DESCONTO EM PORCENTO -----------------
								sPrecoPor = RetornaCasasDecimais((cdbl(Session(sIdProd)) - ((cdbl(Session("Desconto") + Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) + Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria")) + Session("d" & sIdProd) + Session("dc" & sIdProd)) / 100) * cdbl(Session(sIdProd)))),2)
							end if
						ELSE
							if(ucase(Recupera_Campos_Db_oConn("TB_CUPONS", Session("CupomGet"), "Cupom", "TipoDescontoValor")) = "P") then
								sPrecoPor = RetornaCasasDecimais((cdbl(Session(sIdProd)) - ((cdbl(Session("Desconto") + Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) + Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria"))) / 100) * cdbl(Session(sIdProd)))),2)
							else
								sPrecoPor = RetornaCasasDecimais((cdbl(Session(sIdProd))- ((cdbl(Session("Desconto") + Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) + Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria")))))),2)
							end if
							if(sPrecoPor < 0) then sPrecoPor = RetornaCasasDecimais(0,2)
						END IF
						
						if (sUrlCompra = "" or isnull(sUrlCompra)) then

							if(RetornaCasasDecimais((sPreco - sPrecoPor), 2) > 0) then 
								sConteudo = sConteudo & "<p>de <span>"& sSimbolo &""& sPreco &"</span></p>" & vbcrlf 
								sConteudo = sConteudo & "<div>por: R$ <strong> "& sSimbolo &" "& RetornaCasasDecimais(sPrecoPor,2) &"</strong></div>" & vbcrlf 
							
								
								if(sPrecoPrazo > 0 and sParcelamento > 1) then 
									if((RetornaCasasDecimais((sPrecoPor / sParcelamento) >= (sValorParcelaMinima),2))) then
										sConteudo = sConteudo & "<div>ou <span>"& sParcelamento &" </span> x "& sSimbolo &"<strong>"& RetornaCasasDecimais((sPrecoPor / sParcelamento),2) &"</strong></div>" & vbcrlf 
									else
										if (Retorna_Parcelamento(sPrecoPor) > 1) then
											if((RetornaCasasDecimais((sPrecoPor / Retorna_Parcelamento(sPrecoPor)) >= (sValorParcelaMinima),2))) then
												sConteudo = sConteudo & "<div>ou <span>"& Retorna_Parcelamento(sPrecoPor) &"</span> x "& sSimbolo &"<strong>"& RetornaCasasDecimais((sPrecoPor / Retorna_Parcelamento(sPrecoPor)),2) &"</strong></div>" & vbcrlf 
											end if
										end if
									end if
								end if
							else

								if (sUrlCompra = "" or isnull(sUrlCompra)) then
									if(sPrecoPrazo > 0 and sParcelamento > 1) then 
										sConteudo = sConteudo & "<div>por: R$ <strong>" & vbcrlf 
										sConteudo = sConteudo & ""& sSimbolo &" "& sPrecoPrazo &"</strong></div>" & vbcrlf 
																	  
										if((RetornaCasasDecimais((sPrecoPrazo / sParcelamento) >= (sValorParcelaMinima),2))) then 
											sConteudo = sConteudo & "<div>ou <span>"& sParcelamento &"</span> x "& sSimbolo &"<strong>"& RetornaCasasDecimais((sPrecoPrazo / sParcelamento),2) &"</strong></div>" & vbcrlf 
										else
											if (Retorna_Parcelamento(sPrecoPrazo) > 1) then
												if((RetornaCasasDecimais((sPrecoPrazo / Retorna_Parcelamento(sPrecoPrazo)) >= (sValorParcelaMinima),2))) then
													sConteudo = sConteudo & "<div>ou <span>"& Retorna_Parcelamento(sPrecoPrazo) &"</span> x "& sSimbolo &"<strong>"& RetornaCasasDecimais((sPrecoPrazo / Retorna_Parcelamento(sPrecoPrazo)),2) &"</strong></div>" & vbcrlf 
												end if
											end if
										end if
									else
										sConteudo = sConteudo & "<div>por: R$ <strong>" & vbcrlf 						
										sConteudo = sConteudo & ""& sSimbolo &" "& sPreco &"</strong></div>" & vbcrlf 
									end if
								end if
								
							end if

						end if                           							 
					else
						if (sUrlCompra = "" or isnull(sUrlCompra)) then
							if(sPrecoPrazo > 0 and sParcelamento > 1) then 
								sConteudo = sConteudo & "<div>por: R$ <strong> " & vbcrlf 
								sConteudo = sConteudo & "" & sSimbolo &" "& sPrecoPrazo &"</strong></div>" & vbcrlf 
								
								if((RetornaCasasDecimais((sPrecoPrazo / sParcelamento) >= (sValorParcelaMinima),2))) then 
									sConteudo = sConteudo & "<div>ou <span> "& sParcelamento &"</span> x "& sSimbolo &" <strong> "& RetornaCasasDecimais((sPrecoPrazo / sParcelamento),2) &"</strong></div>" & vbcrlf 
								else
									if (Retorna_Parcelamento(sPrecoPrazo) > 1) then
										if((RetornaCasasDecimais((sPrecoPrazo / Retorna_Parcelamento(sPrecoPrazo)) >= (sValorParcelaMinima),2))) then
											sConteudo = sConteudo & "<div>ou <span>"& Retorna_Parcelamento(sPrecoPrazo) &" </span> x "& sSimbolo &"<strong> "& RetornaCasasDecimais((sPrecoPrazo / Retorna_Parcelamento(sPrecoPrazo)),2) &"</strong></div>"
										end if
									end if
								end if							
							else
								sConteudo = sConteudo & "<div>por: R$ <strong> "						
								sConteudo = sConteudo & ""& sSimbolo &" "& sPreco &"</strong></div>"
							end if
						end if
					end if
				'end if

                sConteudo = sConteudo & " <div class=""detalhes"">+</div>"
                
				sConteudo = sConteudo & "</div>"
				sConteudo = sConteudo & "</div>"
				sConteudo = sConteudo & "</a>"
				sConteudo = sConteudo & "</li>"
				
				iExibe = iExibe + 1
				sContaLinha = sContaLinha + 1
			
		rs2.movenext
		loop

		
	end if 
	set Rs2 = nothing
	
	call fecha_conexao()
    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    Monta_Sugestao_ProdutoV2 = sConteudo

end function
'========================================================================
function monta_select_sugestao(prProduto)
    
    if (prProduto = "" or isnull(prProduto)) then exit function
    Dim sSql, sRs, ResultadoIds, sIdNivelB

	sSql = "Select " & _
			"P.ID_PRODUTO, " & _
			"P.id_produto_Sugestao " & _
		 "from " & _
			 "TB_PRODUTOS_SUGESTOES P (nolock) " & _
		 "where " & _
		 	 " EXISTS (SELECT 1 FROM TB_PRODUTOS PS (nolock) WHERE P.ID_PRODUTO = PS.id and PS.ATIVO = 1) " & _
			 " AND P.ID_PRODUTO = "& prProduto &_
             " AND P.id_produto_Sugestao NOT IN ("& prProduto&")"

	set sRs = oConn.execute(sSql)

	if(not sRs.eof) then
		do while not sRs.eof
			sIdNivelB = sIdNivelB & trim(sRs("id_produto_Sugestao")) &","
		sRs.movenext
		loop
		ResultadoIds = left(sIdNivelB,Len(sIdNivelB)-1) 
        sNotIdSugestao      = ResultadoIds
    end if
	set sRs = nothing
    monta_select_sugestao = ResultadoIds
end function
'========================================================================
function monta_select_maisvendidos_2()
    
    Dim sSql, sRs, ResultadoIds, sIdNivelB

	sSql = _
			" Select distinct " & _
				" count(PI.Id_Produto) as total, PI.Id_Produto as idproduto " & _
			" from  " & _
				" TB_PEDIDOS_ITENS PI (nolock) " & _
				" INNER JOIN TB_PRODUTOS P (nolock) ON PI.Id_Produto = P.ID  and p.cdcliente = "& sCdcliente &" " & _
				" INNER JOIN TB_CATEGORIAS C (nolock) ON P.Id_Categoria = C.ID AND C.ATIVO = 1 AND c.Id_Tipo = 'S' " & _
			" group by  " & _
				" PI.Id_Produto"  & _
			" order by  " & _
				" count(PI.Id_Produto) desc "

                'response.write sSql

	set sRs = oConn.execute(sSql)

	if(not sRs.eof) then
		do while not sRs.eof
			sIdNivelB = sIdNivelB & trim(sRs("idproduto")) &","
		sRs.movenext
		loop
		ResultadoIds = left(sIdNivelB,Len(sIdNivelB)-1) 

    end if
	set sRs = nothing

    monta_select_maisvendidos_2 = ResultadoIds
end function
			
'========================================================================
function monta_select_maisvendidos(prProduto)
    
    if (prProduto = "" or isnull(prProduto)) then exit function
    
    Dim sSql, sRs, ResultadoIdsV, sIdNivelV
    ResultadoIdsV = ""
	sSQlV = "SELECT " & _
    		"  I.id_produto,count(I.id_produto) AS TOTAL" & _
			" from" & _
    		" 	tb_pedidos_itens I (NOLOCK)" & _
			" where" & _
 			"    1=1" & _
  			"    AND I.id_pedido in (select id_pedido from tb_pedidos_itens where id_produto = "&prProduto&")" & _
  			"    AND I.id_produto <> "&prProduto&" " & _
			" group by" & _
 			"     id_produto" & _
			"	  having count(I.id_produto) > 2 " & _
			" order by " & _
 			"     count(id_produto) desc" 
			
			'response.write sSQlV

    set sRs = oConn.execute(sSQlV)

			if(not sRs.eof) then
				
				do while not sRs.eof
					sIdNivelV = trim(sRs("ID_produto")) &"," & sIdNivelV
				sRs.movenext
				loop
				ResultadoIdsV = ResultadoIdsV &","& left(sIdNivelV,Len(sIdNivelV)-1)
			else
				if (trim(ResultadoIdsV) = "") then ResultadoIdsV = prProduto
			
			end if

        ResultadoIdsV  = right(ResultadoIdsV,Len(ResultadoIdsV)-1)
        
        if (sNotIdSugestao <> "") then 
            sNotIdSugestao = sNotIdSugestao & "," & ResultadoIdsF
        else
            sNotIdSugestao = ResultadoIdsV
        end if
	set sRs = nothing
	
    monta_select_maisvendidos = ResultadoIdsV
end function
'========================================================================
function monta_select_fabricante(prProduto,prsubcategoria)
    
    if (prProduto = "" or isnull(prProduto)) then exit function
	
    Dim sIdNivelF, sExibirF, ResultadoIdsF
	sSQlF = "Select  top 4"& _
			"	P.Id AS IDF,   "& _
			"	P.Id_SubCategoria,   "& _
			"	P.id_categoria,   "& _
			"	M.Moeda,   "& _
			"	M.Simbolo   "& _
			"from   "& _
			"	TB_PRODUTOS P (nolock)    "& _
			" 	INNER JOIN TB_MOEDAS M (NOLOCK) on  P.Id_Moeda = M.id and M.Ativo = 1 "& _
			"	INNER JOIN TB_CATEGORIAS C(NOLOCK) on P.Id_Categoria = C.id AND C.Id_Tipo = 'S' AND C.Ativo = 1  AND isnull(C.UrlEspecial, '') not like 'http%' "& _
			"	INNER JOIN TB_SUBCATEGORIA SC (NOLOCK) on P.Id_SubCategoria = SC.id AND SC.Ativo = 1 "& _
			"where "& _
			"	P.cdcliente = "& sCdCliente &" and (P.Id_SubCategoria = "& prsubcategoria &") and  "& _
			" 	(P.boolMostra = '' or P.boolMostra is null or P.boolMostra = 'p') and   "
	if (sNotIdSugestao <> "") then 
        sSQlF = sSQlF & " P.id not in ("& sNotIdSugestao &","& prProduto &") and   "
    else
        sSQlF = sSQlF & " P.id not in ("& prProduto &") and   "
    end if
	sSQlF = sSQlF & "	P.Ativo = 1 "
	sSQlF = sSQlF & "	oRDER BY p.ID DESC "
    set RsF = oConn.execute(sSQlF)

	if(not RsF.eof) then
		do while not RsF.eof
			sIdNivelF = sIdNivelF & trim(RsF("IDF")) &","
		RsF.movenext
		loop
		ResultadoIdsF = left(sIdNivelF,Len(sIdNivelF)-1) 
        if (sNotIdSugestao <> "") then 
            sNotIdSugestao = sNotIdSugestao & "," & ResultadoIdsF
        else
            sNotIdSugestao = ResultadoIdsF
        end if
    end if
	set RsF = nothing

    monta_select_fabricante     = ResultadoIdsF

end function
'========================================================================
function monta_select_ultimos_visitados(prProduto)
    
    if (prProduto = "" or isnull(prProduto)) then exit function
	
    Dim sIdNivelU, ResultadoIdsU

	sSQlF = "Select  "& _
			"	P.Id AS IDU,   "& _
			"	P.Id_SubCategoria,   "& _
			"	P.id_categoria,   "& _
			"	M.Moeda,   "& _
			"	M.Simbolo   "& _
			"from   "& _
			"	TB_PRODUTOS P (nolock)    "& _
			" 	INNER JOIN TB_MOEDAS M (NOLOCK) on  P.Id_Moeda = M.id and M.Ativo = 1 "& _
			"	INNER JOIN TB_CATEGORIAS C(NOLOCK) on P.Id_Categoria = C.id AND C.Id_Tipo = 'S' AND C.Ativo = 1  AND isnull(C.UrlEspecial, '') not like 'http%' "& _
			"	INNER JOIN TB_SUBCATEGORIA SC (NOLOCK) on P.Id_SubCategoria = SC.id AND SC.Ativo = 1 "& _
			"where "& _
			"	P.cdcliente = "& sCdCliente &" and  "& _
			" 	(P.boolMostra = '' or P.boolMostra is null or P.boolMostra = 'p') and   "& _
            " 	P.id in ("& prProduto &") and   "
	        if (sNotIdSugestao <> "") then sSQlF = sSQlF & " P.id not in ("& replace(sNotIdSugestao,",,",",") &") and   "
			sSQlF = sSQlF & "	P.Ativo = 1 " 

    set RsF = oConn.execute(sSQlF)
    
	if(not RsF.eof) then
		do while not RsF.eof
			sIdNivelU = sIdNivelU & trim(RsF("IDU")) &","
		RsF.movenext
		loop
		ResultadoIdsU       = left(sIdNivelU,Len(sIdNivelU)-1) 
        
        if (sNotIdSugestao <> "") then 
            sNotIdSugestao = sNotIdSugestao & "," & ResultadoIdsU
        else
            sNotIdSugestao = ResultadoIdsU
        end if
    end if
	set RsF = nothing
    
    monta_select_ultimos_visitados      = ResultadoIdsU
end function
'========================================================================
function monta_select_marcas2(prsubcategoria)
    
    'if (prsubcategoria = "" or isnull(prsubcategoria)) then exit function
	
    Dim sIdNivelF, sExibirF, ResultadoIdsF
		sSQlF = "select   " &_
                    " tb_pedidos_itens.id_produto,count(tb_pedidos_itens.id) as total " &_
                " from " &_
                    "tb_produtos (nolock) " &_
                    "inner join tb_pedidos_itens (nolock) on tb_pedidos_itens.id_produto = tb_produtos.id " &_
                "where " &_
                    "tb_produtos.cdcliente = "& sCdCliente &" " &_
                    "and tb_produtos.id_fabricante = "&prsubcategoria&" " &_
                    "and tb_produtos.BOOLDICA = 1 " &_
                "group by " &_
                    "tb_pedidos_itens.id_produto, tb_produtos.cdcliente,tb_produtos.id_subcategoria " &_
                "Order by " &_
                    "total desc"
'response.write sSQlF
'response.end
    set RsF = oConn.execute(sSQlF)

	if(not RsF.eof) then
		do while not RsF.eof
			sIdNivelF = sIdNivelF & trim(RsF("id_produto")) &","
		RsF.movenext
		loop
		ResultadoIdsF = left(sIdNivelF,Len(sIdNivelF)-1) 
        
    end if
	set RsF = nothing
    monta_select_marcas2     = ResultadoIdsF

end function
'====================================================================
function monta_select_marcas(prProduto,prsubcategoria)
    
    if (prProduto = "" or isnull(prProduto)) then exit function
	
    Dim sIdNivelF, sExibirF, ResultadoIdsF
		sSQlF = "select " &_
                    " tb_pedidos_itens.id_produto,count(tb_pedidos_itens.id) as total " &_
                " from " &_
                    "tb_produtos (nolock) " &_
                    "inner join tb_pedidos_itens (nolock) on tb_pedidos_itens.id_produto = tb_produtos.id " &_
                "where " &_
                    "tb_produtos.id_fabricante <> "&prProduto&" " &_
                    "and tb_produtos.cdcliente = "& sCdCliente &" " &_
                    "and tb_produtos.id_subcategoria = "&prsubcategoria&" " &_
                "group by " &_
                    "tb_pedidos_itens.id_produto, tb_produtos.cdcliente,tb_produtos.id_subcategoria " &_
                "Order by " &_
                    "total desc"

    set RsF = oConn.execute(sSQlF)

	if(not RsF.eof) then
		do while not RsF.eof
			sIdNivelF = sIdNivelF & trim(RsF("id_produto")) &","
		RsF.movenext
		loop
		ResultadoIdsF = left(sIdNivelF,Len(sIdNivelF)-1) 
        
    end if
	set RsF = nothing
    monta_select_marcas     = ResultadoIdsF

end function
'========================================================================
function Monta_Sugestao_Produto(prProduto,prFabricane)
	dim sSql, Rs, Rs2, sCampo
	dim sConteudo, sSubCategoria, sIdCategoria, iExibe, sContaLinha, boolEsgotado, sTipos, sVTamanho
	
	iExibe = 1
	sContaLinha = 1
	
	call abre_conexao()
	
	sSql = "Select  " & _
			"P.Id,  " & _
			"P.Id_SubCategoria,  " & _
			"P.id_categoria,  " & _
			"P.Descricao,  " & _
			"P.Destaque,  " & _
			"P.Imagem,  " & _
			"P.promocao,  " & _
			"P.DataIniPromo,  " & _
			"P.DataFimPromo,  " & _
			"P."& sCampoPreco &" as Preco,  " & _
			"P.Preco_Aprazo,  " & _
			"P.DescontoValor,  " & _
			"P.DescontoIgnorar,  " & _
			"P.Qte_Parcelamento,  " & _
			"P.Qte_Estoque,  " & _
			"P.DataIniDesa,  " & _
			"P.DataFimDesa,  " & _
			"P.DataIniPreVenda,  " & _
			"P.DataFimPreVenda,  " & _
			"P.DescontoTipo,  " & _
			"P.PreVenda,  " & _
			"P.id_tamanho,  " & _
			"P.UrlCompra,  " & _
			"C.categoria,  " & _
			"SC.subcategoria,  " & _
			"isnull(P.id_tipos, '') id_tipos,  " & _
			"M.Moeda,  " & _
			"M.Simbolo  " & _
		"from  " & _
				"TB_PRODUTOS P (nolock) "  & _
				" INNER JOIN TB_MOEDAS M (NOLOCK) on  P.Id_Moeda = M.id " & _
				" INNER JOIN TB_CATEGORIAS C(NOLOCK) on P.Id_Categoria = C.id AND C.Id_Tipo = 'S' AND C.Ativo = 1  AND isnull(C.UrlEspecial, '') not like 'http%' " & _
				" INNER JOIN TB_SUBCATEGORIA SC (NOLOCK) on P.Id_SubCategoria = SC.id AND SC.Ativo = 1 " & _
		"where " &_
				" P.cdcliente = "& sCdCliente &" and P.ID NOT IN ("& prProduto &") and P.ID IN ("& Compre_Tambem_ids(prProduto,prFabricane) &")" & _
				" AND P.ATIVO = 1"
		sSql = sSql &" order by newId() "
	set Rs2 = oConn.execute(sSql)

	if(not rs2.eof) then
		do while not rs2.eof
			sIdProd 		= trim(rs2("id"))
			sDescricao 		= trim(rs2("Descricao"))
			sDestaque 		= trim(rs2("Destaque"))
			sPreco 			= RetornaCasasDecimais(rs2("preco"),2)
			sPrecoPrazo	 	= RetornaCasasDecimais(rs2("preco_aprazo"),2)
			sParcelamento 	= trim(rs2("Qte_Parcelamento"))
			boolEsgotado 	= (cdbl(trim(rs2("Qte_Estoque")))= 0 or cdbl(trim(rs2("Qte_Estoque"))) < 0)
			sPathEspecial 	=  "/"
			sTipos	 		= trim(rs2("id_tipos"))
			sVTamanho		= trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "id_tamanho"))
			sCategoria		= TrataSEO(Recupera_Campos_Db_oConn("TB_CATEGORIAS", Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "id_categoria"), "id", "categoria"))
			sSubCategoria  	= TrataSEO(Recupera_Campos_Db_oConn("TB_SUBCATEGORIA",Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "id_subcategoria"), "id", "subcategoria"))
			sDescricaoSEO	= TrataSEO(rs2("Descricao"))
			sUrlCompra		= Trim(rs2("UrlCompra"))
			
			sImagem = Alterar_Tamanho_Imagem("p", "", "", trim(rs2("imagem")), 140, 140)
			if(sImagem = "" or isnull(sImagem)) then sImagem = "img_indisponivel.jpg"
			Session(sIdProd) = sPreco
			
			if( trim(rs2("promocao")) = "1") then 
				if(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DescontoValor") <> "" and Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DescontoValor") <> "0") then 
					Session("d" & sIdProd) = cdbl(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DescontoValor"))
				end if
			else
				Session("d" & sIdProd) = 0
			end if
			
			if(trim(rs2("DataIniPreVenda")) <> "" and not isnull(rs2("DataIniPreVenda"))) then sDataPreVendaIni = cdate(rs2("DataIniPreVenda"))
			if(trim(rs2("DataFimPreVenda")) <> "" and not isnull(rs2("DataFimPreVenda"))) then sDataPreVendaFim = cdate(rs2("DataFimPreVenda"))
			
			boolPreVenda = false

			if(trim(rs2("PreVenda")) = "1") then 
				boolPreVenda = (date >= sDataPreVendaIni AND date <= sDataPreVendaFim)
			end if
			
			if(sContaLinha = 1) then  sConteudo = sConteudo & "<ul>"
				sConteudo = sConteudo & " <li>"
				sConteudo = sConteudo & " <span class=""compretambem-img"">"

				if (sUrlCompra = "" or isnull(sUrlCompra)) then sConteudo = sConteudo & "<a href="/"/detalhes/"& sDescricaoSEO &"-"& sIdProd &""" title="""& sDescricao &""">"				

				if((right(lcase(sImagem),4) = ".wmv")) then 
					sConteudo = sConteudo & "<EMBED SRC="""& sLinkImagem &"/EcommerceNew/upload/produto/"& sImagem &""" AUTOSTART=""true"""" VOLUME=""100"" LOOP=""true"" CONTROLLER=""true"" >"
				else
					sConteudo = sConteudo & "<img src="""& sLinkImagem &"/EcommerceNew/upload/produto/p/"& sImagem &""" width=""140"" height=""140"" alt="""& sDescricao &""">"
				end if
				sConteudo = sConteudo & "</a></span>"
				if (boolPreVenda) then sConteudo = sConteudo & "<h3 style=""color:#ff0000"">Pr&eacute;-venda</h3>"
				
				if (sUrlCompra = "" or isnull(sUrlCompra)) then
					sConteudo = sConteudo & "<h3><a href="/"/detalhes/"& sDescricaoSEO &"-"& sIdProd &""">"& sDescricao &"</a></h3>"				
				else
					sConteudo = sConteudo & "<h3>"& sDescricao &"</h3>"
				end if
				
				if(Verifica_Status_Loja()) then
					if(boolEsgotado) then
						sConteudo = sConteudo & "<p><a href="/"javascript:AbrePopUp('/pops/pop_avisodedisponibilidade.asp?id=" & sIdProd & "','716','520')""><img src=""/css/imagem/selo_produtoindisponivel.gif"" alt="""" ></a></p>"
					else
						if (sVTamanho <> "") then
							if (sUrlCompra = "" or isnull(sUrlCompra)) then
								sConteudo = sConteudo & "<span class=""compreL""><a href="/"/detalhes/"& sDescricaoSEO &"-"& sIdProd &"""><img src=""/css/imagem/botao_compre.gif"" align=""right"" alt=""""></a></span>"
							else
								sConteudo = sConteudo & "<span class=""compreL""><a href="/""& sUrlCompra &""" target=""_blank""><img src=""/css/imagem/botao_compre_a.gif"" align=""right"" alt=""""></a></span>"
							end if
						else
							if (sTipos <> "" and sTipos <> "0" and InsTr(sTipos, "88") > 0) then
								sConteudo = sConteudo & "<span class=""compreL""><a href="/"/cesta.asp?ID="& sIdProd &"""><img src=""/css/imagem/botao_compre_d.gif"" align=""right"" alt=""""></a></span>"
							else
								if (sUrlCompra = "" or isnull(sUrlCompra)) then
									sConteudo = sConteudo & "<span class=""compreL""><a href="/"/cesta.asp?ID="& sIdProd &"""><img src=""/css/imagem/botao_compre.gif"" align=""right"" alt=""""></a></span>"
								else
									sConteudo = sConteudo & "<span class=""compreL""><a href="/""& sUrlCompra &""" target=""_blank""><img src=""/css/imagem/botao_compre_a.gif"" align=""right"" alt=""""></a></span>"
								end if
							end if
						end if
					end if
				end if
				
				if (not boolEsgotado) then
				
					if(mostrar_desconto_produto(sIdProd)) then 

						if(oConn.State = "0") then call Abre_Conexao()
						
						if(Session("d" & sIdProd) = 0) then
							if(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "Promocao") = "1") then 
								if(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DescontoValor") <> "") then 
									Session("d" & sIdProd) = cdbl(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DescontoValor"))
								end if
							else
								Session("d" & sIdProd) = 0
							end if
						End if
						
						IF(Session("d" & sIdProd) > 0 OR Session("dc" & sIdProd) > 0) THEN

							if(ucase(Recupera_Campos_Db_oConn("TB_PRODUTOS", sIdProd, "id", "DescontoTipo")) = "R") then 
								' DESCONTO EM REAIS -----------------
								if(Session("CupomGet") <> "") then 
									sPrecoPor = RetornaCasasDecimais((  _
												cdbl(Session(sIdProd)) -  _
												( ( cdbl(Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) +  _
													Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria"))) / 100) * cdbl(Session(sIdProd)))),2)

									sPrecoPor = RetornaCasasDecimais((sPrecoPor - Session("d" & sIdProd) - Session("dc" & sIdProd)),2)

									if(ucase(Recupera_Campos_Db_oConn("TB_CUPONS", Session("CupomGet"), "Cupom", "TipoDescontoValor")) = "P") then
										sPrecoPor = RetornaCasasDecimais((cdbl(sPrecoPor) - ((cdbl(Session("Desconto") + Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) + Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria"))) / 100) * cdbl(sPrecoPor))),2)
									else
										sPrecoPor = RetornaCasasDecimais((cdbl(sPrecoPor)- ((cdbl(Session("Desconto") + Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) + Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria")))))),2)
									end if
									if(sPrecoPor < 0) then sPrecoPor = RetornaCasasDecimais(0,2)
									
									' response.write sPrecoPor
									' response.end
								else
									sPrecoPor = RetornaCasasDecimais((  _
												cdbl(Session(sIdProd)) -  _
												( ( cdbl(Session("Desconto") +  _
													Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) +  _
													Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria"))) / 100) * cdbl(Session(sIdProd)))),2)

									sPrecoPor = RetornaCasasDecimais((sPrecoPor - Session("d" & sIdProd) - Session("dc" & sIdProd)),2)
								end if
							else
								' DESCONTO EM PORCENTO -----------------
								sPrecoPor = RetornaCasasDecimais((cdbl(Session(sIdProd)) - ((cdbl(Session("Desconto") + Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) + Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria")) + Session("d" & sIdProd) + Session("dc" & sIdProd)) / 100) * cdbl(Session(sIdProd)))),2)
							end if
						ELSE
							if(ucase(Recupera_Campos_Db_oConn("TB_CUPONS", Session("CupomGet"), "Cupom", "TipoDescontoValor")) = "P") then
								sPrecoPor = RetornaCasasDecimais((cdbl(Session(sIdProd)) - ((cdbl(Session("Desconto") + Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) + Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria"))) / 100) * cdbl(Session(sIdProd)))),2)
							else
								sPrecoPor = RetornaCasasDecimais((cdbl(Session(sIdProd))- ((cdbl(Session("Desconto") + Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) + Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria")))))),2)
							end if
							if(sPrecoPor < 0) then sPrecoPor = RetornaCasasDecimais(0,2)
						END IF
						
						sConteudo = sConteudo & "<span class=""compretambem-de""><a href="/"/detalhes/"& sDescricaoSEO &"-"& sIdProd &""">De: "& sSimbolo &""& sPreco &"</a></span>"
						sConteudo = sConteudo & "<span class=""compretambem-por""><a href="/"/detalhes/"& sDescricaoSEO &"-"& sIdProd &""">Por: "& sSimbolo &" "& RetornaCasasDecimais(sPrecoPor,2) &"</a></span>"
						sConteudo = sConteudo & "<span class=""compretambem-eco"">Economize:"& sSimbolo &"&nbsp;"
						
						if(ucase(Recupera_Campos_Db_oConn("TB_CUPONS", Session("CupomGet"), "Cupom", "TipoDescontoValor")) = "P") then
							sConteudo = sConteudo & RetornaCasasDecimais((sPreco - sPrecoPor), 2)&"</span>"
						else
							sConteudo = sConteudo & RetornaCasasDecimais(Session("Desconto") + Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_categoria")) + Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", sIdProd, "id", "id_subcategoria")) + Session("d" & sIdProd) + Session("dc" & sIdProd), 2) &"</span>"
						end if
						
						if(sPrecoPrazo > 0 and sParcelamento > 1) then 
							if((RetornaCasasDecimais((sPrecoPor / sParcelamento) >= (sValorParcelaMinima),2))) then
								sConteudo = sConteudo & "<span class=""compretambem-vezes""><a href="/"/detalhes/"& sDescricaoSEO &"-"& sIdProd &""">"& sParcelamento &"x "& sSimbolo &""& RetornaCasasDecimais((sPrecoPor / sParcelamento),2) &"</a></span>"
							else
								if (Retorna_Parcelamento(sPrecoPor) > 1) then
									if((RetornaCasasDecimais((sPrecoPor / Retorna_Parcelamento(sPrecoPor)) >= (sValorParcelaMinima),2))) then
										sConteudo = sConteudo & "<span class=""compretambem-vezes""><a href="/"/detalhes/"& sDescricaoSEO &"-"& sIdProd &""">"& Retorna_Parcelamento(sPrecoPor) &"x "& sSimbolo &""& RetornaCasasDecimais((sPrecoPor / Retorna_Parcelamento(sPrecoPor)),2) &"</a></span>"
									end if
								end if
							end if
						end if

						if ( (boolDataCampanhaFrete) AND ( (cdbl(sPrecoPor) >= cdbl(sPrecoPromocaoFreteGratis))  )) then
							sConteudo = sConteudo & "<span class=""compretambem-eco""><a href="/"javascript:AbrePopUp('/pops/pop_fretegratis.asp','716','520')""><img src="""&sLinkImagem&"/EcommerceNew/upload/selos/"&sImgSeloFreteGratis&""" alt="""" ></a></span>"
						end if
					else
						if (sUrlCompra = "" or isnull(sUrlCompra)) then
							if(sPrecoPrazo > 0 and sParcelamento > 1) then 
								sConteudo = sConteudo & "<span class=""compretambem-apenas"">Por apenas:</span>"
								sConteudo = sConteudo & "<span class=""compretambem-por""><a href="/"/detalhes/"& sDescricaoSEO &"-"& sIdProd &""">"& sSimbolo &" "& sPrecoPrazo &"</a></span>"
								
								if(sPrecoPrazo > 0 and sParcelamento > 1) then 
									if((RetornaCasasDecimais((sPrecoPrazo / sParcelamento) >= (sValorParcelaMinima),2))) then
										sConteudo = sConteudo & "<span class=""compretambem-vezes""><a href="/"/detalhes/"& sDescricaoSEO &"-"& sIdProd &""">"& sParcelamento &"x "& sSimbolo &""& RetornaCasasDecimais((sPrecoPrazo / sParcelamento),2) &"</a></span>"
									else
										if (Retorna_Parcelamento(sPrecoPrazo) > 1) then
											if((RetornaCasasDecimais((sPrecoPrazo / Retorna_Parcelamento(sPrecoPrazo)) >= (sValorParcelaMinima),2))) then
												sConteudo = sConteudo & "<span class=""compretambem-vezes""><a href="/"/detalhes/"& sDescricaoSEO &"-"& sIdProd &""">"& Retorna_Parcelamento(sPrecoPrazo) &"x "& sSimbolo &""& RetornaCasasDecimais((sPrecoPrazo / Retorna_Parcelamento(sPrecoPrazo)),2) &"</a></span>"
											end if
										end if
									end if
								end if
								if(cdbl(sPrecoPrazo) <> cdbl(sPreco)) then 
									sConteudo = sConteudo & "<span class=""compretambem-eco"">Economize:"& sSimbolo &""& RetornaCasasDecimais(((RetornaCasasDecimais(sPrecoPrazo - sPreco, 2) * 100) / sPrecoPrazo),2) &"% de desconto</span>"
									sConteudo = sConteudo & "<span class=""compretambem-por""><a href="/"/detalhes/"& sDescricaoSEO &"-"& sIdProd &""">"& sSimbolo &" "& sPreco &"</a></span></a>"
								end if
								if ( (boolDataCampanhaFrete) AND ( (cdbl(sPreco) >= cdbl(sPrecoPromocaoFreteGratis))  )) then
									sConteudo = sConteudo & "<span class=""compretambem-eco""><a href="/"javascript:AbrePopUp('/pops/pop_fretegratis.asp','716','520')""><img src="""&sLinkImagem&"/EcommerceNew/upload/selos/"&sImgSeloFreteGratis&""" alt="""" ></a></span>"
								end if 
							else
								sConteudo = sConteudo & "<span class=""compretambem-apenas"">Por apenas:</span>"						
								sConteudo = sConteudo & "<span class=""compretambem-por""><a href="/"/detalhes/"& sDescricaoSEO &"-"& sIdProd &""">"& sSimbolo &" "& sPreco &"</a></span>"
								if ( (boolDataCampanhaFrete) AND ( (cdbl(sPreco) >= cdbl(sPrecoPromocaoFreteGratis))  )) then
									sConteudo = sConteudo & "<span class=""compretambem-eco""><a href="/"javascript:AbrePopUp('/pops/pop_fretegratis.asp','716','520')""><img src="""&sLinkImagem&"/EcommerceNew/upload/selos/"&sImgSeloFreteGratis&""" alt="""" ></a></span>"
								end if 
							end if
						end if
					end if
				end if
				if(oConn.State = "0") then call Abre_Conexao()
				sConteudo = sConteudo & "</li>"
				if(sContaLinha = 3) then  
					sConteudo = sConteudo & "</ul>"
					sContaLinha = 0
				end if
				
				iExibe = iExibe + 1
				sContaLinha = sContaLinha + 1
			
		rs2.movenext
		loop
	end if 
	set Rs2 = nothing
	
	call fecha_conexao()
	
	Monta_Sugestao_Produto = sConteudo
end function
' =================================================================
Function Compre_Tambem_ids(prProduto,prFabricane)
dim ResultadoIds, sTopCompre

	'1 N I V E L - B U N D E L E * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	Dim sIdNivelB, sExibirB

	sSQlB = "Select top 9  " & _
			"P.ID_PRODUTO, " & _
			"P.id_produto_Sugestao " & _
		 "from " & _
			 "TB_PRODUTOS_SUGESTOES P (nolock) " & _
		 "where " & _
		 	 " EXISTS (SELECT 1 FROM TB_PRODUTOS PS (nolock) WHERE P.ID_PRODUTO = PS.id and PS.ATIVO = 1) " & _
			 " AND P.ID_PRODUTO = "& prProduto 
	set RsB = oConn.execute(sSQlB)

	if(not rsB.eof) then
		sExibirB = 0
		do while not rsB.eof
			sIdNivelB = sIdNivelB & trim(rsB("id_produto_Sugestao")) &","
			sExibirB = sExibirB + 1
		rsB.movenext
		loop
		ResultadoIds = left(sIdNivelB,Len(sIdNivelB)-1) 
		sTopCompre = sExibirB
		sTopCompre = 9 - sTopCompre
	else
		if (ResultadoIds = "") then ResultadoIds = prProduto
		sTopCompre = 9
	end if
	set RsB = nothing

	'* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

	'2 N I V E L - M A I S  V E N D I D O S  * * * * * * * * * * * * * * * * * * * * * *
	if (sTopCompre > 0) then
			Dim sIdNivelV, sExibirV

			sSQlV = "SELECT top "& sTopCompre &" " & _
					"	P1.id, " & _
					" 	count(*) total " & _
					"FROM " & _
					" 	TB_PEDIDOS P1 (nolock) " & _
					" 	INNER JOIN TB_PEDIDOS_ITENS PI (NOLOCK) ON PI.Id_Pedido = P1.ID " & _
					"WHERE " & _
					" 	P1.cdcliente = "& sCdCliente &"" & _
					" 	AND P1.id in (SELECT P2.id from TB_PEDIDOS P2 (nolock) INNER JOIN TB_PEDIDOS_ITENS PI1 (NOLOCK) ON PI1.Id_Pedido = P2.ID WHERE p2.cdcliente = "& sCdCliente &" and PI1.id_produto in ("& prProduto &"))" & _
					"group by P1.id " & _
					"having count(*) > 2 " & _
					"order by count(*) desc "
			set Rsv = oConn.execute(sSQlv)
			if(not Rsv.eof) then
				do while not Rsv.eof
				
					sSQlVa= "SELECT top "& sTopCompre &" PI1.* FROM TB_PEDIDOS_ITENS PI1 (NOLOCK) WHERE PI1.ID_pedido = "&Rsv("id")&" and PI1.id_produto not in ("&  ResultadoIds &")"
					set RsVa = oConn.execute(sSQlVa)
					if(not RsVa.eof) then
						sExibirV = 0
						do while not RsVa.eof
							sIdNivelV = trim(RsVa("ID_produto")) &"," & sIdNivelV
							sExibirV = sExibirV + 1
						RsVa.movenext
						loop
						ResultadoIds = ResultadoIds &","& left(sIdNivelV,Len(sIdNivelV)-1)
						sTopCompre = sTopCompre - sExibirV
						'sTopCompre = 9 - sTopCompre
					else
						if (trim(ResultadoIds) = "") then ResultadoIds = prProduto
						sTopCompre  = 9
					end if
					
				Rsv.movenext
				loop
			end if
			set Rsv = nothing
	end if
	'* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

	'3 N I V E L - F A B R I C A N T E  * * * * * * * * * * * * * * * * * * * * * * * * 
	if (sTopCompre > 0) then
			Dim sIdNivelF, sExibirF
			sSQlF = "Select top "& sTopCompre &"  "& _
					"	P.Id AS IDF,   "& _
					"	P.Id_SubCategoria,   "& _
					"	P.id_categoria,   "& _
					"	M.Moeda,   "& _
					"	M.Simbolo   "& _
					"from   "& _
					"	TB_PRODUTOS P (nolock)    "& _
					" 	INNER JOIN TB_MOEDAS M (NOLOCK) on  P.Id_Moeda = M.id and M.Ativo = 1 "& _
					"	INNER JOIN TB_CATEGORIAS C(NOLOCK) on P.Id_Categoria = C.id AND C.Id_Tipo = 'S' AND C.Ativo = 1  AND isnull(C.UrlEspecial, '') not like 'http%' "& _
					"	INNER JOIN TB_SUBCATEGORIA SC (NOLOCK) on P.Id_SubCategoria = SC.id AND SC.Ativo = 1 "& _
					"where "& _
					"	P.cdcliente = "& sCdCliente &" and (P.id_fabricante = "& prFabricane &") and  "& _
					" 	(P.boolMostra = '' or P.boolMostra is null or P.boolMostra = 'p') and   "& _
					" 	P.id not in ("& ResultadoIds &") and   "& _
					"	P.Ativo = 1 "
			set RsF = oConn.execute(sSQlF)
			if(not RsF.eof) then
				sExibirF = 0
				do while not RsF.eof
					sIdNivelF = sIdNivelF & trim(RsF("IDF")) &","
					sExibirF = sExibirF + 1
				RsF.movenext
				loop
				ResultadoIds = ResultadoIds &","& left(sIdNivelF,Len(sIdNivelF)-1)
			end if
			set RsF = nothing
	end if
	'* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	Compre_Tambem_ids = ResultadoIds
end function


'========================================================================
function Monta_Vitrine_Secao(Categoria)
	dim sSql, Rs, Rs2, sCampo
	dim sConteudo, sSubCategoria, sIdCategoria
	
    Monta_Vitrine_Secao = ""
    sConteudo           = ""
	sCampo              = "SubCategoria"

	if(not isnumeric(Categoria)) then 
		exit function
	end if

	call abre_conexao()
	
	sSql = "Select " & _
				" S.*  " & _
			" from  " & _
				" TB_SUBCATEGORIA S (NOLOCK) " & _
			" where  " & _
				" S.cdcliente 	 = "& sCdCliente &" AND " & _
				" S.Id_Categoria = "& Categoria &" AND  " & _
				" S.ativo 		 = 1  AND 				" & _
				" S.Id in (SELECT P.ID_SUBCATEGORIA FROM TB_PRODUTOS P WHERE P.Id_SubCategoria = S.id and P.ATIVO = 1 AND P.CDCLIENTE = "& sCdCliente &") " & _
			" order by  " & _
				" SubCategoria "
'	response.write ssql
'	response.end
	set Rs = oConn.execute(sSql)

	if(not rs.eof) then
		do while not rs.eof
			sSubCategoria   = rs("SubCategoria")
			sIdCategoria    = rs("id")
			
            sConteudo = sConteudo & "<li><a href=""/subcategoria/"& TrataSeo(sSubCategoria) &"/"& sIdCategoria &""">"& sSubCategoria &"</a></li>"
		rs.movenext
		loop
	end if
	set Rs = nothing
	
	call fecha_conexao()
	
	Monta_Vitrine_Secao = sConteudo
end function
'========================================================================
function Monta_Tabela_SubCategoria_Secao(Categoria)
	dim sSql, Rs, sCampo
	dim sConteudo, sCategoria, sIdCategoria, sCategoriaSEO
	
	sCampo = "SubCategoria"
    
    sSql =  " SELECT " & _
            "   S.SubCategoria, " & _
            "   S.ID " & _
            " FROM " & _
            "   TB_SUBCATEGORIA S (nolock) " & _
			" WHERE " & _
            "   S.cdcliente = "& sCdCliente & _
			"   AND S.Id_Categoria = "& Categoria & _
			"   AND S.ativo = 1 " & _
			"   AND isnull(S.subCategoria,'') <> ''  " & _
			"   AND S.Id in (SELECT P.ID_SUBCATEGORIA FROM TB_PRODUTOS P (NOLOCK) WHERE P.Id_SubCategoria = S.id and P.ATIVO = 1 AND P.CDCLIENTE = "& sCdCliente &") " & _
            " ORDER BY " & _
			"   S.SubCategoria "
    set Rs = oConn.execute(sSql)

	if(not rs.eof) then
		do while not rs.eof
			sCategoria 		= rs(sCampo)
			sCategoriaSEO 	= TrataSEO(rs("SubCategoria"))
			sIdCategoria 	= rs("id")

            sConteudo = sConteudo & "<a href="/"/lista/"&sCategoriaSEO &"-"& sIdCategoria &""">" & sCategoria & "</a>"
			' sConteudo = sConteudo & Monta_Tabela_SubCategoria2(sIdCategoria) & "<br>"
		rs.movenext
		loop
	end if
	set Rs = nothing
	
	Monta_Tabela_SubCategoria_Secao = sConteudo
end function

'========================================================================
function Monta_Tabela_Categoria_SiteMap()
	dim sSql, RsS, sCampo, lContCat
	dim sConteudo, sCategoria, sIdCategoria
	
	sSql = " Select " & _
				" S.* " & _
			" from  " & _
				" TB_CATEGORIAS S (nolock) " & _
			" where  " & _
				" S.cdcliente = "& sCdCliente & _
				" AND S.ativo = 1 " & _
				" AND isnull(S.Categoria,'') <> ''  " & _
				" AND EXISTS (SELECT 1 FROM TB_PRODUTOS P (nolock) WHERE P.Id_Categoria = S.id and P.ATIVO = 1) " & _
			" order by " & _
				" S.Categoria "

	' response.write ssql
	' response.end
	set RsS = oConn.execute(sSql)

	if(not RsS.eof) then
        lContCat = 1

        sConteudo = sConteudo & " <div class=""col-xs-6 col-sm-3 col-md-4"">"
        sConteudo = sConteudo & "   <ul class=""simple-list arrow-list bold-list"">"

		do while not RsS.eof
			sCategoria 		= RsS("Categoria")
			sIdCategoria 	= RsS("id")

            sConteudo = sConteudo & " <li> <a href=""/categoria/"& TrataSEO(trim(RsS("Categoria"))) &"/"& trim(RsS("id")) &"""><b>"& sCategoria &"</b></a>"
            sConteudo = sConteudo &         Monta_Tabela_SubCategoria(sIdCategoria)
            sConteudo = sConteudo & " </li>"

            if(lContCat = 4) Then 
                sConteudo = sConteudo & "   </ul>"
                sConteudo = sConteudo & "</div>"

                sConteudo = sConteudo & " <div class=""col-xs-6 col-sm-3 col-md-4"">"
                sConteudo = sConteudo & "   <ul class=""simple-list arrow-list bold-list"">"

                lContCat = 0
            end if
	
            lContCat = lContCat + 1
            RsS.movenext
		loop

        sConteudo = sConteudo & "   </ul>"
        sConteudo = sConteudo & "</div>"
	end if
	set RsS = nothing
	
	Monta_Tabela_Categoria_SiteMap = sConteudo
end function


'========================================================================
function Monta_Tabela_SubCategoria(Categoria)
	dim sSql, RsS, sCampo
	dim sConteudo, sCategoria, sIdCategoria
	
	if(Categoria = "") then exit function
	
	sCampo = "SubCategoria"

	sSql = "Select " & _
				" S.* , " & _
				" C.Especial AS EspecialC,  " & _
				" C.urlEspecial AS urlEspecialC " & _
			" from  " & _
				" TB_SUBCATEGORIA S (nolock) " & _
				" INNER JOIN TB_CATEGORIAS C (nolock) on S.Id_Categoria = C.Id " & _
			" where  " & _
				" S.cdcliente = "& sCdCliente & _
				" AND S.Id_Categoria = "& Categoria & _
				" AND S.ativo = 1 " & _
				" AND isnull(S.subCategoria,'') <> ''  " & _
				" AND EXISTS (SELECT 1 FROM TB_PRODUTOS P (nolock) WHERE P.Id_SubCategoria = S.id and P.ATIVO = 1) " & _
			" order by " & _
				" S.SubCategoria "

	' response.write ssql
	' response.end
	set RsS = oConn.execute(sSql)

	if(not RsS.eof) then
        sConteudo = sConteudo & " <ul>"

		do while not RsS.eof
			sCategoria 		= RsS(sCampo)
			sIdCategoria 	= RsS("id")

            sConteudo = sConteudo & " <li><a href=""/subcategoria/"& TrataSEO(trim(RsS("subCategoria"))) &"/"& trim(RsS("id")) &""">&nbsp;&nbsp;&nbsp;&nbsp;<i class=""fa fa-angle-double-right"">&nbsp;</i>"& sCategoria &"</a></li>"

			'if(trim(RsS("EspecialC")) = "1") then 
				'sConteudo = sConteudo & " <li><a href="/""& sLinkhttp & trim(RsS("urlEspecialC")) &"lista.asp?sub="& sIdCategoria &""" target=""_self"" name=""mainFrame"">" & sCategoria & "</a></li>"
			'else
				'sConteudo = sConteudo & " <li><a href="/""& sLinkhttp & trim(RsS("urlEspecialC")) &"Secao.asp?sub="& sIdCategoria &""" target=""_self"" name=""mainFrame"">" & sCategoria & "</a></li>"
			'end if
		RsS.movenext
		loop

        sConteudo = sConteudo & " </ul>"		
		'sConteudo = sConteudo & "	<li><a href="/""& sLinkhttp & Recupera_Campos_Db_oConn("TB_CATEGORIAS", Categoria, "id", "urlEspecial") &"Secao.asp?Cat="& Categoria &""" class=""menor"">veja mais...</a></li>"
	end if
	set RsS = nothing
	
	Monta_Tabela_SubCategoria = sConteudo
end function

'========================================================================
function Monta_Tabela_SubCategoria2(Categoria)
	dim sSql, Rs, sCampo
	dim sConteudo, sCategoria, sIdCategoria
	
	sConteudo = ""
	
	if(Idioma = "br") then
		sCampo = "SubCategoria"
	else
		sCampo = "SubCategoria_Jp"
	end if

	sSql = "Select * from TB_SUBCATEGORIA2 (nolock) where cdcliente = "& sCdCliente &" and Id_SubCategoria = "& Categoria &" and ativo = 1 and isnull(subCategoria,'') <> '' order by SubCategoria "
	set Rs = oConn.execute(sSql)

	
	if(not rs.eof) then
		do while not rs.eof
			sCategoria = rs(sCampo)
			sIdCategoria = rs("id")

			if(Recupera_Campos_Db_oConn("TB_CATEGORIAS", Recupera_Campos_Db_oConn("TB_SUBCATEGORIA", Categoria, "id", "Id_Categoria"), "id", "Especial") = "1") then 
				sConteudo = sConteudo & " <li><a href="/""& sLinkhttp & trim(Recupera_Campos_Db_oConn("TB_CATEGORIAS", Recupera_Campos_Db_oConn("TB_SUBCATEGORIA", Categoria, "id", "Id_Categoria"), "id", "urlEspecial")) &"lista.asp?sub2="& sIdCategoria &""" target=""_self"" name=""mainFrame"">" & sCategoria & "</a></li>"
				sConteudo = sConteudo & Monta_Tabela_SubCategoria3(sIdCategoria)				
			end if
		rs.movenext
		loop
	end if
	set Rs = nothing
	
	Monta_Tabela_SubCategoria2 = sConteudo
end function


'========================================================================
function Monta_Tabela_SubCategoria3(Categoria)
	dim sSql, Rs, sCampo
	dim sConteudo, sCategoria, sIdCategoria
	
	sConteudo = ""
	
	if(Idioma = "br") then
		sCampo = "SubCategoria"
	else
		sCampo = "SubCategoria_Jp"
	end if

	sSql = "Select * from TB_SUBCATEGORIA3 (nolock) where cdcliente = "& sCdCliente &" and Id_SubCategoria2 = "& Categoria &" and ativo = 1 order by SubCategoria "
	set Rs = oConn.execute(sSql)

	if(not rs.eof) then
		do while not rs.eof
			sCategoria = rs(sCampo)
			sIdCategoria = rs("id")
			
			sConteudo = sConteudo & "<span class=""a11zul"" style=""margin:0 0 0 10px;"">&bull;&bull;&bull;</span> <a href="/""& sLinkhttp &"produtos.asp?sub3="& sIdCategoria &""" target=""_self"" name=""mainFrame"" class=""a11zul"" style=""line-height:10px;"">"& sCategoria &"</a><br>"
			sConteudo = sConteudo & Monta_Tabela_SubCategoria4(sIdCategoria)
						
		rs.movenext
		loop
		
	end if
	set Rs = nothing
	
	Monta_Tabela_SubCategoria3 = sConteudo
end function


'========================================================================
function Monta_Tabela_SubCategoria4(Categoria)
	dim sSql, Rs, sCampo
	dim sConteudo, sCategoria, sIdCategoria
	
	sConteudo = ""
	
	if(Idioma = "br") then
		sCampo = "SubCategoria"
	else
		sCampo = "SubCategoria_Jp"
	end if

	sSql = "Select * from TB_SUBCATEGORIA4 (nolock) where cdcliente = "& sCdCliente &" and Id_SubCategoria3 = "& Categoria &" and ativo = 1 order by SubCategoria "
	set Rs = oConn.execute(sSql)

	if(not rs.eof) then
		do while not rs.eof
			sCategoria = rs(sCampo)
			sIdCategoria = rs("id")
			
			sConteudo = sConteudo & "<span class=""a11zul"" style=""margin:0 0 0 20px;"">&bull;&bull;&bull;&bull;</span> <a href="/""& sLinkhttp &"produtos.asp?sub4="& sIdCategoria &""" target=""_self"" name=""mainFrame"" class=""a11zul"" style=""line-height:10px;"">"& sCategoria &"</a><br>"
			sConteudo = sConteudo & Monta_Tabela_SubCategoria5(sIdCategoria)
			
		rs.movenext
		loop
		
	end if
	set Rs = nothing
	
	Monta_Tabela_SubCategoria4 = sConteudo
end function

'========================================================================
function Monta_Tabela_SubCategoria5(Categoria)
	dim sSql, Rs, sCampo
	dim sConteudo, sCategoria, sIdCategoria
	
	sConteudo = ""
	
	if(Idioma = "br") then
		sCampo = "SubCategoria"
	else
		sCampo = "SubCategoria_Jp"
	end if

	sSql = "Select * from TB_SUBCATEGORIA5 (nolock) where cdcliente = "& sCdCliente &" and Id_SubCategoria4 = "& Categoria &" and ativo = 1 order by SubCategoria "
	set Rs = oConn.execute(sSql)

	if(not rs.eof) then
		do while not rs.eof
			sCategoria = rs(sCampo)
			sIdCategoria = rs("id")
			
			sConteudo = sConteudo & "<span class=""a11zul"" style=""margin:0 0 0 30px;"">&bull;&bull;&bull;&bull;&bull;</span> <a href="/""& sLinkhttp &"produtos.asp?sub5="& sIdCategoria &""" target=""_self"" name=""mainFrame"" class=""a11zul"" style=""line-height:10px;"">"& sCategoria &"</a><br>"
			sConteudo = sConteudo & Monta_Tabela_SubCategoria6(sIdCategoria)
		rs.movenext
		loop
		
	end if
	set Rs = nothing
	
	Monta_Tabela_SubCategoria5 = sConteudo
end function

'========================================================================
function Monta_Tabela_SubCategoria6(Categoria)
	dim sSql, Rs, sCampo
	dim sConteudo, sCategoria, sIdCategoria
	
	sConteudo = ""
	
	if(Idioma = "br") then
		sCampo = "SubCategoria"
	else
		sCampo = "SubCategoria_Jp"
	end if

	sSql = "Select * from TB_SUBCATEGORIA6 (nolock) where cdcliente = "& sCdCliente &" and Id_SubCategoria5 = "& Categoria &" and ativo = 1 order by SubCategoria "
	set Rs = oConn.execute(sSql)

	if(not rs.eof) then
		do while not rs.eof
			sCategoria = rs(sCampo)
			sIdCategoria = rs("id")
			
			sConteudo = sConteudo & "<span class=""a11zul"" style=""margin:0 0 0 30px;"">&bull;&bull;&bull;&bull;&bull;&bull;</span> <a href="/""& sLinkhttp &"produtos.asp?sub6="& sIdCategoria &""" target=""_self"" name=""mainFrame"" class=""a11zul"" style=""line-height:10px;"">"& sCategoria &"</a><br>"
			sConteudo = sConteudo & Monta_Tabela_SubCategoria7(sIdCategoria)
		rs.movenext
		loop
		
	end if
	set Rs = nothing
	
	Monta_Tabela_SubCategoria6 = sConteudo
end function

'========================================================================
function Monta_Tabela_SubCategoria7(Categoria)
	dim sSql, Rs, sCampo
	dim sConteudo, sCategoria, sIdCategoria
	
	sConteudo = ""
	
	if(Idioma = "br") then
		sCampo = "SubCategoria"
	else
		sCampo = "SubCategoria_Jp"
	end if

	sSql = "Select * from TB_SUBCATEGORIA7 (nolock) where cdcliente = "& sCdCliente &" and Id_SubCategoria6 = "& Categoria &" and ativo = 1 order by SubCategoria "
	set Rs = oConn.execute(sSql)

	if(not rs.eof) then
		do while not rs.eof
			sCategoria = rs(sCampo)
			sIdCategoria = rs("id")
			
			sConteudo = sConteudo & "<span class=""a11zul"" style=""margin:0 0 0 30px;"">&bull;&bull;&bull;&bull;&bull;&bull;&bull;</span> <a href="/""& sLinkhttp &"produtos.asp?sub7="& sIdCategoria &""" target=""_self"" name=""mainFrame"" class=""a11zul"" style=""line-height:10px;"">"& sCategoria &"</a><br>"
			
		rs.movenext
		loop
		
	end if
	set Rs = nothing
	
	Monta_Tabela_SubCategoria7 = sConteudo
end function


'========================================================================
function Mostra_Todas_Clientes_Admin(Busca, sTipo, sPais, sHab)
	dim sId, sCampo1, sCampo2, sCampo3, sCampo4, sCampo5
	dim sConteudo, Rs, sSQl, sMontagem, ArrOpcCombo
	
	call Abre_Conexao()

	sMontagem = ""
	if(Busca <> "" and  not isnull(Busca)) then sMontagem = sMontagem & " and TB_CLIENTES.Nome like '%"& Busca &"%' "
	if(sTipo <> "" and  not isnull(sTipo)) then sMontagem = sMontagem & " and TB_CLIENTES.Id_Tipo = " & sTipo
	if(sPais <> "" and  not isnull(sPais)) then 
		if(sPais = "pt") then 
			sMontagem = sMontagem & " and  (TB_CLIENTES.Nacionalidade = 'pt') "
		else
			sMontagem = sMontagem & " and  (TB_CLIENTES.Nacionalidade = 'jp') "
		end if
	end if
	if(sHab <> "") then 
		select case sHab
			case "hab"
				sMontagem = sMontagem & " and  TB_CLIENTES.ativo = 1 "
			case "nhab"
				sMontagem = sMontagem & " and  TB_CLIENTES.ativo = 0 "
		end select
	end if
	
	sSQl = _
		" SELECT " & _
			"TB_CLIENTES.*, " & _
			"TB_CLIENTES_TIPO.descricao, " & _
			"TB_CLIENTES_TIPO.desconto, " & _
			"TB_CLIENTES_PROVINCIAS.descricao as provincia" & _
		" FROM  ( TB_CLIENTES INNER JOIN TB_CLIENTES_TIPO ON TB_CLIENTES.id_Tipo = TB_CLIENTES_TIPO.id)  " & _
				 " LEFT JOIN TB_CLIENTES_PROVINCIAS ON TB_CLIENTES.id_provincia = TB_CLIENTES_PROVINCIAS.id " & _
				 " where 1=1 " & sMontagem
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by TB_CLIENTES.DataCad desc"
	end if
	
	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	sConteudo = sConteudo & "<table width=""96%""  border=""0"" align=""center"" cellpadding=""2"" cellspacing=""0"">"
	sConteudo = sConteudo & "  <tr align=""center"" bgcolor=""#E9E9E9"" class=""Subtitulo"">"
	sConteudo = sConteudo & "    <td  nowrap scope=""col""><div align=""center""><a href="/"javascript: Ordenar_Resultado('TB_CLIENTES.id')""><img src=""././img/cima.gif"" border=""0"" align=""absmiddle"" alt=""""></a>&nbsp;C&oacute;digo&nbsp;<a href="/"javascript: Ordenar_Resultado('TB_CLIENTES.id desc')""><img src=""././img/baixo.gif"" border=""0"" align=""absmiddle"" alt=""""></a></div></td>"
	sConteudo = sConteudo & "    <td  scope=""col""><div align=""center""><a href="/"javascript: Ordenar_Resultado('TB_CLIENTES.Nome')""><img src=""././img/cima.gif"" border=""0"" align=""absmiddle"" alt=""""></a>&nbsp;Nome &nbsp;<a href="/"javascript: Ordenar_Resultado('TB_CLIENTES.Nome desc')""><img src=""././img/baixo.gif"" border=""0"" align=""absmiddle"" alt=""""></a></div></td>"
	sConteudo = sConteudo & "    <td  scope=""col""><div align=""center""><a href="/"javascript: Ordenar_Resultado('TB_CLIENTES_TIPO.Descricao')""><img src=""././img/cima.gif"" border=""0"" align=""absmiddle"" alt=""""></a>&nbsp;Tipo &nbsp;<a href="/"javascript: Ordenar_Resultado('TB_CLIENTES_TIPO.Descricao desc')""><img src=""././img/baixo.gif"" border=""0"" align=""absmiddle"" alt=""""></a></div></td>"
	sConteudo = sConteudo & "    <td  scope=""col""><div align=""center""><a href="/"javascript: Ordenar_Resultado('TB_CLIENTES_PROVINCIAS.Descricao')""><img src=""././img/cima.gif"" border=""0"" align=""absmiddle"" alt=""""></a>&nbsp;Provincias &nbsp;<a href="/"javascript: Ordenar_Resultado('TB_CLIENTES_PROVINCIAS.Descricao desc')""><img src=""././img/baixo.gif"" alt="""" border=""0"" align=""absmiddle""></a></div></td>"
	sConteudo = sConteudo & "    <td  scope=""col""><div align=""center""><a href="/"javascript: Ordenar_Resultado('TB_CLIENTES_TIPO.Desconto')""><img src=""././img/cima.gif"" border=""0"" align=""absmiddle"" alt=""""></a>&nbsp;Desconto &nbsp;<a href="/"javascript: Ordenar_Resultado('TB_CLIENTES_TIPO.Desconto desc')""><img src=""././img/baixo.gif"" border=""0"" align=""absmiddle"" alt=""""></a></div></td>"
	sConteudo = sConteudo & "    <td  scope=""col""><div align=""center""><a href="/"javascript: Ordenar_Resultado('TB_CLIENTES.ativo')""><img src=""././img/cima.gif"" border=""0"" align=""absmiddle"" alt=""""></a>&nbsp;Ativo &nbsp;<a href="/"javascript: Ordenar_Resultado('TB_CLIENTES.ativo desc')""><img src=""././img/baixo.gif"" alt="""" border=""0"" align=""absmiddle""></a></div></td>"
	sConteudo = sConteudo & "    <td scope=""col"">&nbsp;</td>"
	sConteudo = sConteudo & "    </tr>"
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina
		iExibe = 1

		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCampo1 = trim(rs("nome")) & " " & trim(rs("Sobrenome"))
			sCampo2 = trim(rs("descricao"))
			sCampo3 = trim(rs("desconto"))
			sCampo5 = trim(rs("provincia"))
			sCampo4 = trim(rs("ativo"))
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center""><a href="/"alteracliente.asp?id="& sId &""">"& sId &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"alteracliente.asp?id="& sId &""">"& sCampo1 &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"alteracliente.asp?id="& sId &""">"& sCampo2 &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center""><a href="/"alteracliente.asp?id="& sId &""">"& sCampo5 &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center""><a href="/"alteracliente.asp?id="& sId &""">"& sCampo3 &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"alteracliente.asp?id="& sId &""">"& fBoolSN(sCampo4) &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><a href="/"javascript:excluir('"& sId &"')""><img src=""././img/excluir.gif"" alt="""" border=""0"" align=""absmiddle""></a></td>" & vbcrlf
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop
		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_Todas_Clientes_Admin = sConteudo
end function



'========================================================================
function Mostra_Todas_Pedidos_Admin(Busca, sDataCad, sForma, Flag, OpcCombo)
	dim sId, sCampo1, sCampo2, sCampo3, sCampo4, sCampo5, sCampo6
	dim sConteudo, Rs, sSQl, sMontagem, ArrOpcCombo, sData

	call Abre_Conexao()

	sData = (day(now) & "/" & month(now) & "/" & year(now))
	
	if(Flag <> "") then
		select case Flag
			case "hoje"
				sMontagem = " and TB_PEDIDOS.DataCad = #"& sData &"#"
			case "cancelados"
				sMontagem = " and TB_PEDIDOS.Id_Status = 5"
			case "all"
				sMontagem = ""
		end select
	else
		if(Busca <> "") then sMontagem = sMontagem & 		" and TB_CLIENTES.nome like '%"& Busca &"%' "
		if(OpcCombo <> "") then  sMontagem = sMontagem & 	" and TB_PEDIDOS.Id_Status = "& OpcCombo
		if(sForma <> "") then  sMontagem = sMontagem & 		" and TB_PEDIDOS.Id_FrmPagamento = "& sForma
		if(sDataCad <> "") then  
			sDataCad = (month(sDataCad) & "/" & day(sDataCad) & "/" & year(sDataCad))
			sMontagem = sMontagem & 		" and TB_PEDIDOS.DataCad = #"& sDataCad &"#"
		end if
	end if
	
	sSQl = "Select " & _
				"TB_PEDIDOS.*, " & _
				"TB_CLIENTES.Nome, " & _
				"TB_CLIENTES.SobreNome, " & _
				"TB_CLIENTES_TIPO.Descricao as Grupo, " & _
				"TB_FORMAS_PAGAMENTO.Descricao as Pagamento, " & _
				"TB_STATUS.Descricao as sTatus" & _
			" from  " & _
				"TB_PEDIDOS,   " & _
				"TB_CLIENTES,   " & _
				"TB_CLIENTES_TIPO,   " & _
				"TB_FORMAS_PAGAMENTO,   " & _
				"TB_STATUS " & _
			" where  " & _
				"TB_PEDIDOS.Id_Cliente = TB_CLIENTES.id and    " & _
				"TB_PEDIDOS.Id_Status = TB_STATUS.id and    " & _
				"TB_PEDIDOS.Id_FrmPagamento = TB_FORMAS_PAGAMENTO.id and  " & _
				"TB_CLIENTES.Id_Tipo = TB_CLIENTES_TIPO.id   " &  sMontagem
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by TB_PEDIDOS.datacad desc"
	end if
	
	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0

	sConteudo = sConteudo & "<table width=""96%""  border=""0"" align=""center"" cellpadding=""0"" cellspacing=""3"" bordercolor="""" bgcolor="""">"
	sConteudo = sConteudo & "  <tr bgcolor=""#E9E9E9"" class=""Subtitulo"">"
	sConteudo = sConteudo & "    <th width=""1%""  align=""center"" nowrap ><a href="/"javascript: Ordenar_Resultado('TB_PEDIDOS.id')""><img src=""././img/cima.gif"" alt="""" border=""0"" align=""absmiddle""></a>&nbsp;C&oacute;digo&nbsp;<a href="/"javascript: Ordenar_Resultado('TB_PEDIDOS.id desc')""><img src=""././img/baixo.gif"" border=""0"" align=""absmiddle"" alt=""""></a></th>"
	sConteudo = sConteudo & "    <th width=""30%"" align=""center"" ><a href="/"javascript: Ordenar_Resultado('TB_CLIENTES.DataCad')""><img src=""././img/cima.gif"" border=""0"" align=""absmiddle"" alt=""""></a>&nbsp;Data Compra &nbsp;<a href="/"javascript: Ordenar_Resultado('TB_CLIENTES.DataCad desc')""><img src=""././img/baixo.gif"" border=""0"" align=""absmiddle"" alt=""""></a></th>"
	sConteudo = sConteudo & "    <th width=""30%"" align=""center"" ><a href="/"javascript: Ordenar_Resultado('TB_CLIENTES.Nome')""><img src=""././img/cima.gif"" border=""0"" align=""absmiddle"" alt=""""></a>&nbsp;Nome do Cliente &nbsp;<a href="/"javascript: Ordenar_Resultado('TB_CLIENTES.Nome desc')""><img src=""././img/baixo.gif"" border=""0"" align=""absmiddle"" alt=""""></a></th>"
	sConteudo = sConteudo & "    <th width=""18%"" nowrap scope=""col""><a href="/"javascript: Ordenar_Resultado('TB_CLIENTES_TIPO.Descricao')""><img src=""././img/cima.gif"" border=""0"" align=""absmiddle"" alt=""""></a>&nbsp;Grupo &nbsp;<a href="/"javascript: Ordenar_Resultado('TB_CLIENTES_TIPO.Descricao desc')""><img src=""././img/baixo.gif"" border=""0"" align=""absmiddle"" alt=""""></a></th>"
	sConteudo = sConteudo & "    <th width=""15%"" nowrap scope=""col""><a href="/"javascript: Ordenar_Resultado('TB_FORMAS_PAGAMENTO.Descricao')""><img src=""././img/cima.gif"" border=""0"" align=""absmiddle"" alt=""""></a>&nbsp;Pagamento &nbsp;<a href="/"javascript: Ordenar_Resultado('TB_FORMAS_PAGAMENTO.Descricao desc')""><img src=""././img/baixo.gif"" border=""0"" align=""absmiddle"" alt=""""></a></th>"
	sConteudo = sConteudo & "    <th width=""10%"" nowrap bgcolor="""& Application("bgcolor3") &""" scope=""col""><a href="/"javascript: Ordenar_Resultado('TB_PEDIDOS.total')""><img src=""././img/cima.gif"" border=""0"" align=""absmiddle"" alt=""""></a>&nbsp;Valor &nbsp;<a href="/"javascript: Ordenar_Resultado('TB_PEDIDOS.total desc')""><img src=""././img/baixo.gif"" border=""0"" align=""absmiddle"" alt=""""></a></th>"
	sConteudo = sConteudo & "    <th width=""15%"" nowrap bgcolor="""& Application("bgcolor3") &""" scope=""col""><a href="/"javascript: Ordenar_Resultado('TB_STATUS.Descricao')""><img src=""././img/cima.gif"" border=""0"" align=""absmiddle"" alt=""""></a>&nbsp;Status &nbsp;<a href="/"javascript: Ordenar_Resultado('TB_STATUS.Descricao desc')""><img src=""././img/baixo.gif"" border=""0"" align=""absmiddle"" alt=""""></a></th>"
	sConteudo = sConteudo & "  </tr>"
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina
		iExibe = 1

		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCampo1 = trim(rs("Nome")) & "&nbsp;" & trim(rs("SobreNome"))
			sCampo2 = trim(rs("grupo"))
			sCampo3 = Server.aspEncode(trim(rs("pagamento")))
			sCampo4 = trim(rs("status"))
			sCampo5 = RetornaCasasDecimais(cdbl(trim(rs("total"))),0)
			sCampo6 = trim(rs("dataCad"))
			
			sConteudo = sConteudo & "  <tr >" & vbcrlf
			sConteudo = sConteudo & "    <th align=""center"" ><a href="/"DetalhesPedido.asp?id="& sId &""">"& sId &"</a></th>" & vbcrlf
			sConteudo = sConteudo & "    <th align=""left"" ><a href="/"DetalhesPedido.asp?id="& sId &""">"& sCampo6 &"</th>" & vbcrlf
			sConteudo = sConteudo & "    <th align=""left"" ><a href="/"DetalhesPedido.asp?id="& sId &""">"& sCampo1 &"</th>" & vbcrlf
			sConteudo = sConteudo & "    <th align=""left"" ><a href="/"DetalhesPedido.asp?id="& sId &""">"& sCampo2 &"</th>" & vbcrlf
			sConteudo = sConteudo & "    <th align=""left"" ><a href="/"DetalhesPedido.asp?id="& sId &""">"& sCampo3 &"</th>" & vbcrlf
			sConteudo = sConteudo & "    <th align=""center"" ><a href="/"DetalhesPedido.asp?id="& sId &""">"& sCampo5 &" </th>" & vbcrlf
			sConteudo = sConteudo & "    <th align=""center"" ><a href="/"DetalhesPedido.asp?id="& sId &""">"& sCampo4 &"</th>" & vbcrlf
			sConteudo = sConteudo & "  </tr>" & vbcrlf
		Rs.movenext
		loop
		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_Todas_Pedidos_Admin = sConteudo
end function





'========================================================================
function Mostra_Todas_Produtos_Admin(Busca, Flag, OpcCombo)
	dim sId, sCampo1, sCampo2, sCampo3, sCampo4, sCampo5
	dim sConteudo, Rs, sSQl, sMontagem, ArrOpcCombo

	if(Busca = "" and Flag = "" and OpcCombo = "") then exit function
	
	if(Flag <> "") then
		select case Flag
			case "promocao"
				sMontagem = " where TB_PRODUTOS.Promocao = 1"
			case "lancamento"
				sMontagem = " where TB_PRODUTOS.Lancamento = 1"
			case "disponivel"
				sMontagem = " where TB_PRODUTOS.ativo = 1"
			case "ndisponivel"
				sMontagem = " where TB_PRODUTOS.ativo = 0"
			case "reserva"
				sMontagem = " where (TB_PRODUTOS.Qte_Estoque = TB_PRODUTOS.Qte_Reserva or TB_PRODUTOS.Qte_Estoque < TB_PRODUTOS.Qte_Reserva or TB_PRODUTOS.Qte_Estoque = TB_PRODUTOS.Qte_Reserva)"
			case "all"
				sMontagem = ""
		end select
	else
		if(Busca <> "") then sMontagem = " where TB_PRODUTOS.descricao like '%"& Busca &"%' "

		if(OpcCombo <> "") then 
			if(len(OpcCombo) > 3) then
				ArrOpcCombo = split(OpcCombo, ",")
				sMontagem = " where TB_PRODUTOS.id_categoria = "& ArrOpcCombo(1) &" and  TB_PRODUTOS.id_subcategoria = " & ArrOpcCombo(2)
			else
				exit function
			end if
		end if
	end if
	
	call Abre_Conexao()
	
	sSQl = "Select " & _
				"TB_PRODUTOS.* " & _
			" from  " & _
				"TB_PRODUTOS " & sMontagem
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by TB_PRODUTOS.id desc"
	end if

	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3 ' adUseClient
	Rs.CursorType = 3
	Rs.Open sSQL, oConn
	
	sConteudo = sConteudo & "<table width=""96%""  border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"" >"
	sConteudo = sConteudo & "  <tr align=""center"" bgcolor=""#E9E9E9"" class=""Subtitulo"">"
	sConteudo = sConteudo & "    <td  nowrap scope=""col""><div align=""center""><a href="/"javascript: Ordenar_Resultado('id')""><img src=""././img/cima.gif"" border=""0"" align=""absmiddle"" alt=""""></a>&nbsp;C&oacute;digo&nbsp;<a href="/"javascript: Ordenar_Resultado('id desc')""><img src=""././img/baixo.gif"" border=""0"" align=""absmiddle"" alt=""""></a></div></td>"
	sConteudo = sConteudo & "    <td  scope=""col""><div align=""left""><a href="/"javascript: Ordenar_Resultado('Descricao')""><img src=""././img/cima.gif"" border=""0"" align=""absmiddle"" alt=""""></a>&nbsp;Descricao BR &nbsp;<a href="/"javascript: Ordenar_Resultado('Descricao desc')""><img src=""././img/baixo.gif"" border=""0"" align=""absmiddle"" alt=""""></a></div></td>"
	sConteudo = sConteudo & "    <td  scope=""col""><div align=""left""><a href="/"javascript: Ordenar_Resultado('Descricao_jp')""><img src=""././img/cima.gif"" border=""0"" align=""absmiddle"" alt=""""></a>&nbsp;Descricao JP &nbsp;<a href="/"javascript: Ordenar_Resultado('Descricao_jp desc')""><img src=""././img/baixo.gif"" border=""0"" align=""absmiddle"" alt=""""></a></div></td>"
	sConteudo = sConteudo & "    <td  scope=""col""><div align=""center"">Categoria </div></td>"
	sConteudo = sConteudo & "    <td  scope=""col""><div align=""center"">Valor </div></td>"
	sConteudo = sConteudo & "    <td  scope=""col""><div align=""center""><a href="/"javascript: Ordenar_Resultado('ativo')""><img src=""././img/cima.gif"" border=""0"" align=""absmiddle"" alt=""""></a>&nbsp;Ativo &nbsp;<a href="/"javascript: Ordenar_Resultado('ativo desc')""><img src=""././img/baixo.gif"" border=""0"" align=""absmiddle"" alt=""""></a></div></td>"
	sConteudo = sConteudo & "    <td scope=""col"">&nbsp;</td>"
	sConteudo = sConteudo & "    </tr>"
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina
		iExibe = 1

		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCampo1 = trim(rs("descricao"))
			sCampo2 = trim(rs("descricao_Jp"))
			sCampo3 = Recupera_Campos_Db_oConn("TB_CATEGORIAS", trim(rs("id_categoria")), "id", "categoria")
			sCampo4 = trim(rs("ativo"))
			sCampo5 = RetornaCasasDecimais(cdbl(trim(rs("Preco_Consumidor"))),0)
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center"">"& sId &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"altera_produto.asp?id="& sId &""">"& sCampo1 &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"altera_produto.asp?id="& sId &""">"& sCampo2 &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center""><a href="/"altera_produto.asp?id="& sId &""">"& sCampo3 &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center""><a href="/"altera_produto.asp?id="& sId &""">"& sCampo5 &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"altera_produto.asp?id="& sId &""">"& fBoolSN(sCampo4) &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><a href="/"javascript:excluir('"& sId &"')""><img src=""././img/excluir.gif"" border=""0"" align=""absmiddle"" alt=""""></a></td>" & vbcrlf
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop
		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_Todas_Produtos_Admin = sConteudo
end function


'========================================================================
function Monta_Combo_Status(sCombo)
	dim sSql
	
	' COMBO STATUS
	sSql = "Select * from TB_STATUS where ativo = 1"
	Monta_Combo_Status = "<br><br>STATUS: " & Combo(sSql, true, true, "combo", "", "Selecione um...", "id", "Descricao", "id", sCombo, " onchange='javascript: Muda_Combo(this.value);' ")
end function


'========================================================================
function Monta_Combo_SegSub(sCombo)
	dim sSql
	
	if(sCombo = "") then 
		' COMBO CATEGORIA
		sSql = "Select * from TB_CATEGORIAS where ativo = 1"
		Monta_Combo_SegSub = "<br><br>SEGMENTO: " & Combo(sSql, true, true, "combo", "", "Selecione um...", "id", "categoria", "id", "", " onchange='javascript: Muda_Combo(this.value);' ")
	else
		' COMBO SUB-CATEGORIA
		if(left(sCombo,1) = ",") then sCombo = replace(sCombo, ",","")
		sSql = "Select * from TB_SUBCATEGORIA where id_categoria = "& sCombo &" and ativo = 1"
		Monta_Combo_SegSub = "<br><br>SUB-SEGMENTO: " & Combo(sSql, true, true, "combo", "", "Selecione um...", "id", "subcategoria", "id", "", " onchange='javascript: Muda_Combo(this.value);' ") & " - <a href="/"mostra_produtos.asp"">Iniciar Consultar</a>"
	end if
	
end function


'========================================================================
function Retorna_Total_Email_Grupo(sIdGRupo)
	dim Rs, sSQl
	
	call Abre_Conexao()
	
	sSQL = "Select count(id) as total from TB_NEWSLETTER_EMAILS where id_tipo_newsletter = "& sIdGRupo &" and  ativo = 1"
	set Rs = oConn.execute(sSQL)
	if(not rs.eof) then Retorna_Total_Email_Grupo = rs("total")
	set Rs = nothing
	
	call Fecha_Conexao()
end function


'========================================================================
function Mostra_Todas_Tabelas_Esgotados_Admin(Filtro)
	dim sId, sCampo1, sCampo2, sCampo3, sCampo4, sCampo5, sCampo6, sCampo7
	dim sConteudo, Rs, sSQl

	call Abre_Conexao()

	sSQl = "Select distinct " & _
				"TB_LISTA_ESGOTADOS.IdProduto " & _
			" from  " & _
				"TB_LISTA_ESGOTADOS " & _
			" where 1=1 "
	if(Filtro = "1") then sSQl = sSQl & " and (isnull(DataNotificado))  "
	if(Filtro = "2") then sSQl = sSQl & " and (not isnull(DataNotificado))  "
		
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by TB_LISTA_ESGOTADOS.IdProduto "
	end if
	
	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina
		iExibe = 1
		
		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = trim(rs("IdProduto"))
			sCampo1 = Recupera_Campos_Db_oConn("TB_PRODUTOS", sId, "id", "descricao") 
			sCampo3 = Recupera_Campos_Db_oConn("TB_PRODUTOS", sId, "id", "Qte_Estoque")
			sCampo4 = "(" & TotalClientesAvisadosEsgotados(sId) & ") - <a href="/"javascript:AbrePopUp('pop_ver_clientes.asp?produto="& sId &"', '500','450')"">Ver clientes</a>"
			sExcluirDesc = "<a href="/"javascript:excluir('"& sId &"')""><img src=""././img/excluir.gif"" border=""0"" align=""absmiddle"" alt=""""></a>"
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center"">"& sId &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"/admin.styllus/Produtos/altera_produto.asp?id="& sId &""">"& sCampo1 &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center"">"& sCampo3 &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center"">"& sCampo4 &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col"" align=""center"">"& sExcluirDesc &"</td>" & vbcrlf
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop
		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_Todas_Tabelas_Esgotados_Admin = sConteudo
end function

'========================================================================
function Mostra_Todas_Tabelas_Provincias_Admin(Provincia)
	dim sId, sCategoria, sCategoriaJp
	dim sConteudo, Rs, sSQl, sCampo1, sCampo2, sCampo3

	sSQl = "Select " & _
				"* " & _
			" from  " & _
				"TB_CLIENTES_PROVINCIAS "
	if(Provincia <> "") then 
		sSQl = sSQl & " where Descricao like '%"& Recupera_Campos_Db("TB_CLIENTES_PROVINCIAS", Provincia, "id", "Descricao") &"%' "
	end if
	
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by TB_CLIENTES_PROVINCIAS.descricao"
	end if

	call Abre_Conexao()

	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina
		iExibe = 1
		
		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCategoria = trim(rs("descricao"))
			sCategoriaJp = trim(rs("descricao_jp"))
			sCampo1 = 0
			sCampo2 = 0
			sCampo3 = 0
			if(trim(rs("FreteDe")) <> "0") then sCampo1 = RetornaCasasDecimais(cdbl(trim(rs("FreteDe"))),2)
			if(trim(rs("FreteAte")) <> "0") then sCampo2 = RetornaCasasDecimais(cdbl(trim(rs("FreteAte"))),2)
			if(trim(rs("Valor")) <> "0") then sCampo3 = RetornaCasasDecimais(cdbl(trim(rs("Valor"))),2)
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center"">"& sId &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"AlteraFrete.asp?id="& sId &""">"& sCategoria &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"AlteraFrete.asp?id="& sId &""">"& sCategoriaJp &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"AlteraFrete.asp?id="& sId &""">"& sCampo1 &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"AlteraFrete.asp?id="& sId &""">"& sCampo2 &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"AlteraFrete.asp?id="& sId &""">"& sCampo3 &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center"">"& fBoolSN(trim(rs("ativo"))) &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><a href="/"javascript:excluir('"& sId &"')""><img src=""././img/excluir.gif"" border=""0"" align=""absmiddle"" alt=""""></a></td>" & vbcrlf
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop
		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_Todas_Tabelas_Provincias_Admin = sConteudo
end function



'========================================================================
function Mostra_Clientes_GRupos(sTipoNewsLetter)
	dim sId, sCategoria
	dim sConteudo, Rs, sSQl

	call Abre_Conexao()

	sSQl = "Select " & _
				"* " & _
			" from  " & _
				"TB_CLIENTES " & _
			"where " & _
				"Id_Tipo = "& sTipoNewsLetter
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by TB_CLIENTES.Nome, TB_CLIENTES.datacad "
	end if
	
	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina
		iExibe = 1
		
		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCategoria = trim(rs("nome")) & " " & trim(rs("sobrenome"))
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center"">"& sId &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"javascript: opener.location='altera_cliente.asp?id="& sId &"'; window.close();"">"& sCategoria &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center"">"& fBoolSN(trim(rs("ativo"))) &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop
		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_Clientes_GRupos = sConteudo
end function



'========================================================================
function Mostra_Emails_NewsLetter(sTipoNewsLetter)
	dim sId, sCategoria
	dim sConteudo, Rs, sSQl

	call Abre_Conexao()

	sSQl = "Select " & _
				"* " & _
			" from  " & _
				"TB_NEWSLETTER_EMAILS " & _
			"where " & _
				"Id_Tipo_Newsletter = "& sTipoNewsLetter &" and ativo = 1"
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by TB_NEWSLETTER_EMAILS.Descricao, TB_NEWSLETTER_EMAILS.datacad "
	end if
	
	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina
		iExibe = 1
		
		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCategoria = trim(rs("descricao"))
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center"">"& sId &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"javascript: opener.location='AlteraEmail.asp?id="& sId &"'; window.close();"">"& sCategoria &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop
		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_Emails_NewsLetter = sConteudo
end function


'========================================================================
function Mostra_fotos(sTipoNewsLetter)
	dim sId, sCategoria, sCategoria2
	dim sConteudo, Rs, sSQl

	call Abre_Conexao()

	sSQl = "Select " & _
				"* " & _
			" from  " & _
				"TB_GALERIA_FOTOS " & _
			"where " & _
				"Id_tema = "& sTipoNewsLetter &" and ativo = 1"
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by Imagem_Desc, datacad "
	end if
	
	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina
		iExibe = 1
		
		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCategoria = trim(rs("Imagem_Path"))
			sCategoria2 = trim(rs("Imagem_Desc"))
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center"">"& sCategoria2 &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><img src=""/images/images_galeria/"& sCategoria &""" border=0 alt=""""></a></p></td>" & vbcrlf
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop
		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_fotos = sConteudo
end function
'========================================================================
function Mostra_Todas_NewsLetterEmail_Admin(bEmail, bGrupo)
	dim sId, sCategoria, sCategoriaJp, sAtivo
	dim sConteudo, Rs, sSQl

	call Abre_Conexao()

	sSQl = "Select " & _
				"TB_NEWSLETTER_EMAILS.*,  TB_NEWSLETTER_TIPO.descricao as tipo" & _
			" from  " & _
				"TB_NEWSLETTER_EMAILS, TB_NEWSLETTER_TIPO " & _
			"where " & _
				"TB_NEWSLETTER_EMAILS.Id_Tipo_Newsletter = TB_NEWSLETTER_TIPO.id"
	if(bEmail <> "") then sSQl = sSQl & " and TB_NEWSLETTER_EMAILS.descricao like '%"& bEmail &"%'"
	if(bGrupo <> "") then sSQl = sSQl & " and TB_NEWSLETTER_EMAILS.Id_Tipo_Newsletter = " & bGrupo
	
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by TB_NEWSLETTER_EMAILS.Descricao, TB_NEWSLETTER_EMAILS.datacad "
	end if
	
	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina

		iExibe = 1
		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCategoria = trim(rs("descricao"))
			sCategoriaJp = trim(rs("tipo"))
			sAtivo = rs("ativo")
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center""><a href="/"alteraemail.asp?id="& sId &""">"& sId &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"alteraemail.asp?id="& sId &""">"& sCategoriaJp &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"alteraemail.asp?id="& sId &""">"& sCategoria &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"alteraemail.asp?id="& sId &""">"& fBoolSN(sAtivo) &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><a href="/"javascript:excluir('"& sId &"')""><img src=""././img/excluir.gif"" border=""0"" align=""absmiddle"" alt=""""></a></td>" & vbcrlf
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop
		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_Todas_NewsLetterEmail_Admin = sConteudo
end function




'========================================================================
function Mostra_Todas_NewsLetter_Admin(sTipo)
	dim sId, sCategoria, sCategoriaJp, sCompra, sFrete, sAtivo
	dim sConteudo, Rs, sSQl

	call Abre_Conexao()

	sSQl = "Select " & _
				"TB_NEWSLETTER.*,  TB_NEWSLETTER_TIPO.descricao " & _
			" from  " & _
				"TB_NEWSLETTER, TB_NEWSLETTER_TIPO " & _
			"where " & _
				" TB_NEWSLETTER.Id_Tipo_Newsletter = TB_NEWSLETTER_TIPO.id and " & _
				" TB_NEWSLETTER.Id_Tipo = '"& sTipo &"' "
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by TB_NEWSLETTER.datacad desc"
	end if
	
	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina

		iExibe = 1
		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCategoria = trim(rs("descricao"))
			sCategoriaJp = Server.aspEncode(trim(rs("assunto")))
			sCompra = Server.aspEncode(CortaLen(trim(rs("mensagem")),30))
			sAtivo = rs("Enviado")
			sArquivo = "AlteraInfo.asp"
			if(sTipo = "T") then sArquivo = "AlteraInfoText.asp"
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center""><a href="/""& sArquivo &"?id="& sId &""">"& sId &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/""& sArquivo &"?id="& sId &""">"& sCategoria &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/""& sArquivo &"?id="& sId &""">"& sCategoriaJp &"</p></td>" & vbcrlf
			if(sAtivo) then 
				sConteudo = sConteudo & "	<td scope=""col""><p align=""left"">"& sCompra &"</p></td>" & vbcrlf
			else
				sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/""& sArquivo &"?id="& sId &""">"& sCompra &"</a></p></td>" & vbcrlf
			end if
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center"">"& fBoolSN(sAtivo) &"</p></td>" & vbcrlf
			if(sAtivo) then 
				sConteudo = sConteudo & "	<td scope=""col"">&nbsp;</td>" & vbcrlf
				sConteudo = sConteudo & "	<td scope=""col""><a href="/"javascript:excluir('"& sId &"')""><img src=""././img/excluir.gif"" border=""0"" align=""absmiddle"" alt=""""></a></td>" & vbcrlf
			else
				sConteudo = sConteudo & "	<td scope=""col""><a href="/"javascript:enviar('"& sId &"', '"& sTipo &"')"">Enviar</a></td>" & vbcrlf
				sConteudo = sConteudo & "	<td scope=""col""><a href="/"javascript:excluir('"& sId &"')""><img src=""././img/excluir.gif"" border=""0"" align=""absmiddle"" alt=""""></a></td>" & vbcrlf
			end if
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop
		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_Todas_NewsLetter_Admin = sConteudo
end function


'========================================================================
function Mostra_Todas_Tabelas_FechaLoja_Admin()
	dim sId, sCategoria, sCategoriaJp, sCompra, sFrete, sAtivo
	dim sConteudo, Rs, sSQl

	call Abre_Conexao()

	sSQl = "Select * from TB_FECHAR_LOJA"
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by id desc "
	end if
	
	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina

		iExibe = 1
		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCategoria = trim(rs("descricaoabre"))
			sCategoriaJp = trim(rs("descricaofecha"))
			sCompra = trim(rs("dataabre"))
			sFrete = trim(rs("datafecha"))
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center"">"& sId &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left"">"& sCategoria &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left"">"& sCategoriaJp &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center"">"& sCompra &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center"">"& sFrete &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop
		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_Todas_Tabelas_FechaLoja_Admin = sConteudo
end function


'========================================================================
function Mostra_Todas_Tabelas_TESURIYOU_Admin()
	dim sId, sCategoria, sCategoriaJp, sCompra, sFrete, sAtivo
	dim sConteudo, Rs, sSQl

	call Abre_Conexao()

	sSQl = "Select * from TB_TESURIYOU"
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by descricao, datacad "
	end if
	
	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina

		iExibe = 1
		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCategoria = trim(rs("descricao"))
			sCategoriaJp = trim(rs("descricao_jp"))
			sCompra = cdbl(trim(rs("valor_compra")))
			sFrete = cdbl(trim(rs("valor_frete")))
			sAtivo = rs("ativo")
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center""><a href="/"alteratesu.asp?id="& sId &""">"& sId &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"alteratesu.asp?id="& sId &""">"& sCategoria &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"alteratesu.asp?id="& sId &""">"& sCategoriaJp &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"alteratesu.asp?id="& sId &""">"& sCompra &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"alteratesu.asp?id="& sId &""">"& sFrete &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"alteratesu.asp?id="& sId &""">"& fBoolSN(sAtivo) &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><a href="/"javascript:excluir('"& sId &"')""><img src=""././img/excluir.gif"" border=""0"" align=""absmiddle"" alt=""""></a></td>" & vbcrlf
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop
		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_Todas_Tabelas_TESURIYOU_Admin = sConteudo
end function



'========================================================================
function Mostra_Todas_Tabelas_Frete_Admin(Provincia)
	dim sId, sCategoria, sCategoriaJp, sCompra, sFrete, sAtivo
	dim sConteudo, Rs, sSQl

	call Abre_Conexao()

	sSQl = "Select * from TB_FRETE"
	if(Provincia <> "") then sSQl = sSQl & " where Descricao like '%"& Recupera_Campos_Db("TB_CLIENTES_PROVINCIAS", Provincia, "id", "Descricao") &"%' "
	
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by descricao, datacad "
	end if
	
	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina

		iExibe = 1
		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCategoria = trim(rs("descricao"))
			sCategoriaJp = trim(rs("descricao_jp"))
			sCompra = cdbl(trim(rs("valor_compra")))
			sFrete = cdbl(trim(rs("valor_frete")))
			sAtivo = rs("ativo")
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center""><a href="/"alterafrete.asp?id="& sId &""">"& sId &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"alterafrete.asp?id="& sId &""">"& sCategoria &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"alterafrete.asp?id="& sId &""">"& sCategoriaJp &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"alterafrete.asp?id="& sId &""">"& sCompra &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"alterafrete.asp?id="& sId &""">"& sFrete &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"alterafrete.asp?id="& sId &""">"& fBoolSN(sAtivo) &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><a href="/"javascript:excluir('"& sId &"')""><img src=""././img/excluir.gif"" border=""0"" align=""absmiddle"" alt=""""></a></td>" & vbcrlf
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop
		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_Todas_Tabelas_Frete_Admin = sConteudo
end function


'========================================================================
function MontagemLink(Id, Tipo) 
	select case Tipo
		case "TB_NEWSLETTER_TIPO"
			MontagemLink = "<a href="/"javascript:AbrePopUp('pop_ver_emails.asp?id="& Id &"', '500','450')"">Ver E-mail(s)</a> | "
		case "TB_CLIENTES_TIPO"
			MontagemLink = "<a href="/"javascript:AbrePopUp('pop_ver_emails.asp?id="& Id &"', '500','450')"">Ver Cliente(s)</a> | "
	end select
end function


'========================================================================
function Mostra_Todas_Tabelas_TipoUser_Admin()
	dim sId, sCampo1, sCampo2, sAtivo
	dim sConteudo, Rs, sSQl, sNameArq

	call Abre_Conexao()
	
	sSQl = "Select * from TB_USUARIO_TIPO"
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by tipo"
	end if
	
	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina

		iExibe = 1
		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCampo1 = trim(rs("tipo"))
			sAtivo = rs("ativo")
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center""><a href="/"alteragrupo.asp?id="& sId &""">"& sId &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"alteragrupo.asp?id="& sId &""">"& sCampo1 &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"alteragrupo.asp?id="& sId &""">"& fBoolSN(sAtivo) &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col"">"& MontagemLink(sId, sTabela) &"<a href="/"javascript:excluir('"& sId &"')""><img src=""././img/excluir.gif"" border=""0"" align=""absmiddle"" alt=""""></a></td>" & vbcrlf
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop
		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	
	Mostra_Todas_Tabelas_TipoUser_Admin = sConteudo
end function


'========================================================================
function Mostra_Todas_Tabelas_GruposClientes_Admin()
	dim sId, sCampo1, sCampo2, sCampo3, sAtivo
	dim sConteudo, Rs, sSQl, sNameArq

	call Abre_Conexao()
	
	sSQl = "Select * from TB_CLIENTES_TIPO"
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by descricao, datacad "
	end if
	
	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina

		iExibe = 1
		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCampo1 = trim(rs("descricao"))
			sCampo2 = trim(rs("descricao_jp"))
			sCampo3 = trim(rs("desconto"))
			sAtivo = rs("ativo")
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center""><a href="/"alteragrupo.asp?id="& sId &""">"& sId &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"alteragrupo.asp?id="& sId &""">"& sCampo1 &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"alteragrupo.asp?id="& sId &""">"& sCampo2 &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"alteragrupo.asp?id="& sId &""">"& sCampo3 &"%</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"alteragrupo.asp?id="& sId &""">"& fBoolSN(sAtivo) &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col"">"& MontagemLink(sId, "TB_CLIENTES_TIPO") &"<a href="/"javascript:excluir('"& sId &"')""><img src=""././img/excluir.gif"" border=""0"" align=""absmiddle"" alt=""""></a></td>" & vbcrlf
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop
		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_Todas_Tabelas_GruposClientes_Admin = sConteudo
end function


'========================================================================
function Mostra_Todas_Tabelas_Banner(IdFormato)
	dim sId, sCampo1, sCampo2, sCampo3, sCampo4, sAtivo
	dim sConteudo, Rs, sSQl, sNameArq

	call Abre_Conexao()
	
	Select case IdFormato
		case "1"
			sArquivo = "AlteraBannerSuperior"
		case "2"
			sArquivo = "AlteraBanner120x60"
		case "3"
			sArquivo = "AlteraBannerPopUp"
	end Select
	
	sSQl = "Select * from TB_BANNERS where cdcliente = "& sCdCliente &" and Id_Formato = " & IdFormato
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by Descricao, Datacad "
	end if
	
	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina

		iExibe = 1
		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCampo1 = replace(replace(replace(replace(lcase(trim(rs("descricao"))),"<br>",""),"<p>",""),"<table ",""),"</table>","")
			sCampo2 = Legenda_Horario(trim(rs("Id_FaixaHorario")))
			sCampo3 = trim(rs("DataIni"))
			sCampo4 = trim(rs("DataFim"))
			sAtivo = rs("ativo")
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""" valign=""top"">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center""><a href="/""& sArquivo &".asp?id="& sId &""">"& sId &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/""& sArquivo &".asp?id="& sId &""">"& sCampo1 &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/""& sArquivo &".asp?id="& sId &""">"& sCampo2 &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/""& sArquivo &".asp?id="& sId &""">"& sCampo3 &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/""& sArquivo &".asp?id="& sId &""">"& sCampo4 &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/""& sArquivo &".asp?id="& sId &""">"& fBoolSN(sAtivo) &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><a href="/"javascript:excluir('"& sId &"')""><img src=""././img/excluir.gif"" border=""0"" align=""absmiddle"" alt=""""></a></td>" & vbcrlf
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop
		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_Todas_Tabelas_Banner = sConteudo
end function




'========================================================================
function Mostra_Todas_Tabelas_Padrao_Admin(sTabela)
	dim sId, sCampo1, sCampo2, sAtivo
	dim sConteudo, Rs, sSQl, sNameArq

	call Abre_Conexao()
	
	select case sTabela
		case "TB_TAMANHOS"
			sNameArq = "tamanho"
		case "TB_CORES"
			sNameArq = "cor"
		case "TB_SABORES"
			sNameArq = "sabor"
		case "TB_FRAGRANCIAS"
			sNameArq = "fragrancia"
		case "TB_TIPOS"
			sNameArq = "tipo"
		case "TB_NEWSLETTER_TIPO"
			sNameArq = "grupo"
		case "TB_STATUS"
			sNameArq = "status"
		case "TB_PACOTES"
			sNameArq = "pacote"
	end select
	
	sSQl = "Select * from " & sTabela
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by descricao, datacad "
	end if
	
	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina

		iExibe = 1
		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCampo1 = trim(rs("descricao"))
			sCampo2 = trim(rs("descricao_jp"))
			sAtivo = rs("ativo")
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center""><a href="/"altera"& sNameArq &".asp?id="& sId &""">"& sId &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"altera"& sNameArq &".asp?id="& sId &""">"& sCampo1 &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"altera"& sNameArq &".asp?id="& sId &""">"& sCampo2 &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"altera"& sNameArq &".asp?id="& sId &""">"& fBoolSN(sAtivo) &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col"">"& MontagemLink(sId, sTabela) &"<a href="/"javascript:excluir('"& sId &"')""><img src=""././img/excluir.gif"" border=""0"" align=""absmiddle"" alt=""""></a></td>" & vbcrlf
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop
		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_Todas_Tabelas_Padrao_Admin = sConteudo
end function



'========================================================================
function Mostra_Todas_ValePresente_Admin()
	dim sId, sCategoria, sCategoriaJp, sCategoriaDesc, sAtivo
	dim sConteudo, Rs, sSQl

	call Abre_Conexao()

	sSQl = "Select * from TB_VALE_PRESENTES"
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by valor, datacad "
	end if
	
	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina

		iExibe = 1
		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCategoria = trim(rs("Descricao"))
			sCategoriaJp = Server.aspEncode(trim(rs("Descricao_Jp")))
			sCategoriaDesc = trim(rs("valor"))
			sAtivo = rs("ativo")
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""center""><a href="/"AlteraValePresente.asp?id="& sId &""">"& sId &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"AlteraValePresente.asp?id="& sId &""">"& sCategoriaDesc &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"AlteraValePresente.asp?id="& sId &""">"& sCategoria &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"AlteraValePresente.asp?id="& sId &""">"& sCategoriaJp &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"AlteraValePresente.asp?id="& sId &""">"& fBoolSN(sAtivo) &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><a href="/"javascript:excluir('"& sId &"')""><img src=""././img/excluir.gif"" border=""0"" align=""absmiddle"" alt=""""></a></td>" & vbcrlf
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop
		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_Todas_ValePresente_Admin = sConteudo
end function

'========================================================================
function Mostra_Todas_Moedas_Admin()
	dim sId, sCategoria, sCategoriaJp, sCategoriaDesc, sAtivo
	dim sConteudo, Rs, sSQl

	call Abre_Conexao()

	sSQl = "Select * from TB_MOEDAS"
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by moeda, datacad "
	end if
	
	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina

		iExibe = 1
		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCategoria = trim(rs("moeda"))
			sCategoriaJp = Server.aspEncode(trim(rs("simbolo")))
			sCategoriaDesc = RetornaCasasDecimais(cdbl(trim(rs("valorref"))),2)
			sAtivo = rs("ativo")
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""left""><a href="/"alteramoeda.asp?id="& sId &""">"& sId &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"alteramoeda.asp?id="& sId &""">"& sCategoria &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"alteramoeda.asp?id="& sId &""">"& sCategoriaJp &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"alteramoeda.asp?id="& sId &""">"& sCategoriaDesc &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"alteramoeda.asp?id="& sId &""">"& fBoolSN(sAtivo) &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><a href="/"javascript:excluir('"& sId &"')""><img src=""././img/excluir.gif"" border=""0"" align=""absmiddle"" alt=""""></a></td>" & vbcrlf
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop
		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_Todas_Moedas_Admin = sConteudo
end function


' =========================================================================
function Recupera_Grupos(sName, sSize)
	dim sSql, Rs, Rs1, sSelected
	dim arrPer, lCont, sDis

	sDis = ""
	Recupera_Grupos = "<select name="""& sName &""" size="""& sSize &""" multiple "& sDis &">"
	
	call Abre_Conexao()
	
	sSQl = "Select * from TB_CLIENTES_TIPO where ativo = 1"
	set Rs = oConn.execute(sSql)
	
	if(Not Rs.Eof) then
		do while Not Rs.Eof
			Recupera_Grupos = Recupera_Grupos & "<option value="""& rs("id") &""">"& trim(rs("descricao")) &"</option>"
		Rs.movenext
		loop
	end if
	
	Recupera_Grupos = Recupera_Grupos & "</select>"
end function


' =========================================================================
function Recupera_Permissoes(sName, sTipo, sSize)
	dim sSql, Rs, Rs1, sSelected
	dim arrPer, lCont, sDis

	sDis = ""
	if(lcase(sName) = "tipoa") then sDis = " disabled "
	Recupera_Permissoes = "<select name="""& sName &""" size="""& sSize &""" multiple "& sDis &">"
	
	
	if(lcase(sName) <> "tipoa") then 
		sSQl = "Select * from TB_MENU_ADMIN where ativo = 1 order by menu"
		set Rs1 = oConn.execute(sSql)
		if(Not Rs1.Eof) then 
			do while Not Rs1.Eof
				Recupera_Permissoes = Recupera_Permissoes & "<option value="""& rs1("id") &""">"& trim(rs1("menu")) &"</option>"
			Rs1.movenext
			loop
		end if
		set Rs1 = nothing
	
		Recupera_Permissoes = Recupera_Permissoes & "</select>"
		exit function
	end if
	
	call Abre_Conexao()
	
	sSQl = "Select * from TB_PERMISSOES where id_tipo = "& sTipo
	set Rs = oConn.execute(sSql)
	
	if(Not Rs.Eof) then
		arrPer = trim(rs("permissoes"))
		if(arrPer <> "") then arrPer = split(arrPer, ",")
		
		for lCont = 0 to ubound(arrPer)
			sSQl = "Select * from TB_MENU_ADMIN where id = "& arrPer(LCont)
			set Rs1 = oConn.execute(sSql)
			if(Not Rs.Eof) then Recupera_Permissoes = Recupera_Permissoes & "<option value="""& rs1("id") &""">"& trim(rs1("menu")) &"</option>"
			set Rs1 = nothing
		next
	end if
	
	Recupera_Permissoes = Recupera_Permissoes & "</select>"
end function


'========================================================================
function Mostra_Todas_Usuarios_Admin()
	dim sId, sCategoria, sSubCategoriaJp, sSubCategoria, sAtivo
	dim sConteudo, Rs, sSQl

	call Abre_Conexao()

	sSQl = "Select " & _
				"TB_USUARIOS.*,  TB_USUARIO_TIPO.tipo " & _
			" from  " & _
				"TB_USUARIOS, TB_USUARIO_TIPO " & _
			"where " & _
				"TB_USUARIOS.id_tipo = TB_USUARIO_TIPO.id"
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by TB_USUARIOS.nome, TB_USUARIOS.datacad "
	end if
	
	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina

		iExibe = 1
		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCategoria = trim(rs("nome"))
			sSubCategoria = trim(rs("tipo"))
			sSubCategoriaJp = trim(rs("login"))
			sAtivo = rs("ativo")
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""left"">"& sId &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"alterausuario.asp?id="& sId &""">"& sSubCategoria &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"alterausuario.asp?id="& sId &""">"& sCategoria &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"alterausuario.asp?id="& sId &""">"& sSubCategoriaJp &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"alterausuario.asp?id="& sId &""">"& fBoolSN(sAtivo) &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><a href="/"javascript:excluir('"& sId &"')""><img src=""././img/excluir.gif"" border=""0"" align=""absmiddle"" alt=""""></a></td>" & vbcrlf
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop

		sConteudo = sConteudo & "</table>"

		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_Todas_Usuarios_Admin = sConteudo
end function



' ==========================================================
function Excluir_item(sTabela, Id)
	dim sSql, Rs
	
	call Abre_Conexao()
	
	select case sTabela
		case "TB_CATEGORIAS"
			' VERIFICA EXISTENCIA DE SUB CATEGORIAS VINCULADAS.
			sSql = "Select * from TB_SUBCATEGORIA where id_categoria = " & Id
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				sMsgJs = "Essa categoria nao pode ser excluida!\n\nExclua todas as SUB CATEGORIAS relacionadas!"
				Excluir_item = true
				exit function
			end if
			set Rs = nothing
		case "TB_MOEDAS"
			' VERIFICA EXISTENCIA DE PRODUTOS VINCULADAS.
			sSql = "Select * from TB_PRODUTOS order by descricao, datacad"
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				do while not rs.eof
					if(trim(rs("Id_Moeda")) = trim(Id)) then
						sMsgJs = "Esse ITEM nao pode ser excluido!\n\nExclua os vinculos!"
						Excluir_item = true
						exit function
					end if
				rs.movenext
				loop
			end if
			set Rs = nothing
		case "TB_FRAGRANCIAS"
			' VERIFICA EXISTENCIA DE PRODUTOS VINCULADAS.
			sSql = "Select * from TB_PRODUTOS order by descricao, datacad"
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				do while not rs.eof
					if(InStr(trim(rs("Id_Fragrancia")),Id) > 0) then
						sMsgJs = "Esse ITEM nao pode ser excluido!\n\nExclua os vinculos!"
						Excluir_item = true
						exit function
					end if
				rs.movenext
				loop
			end if
			set Rs = nothing
		case "TB_NEWSLETTER_TIPO"
			' VERIFICA EXISTENCIA NEWSLETTER VINCULADOS.
			sSql = "Select * from TB_NEWSLETTER where Id_Tipo_Newsletter = " & Id
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				sMsgJs = "Esse ITEM nao pode ser excluido!\n\nExclua os vinculos!"
				Excluir_item = true
				exit function
			end if
			set Rs = nothing

			' VERIFICA EXISTENCIA CLIENTES VINCULADOS.
			sSql = "Select * from TB_NEWSLETTER_EMAILS where Id_Tipo_Newsletter = " & Id
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				sMsgJs = "Esse ITEM nao pode ser excluido!\n\nExclua os vinculos!"
				Excluir_item = true
				exit function
			end if
			set Rs = nothing
			
			if(Id = 1 or Id = 3 or Id = 20 or Id = 21) then 
				sMsgJs = "Esse ITEM nao pode ser excluido!\n\nItem padrao do Sistema!"
				Excluir_item = true
				exit function
			end if
		case "TB_TAMANHOS"
			sSql = "Select * from TB_PRODUTOS order by descricao, datacad"
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				do while not rs.eof
					if(InStr(trim(rs("Id_Tamanho")),Id) > 0) then
						sMsgJs = "Esse ITEM nao pode ser excluido!\n\nExclua os vinculos!"
						Excluir_item = true
						exit function
					end if
				rs.movenext
				loop
			end if
			set Rs = nothing
		case "TB_TIPOS"
			sSql = "Select * from TB_PRODUTOS order by descricao, datacad"
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				do while not rs.eof
					if(InStr(trim(rs("Id_Tipos")),Id) > 0) then
						sMsgJs = "Esse ITEM nao pode ser excluido!\n\nExclua os vinculos!"
						Excluir_item = true
						exit function
					end if
				rs.movenext
				loop
			end if
			set Rs = nothing
		case "TB_PACOTES"
			sSql = "Select * from TB_PRODUTOS order by descricao, datacad"
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				do while not rs.eof
					if(InStr(trim(rs("Id_Pacote")),Id) > 0) then
						sMsgJs = "Esse ITEM nao pode ser excluido!\n\nExclua os vinculos!"
						Excluir_item = true
						exit function
					end if
				rs.movenext
				loop
			end if
			set Rs = nothing
		case "TB_CORES"
			sSql = "Select * from TB_PRODUTOS order by descricao, datacad"
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				do while not rs.eof
					if(InStr(trim(rs("Id_Cor")),Id) > 0) then
						sMsgJs = "Esse ITEM nao pode ser excluido!\n\nExclua os vinculos!"
						Excluir_item = true
						exit function
					end if
				rs.movenext
				loop
			end if
			set Rs = nothing
		case "TB_SABORES"
			sSql = "Select * from TB_PRODUTOS order by descricao, datacad"
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				do while not rs.eof
					if(InStr(trim(rs("Id_Sabor")),Id) > 0) then
						sMsgJs = "Esse ITEM nao pode ser excluido!\n\nExclua os vinculos!"
						Excluir_item = true
						exit function
					end if
				rs.movenext
				loop
			end if
			set Rs = nothing
		case "TB_PRODUTOS"
			' VERIFICA EXISTENCIA de produtos VINCULADAS na tabela de pedidos.
			sSql = "Select Id_Produto from TB_PEDIDOS_ITENS where Id_Produto = "& Id
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				sMsgJs = "Esse ITEM nao pode ser excluido!\n\nExclua os vinculos!"
				Excluir_item = true
				exit function
			end if
			set Rs = nothing
			
			
			sSql = "Select imagem from TB_IMAGENS_AUXILIARES where id_produto = " & Id
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				do while not Rs.eof
					if(trim(rs("imagem")) <> "") then call Apaga_Imagem(Server.MapPath("\images\produtos"), rs("imagem"))
				Rs.movenext
				loop
			end if
			set Rs = nothing
						
			' APAGA REGISTROS DA TABELA AUXILIAR
			sSql = "Delete from TB_IMAGENS_AUXILIARES WHERE id_produto = " & Id
			oConn.execute(sSql)
						
			' VERIFICA IMAGEM E APAGAR.
			sSql = "Select imagem from TB_PRODUTOS where id = " & Id
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				if(trim(rs("imagem")) <> "") then call Apaga_Imagem(Server.MapPath("\images\produtos"), rs("imagem"))
			end if
			set Rs = nothing
		case "TB_USUARIO_TIPO"
			sSql = "Select * from TB_USUARIOS where Id_Tipo = " & Id
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				sMsgJs = "Esse ITEM nao pode ser excluido!\n\nExclua os vinculos!"
				Excluir_item = true
				exit function
			end if
			set Rs = nothing
		case "TB_CLIENTES_TIPO"
			' VERIFICA EXISTENCIA DE CLIENTES VINCULADOS.
			sSql = "Select * from TB_CLIENTES where Id_Tipo = " & Id
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				sMsgJs = "Esse ITEM nao pode ser excluido!\n\nExclua os vinculos!"
				Excluir_item = true
				exit function
			end if
			set Rs = nothing

			if(Id = 1 or Id = 3 or Id = 20 or Id = 21) then 
				sMsgJs = "Esse ITEM nao pode ser excluido!\n\nItem padrao do Sistema!"
				Excluir_item = true
				exit function
			end if
		case "TB_CLIENTES_PROVINCIAS"
			' VERIFICA EXISTENCIA DE CLIENTES VINCULADOS.
			sSql = "Select * from TB_CLIENTES where Id_Provincia = " & Id
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				sMsgJs = "Esse ITEM nao pode ser excluido!\n\nExclua os vinculos (Clientes)!"
				Excluir_item = true
				exit function
			end if
			set Rs = nothing
		case "TB_CLIENTES"
			' VERIFICA EXISTENCIA pedidos VINCULADAS.
			sSql = "Select Id_Cliente from TB_PEDIDOS where Id_Cliente = "& Id
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				sMsgJs = "Esse ITEM nao pode ser excluido!\n\nExclua os vinculos!"
				Excluir_item = true
				exit function
			end if
			set Rs = nothing
		case "TB_PEDIDOS"
			' APAGA REGISTROS DA TABELA AUXILIAR
			sSql = "Delete from TB_PEDIDOS_ITENS WHERE id_pedido = " & Id
			oConn.execute(sSql)
		case "TB_STATUS"
			' VERIFICA EXISTENCIA pedidos VINCULADAS.
			sSql = "Select Id_Status from TB_PEDIDOS where Id_Status = "& Id
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				sMsgJs = "Esse ITEM nao pode ser excluido!\n\nExclua os vinculos!"
				Excluir_item = true
				exit function
			end if
			set Rs = nothing
		case "TB_GALERIA_CATEGORIA"
			' VERIFICA EXISTENCIA DE SUB CATEGORIAS VINCULADAS.
			sSql = "Select * from TB_GALERIA_FOTOS where id_tema = " & Id
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				sMsgJs = "Esse ITEM nao pode ser excluido!\n\nExclua os vinculos!"
				Excluir_item = true
				exit function
			end if
			set Rs = nothing
		case "TB_GALERIA_FOTOS"
			' VERIFICA EXISTENCIA DE SUB CATEGORIAS VINCULADAS.
			sSql = "Select * from TB_GALERIA_FOTOS where id = " & Id
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				call Apaga_Arquivo_Pasta(Server.MapPath("/images/images_galeria/" & trim(rs("Imagem_Path"))))
				Excluir_item = true
			end if
			set Rs = nothing
		case "TB_WALLPAPERS"
			sSql = "Select * from TB_WALLPAPERS where id = " & Id
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				call Apaga_Arquivo_Pasta(Server.MapPath("/images/images_wallpapers/" & trim(rs("Imagem_Path"))))
				Excluir_item = true
			end if
			set Rs = nothing

		case "TB_VALE_PRESENTES"
			sSql = "Select * from TB_VALE_PRESENTES_ATIVOS where Id_ValePresnete = " & Id
			set Rs = oConn.execute(sSql)
			if(not Rs.eof) then 
				sMsgJs = "Esse ITEM n&atilde;o pode ser excluido!\n\nExclua os vinculos!"
				Excluir_item = true
				exit function
			end if
			set Rs = nothing
	end select
	
	sSQl = "Delete from "& sTabela &" where id = " & Id
	oConn.execute(sSQl)

	call Fecha_Conexao()
	sMsgJs = "Operação Concluída!"
	Excluir_item = true
end function


' =======================================================================
sub Apaga_Imagem(Path, NameImagem)
	On Error Resume Next
	
	if(NameImagem = "" ) then exit sub

	dim objFile, sContPath
	dim sPathFile
	
	Set objFile = Server.CreateObject("Scripting.FileSystemObject")
	sPathFile = (Path & "\" & NameImagem)
	
	if (objFile.FileExists(sPathFile) = true) then objFile.DeleteFile (sPathFile)
	Set objFile = nothing
end sub


' ==========================================================
function Monta_Combo_TipoUsuerAdmin(IdSub, sOnchange)
	sSql = "Select * from TB_USUARIO_TIPO where ativo = 1 order by tipo"
	Monta_Combo_TipoUsuerAdmin = Combo(sSql, true, true, "tipo", "", "Selecione um...", "id", "tipo", "id", IdSub, sOnchange)
end function


' ==========================================================
function Monta_Combo_Categorias(IdSub)
	sSql = "Select * from TB_CATEGORIAS where ativo = 1 order by categoria"
	Monta_Combo_Categorias = Combo(sSql, true, true, "categoria", "", "Selecione uma...", "id", "categoria", "id", IdSub, " validar='1' ")
end function

' ==========================================================
function Monta_Combo_Ativo(sSelect)
	dim Sel1, Sel2
	
	Sel1 = ""
	Sel2 = ""
	
	if(trim(sSelect) = "1") then Sel1 = " selected " 
	if(trim(sSelect) = "0") then Sel2 = " selected " 

	Monta_Combo_Ativo = Monta_Combo_Ativo &"<select name=""ativo"">"
	Monta_Combo_Ativo = Monta_Combo_Ativo &"	<option value=""1"" "& Sel1 &">Sim</option>"
	Monta_Combo_Ativo = Monta_Combo_Ativo &"	<option value=""0"" "& Sel2 &">Nao</option>"
	Monta_Combo_Ativo = Monta_Combo_Ativo &"</select>"
end function


' ==========================================================
function Monta_Combo_SN(Name, sSelect, Onchange)
	dim Sel1, Sel2
	
	Sel1 = ""
	Sel2 = ""
	
	if(trim(sSelect) = "1") then Sel1 = " selected " 
	if(trim(sSelect) = "0" OR trim(sSelect) = "" OR  IsNull(trim(sSelect))) then Sel2 = " selected " 

	Monta_Combo_SN = Monta_Combo_SN &"<select name="""& Name &""" "& Onchange &">"
	Monta_Combo_SN = Monta_Combo_SN &"	<option value=""1"" "& Sel1 &">Sim</option>"
	Monta_Combo_SN = Monta_Combo_SN &"	<option value=""0"" "& Sel2 &">Nao</option>"
	Monta_Combo_SN = Monta_Combo_SN &"</select>"
end function



' ==========================================================
function Legenda_Horario(sInt)
	if(sInt = "1") then
		Legenda_Horario = "24 horas"
	elseif(sInt = "2") then
		Legenda_Horario = "Manha"
	elseif(sInt = "3") then
		Legenda_Horario = "Tarde"
	end if
end function


' ==========================================================
function fBoolSNJp(sInt)
	if(sInt = "1") then
		fBoolSNJp = "Sim JP"
	elseif(sInt = "0") then
		fBoolSNJp = "<font color=""#06528E"">N&atilde;o JP</font>"
	end if
end function


' ==========================================================
function LegendaVersoes(sTxt)
	if(sTxt = "j") then
		LegendaVersoes = "Somente Japones"
	elseif(sTxt = "p") then
		LegendaVersoes = "Somente Portugues"
	else
		LegendaVersoes = "Em Todos"
	end if
end function



' ==========================================================
function fBoolSN(sInt)
	if(sInt = "1") then
		fBoolSN = "Sim"
	elseif(sInt = "0") then
		fBoolSN = "<font color=""#06528E"">N&atilde;o</font>"
	end if
end function

' ==========================================================
function Gera_Paginas_vitrine(prInicial, prFinal)
	dim sProximaPg, sAnteriorPg, sVariaveis, sFonte, LinkTemp, sBsucaAvancada
	dim sTextoDescr

	sVariaveis 		= "&letra="& sLetra &"&ordenar=" & sOpcao_Ordenar
	sBsucaAvancada 	= ""
	sTextoDescr 	= "Página Anterior"
	sTextoDescr2 	= "Próxima Página"
	sDir 			= "portugues"

	if (InSTr(lcase(Request.ServerVariables("script_name")), "resultado_busca.asp") > 0 ) then 	
		sVariaveis = sVariaveis & _
				"&enviado=ok" &_
				"&sel_OrderBusca="& sOrdemBusca &_
				"&busca=" & sBusca &_
				"&fabricante=" & sfabricante &_
				"&Id_Fabricante=" & sFabricantes &_
				"&sub2=" & sIdSub2  &_
				"&sub3=" & sIdSub3
		
		if(sIdSub <> "") then 
			sVariaveis = sVariaveis & "&sub="& sIdSub
		else
			sVariaveis = sVariaveis & "&sub"& sCategoria &"="& sSubCategoria
		end if
		
		if(sIdCat <> "") then 
			sVariaveis = sVariaveis & "&cat=" & sIdCat
		else
			sVariaveis = sVariaveis & "&categoria=" & sCategoria
		end if

	end if
	
	if (InSTr(lcase(Request.ServerVariables("script_name")), "lista.asp") > 0 ) then 	
		sVariaveis = sVariaveis & _
				"&enviado=ok" &_
				"&ordenarP="& sOrdenarP &_
				"&busca=" & sBusca &_
				"&fabricante=" & sfabricante &_
				"&Id_Fabricante=" & sFabricantes &_
				"&Minimo=" & sMinimo  &_
				"&Maximo=" & sMaximo  &_
				"&sub2=" & sIdSub2  &_
				"&sub3=" & sIdSub3  &_
				"&sub4=" & sIdSub4  &_
				"&sub5=" & sIdSub5  &_
				"&sub6=" & sIdSub6  &_
				"&sub7=" & sIdSub7
		
		if(sIdSub <> "") then 
			sVariaveis = sVariaveis & "&sub="& sIdSub
		end if
		
		if(sIdCat <> "") then 
			sVariaveis = sVariaveis & "&cat=" & sIdCat
		end if
	end if
	
	if (InSTr(lcase(Request.ServerVariables("script_name")), "detalhes.asp") > 0 ) then 	
		sVariaveis = sVariaveis & _
				"&produto="&  Replace_Caracteres_Especiais(replace(trim(request("produto")),"'","")) &_
				"&onAva=abaop" &_
				"#avaliacliente"
	end if	
	
	if (InSTr(lcase(Request.ServerVariables("script_name")), "secao.asp") > 0 ) then 
		sVariaveis = sVariaveis & _
				"&enviado=ok" &_
				"&ordenarP="& sOrdenarP &_
				"&busca=" & sBusca &_
				"&fabricante=" & sfabricante &_
				"&Id_Fabricante=" & sFabricantes &_
				"&Minimo=" & sMinimo  &_
				"&Maximo=" & sMaximo  &_
				"&sub2=" & sIdSub2  &_
				"&sub3=" & sIdSub3  &_
				"&sub4=" & sIdSub4  &_
				"&sub5=" & sIdSub5  &_
				"&sub6=" & sIdSub6  &_
				"&aba=" & sQualAbaSel 


			if(sIdSub <> "") then 
				sVariaveis = sVariaveis & "&sub="& sIdSub
			end if
			
			if(sIdCat <> "") then 
				sVariaveis = sVariaveis & "&cat=" & sIdCat
			end if				
	end if	
	
	if (InSTr(lcase(Request.ServerVariables("script_name")), "vitrine_lancamento.asp") > 0 ) then 	
		sVariaveis = sVariaveis & _
				"&enviado=ok" &_
				"&ordenarP="& sOrdenarP &_
				"&busca=" & sBusca &_
				"&fabricante=" & sfabricante &_
				"&Id_Fabricante=" & sFabricantes &_
				"&Minimo=" & sMinimo  &_
				"&Maximo=" & sMaximo  &_
				"&sub2=" & sIdSub2  &_
				"&sub3=" & sIdSub3  &_
				"&sub4=" & sIdSub4  &_
				"&sub5=" & sIdSub5  &_
				"&sub6=" & sIdSub6  &_
				"&aba=" & sQualAbaSel 
				
			if(sIdSub <> "") then 
				sVariaveis = sVariaveis & "&sub="& sIdSub
			end if
			
			if(sIdCat <> "") then 
				sVariaveis = sVariaveis & "&cat=" & sIdCat
			end if	
			
			if(sOrigem <> "") then 
				sVariaveis = sVariaveis & "&origem=" & sOrigem
			end if					
	end if	
	
	if (InSTr(lcase(Request.ServerVariables("script_name")), "vitrine_ofertas.asp") > 0 ) then 	
		sVariaveis = sVariaveis & _
				"&enviado=ok" &_
				"&ordenarP="& sOrdenarP &_
				"&busca=" & sBusca &_
				"&fabricante=" & sfabricante &_
				"&Id_Fabricante=" & sFabricantes &_
				"&Minimo=" & sMinimo  &_
				"&Maximo=" & sMaximo  &_
				"&sub2=" & sIdSub2  &_
				"&sub3=" & sIdSub3  &_
				"&sub4=" & sIdSub4  &_
				"&sub5=" & sIdSub5  &_
				"&sub6=" & sIdSub6  &_
				"&aba=" & sQualAbaSel 
			if(sIdSub <> "") then 
				sVariaveis = sVariaveis & "&sub="& sIdSub
			end if
			
			if(sIdCat <> "") then 
				sVariaveis = sVariaveis & "&cat=" & sIdCat
			end if				
	end if	
	
	if (InSTr(lcase(Request.ServerVariables("script_name")), "vitrine_maisvendido.asp") > 0 ) then 	
		sVariaveis = sVariaveis & _
				"&enviado=ok" &_
				"&ordenarP="& sOrdenarP &_
				"&busca=" & sBusca &_
				"&fabricante=" & sfabricante &_
				"&Id_Fabricante=" & sFabricantes &_
				"&Minimo=" & sMinimo  &_
				"&Maximo=" & sMaximo  &_
				"&sub2=" & sIdSub2  &_
				"&sub3=" & sIdSub3  &_
				"&sub4=" & sIdSub4  &_
				"&sub5=" & sIdSub5  &_
				"&sub6=" & sIdSub6  &_
				"&aba=" & sQualAbaSel 
			if(sIdSub <> "") then 
				sVariaveis = sVariaveis & "&sub="& sIdSub
			end if
			
			if(sIdCat <> "") then 
				sVariaveis = sVariaveis & "&cat=" & sIdCat
			end if				
	end if	
	
	if (InSTr(lcase(Request.ServerVariables("script_name")), "vitrine_todos.asp") > 0 ) then 	
		sVariaveis = sVariaveis & _
				"&enviado=ok" &_
				"&ordenarP="& sOrdenarP &_
				"&busca=" & sBusca &_
				"&fabricante=" & sfabricante &_
				"&Id_Fabricante=" & sFabricantes &_
				"&Minimo=" & sMinimo  &_
				"&Maximo=" & sMaximo  &_
				"&sub2=" & sIdSub2  &_
				"&sub3=" & sIdSub3  &_
				"&sub4=" & sIdSub4  &_
				"&sub5=" & sIdSub5  &_
				"&sub6=" & sIdSub6  &_
				"&aba=" & sQualAbaSel 
			if(sIdSub <> "") then 
				sVariaveis = sVariaveis & "&sub="& sIdSub
			end if
			
			if(sIdCat <> "") then 
				sVariaveis = sVariaveis & "&cat=" & sIdCat
			end if				
	end if	
	
	if(lcase(Request.ServerVariables("Script_Name")) = "/speciallist/natal2009/default.asp") then 
		sVariaveis = sVariaveis & _
				"&spec="&  Replace_Caracteres_Especiais(replace(trim(request("spec")),"'",""))
	end if	
	
	
	if(lcase(Request.ServerVariables("Script_Name")) = "/speciallist/educadores/default.asp") then 
			if(sSpec <> "") then 
				sVariaveis = sVariaveis & "&spec="& sSpec
			end if	
			if(sMin <> "") then 
				sVariaveis = sVariaveis & "&min="& sMin
			end if
			if(sMax <> "") then 
				sVariaveis = sVariaveis & "&max="& sMax
			end if
			if(sMaior <> "") then 
				sVariaveis = sVariaveis & "&maior="& sMaior
			end if
	end if	

	if (InSTr(lcase(Request.ServerVariables("script_name")), "default.asp") > 0 ) then 
		sVariaveis = sVariaveis & _
				"&enviado=ok" &_
				"&ordenarP="& sOrdenarP &_
				"&busca=" & sBusca &_
				"&fabricante=" & sfabricante &_
				"&Id_Fabricante=" & sFabricantes &_
				"&Minimo=" & sMinimo  &_
				"&Maximo=" & sMaximo  &_
				"&sub2=" & sIdSub2  &_
				"&sub3=" & sIdSub3  &_
				"&sub4=" & sIdSub4  &_
				"&sub5=" & sIdSub5  &_
				"&sub6=" & sIdSub6  &_
				"&sub7=" & sIdSub7  &_
				"&aba=" & sQualAbaSel 
		
		if(sIdSub <> "") then 
			sVariaveis = sVariaveis & "&sub="& sIdSub
		else
			sVariaveis = sVariaveis & "&sub"& sCategoria &"="& sSubCategoria
		end if
		
		if(sIdCat <> "") then 
			sVariaveis = sVariaveis & "&cat=" & sIdCat
		else
			sVariaveis = sVariaveis & "&categoria=" & sCategoria
		end if
	end if
	
	if (iPageCount > 0) then
	
		sAnteriorPg = (iQualPagina - 1)
		if((iQualPagina - 1) > iPageCount) then sAnteriorPg = 1
		
		if(iQualPagina > sNumeroPaginacao) then 
			Gera_Paginas_vitrine = "<a class=""pag"" href="/"javascript:mostrapaginacaoAnt('"& sIdCat &"','"& sIdSub &"','"& sFabricantes &"','"&sAnteriorPg&"','"&sQualAbaSel&"','"&sOrigem&"')"">"& sTextoDescr &"</a>" & Gera_Paginas_vitrine
		end if
		
		iQualPaginatemp 					= iQualPagina
		sNumeroPaginacaoFinal 				= sNumeroPaginacao
		
		if(iQualPagina > sNumeroPaginacao) then 
			iQualPaginatempConta			= LEFT(iQualPagina/5,1)
			
			if((iQualPaginatempConta = 2) and (iQualPagina mod 5 = 0)) then 
				iQualPaginatempConta 		= 1
			end if
			iQualPaginatemp 				= (iQualPaginatempConta * 5)

			if(iQualPaginatemp < iQualPaginatempAux) 	then
				iQualPaginatemp = iQualPaginatempAux
			end if
			
			if( ((iQualPaginatemp + 1) = iQualPagina)) 	then
				iQualPaginatemp = iQualPagina
			end if
			
			sNumeroPaginacaoFinal 			= (iQualPaginatemp + 4)
			
			if((iQualPaginatemp = sNumeroPaginacao) ) then 
				iQualPaginatemp 			= (iQualPaginatemp + 1)
				sNumeroPaginacaoFinal 		= (iQualPaginatemp + 4)
			end if

			if(iQualPagina = iPageCount) then 
				iQualPaginatemp 			= iPageCount
				sNumeroPaginacaoFinal 		= iPageCount
			end if
		else
			iQualPaginatemp 				= 1
		end if
		
		if(sNumeroPaginacaoFinal > iPageCount) then 
			sNumeroPaginacaoFinal 			= iPageCount
		end if
		
		'response.write "iQualPagina : 			" & iQualPagina & "<br>"
		'response.write "iPageCount : 			" & iPageCount & "<br>"
		'response.write "sNumeroPaginacao : 		" & sNumeroPaginacao & "<br>"
		'response.write "LinkTemp : 				" & LinkTemp & "<br>"
		'response.write "iQualPaginatemp : 		" & iQualPaginatemp & "<br>"
		'response.write "sNumeroPaginacaoFinal:	" & sNumeroPaginacaoFinal & "<br>"
		'response.end
		
		for LinkTemp = iQualPaginatemp to sNumeroPaginacaoFinal
			if (LinkTemp  = iQualPagina) then
				Gera_Paginas_vitrine = Gera_Paginas_vitrine & "<a href="/"javascript:mostrapaginacaoAnt('"& sIdCat &"','"& sIdSub &"','"& sFabricantes &"','"&LinkTemp&"','"&sQualAbaSel&"','"&sOrigem&"')""> <span class=""paginaOn"">" & LinkTemp & "</span> </A>" & vbcrlf
			else
				Gera_Paginas_vitrine = Gera_Paginas_vitrine & "<a href="/"javascript:mostrapaginacaoAnt('"& sIdCat &"','"& sIdSub &"','"& sFabricantes &"','"&LinkTemp&"','"&sQualAbaSel&"','"&sOrigem&"')"">" & LinkTemp & "</A>" & vbcrlf	
			end if

			iQualPaginatempAux 	= iQualPaginatemp
		next

		sProximaPg 	= (iQualPagina + 1)
		if((iQualPagina + 1) > iPageCount) then sProximaPg = 1
	
		'if(iPageCount > 15) then Gera_Paginas_vitrine = Gera_Paginas_vitrine & "</select>"

		
		if (iPageCount > 1 and (iQualPagina < iPageCount)) then 
			Gera_Paginas_vitrine = Gera_Paginas_vitrine & "<a class=""pag"" href="/"javascript:mostrapaginacaoAnt('"& sIdCat &"','"& sIdSub &"','"& sFabricantes &"','"&sProximaPg&"','"&sQualAbaSel&"','"&sOrigem&"')"" >"& sTextoDescr2 &"</a>"
		end if
	end if	

end function
' ==========================================================
function Gera_Paginas()
	dim sProximaPg, sAnteriorPg, sVariaveis, sFonte, LinkTemp, sBsucaAvancada
	dim sTextoDescr

	sVariaveis 		= "&letra="& sLetra &"&ordenar=" & sOpcao_Ordenar
	sBsucaAvancada 	= ""
	sTextoDescr 	= "<< anterior"
	sTextoDescr2 	= "proxima >>"
    sTextoDescr3 	= "Última"
	sDir 			= "portugues"
	
	if(lcase(Request.ServerVariables("Script_Name")) = "/busca.asp") then 
		sVariaveis = sVariaveis & _
				"&enviado=ok" &_
				"&ordenarP="& sOrdenarP &_
				"&sel_OrderBusca="& sOrdemBusca &_
				"&sel_busca="& sSelectBusca &_
				"&busca=" & sBusca &_
				"&fabricante=" & sfabricante &_
				"&editora=" & sEditora &_
				"&Id_Fabricante=" & sFabricantes &_
				"&Minimo=" & sMinimo  &_
				"&Maximo=" & sMaximo  &_
				"&sub2=" & sIdSub2  &_
				"&sub3=" & sIdSub3  &_
				"&sub4=" & sIdSub4  &_
				"&sub5=" & sIdSub5  &_
				"&sub6=" & sIdSub6  &_
				"&sub7=" & sIdSub7
		
		if(sIdSub <> "") then 
			sVariaveis = sVariaveis & "&sub="& sIdSub
		else
			sVariaveis = sVariaveis & "&sub"& sCategoria &"="& sSubCategoria
		end if
		
		if(sIdCat <> "") then 
			sVariaveis = sVariaveis & "&cat=" & sIdCat
		else
			sVariaveis = sVariaveis & "&categoria=" & sCategoria
		end if
	end if
	
   
	Gera_Paginas = Gera_Paginas & "<ul>"

	if (iPageCount > 0) then
		sAnteriorPg = (iQualPagina - 1)
		if((iQualPagina - 1) > iPageCount) then sAnteriorPg = 1
		
        if(iQualPagina > 1)then
		    if(iQualPagina < sNumeroPaginacao) then 
			    Gera_Paginas = Gera_Paginas &  "<li><a href="/"" & Request.ServerVariables("Script_Name") & "?Pagina=" & sAnteriorPg & sVariaveis & """>"& sTextoDescr &"</a></li>" & Gera_Paginas
		    end if
		end if 
		iQualPaginatemp 					= iQualPagina
		sNumeroPaginacaoFinal 				= sNumeroPaginacao
		
		if(iQualPagina > sNumeroPaginacao) then 
			iQualPaginatempConta			= LEFT(iQualPagina/5,1)
			
			if((iQualPaginatempConta = 2) and (iQualPagina mod 5 = 0)) then 
				iQualPaginatempConta 		= 1
			end if
			iQualPaginatemp 				= (iQualPaginatempConta * 5)

			if(iQualPaginatemp < iQualPaginatempAux) 	then
				iQualPaginatemp = iQualPaginatempAux
			end if
			
			if( ((iQualPaginatemp + 1) = iQualPagina)) 	then
				iQualPaginatemp = iQualPagina
			end if
			
			sNumeroPaginacaoFinal 			= (iQualPaginatemp + 4)
			
			if((iQualPaginatemp = sNumeroPaginacao) ) then 
				iQualPaginatemp 			= (iQualPaginatemp + 1)
				sNumeroPaginacaoFinal 		= (iQualPaginatemp + 4)
			end if

			if(iQualPagina = iPageCount) then 
				iQualPaginatemp 			= iPageCount
				sNumeroPaginacaoFinal 		= iPageCount
			end if
		else
			iQualPaginatemp 				= 1
		end if
		
		if(sNumeroPaginacaoFinal > iPageCount) then 
			sNumeroPaginacaoFinal 			= iPageCount
		end if
				
		for LinkTemp = iQualPaginatemp to sNumeroPaginacaoFinal
			if (LinkTemp  = iQualPagina) then
				Gera_Paginas = Gera_Paginas & "<li>página " & LinkTemp & "/ " & iPageCount & "</a></li>" & vbcrlf
			end if 
			
		next

		sProximaPg 	= (iQualPagina + 1)
		if((iQualPagina + 1) > iPageCount) then sProximaPg = 1
	
		if (iPageCount > 1 and (iQualPagina < iPageCount)) then 
			Gera_Paginas = Gera_Paginas & "<li><a href="/"" & Request.ServerVariables("Script_Name") & "?Pagina=" & sProximaPg & sVariaveis & """ >"& sTextoDescr2 &"</a></li>"
		end if
	end if	

    Gera_Paginas = Gera_Paginas & "</ul>"
    

    end function
    '========================================================================
function Monta_menu_Colecao()
	dim sConteudo, Rs, sSQl

	call Abre_Conexao()

	sSQl = "select " &_
	            "F.id, " &_
                "F.fabricante " &_
            "from  " &_
	            "TB_FABRICANTE F (nolock) " &_
            "where  " &_
	            "F.CdCliente = 97 " &_
	            "and F.Id in (select P.id_fabricante from TB_PRODUTOS P (nolock) where P.Id_Fabricante = F.Id and P.Ativo = 1) " &_
            "ORDER BY "&_
            "F.Fabricante"
            
	set Rs = oConn.execute(sSql)
	
	if(Not Rs.Eof) then
		Do While (Not Rs.Eof) 
            sIdFab     = trim(rs("id"))

			sConteudo = sConteudo & "	<li><a href="/"/colecao/"&TrataSEO(Lcase(rs("fabricante")))& "-" & sIdFab &""">"& Recupera_Campos_Db("TB_FABRICANTE", sIdFab, "id", "fabricante") &"</a></li>" & vbcrlf

		Rs.movenext
		loop
    end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Monta_menu_Colecao = sConteudo
end function
'========================================================================
function Monta_menu(prCategoria)
	dim sConteudo, Rs, sSQl

	call Abre_Conexao()

	sSQl = " SELECT " & _
	            " P.id_subcategoria," & _
                " S.subcategoria" & _
            " FROM " & _
	            " TB_PRODUTOS P (NOLOCK) " & _
                " INNER JOIN TB_SUBCATEGORIA S(NOLOCK) on S.id = p.id_subcategoria " & _
            " WHERE " & _
	            "P.CdCliente = 97 " &_
                "and P.Ativo = 1 " &_
                "and P.Id_Categoria not in (196078) " &_
	            "and P.Id_SubCategoria not in (196078) " &_
                "and P.Id_Categoria = "& prCategoria &" " &_
	        "GROUP BY " &_
	            "p.Id_SubCategoria," & _
                "S.SUBCATEGORIA"
            
	set Rs = oConn.execute(sSql)
	
	if(Not Rs.Eof) then
		Do While (Not Rs.Eof) 
            sIdSubCat     = trim(rs("id_subcategoria"))

			sConteudo = sConteudo & "	<li><a href="/"/categoria/"& TrataSEO(lcase(rs("subcategoria"))) &"-"& sIdSubCat &""">"& Recupera_Campos_Db("TB_SUBCATEGORIA", sIdSubCat, "id", "SUBCATEGORIA") &"</a></li>" & vbcrlf

		Rs.movenext
		loop
    end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Monta_menu = sConteudo
end function
'========================================================================
function busca_tipos(prtipo,prid)
	dim sConteudo, Rs, sSQl, sCategoriaNew

	call Abre_Conexao()

        if(prtipo = "1")then
            sValor = " and C.id = "&prid&" "
        else
            sValor = " and P.id_fabricante <> '' "
        end if

        sSQl = " Select " &_
	                "C.id as id_categoria,  " &_
	                "C.Categoria, " &_
                    "S.id as id_subcategoria,  " &_
	                "S.SubCategoria, " &_
	                "COUNT(P.id_SubCategoria) as total " &_
                 "from  " &_
	                "TB_PRODUTOS P (nolock)   " &_
	                "inner join TB_SUBCATEGORIA S (nolock) on S.id = P.Id_SubCategoria " &_
	                "inner join TB_CATEGORIAS C (nolock) on C.id = P.Id_Categoria " &_
                 "where  " &_
	                "P .Ativo = 1 " &_
	                "and P.CdCliente = 97 " &_
	                "and P.Id_Categoria not in (196078) " &_
                    ""&sValor&" " &_
                "group by " &_
	                "S.SubCategoria, " &_
	                "C.Categoria, " &_
	                "C.id, " &_
                    "S.id "

           

	set Rs = oConn.execute(sSql)

            if(not Rs.eof) then
				
				do while not Rs.eof
					
                    sConteudo = sConteudo & "<li><a href="/"/Busca.asp?sub="& rs("id_subcategoria") &"""> "& rs("subcategoria")&" ("&rs("total")&")</a></li>" & vbcrlf
          
				Rs.movenext
				loop
            end if



	set Rs = nothing
	
	call Fecha_Conexao()
    busca_tipos = sConteudo
end function
'========================================================================
function busca_colecao()
dim sConteudo, Rs, sSQl

call Abre_Conexao()

        sSQL = "Select " & _
			        "F.id as id_Fabricante,  " & _
			        "F.Fabricante, " & _
			        "COUNT(P.id_fabricante) as total " & _
				        "from " & _
					        "TB_PRODUTOS P (nolock) " & _
					        "inner join TB_FABRICANTE F (nolock) on F.Id = P.Id_Fabricante " & _
				        "where " & _
					        "P .Ativo = 1 "  & _
					        "and P.CdCliente = 97 " & _
					        "and P.Id_Categoria not in (196078) " & _                   
				        "group by " & _
					        "F.Fabricante, " & _
					        "F.id "


    set Rs = oConn.execute(sSql)

        if(not Rs.eof) then
				
			do while not Rs.eof
					
                sConteudo = sConteudo & "<li><a href="/"/Busca.asp?fabricante="& rs("id_Fabricante") &"""> "& rs("Fabricante")&" ("&rs("total")&")</a></li>" & vbcrlf
          
			Rs.movenext
			loop
        end if

    set Rs = nothing
	
	call Fecha_Conexao()
    busca_colecao = sConteudo
end function
'========================================================================
function busca_colecao_rodape()
dim sConteudo, Rs, sSQl


call Abre_Conexao()

sSQL = "Select top 1" & _
			        "F.id as id_Fabricante,  " & _
			        "F.Fabricante " & _
				        "from " & _
					        "TB_PRODUTOS P (nolock) " & _
					        "inner join TB_FABRICANTE F (nolock) on F.Id = P.Id_Fabricante " & _
				        "where " & _
					        "P .Ativo = 1 "  & _
					        "and P.CdCliente = 97 " & _
					        "and P.Id_Categoria not in (196078) " & _                   
				        "group by " & _
					        "F.Fabricante, " & _
					        "F.id "

set Rs = oConn.execute(sSql)

        if(not Rs.eof) then
				
			do while not Rs.eof
					
                sConteudo = sConteudo & "<a href="/"/colecao/"& Lcase(rs("Fabricante")) & "-"& rs("id_Fabricante") &"""> Cole&ccedil;&otilde;es</a>" & vbcrlf
          
			Rs.movenext
			loop
        end if

    set Rs = nothing

call Abre_Conexao()

    call Fecha_Conexao()
    busca_colecao_rodape = sConteudo
end function
'========================================================================
function Tipo_busca(prvalor, prsub, prcat, prfab)
	dim sConteudo, Rs, sSQl

	call Abre_Conexao()

        sSQl = " select " & _
	                "COUNT(Preco_Consumidor) as total " & _
                "from " & _
	                "TB_PRODUTOS " & _
                "where " & _
	                "CdCliente = 97 " & _
	                "and Ativo = 1 " & _
	                "and Preco_Consumidor <= "& prvalor &" " & _
	                "and Id_Categoria not in(196078) " & _
	                "and Id_SubCategoria not in(7402) " 

                    if(prsub <> "" )then sSQl = sSQl & "and Id_SubCategoria  = "& prsub &" "
                    if(prcat <> "" )then sSQl = sSQl & "and Id_Categoria  = "& prcat &" "
                    if(prfab <> "" )then sSQl = sSQl & "and Id_fabricante  = "& prfab &" "


                    'response.write sSQl
                    'response.end
	set Rs = oConn.execute(sSql)
	
	if(Not Rs.Eof) then
        do while (not rs.eof)

            sConteudo = "	<a href="/"/Busca.asp?valorB="& prValor &"&sub="& prsub &"&cat="& prcat &"&fabricante="& prfab &"   "">Até "& prValor &" ("& Rs("total") &")</a>" & vbcrlf

		Rs.movenext
		loop
    end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Tipo_busca = sConteudo
end function
'========================================================================
function Mostra_Todas_Institucional_Categorias_Admin()
	dim sId, sCategoria, sCategoriaJp, sCategoriaDesc, sAtivo
	dim sConteudo, Rs, sSQl

	call Abre_Conexao()

	sSQl = "Select * from TB_INSTITUCIONAL_CATEGORIAS"
	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by categoria, datacad "
	end if
	
	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(Not Rs.Eof) then
		Rs.PageSize = sNumerosPaginasPadrao
		Rs.CacheSize = Rs.PageSize
		iPageCount = Rs.PageCount
		iRecordCount = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina

		iExibe = 1
		Do While (Not Rs.Eof) and (iExibe <= sNumerosPaginasPadrao)
			iExibe = iExibe + 1
			sId = rs("id")
			sCategoria = trim(rs("categoria"))
			sCategoriaJp = trim(rs("categoria_jp"))
			sAtivo = rs("ativo")
			
			sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><div align=""left""><a href="/"alteracategoria.asp?id="& sId &""">"& sId &"</div></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"alteracategoria.asp?id="& sId &""">"& sCategoria &"</a></p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""left""><a href="/"alteracategoria.asp?id="& sId &""">"& sCategoriaJp &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><p align=""center""><a href="/"alteracategoria.asp?id="& sId &""">"& fBoolSN(sAtivo) &"</p></td>" & vbcrlf
			sConteudo = sConteudo & "	<td scope=""col""><a href="/"javascript:excluir('"& sId &"')""><img src=""././img/excluir.gif"" border=""0"" align=""absmiddle"" alt=""""></a></td>" & vbcrlf
			sConteudo = sConteudo & "</tr>" & vbcrlf
		Rs.movenext
		loop

		sConteudo = sConteudo & "</table>"
		
		' PAGINACAO ******************
		If (Not Rs.Eof ) or (iQualPagina > 1) Then
			sConteudo = sConteudo & ("<center><br><table cellpadding=""1"" cellspacing=""0"" border=""0"">")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703""><b>"& iRecordCount &"</b> registros(s) encontrado(s)</td></tr>")
			sConteudo = sConteudo & ("<tr><td align=""left""><font size=""2"" face=""verdana"" color=""#5A0703"">")
			sConteudo = sConteudo & (Gera_Paginas())
			sConteudo = sConteudo & ("</tr></table><br><br></center>")
		End If
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10"">N&atilde;o h&aacute; registros...</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Mostra_Todas_Institucional_Categorias_Admin = sConteudo
end function
'========================================================================
function Verifica_Permissao_Usuario(nModulo)
	dim sMontModulo, sPermissoesUser, sPos
	dim sAtivoMod

	Verifica_Permissao_Usuario = false

	sMontModulo = (nModulo & ",")
	sAtivoMod = Recupera_Campos_Db("TB_MENU_ADMIN", nModulo, "id", "ativo")
	if(sAtivoMod <> "") then
		if(sAtivoMod = "0") then exit function
	end if
	sPermissoesUser = (Session("privilegio") & ",")
	sPos = InStr(1, sPermissoesUser, sMontModulo)
	
	if(sPos > 0) then Verifica_Permissao_Usuario = true
end function
'========================================================================
sub Verificar_Login_Ativo_HotsitePadrao(prPath)
	if(Session("admin_validar") <> "1") then response.redirect(prPath)
end sub


'========================================================================
sub Verificar_Login_Ativo_HotSite()
	if(Session("admin_validar") <> "1") then response.redirect("/hotsite")
end sub
'========================================================================
sub Verificar_Login_Ativo_Clientes(prDiretorio)
	if(Session("admin_validar") <> "1") then response.redirect("/" & prDiretorio)
end sub


'========================================================================
sub Verificar_Login_Ativo()
	if(Session("validar") <> "1") then response.redirect("/admin")
end sub


'========================================================================
function fBollSubCategoria(Categoria)
	Dim sSQl, oRs
	
	fBollSubCategoria = false

	sSQL = "Select Id_Categoria from TB_SUBCATEGORIA  where ativo = 1 and Id_Categoria = " & Categoria
	set oRs = oConn.execute(sSQL)
	if(not oRs.eof) then fBollSubCategoria = true
end function


' ========================================================================
function Combo(sSql, BoolMontaCombo, BoolMontaPrimeiroCombo, sNameCombo, sValorPrimeiroOption, sDescricaoPrimeiroOption, sCampoValor, sCampoDescricao, sCampoSelect1, sCampoSelect2, sValorOnchange)
	call Abre_Conexao()
	
	dim Rs, sSelect, sAxCampoDescricao, ArrCampoDescricao
	
	if(BoolMontaCombo) then Combo = "<select name="""& sNameCombo &""" "& sValorOnchange &">"
	if(BoolMontaPrimeiroCombo) then Combo = Combo & "<option value="""& sValorPrimeiroOption &""">"& sDescricaoPrimeiroOption &"</option>"

	set Rs = oConn.execute(sSql)
	if(not rs.eof) then
		Do While not Rs.EOF
			if((sCampoSelect1 <> "") and (sCampoSelect2 <> "")) then
				'if(trim(rs(sCampoSelect1)) = trim(sCampoSelect2)) then sSelect = " selected"
			end if
			if(InStr(sCampoDescricao, "#") = 0) then
				sAxCampoDescricao = trim(rs(sCampoDescricao))
			else
				if(InSTr(sCampoDescricao, "ENCODE,") > 0) then 
					sCampoDescricao = replace(sCampoDescricao,"ENCODE,","")
					ArrCampoDescricao = split(sCampoDescricao,"#")
					sAxCampoDescricao = (Server.aspEncode(trim(rs(ArrCampoDescricao(0)))) & " - " & trim(rs(ArrCampoDescricao(1))))
				else
					ArrCampoDescricao = split(sCampoDescricao,"#")
					sAxCampoDescricao = (trim(rs(ArrCampoDescricao(0))) & "-" & trim(rs(ArrCampoDescricao(1))))
				end if
				
			end if
			Combo = Combo & "<option value=""" & Rs(sCampoValor) & """ "& sSelect &">" & sAxCampoDescricao & "</option>" & vbcrlf 
			Rs.MoveNext
			sSelect = ""
		Loop
	end if
	set Rs = nothing

	if(BoolMontaCombo) then Combo = Combo & "</select>"
	call Fecha_Conexao()
end function

' =========================================================================
function Corta(sResenha, nCaractes)
	dim spResenha, Msg, k
	
	if(Isnull(sResenha)) then exit function
	
    spResenha = Split(sResenha, " ", -1, 1)
    Msg = ""

	if(ubound(spResenha) > nCaractes) then
	    For k=0 to nCaractes
	       if k = nCaractes then
	          Msg  = Msg & spResenha(k) &"..."
	       else
	          Msg = Msg & spResenha(k) &" "
	       end if
	    next
	else
		Msg = sResenha
	end if

	Corta = Msg
end function


' =========================================================================
function CortaLenBr(sResenha, nCaractes)
	dim spResenha, Msg, k
	
	if(Isnull(sResenha)) then exit function
	
    spResenha = len(trim(sResenha))
    Msg = ""
	lCont = 1
	
	if(spResenha > nCaractes) then
	    For k = 1 to spResenha
	       if (k = nCaractes or lCont = nCaractes) then
	          Msg  = Msg & mid(sResenha, k, 1) &"<br>"
			  lCont = 1
	       else
	          Msg = Msg & mid(sResenha,k,1)
			  lCont = lCont + 1
	       end if
	    next
	else
		Msg = sResenha
	end if

	CortaLenBr = Msg
end function


' =========================================================================
function CortaLen(sResenha, nCaractes)
	dim spResenha, Msg, k
	
	if(Isnull(sResenha)) then exit function
	
    spResenha = len(trim(sResenha))
    Msg = ""

	if(spResenha > nCaractes) then
	    For k=1 to nCaractes
	       if k = nCaractes then
	          Msg  = Msg & mid(sResenha,k,1) &"..."
	       else
	          Msg = Msg & mid(sResenha,k,1)
	       end if
	    next
	else
		Msg = sResenha
	end if

	CortaLen = Msg
end function

' =========================================================================
function Monta_Combo_Data(sTipo, sIni, sFim, Name, sSelect, Onchange)
	dim lCont, sConteudo, sSelected
	dim sSelecione

	if(lcase(sTipo) = "ano") then sIni = (sIni - 9)
	
	sConteudo = sConteudo &"<select name="""& Name &""" "& Onchange &">" & vbcrlf
	sConteudo = sConteudo &"<option value="""">"& sTipo &"</option>" & vbcrlf
	
	for lCont = sIni to sFim
		sSelected = ""
		if(sSelect <> "" and IsNumeric(sSelect)) then 
			if(cint(lCont) = cint(sSelect)) then sSelected = " selected "
		end if

		if(lcase(sTipo) = "ano") then 
			sConteudo = sConteudo &"<option value="""& lCont &""" "& sSelected &">"& lCont &"</option>" & vbcrlf
		else
			sConteudo = sConteudo &"<option value="""& right(("0000" & lCont),2) &""" "& sSelected &">"& lCont &"</option>" & vbcrlf
		end if
	next
	
	sConteudo = sConteudo &"</select>" & vbcrlf
	Monta_Combo_Data = sConteudo
end function


'========================================================================
function TotalClientesAvisadosEsgotados(Produto)
	dim sSql, Rs

	TotalClientesAvisadosEsgotados = 0
	
	call Abre_Conexao()
	
	sSQl = "Select count(*) as total from TB_LISTA_ESGOTADOS where Notificado = 1 and IdProduto = " & Produto
	set Rs = oConn.execute(sSql)
	if(not rs.eof) then TotalClientesAvisadosEsgotados = rs("total")
	set Rs = nothing
end function


'========================================================================
function TotalListaEsgotados(Cliente)
	dim sSql, Rs

	TotalListaEsgotados = 0
	
	call Abre_Conexao()
	
	sSQl = "Select count(*) as total from TB_LISTA_ESGOTADOS where IdCliente = " & Cliente
	set Rs = oConn.execute(sSql)
	if(not rs.eof) then TotalListaEsgotados = rs("total")
	set Rs = nothing

	call Fecha_Conexao()
end function



'========================================================================
function TotalListaDesejos(Cliente)
	dim sSql, Rs

	TotalListaDesejos = 0
	
	call Abre_Conexao()
	
	sSQl = "Select count(*) as total from TB_LISTA_DESEJOS where Id_Cliente = " & Cliente
	set Rs = oConn.execute(sSql)
	if(not rs.eof) then TotalListaDesejos = rs("total")
	set Rs = nothing

	call Fecha_Conexao()
end function


'========================================================================
function TotalInteracoes(Cliente)
	dim sSql, Rs

	TotalInteracoes = 0
	
	call Abre_Conexao()
	
	sSQl = "Select count(*) as total from TB_INTERACOES where Id_Usuario = " & Cliente
	set Rs = oConn.execute(sSql)
	if(not rs.eof) then TotalInteracoes = rs("total")
	set Rs = nothing

	call Fecha_Conexao()
end function

'========================================================================
function TotalValePresente(Cliente)
	dim sSql, Rs

	TotalPedidos = 0
	
	call Abre_Conexao()
	
	sSQl = "Select count(*) as total from TB_VALE_PRESENTES_ATIVOS where Id_Cliente = " & Cliente
	set Rs = oConn.execute(sSql)
	if(not rs.eof) then TotalValePresente = rs("total")
	set Rs = nothing

	call Fecha_Conexao()
end function


' =======================================================================
function Calcula_Refrigerado(TotalPeso)
	dim sSQl, Rs
	
	if(trim(TotalPeso) = "" or trim(TotalPeso) = "0") then
		Calcula_Refrigerado = 0
		exit function
	end if
	
	call Abre_Conexao()
	sSQl = "Select * from TB_CONGELADOS where (ativo = 1) order by Valor_Kg"
	set Rs = oConn.execute(sSQl)

	if(not rs.eof) then
		do while not rs.eof
			sAte = cdbl(rs("Valor_Kg"))
			
			if(cdbl(TotalPeso) <= cdbl(sAte)) then
				Calcula_Refrigerado = RetornaCasasDecimais(cdbl(rs("Valor_Frete")),0)
				exit function
			end if
		rs.movenext
		loop
	end if
	
	set Rs = nothing
	call fecha_Conexao()
end function


'*************************
function encheL(v1, v2, v3)
'*************************
	' v1 = valor
	' v2 = tamanho
	' v3 = caracter a preencher
	xx=""
	for x=1 to v2
	   xx = xx & v3
	next
	encheL = right(xx & trim(v1), v2)
end function

'*************************
function encheR(v1, v2, v3)
'*************************
	' preenche à direita
	' v1 = valor
	' v2 = tamanho
	' v3 = caracter a preencher
	xx=""
	for x=1 to v2
	   xx = xx & v3
	next
	encheR = left(trim(v1) & xx, v2)
end function


'*************************
function Formata_data(v1)
'*************************
	dim DD, MM, AAAA
	
	DD 		= right("00" & day(v1),2)
	MM 		= right("00" & month(v1),2)
	AAAA 	= right("0000" & year(v1),4)

	Formata_data = (DD & "/" & MM & "/" & AAAA)
end function

'*************************
function Formata_Valor(v1)
'*************************
	Formata_Valor = replace(replace(v1, ".", ""), ",", ".")
end function

'=======================================================================================
function Monta_Interesse()
	dim rs, ssql,sCategoria,sCategoriaSEO,sIdCategoria
	
	sCampo = "SubCategoria"
	
	'ssql = "SELECT ID, SUBCATEGORIA FROM TB_SUBCATEGORIA WHERE CDCLIENTE = 32 AND ATIVO = 1 order by SUBCATEGORIA"
	sSql = "Select top (5)"&_
			   		"S.Id, " &_
			   		"S.Id_Categoria, " &_
			   		"S.SubCategoria " &_
				"from " &_
			   		"TB_SUBCATEGORIA S (nolock)  " &_
				"where "&_
					"S.CdCliente = "& sCdCliente &" " &_  
					" and S.SubCategoria <> '' " &_ 
					" and (S.BoolDestaque =  1) " &_
					"AND S.ID IN (SELECT P.ID_SUBCATEGORIA FROM TB_PRODUTOS P (NOLOCK) WHERE P.ID_SUBCATEGORIA = S.ID  AND P.ATIVO = 1) "&_ 
				"order by S.SubCategoria asc" 
	call abre_conexao()
	set Rs = oConn.execute(sSQl)
	Monta_Interesse	="<ul>"
		if (not rs.eof) then
			do while not rs.eof
				sCategoria 		= Rs("SubCategoria")
				sCategoriaSEO	= TrataSEO(rs(sCampo))
				sIdCategoria 	= rs("id")
 				
				Monta_Interesse		= Monta_Interesse & "<li><a target=""_parent"" href="/"/interesse/"&  sCategoriaSEO &""">"& sCategoria &"</a></li>"
				
				rs.movenext
			loop
		end if
		Monta_Interesse	=	Monta_Interesse	&"<li><a href="/"/interesses.asp"">Veja mais..</a></li>"
	Monta_Interesse	=	Monta_Interesse	&"</ul>"
	call fecha_conexao()
end Function

'=======================================================================================
function Monta_Interesse_full()
	dim rs, ssql,sCategoria,sCategoriaSEO,sIdCategoria, sTotalInteresse
	
	sCampo = "SubCategoria"
	
	call abre_conexao()
	
	'ssql = "SELECT ID, SUBCATEGORIA FROM TB_SUBCATEGORIA WHERE CDCLIENTE = 32 AND ATIVO = 1 order by SUBCATEGORIA"
	sSql = "Select distinct "&_
			   		" S.SubCategoria " &_
				"from " &_
			   		"TB_SUBCATEGORIA S (nolock)  " &_
				"where "&_
					" S.CdCliente = "& sCdcliente &" 		" & _
					" AND S.ATIVO = 1 						" & _
					" AND isnull(S.SubCategoria,'') <> ''" 	&_ 
					" AND S.ID IN (SELECT P.ID_SUBCATEGORIA FROM TB_PRODUTOS P (NOLOCK) WHERE P.ID_SUBCATEGORIA = S.ID  AND P.ATIVO = 1) " &_ 
					" AND S.ID_CATEGORIA IN (SELECT C.ID FROM TB_CATEGORIAS C (NOLOCK) WHERE C.ID = S.ID_CATEGORIA  AND C.ATIVO = 1 AND C.ID_TIPO = 'S') " &_ 
				"order by S.SubCategoria " 

	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0

	if (not rs.eof) then
		Rs.PageSize 	= 10000
		Rs.CacheSize 	= Rs.PageSize
		iPageCount 		= Rs.PageCount
		Rs.AbsolutePage = 1
		sTotalInteresse = Rs.RecordCount
		iExibe 			= 0
		
		if ((sTotalInteresse mod 4) = 0) then 
			sTotalInteresse =  cInt(sTotalInteresse/4) 
		else
			sTotalInteresse 	= cInt(sTotalInteresse / 4) +1
		end if
		
		Monta_Interesse_full = Monta_Interesse_full & "<div class=""noindex"">" & vbcrlf
		Monta_Interesse_full = Monta_Interesse_full & "<ul style=""padding:10px 0px 10px 10px;float:left;width:950px;"">" & vbcrlf
		
		do while not rs.eof
			sCategoria 		= Rs("SubCategoria")
			sCategoriaSEO	= TrataInteresse(rs(sCampo))
			'sIdCategoria 	= rs("id")
			if (iExibe = 0) then Monta_Interesse_full = Monta_Interesse_full & "<div class=""coluna_marcas"">" & vbcrlf
		
			Monta_Interesse_full		= Monta_Interesse_full & "<li><a target=""_parent"" href="/"/interesse/"&  sCategoriaSEO &""">"& sCategoria &"</a></li>"
		
			iExibe = iExibe + 1 
			if (iExibe = sTotalInteresse) then 
				Monta_Interesse_full = Monta_Interesse_full & "</div>" & vbcrlf
				iExibe = 0
			end if		
		
		rs.movenext
		loop
		Monta_Interesse_full = Monta_Interesse_full & "</ul>" & vbcrlf
		Monta_Interesse_full = Monta_Interesse_full & "</div>" & vbcrlf
	end if
	
	call fecha_conexao()
end Function

'=====================================================================================

function Mostra_Desconto_Produto(Id_Produto)
	Dim sSql, rs
	call abre_conexao()
	sSql	=	"select preco_consumidor, descontovalor from tb_produtos where id = " & Id_Produto & " and promocao = 1 and (getdate() between DataIniPromo and DataFimPromo) "
	set Rs = oConn.execute(sSql)
	if (not rs.eof) then 
	
	cDesconto = rs("preco_consumidor") - rs("descontovalor")
	
		if (rs("descontovalor")>0) then Mostra_Desconto_Produto	=	"<div class=""selo_desconto"" style=""padding-top:25px;""><span style=""font-weight:bold;font-size:18px;width:120px;text-align:center"">" & (100-(RetornaCasasDecimais(cDesconto / rs("preco_consumidor"),2)*100))& " %" & "</span><br> de desconto</div>"
			if(Session("CupomGet") <> "") then 
				sPrecoPor = RetornaCasasDecimais((  _
							cdbl(Session(nProduto)) -  _
							( ( cdbl(Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", nProduto, "id", "id_categoria")) +  _
								Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", nProduto, "id", "id_subcategoria"))) / 100) * cdbl(Session(nProduto)))),2)
				sPrecoPor = RetornaCasasDecimais((sPrecoPor - Session("d" & nProduto) - Session("dc" & nProduto)),2)
				if(ucase(Recupera_Campos_Db_oConn("TB_CUPONS", Session("CupomGet"), "Cupom", "TipoDescontoValor")) = "P") then
					sPrecoPor = RetornaCasasDecimais((cdbl(sPrecoPor) - ((cdbl(Session("Desconto") + Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", nProduto, "id", "id_categoria")) + Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", nProduto, "id", "id_subcategoria"))) / 100) * cdbl(sPrecoPor))),2)
				else
					sPrecoPor = RetornaCasasDecimais((cdbl(sPrecoPor)- ((cdbl(Session("Desconto") + Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", nProduto, "id", "id_categoria")) + Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", nProduto, "id", "id_subcategoria")))))),2)
				end if
				if(sPrecoPor < 0) then sPrecoPor = RetornaCasasDecimais(0,2)
				Mostra_Desconto_Produto	=	"<div class=""selo_desconto"" style=""padding-top:25px;""><span style=""font-weight:bold;font-size:18px;width:120px;text-align:center"">" & int(((rs("preco_consumidor")-sPrecoPor) / rs("preco_consumidor")) * 100) & " %" & "</span><br> de desconto</div>"
			End If
	end if
	call fecha_conexao()
end function



'------------------------------------------------------------------------
'Gera uma chave aleatória com 'n' dígitos
'Usado para criar um nome aleatório para o arquivo
function fnGeraChave(n)
	dim s

	randomize
	s = ""
	while len(s) < n
		s = chr (int((57 - 48 + 1) * Rnd + 48)) + s
	wend
	fnGeraChave = s
end function
'------------------------------------------------------------------------

Function FormataNome(ByVal Nome)
	'Declarações
	Dim arrNome
	Dim Retorno
	Dim Cont
	Dim objER
	Dim Excecoes
	
	'Inicializando e configurando o objeto (expressão regular)
	Set objER		= New RegExp
	objER.IgnoreCase= True
	objER.Global	= True
	Excecoes		= "^(da|das|de|do|dos|e)$"
	objER.Pattern	= Excecoes
	
	'Por padrão o nome todo ficará minúsculo
	Nome = Lcase(Replace(Nome, "  ", " "))
	
	'Gerando um array com todas as palavras individualmente
	arrNome = Split(Nome, " ")
	
	Retorno = ""
	
	For Cont = 0 To Ubound(arrNome)
		arrNome(Cont) = Trim(arrNome(Cont))	
		'Colocando a primeira letra de cada palavra maiúscula (salvo exceções)
		If Not objER.Test(arrNome(Cont)) Then
			If Not arrNome(Cont) = "" Then
				arrNome(Cont) = Ucase(Left(arrNome(Cont), 1)) & Right(arrNome(Cont), Len(arrNome(Cont))-1)
			End If
		End If
		Retorno = Retorno & " " & arrNome(Cont)
	Next
	
	'Destruindo o objeto (expressão regular)
	Set objER = Nothing
	
	'Retorno da função
	FormataNome = Trim(Retorno)
End Function

' =============================================================================
Function Retorna_Tipo(prProduto)
    dim sSql2, sRs2
    dim sCampo1, sCampo2, sCampo3, sReturnConteudo, sClass, lcont

    if(prProduto = "") then exit function

    Retorna_Tipo = ""

    call Abre_Conexao()

    sSql2 = "select * from TB_TIPOS T (nolock) where T.CDCLIENTE in (" & sCdCliente & ") and T.ativo = 1 and " & _ 
            " t.id in ("& prProduto &")  " & _ 
            " order by t.descricao "
	'response.write sSql2
    ' response.End
    SET sRs2 = oConn.Execute(sSql2)

    if (not sRs2.eof) then
        lcont = 1

        do while (not sRs2.eof)
			sCampo1 = trim(sRs2("ID")) 
            sCampo2 = trim(sRs2("DESCRICAO"))
            sCampo3 = trim(sRs2("DESCRICAO_jp"))
            
            sReturnConteudo = sReturnConteudo & sCampo2 &", "

            lcont = lcont + 1
        sRs2.movenext
        loop
    else
        sReturnConteudo = ""
    end if
    set sRs2 = nothing

	Call Fecha_Conexao()

    if(sReturnConteudo <> "") then sReturnConteudo = left(sReturnConteudo, len(sReturnConteudo)-2)

    Retorna_Tipo = sReturnConteudo
End Function


' =============================================================================
Function Retorna_Frag(prProduto)
    dim sSql2, sRs2
    dim sCampo1, sCampo2, sCampo3, sReturnConteudo, sClass, lcont

    if(prProduto = "") then exit function

    Retorna_Frag = ""

    call Abre_Conexao()

    sSql2 = "select * from TB_FRAGRANCIAS T (nolock) where T.CDCLIENTE in (" & sCdCliente & ") and T.ativo = 1 and " & _ 
            " t.id in ("& prProduto &")  " & _ 
            " order by t.descricao "
	' response.write sSql2
    ' response.End
    SET sRs2 = oConn.Execute(sSql2)

    if (not sRs2.eof) then
        lcont = 1

        do while (not sRs2.eof)
			sCampo1 = trim(sRs2("ID")) 
            sCampo2 = trim(sRs2("DESCRICAO"))
            sCampo3 = trim(sRs2("DESCRICAO_jp"))
            
            sReturnConteudo = sReturnConteudo & sCampo2 &", "

            lcont = lcont + 1
        sRs2.movenext
        loop
    else
        sReturnConteudo = ""
    end if
    set sRs2 = nothing

	Call Fecha_Conexao()

    if(sReturnConteudo <> "") then sReturnConteudo = left(sReturnConteudo, len(sReturnConteudo)-2)

    Retorna_Frag = sReturnConteudo
End Function

' =============================================================================
Function Retorna_Cor(prProduto)
    dim sSql2, sRs2
    dim sCampo1, sCampo2, sCampo3, sReturnConteudo, sClass

    if(prProduto = "") then exit function

    Retorna_Cor = ""

    call Abre_Conexao()

    sSql2 = "select * from TB_CORES T (nolock) where T.CDCLIENTE in (" & sCdCliente & ") and T.ativo = 1 and " & _ 
            " EXISTS (SELECT T.id FROM TB_IMAGENS_AUXILIARES AX (nolock) WHERE AX.id_produto = '"& prProduto &"' and AX.Id_Cor = T.id and isnull(AX.CodAbacos,'') <> '' " & _ 
            " and AX.Qtd_Estoque > 0) order by t.descricao "
	
    SET sRs2 = oConn.Execute(sSql2)

    if (not sRs2.eof) then
        do while (not sRs2.eof)
			sCampo1      = trim(sRs2("ID")) 
            sCampo2      = trim(sRs2("DESCRICAO"))
            sCampo3      = trim(sRs2("DESCRICAO_jp"))
            sImagemAux   = Verifica_Registro_Auxiliar("Id_Cor", trim(sRs2("ID")) )

            sReturnConteudo = sReturnConteudo & "<a href=""javascript:void(0);"" rel=""{gallery: 'gal1', smallimage: '" & sLinkImagem &"/EcommerceNew/upload/produto/"& sImagemAux & "',largeimage: '" & sLinkImagem &"/EcommerceNew/upload/produto/"& sImagemAux &"'}"">"
                sReturnConteudo = sReturnConteudo & "<p><input class=""margin-right-15"" type=""radio"" />"& sCampo2 &"</p>"
            sReturnConteudo = sReturnConteudo & "</a>"
        sRs2.movenext
        loop
    else
        Retorna_Cor = ""
    end if
    set sRs2 = nothing

	Call Fecha_Conexao()

    Retorna_Cor = sReturnConteudo
End Function

' =============================================================================
Function Retorna_Tamanho(prProduto)
    dim sSql2, sRs2
    dim sCampo1, sCampo2, sCampo3, sReturnConteudo, sClass, lcont

    if(prProduto = "") then exit function

    Retorna_Tamanho = ""

    call Abre_Conexao()

    sSql2 = "select * from TB_TAMANHOS T (nolock) where T.CDCLIENTE in (" & sCdCliente & ") and T.ativo = 1 and " & _ 
            " t.id in ("& prProduto &")  " & _ 
            " order by t.descricao "
	'response.write sSql2
    'response.End
    SET sRs2 = oConn.Execute(sSql2)

    if (not sRs2.eof) then
        lcont = 1

        do while (not sRs2.eof)
			sCampo1 = trim(sRs2("ID")) 
            sCampo2 = trim(sRs2("DESCRICAO"))
            sCampo3 = trim(sRs2("DESCRICAO_jp"))
            
            sReturnConteudo = sReturnConteudo & sCampo2 &", "

            lcont = lcont + 1
        sRs2.movenext
        loop
    else
        sReturnConteudo = ""
    end if
    set sRs2 = nothing

	Call Fecha_Conexao()

    if(sReturnConteudo <> "") then sReturnConteudo = left(sReturnConteudo, len(sReturnConteudo)-2)

    Retorna_Tamanho = sReturnConteudo
End Function


' ================================================================
Function TirarAcento(Palavra)
    CAcento = "àáâãäèéêëìíîïòóôõöùúûüÀÁÂÃÄÈÉÊËÌÍÎÒÓÔÕÖÙÚÛÜçÇñÑ"
    SAcento = "aaaaaeeeeiiiiooooouuuuAAAAAEEEEIIIOOOOOUUUUcCnN"
    Texto = ""
    If Palavra <> "" then
            For X = 1 To Len(Palavra)
                   Letra = Mid(Palavra,X,1)
                   Pos_Acento = InStr(CAcento,Letra)
                   If Pos_Acento > 0 Then Letra = mid(SAcento,Pos_Acento,1)
                   Texto = Texto & Letra
            Next
            TirarAcento = Texto
    End If
End Function  

Function TrocarAcento(Palavra)
    CAcento = "àáâãäèéêëìíîïòóôõöùúûüÀÁÂÃÄÈÉÊËÌÍÎÒÓÔÕÖÙÚÛÜçÇñÑ"
    Texto = ""
    If Palavra <> "" Then
            For X = 1 to Len(Palavra)
                   Letra = Mid(Palavra,X,1)
                   Pos_Acento = InStr(CAcento,Letra)
                  If Pos_Acento > 0 Then Letra = "_"
                 Texto = Texto & Letra
            Next
          TrocarAcento = Texto
    End If
End Function
%>