﻿

<head>
    <!-- Basic page needs -->
    <meta charset="utf-8">

    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
    <% if(sTitleCat <> "" and not isnull(sTitleCat)) then  %>
        <title><%= sTitleCat %></title>
    <% else %>
        <% if(sTitleSub <> "" and not isnull(sTitleSub)) then  %>
            <title><%= sTitleSub %></title>
        <% else %>
            <% if(sTitleProduto <> "" and not isnull(sTitleProduto)) then  %>
                <title><%= sTitleProduto %></title>
            <% else %>
                <title><%= Application("NomeLoja") %></title>
            <% end if %>    
        <% end if %>
    <% end if %>

    <% if(PalavrasChavesCat <> "" and not isnull(PalavrasChavesCat)) then  %>
        <meta name="keywords"       content="<%= PalavrasChavesCat %>"/>
    <% else %>
        <% if(PalavrasChavesSub <> "" and not isnull(PalavrasChavesSub)) then  %>
            <meta name="keywords"       content="<%= PalavrasChavesSub %>"/>
        <% else %>
            <% if(keywords <> "" and not isnull(keywords)) then  %>
                <meta name="keywords"       content="<%= keywords %>"/>
            <% else %>
                <meta name="keywords"    content="<%= sTitleProduto %>" />
            <% end if %>
        <% end if %>
    <% end if %>

    <% if(Categoria_DescCat <> "" and not isnull(Categoria_DescCat)) then  %>
        <meta name="description"    content="<%= Categoria_DescCat %>" />
    <% else %>
        <% if(SubCategoria_DescSub <> "" and not isnull(SubCategoria_DescSub)) then  %>
            <meta name="description"    content="<%= SubCategoria_DescSub %>" />
        <% else %>
            <meta name="description"    content="<%= sTitleProduto %>" />
        <% end if %>
    <% end if %>

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/img/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/img/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<link rel="stylesheet" href="/includes/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="/includes/slick/slick.min.css">
	<link rel="stylesheet" href="/includes/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

	<script type="text/javascript" src="/includes/jquery/jquery-3.3.1.min.js"></script>

	<script type="text/javascript" src="/includes/zoom/jquery.zoom.min.js"></script>
	<script type="text/javascript" src="/includes/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/includes/slick/slick.min.js"></script>
	<script type="text/javascript" src="/includes/fancybox/fancybox-2.1.7/jquery.fancybox.js"></script>
	<link rel="stylesheet" href="/includes/fancybox/fancybox-2.1.7/jquery.fancybox.css">

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

         

    <% if( InSTr(lcase(Request.ServerVariables("script_name")), "single_product") > 0) then %>
        <!-- Metadados do Open Graph -->
        <meta property="og:title" content="<%= sNomeProduto %>">
        <meta property="og:description" content="<%= Replace(sNomeProdutoBreve, vbcrlf, " ") %>">

        <meta property="og:url" content="<%=Request.ServerVariables("URL")%>?<%=Request.ServerVariables("QueryString")%>">
        <meta property="og:image" content="https://imagensproduto.foodcrowd.com.br/EcommerceNew/upload/produto/<%= sImagemG %>">

        <meta property="product:brand"              content="<%= Application("Nome") %>">
        <meta property="product:availability"       content="em estoque">
        <meta property="product:condition"          content="novo">
        <meta property="product:price:amount"       content="<%= Replace(Replace(sPrecoQuickH, ".", ""), ",", ".") %>">
        <meta property="product:price:currency"     content="BRL">
        <meta property="product:retailer_item_id"   content="<%= nProduto %>">
        <!-- Fim dos Metadados do Open Graph -->
    <% end if %>

</head>