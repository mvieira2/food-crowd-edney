﻿<!--#include virtual= "/hidden/funcoes.asp"-->
<%
'response.End

if(Session("Id") = "") then response.redirect(sLinkhttp)

' ====================================
dim sData, sEntrega, iContador, iPedido, sSQl, Rs, sCorpo_Msg, sObs, sLinhasProdutosEmail, sSimbolo, sSomaItens
dim sCep, sCidade, sProvincia, sDescProvincia, sBairro, sNumero, sComplemento, sPais, sEndereco, idFrmPagamento, DescFrmPagamento
dim sValorTesuriyou, boolCobrarT, sHorario, boolCalcularTesuriyou, sValorEmbrulhoUnitario, sOpcTipo, sPreVenda, sPreVendaMsg, DescricaoProdutos
dim sTelefone, sFax, sCelular, sLinhasGoogleT, sLinhasGoogleI, CertificadoGE, iPedidoItem, sProdutoMaisCaro, sStatus, sNomePresenteado, sCodigoAbacos, sTipoDownload, sUsoParceiro,sUsoParceiroAux, ArrayMedia
dim CupomDescontoValidado, CupomDesconto, CupomDescontoValor, ProdutolDol4, DescFrmPagamentoJP
dim sBannerPagina, sLinhasCriteoItens, forma_pagamento_bol, sLinhasProxyIds, boolGe
dim TradeInMarca, TradeInModelo, TradeInImei, TradeInSituacao
dim TradeInMarcaBOL, TradeInModeloBOL, TradeInImeiBOL, TradeInSituacaoBOL, booltradeIn
dim TradeInMarcaCC, TradeInModeloCC, TradeInImeiCC, TradeInSituacaoCC
dim TradeInMarcaDB, TradeInModeloDB, TradeInImeiDB, TradeInSituacaoDB, bCidadeAtendida
    
dim sClassHeader: sClassHeader = "bg-1"


sEtapa                  = "4"
sStatus				    = "1"
sEntrega 			    = Replace_Caracteres_Especiais(request("entrega"))
idFrmPagamento 		    = Replace_Caracteres_Especiais(request("forma_pagamento"))
forma_pagamento_bol     = Replace_Caracteres_Especiais(request("forma_pagamento_bol"))
sOpcTipo 			    = Replace_Caracteres_Especiais(replace(trim(request("tipo_entrega")),"'",""))
sHorario 			    = Replace_Caracteres_Especiais(request("horario"))
sObs 				    = Replace_Caracteres_Especiais(replace(trim(request("obs")),"'",""))
sNfP	 			    = "1"  ' Replace_Caracteres_Especiais(request("nfp"))
sData 				    = (day(now) & "/" & month(now) & "/" & year(now))
sPreVenda			    = Replace_Caracteres_Especiais(request("PreVenda"))
sPreVendaMsg		    = Replace_Caracteres_Especiais(request("PreVendaMsg"))

TradeInMarcaBOL        = Replace_Caracteres_Especiais(replace(trim(request("TradeInMarcaBOL")),"'",""))
TradeInModeloBOL       = Replace_Caracteres_Especiais(replace(trim(request("TradeInModeloBOL")),"'",""))
TradeInImeiBOL         = Replace_Caracteres_Especiais(replace(trim(request("TradeInImeiBOL")),"'",""))
TradeInSituacaoBOL     = Replace_Caracteres_Especiais(replace(trim(request("TradeInSituacaoBOL")),"'",""))

TradeInMarcaCC        = Replace_Caracteres_Especiais(replace(trim(request("TradeInMarcaCC")),"'",""))
TradeInModeloCC       = Replace_Caracteres_Especiais(replace(trim(request("TradeInModeloCC")),"'",""))
TradeInImeiCC         = Replace_Caracteres_Especiais(replace(trim(request("TradeInImeiCC")),"'",""))
TradeInSituacaoCC     = Replace_Caracteres_Especiais(replace(trim(request("TradeInSituacaoCC")),"'",""))

TradeInMarcaDB        = Replace_Caracteres_Especiais(replace(trim(request("TradeInMarcaDB")),"'",""))
TradeInModeloDB       = Replace_Caracteres_Especiais(replace(trim(request("TradeInModeloDB")),"'",""))
TradeInImeiDB         = Replace_Caracteres_Especiais(replace(trim(request("TradeInImeiDB")),"'",""))
TradeInSituacaoDB     = Replace_Caracteres_Especiais(replace(trim(request("TradeInSituacaoDB")),"'",""))

CupomDesconto           = SeSsion("cupomdesconto")      ' Replace_Caracteres_Especiais(request("CupomDesconto"))
CupomDescontoValor      = SeSsion("cupomdescontovalor") ' 100.00
CupomDescontoValidado   = false
sLinhasGoogleT 		    = ""
sLinhasGoogleI 		    = ""
boolCalcularTesuriyou   = true
boolGe                  = false
bCidadeAtendida         = "0"

if(forma_pagamento_bol <> "" and forma_pagamento_bol = "1") then idFrmPagamento = "22"

if(idFrmPagamento <> "") then 
	DescFrmPagamento 	= trim(Recupera_Campos_Db("TB_FORMAS_PAGAMENTO", idFrmPagamento, "id", "descricao"))
	if(idFrmPagamento 	= "35") then sStatus = "47"
end if

'============================================================
Select case idFrmPagamento
    case "22"
        TradeInMarca    = TradeInMarcaBOL   
        TradeInModelo   = TradeInModeloBOL  
        TradeInImei     = TradeInImeiBOL    
        TradeInSituacao = TradeInSituacaoBOL

    case "16", "18", "77", "15", "17", "14"
        TradeInMarca    = TradeInMarcaCC   
        TradeInModelo   = TradeInModeloCC  
        TradeInImei     = TradeInImeiCC    
        TradeInSituacao = TradeInSituacaoCC        

    case "85", "87", "86", "88"
        TradeInMarca    = TradeInMarcaDB   
        TradeInModelo   = TradeInModeloDB  
        TradeInImei     = TradeInImeiDB    
        TradeInSituacao = TradeInSituacaoDB
end select 


'============================================================
if(CupomDesconto <> "") then

    if(lcase(trim(CupomDesconto)) = "maisde1000") then CupomDescontoValidado = true
    if(lcase(trim(CupomDesconto)) = "crowd1000") then CupomDescontoValidado = true

    'if(CupomDescontoValidado = true) then 
	 '   For i = 1 to Session("ItemCount")
     '       If(ARYshoppingCart(C_ID, i) <> "500242" AND ARYshoppingCart(C_ID, i) <> "493374" AND ARYshoppingCart(C_ID, i) <> "493375") Then 
     '           CupomDescontoValidado = false
     '           exit for
     '       end if
     '   Next
    'end If   

	'if(CupomDescontoValidado = true) then
	'	call Abre_Conexao()
    '
	'	sSQl = "Select count(*) as total from TB_PEDIDOS where cdcliente = "& sCdCliente &" and id_status not in (5, 98, 3) and CupomDesconto = '"& CupomDesconto &"'"
	'	Set Rs = oConn.execute(sSQl)
    '
	'	if(not Rs.eof) then 
	'		If(cdbl(Rs("total")) >= 1 ) Then CupomDescontoValidado = false
	'	end if
	'	Set Rs = Nothing
	'end if	
else
    CupomDescontoValor = 0
end if

if(NOT CupomDescontoValidado) then CupomDescontoValor = 0


call Abre_Conexao()

' ENTREGA ENDERECO DIFERENTE ================================
if(sEntrega = "1") then 
	sCep 		= Replace_Caracteres_Especiais(replace(trim(request("cep")),"'",""))
	sCidade 	= Replace_Caracteres_Especiais(replace(trim(request("cidade")),"'",""))
	sProvincia 	= Replace_Caracteres_Especiais(replace(trim(request("provincia")),"'",""))
	sBairro 	= Replace_Caracteres_Especiais(replace(trim(request("bairro")),"'",""))
	sNumero 	= Replace_Caracteres_Especiais(replace(trim(request("numero")),"'",""))
	sComplemento= Replace_Caracteres_Especiais(replace(trim(request("complemento")),"'",""))
	sPais 		= Replace_Caracteres_Especiais(replace(trim(request("pais")),"'",""))
	sEndereco 	= Replace_Caracteres_Especiais(replace(trim(request("endereco")),"'",""))

    boolDataCampanhaFrete = Monta_Campanha_Frete()
end if

' ENTREGA CADASTRO ==========================================
if(sEntrega = "") then
	sSqlClienteCompra = "Select top 1 cep, cidade, Id_Provincia, bairro, numero, complemento, pais, endereco from TB_CLIENTES (nolock) where id = "& Session("Id")
	set ClienteCompra = oConn.execute(sSqlClienteCompra)
	if (not ClienteCompra.eof) then
	    sCep 		= trim(ClienteCompra("cep"))
	    sCidade 	= trim(ClienteCompra("cidade"))
	    sProvincia 	= trim(ClienteCompra("Id_Provincia"))
	    sBairro 	= trim(ClienteCompra("bairro"))
	    sNumero 	= trim(ClienteCompra("numero"))
	    sComplemento= trim(ClienteCompra("complemento"))
	    sPais 		= trim(ClienteCompra("pais"))
	    sEndereco 	= trim(ClienteCompra("endereco"))
	end if
	set ClienteCompra = nothing
end if
sDescProvincia = trim(Recupera_Campos_Db_oConn("TB_ESTADOS", sProvincia, "id", "descricao"))


' ============================================================
sCidadeCliente = sCidade
if(InStr(ucase("," & CidadesAtendidas & ","), ucase("," & sCidadeCliente & ",") ) > 0 ) then bCidadeAtendida = "1"

if(bCidadeAtendida = "0") then  
    response.write "<script>alert('Cidade n\u00e2o atendida para entrega!\n\nPor favor, altere seus dados de entrega.'); location.href='/pagamento';</script>"
    response.end
end if
' ============================================================


' PEDIDO ZERADO CUPOM =============================================
if (Session("TotalCompra") = 0 and Replace_Caracteres_Especiais(request("presente")) <> "" ) then
	Session("sValorFrete") = 0
end if
' =================================================================

' GRAVA PEDIDO ==============================================
Session("PorcentagemFinanciadora") = "0"
if(Session("TotalDescontoCat") = "") then Session("TotalDescontoCat") = "0"
if(Session("TotalDescontoSub") = "") then Session("TotalDescontoSub") = "0"
if(Session("TotalDescontoPro") = "") then Session("TotalDescontoPro") = "0"
if(Session("TotalDescontoProDc") = "") then Session("TotalDescontoProDc") = "0"
' ---------------------------------------------------------------------------------

Parcelamento        = Replace_Caracteres_Especiais(request("parcelamento"))
parcelamentoSeguro  = Replace_Caracteres_Especiais(request("parcelamentoSeguro"))

' =====================================================================================
if(idFrmPagamento = "35" or idFrmPagamento = "23" or idFrmPagamento = "22" or idFrmPagamento = "26") then
	Cartao_Nome = ""
    Cartao_CPF = ""
    Cartao_DN = ""
    Cartao_Tel = ""
	Cartao_Numero = ""
	Cartao_Codigo = ""
	Cartao_Validade = ""
else
    Cartao_Nome 	= Replace_Caracteres_Especiais(replace(request("Cartao_Nome"),",","."))
    Cartao_CPF      = Replace_Caracteres_Especiais(replace(request("Cartao_CPF"),",","."))
    Cartao_DN       = Replace_Caracteres_Especiais(replace(request("Cartao_DN1"),",",".")) & "/" & Replace_Caracteres_Especiais(replace(request("Cartao_DN2"),",",".")) & "/" & Replace_Caracteres_Especiais(replace(request("Cartao_DN3"),",","."))
    Cartao_Tel      = Replace_Caracteres_Especiais(replace(request("Cartao_Tel"),",","."))
    Cartao_Numero 	= Replace_Caracteres_Especiais(replace(request("Cartao_Numero"),",","."))
    Cartao_Codigo 	= Replace_Caracteres_Especiais(replace(request("Cartao_Codigo"),",","."))
    Cartao_Validade = Replace_Caracteres_Especiais(replace(request("Cartao_Validade1"),",",".")) & "/" & Replace_Caracteres_Especiais(replace(request("Cartao_Validade2"),",","."))

    if(dFrmPagamento = "85" OR idFrmPagamento = "86" OR idFrmPagamento = "87" OR idFrmPagamento = "88") then 
        Parcelamento    = "1"
        Cartao_Nome 	= "" 
        Cartao_CPF      = ""
        Cartao_DN       = ""
        Cartao_Tel      = ""
        Cartao_Numero 	= ""
        Cartao_Codigo 	= ""
        Cartao_Validade = ""
        DescFrmPagamentoJP= trim(Recupera_Campos_Db_oConn("TB_FORMAS_PAGAMENTO", idFrmPagamento, "id", "descricao_jp"))
    end if
end if

if(Parcelamento = "" or isnull(Parcelamento)) then Parcelamento = 1

if(idFrmPagamento = "21" OR idFrmPagamento = "22" OR idFrmPagamento = "23" OR idFrmPagamento = "35" or idFrmPagamento = "26") then
	Parcelamento = "0"
end if

' VALE PRESENTE - LOGICA ===================
if (false) then
'if(Replace_Caracteres_Especiais(request("presente")) <> "") then 
	if((InStr(Replace_Caracteres_Especiais(TRIM(request("presente"))), "CVINHO112007") = 0) ) then
		' ENTRA AQUI SOMENTE SE O CLIENTE NAO RECALCULOU NA PAGINA ANTERIOR -----------------
		if(Session("ValorPresente") = "" OR Session("ValorPresente") = "0") then

                if (Recupera_Campos_Db("TB_CUPONS", "'" & Replace_Caracteres_Especiais(request("presente")) & "'", "Cupom", "Valor") <> "") then
					Session("ValorPresente") = RetornaCasasDecimais(trim(Recupera_Campos_Db("TB_CUPONS", "'" & Replace_Caracteres_Especiais(request("presente")) & "'", "Cupom", "Valor")),2)
				 	Session("IdPresente") 	 = Replace_Caracteres_Especiais(request("presente"))

					' QUANDO O VALE-PRESENTE EH MAIOR Q A COMPRA TODA
					if(cdbl(Session("ValorPresente")) > cdbl(Session("TotalCompra"))) then

						' GRAVAR O VALOR RESTANTE DO SALDO DO VALE PRESENTE
						call Abre_Conexao()
		
						sSQl = "UPDATE TB_CUPONS SET ClienteUtilizou = "& Session("Id") &", DataUtilizado = getdate(), Utilizado = 1 where Cupom = '"& Replace_Caracteres_Especiais(TRIM(request("presente"))) &"'"
						oConn.execute(sSQl)
		
						call Fecha_Conexao()
						
						' SUBTRAIR SOMENTE O SUBTOTAL DO TOTAL GERAL
						Session("TotalCompra") = 0
					else
						' QUANDO O VALE-PRESENTE EH MAIOR Q O SUBTOTAL
						if(cdbl(Session("ValorPresente")) > cdbl(Session("SubTotalCompra"))) then 
							Session("TotalCompra")	   = (Session("TotalCompra") - cdbl(Session("SubTotalCompra")))
							Session("SubTotalCompra")  = 0
						else
							Session("TotalCompra") = (Session("TotalCompra") - cdbl(Session("ValorPresente")))
						end if
					end if
			end if
		end if
	end if
else
	if(Session("CupomGet") = "" and Session("ValorPresente") = 0) then
		Session("ValorPresente") = 0
		Session("IdPresente") = ""
	end if
end if

' LOGICA DE VALE-FRETE ****************************************************
if(trim(request("cfrete")) <> "") then 
	' GRAVAR O VALOR RESTANTE DO SALDO DO VALE FRETE, QUANDO VP > TOTALCOMPRA
	call Abre_Conexao()
	
	sValorRestante = Session("ValePresenteFreteSaldo")
	if(sValorRestante = "") then sValorRestante = 0
	
	sSQl = "UPDATE TB_CUPONS SET ValorRestante = "& sValorRestante &" where Cupom = '"& trim(request("cfrete")) &"'"
	oConn.execute(sSQl)
	
	call Fecha_Conexao()
else
	Session("ValorPresenteFrete") = 0
	Session("IdPresenteFrete") = ""
end if

if(idFrmPagamento = "") then 
	if(cdbl(Session("TotalCompra")) > 0) Then 
		idFrmPagamento = "22"
	else
		if(Replace_Caracteres_Especiais(trim(request("presente"))) <> "") then 
			idFrmPagamento = "26"	' VALE PRESENTE
		end if
	end if
else
    ' LOGICA PARA DAR DESCONTOS NOS BOLETOS.
    if(idFrmPagamento = "22") then
        sTotalBoletoDesconto        = FormatNumber((nDescontoBoleto * cdbl(Session("SubTotalCompra"))),2)
        Session("TotalDescontoPro") = sTotalBoletoDesconto
        Session("SubTotalCompra22") = FormatNumber(cdbl(Session("SubTotalCompra"))  - cdbl(sTotalBoletoDesconto),2)
        Session("TotalCompra22")    = FormatNumber(cdbl(Session("TotalCompra"))     - cdbl(sTotalBoletoDesconto),2)
    end if
end if

if(sProvincia = "") 	then sProvincia = "24"

if(Replace_Caracteres_Especiais(request("presente")) <> "") then 
	if (Recupera_Campos_Db("TB_CUPONS", "'" & Replace_Caracteres_Especiais(request("presente")) & "'", "Cupom", "Valor") <> "") then
		Session("CupomGet") = Replace_Caracteres_Especiais(request("presente"))
	end if
end if

Desconto = Session("Desconto")
if(Session("CupomGet") <> "") then Desconto = 0

If(Parcelamento = "0" OR Parcelamento = "1") then
	ValorParcela = RetornaCasasDecimais((Session("TotalCompra")),2)
else
	ValorParcela 			= RetornaCasasDecimais((Session("TotalCompra") / Parcelamento),2)
	Session("TotalCompra") 	= Session("TotalCompra")   
end if

' ============================================================
' VERIFICAR ESTOQUE DO PEDIDO ANTES DE FINALIZAR A COMPRA
if(false) then
	For iContador = 1 to iCount
		sEstoqueatual = Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID, iContador), "id", "Qte_Estoque")
		
		if(sEstoqueatual =< 0) then
			Session.Abandon ' ENCERRA AS SESSIOES DO SITE PARA UM NOVO PEDIDO
	
			response.write "<BR><BR><div align=""center"">Infelizmente durante a sua compra o item <strong>"& Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID, iContador), "id", "Descricao") &"</strong> escolhido ficou indisponível em nosso estoque. <br>Para concluir sua compra é necessário incluir novamente os itens desejados em seu carrinho."
			response.write "<br><br><a href=""/"">IR PARA A HOME</a></div>"
	
			call Envia_Email("edney@ominic.com.br", Application("Mailpedidos"), Application("NomeLoja"), "Pedido_finalizado - Sem Estoque - Pedido não entrou!", "Sem estoque: "& Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID, iContador), "id", "Descricao"))
			
			response.end
		end if
	next
end if
' ============================================================

' VERIFICA SE É ENTREGA AGENDADA
	if (session("sCheckedAgendamento")) then
		session("sCheckedAgendamento") = 1
	else
		session("sCheckedAgendamento") = 0
	end if
' ============================================================
' VERIFICA SE É CAMAPNHA FRETE GRATIS
if (Session("sValorFrete") > 0) then
	Session("CampanhaFreteGratis")	= ""
end if
' ============================================================

if(idFrmPagamento = "22") then
    SubTotalCompra  = Session("SubTotalCompra22")
    TotalCompra     = Session("TotalCompra22")
    Parcelamento    = "0"

    'if(CupomDescontoValidado = true and CupomDesconto <> "") then
    '    Session("TotalDescontoPro") = FormatNumber((cdbl(Session("TotalDescontoPro")) + cdbl(CupomDescontoValor)),2)
    '    SubTotalCompra              = FormatNumber(cdbl(Session("SubTotalCompra22"))  - cdbl(CupomDescontoValor),2)
    '    TotalCompra                 = FormatNumber(TotalCompra,2)
    '    ValorParcela                = FormatNumber(TotalCompra,2)
    'end if

    'response.write SubTotalCompra
    'response.end

else
    SubTotalCompra      = Session("SubTotalCompraPrazo")
    TotalCompra         = Session("TotalCompraPrazo")
    'CupomDescontoValor  = 0
   
    if(Parcelamento > 1) then   
        ValorParcela   = FormatNumber(TotalCompra / parcelamento ,2)
    else
        ValorParcela   = FormatNumber(TotalCompra,2)
    end if

    if(CupomDescontoValidado = true) then
        If(Parcelamento = "0" OR Parcelamento = "1") then
            Session("TotalDescontoPro") = FormatNumber(CupomDescontoValor,2)
            SubTotalCompra              = FormatNumber(cdbl(SubTotalCompra)  - cdbl(CupomDescontoValor),2)
            TotalCompra                 = FormatNumber(cdbl(SubTotalCompra)  + cdbl(Session("sValorFrete")),2)
            ValorParcela                = FormatNumber(TotalCompra,2)
        else
            Session("TotalDescontoPro") = FormatNumber(CupomDescontoValor,2)
            SubTotalCompra              = FormatNumber(cdbl(Session("SubTotalCompraPrazo")) - cdbl(CupomDescontoValor),2)
            TotalCompra                 = FormatNumber(Session("SubTotalCompraPrazo") + Session("sValorFrete") ,2)
            ValorParcela                = FormatNumber(Session("TotalCompraPrazo")  / Parcelamento,2)
        end if
    end if
end if

' ============================================================
' VERIFICAR PEDIDO DUPLICADO PARA O MESMO CLIENTE
if(false) then
	call Abre_Conexao()
	
	sSQl = "Select top 3 id, id_cliente, datacad, total from TB_PEDIDOS where cdcliente = "& sCdCliente &" and id_status not in (3, 5, 98) order by Id desc"
	Set Rs = oConn.execute(sSQl)

	if(not Rs.eof) then 
		do while not Rs.eof
			if(trim(rs("id_cliente")) = trim(Session("Id"))) then
				sValorPedidoCheca 	= RetornaCasasDecimais(rs("total"),2)
			
				if(sValorPedidoCheca = RetornaCasasDecimais(TotalCompra,2)) then
					Session.Abandon ' ENCERRA AS SESSIOES DO SITE PARA UM NOVO PEDIDO

					response.write "<BR><BR><div align=""center"">Voce realizou um um pedido ("& trim(rs("id")) &") com o(s) mesmo(s) produto(s) <br/>e foi processado com sucesso, portanto, para sua segurança, este pedido sera cancelado. "
					response.write "<br><br><a href=""/"">IR PARA A HOME</a></div>"
			
					call Envia_Email("edney@ominic.com.br", Application("Mailpedidos"), Application("NomeLoja"), "Pedido_finalizado - Pedido Duplicado mesmo VALOR", "Pedido repetido: "& trim(rs("id")))
					response.end
				end if
			end if
		Rs.movenext
		loop
	end if
	Set Rs = nothing
end if
' ============================================================


' ============================================================
if(false) then
	if(boolDataCampanhaFrete = false and (Session("sValorFrete") = "0" OR Session("sValorFrete") = "")) then
		Session.Abandon ' ENCERRA AS SESSIOES DO SITE PARA UM NOVO PEDIDO
		response.write "<BR><BR><div align=""center"">Erro no calculo (1), favor refazer a compra."
		response.write "<br><br><a href=""/"">IR PARA A HOME</a></div>"
		call Envia_Email("edney@ominic.com.br", Application("Mailpedidos"), Application("NomeLoja"), "Pedido_finalizado - Pedido FRETE ZERADO", "")
		response.end
	end if
	if((SubTotalCompra = "0" OR SubTotalCompra = "")) then
		Session.Abandon ' ENCERRA AS SESSIOES DO SITE PARA UM NOVO PEDIDO
		response.write "<BR><BR><div align=""center"">Erro no calculo (2), favor refazer a compra."
		response.write "<br><br><a href=""/"">IR PARA A HOME</a></div>"
		call Envia_Email("edney@ominic.com.br", Application("Mailpedidos"), Application("NomeLoja"), "Pedido_finalizado - Pedido SUBTOTAL ZERADO", "")
		response.end
	end if
	if((TotalCompra = "0" OR TotalCompra = "")) then
		Session.Abandon ' ENCERRA AS SESSIOES DO SITE PARA UM NOVO PEDIDO
		response.write "<BR><BR><div align=""center"">Erro no calculo (3), favor refazer a compra."
		response.write "<br><br><a href=""/"">IR PARA A HOME</a></div>"
		call Envia_Email("edney@ominic.com.br", Application("Mailpedidos"), Application("NomeLoja"), "Pedido_finalizado - Pedido TOTAL ZERADO", "")
		response.end
	end if
end if
' ============================================================

'RESPONSE.Write boolDataCampanhaFrete
'RESPONSE.Write Session("TotalDescontoPro") & "<br>"
'RESPONSE.Write SubTotalCompra & "<br>"
'RESPONSE.Write TotalCompra & "<br>"
'RESPONSE.Write ValorParcela & "<br>"
'response.End

sSQl = " Insert into TB_PEDIDOS (CdCliente, Id_Cliente, Id_Provincia, Id_Status, Id_FrmPagamento, Entrega_Diferente, Endereco, Cep,  " & _
								"Cidade, Bairro, Numero, Complemento, Pais, SubTotal, Total, PesoTotal, Frete, FreteReal, Tesuriyou, TxRefrigerado, Imposto, Embrulho, TipoEntrega, ValorTipoEntrega, " & _
								"PorcentagemCartao, Desconto, DescontoPro, DescontoCat, DescontoSub, DescontoValePresente, DescontoValeFrete, Horario, Observacoes, " & _
							   " Cartao_Nome, Cartao_Numero, Cartao_Codigo, Cartao_Validade, Cartao_CPF, Cartao_Tel, Cartao_DN, Parcelamento, ParcelamentoSeguro, ValorParcela, " & _
								"IpConexao, DataCad, CupomDesconto, CupomDescontoFrete, VeioDoBanner, NfP, " & _
								"TipoEntregaDescricao, TipoEntregaTransportadora, CampanhaFreteGratis,NomePresenteado, UsoParceiro, UsoParceiroAux, " & _
								"ProtocoloAgendamento, Agendamento, AgendamentoData, AgendamentoPeriodo, TransportadoraPrazoEntrega) " & _	
	   " values ("& sCdCliente &", "& Session("Id") &", "& sProvincia &", "& sStatus &", "& idFrmPagamento &", '"& sEntrega &"', '"& sEndereco &"', "& _ 
				" '"& sCep &"','"& sCidade &"', '"& sBairro &"', '"& sNumero &"', '"& sComplemento &"', 'BR', " & _ 
				"'"& replace(replace(SubTotalCompra,".",""),",",".") 		&"', "& _
				"'"& replace(replace(RetornaCasasDecimais(TotalCompra,2),".",""),",",".") &"', " &_ 
				"'"& replace(replace(Session("sValorPeso"),".",""),",",".") 			&"', "& _
				"'"& replace(replace(Session("sValorFrete"),".",""),",",".") 			&"', "& _
				"'"& replace(replace(Session("sValorFreteReal"),".",""),",",".") 		&"', "& _
				"'"& replace(replace(Session("TotalTesuriyou"),".",""),",",".") 		&"', "& _
				"'"& replace(replace(Session("ValorRefrigerado"),".",""),",",".") 		&"', "& _ 
				"'"& replace(replace(Session("ValorDescontadoImposto"),".",""),",",".") &"', "& _
				"'"& replace(replace(Session("ValorEmbrulho"),".",""),",",".") 			&"', "& _
				"'"& replace(replace(sOpcTipo,".",""),",",".") 							&"', "& _
				"'"& replace(replace(sValorTipoEntrega,".",""),",",".") 				&"', "& _
				"'"& Session("PorcentagemFinanciadora") 								&"', "& _
				"'"& replace(replace(Desconto,".",""),",",".") 							&"', "& _ 
				"'"& replace(replace(Session("TotalDescontoPro"),".",""),",",".") 		&"', "& _
				"'"& replace(replace(Session("TotalDescontoCat"),".",""),",",".") 		&"', "& _
				"'"& replace(replace(Session("TotalDescontoSub"),".",""),",",".") 		&"', "& _ 
				"'"& replace(replace(Session("ValorPresente"),".",""),",",".") 			&"', "& _
				"'"& replace(replace(CupomDescontoValor,".",""),",",".") 	&"', "& _
				"'"& sHorario &"', '"& sObs &"', '"& _ 
					 Cartao_Nome &"', '"& _
					 Cartao_Numero &"', '"& _
					 Cartao_Codigo &"', '"& _
					 Cartao_Validade &"', '"& _
					 Cartao_CPF &"', '"& _
					 Cartao_Tel &"', '"& _
					 Cartao_DN &"', '"& _
					 Parcelamento &"', '"& _
                     parcelamentoSeguro &"', '"& _
					 replace(replace(ValorParcela,".",""),",",".") &"', "& _
				"'"& Request.ServerVariables("Remote_addr") &"', getdate(), '"& CupomDesconto &"', " & _ 
				" '"& Replace_Caracteres_Especiais(request("cfrete")) &"', '"& Session("bannerid") &"', '"& sNfP &"', " & _
				" '"& Session("TipoEntrega") &"', '"& Session("Transportadora") &"', '"& Session("CampanhaFreteGratis") &"','"&sNomePresenteado&"','"& sUsoParceiro &"', '"& sUsoParceiroAux &"', " & _
				" '"& session("sProtocoloAgendamento") &"','"& session("sCheckedAgendamento") &"','"& session("sDatadaentregaAgendamento") &"','"& session("sPeriodoAgendamento") &"', '"& Session("DiaPrazoTotal") &"')"

'response.write "Parcelamento: " & Parcelamento & "<br>"
'response.write "CupomDesconto: " & CupomDesconto & "<br>"
'response.write "TotalDescontoPro: " & Session("TotalDescontoPro") & "<br>"
'response.write "CupomDescontoValor: " & CupomDescontoValor & "<br>"
'response.write "SubTotalCompra: " & SubTotalCompra & "<br>"
'response.write "TotalCompra: " & TotalCompra    & "<br>"   
'response.write sSQl & "<br>"
'response.End

call Abre_Conexao()
'On Error Resume Next
oConn.execute(sSQl)

sSql = "SELECT @@IDENTITY AS 'Id'"
Set Rs = oConn.execute(sSQl)
if(not Rs.eof) then iPedido = rs("id")
Set Rs = nothing

if(idFrmPagamento = "21" OR idFrmPagamento = "22" OR idFrmPagamento = "23" OR idFrmPagamento = "35" or idFrmPagamento = "26") then
	sSQl = "Update TB_PEDIDOS set ValorParcela = total where id = "& iPedido
	oConn.execute(sSQl)
end if


if(Err.Number <> "0") then 
	call Envia_Email("edney@ominicbr.comr", Application("Mailpedidos"), Application("NomeLoja"), "Erro na pagina pedido_finalizado - linha 234", (sSQl & "<br><br>" & Err.Description))
end if
on error goto 0

call Fecha_Conexao()

' RECUPERA ID DO PEDIDO ==============================================
call Abre_Conexao()

' ========================================================
' INTEGRACAO GOOGLE **************************************
if(sPais = "" or isnull(sPais)) then sPais = "Brasil"

    
' ========================================================
' INSERI LINHA NA TABELA HISTORICO ------------------------------
sSQl = "INSERT INTO TB_HISTORICOS (Id_Pedido, Id_Status, Descricao) VALUES ('"& iPedido &"', "& sStatus &", '"& TRIM(Recupera_Campos_Db_oConn("TB_STATUS", sStatus, "id", "DESCRICAO")) &"') "
oConn.execute(sSQl)
' ---------------------------------------------------------------

' VALE PRESENTE - LOGICA ========================================
if(Session("CupomGet") <> "") then 
	sSQl = "Update TB_CUPONS set ClienteUtilizou = '"& Session("Id") &"', NumeroPedidoUtilizado = '"& iPedido &"', Utilizado = 1, DataUtilizado = getdate() where Cupom = '"& Session("CupomGet") &"'"
	oConn.execute(sSQl)
end if

if(lcase(TRIM(request("presente"))) <> "natalvpc2006") then 
	if(Session("ValorPresente") <> "" and Session("ValorPresente") > 0) then
		if(Session("IdPresente") <> "") then 
			sSQl = "Update TB_CUPONS set ClienteUtilizou = '"& Session("Id") &"', NumeroPedidoUtilizado = '"& iPedido &"', Utilizado = 1, DataUtilizado = getdate() where Cupom = '"& Session("IdPresente") &"'"
			oConn.execute(sSQl)
		end if
	end if
		
	if(Session("ValorPresenteFrete") <> "" and Session("ValorPresenteFrete") > 0) then
		sSQl = "Update TB_CUPONS set ClienteUtilizou = '"& Session("Id") &"', NumeroPedidoUtilizado = '"& iPedido &"', Utilizado = 1, DataUtilizado = getdate() where Cupom = '"& Session("IdPresenteFrete") &"'"
		oConn.execute(sSQl)
	end if
end if
	
' GRAVA ITENS DO PEDIDO ===========================================
sLinhasProdutosEmail = ""
sSomaItens = 0
ProdutolDol4 =  ""

if(Parcelamento > 1) then 
	Session("SubTotalCompra") = 0
end if

sLinhasLomadee  = ""
boolTradeIn     = false

For iContador = 1 to iCount
    sTradeIn  = trim(Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID, iContador), "id", "BoolTradeIn"))	


	if(Session("Embrulho_" & ARYshoppingCart(C_ID, iContador)) <> "1") then
		sValorEmbrulhoUnitario = 0
	else
		sValorEmbrulhoUnitario = ARYshoppingCart(C_Embrulho, iContador)
	end if
		
	If(Parcelamento = "0" OR Parcelamento = "1") then
		sPrecoProduto = ARYshoppingCart(C_Preco, iContador)
	else
		sPrecoProduto = ARYshoppingCart(C_PrecoPrazo, iContador)
	end if
		
	if(Parcelamento > 1) then
		If(Parcelamento <= 1) then
			Session("SubTotalCompra") = RetornaCasasDecimais((CDBL(Session("SubTotalCompra")) + (ARYshoppingCart(C_Qtd, iContador) * ARYshoppingCart(C_Preco, iContador))),2)
		else
			Session("SubTotalCompra") = RetornaCasasDecimais((CDBL(Session("SubTotalCompra")) + (ARYshoppingCart(C_Qtd, iContador) * ARYshoppingCart(C_PrecoPrazo, iContador))),2)
		end if
	end if

	sPrecoProdutoDesconto = 0

	if(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "DataIniPromo")) <> "" and not isnull(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "DataIniPromo"))) then 
		BoolDataPromocao = ((cdate(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "DataIniPromo")) <= Date) and (cDate(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "DataFimPromo")) >= Date))
	end if

	if( (trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "DescontoIgnorar")) = "0" or ISNULL(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "DescontoIgnorar"))) ) AND _
		( (Desconto <> "" and Desconto <> "0")																																						OR _ 
			( (trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "Promocao")) = "1" AND BoolDataPromocao) OR Session("CupomGet") <> "")															OR _ 
			(Session("d" & ARYshoppingcart(C_ID,iContador)) <> "" and Session("d" & ARYshoppingcart(C_ID,iContador)) <> "0") 																												OR _
			(Session("dc" & ARYshoppingcart(C_ID,iContador)) <> "" and Session("dc" & ARYshoppingcart(C_ID,iContador)) <> "0")  																											OR _
			(VerificaDesconto(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "id_categoria"), Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "id_subcategoria"))) ) ) then 
		
		if(Session("d" & ARYShoppingcart(C_ID,iContador)) > 0 ) then 
			if(ucase(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "DescontoTipo")) = "R") then 
				sPrecoProdutoDesconto = RetornaCasasDecimais((cdbl(ARYshoppingcart(C_Preco,iContador)) - _
								((cdbl(Desconto + _
								Session("Cat" & Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "id_categoria")) + _
								Session("Sub" & Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "id_subcategoria"))) / 100) * cdbl(Session(ARYshoppingcart(C_ID,iContador))))) - Session("d" & ARYShoppingcart(C_ID,iContador)) ,2)

				sValorPresenteReal = RetornaCasasDecimais((((cdbl(Desconto + _
									Session("Cat" & Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "id_categoria")) + _
									Session("Sub" & Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "id_subcategoria"))) / 100) * cdbl(Session(ARYshoppingcart(C_ID,iContador))))) - Session("d" & ARYShoppingcart(C_ID,iContador)) ,2)
			else
				sPrecoProdutoDesconto = RetornaCasasDecimais((cdbl(ARYshoppingcart(C_Preco,iContador)) - _
								((cdbl(Desconto + _
								Session("Cat" & Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "id_categoria")) + _
								Session("Sub" & Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "id_subcategoria")) + Session("d" & ARYShoppingcart(C_ID,iContador)) ) / 100) * cdbl(Session(ARYshoppingcart(C_ID,iContador))))),2)

				sValorPresenteReal = RetornaCasasDecimais((((cdbl(Desconto + _
								Session("Cat" & Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "id_categoria")) + _
								Session("Sub" & Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "id_subcategoria")) + Session("d" & ARYShoppingcart(C_ID,iContador)) ) / 100) * cdbl(Session(ARYshoppingcart(C_ID,iContador))))),2)
			end if
		else
			sPrecoProdutoDesconto = RetornaCasasDecimais((cdbl(ARYshoppingcart(C_Preco,iContador))- _
							((cdbl(Desconto + _
							Session("Cat" & Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "id_categoria")) + _
							Session("Sub" & Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingcart(C_ID,iContador), "id", "id_subcategoria")))))),2)
		end if
	else
		sPrecoProdutoDesconto = sPrecoProduto
	end if
	
	call Abre_Conexao()

    if(Session("CodAbacosAces" & ARYshoppingCart(C_ID, iContador)) <> "" and not isnull(Session("CodAbacosAces" & ARYshoppingCart(C_ID, iContador))) ) then 
        sCodigoAbacos = Session("CodAbacosAces" & ARYshoppingCart(C_ID, iContador))
    else
	    sCodigoAbacos = Recupera_Codigo_Abacos(ARYshoppingCart(C_ID, iContador) ,ARYshoppingCart(C_Tamanho, iContador), ARYshoppingCart(C_Cor, iContador), "", ARYshoppingCart(C_Tipo, iContador))	
    end if

    if(sTradeIn = "1" and boolTradeIn = false) then 
        boolTradeIn     = true
        'sTradeInValor   = trim(Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID, iContador), "id", "TradeInValorDesconto"))	
        'if(sTradeInValor = "" or isnull(sTradeInValor)) then sTradeInValor = 0
    end if

	sSQl = "Insert Into TB_PEDIDOS_ITENS (Id_Pedido, Id_Produto, Id_Moeda, Id_Tamanho, Id_Cor, Id_Sabor, Id_Fragrancia, Id_Tipo, Id_Pacote, " & _
				" Id_Tamanho_Acrescimo, Id_Cor_Acrescimo, Id_Sabor_Acrescimo, Id_Fragrancia_Acrescimo, " & _
				" Id_Tipo_Acrescimo, Id_Pacote_Acrescimo, Id_Ge, Id_Ge_Acrescimo, " & _
				" Preco_Unitario,  " & _
				" Preco_Unitario_Bruto,  " & _
				" Preco_Unitario_Desconto,  " & _
				" Embrulho, Quantidade, CertificadoGE, ValorGe, PremioLiquidoGe, ProLaboreGe, IOFGe, situacao, CodAbacos, DescontoTradeIn) " & _ 
			"values ("& iPedido &", "& ARYshoppingCart(C_ID, iContador) &", "& ARYshoppingCart(C_Moeda, iContador) &", '"& ARYshoppingCart(C_Tamanho, iContador) &"', '"& ARYshoppingCart(C_Cor, iContador) &"', "& _
			" '"& ARYshoppingCart(C_Sabor, iContador) &"', '"& ARYshoppingCart(C_Fragrancia, iContador) &"', '"& ARYshoppingCart(C_Tipo, iContador) &"', '"& ARYshoppingCart(C_Pacote, iContador) &"'," &_ 
			" '"& ARYshoppingCart(C_TamanhoAcres, iContador) &"','"& ARYshoppingCart(C_CorAcres, iContador) &"', '"& ARYshoppingCart(C_SaborAcres, iContador) &"', '"& ARYshoppingCart(C_FragranciaAcres, iContador) &"', "& _ 
			" '"& ARYshoppingCart(C_TipoAcres, iContador) &"', '"& ARYshoppingCart(C_PacoteAcres, iContador) &"', '0', '0', " & _ 
			" '"& replace(replace(RetornaCasasDecimais(sPrecoProdutoDesconto,2),".",""),",",".") &"', " & _
			" '"& replace(replace(RetornaCasasDecimais(sPrecoProduto,2),".",""),",",".") &"', " & _
			" '"& replace(replace(RetornaCasasDecimais((cdbl(sPrecoProduto) - cdbl(sPrecoProdutoDesconto)) * CDbl(ARYshoppingCart(C_Qtd, iContador)),2),".",""),",",".") &"', " & _
			" '"& replace(replace(sValorEmbrulhoUnitario,".",""),",",".") &"', "& ARYshoppingCart(C_Qtd, iContador) &"," & _ 
			" '', '0', '0', '"& ProLaboreGe &"', '"& IOFGe &"',"& _ 
            "'"& Verifica_Situacao_Produto(ARYshoppingCart(C_ID, iContador)) &"', '"& sCodigoAbacos &"', '"& replace(replace(RetornaCasasDecimais(sTradeInValor, 2),".",""),",",".") &"')"
'	On Error Resume Next

'response.write sSQl
'response.end
    oConn.execute(sSQl)
	
    DescricaoProdutos = (DescricaoProdutos & replace(replace(ARYshoppingCart(C_Titulo, iContador),"'",""),"""","") & ", ")

	if(Err.Number <> "0") then 
		call Envia_Email("edney@ominic.com.br", Application("Mailpedidos"), Application("NomeLoja"), "Erro na pagina pedido_finalizado - linha 412", (sSQl & "<br><br>" & Err.Description))
	end if
	on error goto 0
	
	' RECUPERA ID DO PEDIDO_ITENS ============================================================================
	sSQlI = "Select top 1 id from TB_PEDIDOS_ITENS where Id_Pedido = " & iPedido & " order by id desc"
	Set RsI = oConn.execute(sSQlI)
	if(not RsI.eof) then iPedidoItens 		= RsI("id")
	Set RsI = nothing
	' ========================================================================================================

    IF(ARYshoppingCart(C_GE, iContador) <> "") THEN 
        boolGe = true
    end if

	IF(ARYShoppingcart(C_NomeP,iContador) <> "") THEN 
	    ' INSERIR DADOS DO PRESENTEADO ===================================================
	    sSQlU = "Insert Into TB_PEDIDOS_DADOS_PRESENTEADOS ( " & _ 
										"Id_Pedido, " &_
										"Id_Pedido_Itens, "&_
										"Nome, "&_
										"Email, "&_
										"Mensagem, "&_
										"DataCad)"&_
								"values ("& iPedido &", "& _
										" "& iPedidoItens &", " & _
										" '"& ARYShoppingcart(C_NomeP,iContador) &"', " & _
										" '"& ARYShoppingcart(C_EmailP,iContador) &"', " & _
										" '"& ARYShoppingcart(C_MensagemP,iContador) &"', " & _
										" GetDate()) "
		oConn.execute(sSQlU)
	    ' ================================================================================
	END IF

	' VERIFICA SE EXISTEM PRODUTOS VALE PRESENTE =====================================
	' call Gera_Cupom(iPedido, Session("id"), ARYshoppingCart(C_ID, iContador))
	
	' SUBTRAI ITENS NO ESTOQUE =====================================
	call Subtrai_Estoque(ARYshoppingCart(C_ID, iContador), ARYshoppingCart(C_Qtd, iContador))
	call Subtrai_Estoque_Variacao_Abacos(sCodigoAbacos, ARYshoppingCart(C_Qtd, iContador))

	' ========================================================
	' INTEGRACAO GOOGLE **************************************
	sLinhasCriteoItens = sLinhasCriteoItens & "{'id': """& ARYshoppingCart(C_ID, iContador) &""",  " & _
		                                      " 'price': '"& replace(replace(RetornaCasasDecimais(sPrecoProdutoDesconto,2),".",""),",",".") &"', " & _
		                                      " 'quantity': '"& ARYshoppingCart(C_Qtd, iContador) &"' " & _
		                                      "}," & vbcrlf


    sLinhasGoogleI = sLinhasGoogleI & " ga('ecommerce:addItem', {" & _
                                      "  'id': '"& iPedido &"', " & _
                                      "  'name': '"& ARYshoppingCart(C_Titulo, iContador) &"', " & _
                                      "  'sku': '"& ARYshoppingCart(C_ID, iContador) &"',  " & _
                                      "  'category': '"& trim(Recupera_Campos_Db_oConn("TB_CATEGORIAS", trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingCart(C_ID, iContador), "id", "ID_CATEGORIA")), "id", "CATEGORIA")) &" - "& trim(Recupera_Campos_Db_oConn("TB_SUBCATEGORIA", trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingCart(C_ID, iContador), "id", "ID_SUBCATEGORIA")), "id", "SubCategoria")) &"', " & _
                                      "  'price': '"& replace(replace(RetornaCasasDecimais(sPrecoProdutoDesconto,2),".",""),",",".") &"', " & _
                                      "  'quantity': '"& ARYshoppingCart(C_Qtd, iContador) &"' " & _
                                      "  }); " & vbcrlf

    sLinhasLomadee = sLinhasLomadee & "{" & _
				                            """sku"":"""& ARYshoppingCart(C_ID, iContador) &"""," & _
				                            """name"":"""& ARYshoppingCart(C_Titulo, iContador) &"""," & _
				                            """category"":"""& trim(Recupera_Campos_Db_oConn("TB_CATEGORIAS", trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingCart(C_ID, iContador), "id", "ID_CATEGORIA")), "id", "CATEGORIA")) &" - "& trim(Recupera_Campos_Db_oConn("TB_SUBCATEGORIA", trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingCart(C_ID, iContador), "id", "ID_SUBCATEGORIA")), "id", "SubCategoria")) &"""," & _
				                            """price"":"& replace(replace(RetornaCasasDecimais(sPrecoProdutoDesconto,2),".",""),",",".") &"," & _
				                            """quantity"": "& ARYshoppingCart(C_Qtd, iContador) &"" & _
			                            "},"


	' ========================================================
	
	sSomaItens = (sSomaItens + (cdbl(ARYshoppingCart(C_Qtd, iContador)) * cdbl(ARYshoppingCart(C_Preco, iContador))))

	If(ARYshoppingCart(C_ID, iContador) = "493374" OR ARYshoppingCart(C_ID, iContador) = "493375") Then ProdutolDol4 = ARYshoppingCart(C_ID, iContador)

    sLinhasProxyIds = (sLinhasProxyIds & ARYshoppingCart(C_ID, iContador) &",")

    sLinhasGoogleAvanc4 = sLinhasGoogleAvanc4 & "{'name': '"& ARYshoppingCart(C_Titulo, iContador) &"','id': '"& ARYshoppingCart(C_ID, iContador) &"', 'price': '"& replace(replace(RetornaCasasDecimais(sPrecoProdutoDesconto,2),".",""),",",".") &"', 'brand': '"& trim(Recupera_Campos_Db_oConn("TB_FABRICANTE", trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYShoppingcart(C_ID, iContador), "id", "Id_fabricante")), "id", "FABRICANTE")) &"', 'category': '"& trim(Recupera_Campos_Db_oConn("TB_CATEGORIAS", trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingCart(C_ID, iContador), "id", "ID_CATEGORIA")), "id", "CATEGORIA")) &" - "& trim(Recupera_Campos_Db_oConn("TB_SUBCATEGORIA", trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYshoppingCart(C_ID, iContador), "id", "ID_SUBCATEGORIA")), "id", "SubCategoria")) &"', 'variant': '', 'quantity': "& ARYshoppingCart(C_Qtd, iContador) &", 'coupon': ''},"

    'response.write sLinhasGoogleAvanc4
Next

'response.End
    
' sLinhasGoogleI  = sLinhasGoogleI & "['_trackTrans']" & vbcrlf
sSomaItens      = RetornaCasasDecimais(cdbl(sSomaItens),2)

If(sLinhasGoogleAvanc4 <> "") Then sLinhasGoogleAvanc4 = left(sLinhasGoogleAvanc4, Len(sLinhasGoogleAvanc4)-1)
If(sLinhasCriteoItens <> "") Then sLinhasCriteoItens = left(sLinhasCriteoItens, Len(sLinhasCriteoItens)-1)


	
' ====================================================================================
' Verifica TRADE-IN
if(boolTradeIn = true) then 
    if(TradeInMarca <> "" and TradeInModelo <> "" and TradeInImei <> "" and TradeInSituacao <> "") then 

        Select case lcase(TradeInSituacao)
            case "r"
                sValorDescontoTradeIn = FormatNumber(cdbl(sTradeInValor)/3,2)
            case "b"
                sValorDescontoTradeIn = FormatNumber(cdbl(sTradeInValor)/2,2)
            case "o"
                sValorDescontoTradeIn = FormatNumber(cdbl(sTradeInValor)/1,2)
        end select

        sValorDescontoTradeIn = (cdbl(TotalCompra) * 0.10)

        sTotalTradeIn       = FormatNumber(cdbl(TotalCompra)  - cdbl(sValorDescontoTradeIn),2)
        sSubTotalTradeIn    = FormatNumber(cdbl(TotalCompra)  - cdbl(sValorDescontoTradeIn),2)
        sParcelaTradeIn     = sTotalTradeIn

        sSQlU = "UPDATE TB_PEDIDOS SET   TOTAL          = '"& replace(replace(sTotalTradeIn,".",""),",",".")        &"',                " & _
                "                        SUBTOTAL       = '"& replace(replace(sSubTotalTradeIn,".",""),",",".")     &"',                " & _
                "                        ValorParcela   = '"& replace(replace(sParcelaTradeIn,".",""),",",".")      &"',                " & _
                "                        DESCONTOPRO    = (DESCONTOPRO + "& replace(replace(sValorDescontoTradeIn,".",""),",",".") &"), " & _
                "                        TradeInMarca   = '"& TradeInMarca &"',                                                         " & _
                "                        TradeInModelo  = '"& TradeInModelo &"',                                                        " & _
                "                        TradeInImei    = '"& TradeInImei &"',                                                          " & _
                "                        TradeInSituacao= '"& TradeInSituacao &"',                                                       " & _
                "                        TradeInDesconto= '"& replace(replace(sValorDescontoTradeIn,".",""),",",".") &"'                " & _
    
                " WHERE ID = " & iPedido
        oConn.execute(sSQlU)
    end if
end if

call Abre_Conexao()


if(idFrmPagamento <> "22") then 
    ' Configura XML a ser enviado ao MoIP
    ' ---------------------------------------------------------------------------------------
    On Error Resume Next

    Dim xmlhttp, moip_key, xmlDoc, nodeErro, nodeStatus, moipStatus, moipErro, nodeId, nodeToken, moipId, moipToken

    Set xmlhttp = Server.CreateObject("MSXML2.ServerXMLHTTP.6.0")
	' Set xmlhttp = Server.CreateObject("MSXML2.XMLHTTP")

    sSqlClienteCompra = "Select TOP 1 * from TB_CLIENTES (nolock) where id = "& Session("Id")
    set ClienteCompra = oConn.execute(sSqlClienteCompra)

    if (not ClienteCompra.eof) then
        TelefoneFixo = trim(ClienteCompra("telefone"))

        if(TelefoneFixo = "" or isnull(TelefoneFixo)) then 
            TelefoneFixo = trim(ClienteCompra("celular"))

            if(TelefoneFixo = "" or isnull(TelefoneFixo)) then TelefoneFixo = "1135227545"
        end if

        xml = "<EnviarInstrucao><InstrucaoUnica TipoValidacao=""Transparente""><Razao>"& moip_loja &"</Razao><Valores><Valor Moeda=""BRL"">"& replace(replace(FormatNumber(TotalCompra,2),".",""),",",".") &"</Valor></Valores><IdProprio>"& iPedido &"</IdProprio>"

        if(idFrmPagamento <> "85" and idFrmPagamento <> "86" and idFrmPagamento <> "87" and idFrmPagamento <> "88") then
            xml = xml & "<Parcelamentos><Parcelamento><MinimoParcelas>2</MinimoParcelas><MaximoParcelas>10</MaximoParcelas><Juros>0</Juros></Parcelamento></Parcelamentos>"
        else
            xml = xml & "<Parcelamentos><Parcelamento><MinimoParcelas>2</MinimoParcelas><MaximoParcelas>10</MaximoParcelas><Recebimento>Parcelado</Recebimento><Juros>0</Juros></Parcelamento></Parcelamentos>"
        end if

        xml = xml & "<Recebedor><LoginMoIP>"& moip_login &"</LoginMoIP><Apelido>"& moip_loja &"</Apelido></Recebedor><Pagador><Nome>"& TirarAcento(trim(ClienteCompra("nome"))) &"</Nome><Email>"& TirarAcento(trim(ClienteCompra("email"))) &"</Email><IdPagador>"& TirarAcento(trim(ClienteCompra("cpf"))) &"</IdPagador><EnderecoCobranca><Logradouro>"& TirarAcento(trim(ClienteCompra("endereco"))) &"</Logradouro><Numero>"& TirarAcento(trim(ClienteCompra("numero"))) &"</Numero><Complemento>"& TirarAcento(trim(ClienteCompra("complemento"))) &"</Complemento><Bairro>"& TirarAcento(trim(ClienteCompra("bairro"))) &"</Bairro><Cidade>"& TirarAcento(trim(ClienteCompra("cidade"))) &"</Cidade><Estado>"& TirarAcento(trim(Recupera_Campos_Db_oConn("TB_ESTADOS", trim(ClienteCompra("Id_Provincia")), "id", "SIGLA"))) &"</Estado><Pais>BRA</Pais><CEP>"& TirarAcento(trim(ClienteCompra("cep"))) &"</CEP><TelefoneFixo>"& TirarAcento(TelefoneFixo) &"</TelefoneFixo></EnderecoCobranca></Pagador><FormasPagamento><FormaPagamento>CartaoCredito</FormaPagamento></FormasPagamento></InstrucaoUnica></EnviarInstrucao>"
    end if
    set ClienteCompra = nothing

    moip_key        = Base64Encode(moip_token &":"& moip_chave)

	sSQlI = "UPDATE TB_PEDIDOS SET RetornoBcash = '"& xml &"' where ID = " & iPedido
	oConn.execute(sSQlI)

    'response.write xml
    'response.End
    'response.write "moip_url_auth: " & moip_url_auth & "<br>"
    'response.write "moip_key:" & moip_key & "<br>"

    Call xmlhttp.Open("POST", moip_url_auth, false)
    Call xmlhttp.SetRequestHeader("Authorization", "Basic "& moip_key)
    Call xmlhttp.Send(xml)

    'response.write xmlhttp.responseText
    'response.End
    
    ' Set xmlDoc = Server.CreateObject("MSXML2.DOMDocument")
    Set xmlDoc = Server.CreateObject("Microsoft.XMLDOM")
    xmlDoc.LoadXml(xmlhttp.ResponseXml.xml)

    Set nodeErro    = xmlDoc.documentElement.SelectSingleNode("Resposta")
    Set nodeStatus  = xmlDoc.documentElement.SelectSingleNode("Resposta/Status")

    moipStatus  = UCase(nodeStatus.Text)
    moipErro    = UCase(nodeErro.xml)

    'response.write "moipErro:" & Server.HTMLEncode(moipErro) & "<br>"
    'response.End

    If ( moipStatus = "SUCESSO" ) Then
        Set nodeId      = xmlDoc.documentElement.SelectSingleNode("Resposta/ID")
        Set nodeToken   = xmlDoc.documentElement.SelectSingleNode("Resposta/Token")

        moipId      = nodeId.Text
        moipToken   = nodeToken.Text
        moipErro    = ""
        erro        = False

        sSQlI = "UPDATE TB_PEDIDOS SET RetornoIntegracao = '"& moipToken &"' where ID = " & iPedido
        oConn.execute(sSQlI)
	Else
        url             = ""
        'moipStatus    = "FALHA"
        moipToken       = "N/A"
        moipId          = 0
        moipErro        = UCase(nodeErro.xml)
        returnMessage   = "Falha ao buscar URL MoIP"
        erro = True

		' ATUALIZA PEDIDOS ----------------------------------------------
		sSQl = "UPDATE TB_PEDIDOS SET ID_STATUS = 3, RetornoIntegracao = '"& moipToken &"' WHERE ID = " & iPedido
		oConn.execute(sSQl)
		' ---------------------------------------------------------------

		' INSERI LINHA NA TABELA HISTORICO ------------------------------
		sSQlI = "INSERT INTO TB_HISTORICOS (Id_Pedido, Id_Status, Descricao, Documento) VALUES ('"& iPedido &"', 3, '"& TRIM(Recupera_Campos_Db_oConn("TB_STATUS", "3", "id", "DESCRICAO")) &" [Moip - Erro]', '"& moipErro & "') "
		oConn.execute(sSQlI)
		' ----------------------------------------------------------------

        'ABRE IFRAME PARA TEMPLANTES DE EMAIL -----------------------------
        response.write "<iframe src=""https://integracao.foodcrowd.com.br/TemplateLojas/template.asp?id="& iPedido &"&status=5"" name=""templante"" width=""1"" marginwidth=""0"" height=""1"" marginheight=""0"" scrolling=""No"" frameborder=""0"" hspace=""0"" vspace=""0"" id=""templante"" style=""margin:0px;""></iframe>"
    End If

    dim boolTokenMoipInvalid
    boolTokenMoipInvalid = (moipToken = "" or moipToken = "N/A" or len(moipToken) < 20)

    ' **********************************************
    Tempo = Now
    While DateDiff("s", Tempo, Now) < 7
    Wend
    ' **********************************************

	if(Err.number <> 0) then 
        call Envia_Email("edney@ominic.com.br", "sistema01@ominic.com.br", "sistema01@ominic.com.br - Erro MOIP", "XML - MOIP", Server.HTMLEncode(xml) & "<BR><BR>" & Err.Description & "<BR><BR>" & Server.HTMLEncode(moipErro) & "<BR><BR>" & xmlhttp.ResponseXml.xml)
    end if

    On Error Goto 0
else
    ' **********************************************
    Tempo = Now
    While DateDiff("s", Tempo, Now) < 3
    Wend
    ' **********************************************

    if(CupomDescontoValidado = false) then
	    'sSQlU = "exec dbo.sp_Inconsistencia 'P', " & iPedido
	    'oConn.execute(sSQlU)
    end if
	
	' MANDA DIRETO PARA A ERP =====================================================================
    'response.write "<iframe src=""https://integracao.foodcrowd.com.br/moip/fun_ErpFlex.asp?pedido="& iPedido &"&chave="& sCdCliente &""" name=""ERP"" width=""1"" marginwidth=""0"" height=""1"" marginheight=""0"" scrolling=""No"" frameborder=""0"" hspace=""0"" vspace=""0"" id=""templante"" style=""margin:0px;""></iframe>"	
end if

%>


<!DOCTYPE html>
<html lang="en" dir="ltr">

    <!--#include virtual= "head.asp"-->

    <link rel="stylesheet" href="/css/styleCarrinho.css">

    <style>
        h1, h2, h3, h4, h5, h6 {
            margin: 0 0 5px;
            line-height: 2.35;
            color: #cc6633;
        }
    </style>

<body>
    <!--#include virtual= "header.asp"-->

        <div class=" desktop-margin-top-90" >
	        <div class="margin-60-0 ">
		        <div class="container">
			        <div class="row desktop-margin-90 ">
				        <div class="col-sm-12">
					        <div class="row">
						        <div class="col-sm-12">
                                    <h2 class="cor-4 semi-bold ">
                                        <span class="shape6">Recebemos o seu pedido #<%= iPedido %>.
                                            <br />A Foodcrowd te deseja uma experiência incrível!
                                        </span>
                                    </h2>
						        </div>  

					                <p class="texto-mensagem" style="    margin: 0 0 12px 16px;">
                                        Você receberá em breve um e-mail com sua confirmação de compra.
					                    <br /><br />Em caso de duvida, por favor entre em contato com <a href="mailto:tendimento@foodcrowd.com.br">atendimento@foodcrowd.com.br</a>
                                    </p>

			                        <% if(idFrmPagamento <> "22") then %>
                                        <%if(idFrmPagamento <> "80" and not boolTokenMoipInvalid) then%>
                                            <div id="MoipWidget"
                                                    data-token="<%= moipToken %>"
                                                    callback-method-success="funcaoSucesso"
                                                    callback-method-error="funcaoFalha">
                                            </div>
                        
                                            <p class="texto-mensagem">
                                                <% if(idFrmPagamento <> "85" and idFrmPagamento <> "86" and idFrmPagamento <> "87" and idFrmPagamento <> "88") then  %>
                                                    Código MOIP é: <span id="divcom" style="color:red"><b>Por favor aguarde, pagamento em andamento (10 s)...</b></span><br />
                                                    Status da transação MOIP é: <span id="divstatus" style="color:red"><b>Por favor aguarde, pagamento em andamento (10 s)...</b></span><br />
                                                    <span style="text-align:left"><br /><img src="/img/compraSeguroMoip.JPG" alt="Compra Seguro MOIP" /></span>
                                                <% else %>
                                                    <B>Desabilite sua ferramenta de “anti-pop up” no menu configurações.</B><br /><br />
                                                    Por favor, clique logo [MOIP] abaixo para dar andamento no seu pagamento:<br />
                                                    <span style="float: none;margin-left: 63px;"><br /><span id="divdebito"></span></span>
                                                <%end if%>
                                            </p>

                                        <%end if%>
			                        <%end if%>
    					


                                <%if(idFrmPagamento = "22") then%>
					                <p class="botoes-compra" style="text-align:center">
						                <a href="https://admin.foodcrowd.com.br/EcommerceNew/faturar_pedidos_boleto.asp?id=<%= iPedido %>&chave=<%= Session("Chave") %>" target="_blank"><img src="/img/botao-imprimir-boleto.png" style="width: 105px;    height: auto;    text-align: left;   float: left;    margin: 14px;"></a>
					                </p>

                                    <script type="text/javascript">
                                        window.open('https://admin.foodcrowd.com.br/EcommerceNew/faturar_pedidos_boleto.asp?id=<%= iPedido %>&chave=<%= Session("Chave") %>', '_blank');
                                    </script> 

                                <%end if%>


                                <%if( boolTokenMoipInvalid and (idFrmPagamento <> "22" and idFrmPagamento <> "85" and idFrmPagamento <> "86" and idFrmPagamento <> "87" and idFrmPagamento <> "88") ) then%>

                                    <script type="text/javascript">
                                        function EnviarPag() {
                                            document.forms["formpg"].submit();
                                        }
                                    </script>

                                    <form target="moip" method="post" action="https://www.moip.com.br/PagamentoMoIP.do" name="formpg" id="formpg" >
                                        <input type="hidden" name="id_carteira" value="<%= moip_login %>">
                                        <input type="hidden" name="valor" value="<%= replace(replace(FormatNumber(Session("TotalCompra"),2),".",""),",","") %>">
                                        <input type="hidden" name="nome" value="<%= moip_loja %>">

                                        <input type="hidden" name="descricao"               value="<%= trim(DescricaoProdutos) %>">
                                        <input type="hidden" name="id_transacao"            value="<%= iPedido %>">

                                        <!-- INÍCIO DOS DADOS DO USUÁRIO -->
                                        <input type="hidden" name="frete"                   value="0">
                                        <input type="hidden" name="pagador_nome"            value="<%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", Session("Id"), "id", "nome")) %>">
                                        <input type="hidden" name="pagador_cpf"             value="<%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", Session("Id"), "id", "cpf")) %>">
                                        <input type="hidden" name="pagador_cep"             value="<%= replace(trim(Recupera_Campos_Db_oConn("TB_CLIENTES", Session("Id"), "id", "cep")),"-","") %>">
                                        <input type="hidden" name="pagador_logradouro"      value="<%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", Session("Id"), "id", "endereco")) %>">
                                        <input type="hidden" name="pagador_numero"          value="<%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", Session("Id"), "id", "numero")) %>">
                                        <input type="hidden" name="pagador_complemento"     value="<%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", Session("Id"), "id", "complemento")) %>">
                                        <input type="hidden" name="pagador_bairro"          value="<%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", Session("Id"), "id", "bairro")) %>">
                                        <input type="hidden" name="pagador_cidade"          value="<%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", Session("Id"), "id", "cidade")) %>">
                                        <input type="hidden" name="pagador_estado"          value="<%= trim(Recupera_Campos_Db_oConn("TB_ESTADOS", trim(Recupera_Campos_Db_oConn("TB_CLIENTES", Session("Id"), "id", "Id_Provincia")), "id", "SIGLA")) %>">
                                        <input type="hidden" name="pagador_pais"            value="Brasil">
                                        <input type="hidden" name="pagador_telefone"        value="<%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", Session("Id"), "id", "telefone")) %>">
                                        <input type="hidden" name="pagador_email"           value="<%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", Session("Id"), "id", "email")) %>">
                                        <!-- FIM DOS DADOS DO USUÁRIO -->
                                    </form>

                                    <p>&nbsp;</p>    

                                    <p style="text-align: center;">
                                        <br />
                                        <a href="/faturar_pedidos_moip.asp?id=<%= iPedido %>" target="_blank"><img src="/img/moip2.PNG" alt="Moip" border="0" /></a>
                                    </p>

                                <% else %>

                                        <%if((idFrmPagamento <> "22" and idFrmPagamento <> "80")) then 

                                                if(idFrmPagamento <> "85" and idFrmPagamento <> "86" and idFrmPagamento <> "87" and idFrmPagamento <> "88") then 
                                                    Cartao_Validade = left(replace(Cartao_Validade,"/",""), 2) & "/" & Right(replace(Cartao_Validade,"/",""), 2)
                                                end if

                                                telefone        = trim(Recupera_Campos_Db_oConn("TB_CLIENTES", Session("Id"), "id", "telefone"))
                                                Nascimento      = trim(Recupera_Campos_Db_oConn("TB_CLIENTES", Session("Id"), "id", "DtNascimento"))
                                                Cpf             = trim(Recupera_Campos_Db_oConn("TB_CLIENTES", Session("Id"), "id", "CPF"))

                                                if(telefone = "" or isnull(telefone)) then 
                                                    telefone = trim(Recupera_Campos_Db_oConn("TB_CLIENTES", Session("Id"), "id", "celular"))

                                                    if(telefone = "" or isnull(telefone)) then telefone = "1135227545"
                                                end if

                                                if(Parcelamento = "0") then Parcelamento = "1"
                                            %>

                                            <script type="text/javascript" src="/js/jquery-1.12.4.js"></script>
                                            <script type='text/javascript' src='https://www.moip.com.br/transparente/MoipWidget-v2.js' charset=ISO-8859-1"></script>

                                            <script type="text/javascript">

                                                var funcaoSucesso = function (data) {
                                                    $.ajax({
                                                        url: "gravar_json_pedido.asp",
                                                        data: { pedido: '<%= iPedido %>', dados: JSON.stringify(data) },
                                                        type: "post",
                                                        success: function () {
                                                            var DadosSeparados  = JSON.parse(JSON.stringify(data));
                                                            var CodigoMoIP      = DadosSeparados.CodigoMoIP;
                                                            var Status          = DadosSeparados.Status;

                                                            if (CodigoMoIP == '' && Status == '') {
                                                                CodigoMoIP  = '';
                                                                Status      = data;
                                                            }

                                                            $("#divcom").html(CodigoMoIP);
                                                            $("#divstatus").html(Status);

                                                            <% if(idFrmPagamento = "85" OR idFrmPagamento = "86" OR idFrmPagamento = "87" OR idFrmPagamento = "88") then %>
                                                                $("#divdebito").html("<a href='" + DadosSeparados.url + "' target='_blank'><img src='/img/compraSeguroMoip.JPG' alt='Compra Seguro MOIP' /></a>");
                                                                window.open(DadosSeparados.url);
                                                            <% end if %>
                                                        }
                                                    });
                                                };

                                                var funcaoFalha = function (data) {
                                                    $.ajax({
                                                        url: "gravar_json_pedido.asp",
                                                        data: { pedido: '<%= iPedido %>', dados: JSON.stringify(data) },
                                                        type: "post",
                                                        success: function () {
                                                            var DadosSeparados          = JSON.parse(JSON.stringify(data));
                                                            var sTextoErroMostrarCod    = '';
                                                            var sTextoErroMostrar       = '<br>';

                                                            DadosSeparados.forEach(function (o, index) {
                                                                sTextoErroMostrarCod = (sTextoErroMostrarCod + o.Codigo + ', ');
                                                                sTextoErroMostrar = (sTextoErroMostrar + o.Mensagem + '<br>');
                                                            });

                                                            $("#divcom").html(sTextoErroMostrarCod);
                                                            $("#divstatus").html(sTextoErroMostrar);

                                                            <% if(idFrmPagamento = "85" OR idFrmPagamento = "86" OR idFrmPagamento = "87" OR idFrmPagamento = "88") then %>
                                                                $("#divdebito").html(DadosSeparados.url);
                                                            <% end if %>
                                                        }
                                                    });
                                                };

                                                pagarPedido = function () {
                                                    <% if(idFrmPagamento <> "85" and idFrmPagamento <> "86" and idFrmPagamento <> "87" and idFrmPagamento <> "88") then  %>
                                                            var settings = {
                                                                "Forma": "CartaoCredito",
                                                                "Instituicao": "<%= DescFrmPagamento %>",
                                                                "Parcelas": "<%= Parcelamento %>",
                                                                "CartaoCredito": {
                                                                    "Numero": "<%= Cartao_Numero %>",
                                                                    "Expiracao": "<%= Cartao_Validade %>",
                                                                    "CodigoSeguranca": "<%= Cartao_Codigo %>",
                                                                    "Portador": {
                                                                        "Nome": "<%= Cartao_Nome %>",
                                                                        "DataNascimento": "<%= Cartao_dn %>",
                                                                        "Telefone": "<%= Cartao_tel %> ",
                                                                        "Identidade": "<%= Cartao_CPF %>"
                                                                    }
                                                                }
                                                            }
                                                        <% else %>

                                                            var settings = {
                                                                "Forma": "DebitoBancario",
                                                                "Instituicao": "<%= DescFrmPagamentoJP %>"
                                                            }
                                                        <% end if %>
                                                    MoipWidget(settings);
                                                }

                                            </script>
                                        <%end if%>

                                <% end if %>

					        </div>	
				        </div>
			        </div>
		        </div>
	        </div>
        </div>

    <!--#include virtual= "footer.asp"-->


    <% if( (idFrmPagamento = "80") OR boolTokenMoipInvalid) then%>
        <script type="text/javascript">EnviarPag()</script>   
    <% elseif(idFrmPagamento <> "80" and idFrmPagamento <> "22") then%>
        <script type="text/javascript">pagarPedido();</script>   
    <% end if%>
    
    <% 
    'ABRE IFRAME PARA TEMPLANTES DE EMAIL -----------------------------
    response.write "<iframe src=""https://integracao.foodcrowd.com.br/TemplateLojas/template.asp?id="& iPedido &"&status=1"" name=""templante"" width=""1"" marginwidth=""0"" height=""1"" marginheight=""0"" scrolling=""No"" frameborder=""0"" hspace=""0"" vspace=""0"" id=""templante"" style=""margin:0px;""></iframe>"
    %>


    <script type="text/javascript">
        function gtag_report_conversion(url) {
          var callback = function () {
            if (typeof(url) != 'undefined') {
              window.location = url;
            }
          };
          gtag('event', 'conversion', {
              'send_to': 'AW-772837804/fqNzCI6qkZMBEKyjwvAC',
              'transaction_id': '<%= iPedido %>',
              'event_callback': callback
          });
          return false;
        }
    </script>

</body>
</html>


<%
' ====================================================================================
Function Base64Encode(inData)
    'rfc1521
    '2001 Antonin Foller, PSTRUH Software, http://pstruh.cz
    Const Base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

    Dim cOut, sOut, I

    'For each group of 3 bytes
    For I = 1 To Len(inData) Step 3
        Dim nGroup, pOut, sGroup
        'Create one long from this 3 bytes.
        nGroup = &H10000 * Asc(Mid(inData, I, 1)) + _
        &H100 * MyASC(Mid(inData, I + 1, 1)) + MyASC(Mid(inData,I + 2, 1))
        'Oct splits the long to 8 groups with 3 bits
        nGroup = Oct(nGroup)
        'Add leading zeros
        nGroup = String(8 - Len(nGroup), "0") & nGroup
        'Convert to base64
        pOut = Mid(Base64, CLng("&o" & Mid(nGroup, 1, 2)) + 1, 1) + _
        Mid(Base64, CLng("&o" & Mid(nGroup, 3, 2)) + 1, 1) + _
        Mid(Base64, CLng("&o" & Mid(nGroup, 5, 2)) + 1, 1) + _
        Mid(Base64, CLng("&o" & Mid(nGroup, 7, 2)) + 1, 1)
        'Add the part to output string
        sOut = sOut + pOut
        'Add a new line for each 76 chars in dest (76*3/4 = 57)
        If (I + 2) Mod 57 = 0 Then sOut = sOut '+ vbCrLf
    Next

    Select Case Len(inData) Mod 3
        Case 1: '8 bit final
            sOut = Left(sOut, Len(sOut) - 2) + "=="
        Case 2: '16 bit final
            sOut = Left(sOut, Len(sOut) - 1) + "="
    End Select

    Base64Encode = sOut
End Function

' ====================================================================================
Function MyASC(OneChar)
    If OneChar = "" Then MyASC = 0 Else MyASC = Asc(OneChar)
End Function

call Fecha_Conexao()
    

if(TRUE) then 
    Session("TotalCompra") 		= 0
    Session("sMontaTotal") 		= 0
    Session("SubTotalCompra") 	= 0
    Session("ItemCount") 		= 0
    iCount 						= 0
    Session("ItemCount") 		= iCount
end if

' ========================================================================
' Session.Abandon ' ENCERRA AS SESSIOES DO SITE PARA UM NOVO PEDIDO
' ========================================================================

'ReDim ARYShoppingCart(ShoppingCartAttributes, MaxShoppingCartItems)
'Session("MyShoppingCart") = ARYShoppingCart
'Response.Clear
%>