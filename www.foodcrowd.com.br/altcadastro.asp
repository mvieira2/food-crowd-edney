﻿<!--#include virtual= "/hidden/funcoes.asp"-->

<%
if(Session("Id") = "") then response.redirect("/login.asp?ReturnUrl=arearestrita")

' VARIAVEIS ================================
Dim boolenviado, boolGravado, sIdCateg, sOnload, i
dim sBannerPagina

boolenviado = (request("enviado") = "ok")
boolGravado = false

if(boolenviado) then call Gravar()

'sOnload = " onload = ""javascript: MostraDepA(); """
if Session("Id") <> "" then
	Call abre_conexao()
	if(Recupera_Campos_Db_oConn("TB_CLIENTES", Session("Id"), "id", "TipoPessoa") = "F") then
		sOnload = " onload = ""javascript: MostraDep('F'); """
	else
		sOnload = " onload = ""javascript: MostraDep('J'); """
	end if
	Call fecha_conexao()
else
	response.write "ERRO SISTEMA!"
	response.end
end if


' ==========================================
sub Gravar()
    'response.End

    dim sSql, sTipoNews, sCorpo_Msg
	dim sCampo(40), sData
    
	call Abre_Conexao()

	sCampo(0)       = 2
	sCampo(1)       = Replace_Caracteres_Especiais(replace(trim(request("senha")),"'",""))
	sCampo(2)       = Replace_Caracteres_Especiais(replace(trim(request("email")),"'",""))
	sCampo(4)       = Replace_Caracteres_Especiais(replace(trim(request("sobrenome")),"'",""))
	sCampo(5)       = ("'" & Replace_Caracteres_Especiais(trim(request("dia"))) & "/" & Replace_Caracteres_Especiais(trim(request("mes"))) & "/" & Replace_Caracteres_Especiais(trim(request("ano"))) & " 00:00:00'")
	sCampo(6)       = Replace_Caracteres_Especiais(replace(trim(request("Sexo")),"'",""))
	sCampo(7)       = Replace_Caracteres_Especiais(replace(trim(request("rg")),"'",""))
	sCampo(8)       = Replace_Caracteres_Especiais(replace(trim(request("empresa")),"'",""))
	sCampo(9)       = Replace_Caracteres_Especiais(replace(trim(request("empresa_tipo")),"'",""))
	sCampo(10)      = Replace_Caracteres_Especiais(replace(trim(request("forma_vendas_lojistas")),"'",""))
	sCampo(11)      = Replace_Caracteres_Especiais(replace(trim(request("internet_link")),"'",""))
	sCampo(12)      = Replace_Caracteres_Especiais(replace(trim(request("outros_especificar")),"'",""))
	sCampo(13)      = Replace_Caracteres_Especiais(replace(trim(request("cepc")),"'",""))
	sCampo(14)      = Replace_Caracteres_Especiais(replace(trim(request("cidade")),"'",""))
	sCampo(15)      = Replace_Caracteres_Especiais(replace(trim(request("provincia")),"'",""))
	sCampo(16)      = Replace_Caracteres_Especiais(replace(trim(request("bairroh")),"'",""))
	sCampo(17)      = Replace_Caracteres_Especiais(replace(trim(request("numeroc")),"'",""))
	sCampo(18)      = Replace_Caracteres_Especiais(replace(trim(request("complemento")),"'",""))
	sCampo(19)      = "BR"
	sCampo(20)      = Replace_Caracteres_Especiais(replace(trim(request("japao")),"'",""))
	sCampo(21)      = Replace_Caracteres_Especiais(replace(trim(request("tcomercial")),"'",""))
	sCampo(22)      = Replace_Caracteres_Especiais(replace(trim(request("tfax")),"'",""))
	sCampo(23)      = Replace_Caracteres_Especiais(replace(trim(request("tcelular")),"'",""))
	sCampo(24)      = Replace_Caracteres_Especiais(replace(trim(request("newsletter")),"'",""))
	sCampo(26)      = Replace_Caracteres_Especiais(replace(trim(request("nacionalidade")),"'",""))

	sCampo(27)      = Replace_Caracteres_Especiais(replace(trim(request("NomeMae")),"'",""))
	sCampo(28)      = Replace_Caracteres_Especiais(replace(trim(request("CPF")),"'",""))
	
	sCampo(29)      = Replace_Caracteres_Especiais(replace(trim(request("TipoPessoa")),"'",""))
	sCampo(30)      = Replace_Caracteres_Especiais(replace(trim(request("TipoEndereco")),"'",""))
	sCampo(31)      = Replace_Caracteres_Especiais(replace(trim(request("Enderecoh")),"'",""))
	
	sCampo(32)      = Replace_Caracteres_Especiais(replace(trim(request("InscEstadual")),"'",""))
	sCampo(33)      = Replace_Caracteres_Especiais(replace(trim(request("CNPJ")),"'",""))

    if(boolpre) then 
        sCampo(29) = "F"
        sCampo(3) = Replace_Caracteres_Especiais(replace(trim(request("nome")),"'",""))
    else
	    if(lcase(sCampo(29)) = "f" ) then
            sCampo(3) = Replace_Caracteres_Especiais(replace(trim(request("nome")),"'",""))
		    call Validacao_Lado_Servidor(sCampo(28),"CPF")
	    else
		    sCampo(3) = Replace_Caracteres_Especiais(replace(trim(request("nomeR")),"'",""))
		    call Validacao_Lado_Servidor(sCampo(28),"CNPJ")
	    end if
    end if

	if(sCampo(26) = "esp") then sCampo(26) = Replace_Caracteres_Especiais(replace(trim(request("Nacionalidade_especificar")),"'",""))
	if(sCampo(15) = "") then sCampo(15) = "null"
    if(sCampo(15) = "") then sCampo(15) = "null"

	' GRAVA O EMAIL NA TABELA DE NEWSLETTER
	if((sCampo(24) <> "") and (sCampo(2) <> "")) then
	    sSql = "Select top 1 descricao from TB_NEWSLETTER_EMAILS where descricao = '"& sCampo(2) &"' "
	    set Rs = oConn.execute(sSql)
		if(Rs.eof) then oConn.execute("Insert into TB_NEWSLETTER_EMAILS (Id_Tipo_Newsletter, Nome, Descricao, Ativo) values ("& sCampo(24) &", '"& sCampo(3) &"', '"& sCampo(2) &"', 1)")
	    set Rs = nothing
	end if

	if(sCampo(20) = "1") then sCampo(19) = "Japao"
	if(sCampo(10) <> "Pela Internet / Endereco/ Link") then sCampo(11) = ""

	if(sCampo(31) = "") then sCampo(31) = Replace_Caracteres_Especiais(replace(trim(request("EnderecoC")),"'",""))
    if(sCampo(16) = "") then sCampo(16) = Replace_Caracteres_Especiais(replace(trim(request("BairroC")),"'",""))
	
	sSql = "Select top 1 id, datacad from TB_CLIENTES (NOLOCK) where cdcliente = "& sCdCliente &" and Cpf = '"& sCampo(28) &"'  and id <> " & Session("Id")   

    if(sCampo(29) <> "F") then
	    sSql = "Select top 1 id, datacad from TB_CLIENTES (NOLOCK) where cdcliente = "& sCdCliente &" and Cnpj = '"& sCampo(33) &"'  and id <> " & Session("Id")  
	end if

    if(boolpre) then
	    sSql = "Select top 1 id, datacad from TB_CLIENTES (NOLOCK) where cdcliente = "& sCdCliente &" and email = '"& sCampo(2) &"'  and id <> " & Session("Id")  
    end if

    set Rs = oConn.execute(sSql)

    if(not IsNumeric(sCampo(15)) ) then 
		sSQl = "Select top 1 ID from TB_ESTADOS WITH (nolock)  WHERE upper(DESCRICAO) = '"& ucase(Replace_Caracteres_Especiais(replace(trim(request("provinciaLegenda")),"'",""))) &"'"
		set RsC = oConn.execute(sSQl)
		if(not RsC.eof) then sCampo(15) = RsC("ID")
		set RsC = nothing

        sCampo(14) = Replace_Caracteres_Especiais(replace(trim(request("cidade")),"'",""))
    end if

	' TRAVA CONTRA HACKER SAFE ---------------------------
	if((not IsNumeric(sCdCliente)) OR not IsNumeric(sCampo(0))) then
		response.write "ERRO NO SISTEMA! (1)"
		response.end
	end if
	if(sCampo(28) <> "") then
		if(not IsNumeric(sCampo(28))) then
			response.write "ERRO NO SISTEMA! (2)"
			response.end
		end if
	end if
	
    if(rs.eof) then
		sSql = "Update TB_CLIENTES set Id_Provincia = "& sCampo(15) &", Nome = '"& sCampo(3) &"', " & _
			"DtNascimento = "& sCampo(5) &", Sexo = '"& sCampo(6) &"', " & _
			"Cep = '"& sCampo(13) &"', Endereco = '"& sCampo(31) &"', Cidade = '"& sCampo(14) &"', Bairro = '"& sCampo(16) &"', Numero = '"& sCampo(17) &"', " & _
			"Complemento = '"& sCampo(18) &"', obs = '"& sCampo(35) &"', Pais = '"& sCampo(19) &"', Telefone = '"& sCampo(21) &"', " & _
			"Celular = '"& sCampo(23) &"', Email = '"& sCampo(2) &"', " & _
			" CPF = '"& sCampo(28) &"', CNPJ = '"& sCampo(33) &"', InscEstadual = '"& sCampo(34) &"', TipoPessoa = '"& sCampo(29) &"', TipoEndereco = 'R', DataAlt = getDate() " & _
			" where id = " & Session("Id")

'Response.Write sSql
'Response.End

		oConn.execute(sSql)

		boolGravado = true
		sMsgJs      = "Operação Concluída!"
	else
		sMsgJs      = "Registro Existente!"
        boolGravado = true
	end if
    set Rs = nothing

	call Fecha_Conexao()
end sub

%>


<!DOCTYPE html>
<html lang="en" dir="ltr">
    <!--#include virtual= "head.asp"-->

    <script type="text/javascript" src="/js/cep_ajax.js"></script>
    <script type="text/javascript" src="/js/ajax.js"></script>

    <script language="JavaScript" type="text/JavaScript">
            <!--
            <%if(boolGravado) then%>
                alert('<%= sMsgJs %>');

                <% if(Session("ItemCount") > 0) then %>
                    location.href='/pagamento';
                <%else%>
                    location.href='/arearestrita';
	            <%end if%>  
            <%end if%>

            $(document).ready(function() {
                $("#finalizar_final").click(function () {
                    return validaCad();
                });

            });

            function ValidarNumero(obj) {
                // var expressao = /^[0-9]/;
                var expressao = /^\d+$/;
	
                if(!expressao.test(obj.value)) {
                    return false;
                } else {
                    return true;
                }
            }

            function validaCad() {
                var objForm = document.forma;
                var reDigits = /^\d+$/;
		

                if (objForm.TipoPessoa[0].checked == true) {

                    if (objForm.nome.value == '') {
                        alert('Campo NOME é Obrigatório!');
                        objForm.nome.focus();
                        return false;
                    }

                    if (objForm.cpf.value == '') {
                        alert('CPF inválido!');
                        objForm.cpf.focus();
                        objForm.cpf.select();
                        return false;
                    } else {
                        if (!ValidaCpf(objForm.cpf.value)) {
                            alert('CPF inválido!');
                            objForm.cpf.focus();
                            objForm.cpf.select();
                            return false;
                        }
                    }
                } else {
                    if(objForm.nomeR.value == '') {
                        alert('Campo RAZÃO SOCIAL é Obrigatório!');
                        objForm.nomeR.focus();
                        return false;	
                    }	
                    if(objForm.CNPJ.value == '') {
                        alert('CNPJ inválido!');
                        objForm.CNPJ.focus();
                        return false;	
                    } else {
                        if (!validaCnpj(objForm.CNPJ.value)) {
                            alert('CNPJ inválido!');
                            objForm.CNPJ.focus();
                            objForm.CNPJ.select();
                            return false;
                        }
                    }
                    if (objForm.InscEstadual.value == '') {
                        alert('Campo INSCRIÇÃO ESTADUAL é Obrigatório!');
                        objForm.InscEstadual.focus();
                        return false;	
                    }					
                }	

                if(objForm.email.value.indexOf("-") > 0) {
                    alert('Seu E-mail não pode contem caracteres especiais!');
                    objForm.email.focus();
                    objForm.email.select();
                    return false;        
                }
                if(! ValidaEmail(objForm.email.value)) {
                    alert('E-mail Invalido!');
                    objForm.email.focus();
                    objForm.email.select();
                    return false;        
                }


                if (objForm.tcomercial.value != '') {
                    if (!ValidarNumero(objForm.tcomercial)) {
                        alert('Campo TELEFONE aceita apenas números!');
                        objForm.tcomercial.focus();
                        return false;
                    }
                }  else {
                    alert('Campo TELEFONE aceita apenas números!');
                    objForm.tcomercial.focus();
                    return false;
                }

                if (objForm.tcomercial.value != '') {
                    if (objForm.tcomercial.value.length < 7) {
                        alert('Campo TELEFONE aceita no mínimo 7 números.');
                        objForm.tcomercial.focus();
                        return false;
                    }
                }


                if(objForm.cepc.value == '') {
                    alert('cep inválido!');
                    objForm.cepc.focus();
                    return false;	
                } else {
                    if (!ValidarNumero(objForm.cepc)) {
                        alert('Campo cep aceita apenas números!');
                        objForm.cepc.focus();
                        return false;
                    }
                }

                if (objForm.cepc.value != '') {
                    if (objForm.cepc.value.length < 8) {
                        alert('Campo CEP deve conter 8 dígitos.');
                        objForm.cepc.focus();
                        return false;
                    }
                }	
                if(objForm.enderecoc.value == '') {
                    alert('Campo ENDEREÇO é Obrigatório!');
                    objForm.enderecoc.focus();
                    return false;	
                }
                else {
                    if(objForm.enderecoc.value.length < 2 ) {
                        alert('Campo ENDEREÇO está inválido!');
                        objForm.enderecoc.focus();
                        return false;
                    } 
                }
                if (objForm.numeroc.value != '') {
                    if (!ValidarNumero(objForm.numeroc)) {
                        alert('Campo NÚMERO aceita apenas números!');
                        objForm.numeroc.focus();
                        return false;
                    }
                } else {
                    alert('Campo NÚMERO aceita apenas números!');
                    objForm.numeroc.focus();
                    return false;
                }
                if (objForm.bairroc.value == ''  || objForm.bairroc.value == 'undefined') {
                    alert('Campo BAIRRO é Obrigatório!');
                    objForm.bairroc.focus();
                    return false;
                }		
                else {
                    if(objForm.bairroc.value.length < 3 ) {
                        alert('Campo BAIRRO está inválido!');
                        objForm.bairroc.focus();
                        return false;
                    } 
                }
                if (objForm.cidade.value == '' || objForm.cidade.value == 'undefined') {
                    alert('Campo CIDADE é Obrigatório!');
                    objForm.cidade.focus();
                    return false;
                }
                if (objForm.provincia.value == '' || objForm.provincia.value == 'undefined') {
                    alert('Campo ESTADO é Obrigatório!');
                    objForm.provincia.focus();
                    return false;
                }
               
                if (objForm.tcomercial.value != '') {
                    if (objForm.tcomercial.value.substring(0,5) == '00000' || 
                        objForm.tcomercial.value.substring(0,5) == '11111' || 
                        objForm.tcomercial.value.substring(0,5) == '22222' || 
                        objForm.tcomercial.value.substring(0,5) == '33333' || 
                        objForm.tcomercial.value.substring(0,5) == '44444' || 
                        objForm.tcomercial.value.substring(0,5) == '55555' || 
                        objForm.tcomercial.value.substring(0,5) == '66666' || 
                        objForm.tcomercial.value.substring(0,5) == '77777' || 
                        objForm.tcomercial.value.substring(0,5) == '88888' || 
                        objForm.tcomercial.value.substring(0,5) == '99999') {

                        alert('TELEFONE inválido!');
                        objForm.tcomercial.focus();
                        return false;
                    }
                }	
	
                if (objForm.provincia.value == '') {
                    alert('Campo ESTADO é Obrigatório!');
                    return false;
                }
                var objForm = document.forma;
	
                for(var a=0; a < objForm.elements.length; a++) { 
                    if (objForm.elements[a].validar=="1") {
                        if(objForm.elements[a].value == '') {
                            alert('Todos os campos são obrigatórios!');
                            objForm.elements[a].focus();
                            return false;        
                        }
                    }
                }

                return true;

            }
    

        var reEmail1 = /^[\w!#$%&'*+\/=?^`{|}~-]+(\.[\w!#$%&'*+\/=?^`{|}~-]+)*@(([\w-]+\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
        var reEmail2 = /^[\w-]+(\.[\w-]+)*@(([\w-]{2,63}\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
        var reEmail3 = /^[\w-]+(\.[\w-]+)*@(([A-Za-z\d][A-Za-z\d-]{0,61}[A-Za-z\d]\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
        var reEmail = reEmail3;
 
        function ValidaEmail(campoemail)
        {
            eval("reEmail = reEmail");
            if (reEmail.test(campoemail)) {
                return true;
            } else if (campoemail != null && campoemail != "") {
                return false;
            }
        } 


        function MarcarCampos() {
            <%if(Replace_Caracteres_Especiais(request("cep")) <> "") then%>	document.forma.cep.value = '<%= Replace_Caracteres_Especiais(request("cep")) %>';<%end if%>
            <%if(Replace_Caracteres_Especiais(request("estado")) <> "") then%>document.forma.provincia.value = '<%= Replace_Caracteres_Especiais(request("estado")) %>';<%end if%>
            <%if(Replace_Caracteres_Especiais(request("cidade")) <> "") then%>document.forma.cidade.value = '<%= Replace_Caracteres_Especiais(request("cidade")) %>';<%end if%>
            <%if(Replace_Caracteres_Especiais(request("bairro")) <> "") then%>document.forma.bairro.value = '<%= Replace_Caracteres_Especiais(request("bairro")) %>';<%end if%>
            }

        function abrir_cep(v1, sC, sW, sH){
            janela = window.open(v1 + '?campo=' + eval('document.forma.' + sC + '.value'), "ToDo","width="+ sW +",height="+ sH +",location=no,resizable=yes, menubar=no,status=no, scroolbar=auto, toolbar=no, screenX=1, screenY=1, top=10, left=10 ")
        }


    function MostraDep(sbt) {
        if(sbt == 'F') {
            document.getElementById('pf1').style.display='block';
            document.getElementById('pf2').style.display='block';
            document.getElementById('pf3').style.display = 'block';
            document.getElementById('pf4').style.display = 'block';

            document.getElementById('pj1').style.display='none';
            document.getElementById('pj2').style.display='none';
            document.getElementById('pj3').style.display='none';
        } 
	
        if(sbt == 'J') {
            document.getElementById('pf1').style.display='none';
            document.getElementById('pf2').style.display='none';
            document.getElementById('pf3').style.display = 'none';
            document.getElementById('pf4').style.display = 'none';

            document.getElementById('pj1').style.display='block';
            document.getElementById('pj2').style.display='block';
            document.getElementById('pj3').style.display='block';
        }

        if(sbt == '') {
            document.getElementById('pf1').style.display='block';
            document.getElementById('pf2').style.display='block';
            document.getElementById('pf3').style.display = 'block';
            document.getElementById('pf4').style.display = 'block';

            document.getElementById('pj1').style.display='none';
            document.getElementById('pj2').style.display='none';
            document.getElementById('pj3').style.display='none';
        } 
    }
        //-->
    </script>

<body <%= sOnload%>>

	<!--#include virtual= "header.asp"-->

    <div class=" desktop-margin-top-90" >
	<div class="margin-60-0 ">
		<div class="container">
			<div class="row desktop-margin-90 ">
				<div class="col-sm-12">

                <%	if(Session("Id") <> "") then
		                Dim sDDDTelefone, sTelefone, sDDDTelefoneC, sTelefoneC

		                sIdCateg = Session("id")
		
                        call Abre_Conexao()

		                sTelefone   = trim(Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "Telefone"))
		                sTelefoneC  = trim(Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "Celular"))

                        if(sTelefone <> "" and not isnull(sTelefone)) then sTelefone = replace(sTelefone, "-", "")
                        if(sTelefoneC <> "" and not isnull(sTelefoneC)) then sTelefoneC = replace(sTelefoneC, "-", "")

                    end if
                %>

                    <form name="forma"  method="post" action="<%= Request.ServerVariables("Script_Name")%>" >

                        <input type="hidden" name="enviado" value="ok">
                        <input name="redirecionar" type="hidden" value="">
                        <input type="hidden" name="enderecoH"   id="enderecoH" >
                        <input type="hidden" name="bairroH"     id="bairroH"   >
                        <input type="hidden" name="cidadeH"     id="cidadeH"   >
                        <input type="hidden" name="provinciaH"  id="provinciaH">

                            <div class="row">
                                <h2 class="cor-4 semi-bold "><span class="shape6">ALTERAR CADASTRO</span></h2>
                                <p>[<a href="/arearestrita">voltar Minha conta</a>]</p>
                                <p>&nbsp;</p>

                                  <div class="col-sm-12">
                                    <div class="input-text form-group">
						                    <label class="label_pf " for="input_pf"><input onclick="javascript: MostraDep(this.value);" name="TipoPessoa" value="F" type="radio" class="form-control" <%if(Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "TipoPessoa") = "F") then response.write  "checked ": end if %>>  Pessoa física</label>
						                    <label class="label_pj " for="input_pj"><input onclick="javascript: MostraDep(this.value);" name="TipoPessoa" value="J" type="radio"  class="form-control" <%if(Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "TipoPessoa") = "J") then response.write  "checked ": end if %>>         Pessoa jurídica</label>
                                    </div>
                                  </div>

                                    <p>&nbsp;</p>

                                  <div class="col-sm-6 form-group" id="pf1" >
                                    <label>Nome Completo:</label>
                                    <div class="input-text"><input type="text" placeholder="Nome Completo" name="nome"  maxlength="100" class="form-control" value="<%= Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "nome")%>"></div>
                                  </div>

                                  <div class="col-sm-6 form-group" id="pf2">
                                    <label>CPF:</label>
                                    <div class="input-text"><input type="number" name="cpf"  placeholder="CPF"  maxlength="11" class="form-control" value="<%= Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "cpf")%>"></div>
                                  </div>

                                 <div class="col-sm-6 form-group" id="pj1"  style="display:none;">
                                    <label>Razão Social:</label>
                                    <div class="input-text"><input type="text" placeholder="Razão Social" name="nomeR"  maxlength="100"  class="form-control" value="<%= Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "nome")%>"></div>
                                  </div>

                                  <div class="col-sm-6 form-group" id="pj2"  style="display:none;">
                                    <label>CNPJ:</label>
                                    <div class="input-text"><input type="number" name="CNPJ"  class="form-control" placeholder="CNPJ"  maxlength="14" value="<%= Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "CNPJ")%>"></div>
                                  </div>

                                  <div class="col-sm-12 form-group">
                                    <label>E-mail:</label>
                                    <div class="input-text"><input type="email" placeholder="E-mail" name="email" maxlength="60" class="form-control" required="required" value="<%= Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "email")%>"></div>
                                  </div>

                                  <div class="col-sm-6 form-group"  id="pf3">
                                    <label>Data Nascimento:</label>
                                    <div class="input-text">
                                            <table>
                                            <tr>
                                            <td><select name="dia"  class="form-control" style="width: 67px;">
                                            <%for i = 1 to 31%>
	                                            <option value="<%=right("00" & i, 2)%>" <%if(day(Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "DtNascimento")) =i) then response.write  "selected": end if %>><%=right("00" & i, 2)%></option>
                                            <%next%>
                                            </select></td>
                                            <td><select name="mes"  class="form-control" style="width: 67px;">
                                            <%for i = 1 to 12%>
	                                            <option value="<%=right("00" & i, 2)%>" <%if(month(Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "DtNascimento")) =i) then response.write  "selected": end if %>><%=right("00" & i, 2)%></option>
                                            <%next%>
                                            </select></td>
                                            <td><select name="ano" size="1"  class="form-control" style="width: 100px;">
                                            <%for i = 1920 to 2000%>
	                                            <option value="<%=i%>" <%if(year(Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "DtNascimento")) =i) then response.write  "selected": end if %>><%=i%></option>
                                            <%next%>
                                            </select></td>
                                            </tr>
                                            </table>

                                    </div>
                                  </div>

                                  <div class="col-sm-6 form-group"  id="pj3"  style="display:none;">
                                    <label>Inscrição Estadual:</label>
                                    <div class="input-text"><input name="InscEstadual" class="form-control" type="number" size="20" maxlength="20" value="<%= Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "InscEstadual")%>"></div>
                                  </div>

                                  <div class="col-sm-6 form-group"  id="pf4">
                                    <label>Sexo:</label>
                                    <div class="input-text">
                                        <label class="label_masc" for="masc"><input name="Sexo" value="M" type="radio"      <%if(Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "Sexo") = "M") then response.write  "checked ": end if %>>Masculino</label> 
                                        <label class="label_fem" for="fem"><input name="Sexo" value="F" type="radio"        <%if(Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "Sexo") ="F") then response.write  "checked ": end if %>>Feminino </label>
                                        <br /><br />
                                    </div>
                                  </div>

                                  <div class="col-sm-6 form-group">
                                    <label>Telefone:</label>
                                    <div class="input-text"><input type="number" name="tcomercial" placeholder="Telefone (com DDD)" size="12" maxlength="12" class="form-control" required="required" value="<%= sTelefone %>"></div>
                                  </div>
                                  <div class="col-sm-6 form-group">
                                    <label>Celular:</label>
                                    <div class="input-text"><input type="number" name="tcelular" placeholder="Celular (com DDD)" size="12" maxlength="12" class="form-control" required="required" value="<%= sTelefoneC %>"></div>
                                  </div>

                                  <div class="col-xs-12 form-group">
                                    <label>Cep:</label>
                                    <div class="input-text"><input type="number" maxlength="8" name="cepc"   id="cepc" onblur="preenche_endereco(this.value, '')" placeholder="CEP"  class="form-control" required="required" style="width:112px" value="<%= Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "cep")%>" /></div>
                                  </div>

                                  <div class="col-xs-12 form-group">
                                    <label>Endereço:</label>
                                    <div class="input-text"><input type="text" name="enderecoc" id="endereco" placeholder="Endereço" class="form-control" required="required" value="<%= Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "endereco")%>"></div>
                                  </div>
                              
                                  <div class="col-sm-6 form-group">
                                    <label>Número:</label>
                                    <div class="input-text"><input type="number" name="numeroc" id="numero" placeholder="N°"  maxlength="10" class="form-control" required="required" value="<%= Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "numero")%>"></div>
                                  </div>
                              
                                  <div class="col-sm-6 form-group">
                                    <label>Complemento:</label>
                                    <div class="input-text"><input type="text"  name="complemento" placeholder="Complemento"  maxlength="100" class="form-control" value="<%= Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "complemento")%>"></div>
                                  </div>
                              
                                  <div class="col-sm-6 form-group">
                                    <label>Cidade:</label>
                                    <div class="input-text"><input type="text" name="cidade" id="cidade" placeholder="Cidade" class="form-control" required="required" value="<%= Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "Cidade") %>"></div>
                                  </div>
                              
                                  <div class="col-sm-6 form-group">
                                    <label>Estado:</label>
                                    <div class="input-text"><%= Monta_Combo_Provincia("provincia", ""& Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "id_provincia") &"", " name='provinciaLegenda' id='provinciaLegenda' class='form-control' required='required' ") %></div>
                                  </div>

                                  <div class="col-sm-12 form-group">
                                    <label>Bairro:</label>
                                    <div class="input-text"><input type="text"  name="bairroc" id="bairro" placeholder="Bairro" maxlength="60" class="form-control" required="required" value="<%= Recupera_Campos_Db_oConn("TB_CLIENTES", sIdCateg, "id", "Bairro")%>"></div>
                                  </div>

                                  <div class="col-xs-12 form-group">
                                    <div class="submit-text">
                                        <input type="image" src="/img/btcomprar.JPG" name="CADASTRAR" value="CADASTRAR" id="finalizar_final" class="pull-right"/>
                                    </div>
                                  </div>


                            </div>
                      </form>

				</div>

			</div>
		</div>
	</div>


</div>

    <!--#include virtual= "footer.asp"-->

</body>
</html>
