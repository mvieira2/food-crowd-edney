<!--#include virtual= "/hidden/funcoes.asp"-->

<%
dim sConteudoInst, sTituloItem1, sConteudoItem1, sTituloItem2, sConteudoItem2, sTituloItem3, sConteudoItem3

dim pgBanner2: pgBanner2 = Mostra_Banner("2")

sConteudoInst 	    = trim(Recupera_Campos_Db_oConn("TB_INSTITUCIONAL", "1529", "Id_Categoria", "Conteudo"))
if(sConteudoInst    = "" or isnull(sConteudoInst)) then sConteudoInst = Application("TextoSobreemprsa")

call Abre_Conexao()
    
sTituloItem1        = ucase(trim(Recupera_Campos_Db_oConn("TB_INSTITUCIONAL_CATEGORIAS", "1533", "ID", "Categoria")))
sConteudoItem1      = trim(Recupera_Campos_Db_oConn("TB_INSTITUCIONAL", "1533", "Id_Categoria", "Conteudo"))
    
sTituloItem2        = ucase(trim(Recupera_Campos_Db_oConn("TB_INSTITUCIONAL_CATEGORIAS", "1534", "ID", "Categoria")))
sConteudoItem2      = trim(Recupera_Campos_Db_oConn("TB_INSTITUCIONAL", "1534", "Id_Categoria", "Conteudo"))
    
sTituloItem3        = ucase(trim(Recupera_Campos_Db_oConn("TB_INSTITUCIONAL_CATEGORIAS", "1535", "ID", "Categoria")))
sConteudoItem3      = trim(Recupera_Campos_Db_oConn("TB_INSTITUCIONAL", "1535", "Id_Categoria", "Conteudo"))

call Fecha_Conexao()
%>

<!DOCTYPE html>
<html lang="en" dir="ltr">

    <!--#include virtual= "head.asp"-->

<body>
    <!--#include virtual= "header.asp"-->
    <!--#include virtual= "banners.asp"-->

    <div class="container">
    <div class="row margin-60-0 ">
        <div class="col-sm-4 navbar-right">
            <iframe width="350" height="294" src="https://www.youtube.com/embed/VZrP5JrA7FU?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


            <% 'if(pgBanner2 <> "") then  %>
                <%'= pgBanner2 %>
            <% 'end if %>

        </div>
        <div class="col-sm-8">
            <h2 class="semi-bold shape">NÓS SOMOS A <br> <span class="cor-1 semi-bold">FOOD</span> <span class="cor-2 semi-bold">CROWD</span> </h2>
                <p class="descrition"><%= sConteudoInst %></p>
                <p><a href="/quemsomos" class="btn botao-2">SAIBA MAIS</a></p>
            </div>

        </div>
    </div>

    <hr>

    <div class="container">
        <div class="row  ">
            <div class="col-sm-4 ">
                <div class="row margin-60-0">
                    <div class="col-xs-2 "><img class="homeIcones" src="/img/icons/icon1.png" alt=""></div>
                    <div class="col-xs-10 "><h4 class="semi-bold "><%= sTituloItem1 %></h4></div>
                    <div class="col-sm-12"><p class="descrition"><%= sConteudoItem1 %></p></div>
                </div>
            </div>

            <div class="col-sm-4 ">
                <div class="row margin-60-0">
                    <div class="col-xs-2 "><img class="homeIcones" src="/img/icons/icon2.png" alt=""></div>
                    <div class="col-xs-10 "><h4 class="semi-bold "><%= sTituloItem2 %></h4></div>
                    <div class="col-sm-12"><p class="descrition"><%= sConteudoItem2 %></p></div>
                </div>
            </div>

            <div class="col-sm-4 ">
                <div class="row margin-60-0">
                    <div class="col-xs-2 "><img class="homeIcones" src="./img/icons/icon3.png" alt=""></div>
                    <div class="col-xs-10 "><h4 class="semi-bold "><%= sTituloItem3 %></h4></div>
                    <div class="col-sm-12"><p class="descrition"><%= sConteudoItem3 %></p></div>
                </div>
            </div>
        </div>
    </div>
    
    <hr>

    <div class="banners hide">
        <div id="myCarousel2" class="carousel slide" data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner">

                <div class="item active">
                    <img src="./img/banner3.png" alt="Los Angeles">
                    <div class="carousel-caption">

                        <p class="banner-descritions semi-bold">
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        </p>
                        <p class="text-center">
                            <a href="#" class="btn botao-3">SAIBA MAIS</a>

                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="container ">
        <div class="row margin-60-0 box-ofertas">

            <div class="col-sm-12 "><h2 class="semi-bold shape2">  EXPERIÊNCIAS EM DESTAQUE </h2><br></div>

            <%= Monta_Carrossel_Produtos("", false, "", "797928") %>
            <%= Monta_Carrossel_Produtos("", false, "797928", "") %>

            <div class="col-sm-5"><a href="/categorias.asp"><div class="sombra"></div><img src="/img/banner6.png" alt=""><strong class="nome-da-oferta-2 shape4">TODAS AS EXPERIÊNCIAS</strong></a></div>
        </div>

    </div>

    <div class="box-verde margin-60-0 ">

        <div class="container ">
            <div class="row" style="margin:0px;">
                <div class="col-sm-9 col-xs-6"><h4>Dúvidas? Entre em contato conosco.</h4></div>
                <div class="col-sm-3 col-xs-6 text-right"><a href="/contato" class="btn botao-1">SAIBA MAIS</a></div>
            </div>
        </div>
    </div>

    <div class="container hide">
        <div class="row margin-60-0 ">
            <div class="col-xs-10">
                <h2 class="semi-bold shape2"> BLOG </h2>

            </div>
            <div class="col-xs-2 text-right">
                <div class="setas slider-1-setas noselect">
                    <i class="material-icons seta-esquerda">&#xe5c4;</i>
                    <i class="material-icons seta-direita">&#xe5c8;</i>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
                <div class="slider-1  box-ofertas row">

                    <!-- slider aqui -->
                    <div class="col-sm-4">
                        <a href="">
                            <div class="sombra"></div>
                            <img src="./img/blog1.png" alt="">
                        </a>
                    </div>
                    <!-- fecha slider aqui -->

                    <div class="col-sm-4 ">
                        <a href="">
                            <div class="sombra"></div>
                            <img src="./img/blog2.png" alt="">
                        </a>
                    </div>

                    <div class="col-sm-4 ">
                        <a href="">
                            <div class="sombra"></div>
                            <img src="./img/blog3.png" alt="">
                        </a>
                    </div>

                    <div class="col-sm-4 ">
                        <a href="">
                            <div class="sombra"></div>
                            <img src="./img/blog3.png" alt="">
                        </a>
                    </div>

                    <div class="col-sm-4 ">
                        <a href="">
                            <div class="sombra"></div>
                            <img src="./img/blog3.png" alt="">
                        </a>
                    </div>

                </div>

            </div>

        </div>
    </div>

    <div class="container noticias hide">
        <div class="row margin-30-0 ">
            <div class="col-xs-12">
                <h2 class="semi-bold shape2"> NOTÍCIAS </h2>
                <br>
            </div>

            <!-- comeca linha das noticias -->
            <div class="col-sm-12">
                <a href="#">
                    <div class="row">
                        <div class="col-sm-8">
                            <span class="semi-bold cor-1 ">Notícia Aqui: Lorem ipsum dolor sit amet</span>
                        </div>
                        <div class="col-sm-2">
                            <span class="cor-3">
                                29 Outubro 2018
                            </span>
                        </div>
                        <div class="col-sm-2 cor-3">
                            por <span class="cor-2">John Smith</span>
                        </div>

                    </div>
                    <hr>
                </a>
            </div>
            <!-- fecha linha das noticias -->

            <!-- comeca linha das noticias -->
            <div class="col-sm-12">
                <a href="">
                    <div class="row">
                        <div class="col-sm-8">
                            <span class="semi-bold cor-2 ">Notícia Aqui: Lorem ipsum dolor sit amet</span>
                        </div>
                        <div class="col-sm-2">
                            <span class="cor-3">
                                29 Outubro 2018
                            </span>
                        </div>
                        <div class="col-sm-2 cor-3">
                            por <span class="cor-1">John Smith</span>
                        </div>

                    </div>
                    <hr>
                </a>
            </div>
            <!-- fecha linha das noticias -->

            <!-- comeca linha das noticias -->
            <div class="col-sm-12">
                <a href="#">
                    <div class="row">
                        <div class="col-sm-8">
                            <span class="semi-bold cor-1 ">Notícia Aqui: Lorem ipsum dolor sit amet</span>
                        </div>
                        <div class="col-sm-2">
                            <span class="cor-3">
                                29 Outubro 2018
                            </span>
                        </div>
                        <div class="col-sm-2 cor-3">
                            por <span class="cor-2">John Smith</span>
                        </div>

                    </div>
                    <hr>
                </a>
            </div>
            <!-- fecha linha das noticias -->

        </div>
    </div>

    <!--#include virtual= "footer.asp"-->
</body>
</html>
