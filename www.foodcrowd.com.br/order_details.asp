<!--#include virtual= "/hidden/funcoes.asp"-->

<%
if(Session("Id") = "") then response.redirect("/login.asp?ReturnUrl=arearestrita")

dim sClassHeader: sClassHeader = "bg-1"

' VARIAVEIS ====================================================
Dim sIdCateg, boolenviado, boolGravado, sAcao, sSQl, EmailCliente, sData
dim sStatus, sRastreamento, sRastreamentoEmpresa, sNotificacao, sTipoDownload

sIdCateg = Cdbl(Request("id"))

call Abre_Conexao()

' ====================================================
if (trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Cliente")) <> Session("Id") ) then
	response.write "<P><div align=""center"">ERRO NO SISTEMA</div></P>"
	response.end
end if

' ====================================================
function Monta_Linhas_Produtos()
	dim sConteudo, Rs, sSql
	dim sCampo(15)
	
	if(sIdCateg = "") then exit function
	
	sSql = "Select * from TB_PEDIDOS_ITENS (nolock) where id_pedido = " & sIdCateg
	Set Rs = oConn.execute(sSql)

	if(not rs.eof) then
		do while not rs.eof
			sCampo(0) = trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", rs("id_produto"), "id", "Imagem"))
			sCampo(1) = trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", rs("id_produto"), "id", "Descricao"))
			sCampo(2) = cdbl(rs("quantidade"))
			sCampo(3) = RetornaCasasDecimais(cdbl(rs("Preco_Unitario")),2)
			sCampo(4) = Server.HtmlEncode(trim(Recupera_Campos_Db_oConn("TB_MOEDAS", rs("Id_Moeda"), "id", "Simbolo")))
			sCampo(5) = RetornaCasasDecimais((cdbl(rs("Preco_Unitario")) * sCampo(2)),2)
			sCampo(6) = trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", rs("id_produto"), "id", "CodReferencia"))
			sCampo(7) = trim(rs("Id_Tamanho"))
			sCampo(8) = trim(rs("Id_Cor"))
			sCampo(9) = trim(rs("Id_Sabor"))
			sCampo(10) = trim(rs("Id_Fragrancia"))
			sCampo(11) = trim(rs("Id_Tipo"))
			sCampo(13) = trim(rs("Id_Ge"))
			sCampo(14) = trim(rs("ValorGe"))
			sCampo(15) = trim(rs("CertificadoGE"))

			if(sCampo(0) = "" OR ISNULL(sCampo(0))) then sCampo(0) = "sem_imagem.jpg"
			sConteudo = sConteudo & "                <tr valign=""middle"">"
			sConteudo = sConteudo & "                  <td align=""center"" bgcolor=""#FFFFFF"" width=""120""><center>"

			if((right(lcase(sCampo(0)),4) = ".wmv")) then
				sConteudo = sConteudo & "<img src="""& sLinkImagem &"/Ecommercenew/upload/produto/imagem_video.jpg"" width=""70"" height=""80"" hspace=""1"" vspace=""0"" border=""0"" align=""absmiddle"" alt="""">"
			else
				sConteudo = sConteudo & "<img src=""" & sLinkImagem & "/ecommercenew/upload/produto/"& sCampo(0) &"""width=""70"" height=""80"" hspace=""1"" vspace=""0"" border=""0"" align=""absmiddle"" alt="""">"
			end if
			
			sConteudo = sConteudo & "</center></td>"
			sConteudo = sConteudo & "                  <td bgcolor=""#FFFFFF"" ><span class=""descriprod""><strong align=""left"">"& rs("id") & "-"& sCampo(1) &"</strong><br> <span class=""a11preto"">Ref.</spam> "& sCampo(6)  &"</td>"
			sConteudo = sConteudo & "                  <td bgcolor=""#FFFFFF"" ><span class=""descriprod""><div align=""center"">"& sCampo(2) &"</div></td>"
			sConteudo = sConteudo & "                  <td bgcolor=""#FFFFFF"" ><span class=""descriprod""><div align=""center"">"& sCampo(4) &"&nbsp;"& sCampo(3) &"</div></td>"
			sConteudo = sConteudo & "                  <td bgcolor=""#FFFFFF"" ><span class=""descriprod""><div align=""center"">"& sCampo(4) &"&nbsp;"& sCampo(5) &"</div></td>"
			sConteudo = sConteudo & "                </tr>"
			sConteudo = sConteudo & "		          <tr>"
			sConteudo = sConteudo & "		            <td colspan=""5""><span class=""descriprod"">"

			If(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", rs("id_produto"), "id", "valepresente")) = "1") then
				sConteudo = sConteudo & "<strong> Numero do Cupom: </strong>" & Recupera_Campos_Db_oConn("TB_CUPONS","'"& sIdCateg & "'", "NumeroPedido", "Cupom") & "<Br>"
			end if
				
			if(sCampo(7) <> "") then sConteudo = sConteudo & "	<strong> Tamanho:</strong>    | " & sCampo(7)  & " |"
			if(sCampo(8) <> "") then sConteudo = sConteudo & "	<strong> Cor: </strong>       " & sCampo(8)  & " |"
			if(sCampo(9) <> "") then sConteudo = sConteudo & "	<strong> Sabor: </strong> 	  " & sCampo(9)  & " |"
			if(sCampo(10) <> "") then sConteudo = sConteudo & "	<strong> Fragrancia:</strong> " & sCampo(10) & " |"
			if(sCampo(11) <> "") then sConteudo = sConteudo & "	<strong> Tipo:</strong>       " & sCampo(11) & " |"
			if(sCampo(14) > 0) then sConteudo = sConteudo & " Garantia Estendida - 12 meses - Mapfre - <strong>Valor R$ "& sCampo(14) &"</strong> - Certificado: " & sCampo(15)
			
			sConteudo = sConteudo & "					</td>"
			sConteudo = sConteudo & "		          </tr>"
		rs.movenext
		loop
	end if
	Set Rs = nothing

	Monta_Linhas_Produtos = sConteudo
end function

%>

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <!--#include virtual= "head.asp"-->

    <body>
        <!--#include virtual= "header.asp"-->

        <div class=" desktop-margin-top-90" >
	    <div class="margin-60-0 ">
		    <div class="container">
			    <div class="row desktop-margin-90 ">
				    <div class="col-sm-12">
					    <div class="row">
						    <div class="col-sm-12"><h2 class="cor-4 semi-bold "><span class="shape7">PEDIDO [<%= sIdCateg %>]</span></h2></div>

						    <div class="col-sm-10 col-sm-push-1">

                                <p>[<a href="/arearestrita">voltar Minha conta</a>]</p>

                                <p>&nbsp;</p>
								
							    <div class="table-responsive" style="overflow:hidden">

                                      <div class="orders-list table-responsive"> 

                                      <!--order info tables-->
                                      <table class="table table-bordered cart_summary table-striped">
                                        <tr>
                                          <td class="order-number">Código</td>
                                          <td data-title="Order Number">#<%=  sIdCateg %></td>
                                        </tr>
                                        <tr>
                                          <td class="order-number">Data</td>
                                          <td data-title="Order Date"><%= trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "DataCad")) %></td>
                                        </tr>
                                        <tr>
                                          <td class="order-number">Status</td>
                                          <td data-title="Order Status"><%= trim(Recupera_Campos_Db_oConn("TB_STATUS", trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Status")), "id", "descricao")) %></td>
                                        </tr>
                                        <tr>
                                          <td class="order-number">Transportadora/ Frete</td>
                                          <td data-title="Shipment"><%= trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "TipoEntregaTransportadora")) %> / <b>R$ <%= RetornaCasasDecimais(trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Frete")),2) %></b> </td>
                                        </tr>
                                        <tr>
                                          <td class="order-number">pagamento</td>
                                          <td data-title="Payment"><%= Server.HtmlEncode(trim(Recupera_Campos_Db_oConn("TB_FORMAS_PAGAMENTO", trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_FrmPagamento")), "id", "Descricao"))) %></td>
                                        </tr>
                                        <tr>
                                          <td class="order-number">Observação</td>
                                          <td data-title="Comment"><%= sIdCateg %></td>
                                        </tr>

                                        <tr>
                                          <td class="order-number">Desconto (produto)</td>
                                          <td data-title="Total"><p><b>R$ <%= RetornaCasasDecimais(trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "DescontoPro")),2) %></b></p></td>
                                        </tr>
                                        <tr>
                                          <td class="order-number">Frete:</td>
                                          <td data-title="Total"><p><b>R$ <%= RetornaCasasDecimais(trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "frete")),2) %></b></p></td>
                                        </tr>
                                        <tr>
                                          <td class="order-number">Total Pedido</td>
                                          <td data-title="Total"><p><b>R$ <%= RetornaCasasDecimais(trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Total")),2) %></b></p></td>
                                        </tr>
                                      </table>
                                    </div>

                                    <div class="row">
                                      <div class="col-xs-12 col-md-6 col-sm-6">
                                      <div class="page-title">
                                      <h2>Dados Cobrança</h2>
                                    </div>
          
                                        <table class="table table-bordered cart_summary">
                                          <tr>
                                            <td>E-Mail</td>
                                            <td data-title="E-Mail" ><a class="color_dark" href="mailto: <%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Cliente")), "id", "email")) %>"><%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Cliente")), "id", "email")) %></a> </td>
                                          </tr>
                                          <tr>
                                            <td>Nome Completo</td>
                                            <td data-title="Company Name" ><a href="altera_cadastro.asp?id=<%= trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Cliente")) %>" style="font-weight: bold"><%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Cliente")), "id", "Nome")) %> </a><a href="altera_cadastro.asp?id=<%= trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Cliente")) %>" style="font-weight: bold"><%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Cliente")), "id", "SobreNome")) %></a></td>
                                          </tr>
                                          <tr>
                                            <td>CEP</td>
                                            <td data-title="Address 1" ><%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Cliente")), "id", "Cep")) %></td>
                                          </tr>
                                          <tr>
                                            <td>Endereço</td>
                                            <td data-title="Zip / Postal Code" ><%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Cliente")), "id", "endereco")) %></td>
                                          </tr>
                                          <tr>
                                            <td>Número</td>
                                            <td data-title="Zip / Postal Code" ><%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Cliente")), "id", "Numero")) %></td>
                                          </tr>
                                          <tr>
                                            <td>Bairro</td>
                                            <td data-title="Zip / Postal Code" ><%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Cliente")), "id", "Bairro")) %></td>
                                          </tr>
                                          <tr>
                                            <td>Complemento</td>
                                            <td data-title="Zip / Postal Code" ><%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Cliente")), "id", "Complemento")) %></td>
                                          </tr>
                                          <tr>
                                            <td>Cidade</td>
                                            <td data-title="Zip / Postal Code" ><%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Cliente")), "id", "Cidade")) %></td>
                                          </tr>
                                          <tr>
                                            <td>Estado</td>
                                            <td data-title="Zip / Postal Code" ><%if(trim(Recupera_Campos_Db_oConn("TB_CLIENTES", trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Cliente")), "id", "Id_Provincia")) <> "") then response.write trim(Recupera_Campos_Db_oConn("TB_ESTADOS", trim(Recupera_Campos_Db_oConn("TB_CLIENTES", trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Cliente")), "id", "Id_Provincia")), "id", "Descricao")) %></td>
                                          </tr>
                                          <tr>
                                            <td>Telefone</td>
                                            <td data-title="Zip / Postal Code" ><%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Cliente")), "id", "Telefone")) %> / <%= trim(Recupera_Campos_Db_oConn("TB_CLIENTES", trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Cliente")), "id", "celular")) %></td>
                                          </tr>
                                        </table>
                                      </div>
                                      <div class="col-xs-12 col-md-6 col-sm-6">
                                       <div class="page-title">
                                      <h2>Dados Entrega</h2>
                                    </div>
                                        <table class="table table-bordered cart_summary">
                                          <tr>
                                            <td>CEP</td>
                                            <td data-title="Zip / Postal Code" ><%= trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Cep")) %></td>
                                          </tr>
                                          <tr>
                                            <td>Endereço</td>
                                            <td data-title="Zip / Postal Code" ><%= trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Endereco")) %></td>
                                          </tr>
                                          <tr>
                                            <td>Número</td>
                                            <td data-title="Zip / Postal Code" ><%= trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Numero")) %></td>
                                          </tr>
                                          <tr>
                                            <td>Bairro</td>
                                            <td data-title="Zip / Postal Code" ><%= trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Bairro")) %></td>
                                          </tr>
                                          <tr>
                                            <td>Complemento</td>
                                            <td data-title="Zip / Postal Code" ><%= trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Complemento")) %></td>
                                          </tr>
                                          <tr>
                                            <td>Cidade</td>
                                            <td data-title="Zip / Postal Code" ><%= trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Cidade")) %></td>
                                          </tr>
                                          <tr>
                                            <td>Estado</td>
                                            <td data-title="Zip / Postal Code" ><%if(trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Provincia")) <> "") then response.write trim(Recupera_Campos_Db_oConn("TB_ESTADOS", trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Id_Provincia")), "id", "Descricao"))  %></td>
                                          </tr>
                                          <tr>
                                            <td>Observação</td>
                                            <td data-title="Company Name" ><%= trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "Observacoes")) %></td>
                                          </tr>
                                          <tr>
                                            <td>IP</td>
                                            <td data-title="Company Name" ><%= trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", sIdCateg, "id", "IpConexao")) %></td>
                                          </tr>
                                        </table>
                                      </div>
                                    </div>
            

                                      <table class="table table-bordered cart_summary table-striped">
                                        <thead>
                                          <tr> 
                                            <th></th>
                                            <th>Produto</th>
                                            <th>Quantidade</th>
                                            <th>Unit.</th>
                                            <th>Total</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            <%= Monta_Linhas_Produtos() %>
                                        </tbody>
                                      </table>
 
							    </div>	
						    </div>
					    </div>	
				    </div>
			    </div>
		    </div>
	    </div>
    </div>

        <!--#include virtual= "footer.asp"-->
    </body>

</html>
