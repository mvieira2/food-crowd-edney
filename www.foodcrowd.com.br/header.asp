<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K6Q8NCX" height="0" width="0" style="display: none; visibility: hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->


<style>

    .input-group .form-control {
        position: relative;
        z-index: 2;
        float: left;
        width: 78%;
        margin-bottom: 0;
        margin: 6px 0 0 0;
        margin-right: 96px;
    }

    .nav > li > a {
        position: relative;
        display: block;
        padding: 10px 10px;
    }

</style>


<script>
    $(document).ready(function(){
        $('.dropdown-submenu > a').on("click", function(e){
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
        });
    });
</script>


<header class="header <%= sClassHeader %>">

    <nav class="navbar navbar-fixed-top">

        <div class="container">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand mobile" href="/"><img class="logo logo-mobile" src="/img/logo.png" alt="<%= Application("NomeLoja") %>"></a>
            </div>

            <div class="collapse navbar-collapse" id="myNavbar">

                <div class="row">
                    <div class="col-sm-2"><a class="navbar-brand desktop" href="/"><img src="/img/logo.png" class="logo " alt="<%= Application("NomeLoja") %>"></a></div>

                    <div class="col-sm-6">

                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="/quemsomos">SOBRE </a></li>
                            <li><a href="/produtores">PRODUTORES</a></li>
                            <li><a href="/receitas">RECEITAS</a></li>
                            <li class=" ">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="/produtos">PRODUTOS <span class="caret hide"></span></a>

                                <ul class="dropdown-menu">

                                    <li class="dropdown-submenu">
                                        <%= Montar_Links_Consulta("7", "", "", "", "", "") %>
                                    </li>

                                 </ul>
                            </li>

                            <li><a href="/contato">FALE CONOSCO</a></li>
                            <li class="mobile"><a href="/carrinho">MEU CARRINHO</a></li>
                        </ul>


                    </div>

                    <div class="col-sm-4">
                        <ul class="nav navbar-nav redes-sociais ">
                            <li>
                                <form action="/categorias.asp" method="post" name="frmBus" onsubmit="return validabusca()" id="frmBus">
                                    <input type="hidden" name="acao" value="busca">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="O que procura ?" name="busca" id="busca">
                                        <button class="btn-search" type="button" onclick="javascript: validabusca();" style="float: right; position: absolute; height: 35px; margin: 6px 0 0 -98px;"><i class="fa fa-search"></i></button>
                                    </div>
                                </form>

                            </li>
                            <li><a href="https://www.facebook.com/foodcrowdbr/" target="_blank"><i class="fa fa-facebook-f"></i></a></li>
                            <li><a href="https://www.instagram.com/foodcrowdbr/?hl=en" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="https://br.pinterest.com/foodcrowd/" target="_blank"><i class="fa fa-pinterest"></i></a></li>

                            <% if(Session("ItemCount") > 0) then  %>
                            <li><a href="/carrinho">
                                <img style="width: 18px; height: auto; margin: -6px 0 0 0;" src="/img/icons/carrinho.png" alt="Carrinho de compras"></a></li>
                            <% end if %>

                            <li>
                                <% if(Session("Id") <> "") then    %>
                                <a href="/arearestrita">
                                    <img style="width: 16px; height: auto; margin: 0 0 6px 0;" src="/img/icons/login2.png" alt="Area restrita"></a>
                                <% else %>
                                <a href="/login">
                                    <img style="width: 24px; height: auto;" src="/img/icons/login.png" alt="Login do Usuário"></a>
                                <% end if %>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>
        </div>
    </nav>

</header>
