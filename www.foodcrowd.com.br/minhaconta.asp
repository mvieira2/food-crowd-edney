<!--#include virtual= "/hidden/funcoes.asp"-->

<%
dim sClassHeader: sClassHeader = "bg-1"

if(Session("Id") = "") then response.redirect("/login.asp?ReturnUrl=arearestrita")



%>


<!DOCTYPE html>
<html lang="en" dir="ltr">

    <!--#include virtual= "head.asp"-->

    <body>
	    <!--#include virtual= "header.asp"-->

        <div class=" desktop-margin-top-90" >
	    <div class="margin-60-0 ">
		    <div class="container">
			    <div class="row desktop-margin-90 ">
				    <div class="col-sm-12">
						    <div class="row">
							    <div class="col-sm-12"><h2 class="cor-4 semi-bold "><span class="shape7">SUA CONTA</span></h2></div>

								    <div class="row desktop-margin-90">

									    <div class="col-sm-6">
										    <div class="box-1">

											    <div class="media">
												    <div class="media-left media-middle"><img src="/img/icons/icon5.png" class="media-object" style="width:60px"></div>

												    <div class="media-body" style="padding-left: 20px;">
													    <h4 class="media-heading"><a href="">Minha conta</a><i class="fa fa-angle-down"></i></h4>
                                                        <br />
                                                        <p><a href="/meucadastro">Meu Cadastro</a></p>
                                                        <p><a href="/alterarsenha">Alterar Senha</a></p>
                                                        <p><a href="/meuspedidos">Meus Pedidos</a></p>
                                                        <p><a href="/logout">Deslogar</a></p>
												    </div>
											    </div>
										    </div>

									    </div>
								    </div>

						    </div>	

				    </div>

			    </div>
		    </div>
	    </div>

    </div>

        <!--#include virtual= "footer.asp"-->
    </body>

</html>
