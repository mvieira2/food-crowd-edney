<!--#include virtual="/hidden/funcoes.asp"-->
<script src="/js/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="/js/Ajax.js" type="text/javascript"></script>
<% 
response.Charset = "ISO-8859-1"
dim sTitulo, nProduto, sTamanho, sCor
dim sSelect
dim sSqlInsC, sSqlIns, Rs, RsC

sTamanho 	= ""
sCor 		= ""

nProduto 	= Replace_Caracteres_Especiais(request("produto"))
sTamanho 	= Replace_Caracteres_Especiais(request("tamanho"))
sCor 		= Replace_Caracteres_Especiais(request("cor"))
sTipo 		= Replace_Caracteres_Especiais(request("id_tipo"))

if(nProduto = "" or isnull(nProduto) or not isnumeric(nProduto)) then
	response.write "ERRO NO SISTEMA (Produto)!"
	response.end
end if
if(sTipo = "") then
	response.write "ERRO NO SISTEMA (Tipo)!"
	response.end
end if

Select Case sTipo

	Case "tamanho"
		call abre_conexao()
		
		sSqlIns =  	"SELECT " & _
						" A.Id_Cor as campo, *  " & _
					" FROM   " & _
						" TB_IMAGENS_AUXILIARES A (NOLOCK)  " & _
						" INNER JOIN TB_CORES C (NOLOCK) ON A.ID_COR = C.ID  " & _
					" where  " & _
						" A.id_produto = '"& nProduto &"' and  " & _
						" A.id_tamanho = '" & sTamanho &"'"
		if (sCor <> "") then
			sSqlIns = sSqlIns &" and  A.id_cor = '" & sCor &"'"
		end if
		
		sSqlIns = sSqlIns & " order by C.Descricao "
		Set Rs = oConn.execute(sSqlIns)

		response.write "<script>$('#selectcor_2').append('<option value="""" >Escolha a Cor</option>');</script>"		
		if (not Rs.Eof) Then
			do while (not Rs.Eof )
				response.write "<script>$('#selectcor_2').append('<option value="""& trim(Rs("campo")) &""" >"& trim(Rs("Descricao")) &"</option>');</script>"
			Rs.Movenext
			loop
		end if
		set Rs = nothing

		call fecha_Conexao()

	Case "tamanhoT"
		call abre_conexao()
		
		sSqlIns = "select " & _
				" *  " & _
		" from  " & _
			" TB_TAMANHOS T (nolock)  " & _
		" where  " & _
			" T.cdcliente = "& sCdCliente &" and  " & _
			" T.ativo = 1 and  " & _
			" EXISTS (SELECT T.id FROM TB_IMAGENS_AUXILIARES AX (nolock) WHERE AX.id_produto = '"& nProduto &"' and AX.Id_TAMANHO = T.id and isnull(AX.CodAbacos,'') <> '' and AX.Qtd_Estoque > 0) "
		Set Rs = oConn.execute(sSqlIns)

		sSqlIns = sSqlIns & " order by T.Descricao "		
		
		
		response.write "<script>$('#select').append('<option value="""" >Escolha o Tamanho</option>');</script>"		
		if (not Rs.Eof) Then
			do while (not Rs.Eof )
				response.write "<script>$('#select').append('<option value="""& trim(Rs("id")) &""" >"& trim(Rs("Descricao")) &"</option>');</script>"
			Rs.Movenext
			loop
		end if
		
	Case "corT"
		call abre_conexao()

		sSqlIns = "select " & _
				" *  " & _
		" from  " & _
			" TB_CORES T (nolock)  " & _
		" where  " & _
			" T.cdcliente = "& sCdCliente &" and  " & _
			" T.ativo = 1 and  " & _
			" EXISTS (SELECT T.id FROM TB_IMAGENS_AUXILIARES AX (nolock) WHERE AX.id_produto = '"& nProduto &"' and AX.id_cor = T.id and isnull(AX.CodAbacos,'') <> '' and AX.Qtd_Estoque > 0) "
		Set RsC = oConn.execute(sSqlIns)

		sSqlIns = sSqlIns & " order by T.Descricao "		
		response.write "<script>$('#selectcor_2').append('<option value="""" >Escolha a Cor</option>');</script>"		
		if (not RsC.Eof) Then
			do while (not RsC.Eof )
				response.write "<script>$('#selectcor_2').append('<option value="""& trim(RsC("id")) &""" >"& trim(RsC("Descricao")) &"</option>');</script>"
			RsC.Movenext
			loop
		end if
		set RsC = nothing
		
		call fecha_Conexao()

	Case "cor"
		call abre_conexao()

		sSqlIns =  	"SELECT " & _
							" A.id_tamanho as campoC, *  " & _
					" FROM   " & _
						" TB_IMAGENS_AUXILIARES A (NOLOCK)  " & _
						" INNER JOIN TB_TAMANHOS C (NOLOCK) ON A.ID_TAMANHO = C.ID  " & _
					" where  " & _
						" A.id_produto 	= '"& nProduto &"' and  " & _
						" A.id_cor 		= '" & sCor &"' and " & _
						" EXISTS (SELECT C.id FROM TB_IMAGENS_AUXILIARES AX (nolock) WHERE AX.id_produto = '"& nProduto &"' and AX.id_tamanho = C.id and isnull(AX.CodAbacos,'') <> '' and AX.Qtd_Estoque > 0) "						
		sSqlIns = sSqlIns & " order by C.Descricao "						
						
		if (sTamanho <> "") then 
			sSqlIns = sSqlIns &" and  A.id_tamanho = '" & sTamanho &"'"
		end if
		
'		response.write sSqlIns
'		response.end
		
		Set RsC = oConn.execute(sSqlIns)
		
		response.write "<script>$('#select').append('<option value="""" >Escolha o Tamanho</option>');</script>"		
		if (not RsC.Eof) Then
			do while (not RsC.Eof )
				response.write "<script>$('#select').append('<option value="""& trim(RsC("campoC")) &""" >"& trim(RsC("Descricao")) &"</option>');</script>"
			RsC.Movenext
			loop
		end if

		set RsC = nothing

		call fecha_Conexao()
end select
'response.write sSqlIns
'response.write  "<script>$('#load_cor').html("""& sSqlIns &"""); </script>"
 %>
