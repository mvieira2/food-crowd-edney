<!--#include virtual="/hidden/funcoes.asp"-->
<%
Dim sPathAccess, oConnCEP, stringBD_CEP, sConteudo
Dim sResultado, sCep, sFormato
Dim sUf, sCidade, sBairro, sLogradouro

sCep =	Replace_Caracteres_Especiais(request("cupom"))

if (sCep <> "") then call  Envia_Cep()

' =======================================================================
function Replace_Caracteres_Especiais(prConteudo)

	if(trim(prConteudo) <> "" and InStr(lcase(trim(prConteudo)),"cript") > 0) then
		response.write "<P><div align=""center"">ERRO NO SISTEMA!</div></P>"
		response.end
	end if

	Replace_Caracteres_Especiais = replace(replace(replace(replace(replace(replace(replace(replace(prConteudo, "'", ""), """", ""), ")", ""), "(", ""), ";", ""), "|", ""), "%", ""), "`", "")
end function

'========================================================================
function Envia_Cep()

    Select case lcase(trim(sCep))

        case "crowd1000"

            boolValidoCArrinho = false

            if(InStr(lcase(trim(sCep)), "crowd1000") > 0) then 

                if(bVerificaCupom(lcase(trim(sCep)), 100)) then 
                    boolValidoCArrinho      = true
                else
                    boolValidoCArrinho      = false
                end if
    
            else
                boolValidoCArrinho      = false
            end if

            if(boolValidoCArrinho) then 
                response.write "1"
            else
                SeSsion("cupomdesconto")        = ""
                SeSsion("cupomdescontovalor")   = 0
                response.write "0"
            end if


        case "maisde1000"
            boolValidoCArrinho = false

            if(InStr(lcase(trim(sCep)), "maisde1000") > 0) then 

                if(bVerificaCupom(lcase(trim(sCep)), 50)) then 
                    boolValidoCArrinho      = true
                else
                    boolValidoCArrinho      = false
                end if
    
            else
                boolValidoCArrinho      = false
            end if

            if(boolValidoCArrinho) then 
                response.write "1"
            else
                SeSsion("cupomdesconto")        = ""
                SeSsion("cupomdescontovalor")   = 0
                response.write "0"        
            end if


        case "a5led"        ' 499
            boolValidoCArrinho = false

            For i = 1 to Session("ItemCount")
                if(ARYShoppingcart(C_iD, i)  = "500566") then 
                    boolValidoCArrinho          = true
                end if
            next

            if(boolValidoCArrinho) then 
                response.write "1"
            else
                response.write "0"        
            end if


        case "idol4"        ' 629
            boolValidoCArrinho = false

            For i = 1 to Session("ItemCount")
                if(ARYShoppingcart(C_iD, i)  = "500574") then 
                    boolValidoCArrinho          = true
                end if
            next

            if(boolValidoCArrinho) then 
                response.write "1"
            else
                response.write "0"        
            end if

        case "a7"   ' 749
            boolValidoCArrinho = false

            For i = 1 to Session("ItemCount")
                if(ARYShoppingcart(C_iD, i)  = "683703") then 
                    boolValidoCArrinho          = true
                end if
            next

            if(boolValidoCArrinho) then 
                response.write "1"
            else
                response.write "0"        
            end if


        case "alcatelsemp"
            boolValidoCArrinho = true

            if(boolValidoCArrinho) then 
                response.write "1"
            else
                response.write "0"        
            end if
    
        case "heptafriday"
            boolValidoCArrinho = false

            For i = 1 to Session("ItemCount")
                if(ARYShoppingcart(C_iD, i)  = "683703") then 
                    boolValidoCArrinho          = true
                end if
            next

            if(boolValidoCArrinho) then 
                response.write "1"
            else
                response.write "0"        
            end if

        case "selfiealcatel"
            boolValidoCArrinho = false

            For i = 1 to Session("ItemCount")
                if(ARYShoppingcart(C_iD, i)  = "681931" OR ARYShoppingcart(C_iD, i)  = "681932") then 
                    boolValidoCArrinho          = true
                end if
            next

            if(boolValidoCArrinho) then 
                response.write "1"
            else
                response.write "0"        
            end if


        case "proxymediaacessorios"
            boolValidoCArrinho = false

            For i = 1 to Session("ItemCount")
                IdCategoriaCarrinho = Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID, i), "id", "Id_Categoria")       
                if(IdCategoriaCarrinho = "196238") then boolValidoCArrinho = true: exit for
            next

            if(boolValidoCArrinho) then 
                if(InStr(Session("bannerid"), "proxymedia") > 0) then         
                    response.write "1"
                else
                    response.write "0"        
                end if
            else
                response.write "0"        
            end if


        case "proxymedia5"
            boolValidoCArrinho = false

            if(InStr(Session("bannerid"), "proxymedia") > 0) then 
                boolValidoCArrinho      = true
            else
                boolValidoCArrinho      = false
            end if

            if(boolValidoCArrinho) then 
                response.write "1"
            else
                response.write "0"        
            end if


        case "timaocampeao"
            boolValidoCArrinho = false

            For i = 1 to Session("ItemCount")
                if(ARYShoppingcart(C_iD, i)  = "681931" OR ARYShoppingcart(C_iD, i)  = "681932") then 
                    sDescontoValorProduto       = "109,90"    
                    boolValidoCArrinho          = true
                end if
            next

            if(boolValidoCArrinho) then 
                response.write "1"
            else
                response.write "0"        
            end if

        case "10off"
            boolValidoCArrinho = false
            boolValidoCArrinho = true

            if(boolValidoCArrinho) then 
                response.write "1"
            else
                response.write "0"        
            end if
    
        case "10acessorio"
            boolValidoCArrinho = false

            For i = 1 to Session("ItemCount")
                IdCategoriaCarrinho = Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID, i), "id", "Id_Categoria")       
                if(IdCategoriaCarrinho = "196238") then boolValidoCArrinho = true: exit for
            next

            if(boolValidoCArrinho) then 
                response.write "1"
            else
                response.write "0"        
            end if
    
        case "fieltorcedor"
            boolValidoCArrinho = false

            For i = 1 to Session("ItemCount")
                if(ARYShoppingcart(C_iD, i)  = "500242" OR ARYShoppingcart(C_iD, i)  = "500550") then 
                    sDescontoValorProduto       = "9,90"    
                    boolValidoCArrinho          = true
                end if
            next

            if(boolValidoCArrinho) then 
                response.write "1"
            else
                response.write "0"        
            end if

        case else
    	    response.write "0"
    end select 
end function



'========================================================================
Function RemoveAcentos(ByVal Texto)
    Dim ComAcentos
    Dim SemAcentos
    Dim Resultado
	Dim Cont
    'Conjunto de Caracteres com acentos
    ComAcentos = "����������������������������������������������"
    'Conjunto de Caracteres sem acentos
    SemAcentos = "AIOUEAIOUEAIOUEAOAIOUEaioueaioueaioueaoaioueCc"
    Cont = 0
    Resultado = Texto
    Do While Cont < Len(ComAcentos)
	Cont = Cont + 1
	Resultado = Replace(Resultado, Mid(ComAcentos, Cont, 1), Mid(SemAcentos, Cont, 1))
    Loop
    RemoveAcentos = Resultado
End Function

%>