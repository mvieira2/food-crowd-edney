<!--#include virtual="/hidden/funcoes.asp"-->
<%
Dim sPathAccess, oConnCEP, stringBD_CEP, sConteudo
Dim sResultado, sCep, sFormato
Dim sUf, sCidade, sBairro, sLogradouro

sCep		=	Replace_Caracteres_Especiais(request("cep"))
sFormato	=	lCase(trim(request("formato")))

if (sCep <> "") then call  Envia_Cep()
if (sFormato = "") then sFormato = "javascript"


' =======================================================================
function Replace_Caracteres_Especiais(prConteudo)

	if(trim(prConteudo) <> "" and InStr(lcase(trim(prConteudo)),"cript") > 0) then
		response.write "<P><div align=""center"">ERRO NO SISTEMA!</div></P>"
		response.end
	end if

	Replace_Caracteres_Especiais = replace(replace(replace(replace(replace(replace(replace(replace(replace(prConteudo, "'", ""), """", ""), ")", ""), "(", ""), ";", ""), "|", ""), "%", ""), "`", ""),"-","")
end function

'========================================================================
function Envia_Cep()
	Dim txtCep, srvXMLHttp, xmlResult, sUrl, sPost
    dim resultado(7)  
    dim arr_2(14)  

	On Error Resume Next
	
	' sUrl	= "http://www.buscarcep.com.br/?cep="&sCep&"&formato=xml&chave=1iyggeSRrFZjWgqKsXY0mq/L9/4Jqs/"
    sUrl	= "http://republicavirtual.com.br/web_cep.php?cep="& sCep &"&formato=query_string"  
	sPost	= (sUrl & sComplemento)

    ' &resultado=1&resultado_txt=sucesso+-+cep+completo&uf=SP&cidade=S%E3o+Paulo&bairro=Jardim+Oriental&tipo_logradouro=Rua&logradouro=dos+Jatob%E1s

    'response.Write sPost
    'response.End

	' =======================================================================
    set srvXMLHttp = CreateObject("MSXML2.ServerXMLHTTP")   
    srvXMLHttp.open "GET", sUrl, false   
    srvXMLHttp.send ""  
       
    xmlhttp_resultado = srvXMLHttp.responseText   
    set srvXMLHttp = nothing   
	' =======================================================================
    
    arr_resultado = split( xmlhttp_resultado, "&" )  
 
    for i = lbound( arr_resultado ) to ubound( arr_resultado )  
        resultado( i ) = arr_resultado( i )  
    next  
 
    arr = split( join( resultado, "=" ), "=" )  
  
    for i = lbound( arr ) to ubound( arr )  
        arr_2( i ) = replace( arr( i ), "+", " " )  
    next      

	if(Err.Number <> 0) then
		resultado = 0
	end if
	
	if ubound(resultado) > 0 then
		uf            = arr_2(6)
		cidade	      = arr_2(8)
		bairro        = arr_2(10)
		t_logradouro  = arr_2(12)
		logradouro    = arr_2(14)
		cep           = sCep

        'response.Write uf & "<br>"
        'response.Write cidade & "<br>"
        'response.Write bairro & "<br>"
        'response.Write t_logradouro & "<br>"
        'response.Write logradouro & "<br>"
        'response.Write cep & "<br>"
        'response.End

		Select Case sFormato
			Case "json"
				sConteudo = "{""resultado"":""1"",""resultado_txt"":""sucesso - cep completo"",""uf"":"""& Recupera_Id_Provincia(uf) &""",""cidade"":"""& cidade &""",""bairro"":"""& bairro &""",""logradouro"":"""& logradouro &"""}"
				
			Case "javascript"
				sConteudo =  "var resultadoCEP = {'uf': '"& RemoveAcentos(Recupera_Id_Provincia(uf)) &"','uflegenda': '"& uf &"','cidade':'"& RemoveAcentos(cidade) &"','bairro':'"& RemoveAcentos(bairro) &"','logradouro':'"& RemoveAcentos(logradouro) &"','resultado':'1','resultado_txt':'sucesso%20-%20cep%20completo'}"

			Case "xml"
				'sConteudo = sConteudo &"<?xml version=""1.0"" encoding=""UTF-8""?>" & vbcrlf
				sConteudo = sConteudo &"<webservicecep>" & vbcrlf
				sConteudo = sConteudo &"<uf>"& uf &"</uf>" & vbcrlf
				sConteudo = sConteudo &"<cidade>"& cidade &"</cidade>" & vbcrlf
				sConteudo = sConteudo &"<bairro>"& bairro &"</bairro>" & vbcrlf
				sConteudo = sConteudo &"<logradouro>"& logradouro &"</logradouro>" & vbcrlf
				sConteudo = sConteudo &"<resultado>1</resultado>" & vbcrlf
				sConteudo = sConteudo &"<resultado_txt>sucesso - cep completo</resultado_txt>" & vbcrlf
				sConteudo = sConteudo &"</webservicecep>" & vbcrlf
				
			Case "query_string"
				sConteudo =  "&uf="& Recupera_Id_Provincia(uf) &"&cidade="& replace(cidade," ","+") &"&bairro="& replace(bairro," ","+") &"&logradouro="& replace(logradouro," ","+") &"&resultado=1&resultado_txt=sucesso+-+cep+completo"
		end select



	end if
	
	' Esvazia vari�veis de Objetos
	Set srvXMLHttp = Nothing
	Set xmlResult  = Nothing

	response.write sConteudo
end function

' =======================================================================
function Recupera_Name_Provincia(sDescProvincia)
	dim Rs1, sSql1, sCampoIf
	call abre_conexao()
	sSql1 = "Select top 1 Descricao from TB_ESTADOS (nolock) where Sigla = '"& sDescProvincia &"' "
	Set Rs1 = oconn.execute(sSql1)
	if(not rs1.eof) then Recupera_Name_Provincia = trim(rs1("Descricao"))
	Set Rs1 = nothing
end function
' =======================================================================


' =======================================================================
function Recupera_Id_Provincia(sDescProvincia)
	dim Rs1, sSql1, sCampoIf
	call abre_conexao()
	sSql1 = "Select top 1 id from TB_ESTADOS (nolock) where Sigla = '"& sDescProvincia &"' "
	Set Rs1 = oconn.execute(sSql1)
	if(not rs1.eof) then Recupera_Id_Provincia = trim(rs1("id"))
	Set Rs1 = nothing
end function
' =======================================================================


Function RemoveAcentos(ByVal Texto)
    Dim ComAcentos
    Dim SemAcentos
    Dim Resultado
	Dim Cont
    'Conjunto de Caracteres com acentos
    ComAcentos = "����������������������������������������������"
    'Conjunto de Caracteres sem acentos
    SemAcentos = "AIOUEAIOUEAIOUEAOAIOUEaioueaioueaioueaoaioueCc"
    Cont = 0
    Resultado = Texto
    Do While Cont < Len(ComAcentos)
	Cont = Cont + 1
	Resultado = Replace(Resultado, Mid(ComAcentos, Cont, 1), Mid(SemAcentos, Cont, 1))
    Loop
    RemoveAcentos = Resultado
End Function

%>