﻿
function preenche_endereco(cep, prNomeDiv) {
    if ($.trim(cep) != '') {

        if (cep.length == 8) {
            $("#img_loading").show();
        var rootPath = '/ajax/cep_ajax.asp?formato=javascript&cep=' + cep;

        $.getScript(rootPath, function () {
            $("#img_loading").hide();
            if (resultadoCEP["retorno"] != "") {


                if (resultadoCEP["logradouro"] != "") {

					$('#pais').val("Brasil");
	                $('#pais').attr('readonly', true);
				
                    $('#endereco').attr('readonly', true);
                    $("#endereco").val(unescape(resultadoCEP["logradouro"]));
                }
                else {
                    $('#endereco').attr('readonly', false);
                    $('#pais').attr('readonly', false);
	
	            }

                if (resultadoCEP["bairro"] != "") {
                    $('#bairro').attr('readonly', true);
                    $("#bairro").val(unescape(resultadoCEP["bairro"]));
                }
                else {
                    $('#bairro').attr('readonly', false);
                }

                if (resultadoCEP["cidade"] != "") {
                    $('#cidade').attr('readonly', true);
                    $("#cidade").val(unescape(resultadoCEP["cidade"]));
                }
                else {
                    $('#cidade').attr('readonly', false);
                }

                if (resultadoCEP["uf"] != "") {
                    $('#provinciaLegenda').attr('readonly', true);
                    $("#provinciaLegenda").val(unescape(resultadoCEP["uf"]));
                }
                else {
                    $('#provincia').attr('readonly', false);
                }
                if (resultadoCEP["bairro"] == "" && resultadoCEP["logradouro"] == "") {
                    AbrePopUp("/ajax/pop_cep/procurar_cep_atual.asp?enviado=ok&formato=javascript&cep=" + cep, '420', '500');
                }

                if (prNomeDiv !=) {
                    $("#" + prNomeDiv).load('/hidden/calculo_frete_detalhes.asp', { tipo: 'CESTA', cepAG: cep, valor: 0, PaginaPedido: '1', tiposaida: '', CidadeFrete: resultadoCEP["cidade"], EstadoFrete: resultadoCEP["uflegenda"] }, function () {
                }

                    
                });

            }
        });
	 }
	  else
	  {
            alert("CEP Inválido!");
            $('#cep').val('');
            $('#cep').focus();
            return false;
	  }
    }
}