<!--#include virtual= "/hidden/funcoes.asp"-->

<%
dim nProduto, sNomeProduto, sNomeProdutoSeo, sNomeCod, boolLancamento, boolPromocao, sNomeProdutoJP
dim sNomeCategoria, sNomeSubCategoria, sTamanho, sCores, sFrangrancias, sSabores, sTipos, sPacotes, sLeiaMais, sUrlEspecial, sFabricantes
dim sImagem, sImagem2, sImagem3,sAutor, sCampoCombo, boolComprar, sEstoque, sSimbolo, sPrecoQuick, sPrecoOficialGECalculo, sPrecoAcres, ArrPrecoAcres, sCampoAuxSelected, sValorAuxSelected, sDescricaojp
dim OpcTam, OpcCor, OpcSabor, OpcFrag, OpcTipo, OpcTamSel, OpcCorSel, OpcSaborSel, OpcFragSel, OpcTipoSel
dim OpcPac, OpcPacSel, boolMaisVendido, boolEsgotado, sDataPreVendaIni, sDataPreVendaFim, sNomeFabricante, sIdFabricante, sNomeEditora
dim sPrecoPrazoQuick, sParcelamento, sMsgCartao, sLegenda, sDestaque, sDestaqueJP, sValorAtualCor
dim boolGE, sValorGe, OpcGe, imagem_pdf, sImagem5
dim sOnload,slistadesejo,sTxtFacebook,sCorAtiva
Dim iPageCount, iRecordCount, iQualPagina, iExibe, sUrlCompra, sBoolVazio
Dim sCepDigitadoFrete, sPesoFrete, sValorProdutoFrete, sTrailer, sTrechos,sOnloadBody,sFundoD
dim sprimeira_gravacao, ssegunda_gracacao, sPalavrasChaves, sUrlAtual
dim sBannerPagina
dim prTipo4, sNomeProdutoBreve
dim sClassHeader: sClassHeader = "bg-1"

sPrecoAcres 		= ""
nProduto 			= Replace_Caracteres_Especiais(request("produto"))
OpcTam 				= Replace_Caracteres_Especiais(request("tamanhos"))
OpcCor 				= Replace_Caracteres_Especiais(request("cores"))
OpcSabor 			= Replace_Caracteres_Especiais(request("sabores"))
OpcFrag 			= Replace_Caracteres_Especiais(request("fragrancias"))
OpcTipo 			= Replace_Caracteres_Especiais(request("tipos"))
OpcPac 				= Replace_Caracteres_Especiais(request("pacotes"))
OpcGe 				= Replace_Caracteres_Especiais(request("ge"))
sFabricantes		= Replace_Caracteres_Especiais(request("Id_Fabricante"))
sOnload				= Replace_Caracteres_Especiais(request("onAva"))
sCepDigitadoFrete	= Replace_Caracteres_Especiais(trim(request("cepd")))
slistadesejo 		= Replace_Caracteres_Especiais(request("listadesejo"))
sCorAtiva           = Replace_Caracteres_Especiais(trim(request("cativo"))) 
sprimeira_gravacao  = Replace_Caracteres_Especiais(trim(request("primeira_gravacao"))) 
ssegunda_gracacao   = Replace_Caracteres_Especiais(trim(request("segunda_gracacao"))) 
sGoogle     		= trim(Recupera_Campos_Db("TB_PRODUTOS", nProduto, "id", "Descricao_Jp"))
sUrlAtual           = Request.ServerVariables("HTTP_X_ORIGINAL_URL")

if(InStr(lcase(sUrlAtual), "/110v/") = 0 and InStr(lcase(sUrlAtual), "/220v/") = 0) then sUrlAtual = "/110v/"

if (Session("idProdutoVisitados") <> "") then
    Session("idProdutoVisitados")       = Session("idProdutoVisitados") & ","& nProduto
else
    Session("idProdutoVisitados")       = nProduto
end if

if(nProduto = "") then
	response.write "<P><div align=""center"">ERRO NO SISTEMA!</div></P>"
	response.end
end if
if(nProduto <> "" and not isnumeric(nProduto)) then
	response.write "<P><div align=""center"">ERRO NO SISTEMA!</div></P>"
	response.end
end if

sTxtFacebook        = RemoveHTMLTags(trim(Recupera_Campos_Db("TB_PRODUTOS", nProduto, "id", "DescricaoC")))
sUrlCompra		    = trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", nProduto, "id", "UrlCompra"))

if(sUrlCompra <> "") then
	response.write "<P><div align=""center"">ERRO NO SISTEMA(URL)!</div></P>"
	response.end
end if

 
' PAGINACAO =============================================================
If (Replace_Caracteres_Especiais(Request("Pagina")) = "") Then
	iQualPagina = 1
Else
	if(isnumeric(Replace_Caracteres_Especiais(Request("Pagina")))) then 
		iQualPagina = CInt(Replace_Caracteres_Especiais(Request("Pagina")))
	else
		iQualPagina = 1
	end if
End If

if(sPrecoAcres = "") then sPrecoAcres = 0
boolMaisVendido = true
boolGE          = false

if(OpcGe <> "") then 
	sPrecoAcres = cdbl(OpcGe)
end if

if(OpcTam <> "") then 
	if(InStr(OpcTam,"-") > 0) then 
		ArrPrecoAcres = split(OpcTam,"-")
		OpcTamSel = ArrPrecoAcres(0)
		sPrecoAcres = cdbl(ArrPrecoAcres(1))
	else
		OpcTamSel = OpcTam
	end if
end if
if(OpcCor <> "") then 
	if(InStr(OpcCor,"-") > 0) then 
		ArrPrecoAcres = split(OpcCor,"-")
		OpcCorSel = ArrPrecoAcres(0)
		sPrecoAcres = (sPrecoAcres + cdbl(ArrPrecoAcres(1)))
	else
		OpcCorSel = OpcCor
	end if
end if
if(OpcSabor <> "") then 
	if(InStr(OpcSabor,"-") > 0) then 
		ArrPrecoAcres = split(OpcSabor,"-")
		OpcSaborSel = ArrPrecoAcres(0)
		sPrecoAcres = cdbl((sPrecoAcres + cdbl(ArrPrecoAcres(1))))
	else
		OpcSaborSel = OpcSabor
	end if
end if
if(OpcFrag <> "") then 
	if(InStr(OpcFrag,"-") > 0) then 
		ArrPrecoAcres = split(OpcFrag,"-")
		OpcFragSel = ArrPrecoAcres(0)
		sPrecoAcres = cdbl((sPrecoAcres + cdbl(ArrPrecoAcres(1))))
	else
		OpcFragSel = OpcFrag
	end if
end if
if(OpcTipo <> "") then 
	if(InStr(OpcTipo,"-") > 0) then 
		ArrPrecoAcres = split(OpcTipo,"-")
		OpcTipoSel = ArrPrecoAcres(0)
		sPrecoAcres = cdbl((sPrecoAcres + cdbl(ArrPrecoAcres(1))))
	else
		OpcTipoSel = OpcTipo
	end if
end if
if(OpcPac <> "") then 
	if(InStr(OpcPac,"-") > 0) then 
		ArrPrecoAcres = split(OpcPac,"-")
		OpcPacSel = ArrPrecoAcres(0)
		sPrecoAcres = cdbl((sPrecoAcres + cdbl(ArrPrecoAcres(1))))
	else
		OpcPacSel = OpcPac
	end if
end if
call Abre_Conexao()


' RECUPERA O CATEGORIA E SUB CATEGORIA ==================================
if(nProduto <> "" and isnumeric(nProduto)) then
    sSql = 		" SELECT top 1 " & _
					"TB_PRODUTOS.*, " & _
                    "TB_CATEGORIAS.capa, " & _
                    "TB_CATEGORIAS.categoria, " & _
                    "TB_CATEGORIAS.PalavrasChaves, " & _
                    "TB_CATEGORIAS.UrlEspecial, " & _
                    "TB_SUBCATEGORIA.SubCategoria " & _
				" FROM  " & _
					" TB_PRODUTOS (nolock) " & _
					" INNER JOIN TB_CATEGORIAS (NOLOCK) on  TB_PRODUTOS.Id_Categoria = TB_CATEGORIAS.Id and TB_CATEGORIAS.ativo = 1 AND TB_CATEGORIAS.Id_Tipo = 'S' " & _
					" INNER JOIN TB_SUBCATEGORIA (NOLOCK) on TB_CATEGORIAS.Id = TB_SUBCATEGORIA.Id_Categoria and TB_SUBCATEGORIA.ativo = 1 and TB_PRODUTOS.Id_SubCategoria = TB_SUBCATEGORIA.Id " & _
				" WHERE  " & _
                    " TB_PRODUTOS.id = " & nProduto
	set oRsProduto = oConn.execute(sSql)

	If (Not oRsProduto.Eof) Then

	    sCampoCombo 		= "Descricao"
	    sNomeProduto 		= trim(oRsProduto("Descricao"))
	    sNomeProdutoBreve 	= trim(oRsProduto("DescricaoBreve"))
        sNomeProdutoJP 		= oRsProduto("Descricao_JP")
	    sDestaque 			= trim(oRsProduto("Destaque"))
        sDestaqueJP 		= trim(oRsProduto("Destaque_JP"))
	    sNomeCategoria 		= oRsProduto("categoria")
	    sNomeSubCategoria 	= oRsProduto("SubCategoria")
        sIdSubCategoria 	= oRsProduto("id_subcategoria")
	    sNomeEditora		= oRsProduto("Fabricante")
	    sPesoFrete			= oRsProduto("Peso")
	    sIdFabricante		= oRsProduto("id_fabricante") 
        sIdCat 			    = oRsProduto("id_categoria")
        sTrailer            = oRsProduto("Trailer")
        Boolbundle          = oRsProduto("Boolbundle")
        sFundoD             = oRsProduto("capa") 
        Boolsugestao        = trim(oRsProduto("Boolsugestao"))
	    sNomeProdutoSeo		= TrataSEO(oRsProduto("Descricao"))
        id_categoria        = oRsProduto("id_categoria")
	    sNomeCategoriaSeo 	= TrataSEO(oRsProduto("categoria"))
        sPalavrasChaves     = TrataSEO(oRsProduto("PalavrasChaves"))
	    sNomeSubCategoriaSeo= TrataSEO(oRsProduto("SubCategoria"))
	    sUrlEspecial	    = trim(oRsProduto("UrlEspecial"))

	    sFabricanteSeo 		= TrataSEO(Recupera_Campos_Db_oConn("TB_FABRICANTE", oRsProduto("id_fabricante"), "id", "Fabricante"))
        sTrechos            = Recupera_Campos_Db_oConn("TB_PRODUTOS_TRECHOS", nProduto, "id_produto", "Arquivo")
	    sNomeFabricante		= Recupera_Campos_Db_oConn("TB_FABRICANTE", oRsProduto("id_fabricante"), "id", "Fabricante")
	    boolLancamento 	    = (oRsProduto("lancamento") = "1")
	    boolPromocao 	    = (oRsProduto("promocao") = "1")

	    boolComprar 	    = ((oRsProduto("ativo") = "1") and _ 
					    	    (cdbl(oRsProduto("Qte_Estoque")) > 0) and _ 
					    	    (Verifica_Status_Loja()))

	    boolEsgotado 	    = (cdbl(oRsProduto("Qte_Estoque")) = 0 or cdbl(oRsProduto("Qte_Estoque")) < 0)

	    boolGE 			    = (oRsProduto("ge") = "1")
	
	    sNomeCod 		    = oRsProduto("CodReferencia")
	    sLeiaMais		    = oRsProduto("MsgCartao")

	    sImagem 		    = oRsProduto("Imagem")	
        sImagem2 		    = oRsProduto("Imagem2")
	    sImagem3 		    = oRsProduto("Imagem3")
	    sImagem4 		    = oRsProduto("Imagem4")
        sImagem5 		    = oRsProduto("Imagem5")
        sImagem6 		    = oRsProduto("Imagem6")
        sImagem7 		    = oRsProduto("Imagem7")

	    'sImagemP 		    = Alterar_Tamanho_Imagem("p", "", "", sImagem, 94, 63)
        'sImagem2P 		    = Alterar_Tamanho_Imagem("p", "", "", sImagem2, 94, 63)
	    'sImagem3P 		    = Alterar_Tamanho_Imagem("p", "", "", sImagem3, 94, 63)
	    'sImagem4P 		    = Alterar_Tamanho_Imagem("p", "", "", sImagem4, 181, 154)

	    sImagemG 		    = Alterar_Tamanho_Imagem("g", "", "", sImagem, 325, 250)
        sImagem2G 		    = Alterar_Tamanho_Imagem("g", "", "", sImagem2, 325, 250)
	    sImagem3G 		    = Alterar_Tamanho_Imagem("g", "", "", sImagem3, 325, 250)
	    sImagem4G 		    = Alterar_Tamanho_Imagem("g", "", "", sImagem4, 573, 318)
        sImagem5G 		    = Alterar_Tamanho_Imagem("g", "", "", sImagem5, 573, 318)
        sImagem6G 		    = Alterar_Tamanho_Imagem("g", "", "", sImagem6, 573, 318)
        sImagem7G 		    = Alterar_Tamanho_Imagem("g", "", "", sImagem7, 573, 318)

	    sTamanho 		    = oRsProduto("Id_Tamanho")
	    sCores 			    = oRsProduto("Id_Cor")
	    sFrangrancias 	    = oRsProduto("Id_Fragrancia")
	    sSabores 		    = oRsProduto("Id_Sabor")
	    sTiposProduto	    = oRsProduto("Id_Tipos")
	    sPacotesProduto	    = oRsProduto("Id_Pacote")
	    sAutor 			    = oRsProduto("Autor")
	    sDescricaoCompleta  = oRsProduto("DescricaoC")
   
	    sDescricaojp 	    = oRsProduto("Descricaoc_jp")
	    sEstoque 		    = oRsProduto("Qte_Estoque")
	    sSimbolo 		    = Server.HtmlEncode(Recupera_Campos_Db_oConn("TB_MOEDAS", oRsProduto("Id_Moeda"), "id", "Simbolo"))
	    sPrecoQuick		    = RetornaCasasDecimais(oRsProduto(sCampoPreco),2)
	    sPrecoQuickH		= RetornaCasasDecimais(oRsProduto(sCampoPreco),2)
	    sPrecoPrazoQuick    = RetornaCasasDecimais(oRsProduto("Preco_aprazo"),2)
	    sParcelamento 	    = oRsProduto("qte_parcelamento")
	    sMsgCartao 		    = oRsProduto("MsgCartao")
	    sDataPreVendaIni    = trim(oRsProduto("DataIniPreVenda"))
	    sDataPreVendaFim    = trim(oRsProduto("DataFimPreVenda"))

        keywords            = trim(oRsProduto("keywords"))

	
	    if((sImagem = "") or (isnull(sImagem))) then sImagem = "img_indisponivel.jpg"
	    if(sMsgCartao <> "" and not isnull(sMsgCartao)) then sMsgCartao = replace(sMsgCartao, "#", """")
	
	    Session(nProduto) 		= sPrecoQuick

	    if(sPrecoAcres > 0) then 
		    Session(nProduto) 	= (Session(nProduto) + cdbl(sPrecoAcres))
		    sPrecoPrazoQuick    = (sPrecoPrazoQuick + cdbl(sPrecoAcres))
		    sPrecoQuick         = (sPrecoQuick + cdbl(sPrecoAcres))
	    end if

	    if(boolPromocao) then 
		    if(oRsProduto("DescontoValor") <> "" and oRsProduto("DescontoValor") <> "0") then 
			    Session("d" & nProduto) = cdbl(oRsProduto("DescontoValor"))
		    end if
        else
            Session("d" & nProduto) = 0
	    end If
	
	    if((sDataPreVendaIni) <> "" and not isnull(sDataPreVendaIni)) then sDataPreVendaIni = cdate(sDataPreVendaIni)
	    if((sDataPreVendaFim) <> "" and not isnull(sDataPreVendaFim)) then sDataPreVendaFim = cdate(sDataPreVendaFim)

	    if(trim(oRsProduto("Prevenda")) = "1") then 
		    boolPreVenda = (date >= sDataPreVendaIni AND date <= sDataPreVendaFim)
	    end if

	    sValorProdutoFrete = sPrecoQuick
	
        sTitleProduto       = trim(oRsProduto("description"))
        if(sTitleProduto = "" OR isnull(sTitleProduto)) then sTitleProduto  = sNomeProduto
        sTitleProduto       = (sTitleProduto & " | R$ "& formatNumber((cdbl(sPrecoQuick) - cdbl(Session("d" & nProduto))),2) & " | " & Application("URLdaLoja"))


	    if(InStr("," & sCdCliente & ",", "," & oRsProduto("cdcliente") & ",") = 0) then
		    response.write "<P><div align=""center"">ERRO NO SISTEMA (ADMIN)</div></P>"
		    response.end
	    end if	
	    if(oRsProduto("ativo") <> "1") then
		    response.write "<P><div align=""center"">ERRO NO SISTEMA (P)</div></P>"
		    response.end
	    end if
	     if(Recupera_Campos_Db_oConn("TB_CATEGORIAS", oRsProduto("id_categoria"), "id", "ativo") <> "1") then
		    response.write "<P><div align=""center"">ERRO NO SISTEMA (C)</div></P>"
		    response.end
	     end if
	    if(Recupera_Campos_Db_oConn("TB_SUBCATEGORIA", oRsProduto("id_subcategoria"), "id", "ativo") <> "1") then
		    response.write "<P><div align=""center"">ERRO NO SISTEMA (S)</div></P>"
		    response.end
	    end if
   
    End If 
    set oRsProduto = nothing
else
	response.write "<P><div align=""center"">ERRO NO SISTEMA</div></P>"
	response.end
end if


' ==========================================
sub GravarEsgotado()
    dim sSql, sTipo
	dim sData

	if(Session("Id") = "") then exit sub
	
	call Abre_Conexao()
	
	sSql = "Select * from TB_LISTA_ESGOTADOS (NOLOCK) where IdProduto = "& nProduto &" AND IdCliente = " & Session("Id") & " and (Notificado = 0 or isnull(Notificado,'') = '')"
	set Rs = oConn.execute(sSql)
	
    if(rs.eof) then
		sSql = "Insert into TB_LISTA_ESGOTADOS (IdProduto, IdCliente, Notificado,DataCad) values ("& nProduto &", "& Session("Id") &", 0, getdate())"
		oConn.execute(sSql)

	    response.Write "<script>alert('Pedido de notificação recebida com sucesso\n\nAvisaremos assim que o produto estiver disponível!')</script>"
    else
		response.write  "<script>alert('Esse produto ja foi inserido na sua lista de notificações de produtos ESGOTADOS\n\n\Entre na sua Area Cadastral para acompanhar!')</script>"

    end if
    set Rs = nothing

	boolGravado = true
	call Fecha_Conexao()
end sub


' =========================================================================
function Verificar_Estoque_Variavel(prTabelaVariacao, prCampoProdutos, prIdProduto)

	Verificar_Estoque_Variavel = false

	if(prIdProduto  = "") then exit function

	dim lContArray, ArraySplit, sArray2
	
	ArraySplit 	= split(sIdProdutosVariacao, ",")
	sArray2 	= Session("ArrayEstoque")

	for lContArray = 0 to ubound(sArray2)
		if(trim(sArray2(lContArray)) <> "") then 

			if(cdbl(trim(sArray2(lContArray))) = cdbl(trim(prIdProduto))) then
				Verificar_Estoque_Variavel = true
			end if
		end if
	next
end function



' =========================================================================
function Recupera_Acrescimo(Valor, Campo)
	dim Rs, sSQl
	
	Recupera_Acrescimo = ""
	
	sSQl = "Select * from TB_IMAGENS_AUXILIARES where id_produto = "& nProduto &" and "& Campo &" = "& valor
	set Rs = oConn.execute(sSQl)
	if(not rs.eof) then Recupera_Acrescimo = rs(sCampoAcrescimoLogin)
	set Rs = nothing
end function

' =========================================================================
function Verifica_Registro_Auxiliar(sCampo, Valor)
	dim RsTB, sSQlAu
	
	if(trim(Valor) = "" or isnull(Valor)) then exit function
	
	sSQlAu = "Select imagem from TB_IMAGENS_AUXILIARES where id_produto = "& nProduto &" and "& sCampo &" in ("& Valor & ")"
	set RsTB = oConn.execute(sSQlAu)
	if(not RsTB.eof) then 
        Verifica_Registro_Auxiliar = RsTB("imagem")
        set RsTB = nothing
    end if

end function

' =============================================================================
Function BoolTemVideo()
    Dim sSql
    BoolTemVideo    =   false
    Call Abre_Conexao()

    sSql = "select id from TB_PUBLICACOES (nolock) where CdCliente = "&sCdCliente&" and id_tipo = 4 and ativo = 1 and resumo like '" & CSTR(nProduto) & "'"
    Set Rs = oConn.execute(sSql)
    if (not rs.eof) then BoolTemVideo = true
    Set Rs = nothing
	
	Call Fecha_Conexao()
End Function


' =============================================================================
Function Retorna_Sabores(prProduto)
    dim sSql2, sRs2, sPreco
    dim sCampo1, sCampo2, sCampo3, sReturnConteudo, sClass, lcont

    if(prProduto = "") then exit function

    Retorna_Sabores = ""

    call Abre_Conexao()

    sSql2 = "select * from TB_SABORES T (nolock) where T.cdcliente = "& sCdCliente &" and T.ativo = 1 and " & _ 
            " EXISTS (SELECT T.id FROM TB_IMAGENS_AUXILIARES AX (nolock) WHERE AX.id_produto = '"& prProduto &"' and AX.Id_Sabor = T.id and isnull(AX.CodAbacos,'') <> '' " & _ 
            " and AX.Qtd_Estoque > 0) order by t.descricao "
	'response.write sSql2
    'response.End
    SET sRs2 = oConn.Execute(sSql2)

    if (not sRs2.eof) then

        lcont = 1

        do while (not sRs2.eof)
			sCampo1      = trim(sRs2("ID")) 
            sCampo2      = trim(sRs2("DESCRICAO"))
            sCampo3      = trim(sRs2("DESCRICAO_jp"))
            sChecked     = ""

	        sSQL = "Select top 1 Qtd_Estoque, Acrescimo_Consumidor from TB_IMAGENS_AUXILIARES (NOLOCK) WHERE id_produto = "& prProduto &" and Id_Sabor = " & sCampo1
	        set Rs = oConn.execute(sSQL)
	        if(not rs.eof) then 
                sPreco      = formatNumber(rs("Acrescimo_Consumidor"),2)
                sEstoque    = cint(rs("Qtd_Estoque"))
            end if

	        set Rs = nothing

            if(OpcSabor <> "" and IsNumeric(OpcSabor)) then 
                if(cint(OpcSabor) = cint(sCampo1)) then sChecked = " checked "
            else
                if(lcont = 1) then OpcSabor = sCampo1
            end if
                
            if(sEstoque > 0) then 
                sReturnConteudo = sReturnConteudo & "<p style=""margin-bottom:2px;""><input type=""radio"" "& sChecked &" name=""id_sabor"" id=""id_sabor"" value="""& sCampo1 &""" />&nbsp;"& sCampo2 & " cm - <b>R$ "& sPreco &"</b> </p>"
            end if

            lcont = lcont + 1
        sRs2.movenext
        loop
    else
        Retorna_Sabores = ""
    end if
    set sRs2 = nothing

	Call Fecha_Conexao()

    Retorna_Sabores = sReturnConteudo
End Function


' =============================================================================
Function Retorna_Tipo(prProduto)
    dim sSql2, sRs2
    dim sCampo1, sCampo2, sCampo3, sReturnConteudo, sClass, lcont

    if(prProduto = "") then exit function

    Retorna_Tipo = ""

    call Abre_Conexao()

    sSql2 = "select * from TB_TIPOS T (nolock) where T.cdcliente = "& sCdCliente &" and T.ativo = 1 and " & _ 
            " EXISTS (SELECT T.id FROM TB_IMAGENS_AUXILIARES AX (nolock) WHERE AX.id_produto = '"& prProduto &"' and AX.Id_Tipo = T.id and isnull(AX.CodAbacos,'') <> '' " & _ 
            " and AX.Qtd_Estoque > 0) order by t.descricao "
	' response.write sSql2
    ' response.End
    SET sRs2 = oConn.Execute(sSql2)

    if (not sRs2.eof) then

        lcont = 1

        do while (not sRs2.eof)
			sCampo1 = trim(sRs2("ID")) 
            sCampo2 = trim(sRs2("DESCRICAO"))
            sCampo3 = trim(sRs2("DESCRICAO_jp"))
            
            sBoolchecked = InStr(sUrlAtual, trim(sCampo2)) > 0

            if(sBoolchecked) then 
                checked = " checked "
            else
                checked = ""
            end if

            if(lcont = 1) then 
                sReturnConteudo = sReturnConteudo & "<li><a href=""#"">"& sCampo2 &"</a></li>"
                'sReturnConteudo = sReturnConteudo & "<p style=""margin-bottom:2px;""><input type=""radio"" "& checked &" name=""id_tipo"" id=""id_tipo"" value="""& sCampo1 &""" title="""& sCampo2 &""" onclick=""javascript: MudaURLRadio(this.title);"" />&nbsp;<span class=""preco_parcelas"">"& sCampo2 &"</span></p>"
            else
                'sReturnConteudo = sReturnConteudo & "<p style=""margin-bottom:2px;""><input type=""radio"" "& checked &" name=""id_tipo"" id=""id_tipo"" value="""& sCampo1 &"""  title="""& sCampo2 &"""  onclick=""javascript: MudaURLRadio(this.title);""  />&nbsp;<span class=""preco_parcelas"">"& sCampo2 & "</span></p>"
            end if

            lcont = lcont + 1
        sRs2.movenext
        loop
    else
        Retorna_Tipo = ""
    end if
    set sRs2 = nothing

	Call Fecha_Conexao()

    Retorna_Tipo = sReturnConteudo
End Function




' =============================================================================
Function Retorna_Pacote(prProduto)
    dim sSql2, sRs2
    dim sCampo1, sCampo2, sReturnConteudo, sClass

    call Abre_Conexao()

    sSql2 = "select * from TB_PACOTES T (nolock) where T.cdcliente = "& sCdCliente &" and T.ativo = 1 and EXISTS (SELECT T.id FROM TB_IMAGENS_AUXILIARES AX (nolock) WHERE AX.id_produto = '"& prProduto &"' and AX.Id_Pacote = T.id) order by t.descricao "    
    SET sRs2 = oConn.Execute(sSql2)

    sReturnConteudo = ""

    if (not sRs2.eof) then
        do while (not sRs2.eof)
			sCampo1      = trim(sRs2("ID")) 
            sCampo2      = UCASE(trim(sRs2("DESCRICAO")))

            sClass = "n selected"
            if(lcase(sCampo2) = "sim") Then sClass = "s"
            

            sReturnConteudo = sReturnConteudo & "<input type=""button"" class="""& sClass &""" value="""& sCampo2 &""" name=""gravacao"& sCampo1 &""" id=""gravacao"& sCampo1 &""" onclick=""javascript: document.forma.pacotes.value='"& sCampo1 &"'; document.forma.pacotesdescricao.value='"& sCampo2 &"'; "">"
            
        sRs2.movenext
        loop
    else
        Retorna_Pacote = ""
    end if
    set sRs2 = nothing
	
    Retorna_Pacote = sReturnConteudo

	Call Fecha_Conexao()
End Function

' =============================================================================
Function Retorna_Cor(prProduto)
    dim sSql2, sRs2
    dim sCampo1, sCampo2, sCampo3, sReturnConteudo, sClass

    if(prProduto = "") then exit function

    Retorna_Cor = ""

    call Abre_Conexao()

    sSql2 = "select * from TB_CORES T (nolock) where T.cdcliente = "& sCdCliente &" and T.ativo = 1 and " & _ 
            " EXISTS (SELECT T.id FROM TB_IMAGENS_AUXILIARES AX (nolock) WHERE AX.id_produto = '"& prProduto &"' and AX.Id_Cor = T.id and isnull(AX.CodAbacos,'') <> '' " & _ 
            " and AX.Qtd_Estoque > 0) order by t.descricao "
	
    SET sRs2 = oConn.Execute(sSql2)

    if (not sRs2.eof) then
        do while (not sRs2.eof)
			sCampo1      = trim(sRs2("ID")) 
            sCampo2      = trim(sRs2("DESCRICAO"))
            sCampo3      = trim(sRs2("DESCRICAO_jp"))
            sImagemAux   = Verifica_Registro_Auxiliar("Id_Cor", trim(sRs2("ID")) )

            sReturnConteudo = sReturnConteudo & "<a href=""javascript:void(0);"" rel=""{gallery: 'gal1', smallimage: '" & sLinkImagem &"/EcommerceNew/upload/produto/"& sImagemAux & "',largeimage: '" & sLinkImagem &"/EcommerceNew/upload/produto/"& sImagemAux &"'}"">"
                sReturnConteudo = sReturnConteudo & "<p><input class=""margin-right-15"" type=""radio"" />"& sCampo2 &"</p>"
            sReturnConteudo = sReturnConteudo & "</a>"
        sRs2.movenext
        loop
    else
        Retorna_Cor = ""
    end if
    set sRs2 = nothing

	Call Fecha_Conexao()

    Retorna_Cor = sReturnConteudo
End Function

' =============================================================================
Function Retorna_Tamanho(prProduto)
    dim sSql2, sRs2
    dim sCampo1, sCampo2, sCampo3, sReturnConteudo, sClass

    if(prProduto = "") then exit function
    
    Retorna_Tamanho = ""

    call Abre_Conexao()

    sSql2 = "select * from TB_TAMANHOS T (nolock) where T.cdcliente = "& sCdCliente &" and T.ativo = 1 and " & _ 
            " EXISTS (SELECT T.id FROM TB_IMAGENS_AUXILIARES AX (nolock) WHERE AX.id_produto = '"& prProduto &"' and AX.Id_Tamanho = T.id " & _ 
            " and isnull(AX.Imagem,'') <> '' and isnull(AX.CodAbacos,'') <> '' and AX.Qtd_Estoque > 0) order by t.descricao "
	SET sRs2 = oConn.Execute(sSql2)

    if (not sRs2.eof) then
        do while (not sRs2.eof)
			sCampo1      = trim(sRs2("ID")) 
            sCampo2      = trim(sRs2("DESCRICAO"))
            sCampo3      = trim(sRs2("DESCRICAO_jp"))
            sImagemAux   = Verifica_Registro_Auxiliar("Id_Tamanho", trim(sRs2("ID")) )

            sReturnConteudo = sReturnConteudo & "<a href=""javascript:void(0);"" rel=""{gallery: 'gal1', smallimage: '" & sLinkImagem &"/EcommerceNew/upload/produto/"& sImagemAux & "',largeimage: '" & sLinkImagem &"/EcommerceNew/upload/produto/"& sImagemAux &"'}"">"
            sReturnConteudo = sReturnConteudo & "<p style=""margin-bottom:2px;"">" & sCampo2 & "</p>"
            sReturnConteudo = sReturnConteudo & "</a>"
        sRs2.movenext
        loop
    else
        Retorna_Tamanho = ""
    end if
    set sRs2 = nothing

	Call Fecha_Conexao()

    Retorna_Tamanho = sReturnConteudo
End Function
    

' =================================================================================
function Monta_Esp_Tecnica()
    dim sSql2, sRs2, sConteudo, sDescricao, sDescricaoArray

    call Abre_Conexao()

    sSql2 = " SELECT " &_
	        "     AX.* " &_
            " FROM  " &_
	        "     TB_PRODUTOS_TRECHOS AX (NOLOCK) " &_
            " WHERE  " &_
            "     AX.Id_Produto = "& nProduto & _
	        "     AND ISNULL(descricao,'') <> '' " & _
            " ORDER BY AX.id  "
	SET sRs2 = oConn.Execute(sSql2)

    sConteudo = ""

    if (not sRs2.eof) then
        do while (not sRs2.eof)

        sDescricao      = trim(sRs2("descricao"))
        sDescricaoArray = split(sDescricao, "#")

        sConteudo = sConteudo & " <div>" & _
                    	        "    <span class=""txt_1"">"& sDescricaoArray(0) &"</span>" & _
                                "    <span class=""txt_2"">"& sDescricaoArray(1) &"</span>" & _
                                " </div>"

        sRs2.movenext
        loop
    end if
    set sRs2 = nothing

    Call Fecha_Conexao()

    Monta_Esp_Tecnica = sConteudo
end function

%>


<!DOCTYPE html>
<html lang="en" dir="ltr">

    <!--#include virtual= "head.asp"-->

    <script src="/scripts/script.js" type="text/javascript" ></script>

    <style>
        ol, ul {
            margin-top: 0;
            margin-bottom: 10px;
            line-height: 2.5;
        }
    </style>

    <body>
        <!--#include virtual= "header.asp"-->

        <div class=" desktop-margin-top-90" >

	        <div class="margin-60-0 ">
    		    <div class="container">
			        <div class="row desktop-margin-90 ">
    				    <div class="col-sm-12">

    					    <form name="forma" action="" method="post" id="detalhes">
                            <input type="hidden" name="qty" id="qty" value="1" />

    						    <div class="row">

							        <div class="col-sm-6">
								        <div class="container-slider-fotos">
									        <div class="slider-fotos-produtos-imagem-grande">
										        <% if(sImagemG <> "")  then %><div class="img-item"><img  src="<%= sImagemG  %>"   alt="<%= sNomeProduto %>" title="<%= sNomeProduto %>"></div><% end if %>
										        <% if(sImagem2G <> "") then %><div class="img-item"><img  src="<%= sImagem2G %>"   alt="<%= sNomeProduto %>" title="<%= sNomeProduto %>"></div><% end if %>
										        <% if(sImagem3G <> "") then %><div class="img-item"><img  src="<%= sImagem3G %>"   alt="<%= sNomeProduto %>" title="<%= sNomeProduto %>"></div><% end if %>
										        <% if(sImagem4G <> "") then %><div class="img-item"><img  src="<%= sImagem4G %>"   alt="<%= sNomeProduto %>" title="<%= sNomeProduto %>"></div><% end if %>
                                                <% if(sImagem5G <> "") then %><div class="img-item"><img  src="<%= sImagem5G %>"   alt="<%= sNomeProduto %>" title="<%= sNomeProduto %>"></div><% end if %>
                                                <% if(sImagem6G <> "") then %><div class="img-item"><img  src="<%= sImagem6G %>"   alt="<%= sNomeProduto %>" title="<%= sNomeProduto %>"></div><% end if %>
                                                <% if(sImagem7G <> "") then %><div class="img-item"><img  src="<%= sImagem7G %>"   alt="<%= sNomeProduto %>" title="<%= sNomeProduto %>"></div><% end if %>
									        </div>		

									        <p class="text-center msgZoom" style="display: none;"><small>Passe o mouse na foto para ver o zoom</small></p>		

									        <div class="slider-fotos-produtos-lista">
										        <% if(sImagemG <> "")  then %> <div class="img-pequena"><img src="<%= sImagemG  %>"   alt="<%= sNomeProduto %>" title="<%= sNomeProduto %>"></div><% end if %>
										        <% if(sImagem2G <> "") then %><div class="img-pequena"><img  src="<%= sImagem2G %>"   alt="<%= sNomeProduto %>" title="<%= sNomeProduto %>"></div><% end if %>
										        <% if(sImagem3G <> "") then %><div class="img-pequena"><img  src="<%= sImagem3G %>"   alt="<%= sNomeProduto %>" title="<%= sNomeProduto %>"></div><% end if %>
										        <% if(sImagem4G <> "") then %><div class="img-pequena"><img  src="<%= sImagem4G %>"   alt="<%= sNomeProduto %>" title="<%= sNomeProduto %>"></div><% end if %>
                                                <% if(sImagem5G <> "") then %><div class="img-pequena"><img  src="<%= sImagem5G %>"   alt="<%= sNomeProduto %>" title="<%= sNomeProduto %>"></div><% end if %>
                                                <% if(sImagem6G <> "") then %><div class="img-pequena"><img  src="<%= sImagem6G %>"   alt="<%= sNomeProduto %>" title="<%= sNomeProduto %>"></div><% end if %>
                                                <% if(sImagem7G <> "") then %><div class="img-pequena"><img  src="<%= sImagem7G %>"   alt="<%= sNomeProduto %>" title="<%= sNomeProduto %>"></div><% end if %>
									        </div>
								        </div>
							        </div>

							        <div class="col-sm-6" style="line-height: 2.2;">

									    <h2 class="semi-bold shape7"><%= ucase(sNomeProduto) %></h2>
                                        <br />
                                        <h4 class="semi-bold "><%= sDestaque %></h4>

                                        <% if(Session("dc" & nProduto) > 0) then %>

                                            <p class="special-price"> <span class="price-label"></span> <span class="price"> R$ <%= formatNumber((cdbl(sPrecoQuick) - cdbl(Session("dc" & nProduto))),2) %> </span> </p>
                                            <p class="old-price"> <span class="price-label"></span> <span class="price">  R$ <%= sPrecoQuick %> </span> </p>

                                            <% if(sPrecoPrazoQuick > 0 and sParcelamento > 1) then %>
                                                <span class="preco_parcelas">em <%= sParcelamento %>x R$ <%= ( formatNumber(cdbl(formatNumber((cdbl(sPrecoPrazoQuick)),2)) / cdbl(sParcelamento),2) ) %> sem juros cartão de crédito</span>
                                            <%end if%>
						    
                                        <% else %>

                                            <% if(Session("d" & nProduto) > 0) then                                
                                                sPrecoPrazoQuick = (cdbl(sPrecoPrazoQuick) - cdbl(Session("d" & nProduto)))
                                                %>

                                                <%if(nDescontoBoleto <> "") then 
                                                    if(nDescontoBoleto > 0) then 
                                                        sPrecoQuick = (cdbl(sPrecoQuick) - cdbl(Session("d" & nProduto)))
                                                        'sPrecoQuick = FormatNumber((cdbl(sPrecoQuick) - (cdbl(sPrecoQuick) * cdbl(nDescontoBoleto)) ), 2)
                                                %>
                                                        <p class="special-price"> <span class="price-label"></span> <span class="price"> R$ <%= formatNumber((cdbl(sPrecoQuick)),2) %> </span> 
                                                        <p class="old-price"> <span class="price-label"></span> <span class="price"> R$ <%= Session(nProduto) %> </span> </p>

                                                        <% if(sPrecoPrazoQuick > 0 and sParcelamento > 1) then %>
                                                            <br /><span class="preco_parcelas">em <%= sParcelamento %>x R$ <%= ( formatNumber(cdbl(formatNumber((cdbl(sPrecoPrazoQuick) ),2)) / cdbl(sParcelamento),2) ) %> sem juros cartão de crédito</span>
                                                        <%end if%>
                                                        <!--<br /><span class="preco_destaque">ou à vista no boleto com <%= nDescontoBoletoLeng %>% de desconto</span>-->

                                                    <% else %>
						                                <p class="special-price"> <span class="price-label"></span> <span class="price"> R$ <%= formatNumber((cdbl(sPrecoQuick) - cdbl(Session("d" & nProduto))),2) %> </span> </p>
                  
                                                    <% end if %>

                                                <% else %>
                                                    <p class="special-price"> <span class="price-label"></span> <span class="price"> R$ <%= formatNumber((cdbl(sPrecoQuick) - cdbl(Session("d" & nProduto))),2) %> </span> </p>
                                                <% end if %>
                          
                                            <% else %>

									            <h2 class="font-3 cor-1">R$ <%= formatNumber(cdbl(sPrecoQuick), 2) %></h2>

                                                <% if(sPrecoPrazoQuick > 0 and sParcelamento > 1) then %>
                                                    <span class="preco_parcelas"> em <%= sParcelamento %>x de R$ <%= ( formatNumber(cdbl(sPrecoPrazoQuick) / cdbl(sParcelamento),2) ) %> cartão de crédito</span>
                                                <%end if%>

                                                <%if(nDescontoBoleto <> "") then 
                                                    if(nDescontoBoleto > 0) then 
                                                        sPrecoQuick = (cdbl(sPrecoQuick) - cdbl(Session("d" & nProduto)))
                                                %>
                                                        <!--<br /><span class="preco_parcelas">à vista no boleto com <%= nDescontoBoletoLeng %>% desconto</span>-->
                                    
                                                    <% else %>
                                                        <br /><span class="preco_parcelas"> ou R$ <%= formatNumber(cdbl(sPrecoQuick), 2) %> à vista</span>
                                                    <% end if %>
                                                <% else %>
                                                        <br /><span class="preco_parcelas"> ou R$ <%= formatNumber(cdbl(sPrecoQuick), 2) %> à vista</span>
                                                <% end if %>

                                            <%end if%>

                                        <%end if%>

                                        
                                        <% if(boolComprar) then  %>
                                            <p>&nbsp;</p>
                                            <button class="botao-3 btn semi-bold" onclick="javascript: LinkCesta();" type="button" title="Add to Cart"> <img src="/img/icons/icon-carrinho.png" > adicionar ao carrinho  </button>
                                        <% else %>
                                            <p>&nbsp;</p>
                                            <button class="botao-3 btn semi-bold" style="background-color: #e1653d;"> <img src="/img/icons/icon-carrinho.png"> Esgotado  </button>
                                        <% end if %>
                                        
                                        <p class="mobile">&nbsp;</p>
								    
							        </div>

							        <div class="col-sm-12 desktop-margin-top-90">
								        <h2 class="cor-4 semi-bold "><span class="shape5">DESCRIÇÃO</span></h2>
								        <p style="line-height: 2.5;"><%= sDescricaoCompleta %></p>


                                        <p>&nbsp;</p>
                                        <div class="share-box">
                                            <div class="socials-box"> 
                                                <iframe src="https://www.facebook.com/plugins/share_button.php?href=<%= sPathHttps %>detalhe-produto/<%= sNomeProdutoSeo %>/<%= nProduto %>/&layout=button&size=small&mobile_iframe=true&appId=1758216144235526&width=97&height=20" width="97" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                                            </div>
                                        </div>
							        </div>

						        </div>	
					        </form>
    				    </div>
			        </div>
		        </div>
	        </div>

        </div>

        <!--#include virtual= "footer.asp"-->

        <script type="text/javascript">

            function MudaURLRadio(prTexto) {
                location.href = "/detalhe-produto/<%= sNomeProdutoSeo %>/" + prTexto + "/<%= nProduto %>";
            }

            function isObject(obj) {
                if (obj == null) { return false }
                if (typeof (obj) == 'object') { return true; } else { return false; }
            }

            function mostra_Imagem(sProduto, sTipo) {
                $(document).ready(function () {
                    $("#trocaimg").load('/busca_foto_ajax.asp', { produto: sProduto, tipo: sTipo });
                });
            }

            function LinkCesta() {
                var objForm = document.forma;
                var slink = '';
                var nVariacoes = 0;

                var prTipo = '';
                // var prTamanho = $('#select').find('option').filter(':selected').val();
                // var prCor 	=  $('#selectcor_2').find('option').filter(':selected').val();

                $('input:radio[name=id_tipo]').each(function () {
                    if ($(this).is(':checked'))
                        prTipo = parseInt($(this).val());
                })

                $('input:radio[name=id_sabor]').each(function () {
                    if ($(this).is(':checked'))
                        prSabor = parseInt($(this).val());
                })


                if (isObject(objForm.cores)) {
                    slink = slink + '&cores=' + prCor
                    if (objForm.cores.value != '') nVariacoes = (nVariacoes + 1)
                } else {
                    slink = slink + '&cores='
                }

                if (isObject(objForm.id_tipo)) {

                    if (prTipo == '') {
                        alert('Por favor, selecione uma Voltagem.')
                        return false;
                    }

                    slink = slink + '&tipos=' + prTipo;
                    if (prTipo != '') nVariacoes = (nVariacoes + 1)
                } else {
                    slink = slink + '&tipos=';
                }

                if (isObject(objForm.id_sabor)) {
                    slink = slink + '&sabores=' + prSabor;
                    if (prSabor != '') nVariacoes = (nVariacoes + 1)
                } else {
                    slink = slink + '&sabores=';
                }

                if (nVariacoes > 0) {
                    location.href = '/carrinho.asp?quantidade='+ $("#qty").val() +'&id=<%= nProduto %>' + slink;
                } else {
                    if ((!isObject(objForm.tamanhos)) &&
                        (!isObject(objForm.cores)) &&
                        (!isObject(objForm.sabores)) &&
                         (!isObject(objForm.fragrancias)) &&
                         (!isObject(objForm.tipos)) &&
                         (!isObject(objForm.pacotes)) &&
                         (!isObject(objForm.ge)) &&
                         (!isObject(objForm.quantidade))) {
                        location.href = '/carrinho.asp?quantidade='+ $("#qty").val() +'&id=<%= nProduto %>' + slink;
                    }//else {

                    //alert('Por favor, selecione cor e tamanho!');
                    // location.href = 'cesta.asp?id=<%= nProduto %>'
                    //}
                }
            }
        </script>

    </body>

</html>
