<!--#include virtual= "/hidden/funcoes.asp"-->

<%
if(Session("Id") = "") then response.redirect("/login.asp?ReturnUrl=arearestrita")

Dim iPageCount, iRecordCount, iQualPagina, iExibe, sOpcao_Ordenar
dim sFlagOpc, sBusca, sCombo, boolEnviado
Dim sAcao, sId, boolGravado

dim sClassHeader: sClassHeader = "bg-1"

sOpcao_Ordenar = Replace_Caracteres_Especiais(request("ordenar"))

If (Replace_Caracteres_Especiais(Request("Pagina")) = "") Then
	iQualPagina = 1
Else
	iQualPagina = CInt(Replace_Caracteres_Especiais(Request("Pagina")))
End If


' ===================================================================================
function Monta_Linhas_Historico()
	dim sId, sCampo1, sCampo2, sCampo3, sCampo4, sCampo5, sCampo6, sTotalPedido, sIdFormaPagamento
	dim sConteudo, Rs, sSQl, sMontagem

	call Abre_Conexao()
	
	sSQl = "Select " & _
				"TB_PEDIDOS.*, " & _
				"TB_CLIENTES.Nome, " & _
				"TB_CLIENTES.SobreNome, " & _
				"TB_FORMAS_PAGAMENTO.Descricao as Pagamento, " & _
				"TB_FORMAS_PAGAMENTO.id as idPagamento, " & _
				"TB_STATUS.Descricao as sTatus, " & _
                "isnull(TB_STATUS.DescricaoCliente,'') as sTatusCliente, " & _
				"TB_STATUS.id as sIdTatus" & _
			" from  " & _
				"TB_PEDIDOS                     (NOLOCK)    " & _
				"INNER JOIN TB_CLIENTES         (NOLOCK) ON TB_PEDIDOS.Id_Cliente   = TB_CLIENTES.id   " & _
				"INNER JOIN TB_FORMAS_PAGAMENTO (NOLOCK) ON TB_FORMAS_PAGAMENTO.ID  = TB_PEDIDOS.Id_FrmPagamento    " & _
				"INNER JOIN TB_STATUS           (NOLOCK) ON TB_PEDIDOS.Id_Status    = TB_STATUS.id    " & _
			" where  " & _
				"TB_PEDIDOS.Id_Cliente = "& Session("Id")

	if(sOpcao_Ordenar <> "") then 
		sSQL = sSQL & " ORDER BY " & sOpcao_Ordenar
	else
		sSQL = sSQL & " order by TB_PEDIDOS.datacad desc"
	end if

	set Rs = Server.CreateObject("ADODB.RecordSet")
	Rs.CursorLocation = 3
	Rs.CursorType = 3
	Rs.Open sSQL, oConn, 0
	
	if(Not Rs.Eof) then
		Rs.PageSize     = 25
		Rs.CacheSize    = Rs.PageSize
		iPageCount      = Rs.PageCount
		iRecordCount    = Rs.RecordCount
		Rs.AbsolutePage = iQualPagina
		iExibe          = 1
		
		Do While (Not Rs.Eof) and (iExibe <= 25)
			iExibe              = iExibe + 1

			sId                 = rs("id")
			sCampo1             = trim(rs("Nome")) & "&nbsp;" & trim(rs("SobreNome"))
			sCampo2             = trim(rs("datacad"))
			sCampo3             = trim(rs("pagamento"))
			sCampo4             = trim(rs("sTatusCliente"))
    		sCampo5             = RetornaCasasDecimais(cdbl(trim(rs("total"))),2)
			sCampo6             = trim(rs("sIdTatus"))
			sIdFormaPagamento   = trim(rs("idPagamento"))
            sEntrega            = ""
	
            if(sCampo4 = "") then sCampo4 = trim(rs("status"))
    		
			if((sCampo6 = 1 or sCampo6 = 108 or sCampo6 = 34 or sCampo6 = 98) and (sIdFormaPagamento = 22 or sIdFormaPagamento = 23)) then 
                sCampo3 = "<a href=""https://adminv1.ominic.com.br/EcommerceNew/faturar_pedidos_boleto.asp?id="& sId &""" target=""_blank"" class=""a11preto"">"& sCampo3 &"</a>"
			end if
			
            if(sCampo6 = "4" or sCampo6 = "6" ) then 
                sEntrega = "<a href=""http://www.ontimelog.com.br/ferramentas/rastreamento/ontimeRastreamento.asp?cod=95&pedido="& sId &""" target=""_blank""><img src=""https://adminv1.ominic.com.br/images/ontimelog.jpg"" border=""0"" width=""80"" height=""25""></a>"
            end if
            
            if((sCampo6 = 1 ) and (sIdFormaPagamento <> 22) ) then 
                sCampo3 = sCampo3 & " - <a href=""/faturar_pedidos_moip.asp?id="& sId &""" target=""_blank"" class=""a11preto""><b>Efetuar pagamento</b></a>"
            end if

            sFonte = ""
            if(InStr(lcase(sCampo4), "erro") > 0 ) then sFonte = "<font color=red>"

            sConteudo = sConteudo & " <tr> " & vbcrlf
                sConteudo = sConteudo & " <td data-title=""Order Number""><a href=""/order_details.asp?id="& sId &""" class=""order-number"">#"& sId &"</a></td>" & vbcrlf
                sConteudo = sConteudo & " <td data-title=""Order Date"">"& sCampo2 &"</td>" & vbcrlf
                sConteudo = sConteudo & " <td data-title=""Order Status"">"& sFonte &""& sCampo4 &" "& sEntrega &"</td>" & vbcrlf
                sConteudo = sConteudo & " <td data-title=""Total""><span class=""order-total"">R$ "& sCampo5 &"</span></td>" & vbcrlf
            sConteudo = sConteudo & " </tr>" & vbcrlf


			sTotalPedido	= Cdbl(sTotalPedido + sCampo5)
		Rs.movenext
		loop
		
		sConteudo = sConteudo & "<tr>" & vbcrlf
		    sConteudo = sConteudo & "<td  align=""left"" height=""40"">&nbsp;</td>" & vbcrlf
		    sConteudo = sConteudo & "<td  align=""left"" height=""40"">&nbsp;</td>" & vbcrlf
		    sConteudo = sConteudo & "<td  align=""left"" valign=""bottom"" ><span class=""a12pretobold"">Total de todos os Pedidos:</td>" & vbcrlf
		    sConteudo = sConteudo & "<td  align=""left"" valign=""bottom"" ><span class=""a12azulbold"">R$ "& FormatNumber(sTotalPedido,2) & "</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
	
	else
		sConteudo = sConteudo & "<tr align=""center"" bgcolor="""& Application("Text_site") &""">" & vbcrlf
		sConteudo = sConteudo & "	<td scope=""col"" align=""center"" colspan=""10""><span class=""descriprod"">Não há registros!</td>" & vbcrlf
		sConteudo = sConteudo & "</tr>" & vbcrlf
		sConteudo = sConteudo & "</table>"
	end if
	set Rs = nothing
	
	call Fecha_Conexao()
	Monta_Linhas_Historico = sConteudo
end function


%>

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <!--#include virtual= "head.asp"-->

    <body>
        <!--#include virtual= "header.asp"-->

        <div class=" desktop-margin-top-90" >
	    <div class="margin-60-0 ">
		    <div class="container">
			    <div class="row desktop-margin-90 ">
				    <div class="col-sm-12">
					    <div class="row">
						    <div class="col-sm-12"><h2 class="cor-4 semi-bold "><span class="shape7">HISTÓRICO DE PEDIDOS</span></h2></div>

						    <div class="col-sm-10 col-sm-push-1">
								
                                
                                <p>[<a href="/arearestrita">voltar Minha conta</a>]</p>

                                <p>&nbsp;</p>

							    <div class="table-responsive">

                                      <table class="table table-striped  table-estilo-1 text-center table-bordered">
                                        <thead>
                                          <tr> 
                                            <!--titles for td-->
                                            <th class="text-center"><b>Order Number</b></th>
                                            <th class="text-center">Order Date</th>
                                            <th class="text-center">Order Status</th>
                                            <th class="text-center">Total</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            <%= Monta_Linhas_Historico() %>
                                        </tbody>
                                      </table>

								    <!--<table class="table table-striped  table-estilo-1 text-center table-bordered">
									    <thead>
										    <tr>
											    <th class="text-center">Quantidade</th>
											    <th class="text-center">Produto</th>
											    <th class="text-center">Valor</th>
											    <th class="text-center">Status</th>
										    </tr>
									    </thead>
									    <tbody>
										    <tr>
											    <td>2</td>
											    <td>Nome do produto</td>
											    <td>R$XXX,XX</td>
											    <td>
												    <i class="fa fa-check"></i>
												    entregue
											    </td>
										    </tr>
										    <tr>
											    <td>2</td>
											    <td>Nome do produto</td>
											    <td>R$XXX,XX</td>
											    <td>
												    <i class="fa fa-close"></i>
												    erro
											    </td>
										    </tr>
										    <tr>
											    <td>2</td>
											    <td>Nome do produto</td>
											    <td>R$XXX,XX</td>
											    <td>
												    <i class="fa fa-send-o"></i>
												    enviado
											    </td>
										    </tr>
										    <tr>
											    <td>2</td>
											    <td>Nome do produto</td>
											    <td>R$XXX,XX</td>
											    <td>
												    <i class="fa fa-spinner"></i>
												    processando
											    </td>
										    </tr>

									    </tbody>
								    </table>-->

							    </div>	
						    </div>
					    </div>	
				    </div>
			    </div>
		    </div>
	    </div>
    </div>

        <!--#include virtual= "footer.asp"-->
    </body>

</html>
