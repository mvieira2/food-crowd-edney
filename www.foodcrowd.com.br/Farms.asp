<!--#include virtual= "/hidden/funcoes.asp"-->


<!DOCTYPE html>
<html lang="en" dir="ltr">
    <!--#include virtual= "head.asp"-->

    <body>
        <!--#include virtual= "header.asp"-->

        <div class="banners">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="/img/banner-topo.png" alt="Los Angeles">
                        <div class="carousel-caption">
                            <div class="banner-titles">
                                <h2 class="extra-bold" >Nossos Produtores</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container">
        <div class="row margin-60-0 ">
            <div class="col-sm-4 ">
                <div class="scroll-left">
                    <div class="row box-ofertas box-ofertas-hover-2">
                        <div class="col-sm-12 col-xs-6">
                            <a href="#menu1" data-toggle="tab" class="active">
                                <div class="sombra"></div>
                                <img src="/img/banner11.jpg" alt="FAZENDA CASTANHEIRA">
                                <strong class="nome-da-oferta-3 text-center"></strong>
                            </a>
                        </div>

                        <!--<div class="col-sm-12 col-xs-6">
                            <a href="#menu2" data-toggle="tab" class="">
                                <div class="sombra"></div>
                                <img src="/img/banner11.png" alt="">
                                <strong class="nome-da-oferta-3 text-center">
                                    Nome da Fazenda 2
                                </strong>
                            </a>
                        </div>
                        <div class="col-sm-12 col-xs-6">
                            <a href="#menu3" data-toggle="tab">
                                <div class="sombra"></div>
                                <img src="/img/banner11.png" alt="">
                                <strong class="nome-da-oferta-3 text-center">
                                    Nome da Fazenda 3
                                </strong>
                            </a>
                        </div>
                        <div class="col-sm-12 col-xs-6">
                            <a href="#menu4" data-toggle="tab">
                                <div class="sombra"></div>
                                <img src="/img/banner11.png" alt="">
                                <strong class="nome-da-oferta-3 text-center">
                                    Nome da Fazenda 4
                                </strong>
                            </a>
                        </div>
                        <div class="col-sm-12 col-xs-6">
                            <a href="#menu5" data-toggle="tab">
                                <div class="sombra"></div>
                                <img src="/img/banner11.png" alt="">
                                <strong class="nome-da-oferta-3 text-center">
                                    Nome da Fazenda 5
                                </strong>
                            </a>
                        </div>
                        <div class="col-sm-12 col-xs-6">
                            <a href="#menu6" data-toggle="tab">
                                <div class="sombra"></div>
                                <img src="/img/banner11.png" alt="">
                                <strong class="nome-da-oferta-3 text-center">
                                    Nome da Fazenda 6
                                </strong>
                            </a>
                        </div>-->

                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="tab-content">

                    <div id="menu1" class="tab-pane fade active in">
                        <h2 class="shape5 semi-bold ">  FAZENDA CASTANHEIRA<!--<br />PRODUTOR NINO MARÇANO--></h2>

                        <br />

                        <p class="descrition">
A Fazenda Castanheira já está em sua terceira geração. O Nino nos conta que tudo começou com seu avô, e a paixão pela pecuária foi se transmitindo pelas gerações. <br /><br />

O Nino hoje é quem cuida da fazenda com muita paixão e dedicação. Há cerca de 20 anos, quando ele assumiu a pecuária da família, seu foco foi produzir carnes especiais a partir da seleção de um gado de alta qualidade, trabalhando com raças britânicas. <br /><br />

A qualidade da carne na Fazenda Castanheira começa a ser definida já na seleção da raça e dos touros e vacas e a dieta de terminação é formulada para que não haja interferência no sabor original, mas sim proporcionando uma textura incrível com um alto índice de marmoreio e cobertura. <br /><br />

Um grande diferencial da Fazenda Castanheira é o uso de ultrassom para medir o índice de marmoreio antes do abate do gado - uma técnica desenvolvida nos Estados Unidos, garantindo qualidade do produto de maneira consistente e controlada.<br /><br />

O Nino nos explica o resultado do ultrassom: “Quando você avalia o animal antes de ir para o frigorífico, você consegue chegar com este animal no ápice de qualidade dele.” <br /><br />

<iframe width="700" height="315" src="https://www.youtube.com/embed/Y04UsKaHBAU?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<!--

                            A Fazenda Castanheira já está em sua terceira geração. O Nino nos conta que tudo começou com seu avô, e a paixão pela pecuária foi se transmitindo pelas gerações. <br /><br />
                            
                            O Nino hoje é quem cuida com muita paixão e dedicação da fazenda. Há mais de 20 anos, quando ele assumiu a pecuária da família, seu foco foi produzir um gado de alta qualidade, trabalhando com raças britânicas produzindo carnes especiais. <br /><br />
                            
                            A pecuária na Fazenda Castanheira começa a ser definida já na seleção da raça e dos touros que vão inseminar as vacas que produzirão os seus bezerros. <br /><br />
                            
                            <iframe width="700" height="315" src="https://www.youtube.com/embed/Y04UsKaHBAU?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <Br /><Br />

                            A dieta é formulada para que o sabor da carne não tenha interferência, proporcionando um sabor incrível com um alto índice de marmoreio e cobertura. <br /><br />
                            
                            Um grande diferencial da Fazenda Castanheira é o ultrassom do gado, uma técnica desenvolvida nos Estados Unidos que permite que antes que a novilha vá para o abate, se possa ter certeza da qualidade da carne, pois nesse ultrassom é possível medir o índice de marmoreio. <br /><br />
                            
                            O Nino nos explica o resultado do ultrassom: “Quando você avalia o animal antes de ir para o frigorífico, você consegue chegar com este animal no ápice de qualidade dele.”  <br /><br />-->
                        </p>                  

                    </div>

                    <!--<div id="menu2" class="tab-pane fade">
                        <h2 class="shape5 semi-bold ">  NOME FAZENDA 2</h2>
                        <p class="descrition">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus sodales nibh, non malesuada lacus pulvinar at. Donec arcu ligula, vestibulum non nisi vel, bibendum gravida nunc. Integer quam leo, imperdiet eget mollis ac, eleifend vel neque. Nunc hendrerit nisl tellus, non hendrerit purus ornare at. Aliquam imperdiet lacinia dui eu gravida. Etiam ut bibendum lectus. Mauris vel rhoncus elit. Vestibulum sem lorem, malesuada sed mi sed, sodales porta justo. Donec ut lectus vestibulum, viverra quam vitae, mollis dui. Quisque suscipit lectus vitae mi semper consequat. In fringilla odio ut risus facilisis posuere. Proin arcu nunc, consequat non tincidunt nec, venenatis at enim. Vivamus semper diam ut tincidunt ornare. Sed sollicitudin aliquam auctor. Phasellus rhoncus efficitur diam, id pretium leo vulputate et.
                            <br><br>
                            Praesent et turpis tempor magna aliquet commodo eget nec erat. Duis vel tempor enim. Ut elit purus, fermentum aliquet lacus eget, ullamcorper vestibulum diam. Quisque est nibh, tristique a massa ut, eleifend pretium ligula. Curabitur eget pretium ante. Sed id quam risus. Duis pellentesque orci quis ipsum consectetur malesuada. Vivamus malesuada, lorem sagittis vehicula iaculis, tortor urna luctus orci, in sollicitudin nisi leo in purus. Mauris eu diam nisi. Phasellus imperdiet sodales metus, in vehicula tellus venenatis eget. Duis viverra, magna nec aliquet consequat, lectus diam ultricies elit, non ornare urna libero ut odio. Donec aliquet arcu fringilla tellus bibendum efficitur. Morbi efficitur viverra feugiat. Quisque at nunc hendrerit, pharetra tortor vel, laoreet ante.
                            <br><br>
                            Nulla rhoncus quam quis enim euismod, sed dictum leo luctus. Phasellus justo ante, finibus lobortis odio nec, eleifend egestas nisl. Sed laoreet ipsum metus, nec bibendum orci aliquet non. Cras sodales non arcu ut semper. Donec ut nibh a odio varius laoreet. Vivamus felis urna, faucibus nec tortor vitae, venenatis ullamcorper metus. Proin faucibus quam non dignissim tempor.
                            <br><br>
                            Proin ac arcu sed lacus porttitor congue nec ac nisi. Nulla mattis tincidunt mattis. Donec sed efficitur ipsum. Fusce finibus dapibus nulla ac bibendum. Integer at elit arcu. Proin at mauris porta dui pulvinar lacinia sit amet a tellus. Sed quis augue nec ligula interdum lobortis sed non ante. Donec vitae nulla sed dolor congue tempus. In pulvinar, ante interdum tincidunt maximus, justo dolor rutrum ante, ut porta dui nisl id est. Praesent pellentesque auctor nisi, at facilisis urna ultrices sed. Sed eleifend nulla vitae odio viverra facilisis. Praesent aliquet odio id leo aliquet auctor.

                        </p>
                    </div>
                    <div id="menu3" class="tab-pane fade">
                        <h2 class="shape5 semi-bold ">  NOME FAZENDA 3</h2>
                        <p class="descrition">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus sodales nibh, non malesuada lacus pulvinar at. Donec arcu ligula, vestibulum non nisi vel, bibendum gravida nunc. Integer quam leo, imperdiet eget mollis ac, eleifend vel neque. Nunc hendrerit nisl tellus, non hendrerit purus ornare at. Aliquam imperdiet lacinia dui eu gravida. Etiam ut bibendum lectus. Mauris vel rhoncus elit. Vestibulum sem lorem, malesuada sed mi sed, sodales porta justo. Donec ut lectus vestibulum, viverra quam vitae, mollis dui. Quisque suscipit lectus vitae mi semper consequat. In fringilla odio ut risus facilisis posuere. Proin arcu nunc, consequat non tincidunt nec, venenatis at enim. Vivamus semper diam ut tincidunt ornare. Sed sollicitudin aliquam auctor. Phasellus rhoncus efficitur diam, id pretium leo vulputate et.
                            <br><br>
                            Praesent et turpis tempor magna aliquet commodo eget nec erat. Duis vel tempor enim. Ut elit purus, fermentum aliquet lacus eget, ullamcorper vestibulum diam. Quisque est nibh, tristique a massa ut, eleifend pretium ligula. Curabitur eget pretium ante. Sed id quam risus. Duis pellentesque orci quis ipsum consectetur malesuada. Vivamus malesuada, lorem sagittis vehicula iaculis, tortor urna luctus orci, in sollicitudin nisi leo in purus. Mauris eu diam nisi. Phasellus imperdiet sodales metus, in vehicula tellus venenatis eget. Duis viverra, magna nec aliquet consequat, lectus diam ultricies elit, non ornare urna libero ut odio. Donec aliquet arcu fringilla tellus bibendum efficitur. Morbi efficitur viverra feugiat. Quisque at nunc hendrerit, pharetra tortor vel, laoreet ante.
                            <br><br>
                            Nulla rhoncus quam quis enim euismod, sed dictum leo luctus. Phasellus justo ante, finibus lobortis odio nec, eleifend egestas nisl. Sed laoreet ipsum metus, nec bibendum orci aliquet non. Cras sodales non arcu ut semper. Donec ut nibh a odio varius laoreet. Vivamus felis urna, faucibus nec tortor vitae, venenatis ullamcorper metus. Proin faucibus quam non dignissim tempor.
                            <br><br>
                            Proin ac arcu sed lacus porttitor congue nec ac nisi. Nulla mattis tincidunt mattis. Donec sed efficitur ipsum. Fusce finibus dapibus nulla ac bibendum. Integer at elit arcu. Proin at mauris porta dui pulvinar lacinia sit amet a tellus. Sed quis augue nec ligula interdum lobortis sed non ante. Donec vitae nulla sed dolor congue tempus. In pulvinar, ante interdum tincidunt maximus, justo dolor rutrum ante, ut porta dui nisl id est. Praesent pellentesque auctor nisi, at facilisis urna ultrices sed. Sed eleifend nulla vitae odio viverra facilisis. Praesent aliquet odio id leo aliquet auctor.

                        </p>
                    </div>
                    <div id="menu4" class="tab-pane fade">
                        <h2 class="shape5 semi-bold ">  NOME FAZENDA 4</h2>
                        <p class="descrition">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus sodales nibh, non malesuada lacus pulvinar at. Donec arcu ligula, vestibulum non nisi vel, bibendum gravida nunc. Integer quam leo, imperdiet eget mollis ac, eleifend vel neque. Nunc hendrerit nisl tellus, non hendrerit purus ornare at. Aliquam imperdiet lacinia dui eu gravida. Etiam ut bibendum lectus. Mauris vel rhoncus elit. Vestibulum sem lorem, malesuada sed mi sed, sodales porta justo. Donec ut lectus vestibulum, viverra quam vitae, mollis dui. Quisque suscipit lectus vitae mi semper consequat. In fringilla odio ut risus facilisis posuere. Proin arcu nunc, consequat non tincidunt nec, venenatis at enim. Vivamus semper diam ut tincidunt ornare. Sed sollicitudin aliquam auctor. Phasellus rhoncus efficitur diam, id pretium leo vulputate et.
                            <br><br>
                            Praesent et turpis tempor magna aliquet commodo eget nec erat. Duis vel tempor enim. Ut elit purus, fermentum aliquet lacus eget, ullamcorper vestibulum diam. Quisque est nibh, tristique a massa ut, eleifend pretium ligula. Curabitur eget pretium ante. Sed id quam risus. Duis pellentesque orci quis ipsum consectetur malesuada. Vivamus malesuada, lorem sagittis vehicula iaculis, tortor urna luctus orci, in sollicitudin nisi leo in purus. Mauris eu diam nisi. Phasellus imperdiet sodales metus, in vehicula tellus venenatis eget. Duis viverra, magna nec aliquet consequat, lectus diam ultricies elit, non ornare urna libero ut odio. Donec aliquet arcu fringilla tellus bibendum efficitur. Morbi efficitur viverra feugiat. Quisque at nunc hendrerit, pharetra tortor vel, laoreet ante.
                            <br><br>
                            Nulla rhoncus quam quis enim euismod, sed dictum leo luctus. Phasellus justo ante, finibus lobortis odio nec, eleifend egestas nisl. Sed laoreet ipsum metus, nec bibendum orci aliquet non. Cras sodales non arcu ut semper. Donec ut nibh a odio varius laoreet. Vivamus felis urna, faucibus nec tortor vitae, venenatis ullamcorper metus. Proin faucibus quam non dignissim tempor.
                            <br><br>
                            Proin ac arcu sed lacus porttitor congue nec ac nisi. Nulla mattis tincidunt mattis. Donec sed efficitur ipsum. Fusce finibus dapibus nulla ac bibendum. Integer at elit arcu. Proin at mauris porta dui pulvinar lacinia sit amet a tellus. Sed quis augue nec ligula interdum lobortis sed non ante. Donec vitae nulla sed dolor congue tempus. In pulvinar, ante interdum tincidunt maximus, justo dolor rutrum ante, ut porta dui nisl id est. Praesent pellentesque auctor nisi, at facilisis urna ultrices sed. Sed eleifend nulla vitae odio viverra facilisis. Praesent aliquet odio id leo aliquet auctor.

                        </p>
                    </div>
                    <div id="menu5" class="tab-pane fade">
                        <h2 class="shape5 semi-bold ">  NOME FAZENDA 5</h2>
                        <p class="descrition">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus sodales nibh, non malesuada lacus pulvinar at. Donec arcu ligula, vestibulum non nisi vel, bibendum gravida nunc. Integer quam leo, imperdiet eget mollis ac, eleifend vel neque. Nunc hendrerit nisl tellus, non hendrerit purus ornare at. Aliquam imperdiet lacinia dui eu gravida. Etiam ut bibendum lectus. Mauris vel rhoncus elit. Vestibulum sem lorem, malesuada sed mi sed, sodales porta justo. Donec ut lectus vestibulum, viverra quam vitae, mollis dui. Quisque suscipit lectus vitae mi semper consequat. In fringilla odio ut risus facilisis posuere. Proin arcu nunc, consequat non tincidunt nec, venenatis at enim. Vivamus semper diam ut tincidunt ornare. Sed sollicitudin aliquam auctor. Phasellus rhoncus efficitur diam, id pretium leo vulputate et.
                            <br><br>
                            Praesent et turpis tempor magna aliquet commodo eget nec erat. Duis vel tempor enim. Ut elit purus, fermentum aliquet lacus eget, ullamcorper vestibulum diam. Quisque est nibh, tristique a massa ut, eleifend pretium ligula. Curabitur eget pretium ante. Sed id quam risus. Duis pellentesque orci quis ipsum consectetur malesuada. Vivamus malesuada, lorem sagittis vehicula iaculis, tortor urna luctus orci, in sollicitudin nisi leo in purus. Mauris eu diam nisi. Phasellus imperdiet sodales metus, in vehicula tellus venenatis eget. Duis viverra, magna nec aliquet consequat, lectus diam ultricies elit, non ornare urna libero ut odio. Donec aliquet arcu fringilla tellus bibendum efficitur. Morbi efficitur viverra feugiat. Quisque at nunc hendrerit, pharetra tortor vel, laoreet ante.
                            <br><br>
                            Nulla rhoncus quam quis enim euismod, sed dictum leo luctus. Phasellus justo ante, finibus lobortis odio nec, eleifend egestas nisl. Sed laoreet ipsum metus, nec bibendum orci aliquet non. Cras sodales non arcu ut semper. Donec ut nibh a odio varius laoreet. Vivamus felis urna, faucibus nec tortor vitae, venenatis ullamcorper metus. Proin faucibus quam non dignissim tempor.
                            <br><br>
                            Proin ac arcu sed lacus porttitor congue nec ac nisi. Nulla mattis tincidunt mattis. Donec sed efficitur ipsum. Fusce finibus dapibus nulla ac bibendum. Integer at elit arcu. Proin at mauris porta dui pulvinar lacinia sit amet a tellus. Sed quis augue nec ligula interdum lobortis sed non ante. Donec vitae nulla sed dolor congue tempus. In pulvinar, ante interdum tincidunt maximus, justo dolor rutrum ante, ut porta dui nisl id est. Praesent pellentesque auctor nisi, at facilisis urna ultrices sed. Sed eleifend nulla vitae odio viverra facilisis. Praesent aliquet odio id leo aliquet auctor.

                        </p>
                    </div>
                    <div id="menu6" class="tab-pane fade">
                        <h2 class="shape5 semi-bold ">  NOME FAZENDA 6</h2>
                        <p class="descrition">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus sodales nibh, non malesuada lacus pulvinar at. Donec arcu ligula, vestibulum non nisi vel, bibendum gravida nunc. Integer quam leo, imperdiet eget mollis ac, eleifend vel neque. Nunc hendrerit nisl tellus, non hendrerit purus ornare at. Aliquam imperdiet lacinia dui eu gravida. Etiam ut bibendum lectus. Mauris vel rhoncus elit. Vestibulum sem lorem, malesuada sed mi sed, sodales porta justo. Donec ut lectus vestibulum, viverra quam vitae, mollis dui. Quisque suscipit lectus vitae mi semper consequat. In fringilla odio ut risus facilisis posuere. Proin arcu nunc, consequat non tincidunt nec, venenatis at enim. Vivamus semper diam ut tincidunt ornare. Sed sollicitudin aliquam auctor. Phasellus rhoncus efficitur diam, id pretium leo vulputate et.
                            <br><br>
                            Praesent et turpis tempor magna aliquet commodo eget nec erat. Duis vel tempor enim. Ut elit purus, fermentum aliquet lacus eget, ullamcorper vestibulum diam. Quisque est nibh, tristique a massa ut, eleifend pretium ligula. Curabitur eget pretium ante. Sed id quam risus. Duis pellentesque orci quis ipsum consectetur malesuada. Vivamus malesuada, lorem sagittis vehicula iaculis, tortor urna luctus orci, in sollicitudin nisi leo in purus. Mauris eu diam nisi. Phasellus imperdiet sodales metus, in vehicula tellus venenatis eget. Duis viverra, magna nec aliquet consequat, lectus diam ultricies elit, non ornare urna libero ut odio. Donec aliquet arcu fringilla tellus bibendum efficitur. Morbi efficitur viverra feugiat. Quisque at nunc hendrerit, pharetra tortor vel, laoreet ante.
                            <br><br>
                            Nulla rhoncus quam quis enim euismod, sed dictum leo luctus. Phasellus justo ante, finibus lobortis odio nec, eleifend egestas nisl. Sed laoreet ipsum metus, nec bibendum orci aliquet non. Cras sodales non arcu ut semper. Donec ut nibh a odio varius laoreet. Vivamus felis urna, faucibus nec tortor vitae, venenatis ullamcorper metus. Proin faucibus quam non dignissim tempor.
                            <br><br>
                            Proin ac arcu sed lacus porttitor congue nec ac nisi. Nulla mattis tincidunt mattis. Donec sed efficitur ipsum. Fusce finibus dapibus nulla ac bibendum. Integer at elit arcu. Proin at mauris porta dui pulvinar lacinia sit amet a tellus. Sed quis augue nec ligula interdum lobortis sed non ante. Donec vitae nulla sed dolor congue tempus. In pulvinar, ante interdum tincidunt maximus, justo dolor rutrum ante, ut porta dui nisl id est. Praesent pellentesque auctor nisi, at facilisis urna ultrices sed. Sed eleifend nulla vitae odio viverra facilisis. Praesent aliquet odio id leo aliquet auctor.

                        </p>
                    </div>-->

                </div>
            </div>
        </div>
    </div>

        <!--<div class="container"><hr></div>-->

        <!--<div class="container ">
        <div class="row margin-60-0 ">
            <div class="col-xs-10"><h2 class="semi-bold shape3"> FOTOS  </h2></div>
            <div class="col-xs-2 text-right">
                <div class="setas slider-1-setas noselect">
                    <i class="material-icons seta-esquerda">&#xe5c4;</i>
                    <i class="material-icons seta-direita">&#xe5c8;</i>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
                <div class="slider-1  box-ofertas row">
                    <div class="col-sm-4">
                        <a class="zoom" rel="zoom" href="/img/banner10.png" title="Descrição">
                            <div class="sombra"></div>
                            <img src="/img/banner10.png" alt="">
                        </a>
                        <a class="zoom" rel="zoom"  href="/img/banner10.png">
                            <h4 class="text-center cor-2">
                                <br>
                                Descrição
                            </h4>
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <a class="zoom" rel="zoom" href="/img/banner10.png">
                            <div class="sombra"></div>
                            <img src="/img/banner10.png" alt="">
                        </a>
                        <a class="zoom" rel="zoom"  href="/img/banner10.png">
                            <h4 class="text-center cor-2">
                                <br>
                                Descrição
                            </h4>
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <a class="zoom" rel="zoom"  href="/img/banner10.png">
                            <div class="sombra"></div>
                            <img src="/img/banner10.png" alt="">
                        </a>
                        <a class="zoom" rel="zoom"  href="/img/banner10.png">
                            <h4 class="text-center cor-2">
                                <br>
                                Descrição
                            </h4>
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <a class="zoom" rel="zoom"  href="/img/banner10.png">
                            <div class="sombra"></div>
                            <img src="/img/banner10.png" alt="">
                        </a>

                        <a class="zoom" rel="zoom"  href="/img/banner10.png">
                            <h4 class="text-center cor-2">
                                <br>
                                Descrição
                            </h4>
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <a class="zoom" rel="zoom"  href="/img/banner10.png">
                            <div class="sombra"></div>
                            <img src="/img/banner10.png" alt="">
                        </a>

                        <a class="zoom" rel="zoom"  href="/img/banner10.png" title="Decricao">
                            <h4 class="text-center cor-2">
                                <br>
                                Descrição
                            </h4>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>-->

        <div class="banners" style="margin-bottom:60px;">
           <div id="myCarousel2" class="carousel slide" data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner">

                <div class="item active">
                    <img src="/img/banner3.png" alt="Los Angeles">
                    <div class="carousel-caption">
                        <div class="banner-titles">
                            <h2 class="semi-bold" style="margin-bottom:20px;" >Faça parte da Food Crowd</h2>
                        </div>
                        <p class="text-center">
                            <a href="/facaparte" class="btn botao-4">FAÇA PARTE</a>
                        </p>
                    </div>
                </div>
            </div>

        </div>
        </div>

        <!--#include virtual= "footer.asp"-->
    </body>
</html>
