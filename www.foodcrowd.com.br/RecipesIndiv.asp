<!--#include virtual= "/hidden/funcoes.asp"-->

<%
dim sClassHeader: sClassHeader = "bg-1"
dim sConteudoInst, sTitulo
dim sSecao,  sAtivo(10)

sSecao 	= Replace_Caracteres_Especiais(trim(request("ID")))
    
if (sSecao <> "") then

    call Abre_Conexao()

	sSql = "Select TOP 1 * from TB_PUBLICACOES (nolock) where ID = "& sSecao &" AND ID_TIPO = 2 AND ATIVO = 1 "
    'response.write sSql
	set Rs1 = oConn.execute(sSql)

	if(not Rs1.eof) then
	    sTitulo         = trim(Rs1("NOME"))
        sNomeProdutoSeo = TrataSEO(trim(Rs1("NOME")))
	    NOME_2          = trim(Rs1("NOME_2"))
        DESCRICAO       = trim(Rs1("DESCRICAO"))
        ativo           = trim(Rs1("ativo"))
        simagem1        = trim(Rs1("Imagem"))
        simagem2        = trim(Rs1("Imagem_2"))
        simagem3        = trim(Rs1("Imagem_3"))
    end if
	set Rs1 = nothing

    call Fecha_Conexao()

    if(ativo <> "1") then 
        response.Write "[matéria desativada]<br>"
        response.end
    end if


    if(simagem1 <> "" and not isnull(simagem1)) then simagem1 = "<img src="""& sLinkImagem &"/EcommerceNew/upload/publicacoes/"& simagem1 &""" alt="""& sTitulo &""" />"
    if(simagem2 <> "" and not isnull(simagem2)) then simagem2 = "<br/><br/><img src="""& sLinkImagem &"/EcommerceNew/upload/publicacoes/"& simagem2 &""" alt="""& sTitulo &""" />"
    if(simagem3 <> "" and not isnull(simagem3)) then simagem3 = "<br/><br/><img src="""& sLinkImagem &"/EcommerceNew/upload/publicacoes/"& simagem3 &""" alt="""& sTitulo &""" />"

end if

%>

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <!--#include virtual= "head.asp"-->

    <body>
        <!--#include virtual= "header.asp"-->

        <div class=" desktop-margin-top-90" >
            <div class="margin-60-0 ">
                <div class="container">
			        <div class="row ">

				        <div class="col-sm-12">
					        <h1 class="cor-4 extra-bold "><span class="shape6"><%= sTitulo %></span></h1>

                            <%= simagem1 %>
                            <%= simagem2 %>
                            <%= simagem3 %>

                            <br /><br />

                            <h4><%= NOME_2 %></h4>
                            
                            <br />

                            <p><%= DESCRICAO %></p>

                            <p>&nbsp;</p>
                            <div class="share-box">
                                <div class="socials-box"> 
                                    <iframe src="https://www.facebook.com/plugins/share_button.php?href=<%= sPathHttps %>detalhe-receita/<%= sNomeProdutoSeo %>/<%= sSecao %>/&layout=button&size=small&mobile_iframe=true&appId=1758216144235526&width=97&height=20" width="97" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                                </div>
                            </div>


				        </div>
				
				    </div>
			    </div>
		    </div>
	    </div>

        <!--#include virtual= "footer.asp"-->
    </body>

</html>
