<!--#include virtual= "/hidden/funcoes.asp"-->
<%
dim sClassHeader: sClassHeader = "bg-1"

if(Session("Id") <> "") then
	if(Session("ItemCount") > 0) then 
        Response.Redirect "/pagamento"
    else
        Response.Redirect "/arearestrita"
	end if  
end if

dim sPaginaAnterior, sPaginaRedirect
dim sLogin, sSenha, sEmail, sAcao, boolAcao, boolAtencao, sUrl, sIdProduto, boolAtencaoEmail, sDisponivel, sMP, rastreio
dim sBannerPagina
   
sEtapa              = "2"
boolAcao 			= false
boolAtencao 		= false
boolAtencaoEmail	= false
sPaginaAnt          = request.servervariables("HTTP_REFERER")
sAcao 	            = Replace_Caracteres_Especiais(trim(request("acao")))
sLogin 			    = trim(request("login"))
sSenha 			    = Replace_Caracteres_Especiais(trim(request("senha")))
sEmail 			    = Replace_Caracteres_Especiais(trim(request("email")))
sProduto 		    = Replace_Caracteres_Especiais(trim(request("produto")))
sUrl			    = Replace_Caracteres_Especiais(trim(request("urlAva")))
sIdProduto		    = Replace_Caracteres_Especiais(trim(request("produto")))
sMP                 = Replace_Caracteres_Especiais(trim(request("mp")))
ReturnUrl		    = Replace_Caracteres_Especiais(trim(request("ReturnUrl")))
listadesejo         = Replace_Caracteres_Especiais(trim(request("listadesejo")))
sPaginaAnterior     = Replace_Caracteres_Especiais(trim(request("sPaginaAnterior")))
sDisponivel         = Replace_Caracteres_Especiais(trim(request("disponivel")))
sEsgotadoL          = Replace_Caracteres_Especiais(trim(request("id_produto")))
rastreio            = Replace_Caracteres_Especiais(trim(request("rastreio")))

if(sMP = "1") then
	response.write "<script>alert('Para acompanhar o seu pedido faça o login e clique novamente em MEUS PEDIDOS.');</script>"
end if

if(sEsgotadoL <> "")then
    response.write "<script>AbrePopUp('/pops/pop_avisodedisponibilidade.asp?id="& nProduto & "','716','520');</script>"
end if
'========================================================================

if(sProduto <> "" and not isnumeric(sProduto)) then
	response.write "<P><div align=""center"">ERRO NO SISTEMA!</div></P>"
	response.end
end if
if(sIdProduto <> "" and not isnumeric(sIdProduto)) then
	response.write "<P><div align=""center"">ERRO NO SISTEMA!</div></P>"
	response.end
end if
if(sUrl <> "" and InStr(lcase(sUrl),".asp") = 0) then 
	response.write "<P><div align=""center"">ERRO NO SISTEMA (ADMIN)</div></P>"
	response.end
end if
if(rastreio = "rastreio" and Session("Id") <> "") then 
    response.Redirect("/historico_pedidos.asp")
end if
        

'=======================================================================
sPaginaRedirect = ""

'================ RECEBE INFORMAÇÃO PARA LOGAR =========================
if((sAcao = "logar") and not (boolAtencaoEmail)) then

	if(Efetua_Login(sLogin, sSenha, "", "")) then 

        if(Session("Endereco") = "" or isnull(Session("Endereco"))) then 
            response.Redirect("/altcadastro.asp")
            response.End
        end if

        if(rastreio = "rastreio") then 
            response.Redirect("/historico_pedidos.asp")
            response.End
        end if

		if(Session("ItemCount") > 0) then 
            Response.Redirect "/pagamento"
        else
            if(ReturnUrl <> "") then 
                Response.Redirect ReturnUrl
            else
                Response.Redirect "/arearestrita"
            end if        
		end if  
	else
        sMsgJs      = "Usuário e(ou) senha inválido!"
		sPathHttps 	= ""
		boolAtencao = true
	end if

	if(not boolAtencao ) then boolAcao = true
end if

%>

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <!--#include virtual= "head.asp"-->

    <script type="text/javascript">
                <%if(boolAcao) then%>
                    <%if(sAcao = "logar") then%>         
                        <% if(ReturnUrl <> "") then %>
                            window.location.href = '<%= ReturnUrl %>';
                <% else %>
                    <% if(sPaginaRedirect <> "") then %>
                        window.location.href = '<%= sPaginaRedirect %>';			
                <% else %>
                    window.location.href = '<%= sPaginaAnterior %>';
                <% end if %>
            <% end if %>
        <% end if %>
    <%end if%>

    <%if(boolAtencao) then %>
        <% if(sMsgJs <> "" )then %>
            alert("<%=sMsgJs %>");
                <% end if %>
            <%end if %>
            <%if(boolGravado) then%>
                alert('<%= sMsgJs %>');
                window.close();
                <%end if%>

        <%if(sDisponivel = "0") then %>
            alert('Efetue seu login e clique novamente no produto, \n\ para ser notificado quando estiver disponível!');
            <%end if %>

            function ValidaLogin() {

                if (document.getElementById("login").value == "") {
                    alert("Campos obrigatorios!");
                    document.getElementById("login").focus();
                    return false;
            } else {
                    if (!ValidaEmail(document.getElementById("login").value)) {
                        alert("E-mail inválido!");
                        document.getElementById("login").focus();
                        return false;
            }
            }

                if (document.getElementById("senha").value == "") {
                    alert("Campos obrigatorios!");
                    document.getElementById("senha").focus();
                    return false;
            }
            }

                function ValidaCad() {

                    if (document.getElementById("emailCad").value == "") {
                        alert("Campos obrigatorios!");
                        document.getElementById("emailCad").focus();
                    } else {
                        if (!ValidaEmail(document.getElementById("emailCad").value)) {
                            alert("E-mail inválido!");
                            document.getElementById("emailCad").focus();
                        } else {
                            CriarCadastro();
                        }
                    }
                }

            function AbrePopUp(sUrl, sW, sH) {
                var Janela = window.open(sUrl, 'home', 'scrollbars=yes,resizable=yes,width=' + sW + ',height=' + sH)
                Janela.focus()
            }

            function CriarCadastro() {
                location.href='/cadastro.asp?e=' + document.getElementById("emailCad").value;
            }
        </script>

<body>
    <!--#include virtual= "header.asp"-->

    <div class=" desktop-margin-top-90" >
	<div class="margin-60-0 ">
		<div class="container">
			<div class="row desktop-margin-90 ">

				<div class="col-sm-6 borderRightOrange">
                        <form method="post" action="<%= Request.ServerVariables("Script_Name")%>" id="fLogin" name="fLogin" >
                        <input type="hidden" name="acao"            value="logar">
                        <input type="hidden" name="Produto"         value="<%= sProduto %>">
                        <input type="hidden" name="urlAva"          value="<%= sUrl %>">
                        <input type="hidden" name="id_produto"      value="<%= sIdProduto %>">
                        <input type="hidden" name="ReturnUrl"       value="<%= ReturnUrl %>">
                        <input type="hidden" name="sPaginaAnterior" value="<%=sPaginaAnt %>">
                        <input type="hidden" name="listadesejo"     value="<%=listadesejo %>">
                        <input type="hidden" name="rastreio"        value="<%=rastreio %>">

						<div class="row">
							<div class="col-sm-8 col-sm-push-1"><h2 class="cor-4 semi-bold "><span class="shape6">FAZER LOGIN</span></h2></div>
							<div class="col-sm-8 col-sm-push-2"><div class="form-group"><label for="emaulUser">E-MAIL:</label><input type="email" class="form-control" id="emmail_login" name="login" required="required"></div>
								<div class="form-group"><label for="pwd">SENHA:</label><input id="password_login" type="password" class="form-control" name="senha" required="required"></div>
								<div class="form-group"><a href="#" onclick="javascript: AbrePopUp('/pop_esqueceusenha.asp', '450', '250');">Esqueceu sua senha?</a></div>
								<div class="form-group"><button type="submit" class="btn botao-5 pull-right">ENTRAR</button></div>
							</div>
						</div>	
					</form>
				</div>


				<div class="col-sm-6">

					<form method="post" id="fLogin2" name="fLogin2" action="/cadastro.asp">
						<div class="row">
							<div class="col-sm-8 col-sm-push-1"><h2 class="cor-4 semi-bold "><span class="shape6">NÃO É CADASTRADO?</span></h2></div>
							<div class="col-sm-8 col-sm-push-2">
								<div class="form-group"><label for="emaulUser2">E-MAIL:</label><input type="email" class="form-control" id="emailCad" name="emailCad" required="required"></div>
								<div class="form-group"><button type="submit" class="btn botao-5 pull-right" >CONTINUAR</button></div>
							</div>
						</div>	

                        <div class="register-benefits" style="margin: 28px 0 0 57px;">
                            <h5>Cadastre-se hoje mesmo e você poderá:</h5>
                            <ul>
                            <li>Visualizar e alterar seu carrinho de compras</li>
                            <li>Acompanhar seus pedidos</li>
                            <li>Manter um registro atualizado de suas compras</li>
                            </ul>
                        </div>

					</form>

				</div>

			</div>
		</div>
	</div>


</div>

    <!--#include virtual= "footer.asp"-->

</body>
</html>
