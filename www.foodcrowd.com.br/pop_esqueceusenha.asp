﻿<!--#include virtual="/hidden/funcoes.asp"-->
<%
' VARIAVEIS ================================
Dim sIdCateg, boolenviado, boolGravado

boolenviado = (Replace_Caracteres_Especiais(request("enviado")) = "ok")

if(boolenviado) then 
	if(InStr(Replace_Caracteres_Especiais(replace(trim(request("email")),"'","")),"@") > 0) then 
		call Esqueceu_Senha(Replace_Caracteres_Especiais(replace(trim(request("email")),"'","")))
	else
		sMsgJs = "E-mail inválido!"
	end if
end if
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><%=Application("NomeLoja")%></title>
    <link rel="stylesheet" type="text/css" href="/css/style.css">

    <script type="text/javascript" src="/js/funcoes.js"></script>

    <script language="JavaScript" type="text/JavaScript">
    <!--
    <%if(boolenviado) then%>
        alert('<%= sMsgJs %>');

        <% if (InStr(sMsgJs, "inválido") = 0) then %>
            window.close();
        <%end if%>

    <% end if%>

    function valida() {
        var objForm = document.forma;
	
        if(! ValidaEmail(objForm.email.value)) {
            alert('E-mail inválido!');
            objForm.email.focus();
            objForm.email.select();
            return false;        
        }	
    }
    //-->
    </script>
</head>

<body style="min-height:150px">

<div style="padding:10px;">
<h3>ESQUECEU SENHA?</h3>

<form name="forma"  method="post" onSubmit="javascript: return valida();" action="<%= Request.ServerVariables("Script_Name")%>">
<input type="hidden" name="enviado" value="ok">
    <ul class="form" style="padding:5px;">
        <p>Se voc&ecirc; esqueceu sua senha, digite abaixo o e-mail que cadastrou na loja.</p><br>
           <strong style="color:#666;">Email:</strong></label>
           <input name="email" type="text" class="seuemail campo" size="40" validar="1" style="width:190px;"> <input name="logar" type="submit"  value="Enviar"  class="btForm btProsseguir"/>
    </ul>
</form>
</div> 


</body>
</html>