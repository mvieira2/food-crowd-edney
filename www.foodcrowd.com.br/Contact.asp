<!--#include virtual= "/hidden/funcoes.asp"-->

<%
Dim boolGravado, boolenviado
dim sConteudoInst, sTitulo
dim sSecao,  sAtivo(10)

boolGravado = false
boolenviado = (request("enviado") = "ok")

if(boolenviado ) then 
	if VerificaCaptcha(Replace_Caracteres_Especiais(trim(request("Cap"))),Replace_Caracteres_Especiais(trim(request("Captcha")))) then 
        call Executar()
    end if
end if

function Email_BlackList(email)
	Email_BlackList	=	True
	call abre_conexao
	sSql = "Select id from tb_email_blacklist where email like '%" & lcase(trim(email)) & "%'"
	set Rs = oConn.execute(sSql)
	if (not Rs.eof) then Email_BlackList	=	False
	call fecha_conexao
end function


' ==========================================
sub Executar()
    dim sCorpo_Msg, sNome, sEmail, sMensagem, sTelefone, sAssunto, sCaptcha
	
	sNome 		= Replace_Caracteres_Especiais(request("iNome"))
    iTelefone 	= Replace_Caracteres_Especiais(request("iTelefone"))
    iAssunto 	= Replace_Caracteres_Especiais(request("iAssunto"))
	sEmail 		= Replace_Caracteres_Especiais(request("iEmail"))
	sMensagem 	= Replace_Caracteres_Especiais(request("tMsg"))

    if(sNome = "" OR sEmail = "" OR sMensagem = "") then 
    	sMsgJs 		= "E-mail não enviado!"
    	boolGravado = true
        exit sub
    end if
	
	sCorpo_Msg =              "<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.01 Transitional//EN"">" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "<html>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "<head>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "	<title></title>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "</head>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "<body>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "<div class=""formTrabalhe"">" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "<h3 class=""tituPag"">Contato por email</h3>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & " <br>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "   <table cellspacing=""0"" cellpadding=""5"" width=""300"">" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "     <tr>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "			<td class=""tde"">Nome</td>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "			<td class=""tdd"">"& sNome &"</td>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "     </tr>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "     <tr>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "			<td class=""tde"">Email</td>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "			<td class=""tdd"">"& sEmail &"</td>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "     </tr>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "     <tr>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "			<td class=""tde"">Telefone</td>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "			<td class=""tdd"">"& iTelefone &"</td>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "     </tr>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "     <tr>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "			<td class=""tde"">Assunto</td>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "			<td class=""tdd"">"& iAssunto &"</td>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "     </tr>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "     <tr>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "			<td class=""tde"">Mensagem</td>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "			<td class=""tdd"">"& replace(trim(sMensagem),vbcrlf,"<br>") &"</td>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "     </tr>" & vbcrlf	
	sCorpo_Msg = sCorpo_Msg & " </table>" & vbcrlf	
	sCorpo_Msg = sCorpo_Msg & "<div>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "</body>" & vbcrlf
	sCorpo_Msg = sCorpo_Msg & "<html>" & vbcrlf

	call Envia_Email(Application("Mailpedidos"), Application("Nome") & "<sistema01@foodcrowd.com.br>", Application("NomeLoja"), "Contato WebSite - " & iAssunto, sCorpo_Msg)

	sMsgJs 		= "E-mail enviado com sucesso! Obrigado pelo contato! Em breve entraremos em contato."
	boolGravado = true
end sub

'================================= CAPTCHA
function VerificaCaptcha(Cap,Captcha)
	VerificaCaptcha	= false
	select case Cap
		case 1
			cod = "0266"
		case 2
			cod = "9032"
		case 3
			cod = "5610"
		case 4
			cod = "1277"
		case 5
			cod = "0501"
		case 6
			cod = "7090"
		case 7
			cod = "2111"
		case 8
			cod = "9831"
		case 9
			cod = "0003"
		case 10
			cod = "1012"
		case 11
			cod = "5123"
		case 12
			cod = "2299"
		case 13
			cod = "6974"
		case 14
			cod = "4415"
		case 15
			cod = "7618"
		case 16
			cod = "8870"
		case 17
			cod = "1610"
		case 18
			cod = "0102"
		case 19
			cod = "6048"
		case 20
			cod = "5599"
	end select

	if (cod = Captcha) then
		VerificaCaptcha = true
	else
		response.write "<script>alert('Codigo da imagem incorreto, tente novamente');history.back();</script>"
	end if
End Function

' ===========================================================
Function GeraCaptcha()
	Randomize 
	GeraCaptcha = Int(Rnd * 19 + 1)
End Function

CpCod	=	 geracaptcha()

%>

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <!--#include virtual= "head.asp"-->

    <script language="JavaScript" type="text/JavaScript">
        <!-- 
            <%if(boolGravado) then%>
                alert('<%= sMsgJs %>');
                parent.top.location.href='/';
            <% end if%>
        //-->
    </script>

<body>
    <!--#include virtual= "header.asp"-->

    <div class="banners">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="/img/banner-topo.png" alt="Los Angeles">
                    <div class="carousel-caption">
                        <div class="banner-titles">
                            <h2 class="extra-bold" >FALE CONOSCO</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="container">
    <div class="row margin-60-0 ">

        <div class="col-sm-6 col-xs-6 col-sm-push-1 text-center">
            <img src="/img/icons/icon-telefone.png" alt="" style=" width: 62%; max-width: 140px; margin: 20px; ">
            <h4 class="cor-4 semi-bold" style="margin-top:50px;">HORÁRIO DE ATENDIMENTO</h4>
            <p class="cor-4" ><%= Application("AtendimentoEmpresa") %></p>
            <p ><strong class="cor-2 semi-bold">Telefone</strong></p>
            <p class="cor-4"><a href="tel:<%= Application("telefoneloja") %>"><small><%= Application("telefoneloja") %></small></a></p>
        </div>
        <div class="col-sm-6 col-xs-6 col-sm-pull-1 text-center">
            <img src="/img/icons/icon-email.png" alt="" style=" width: 62%; max-width: 140px; margin: 20px; ">
            <h4 class="cor-4 semi-bold" style="margin-top:50px;">HORÁRIO DE ATENDIMENTO</h4>
            <p class="cor-4" ><%= Application("AtendimentoEmpresa") %></p>
            <p ><strong class="cor-2 semi-bold">E-mail</strong></p>
            <p class="cor-4"><a href="mailto:<%= Application("Mailpedidos") %>"><small><%= Application("Mailpedidos") %></small></a></p>
        </div>
    </div>
</div>


<div class="mapa margin-60-0 ">

    <div id="map" style="height:100%; height:400px;">

            <script>
            var map;
            var position = {lat: -23.5629, lng: -46.6544};
            function initMap() {
              map = new google.maps.Map(document.getElementById('map'), {
                center: position,
                zoom: 10,
                disableDefaultUI: true,
              });

              var image = '/img/icons/pointer.png';
                var beachMarker = new google.maps.Marker({
                  position: position,
                  map: map,
                  icon: image,
                  animation: google.maps.Animation.DROP,
                });
            }
            </script>

    </div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9CzY9RfUKkf9PxOV9J_Ldes1DQo2et_U&callback=initMap" ></script>

</div>


<div class="container ">
    <div class="row margin-60-0 ">
        <div class="col-xs-12">
            <h2 class="semi-bold shape3"> ENTRE EM CONTATO  </h2>

        </div>

        <div class="col-sm-12">

            <form name="forma"  method="post" onSubmit="javascript: return valida();" action="<%= Request.ServerVariables("Script_Name")%>" id="frmContato" class="margin-30-0">
            <input type="hidden" name="enviado" value="ok">

                <div class="row">
                    <div class="col-sm-4"><div class="form-group"><input type="text" class="form-control"   id="iNome"      name="iNome"        placeholder="Nome e Sobrenome"  required="required"></div></div>
                    <div class="col-sm-4"><div class="form-group"><input type="email" class="form-control"  id="iEmail"     name="iEmail"       placeholder="E-mail"            required="required"></div></div>
                    <div class="col-sm-4"><div class="form-group"><input type="text" class="form-control"   id="iTelefone"  name="iTelefone"    placeholder="Telefone"         required="required"></div></div>
                    <div class="col-sm-12"><div class="form-group"><textarea class="form-control" rows="5"  id="tMsg"       name="tMsg"         placeholder="Mensagem"          required="required"></textarea></div></div>
                    <div class="col-sm-12"><div class="form-group">
                        <img align="middle" src="/captcha/imagens/<%= CpCod %>.jpg">&nbsp;<span class="TextoMenor2"><span class="Texto">
                        <input type="Hidden" 	name="Cap" 		id="Cap" 		value="<%=CpCod%>" >
                        <input type="password"	name="Captcha"	id="Captcha" size="8" maxlength="8" style="border:1px solid; width:77px" class="required">
                    </div></div>
                    <div class="col-sm-12 text-right"><button class="btn botao-4">ENVIAR</button></div>
                </div>
            </form>

        </div>
    </div>
</div>

    <!--#include virtual= "footer.asp"-->


</body>
</html>
