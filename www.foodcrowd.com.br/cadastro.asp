﻿<!--#include virtual= "/hidden/funcoes.asp"-->

<%
dim sClassHeader: sClassHeader = "bg-1"

' VARIAVEIS ================================
Dim boolenviado, boolGravado, sPaginaRedirecionar, sPaginaCad, seMail, sTipoP, RsLogin
dim sBannerPagina, boolpre
    
Dim objCriptografia
Set objCriptografia = New Criptografia

boolenviado         = (Replace_Caracteres_Especiais(request("enviado")) = "ok")
boolGravado         = false
sPaginaRedirecionar = "cadastro.asp"
seMail              = Replace_Caracteres_Especiais(replace(trim(request("emailCad")),"'",""))
boolpre             = (Replace_Caracteres_Especiais(request("pre")) = "ok")


if(boolenviado) then 
    call Gravar()
else
    if(seMail <> "" and not boolpre) then 
        call Abre_Conexao()

		sSql = "SELECT TOP 1 ID FROM TB_CLIENTES (NOLOCK) WHERE EMAIL = '"& seMail &"' and cdcliente = "& sCdCliente
		set RsLogin = oConn.execute(sSql)
	
		if(not RsLogin.eof) then
		    set RsLogin = nothing
            call Fecha_Conexao()

            response.write "<script>alert('E-mail existente na base de dados!'); location.href='/login';</script>"
            response.End
        end if
    end if
end if


' ==========================================
sub Gravar()
    'response.End

    dim sSql, sTipoNews, sCorpo_Msg
	dim sCampo(40), sData
    
	call Abre_Conexao()

	sCampo(0)       = 2
	sCampo(1)       = Replace_Caracteres_Especiais(replace(trim(request("senha")),"'",""))
	sCampo(2)       = Replace_Caracteres_Especiais(replace(trim(request("email")),"'",""))
	sCampo(4)       = Replace_Caracteres_Especiais(replace(trim(request("sobrenome")),"'",""))
	sCampo(5)       = ("'" & Replace_Caracteres_Especiais(trim(request("dia"))) & "/" & Replace_Caracteres_Especiais(trim(request("mes"))) & "/" & Replace_Caracteres_Especiais(trim(request("ano"))) & "'")
	sCampo(6)       = Replace_Caracteres_Especiais(replace(trim(request("Sexo")),"'",""))
	sCampo(7)       = Replace_Caracteres_Especiais(replace(trim(request("rg")),"'",""))
	sCampo(8)       = Replace_Caracteres_Especiais(replace(trim(request("empresa")),"'",""))
	sCampo(9)       = Replace_Caracteres_Especiais(replace(trim(request("empresa_tipo")),"'",""))
	sCampo(10)      = Replace_Caracteres_Especiais(replace(trim(request("forma_vendas_lojistas")),"'",""))
	sCampo(11)      = Replace_Caracteres_Especiais(replace(trim(request("internet_link")),"'",""))
	sCampo(12)      = Replace_Caracteres_Especiais(replace(trim(request("outros_especificar")),"'",""))
	sCampo(13)      = Replace_Caracteres_Especiais(replace(trim(request("cepc")),"'",""))
	sCampo(14)      = Replace_Caracteres_Especiais(replace(trim(request("cidade")),"'",""))
	sCampo(15)      = Replace_Caracteres_Especiais(replace(trim(request("provincia")),"'",""))
	sCampo(16)      = Replace_Caracteres_Especiais(replace(trim(request("bairroh")),"'",""))
	sCampo(17)      = Replace_Caracteres_Especiais(replace(trim(request("numeroc")),"'",""))
	sCampo(18)      = Replace_Caracteres_Especiais(replace(trim(request("complemento")),"'",""))
	sCampo(19)      = "BR"
	sCampo(20)      = Replace_Caracteres_Especiais(replace(trim(request("japao")),"'",""))
	sCampo(21)      = Replace_Caracteres_Especiais(replace(trim(request("tcomercial")),"'",""))
	sCampo(22)      = Replace_Caracteres_Especiais(replace(trim(request("tfax")),"'",""))
	sCampo(23)      = Replace_Caracteres_Especiais(replace(trim(request("tcelular")),"'",""))
	sCampo(24)      = Replace_Caracteres_Especiais(replace(trim(request("newsletter")),"'",""))
	sCampo(26)      = Replace_Caracteres_Especiais(replace(trim(request("nacionalidade")),"'",""))

	sCampo(27)      = Replace_Caracteres_Especiais(replace(trim(request("NomeMae")),"'",""))
	sCampo(28)      = Replace_Caracteres_Especiais(replace(trim(request("CPF")),"'",""))
	
	sCampo(29)      = Replace_Caracteres_Especiais(replace(trim(request("TipoPessoa")),"'",""))
	sCampo(30)      = Replace_Caracteres_Especiais(replace(trim(request("TipoEndereco")),"'",""))
	sCampo(31)      = Replace_Caracteres_Especiais(replace(trim(request("Enderecoh")),"'",""))
	
	sCampo(32)      = Replace_Caracteres_Especiais(replace(trim(request("InscEstadual")),"'",""))
	sCampo(33)      = Replace_Caracteres_Especiais(replace(trim(request("CNPJ")),"'",""))

    if(boolpre) then 
        sCampo(29) = "F"
        sCampo(3) = Replace_Caracteres_Especiais(replace(trim(request("nome")),"'",""))
    else
	    if(lcase(sCampo(29)) = "f" ) then
            sCampo(3) = Replace_Caracteres_Especiais(replace(trim(request("nome")),"'",""))
		    call Validacao_Lado_Servidor(sCampo(28),"CPF")
	    else
		    sCampo(3) = Replace_Caracteres_Especiais(replace(trim(request("nomeR")),"'",""))
		    call Validacao_Lado_Servidor(sCampo(28),"CNPJ")
	    end if
    end if

	if(sCampo(26) = "esp") then sCampo(26) = Replace_Caracteres_Especiais(replace(trim(request("Nacionalidade_especificar")),"'",""))
	if(sCampo(15) = "") then sCampo(15) = "null"
    if(sCampo(15) = "") then sCampo(15) = "null"

	' GRAVA O EMAIL NA TABELA DE NEWSLETTER
	if((sCampo(24) <> "") and (sCampo(2) <> "")) then
	    sSql = "Select top 1 descricao from TB_NEWSLETTER_EMAILS where descricao = '"& sCampo(2) &"' "
	    set Rs = oConn.execute(sSql)
		if(Rs.eof) then oConn.execute("Insert into TB_NEWSLETTER_EMAILS (Id_Tipo_Newsletter, Nome, Descricao, Ativo) values ("& sCampo(24) &", '"& sCampo(3) &"', '"& sCampo(2) &"', 1)")
	    set Rs = nothing
	end if

	if(sCampo(20) = "1") then sCampo(19) = "Japao"
	if(sCampo(10) <> "Pela Internet / Endereco/ Link") then sCampo(11) = ""

	if(sCampo(31) = "") then sCampo(31) = Replace_Caracteres_Especiais(replace(trim(request("EnderecoC")),"'",""))
    if(sCampo(16) = "") then sCampo(16) = Replace_Caracteres_Especiais(replace(trim(request("BairroC")),"'",""))
	
	sSql = "Select top 1 id, datacad from TB_CLIENTES (NOLOCK) where cdcliente = "& sCdCliente &" and Cpf = '"& sCampo(28) &"' "
    if(sCampo(29) <> "F") then
	    sSql = "Select top 1 id, datacad from TB_CLIENTES (NOLOCK) where cdcliente = "& sCdCliente &" and Cnpj = '"& sCampo(33) &"' "
	end if

    if(boolpre) then
	    sSql = "Select top 1 id, datacad from TB_CLIENTES (NOLOCK) where cdcliente = "& sCdCliente &" and email = '"& sCampo(2) &"' "
    end if

    set Rs = oConn.execute(sSql)

    if(not IsNumeric(sCampo(15)) ) then 
		sSQl = "Select top 1 ID from TB_ESTADOS WITH (nolock)  WHERE upper(DESCRICAO) = '"& ucase(Replace_Caracteres_Especiais(replace(trim(request("provinciaLegenda")),"'",""))) &"'"
		set RsC = oConn.execute(sSQl)
		if(not RsC.eof) then sCampo(15) = RsC("ID")
		set RsC = nothing

        sCampo(14) = Replace_Caracteres_Especiais(replace(trim(request("cidade")),"'",""))
    end if

	' TRAVA CONTRA HACKER SAFE ---------------------------
	if((not IsNumeric(sCdCliente)) OR not IsNumeric(sCampo(0))) then
		response.write "ERRO NO SISTEMA! (1)"
		response.end
	end if
	if(sCampo(28) <> "") then
		if(not IsNumeric(sCampo(28))) then
			response.write "ERRO NO SISTEMA! (2)"
			response.end
		end if
	end if
	
    if(rs.eof) then
        if(boolpre) then 
            sCampo(5) = "null"
        end if

        sSql = "Insert into TB_CLIENTES (CdCliente, Id_Tipo, Id_Provincia, Nome, SobreNome, DtNascimento, Sexo, Rg, Empresa, Empresa_Tipo, " & _
				"Empresa_FormaVenda, Empresa_FormaVenda_Link, Empresa_FormaVenda_Outras, Cep, Endereco, Cidade, Bairro, Numero, " & _
				"Complemento, Pais, Telefone, Fax, Celular, Email, Senha, Nacionalidade, NomeMae, CPF, TipoPessoa, TipoEndereco, InscEstadual, CNPJ, Obs, ativo) "& _ 
				" values ("& sCdCliente &", "& sCampo(0) &", "& sCampo(15) &", '"& sCampo(3) &"', '"& sCampo(4) &"', "& sCampo(5) &", '"& sCampo(6) &"', "& _
				" '"& sCampo(7) &"', '"& sCampo(8) &"', '"& sCampo(9) &"', '"& sCampo(10) &"', '"& sCampo(11) &"', " & _
				" '"& sCampo(12) &"', '"& sCampo(13) &"', '"& sCampo(31) &"', '"& sCampo(14) &"', '"& sCampo(16) &"', '"& sCampo(17) &"', " & _
				" '"& sCampo(18) &"', '"& sCampo(19) &"', '"& sCampo(21) &"', '"& sCampo(22) &"', '"& sCampo(23) &"', '"& sCampo(2) &"', '"& objCriptografia.QuickEncrypt(sCampo(1)) &"', '"& sCampo(26) &"', " & _
				" '"& sCampo(27) &"', '"& sCampo(28) &"', '"& sCampo(29) &"', '"& sCampo(30) &"', '"& sCampo(32) &"', '"& sCampo(33) &"' , '"& sCampo(34) &"', " & _
				" 1) "
        'RESPONSE.WRITE sSql
        'RESPONSE.END
	    oConn.execute(sSql)

        boolGravado = true
		sMsgJs 		= "Obrigado pela preferência e boas compras!"
	
		sCorpo_Msg = "<html>"
		sCorpo_Msg = sCorpo_Msg & "<head>"
		sCorpo_Msg = sCorpo_Msg & "<meta http-equiv=""Content-Type"" content=""text/html; charset=iso-8859-1"" >"
		sCorpo_Msg = sCorpo_Msg & "<title>"& Application("NomeLoja")&"</title>"
		sCorpo_Msg = sCorpo_Msg & "</head>"
		sCorpo_Msg = sCorpo_Msg & "<body>"
		sCorpo_Msg = sCorpo_Msg & "<div id=""wrap"">"
		sCorpo_Msg = sCorpo_Msg & "<div id=""header"">"
		sCorpo_Msg = sCorpo_Msg & "<h1><a href="""& Application("URLdaLoja") &"""><img src="""& Application("URLdaLoja") &"/img/logo.png"" alt="""& ucase(Application("Nome")) &""" border=""0""></a></h1>"
		sCorpo_Msg = sCorpo_Msg & "</div>"
		sCorpo_Msg = sCorpo_Msg & "    <div id=""content"" class=""indique"">"
		sCorpo_Msg = sCorpo_Msg & "    	<p>Olá, <strong>"& sCampo(3) & "!</strong></p><br>"

                sCorpo_Msg = sCorpo_Msg & "Seja bem-vindo e muito obrigado por se juntar à comunidade <b>"& Application("Nome") &"</b>!<br><br>"

                sCorpo_Msg = sCorpo_Msg & "Nossa missão é levar o que há de melhor da nossa agropecuária direto para a sua mesa, de maneira conveniente, transparente e sustentável – dando aos produtores (a experiência de comer bem começa com eles!) a oportunidade de compartilhar suas histórias, suas paixões, seus credos... aquilo que que faz a sua comida especial.<br><br>"

                sCorpo_Msg = sCorpo_Msg & "São produtos originados criteriosamente, do tipo que dificilmente você encontrará em supermercados, manuseados com carinho e entregues diretamente na sua porta.<br><br>"

                sCorpo_Msg = sCorpo_Msg & "Convidamos você a dar adeus à comida de procedência desconhecida, descobrir sabores incríveis e compartilhar a experiência de comer bem!<br><br>"

                sCorpo_Msg = sCorpo_Msg & "<a href=""https://www.foodcrowd.com.br/produtos"" >Conheça nossos produtos!</a><br><br>"

                sCorpo_Msg = sCorpo_Msg & "Um grande abraço.<br>"
                sCorpo_Msg = sCorpo_Msg & "Equipe <b>"& Application("Nome") &"</b><br><br>"

		sCorpo_Msg = sCorpo_Msg & "    </div>"
		sCorpo_Msg = sCorpo_Msg & "  </div>"	
		sCorpo_Msg = sCorpo_Msg & "</body>"
		sCorpo_Msg = sCorpo_Msg & "</html>"	
			
        'response.write sCorpo_Msg
        'response.End

 		call Envia_Email(sCampo(2), Application("Nome")	& "<sistema01@foodcrowd.com.br>", Application("NomeLoja"), "Confirmação de cadastro - Bem Vindo!", sCorpo_Msg)
        'call Envia_Email("edneyrulli@yahoo.com.br", Application("Nome")	& "<sistema01@foodcrowd.com.br>", Application("NomeLoja"), "Confirmação de cadastro - Bem Vindo!", sCorpo_Msg)
		
        if(boolpre) then 
            sPaginaCad = "/"
        else    
		    if(Efetua_Login(sCampo(2), sCampo(1), "", "")) then 
                if(Session("ItemCount") <> "" and Session("ItemCount") <> "0") then 
    			    sPaginaCad = "/pagamento"
                else
                    sPaginaCad = "/"
                end if
		    end if
        end if

	else
		sMsgJs 		= "CPF já existente no sistema.\n\nCadastro efetuado em " & FormatDateTime(rs("datacad"),2) & "."
        boolGravado = true
		sPaginaCad 	= "/login"
	end if
    set Rs = nothing

	call Fecha_Conexao()
end sub
Set objCriptografia = Nothing


%>

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <!--#include virtual= "head.asp"-->

    <script type="text/javascript" src="/js/cep_ajax.js"></script>
    <script type="text/javascript" src="/js/ajax.js"></script>

    <script language="JavaScript" type="text/JavaScript">
            <!--
            <%if(boolGravado) then%>
                alert('<%= sMsgJs %>');
                parent.top.location.href='<%= sPaginaCad %>';
            <%end if%>

            $(document).ready(function() {
                $("#finalizar_final").click(function () {
                    return validaCad();
                });

            });

            function ValidarNumero(obj) {
                // var expressao = /^[0-9]/;
                var expressao = /^\d+$/;
	
                if(!expressao.test(obj.value)) {
                    return false;
                } else {
                    return true;
                }
            }

            function validaCad() {
                var objForm = document.forma;
                var reDigits = /^\d+$/;
		
                <% if (not boolpre) then %>
                    if (objForm.TipoPessoa[0].checked == true) {
                <% else %>
                    if (true) {
                <% end if %>

                    if (objForm.nome.value == '') {
                        alert('Campo NOME é Obrigatório!');
                        objForm.nome.focus();
                        return false;
                    }
                            
                    <% if (not boolpre) then %>
                        if (objForm.cpf.value == '') {
                            alert('CPF inválido!');
                            objForm.cpf.focus();
                            objForm.cpf.select();
                            return false;
                        } else {
                            if (!ValidaCpf(objForm.cpf.value)) {
                                alert('CPF inválido!');
                                objForm.cpf.focus();
                                objForm.cpf.select();
                                return false;
                            }
                        }
                    <% end if %>

                } else {
                    if(objForm.nomeR.value == '') {
                        alert('Campo RAZÃO SOCIAL é Obrigatório!');
                        objForm.nomeR.focus();
                        return false;	
                    }	
                    if(objForm.CNPJ.value == '') {
                        alert('CNPJ inválido!');
                        objForm.CNPJ.focus();
                        return false;	
                    }
                    if (objForm.InscEstadual.value == '') {
                        alert('Campo INSCRIÇÃO ESTADUAL é Obrigatório!');
                        objForm.InscEstadual.focus();
                        return false;	
                    }					
                }	

                if(objForm.email.value.indexOf("-") > 0) {
                    alert('Seu E-mail não pode contem caracteres especiais!');
                    objForm.email.focus();
                    objForm.email.select();
                    return false;        
                }
                if(! ValidaEmail(objForm.email.value)) {
                    alert('E-mail Invalido!');
                    objForm.email.focus();
                    objForm.email.select();
                    return false;        
                }

                if(objForm.senha.value != '' && objForm.senha.value != '') {
                    if(objForm.senha.value != objForm.conf_senha.value) {
                        alert('Campo senha e confirma senha estão inválidos!');
                        objForm.senha.focus();
                        return false;
                    } else {
                        if(objForm.senha.value.length < 6 ) {
                            alert('Campo senha tem que ser maior que 6!');
                            objForm.senha.focus();
                            return false;
                        } 
                    }
                } else {
                    alert('Todos os campos são obrigatórios!');
                    objForm.senha.focus();
                    return false;        	
                }

                if (objForm.tcomercial.value != '') {
                    if (!ValidarNumero(objForm.tcomercial)) {
                        alert('Campo TELEFONE aceita apenas números!');
                        objForm.tcomercial.focus();
                        return false;
                    }
                }  else {
                    alert('Campo TELEFONE aceita apenas números!');
                    objForm.tcomercial.focus();
                    return false;
                }

                if (objForm.tcomercial.value != '') {
                    if (objForm.tcomercial.value.length < 7) {
                        alert('Campo TELEFONE aceita no mínimo 7 números.');
                        objForm.tcomercial.focus();
                        return false;
                    }
                }

                if (objForm.tcomercial.value != '') {
                    if (objForm.tcomercial.value.substring(0,5) == '00000' || 
                        objForm.tcomercial.value.substring(0,5) == '11111' || 
                        objForm.tcomercial.value.substring(0,5) == '22222' || 
                        objForm.tcomercial.value.substring(0,5) == '33333' || 
                        objForm.tcomercial.value.substring(0,5) == '44444' || 
                        objForm.tcomercial.value.substring(0,5) == '55555' || 
                        objForm.tcomercial.value.substring(0,5) == '66666' || 
                        objForm.tcomercial.value.substring(0,5) == '77777' || 
                        objForm.tcomercial.value.substring(0,5) == '88888' || 
                        objForm.tcomercial.value.substring(0,5) == '99999') {

                        alert('TELEFONE inválido!');
                        objForm.tcomercial.focus();
                        return false;
                    }
                        }

                if (objForm.tcelular.value != '') {
                    if (!ValidarNumero(objForm.tcelular)) {
                        alert('Campo tcelular aceita apenas números!');
                        objForm.tcelular.focus();
                        return false;
                    }
                }  else {
                    alert('Campo tcelular aceita apenas números!');
                    objForm.tcelular.focus();
                    return false;
                }

                if (objForm.tcelular.value != '') {
                    if (objForm.tcelular.value.length < 7) {
                        alert('Campo tcelular aceita no mínimo 7 números.');
                        objForm.tcelular.focus();
                        return false;
                    }
                }

                <% if (not boolpre) then %>
                    if(objForm.cepc.value == '') {
                        alert('cep inválido!');
                        objForm.cepc.focus();
                        return false;	
                    } else {
                        if (!ValidarNumero(objForm.cepc)) {
                            alert('Campo cep aceita apenas números!');
                            objForm.cepc.focus();
                            return false;
                        }
                    }

                    if (objForm.cepc.value != '') {
                        if (objForm.cepc.value.length < 8) {
                            alert('Campo CEP deve conter 8 dígitos.');
                            objForm.cepc.focus();
                            return false;
                        }
                    }	
                    if(objForm.enderecoc.value == '') {
                        alert('Campo ENDEREÇO é Obrigatório!');
                        objForm.enderecoc.focus();
                        return false;	
                    }
                    else {
                        if(objForm.enderecoc.value.length < 2 ) {
                            alert('Campo ENDEREÇO está inválido!');
                            objForm.enderecoc.focus();
                            return false;
                        } 
                    }
                    if (objForm.numeroc.value != '') {
                        if (!ValidarNumero(objForm.numeroc)) {
                            alert('Campo NÚMERO aceita apenas números!');
                            objForm.numeroc.focus();
                            return false;
                        }
                    } else {
                        alert('Campo NÚMERO aceita apenas números!');
                        objForm.numeroc.focus();
                        return false;
                    }
                    if (objForm.bairroc.value == ''  || objForm.bairroc.value == 'undefined') {
                        alert('Campo BAIRRO é Obrigatório!');
                        objForm.bairroc.focus();
                        return false;
                    }		
                    else {
                        if(objForm.bairroc.value.length < 3 ) {
                            alert('Campo BAIRRO está inválido!');
                            objForm.bairroc.focus();
                            return false;
                        } 
                    }
                    if (objForm.cidade.value == '' || objForm.cidade.value == 'undefined') {
                        alert('Campo CIDADE é Obrigatório!');
                        objForm.cidade.focus();
                        return false;
                    }
                    if (objForm.provincia.value == '' || objForm.provincia.value == 'undefined') {
                        alert('Campo ESTADO é Obrigatório!');
                        objForm.provincia.focus();
                        return false;
                    }
               	
                    if (objForm.provincia.value == '') {
                        alert('Campo ESTADO é Obrigatório!');
                        return false;
                    }
                    var objForm = document.forma;
	
                    for(var a=0; a < objForm.elements.length; a++) { 
                        if (objForm.elements[a].validar=="1") {
                            if(objForm.elements[a].value == '') {
                                alert('Todos os campos são obrigatórios!');
                                objForm.elements[a].focus();
                                return false;        
                            }
                        }
                    }

                        return true;

               <% end if %>
            }
    

        var reEmail1 = /^[\w!#$%&'*+\/=?^`{|}~-]+(\.[\w!#$%&'*+\/=?^`{|}~-]+)*@(([\w-]+\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
        var reEmail2 = /^[\w-]+(\.[\w-]+)*@(([\w-]{2,63}\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
        var reEmail3 = /^[\w-]+(\.[\w-]+)*@(([A-Za-z\d][A-Za-z\d-]{0,61}[A-Za-z\d]\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
        var reEmail = reEmail3;
 
        function ValidaEmail(campoemail)
        {
            eval("reEmail = reEmail");
            if (reEmail.test(campoemail)) {
                return true;
            } else if (campoemail != null && campoemail != "") {
                return false;
            }
        } 


        function MarcarCampos() {
            <%if(Replace_Caracteres_Especiais(request("cep")) <> "") then%>	document.forma.cep.value = '<%= Replace_Caracteres_Especiais(request("cep")) %>';<%end if%>
            <%if(Replace_Caracteres_Especiais(request("estado")) <> "") then%>document.forma.provincia.value = '<%= Replace_Caracteres_Especiais(request("estado")) %>';<%end if%>
            <%if(Replace_Caracteres_Especiais(request("cidade")) <> "") then%>document.forma.cidade.value = '<%= Replace_Caracteres_Especiais(request("cidade")) %>';<%end if%>
            <%if(Replace_Caracteres_Especiais(request("bairro")) <> "") then%>document.forma.bairro.value = '<%= Replace_Caracteres_Especiais(request("bairro")) %>';<%end if%>
            }

        function abrir_cep(v1, sC, sW, sH){
            janela = window.open(v1 + '?campo=' + eval('document.forma.' + sC + '.value'), "ToDo","width="+ sW +",height="+ sH +",location=no,resizable=yes, menubar=no,status=no, scroolbar=auto, toolbar=no, screenX=1, screenY=1, top=10, left=10 ")
        }


    function MostraDep(sbt) {
        if(sbt == 'F') {
            document.getElementById('pf1').style.display='block';
            document.getElementById('pf2').style.display='block';
            document.getElementById('pf3').style.display = 'block';
            document.getElementById('pf4').style.display = 'block';

            document.getElementById('pj1').style.display='none';
            document.getElementById('pj2').style.display='none';
            document.getElementById('pj3').style.display='none';
        } 
	
        if(sbt == 'J') {
            document.getElementById('pf1').style.display='none';
            document.getElementById('pf2').style.display='none';
            document.getElementById('pf3').style.display = 'none';
            document.getElementById('pf4').style.display = 'none';

            document.getElementById('pj1').style.display='block';
            document.getElementById('pj2').style.display='block';
            document.getElementById('pj3').style.display='block';
        }

        if(sbt == '') {
            document.getElementById('pf1').style.display='block';
            document.getElementById('pf2').style.display='block';
            document.getElementById('pf3').style.display = 'block';
            document.getElementById('pf4').style.display = 'block';

            document.getElementById('pj1').style.display='none';
            document.getElementById('pj2').style.display='none';
            document.getElementById('pj3').style.display='none';
        } 
    }
        //-->
    </script>

<body>

	<!--#include virtual= "header.asp"-->

    <div class=" desktop-margin-top-90" >
	<div class="margin-60-0 ">
		<div class="container">
			<div class="row desktop-margin-90 ">


				<div class="col-sm-12">

                    <form name="forma"  method="post" action="<%= Request.ServerVariables("Script_Name")%>" >

                        <input type="hidden" name="enviado" value="ok">
                        <input name="redirecionar" type="hidden" value="">
                        <input type="hidden" name="enderecoH"   id="enderecoH" >
                        <input type="hidden" name="bairroH"     id="bairroH"   >
                        <input type="hidden" name="cidadeH"     id="cidadeH"   >
                        <input type="hidden" name="provinciaH"  id="provinciaH">

                          <div class="row">
								<h2 class="cor-4 semi-bold ">
                                    <% if(boolpre) then %>
    									<span class="shape6">FAÇA PARTE</span>
                                    <% else %>
    									<span class="shape6">CRIE SUA CONTA</span>
                                    <% end if %>
								</h2>

                              <p>&nbsp;</p>


                              <% if(not boolpre) then %>

                                  <div class="col-sm-12">
                                    <div class="input-text form-group">
						                    <label class="label_pf " for="input_pf"><input onclick="javascript: MostraDep(this.value);" name="TipoPessoa" value="F" type="radio" class="form-control" checked>  Pessoa física</label>
						                    <label class="label_pj " for="input_pj"><input onclick="javascript: MostraDep(this.value);" name="TipoPessoa" value="J" type="radio"  class="form-control">         Pessoa jurídica</label>
                                    </div>
                                  </div>

                                    <p>&nbsp;</p>

                                  <div class="col-sm-6 form-group" id="pf1" >
                                    <label>Nome Completo:</label>
                                    <div class="input-text"><input type="text" placeholder="Nome Completo" name="nome"  maxlength="100" class="form-control" ></div>
                                  </div>

                                  <div class="col-sm-6 form-group" id="pf2">
                                    <label>CPF:</label>
                                    <div class="input-text"><input type="number" name="cpf"  placeholder="CPF"  maxlength="11" class="form-control" ></div>
                                  </div>

                                 <div class="col-sm-6 form-group" id="pj1"  style="display:none;">
                                    <label>Razão Social:</label>
                                    <div class="input-text"><input type="text" placeholder="Razão Social" name="nomeR"  maxlength="100"  class="form-control"></div>
                                  </div>

                                  <div class="col-sm-6 form-group" id="pj2"  style="display:none;">
                                    <label>CNPJ:</label>
                                    <div class="input-text"><input type="number" name="CNPJ"  class="form-control" placeholder="CNPJ"  maxlength="14" onblur="validaCnpj(this);" ></div>
                                  </div>

                                  <div class="col-sm-12 form-group">
                                    <label>E-mail:</label>
                                    <div class="input-text"><input type="email" placeholder="E-mail" name="email" value="<%= seMail %>"  maxlength="60" class="form-control" required="required"></div>
                                  </div>

                                  <div class="col-sm-6 form-group">
                                    <label>Senha:</label>
                                    <div class="input-text"><input type="password" name="senha" placeholder="Senha" class="form-control" required="required"/></div>
                                  </div>
                                  <div class="col-sm-6 form-group">
                                    <label>Confirmar Senha:</label>
                                    <div class="input-text"><input type="password" name="conf_senha" maxlength="50" placeholder="Confirme a senha"  class="form-control" required="required" /></div>
                                  </div>

                                  <div class="col-sm-6 form-group"  id="pf3">
                                    <label>Data Nascimento:</label>
                                    <div class="input-text">
                                        <table>
                                            <tr>
                                                <td><select name="dia"  class="form-control" style="width: 67px;">
	                                                <%for i = 1 to 31%>
	                                                    <option value="<%=right("00" & i, 2)%>"><%=right("00" & i, 2)%></option>
	                                                <%next%>
                                                </select></td>
                                                <td><select name="mes"  class="form-control" style="width: 67px;">
	                                                <%for i = 1 to 12%>
    	                                                <option value="<%=right("00" & i, 2)%>"><%=right("00" & i, 2)%></option>
	                                                <%next%>
                                                </select></td>
                                                <td><select name="ano" size="1"  class="form-control" style="width: 100px;">
                                                    <%for i = 1920 to 2000%>
                                                        <option value="<%=i%>"><%=i%></option>
                                                    <%next%>
                                                </select></td>
                                            </tr>
                                        </table>
                                    </div>
                                  </div>

                                  <div class="col-sm-6 form-group"  id="pj3"  style="display:none;">
                                    <label>Inscrição Estadual:</label>
                                    <div class="input-text"><input name="InscEstadual" class="form-control" type="number" size="20" maxlength="20" ></div>
                                  </div>

                                  <div class="col-sm-6 form-group"  id="pf4">
                                    <label>Sexo:</label>
                                    <div class="input-text">
							            <label class="label_masc" for="masc"><input checked="true" name="Sexo" value="M" type="radio" >Masculino</label> 
							            <label class="label_fem" for="fem"><input name="Sexo" value="F" type="radio">Feminino </label>
                                        <br /><br />
                                    </div>
                                  </div>

                                  <div class="col-sm-6 form-group">
                                    <label>Telefone:</label>
                                    <div class="input-text"><input type="number" name="tcomercial" placeholder="Telefone (com DDD)" size="12" maxlength="12" class="form-control"></div>
                                  </div>
                                  <div class="col-sm-6 form-group">
                                    <label>Celular:</label>
                                    <div class="input-text"><input type="number" name="tcelular" placeholder="Celular (com DDD)" size="12" maxlength="12" class="form-control"></div>
                                  </div>

                                  <div class="col-xs-12 form-group">
                                    <label>Cep:</label>
                                    <div class="input-text"><input type="number" maxlength="8" name="cepc"   id="cepc" onblur="preenche_endereco(this.value, '')" placeholder="CEP"  class="form-control" required="required" style="width:112px" /></div>
                                  </div>

                                  <div class="col-xs-12 form-group">
                                    <label>Endereço:</label>
                                    <div class="input-text"><input type="text" name="enderecoc" id="endereco" placeholder="Endereço" class="form-control" required="required"></div>
                                  </div>
                              
                                  <div class="col-sm-6 form-group">
                                    <label>Número:</label>
                                    <div class="input-text"><input type="number" name="numeroc" id="numero" placeholder="N°"  maxlength="10" class="form-control" required="required"></div>
                                  </div>
                              
                                  <div class="col-sm-6 form-group">
                                    <label>Complemento:</label>
                                    <div class="input-text"><input type="text"  name="complemento" placeholder="Complemento"  maxlength="100" class="form-control" ></div>
                                  </div>
                              
                                  <div class="col-sm-6 form-group">
                                    <label>Cidade:</label>
                                    <div class="input-text"><input type="text" name="cidade" id="cidade" placeholder="Cidade" class="form-control" required="required"></div>
                                  </div>
                              
                                  <div class="col-sm-6 form-group">
                                    <label>Estado:</label>
                                    <div class="input-text"><%= Monta_Combo_Provincia("provincia", ""& Pre_UF &"", " name='provinciaLegenda' id='provinciaLegenda' class='form-control' required='required' ") %></div>
                                  </div>

                                  <div class="col-sm-12 form-group">
                                    <label>Bairro:</label>
                                    <div class="input-text"><input type="text"  name="bairroc" id="bairro" placeholder="Bairro" maxlength="60" class="form-control" required="required"></div>
                                  </div>

                              <% else %>
                                        <input type="hidden" name="pre" value="ok">


                                      <div class="col-sm-6 form-group" id="pf1" >
                                        <label>Nome Completo:</label>
                                        <div class="input-text"><input type="text" placeholder="Nome Completo" name="nome"  maxlength="100" class="form-control" ></div>
                                      </div>

                                      <div class="col-sm-6 form-group">
                                        <label>E-mail:</label>
                                        <div class="input-text"><input type="email" placeholder="E-mail" name="email" id="email" maxlength="60" class="form-control"></div>
                                      </div>

                                      <div class="col-sm-6 form-group">
                                        <label>Telefone:</label>
                                        <div class="input-text"><input type="number" name="tcomercial" placeholder="Telefone (com DDD)" size="12" maxlength="12" class="form-control" required="required"></div>
                                      </div>
                                      <div class="col-sm-6 form-group">
                                        <label>Celular:</label>
                                        <div class="input-text"><input type="number" name="tcelular" placeholder="Celular (com DDD)" size="12" maxlength="12" class="form-control" required="required"></div>
                                      </div>

                                      <div class="col-sm-6 form-group">
                                        <label>Senha:</label>
                                        <div class="input-text"><input type="password" name="senha" placeholder="Senha" class="form-control" required="required"/></div>
                                      </div>
                                      <div class="col-sm-6 form-group">
                                        <label>Confirmar Senha:</label>
                                        <div class="input-text"><input type="password" name="conf_senha" maxlength="50" placeholder="Confirme a senha"  class="form-control" required="required"/></div>
                                      </div>

                                      <div class="col-sm-12 form-group"  id="pf4">
                                        <label>Sexo:</label>
                                        <div class="input-text">
							                <label class="label_masc" for="masc"><input checked="true" name="Sexo" value="M" type="radio" >Masculino</label> 
							                <label class="label_fem" for="fem"><input name="Sexo" value="F" type="radio">Feminino </label>
                                            <br /><br />
                                        </div>
                                      </div>

                              <% end if %>

                              <div class="col-xs-12 form-group">
                                <div class="billing-checkbox"><input name="newsletter" type="Checkbox"  value="119" checked="checked"> Desejo receber informações e promoções ?</div>
                                <p style="color: red;"><b style="color: red;">Atenção:</b> Caso não receba o E-mail de confirmação, pedimos que verifique a pasta de Span/ Bulk/ Junk do seu provedor de E-mail.</p>
                                <div class="submit-text">
                                    <input type="image" src="/img/btcomprar.JPG" name="CADASTRAR" value="CADASTRAR" id="finalizar_final" class="pull-right"/>
                                </div>
                              </div>


                        </div>
                      </form>

				</div>

			</div>
		</div>
	</div>


</div>

    <!--#include virtual= "footer.asp"-->

</body>
</html>
