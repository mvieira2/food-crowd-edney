<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!--#include virtual="/hidden/funcoes.asp"-->
<%
dim NumeroDocumento, StringGravar, ArrayCampos(15)

NumeroDocumento		 = Replace_Caracteres_Especiais(trim(request("pedido")))
StringGravar   		 = Replace_Caracteres_Especiais(trim(request("dados")))

if(NumeroDocumento = "" OR not isnumeric(NumeroDocumento)) then 
	Response.write "(NumeroDocumento - Dados n�o enviados)"
    response.end
end if

call abre_conexao()

if(StringGravar <> "") then 
    ' {"Codigo":1,"StatusPagamento":"Falha","Mensagem":"Erro desconhecido. Por favor, entre em contato com o suporte Moip"}
    ' [{"Codigo":907,"Mensagem":"C�digo de seguran�a inv�lido"}]

    ' call Envia_Email("edney@foodcrowd.com.br", "sistema01@foodcrowd.com.br", "sistema01@foodcrowd.com.br", "Alcatel - MOIP XML retorno", StringGravar)

    sSQl = "UPDATE TB_PEDIDOS SET pesquisa = '"& StringGravar &"' WHERE ID = " & NumeroDocumento
    oConn.execute(sSQl)

    Arraydados = split(StringGravar,",")

    for lCont = 0 to ubound(Arraydados)
        ArrayCampos(lCont) = Arraydados(lCont)
    next
end if


' =======================================================================================
sSQl = "SELECT TOP 1 H.ID FROM TB_PEDIDOS H (NOLOCK) WHERE H.ID = "& NumeroDocumento
set RsHistorico = oConn.execute(sSQl)
if(RsHistorico.eof) then 
	response.write "Erro 200 <br> PEDIDO N�O EXISTE " & NumeroDocumento
	response.end
end if
set RsHistorico = nothing


' =======================================================================================
if(Instr(lcase(trim(ArrayCampos(1))), "falha") > 0 ) then 
	response.write StringGravar
	response.end
end if

' =======================================================================================
'sValorPedido    = replace(replace(FormatNumber(trim(Recupera_Campos_Db_oConn("TB_PEDIDOS", NumeroDocumento, "id", "total")),2),".",""),",","")
'sValorPago      = replace(replace(replace(lcase(trim(ArrayCampos(9))),".",""),",",""),"totalpago:","")

'if(sValorPedido <> sValorPago) then
    'call Envia_Email("edney@foodcrowd.com.br", "sistema01@foodcrowd.com.br", "Alcatel4u", "Pedido: " & NumeroDocumento & " erro no valor de confirmacao", "Erro MOIP confirma��o de valor - Pedido: " & NumeroDocumento & "<br>Valor Pedido:" & sValorPedido & "<br>Valor MOIP:" & sValorPago )
    'response.write "Erro 300 <br> PEDIDO COM VALOR DIFERENTE DO CONFIRMADO"
	'response.end
'end if 

' =======================================================================================
if(NumeroDocumento <> "") then 

    if(TRIM(Recupera_Campos_Db_oConn("TB_PEDIDOS", NumeroDocumento, "id", "id_status")) = "1") then 
    
        statusMoip = replace(replace(replace(lcase(trim(ArrayCampos(0))),".",""),",",""),"{status:","")

        if(Instr(lcase(trim(statusMoip)), "status") = 0 ) then 

		    ' ATUALIZA PEDIDOS ----------------------------------------------
		    sSQl = "UPDATE TB_PEDIDOS SET ID_STATUS = 3 WHERE ID = " & NumeroDocumento
		    oConn.execute(sSQl)
		    ' ---------------------------------------------------------------

		    ' INSERI LINHA NA TABELA HISTORICO ------------------------------
		    sSQlI = "INSERT INTO TB_HISTORICOS (Id_Pedido, Id_Status, Descricao, Documento) VALUES ('"& NumeroDocumento &"', 3, '"& TRIM(Recupera_Campos_Db_oConn("TB_STATUS", "3", "id", "DESCRICAO")) &" [Moip - Erro]', '"& StringGravar & "') "
		    oConn.execute(sSQlI)
		    ' ----------------------------------------------------------------

            'ABRE IFRAME PARA TEMPLANTES DE EMAIL -----------------------------
            response.write "<iframe src=""https://integracao.foodcrowd.com.br/TemplateLojas/template.asp?id="& NumeroDocumento &"&status=3"" name=""templante"" width=""1"" marginwidth=""0"" height=""1"" marginheight=""0"" scrolling=""No"" frameborder=""0"" hspace=""0"" vspace=""0"" id=""templante"" style=""margin:0px;""></iframe>"

	        'response.write StringGravar
	        'response.end
        end if

        'response.Write lcase(trim(ArrayCampos(0))) & "<br>"
        'response.Write lcase(trim(ArrayCampos(1))) & "<br>"
        'response.Write statusMoip & "<br>"
        'response.End

        ' =================================================================================
        if(statusMoip = "autorizado") then 
		    ' ATUALIZA PEDIDOS ----------------------------------------------
		    sSQl = "UPDATE TB_PEDIDOS SET ID_STATUS = 2 WHERE ID = " & NumeroDocumento
		    oConn.execute(sSQl)
		    ' ---------------------------------------------------------------

		    ' INSERI LINHA NA TABELA HISTORICO ------------------------------
		    sSQlI = "INSERT INTO TB_HISTORICOS (Id_Pedido, Id_Status, Descricao, Documento) VALUES ('"& NumeroDocumento &"', 2, '"& TRIM(Recupera_Campos_Db_oConn("TB_STATUS", "2", "id", "DESCRICAO")) &" [Moip - Aprovado - Site]', '"	& ArrayCampos(7) & "') "
		    oConn.execute(sSQlI)
		    ' ----------------------------------------------------------------

            'ABRE IFRAME PARA TEMPLANTES DE EMAIL -----------------------------
            response.write "<iframe src=""https://integracao.foodcrowd.com.br/TemplateLojas/template.asp?id="& NumeroDocumento &"&status=2"" name=""templante"" width=""1"" marginwidth=""0"" height=""1"" marginheight=""0"" scrolling=""No"" frameborder=""0"" hspace=""0"" vspace=""0"" id=""templante"" style=""margin:0px;""></iframe>"


            ' MANDA DIRETO PARA A ERP =====================================================================
            response.write "<iframe src=""https://integracao.foodcrowd.com.br/moip/fun_ErpFlex.asp?pedido="& NumeroDocumento &"&chave="& TRIM(Recupera_Campos_Db_oConn("TB_PEDIDOS", NumeroDocumento, "id", "cdcliente")) &""" name=""ERP"" width=""1"" marginwidth=""0"" height=""1"" marginheight=""0"" scrolling=""No"" frameborder=""0"" hspace=""0"" vspace=""0"" id=""templante"" style=""margin:0px;""></iframe>"	
        end if

        ' =================================================================================
        if(statusMoip = "emanalise" OR statusMoip = "iniciado") then 
		    ' ATUALIZA PEDIDOS ----------------------------------------------
		    sSQl = "UPDATE TB_PEDIDOS SET ID_STATUS = 55 WHERE ID = " & NumeroDocumento
		    oConn.execute(sSQl)
		    ' ---------------------------------------------------------------

		    ' INSERI LINHA NA TABELA HISTORICO ------------------------------
		    sSQlI = "INSERT INTO TB_HISTORICOS (Id_Pedido, Id_Status, Descricao, Documento) VALUES ('"& NumeroDocumento &"', 55, '"& TRIM(Recupera_Campos_Db_oConn("TB_STATUS", "55", "id", "DESCRICAO")) &" [Moip - Em analise]', '"	& ArrayCampos(7) & "') "
		    oConn.execute(sSQlI)
		    ' ----------------------------------------------------------------

            'ABRE IFRAME PARA TEMPLANTES DE EMAIL -----------------------------
            response.write "<iframe src=""https://integracao.foodcrowd.com.br/TemplateLojas/template.asp?id="& NumeroDocumento &"&status=55"" name=""templante"" width=""1"" marginwidth=""0"" height=""1"" marginheight=""0"" scrolling=""No"" frameborder=""0"" hspace=""0"" vspace=""0"" id=""templante"" style=""margin:0px;""></iframe>"
        end if

        ' =================================================================================
        if(statusMoip = "cancelado") then 
		    ' ATUALIZA PEDIDOS ----------------------------------------------
		    sSQl = "UPDATE TB_PEDIDOS SET ID_STATUS = 3 WHERE ID = " & NumeroDocumento
		    oConn.execute(sSQl)
		    ' ---------------------------------------------------------------

		    ' INSERI LINHA NA TABELA HISTORICO ------------------------------
		    sSQlI = "INSERT INTO TB_HISTORICOS (Id_Pedido, Id_Status, Descricao, Documento) VALUES ('"& NumeroDocumento &"', 3, '"& TRIM(Recupera_Campos_Db_oConn("TB_STATUS", "3", "id", "DESCRICAO")) &" [Moip - Erro]', '"	& ArrayCampos(7) & "') "
		    oConn.execute(sSQlI)
		    ' ----------------------------------------------------------------

            'ABRE IFRAME PARA TEMPLANTES DE EMAIL -----------------------------
            response.write "<iframe src=""https://integracao.foodcrowd.com.br/TemplateLojas/template.asp?id="& NumeroDocumento &"&status=3"" name=""templante"" width=""1"" marginwidth=""0"" height=""1"" marginheight=""0"" scrolling=""No"" frameborder=""0"" hspace=""0"" vspace=""0"" id=""templante"" style=""margin:0px;""></iframe>"
        end if

    end if
end if
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>Retorno Operadora</title>
	</head>
	<body ></body>
</html>
