<!--#include virtual= "/hidden/funcoes.asp"-->

<%
' SO PARA RODAR OFF-LINE
Response.buffer = true

Dim i, adiciona, j, BoolPopUpDownload, BoolPopUpPresente, sMemsagemCredito, boolPreVenda
Dim iProduto, iQuantidade, sQteEstoque, boolTemDownloadEfisico, boolTemPresentefisico, BoolDataPromocao, boolPromocao
Dim iSubtotal, iDeleteItem, boolMostraDescontoCat, boolMostraDescontoSub, boolMostraDescontoPro, sSugestaoFab, sSugestaoProd
Dim oRs, sSQL, iQtd, sImagem, sSimbolo, sFrete, sMontaTotal, sValorDescontadoImposto, sCepDigitado, sValorEmbrulho, sIdProdutoCesta, sTotalDescontoCesta
dim OpcTamanho, OpcCor, OpcSabor, OpcFragrancia, OpcTipo, OpcPac, sProvinciaCad, sValorFrete, sValorRefrigerado, sPrecoTabela, sPrecoTabelaTotal, sPrecoTabelaTotalSemDesconto, OpcGe, boolValidado, sPrazoGarantia, sTipoDownload, sPrecoUniTotal
dim OpcNomePresente, OpcEmail, OpcMensagem, sMemsagemAjuda, sValePresenteSaldo, sPrecoUniCheioD, sPrecoTabelaReal, sNomeProdutoSEO
Dim sPrecoUnitario, sPrecoUnitarioDesconto, IdExt
Dim sValorCumpomDescricao, LinhasCarrinhoMobile, LinhasCarrinhoMobileTotais
dim sBannerPagina, boolMobile

dim sClassHeader: sClassHeader = "bg-1"
    
iProduto 		= Replace_Caracteres_Especiais(Request("ID"))
IdExt 			= Replace_Caracteres_Especiais(request("IdExt"))
iDeleteItem		= Replace_Caracteres_Especiais(Request("DeleteItem"))
iQuantidade		= Replace_Caracteres_Especiais(Request("quantidade"))
OpcTamanho 		= Replace_Caracteres_Especiais(trim(request("tamanhos")))
OpcCor 			= Replace_Caracteres_Especiais(trim(request("cores")))
OpcSabor 		= Replace_Caracteres_Especiais(trim(request("sabores")))
OpcFragrancia	= Replace_Caracteres_Especiais(trim(request("fragrancias")))
OpcTipo 		= Replace_Caracteres_Especiais(trim(request("tipos")))
OpcPac 			= Replace_Caracteres_Especiais(trim(request("pacotes")))
OpcGe 			= Replace_Caracteres_Especiais(request("ge"))
sCepDigitado	= Replace_Caracteres_Especiais(trim(request("cepd")))
OpcNomePresente = Replace_Caracteres_Especiais(trim(request("nome")))
OpcEmail        = Replace_Caracteres_Especiais(trim(request("email")))
OpcMensagem     = Replace_Caracteres_Especiais(trim(request("mensagem")))
boolMobile      = (Replace_Caracteres_Especiais(trim(request("mobile"))) = "1")

session("cep_diferente") = ""
session("boolPreVenda") = ""
Session("ValorPresente")= 0
boolMostraDescontoCat 	= false
boolMostraDescontoSub 	= false
boolMostraDescontoPro 	= false
boolTemDownloadEfisico 	= false
boolTemPresentefisico   = false
sProvinciaCad 		  	= ""
sMemsagemCredito		= ""
sMemsagemAjuda			= ""
sTotalDescontoCesta		= 0
sValePresenteSaldo		= 0
sPrecoTabelaReal		= 0
iSubtotalCupom          = 0

if(OpcGe <> "") then 
	Session("GE_" & iProduto) = OpcGe
end if

if(sCepDigitado = "") then
	if(Session("Id") <> "") then 
		sProvinciaCad = Recupera_Campos_Db("TB_CLIENTES", Session("Id"), "id", "Cep")
	else
		if(Session("sCepAgendamento") <> "" and lcase(Session("sCepAgendamento")) <> "undefined") then
			sProvinciaCad = Session("sCepAgendamento")
		end if
	end if
else
	sProvinciaCad = sCepDigitado
	session("cep_diferente")    = sCepDigitado
end if

' PEGA A QUANTIDADE MINIMA DO PRODUTO
if(iProduto <> "" and (iQuantidade = "" or iQuantidade = "0") ) then 
	if((Recupera_Campos_Db("TB_PRODUTOS", iProduto, "id", sCampoQtedMinima) <> "") and (Recupera_Campos_Db("TB_PRODUTOS", iProduto, "id", sCampoQtedMinima) <> "0")) then 
		iQuantidade = Recupera_Campos_Db("TB_PRODUTOS", iProduto, "id", sCampoQtedMinima)
	end if
end if


' ==================================================================================
If (Replace_Caracteres_Especiais(Request("recalcular.x")) <> "" or Replace_Caracteres_Especiais(Request("recalcular")) <> "")  Then Recalcular 0

If (Replace_Caracteres_Especiais(Request("cancelar.x")) <> "" or Replace_Caracteres_Especiais(Request("cancelar")) <> "")  Then
	
	sLinkCancelar = "/carrinho.asp"
	
	Session("TotalCompra") 		= 0
	Session("sMontaTotal") 		= 0
	Session("SubTotalCompra") 	= 0
	Session("ItemCount") 		= 0
	iCount 						= 0
	Session("ItemCount") 		= iCount

	ReDim ARYShoppingCart(ShoppingCartAttributes, MaxShoppingCartItems)
	Session("MyShoppingCart") = ARYShoppingCart
	Response.Clear
	Response.Redirect sLinkhttp & sLinkCancelar
End If

'Se tem itens em iProduto, faz todos os procedimentos
If Not( IsNull( iProduto ) Or IsEmpty( iProduto )  OR iProduto = "" OR not isNumeric(iProduto)) Then 
	call Abre_Conexao()
	
	Set oRs = Server.CreateObject("ADODB.RecordSet")
	
	sSQL = "SELECT top 1 * FROM TB_PRODUTOS (nolock) WHERE ID in ("& iProduto &") and Qte_Estoque > 0 and (ativo = 1 or ValePresente = 1) "
	oRs.Open sSQL, oConn

	i 		= 0
	iQtd 	= 0
	j 		= 0

	'FOR j = 1 TO iCount
		'sTipoDownload = TRIM(UCASE(ARYShoppingcart(C_Tipo, j)))
	'next
	
	Do While Not oRs.EOF
		iQtd 		= iQuantidade
		adiciona 	= True
		
		' VERIFICA SE O PRODUTO ESTA ENCOTNRA NA CESTA === FALSE
		if(true) then
			For j = 1 to iCount
				If (trim(ARYshoppingcart(C_ID, j)) = trim(oRs("ID"))) Then
					adiciona = False
					
					If isNumeric(iQtd) Then 
						ARYshoppingcart(C_Qtd, j) = CStr(CInt(iQtd))
					end if
				else
					If (TRIM(UCASE(ARYShoppingcart(C_Tipo, j))) = "VALE PRESENTE") Then
                        BoolPopUpPresente 	    = true
                        boolTemPresentefisico   = true
				        adiciona 			    = false
                    else
                        If (sTipoDownload = "DOWNLOAD") Then
						    If (trim(oRs("Id_Tipos")) = "88") Then
							    boolTemDownloadEfisico = false
						    else
							    boolTemDownloadEfisico = true
						    end if
					    else
						    If (trim(oRs("Id_Tipos")) = "88") Then
							    boolTemDownloadEfisico = true
						    else
                                boolTemDownloadEfisico = false
						    end if
					    end if

					    If (sTipoDownload = "VALE PRESENTE") Then
						    If (trim(oRs("Id_Tipos")) = "93") Then
							    boolTemPresentefisico = false
						    else
							    boolTemPresentefisico = true
						    end if
					    else
						    If (trim(oRs("Id_Tipos")) = "93") Then
							    boolTemPresentefisico = true
						    else
                                boolTemPresentefisico = false
						    end if
					    end if
                    end if

				End If
			Next

			If (boolTemDownloadEfisico) Then
				BoolPopUpDownload 	= true
				adiciona 			= false
			else
				BoolPopUpDownload 	= false
			End If
            If (boolTemPresentefisico) Then
				BoolPopUpPresente 	= true
				adiciona 			= false
            else
                BoolPopUpPresente 	= false 
            end if
		end if
		
		If adiciona Then
			If isNumeric(iQtd) then
				If iQtd > 0 then
					If iCount <= MaxShoppingCartItems Then
						iCount = iCount + 1
					Else
						Response.Write("<br><font face=""verdana"" size=""3"" color=""red""><b>Atencao!<br><br><font face=""verdana"" size=""-1"">Foi excedido o numero maximo de items por pedido.</b><br>")
					End If
					Session("ItemCount") = iCount
					
					If Not oRs.Eof Then
						ARYShoppingcart(C_ID,iCount) 		 	  = CLng(oRs("ID"))
						ARYShoppingcart(C_Titulo,iCount) 	 	  = oRs(sCampoDescricao)
						ARYShoppingcart(C_Qtd,iCount) 		 	  = CStr(CInt(iQtd))
						ARYShoppingcart(C_Imagem,iCount) 	 	  = trim(oRs("imagem"))
						ARYShoppingcart(C_Estoque,iCount) 	 	  = trim(oRs("qte_estoque"))
						ARYShoppingcart(C_Moeda,iCount) 	 	  = trim(oRs("Id_Moeda"))
						ARYShoppingcart(C_Ref,iCount) 		 	  = trim(oRs("CodReferencia"))
						
						ARYShoppingcart(C_Congelado,iCount) 	  = trim(oRs("Congelado"))
						ARYShoppingcart(C_Peso,iCount) 		 	  = trim(oRs("Peso"))
						ARYShoppingcart(C_Manuseio,iCount) 		  = trim(oRs("Preco_Manuseio"))
						ARYShoppingcart(C_Embrulho,iCount) 		  = trim(oRs("Preco_Embrulho"))
						ARYShoppingcart(C_QteParcelamento,iCount) = trim(oRs("Qte_Parcelamento"))
						ARYShoppingcart(C_PrecoPrazo,iCount) 	  = trim(oRs("Preco_APrazo"))
						ARYShoppingcart(C_Preco,iCount) 	 	  = CDbl(oRs(sCampoPreco))
						ARYshoppingcart(C_PrecoCesta,iCount)	  = CDbl(oRs(sCampoPreco))
    						
                        ARYShoppingcart(C_NomeP,iCount) 	 	  = OpcNomePresente
                        ARYShoppingcart(C_EmailP,iCount) 	 	  = OpcEmail
                        ARYShoppingcart(C_MensagemP,iCount) 	  = OpcMensagem

						ARYShoppingcart(C_Tamanho,iCount) 	 	  = Recupera_Variacoes("TB_TAMANHOS", OpcTamanho, "id", "Descricao")
						ARYShoppingcart(C_Cor,iCount) 		 	  = Recupera_Variacoes("TB_CORES", OpcCor, "id", "Descricao")
						ARYShoppingcart(C_Sabor,iCount) 	 	  = Recupera_Variacoes("TB_SABORES", OpcSabor, "id", "Descricao")
						ARYShoppingcart(C_Fragrancia,iCount) 	  = Recupera_Variacoes("TB_FRAGRANCIAS", OpcFragrancia, "id", "Descricao")
						ARYShoppingcart(C_Tipo,iCount) 		 	  = Recupera_Variacoes("TB_TIPOS", OpcTipo, "id", "Descricao")
						ARYShoppingcart(C_Pacote,iCount) 		  = Recupera_Variacoes("TB_PACOTES", OpcPac, "id", "Descricao")
						ARYShoppingcart(C_GE,iCount) 	  		  = "Garantia Estendia - 12 meses - Mapfre"
					
						ARYShoppingcart(C_TamanhoAcres,iCount) 	  = CDbl(Recupera_Variacoes_Acrescimo("Id_Tamanho", OpcTamanho))
						ARYShoppingcart(C_CorAcres,iCount) 		  = CDbl(Recupera_Variacoes_Acrescimo("Id_Cor", OpcCor))
						ARYShoppingcart(C_SaborAcres,iCount) 	  = CDbl(Recupera_Variacoes_Acrescimo("Id_Sabor", OpcSabor))
						ARYShoppingcart(C_TipoAcres,iCount) 	  = CDbl(Recupera_Variacoes_Acrescimo("Id_Tipo", OpcTipo))
						ARYShoppingcart(C_PacoteAcres,iCount) 	  = CDbl(Recupera_Variacoes_Acrescimo("Id_Pacote", OpcPac))
						ARYShoppingcart(C_FragranciaAcres,iCount) = CDbl(Recupera_Variacoes_Acrescimo("Id_Fragrancia", OpcFragrancia))
						ARYShoppingcart(C_GEAcres,iCount) 		  = CDbl(Session("GE_" & ARYShoppingcart(C_ID,iCount)))
    						
                        if(IdExt <> "" and IsNumeric(IdExt)) then 
						    ARYShoppingcart(C_Preco,iCount) 	 	  = CDbl(trim(Recupera_Campos_Db_oConn("TB_IMAGENS_AUXILIARES", IdExt, "id", "Acrescimo_Consumidor")))
						    ARYShoppingcart(C_PrecoPrazo,iCount) 	  = CDbl(ARYShoppingcart(C_Preco,iCount))
						    ARYshoppingcart(C_PrecoCesta,iCount)	  = CDbl(ARYShoppingcart(C_Preco,iCount))
                        end if

						if(OpcTamanho <> "") then 		ARYShoppingcart(C_Tamanho,iCount) 	 = (ARYShoppingcart(C_Tamanho,iCount) 	)
						if(OpcCor <> "") then 			ARYShoppingcart(C_Cor,iCount) 		 = (ARYShoppingcart(C_Cor,iCount) 		)
						if(OpcSabor <> "") then 		ARYShoppingcart(C_Sabor,iCount) 	 = (ARYShoppingcart(C_Sabor,iCount) 	)
						if(OpcFragrancia <> "") then 	ARYShoppingcart(C_Fragrancia,iCount) = (ARYShoppingcart(C_Fragrancia,iCount))
						if(OpcTipo <> "") then 			ARYShoppingcart(C_Tipo,iCount) 		 = (ARYShoppingcart(C_Tipo,iCount) 		)
						if(OpcPac <> "") then 			ARYShoppingcart(C_Pacote,iCount) 	 = (ARYShoppingcart(C_Pacote,iCount) 	)
						if(OpcGe <> "") then 			ARYShoppingcart(C_GE,iCount) 	 	 = (ARYShoppingcart(C_GE,iCount)		)

						if(ARYShoppingcart(C_SaborAcres,iCount) > 0) then 		ARYShoppingcart(C_Preco,iCount) = ARYShoppingcart(C_SaborAcres,iCount)
						if(ARYShoppingcart(C_TamanhoAcres,iCount) > 0) then 	ARYShoppingcart(C_Preco,iCount) = (ARYShoppingcart(C_Preco,iCount) + ARYShoppingcart(C_TamanhoAcres,iCount))
						if(ARYShoppingcart(C_CorAcres,iCount) > 0) then 		ARYShoppingcart(C_Preco,iCount) = (ARYShoppingcart(C_Preco,iCount) + ARYShoppingcart(C_CorAcres,iCount))
						if(ARYShoppingcart(C_TipoAcres,iCount) > 0) then 		ARYShoppingcart(C_Preco,iCount) = (ARYShoppingcart(C_Preco,iCount) + ARYShoppingcart(C_TipoAcres,iCount))
						if(ARYShoppingcart(C_PacoteAcres,iCount) > 0) then 		ARYShoppingcart(C_Preco,iCount) = (ARYShoppingcart(C_Preco,iCount) + ARYShoppingcart(C_PacoteAcres,iCount))
						if(ARYShoppingcart(C_FragranciaAcres,iCount) > 0) then 	ARYShoppingcart(C_Preco,iCount) = (ARYShoppingcart(C_Preco,iCount) + ARYShoppingcart(C_FragranciaAcres,iCount))
						if(ARYShoppingcart(C_GEAcres,iCount) > 0) then 			ARYShoppingcart(C_Preco,iCount) = (ARYShoppingcart(C_Preco,iCount) + ARYShoppingcart(C_GEAcres,iCount))
						
						If (ARYshoppingcart(C_Titulo,iCount)) <> "" Then
							
                            sPrecoUnitarioDesconto          = CDbl(ARYshoppingcart(C_Preco,iCount))

                            if(trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYShoppingcart(C_ID,iCount), "id", "DataIniPromo")) <> "" and not isnull(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYShoppingcart(C_ID,iCount), "id", "DataIniPromo"))) then 
				                BoolDataPromocao = ((cdate(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYShoppingcart(C_ID,iCount), "id", "DataIniPromo")) <= Date) and (cDate(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYShoppingcart(C_ID,iCount), "id", "DataFimPromo")) >= Date))
			                end if
                            
                            boolPromocao 	= ((Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYShoppingcart(C_ID,iCount), "id", "promocao") = "1") and BoolDataPromocao)
                            
                            if(boolPromocao) then 
		                        if(Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID,iCount), "id", "DescontoValor") <> "" and Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID,iCount), "id", "DescontoValor") <> "0") then 
			                        Session("d" & ARYShoppingcart(C_ID,iCount)) = cdbl(Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID,iCount), "id", "DescontoValor"))
		                        end if
	                        end If

							if(Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID,iCount), "id", "Id_Categoria")) > 0) then 
								sPrecoUnitarioDesconto  = (cdbl(sPrecoUnitarioDesconto) - ((cdbl(Session("Cat" & Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID,iCount), "id", "Id_Categoria"))) / 100) * cdbl(sPrecoUnitarioDesconto)))
								boolMostraDescontoCat   = true
							end if
							if(Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID,iCount), "id", "Id_SubCategoria")) > 0) then 
								sPrecoUnitarioDesconto = (cdbl(sPrecoUnitarioDesconto) - ((cdbl(Session("Sub" & Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID,iCount), "id", "Id_SubCategoria"))) / 100) * cdbl(sPrecoUnitarioDesconto)))
								boolMostraDescontoSub = true
							end if

							if((Session("d" & ARYShoppingcart(C_ID,iCount)) > 0)) then 
								if(ucase(trim(oRs("DescontoTipo"))) = "R") then 
									sPrecoUnitarioDesconto      = (CDbl(sPrecoUnitarioDesconto) - ((cdbl(Session("d" & ARYShoppingcart(C_ID,iCount)))) ))
									IF(sPrecoUnitarioDesconto < 0) THEN sPrecoUnitarioDesconto 	= 0
								else
									sPrecoUnitarioDesconto      = (cdbl(sPrecoUnitarioDesconto) - ((cdbl(Session("d" & ARYShoppingcart(C_ID,iCount))) / 100) * cdbl(sPrecoUnitarioDesconto)))
									IF(sPrecoUnitarioDesconto < 0) THEN sPrecoUnitarioDesconto 	= 0
								end if

								boolMostraDescontoPro = true
							end if
							
                            iSubTotal       = (iSubtotal + (sPrecoUnitarioDesconto * CInt(ARYshoppingcart(C_Qtd,iCount))))

							' SUBTRAI ITENS NO ESTOQUE =====================================
'							call Subtrai_Estoque(ARYshoppingCart(C_ID, iCount), ARYshoppingCart(C_Qtd, iCount))
						End If
					End If
				End If
			End If
		End If
		
		i = i + 1
		oRs.MoveNext
	Loop
	
	' DESCONTO ESTA SENDO APLICADO NO SUB TOTAL DA SOMA DOS PRODUTOS
	if(Session("Desconto") <> "") then 
		if(Session("CupomGet") = "") then 
			iSubtotal = (cdbl(iSubtotal) - ((cdbl(Session("Desconto")) / 100) * cdbl(iSubtotal)))
		else
			if(ucase(Recupera_Campos_Db("TB_CUPONS", Session("CupomGet"), "Cupom", "TipoDescontoValor")) = "R") then 
				iSubtotal = (cdbl(iSubtotal)  - cdbl(Session("Desconto")) )
			else
				iSubtotal = (cdbl(iSubtotal) - ((cdbl(Session("Desconto")) / 100) * cdbl(iSubtotal)))
			end if
		end if
	end if
	
	Session("SubTotalCompra") 		= iSubtotal
	Session("TotalCompraSemFrete") 	= iSubtotal
	
	' MONTA O TOTAL - VEIFICANDO O DESCONTO =======================================
	sMontaTotal = cdbl(iSubtotal)

	sValorDescontadoImposto = ((cdbl(ValorImposto) / 100) * cdbl(sMontaTotal))
	sValorDescontadoImposto = replace(replace(RetornaCasasDecimais(sValorDescontadoImposto,2),",",""),".","")
	Session("ValorDescontadoImposto") = sValorDescontadoImposto

	
	sValorFrete = Calcula_Frete("1", sProvinciaCad, sMontaTotal)
	if (session("sCheckedAgendamento")) then sValorFrete    = sValorFrete + Session("sAcrescimoAgendamento")
	
	sValorRefrigerado 			= Calcula_Refrigerado(sTotalKiloCongelado)
	Session("ValorRefrigerado") = sValorRefrigerado
	sMontaTotal 	  			= (cdbl(sMontaTotal) + cdbl(sValorFrete) + cdbl(sValorRefrigerado))
	sMontaTotal 	  			= RetornaCasasDecimais(sMontaTotal,2)
	Session("sMontaTotal") 		= sMontaTotal
	
	Session("MyShoppingCart") = ARYShoppingcart
	oRs.Close
	Set oRs = Nothing
	call fecha_Conexao()
End If

If Not( IsNull( iDeleteItem ) Or IsEmpty( iDeleteItem ) OR iDeleteItem = "") Then 'Se tem itens em iProduto, faz todos os
	if(iDeleteItem <> "") then Recalcular CInt(iDeleteItem)
End If


If Replace_Caracteres_Especiais(Request("continuar.x")) <> "" Then
	Response.Clear
	response.redirect sLinkhttp
End If

If (Replace_Caracteres_Especiais(Request("finalizar")) <> "") Then Recalcular 0


' =========================================================================
Sub Recalcular(iDeletar)
	Dim Quantity, iPos, iQuantity, iCountInic, n, x

    'response.Write "iCount---" & iCount & "<br>"

	For i = 1 to iCount

		'O Form q exibe os produtos tem nomes Quantity <numero>
		If i = iDeletar Then
			Quantity = 0
		Else

            if(boolMobile) then 
			    QuantityM   = Replace_Caracteres_Especiais(Request("QuantityM" & CStr(i)))
            else
			    Quantity    = Replace_Caracteres_Especiais(Request("Quantity" & CStr(i)))
            end if

            if(QuantityM <> "") then Quantity = QuantityM & "," 

            'response.Write "boolMobile---" & boolMobile & "<br>"
            'response.Write "Quantity---" & Replace_Caracteres_Especiais(Request("Quantity" & CStr(i))) & "<br>"
            'response.Write "QuantityM---" & Replace_Caracteres_Especiais(Request("QuantityM" & CStr(i))) & "<br>"
            'response.Write "Quantity---" & Quantity & "<br>"

			iPos 		= Instr(Quantity, ",")

            if (iPos > 0) then
                iQuantity = Mid(Quantity, 1, iPos-1)  ' Retira  a virgula da string Quantity
            else
                iQuantity = 1
            end if
		End If

        'response.Write "Quantity---" & Quantity & "<br>"
        'response.Write "iQuantity---" & iQuantity & "<br>"


		If IsNumeric(iQuantity) Then
			ARYshoppingcart(C_Qtd, i) = abs(CLng(iQuantity))
		Else
			ARYshoppingcart(C_Qtd, i) = 1
		End If
	Next

    'response.Write "ARYshoppingcart(C_Qtd, i)---" & ARYshoppingcart(C_Qtd, 1) & "<br>"

	iCountInic = iCount
	
	For i = 1 to iCount
		Session("embrulho_" & ARYshoppingcart(C_ID, i)) = Replace_Caracteres_Especiais(request("embrulho_" & ARYshoppingcart(C_ID, i)))

		if i = iDeletar then
			iQuantity = 0
		else

            if(boolMobile) then 
			    QuantityM   = Replace_Caracteres_Especiais(Request("QuantityM" & CStr(i)))
            else
			    Quantity    = Replace_Caracteres_Especiais(Request("Quantity" & CStr(i)))
            end if

            if(QuantityM <> "") then Quantity = QuantityM & "," 

			iPos=Instr(Quantity,",")
            if (iPos > 0) then
                iQuantity = Mid(Quantity,1,iPos-1)'Retira  a virgula da string Quantity
            else
                iQuantity = 1
            end if
		End If

		If (CInt(iQuantity) = 0) Then
			For n = i+iCount-iCountInic to iCount-1
				For x = 1 to UBound(ARYshoppingcart,1)
					ARYshoppingcart(x,n) = ARYshoppingcart(x,n + 1)
				Next
			Next
			iCount = iCount - 1
		End If
	Next

	Session("MyShoppingCart") = ARYShoppingcart
	Session("ItemCount") = iCount
End Sub

' ===========================================================================
if Replace_Caracteres_Especiais(Request("DeleteItem")) <> "" then
   Response.Clear
   Response.Redirect sLinkhttp & "carrinho.asp"
end if
' ===========================================================================
function Recupera_Variacoes(Tabela, Opcao, Chave, Retorno)
	Recupera_Variacoes = ""
	if(Opcao <> "") then Recupera_Variacoes = Recupera_Campos_Db_oConn(Tabela, Opcao, Chave, Retorno)
end function

' ===========================================================================
function Recupera_Variacoes_Acrescimo(sCampoChave, sValor)
	dim sSQl, Rs

    if(not IsNumeric(sValor)) then exit function

	Recupera_Variacoes_Acrescimo = "0"
	
	if(sValor <> "") then 
		call Abre_Conexao()
	
    	sSQl = "Select "& sCampoAcrescimoLogin &" from TB_IMAGENS_AUXILIARES (NOLOCK) where id_produto = "& iProduto &" and "& sCampoChave &" = "& sValor
    	Set Rs = oConn.execute(sSQl)

		if(not Rs.eof) then 
			Recupera_Variacoes_Acrescimo = rs(sCampoAcrescimoLogin)
			if(Recupera_Variacoes_Acrescimo = "" or isnull(Recupera_Variacoes_Acrescimo)) then Recupera_Variacoes_Acrescimo = 0
		end if
		Set Rs = nothing
		call Fecha_Conexao()
	end if
end function


' ===========================================================================
function MontaDePara(Id_Produto, prQuantidadeCarrinho)
	dim sSQl, Rs, 	sPrecoProduto,	sDescontoCupom,	sPrecoDesconto, parcelamento, preco_aprazoCarrinho, Qte_ParcelamentoCarrinho
	
	call abre_conexao()

	sSql	=	" select TOP 1 preco_consumidor, preco_aprazo, descontovalor, Qte_Parcelamento, promocao, DataIniPromo, DataFimPromo  " & _
                "   from tb_produtos (NOLOCK)  " & _
                "   where cdcliente = "& sCdcliente &" AND id = " & Id_Produto

	Set Rs = oConn.execute(sSQl)

	if(not Rs.eof) then 
		sPrecoProduto		     = RetornaCasasDecimais(cdbl(Rs("preco_consumidor")) * cdbl(prQuantidadeCarrinho),2)
		sPrecoDesconto		     = RetornaCasasDecimais(cdbl(Rs("descontovalor")) * cdbl(prQuantidadeCarrinho),2)
        preco_aprazoCarrinho     = RetornaCasasDecimais(cdbl(Rs("preco_aprazo")) * cdbl(prQuantidadeCarrinho),2)
        Qte_ParcelamentoCarrinho = Rs("Qte_Parcelamento")

        if(preco_aprazoCarrinho = "" or isnull(preco_aprazoCarrinho)) then preco_aprazoCarrinho = sPrecoProduto
	end if
	Set Rs = nothing
	
	MontaDePara	= ""

	if (sPrecoDesconto > 0) then
        if (Session("dc" & Id_Produto) > 0) then

            sPrecoFinalMostraCarrinho       = RetornaCasasDecimais((sPrecoProduto - (cdbl(sPrecoDesconto) + cdbl(Session("dc" & Id_Produto)))),2)
            sPrecoFinalMostraCarrinhoCupom  = sPrecoFinalMostraCarrinho

		    MontaDePara	=	MontaDePara	&	"<div style=""text-align:center; font-size: 15px;""><span style=""text-align: center; color: #999;""><strike>R$ "   &	RetornaCasasDecimais(sPrecoProduto,2) &	"</strike></span><br>"
		    MontaDePara	=	MontaDePara	&	"<span style=""text-align: center;"" class=""AlinharMobilePreco""><strong>R$ "    &	sPrecoFinalMostraCarrinhoCupom	&	"<strong></span><br class=""desktop"">"
		    MontaDePara	=	MontaDePara	&	"<span style=""text-align: center; color: #999;"">(- R$ "    &	RetornaCasasDecimais(cdbl(sPrecoDesconto) + cdbl(Session("dc" & Id_Produto)),2) &	")</span></div>"
		    
            if(SeSsion("cupomdesconto")        = "proxymedia5") then 
                if(sPrecoFinalMostraCarrinho > 0) then
                    sTotalDescontoCesta = RetornaCasasDecimais(cdbl(sTotalDescontoCesta) + cdbl(sPrecoDesconto) + cdbl(Session("dc" & Id_Produto)) + (cdbl(sPrecoFinalMostraCarrinho) * cdbl(SeSsion("cupomdescontovalor"))),2)
                end if    
            else
                sTotalDescontoCesta = RetornaCasasDecimais(cdbl(sTotalDescontoCesta) + cdbl(sPrecoDesconto) + cdbl(Session("dc" & Id_Produto)),2)
            end if

        else
            sPrecoFinalMostraCarrinho = RetornaCasasDecimais((sPrecoProduto - sPrecoDesconto),2)

            'if(SeSsion("cupomdesconto")        = "proxymedia5") then 
            '    if(sPrecoFinalMostraCarrinho > 0) then
            '        sPrecoFinalMostraCarrinhoCupom = formatNumber((cdbl(sPrecoFinalMostraCarrinho) - cdbl(SeSsion("cupomdescontovalor")) ),2)        
            '    end if    
            'else
                sPrecoFinalMostraCarrinhoCupom = sPrecoFinalMostraCarrinho
            'end if

		    MontaDePara	=	MontaDePara	&	"<div style=""font-size: 15px;""><span style=""color: #999;""><strike>R$ "   &	RetornaCasasDecimais(sPrecoProduto,2) &	"</strike></span><br>"
		    MontaDePara	=	MontaDePara	&	"<span class=""AlinharMobilePreco""><strong>R$ "  &	sPrecoFinalMostraCarrinhoCupom	&	"<strong></span><br class=""desktop"">"
		    MontaDePara	=	MontaDePara	&	"<span style=""color: #999;"" class=""desktop"">(- R$ "  &	RetornaCasasDecimais(sPrecoDesconto,2)				&	")</span></div>"

            if(SeSsion("cupomdesconto")        = "proxymedia5") then 
                if(sPrecoFinalMostraCarrinho > 0) then
                    sTotalDescontoCesta = RetornaCasasDecimais(cdbl(sTotalDescontoCesta) + cdbl(sPrecoDesconto) + (cdbl(sPrecoFinalMostraCarrinho) * cdbl(SeSsion("cupomdescontovalor"))),2)
                end if    
            else
                sTotalDescontoCesta = RetornaCasasDecimais(cdbl(sTotalDescontoCesta) + cdbl(sPrecoDesconto),2)
            end if

        end if
	else
        if (Session("dc" & Id_Produto) > 0) then
            sPrecoFinalMostraCarrinho = RetornaCasasDecimais(cdbl(sPrecoUnitarioDesconto),2)

            'if(SeSsion("cupomdesconto")        = "proxymedia5") then 
            '    if(sPrecoFinalMostraCarrinho > 0) then
            '        sPrecoFinalMostraCarrinhoCupom = formatNumber((cdbl(sPrecoFinalMostraCarrinho) - cdbl(SeSsion("cupomdescontovalor")) ),2)        
            '    end if    
            'else
                sPrecoFinalMostraCarrinhoCupom = sPrecoFinalMostraCarrinho
            'end if

		    MontaDePara	=	MontaDePara	&	"<div style=""text-align:center; margin-top:1px;font-size: 15px;""><span style=""text-align: center; color: #999;""><strike>De: R$ "   &	RetornaCasasDecimais(cdbl(sPrecoUnitario),2) &	"</strike></span><br>"
		    MontaDePara	=	MontaDePara	&	"<span style=""text-align: center;""><strong>Por: R$ <strong>"  & sPrecoFinalMostraCarrinhoCupom    &	"<strong></span><br class=""desktop"">"
		    MontaDePara	=	MontaDePara	&	"<span style=""text-align: center; color: #999;"" class=""desktop"">(- R$ "       & RetornaCasasDecimais(cdbl(Session("dc" & Id_Produto)),2)	&	")</span></div>"

            if(SeSsion("cupomdesconto")        = "proxymedia5") then 
                if(sPrecoFinalMostraCarrinho > 0) then
                    sTotalDescontoCesta = RetornaCasasDecimais(cdbl(sTotalDescontoCesta) + cdbl(Session("dc" & Id_Produto)) + (cdbl(sPrecoFinalMostraCarrinho) * cdbl(SeSsion("cupomdescontovalor"))),2)
                end if    
            else
                sTotalDescontoCesta = RetornaCasasDecimais(cdbl(sTotalDescontoCesta) + cdbl(Session("dc" & Id_Produto)) ,2)
            end if

        else
            if(nDescontoBoleto <> "") then 
                sPrecoUnitarioDesconto = (cdbl(sPrecoUnitarioDesconto) * cdbl(prQuantidadeCarrinho))
                sPrecofinalBoleto = formatNumber((cdbl(sPrecoUnitarioDesconto) - (cdbl(nDescontoBoleto) * cdbl(sPrecoUnitarioDesconto)) ),2)
            end if

		    MontaDePara	=	MontaDePara	&	"<span class=""AlinharMobilePreco""><strong>R$ <strong>"  &	RetornaCasasDecimais(cdbl(preco_aprazoCarrinho), 2)	&	"<strong></span><br>"

		    'MontaDePara	= "R$ " & RetornaCasasDecimais(cdbl(preco_aprazoCarrinho), 2) & "<br>em até " & Qte_ParcelamentoCarrinho & "x"
            'MontaDePara	=	MontaDePara	& "<br/>ou boleto R$ "& sPrecofinalBoleto 

            'if(cint(nDescontoBoleto) > 0) then 
            '    MontaDePara	=	MontaDePara	&" desconto "& nDescontoBoleto * 100 &"%"
            'end if

        end if
	end if

	call abre_conexao()
end function

%>


<!DOCTYPE html>
<html lang="en" dir="ltr">
    <!--#include virtual= "head.asp"-->

    <script type="text/javascript">
        function ValidarNumero(obj) {
	        var expressao = /^[0-9]/;
	
	        if(!expressao.test(obj.value)) {
                return false;
	        } else {
                return true;
	        }
        }

        function valida() {
            var objForm = document.frm_basket;

	        if (isObject(objForm.cepd)) {
	            if (objForm.cepd.value != '') {
	                if (!ValidarNumero(objForm.cepd)) {
	                    alert('Campo CEP aceita apenas número!');
	                    objForm.cepd.focus();
	                    return false;
	                }
	            }
	        }
        }
        function Recalcular(Quant, prMobile) {
	        var objForm = document.frm_basket;

            objForm.action = 'carrinho.asp?mobile='+ prMobile +'&Recalcular=' + Quant;
	        objForm.submit();
        }

        function VerificarEnter(e) {

            var evento = window.event || e;
            var tecla = evento.keyCode || evento.witch;
            if (tecla == 13) {
                return false;
            }

        }
    </script>

    <link rel="stylesheet" href="/css/styleCarrinho.css">

    <style>
        .checkout-btn2 {
            float: right;
        }
        .jumbotron p {
            font-size: 17px;
            font-weight: 200;
        }

        .AlinharMobilePreco {
            font-size: 16px;
            float: right;
        }

        .desktop {
            /*display: block;*/
            display: none;
        }

        .mobile {
            display: block !important;
        }

    </style>

<body>

    <!--#include virtual= "header.asp"-->

    <div class=" desktop-margin-top-90" >
	    <div class="margin-60-0 ">
		<div class="container">
			<div class="row desktop-margin-90">
				<div class="col-sm-12">

					<div class="row">
						<div class="col-sm-12"><h2 class="cor-4 semi-bold "><span class="shape6">MEU CARRINHO</span></h2></div>

                        <form action="/carrinho.asp" method="post" name="frm_basket" style="padding: 0px; margin: 0px;" onsubmit="javascript: return valida();">
            
                                <div class="order-detail-content desktop">

                                  <div class="">

                                        <table class="table table-bordered cart_summary" >

                                          <% If iCount > 0 Then %>
                                              <thead>
                                                <tr class="desktop">
                                                  <th></th>
                                                  <th>Produto</th>
                                                  <th>Preço Unit.</th>
                                                  <th>Qtd.</th>
                                                  <th>Total</th>
                                                  <th></th>
                                                </tr>
                                                <tr class="mobile">
                                                  <th >Dados do Produto</th>
                                                </tr>
                                              </thead>
                                        <% end if %>

                                        <% If iCount > 0 Then         
	                                                if (BoolPopUpDownload)then
		                                                response.write 	"<script>ShowBox('#popAlert')</script>" & vbcrlf
	                                                end if
	                                                if (BoolPopUpPresente)then
		                                                response.write 	"<script>ShowBox('#popAlert')</script>" & vbcrlf
	                                                end if

	                                                Session("TotalCompra")      = 0
	                                                Session("sMontaTotal")      = 0
	                                                Session("SubTotalCompra")   = 0
	
	                                                iSubtotal 			        = 0
	                                                sValorEmbrulho 		        = 0
	                                                sIdProdutoCesta 	        = ""
                                                    LinhasCarrinhoMobile        = ""
                                                    LinhasCarrinhoMobileTotais  = ""
	                    
	                                                ' LISTA OS PRODUTOS NA CESTA =============================================
	                                                For i = 1 to iCount
                                                        sImagem = Alterar_Tamanho_Imagem("p", "", "", ARYShoppingcart(C_Imagem, i), 94, 63) 
		                                                if(sImagem = "" or isnull(sImagem)) then  sImagem = "sem_imagem.gif"
	
		                                                sDataPreVendaIni 	= trim(Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID,i), "id", "DataIniPreVenda"))
		                                                sDataPreVendaFim 	= trim(Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID,i), "id", "DataFimPreVenda"))
		                                                sPrazoPrevenda		= trim(Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID,i), "id", "Prazo_Garantia"))
		                                                sPrevenda			= trim(Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID,i), "id", "PreVenda"))
		
		                                                if((trim(sDataPreVendaIni)) <> "" and not isnull(sDataPreVendaIni)) then sDataPreVendaIni = cdate(sDataPreVendaIni)
		                                                if((trim(sDataPreVendaFim)) <> "" and not isnull(sDataPreVendaFim)) then sDataPreVendaFim = cdate(sDataPreVendaFim)
		
		                                                boolPreVenda = false
		                                                if(trim(sPrevenda) = "1") then boolPreVenda = (date >= sDataPreVendaIni AND date <= sDataPreVendaFim)
	
		                                                ' SOMA VALOR DO EMBRULHO ============================
		                                                if((trim(ARYShoppingcart(C_Embrulho, i)) > 0) AND (Replace_Caracteres_Especiais(Session("embrulho_" & ARYshoppingcart(C_ID, i))) = "1")) then 
			                                                sValorEmbrulho = (sValorEmbrulho + (ARYShoppingcart(C_Embrulho, i)))
		                                                end if
		
		                                                If (ARYshoppingcart(C_Titulo,i)) <> "" Then
			                                                sPrecoUnitario              = CDbl(ARYshoppingcart(C_Preco,i))
                                                            sPrecoUnitarioDesconto      = CDbl(ARYshoppingcart(C_Preco,i))

			                                                if(Session("Cat" & Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYShoppingcart(C_ID,i), "id", "Id_Categoria")) > 0) then 
				                                                sPrecoUnitarioDesconto  = (cdbl(sPrecoUnitarioDesconto) - ((cdbl(Session("Cat" & Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYShoppingcart(C_ID,i), "id", "Id_Categoria"))) / 100) * cdbl(sPrecoUnitarioDesconto)))
				                                                boolMostraDescontoCat   = true
			                                                end if
			
			                                                if(Session("Sub" & Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYShoppingcart(C_ID,i), "id", "Id_SubCategoria")) > 0) then 
				                                                sPrecoUnitarioDesconto  = (cdbl(sPrecoUnitarioDesconto) - ((cdbl(Session("Sub" & Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYShoppingcart(C_ID,i), "id", "Id_SubCategoria"))) / 100) * cdbl(sPrecoUnitarioDesconto)))
				                                                boolMostraDescontoSub   = true
			                                                end if

						                                    if((Session("d" & ARYShoppingcart(C_ID,i)) > 0 OR Session("dc" & ARYShoppingcart(C_ID,i)) > 0)) then 
                                                                if(ucase(Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID,i), "id", "DescontoTipo")) = "R") then 
								                                    sPrecoUnitarioDesconto      = FormatNumber(CDbl(sPrecoUnitarioDesconto) - ((cdbl(Session("d" & ARYShoppingcart(C_ID,i))) + cdbl(Session("dc" & ARYShoppingcart(C_ID,i))) ) ),2)
								                                    IF(sPrecoUnitarioDesconto < 0) THEN sPrecoUnitarioDesconto 	= 0
							                                    else
								                                    sPrecoUnitarioDesconto      = FormatNumber(cdbl(sPrecoUnitarioDesconto) - ((cdbl(Session("d" & ARYShoppingcart(C_ID,i)) ) / 100) * cdbl(sPrecoUnitarioDesconto)),2)
								                                    IF(sPrecoUnitarioDesconto < 0) THEN sPrecoUnitarioDesconto 	= 0
							                                    end if
							                                    boolMostraDescontoPro = true
						                                    end if

		                                                End If
	
		                                                sPrecoTabela 	= ARYshoppingcart(C_Preco, i)
		                                                sPrazoGarantia	= Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID,i), "id", "Prazo_Garantia")

                                                        if(boolPreVenda) then
                                                            session("boolPreVenda") = sPrazoGarantia
                                                        end if

                                                        sNomeProdutoSEO             = TrataSEO(Recupera_Campos_Db("TB_PRODUTOS", ARYShoppingcart(C_ID,i), "id", "Descricao"))
                                                    %>

                                         <tbody>

                                        <tr class="desktop">
                                          <td >
                                              <a href="/detalhe-produto/<%= sNomeProdutoSEO %>/<%= ARYShoppingcart(C_ID,i) %>">
                                                <% response.write "<img style=""width: 100px;height: auto;"" src="""& sImagem &""" name="""& ARYShoppingcart(C_Titulo, i) &""" alt="""& ARYShoppingcart(C_Titulo, i) &"""  />" %>
                                               </a>
                                          </td>
                                          <td >
                                                <p class="product-name AlinharMobileNome" style="font-weight: 100;"><a  href="/detalhe-produto/<%= sNomeProdutoSEO %>/<%= ARYShoppingcart(C_ID,i) %>"><%= ARYShoppingcart(C_Titulo, i) %></a></p>

                                                <%if(boolPreVenda) then %><br><span class="tituloPreVenda">*PRÉ-VENDA</span><%end if%>
                                                <%if(ARYShoppingcart(C_Tamanho,i) <> "") then %>    <small><a href="#">Tamanho:&nbsp;<%= ARYShoppingcart(C_Tamanho,i) %></a></small><%end if%>
                                                <%if(ARYShoppingcart(C_Cor,i) <> "") then %>        <small><a href="#">Cor:&nbsp;<%= ARYShoppingcart(C_Cor,i) %></a></small><%end if%>
                                                <%if(ARYShoppingcart(C_Sabor,i) <> "") then %>      <small><a href="#">Tamanho (cm):&nbsp;<%= ARYShoppingcart(C_Sabor,i) %></a></small><%end if%>
                                                <%if(ARYShoppingcart(C_Fragrancia,i) <> "") then %> <small><a href="#">Fragr&acirc;ncia:<%= ARYShoppingcart(C_Fragrancia,i) %></a></small><%end if%>
                                                <%if(ARYShoppingcart(C_Tipo, i)  <> "") then %>     <small><a href="#"><%= ARYShoppingcart(C_Tipo, i)  %></a></small><%end if%>
                                                <%if(ARYShoppingcart(C_Pacote,i) <> "") then %>     <small><a href="#">Pacote:&nbsp;<%= ARYShoppingcart(C_Pacote,i) %></a></small><%end if%>
                                          </td>
                                          <td ><span>R$<%= formatNumber(sPrecoUnitario, 2) %></span></td>
                                          <td ><%= Retorna_Estoque_Produto_Combo("Quantity" & CStr(i), ARYshoppingcart(C_Estoque,i), ARYshoppingcart(C_Qtd,i), ARYshoppingcart(C_ID,i), Recupera_Codigo_Abacos(ARYshoppingCart(C_ID, i), ARYshoppingCart(C_Tamanho, i), ARYshoppingCart(C_Cor, i), "", ARYshoppingCart(C_Tipo, i)), "") %></td>
                                          <td ><span>
                                                <% ARYshoppingcart(C_PrecoCesta,i) =  RetornaCasasDecimais(CDbl(sPrecoUnitarioDesconto) * CInt(ARYshoppingcart(C_Qtd,i)),2)%>
                        
                                                <%if (Session("embrulho_" & ARYshoppingcart(C_ID, i)) = "1") then %>
                                                    R$<%= RetornaCasasDecimais(cdbl(ARYshoppingcart(C_PrecoCesta,i)) + cdbl(ARYShoppingcart(C_Embrulho, i)),2)%>
                                                <%else %>
                                                    <%= MontaDePara(ARYShoppingcart(C_Id, i), ARYshoppingcart(C_Qtd,i)) %>
                                                <%end if %>
                                            </span></td>

                                          <td style="text-align: center;">
                                              <input name="deletaritem" type="image" onclick="document.frm_basket.action='/carrinho.asp?DeleteItem=<%=CStr(i)%>';" src="/img/icone_x.png">
                                          </td>
                                        </tr>

                                    <%
                                                        LinhasCarrinhoMobile = LinhasCarrinhoMobile & "<div class=""row linha-produto"">" & vbcrlf
                                                        LinhasCarrinhoMobile = LinhasCarrinhoMobile &  "    <div class=""col-sm-2 col-xs-4"">" & vbcrlf
                                                        LinhasCarrinhoMobile = LinhasCarrinhoMobile &  "        <div class=""input-group botoes-estilo-1"">" & vbcrlf
                                                        LinhasCarrinhoMobile = LinhasCarrinhoMobile &               Retorna_Estoque_Produto_Combo("QuantityM" & CStr(i), ARYshoppingcart(C_Estoque,i), ARYshoppingcart(C_Qtd, i), ARYshoppingcart(C_ID,i), Recupera_Codigo_Abacos(ARYshoppingCart(C_ID, i), ARYshoppingCart(C_Tamanho, i), ARYshoppingCart(C_Cor, i), "", ARYshoppingCart(C_Tipo, i)), "1")
                                                        LinhasCarrinhoMobile = LinhasCarrinhoMobile &  "        </div>" & vbcrlf
                                                        LinhasCarrinhoMobile = LinhasCarrinhoMobile &  "    </div>" & vbcrlf
                                                        LinhasCarrinhoMobile = LinhasCarrinhoMobile &  "    <div class=""col-sm-8 col-xs-8"">" & vbcrlf
                                                        LinhasCarrinhoMobile = LinhasCarrinhoMobile &  "        <p class=""semi-bold""><a  href=""/detalhe-produto/" & sNomeProdutoSEO & "/" & ARYShoppingcart(C_ID,i) & """>" & ARYShoppingcart(C_Titulo, i) & "</a></p>" & vbcrlf
                                                        LinhasCarrinhoMobile = LinhasCarrinhoMobile &  "    </div>" & vbcrlf
                                                        LinhasCarrinhoMobile = LinhasCarrinhoMobile &  "    <div class=""col-sm-2 col-xs-12 text-right"">" & vbcrlf
                                                        LinhasCarrinhoMobile = LinhasCarrinhoMobile &  "        <span class=""price cor-4 extra-bold"">" & vbcrlf
                        
                                                        if (Session("embrulho_" & ARYshoppingcart(C_ID, i)) = "1") then 
                                                            LinhasCarrinhoMobile = LinhasCarrinhoMobile & "R$ " & RetornaCasasDecimais(cdbl(ARYshoppingcart(C_PrecoCesta,i)) + cdbl(ARYShoppingcart(C_Embrulho, i)),2)
                                                        else
                                                            LinhasCarrinhoMobile = LinhasCarrinhoMobile & MontaDePara(ARYShoppingcart(C_Id, i), ARYshoppingcart(C_Qtd,i))
                                                        end if

                                                        LinhasCarrinhoMobile = LinhasCarrinhoMobile &  "         </span>" & vbcrlf
                                                        LinhasCarrinhoMobile = LinhasCarrinhoMobile &  "        <br/><span class=""excluirM""><input name=""deletaritemM"" type=""image"" onclick=""document.frm_basket.action='/carrinho.asp?DeleteItem=" & CStr(i) & "';"" src=""/img/icone_x.png""></span>" & vbcrlf
                                                        LinhasCarrinhoMobile = LinhasCarrinhoMobile &  "        <hr class=""mobile"" style=""border-color: #df6b3a; "" />" & vbcrlf
                                                        LinhasCarrinhoMobile = LinhasCarrinhoMobile &  "    </div>" & vbcrlf
                                                        LinhasCarrinhoMobile = LinhasCarrinhoMobile &  "</div>" & vbcrlf


				                                        iSubTotal           = iSubtotal + cdbl(ARYshoppingcart(C_PrecoCesta,i))

		                                                sIdProdutoCesta 	= (sIdProdutoCesta 	& ARYShoppingcart(C_ID,i) & ",")
		                                                sSugestaoFab		= trim(Recupera_Campos_Db_oConn("TB_PRODUTOS", ARYShoppingcart(C_ID,i), "id", "Id_fabricante"))
		                                                sSugestaoProd		= ARYShoppingcart(C_ID,i)
	                                                Next 
                    

                                                    ' SOMAR O VALOR DO EMBRULHO JUNTO AO SUBTOTAL
                                                    if (sValorEmbrulho > 0 and sValorEmbrulho <> "") then 
                                                        iSubtotal = cdbl(iSubtotal) + cdbl(sValorEmbrulho)
                                                    end if

	                                                Session("SubTotalCompra") 		= RetornaCasasDecimais(iSubtotal,2)
	                                                Session("TotalCompraSemFrete") 	= RetornaCasasDecimais(iSubtotal,2)

	                                                ' MONTA O TOTAL - SEM O DESCONTO DE CUPOM =======================================
	                                                sMontaTotal = RetornaCasasDecimais(cdbl(iSubtotal) + cdbl(sTotalDescontoCesta),2)

	                                                ' ==================================================================================================================
	                                                if(Session("CupomGet") = "") then 
		                                                if(Replace_Caracteres_Especiais(request("presente")) <> "") then
			                                                call Abre_Conexao()
			
			                                                sSQlVale = "SELECT NumeroPedidoUtilizado,TipoDesconto,ProdutosDesconto, Valor, TipoDescontoValor FROM TB_CUPONS (NOLOCK) WHERE Cupom = '"& Replace_Caracteres_Especiais(request("presente")) &"' " & _ 
						                                                " AND (getdate() between DataValIni and DataValFim) " & _ 
						                                                " AND (SubString(Cupom, 1, 1) = 'C' OR SubString(Cupom, 1, 8) = 'CLJABRIL') " & _ 
						                                                " AND  CDCLIENTE = " & sCdCliente
                                                            set RsVale = oConn.execute(sSQlVale)
			
			                                                sProdutosCupom = ""
			
			                                                if(not RsVale.eof) then 
				                                                sPedidoUtilizado 	= trim(RsVale("NumeroPedidoUtilizado"))	' VERIFICAR SE O CUPOM JA TEM PEDIDO UTILIZADO
				                                                sTipoDesconto    	= trim(RsVale("TipoDesconto"))			' TIPO DO DESCONTO - MULTI COMPRAS OU UNITARIO
				                                                sTipoDescontoValor  = trim(RsVale("TipoDescontoValor"))		' TIPO DO DESCONTO - MULTI COMPRAS OU UNITARIO
				                                                sProdutosCupom   	= trim(RsVale("ProdutosDesconto"))		' VERIFICAR SE O CUPOM SO FUNCIONA COM ALGUNS PRODUTOS
				
                                                                if(sProdutosCupom = "" or isnull(sProdutosCupom)) then 
		                                                            ' PEGA TODOS OS PRODUTOS DA BASE PQ O CUPOM SERVIRA PARA TODOS OS PRODUTOS ATUAIS
		                                                            sSql = "Select id from TB_PRODUTOS (NOLOCK) where cdcliente = "& sCdCliente &" AND ATIVO = 1 and isnull(descontoignorar,'') <> 1 and qte_estoque > 0 order by id"
		                                                            set RsCombo = oConn.execute(sSQL)

		                                                            if(not RsCombo.eof) then 
			                                                            do while not RsCombo.eof
				                                                            sProdutosCupom = (sProdutosCupom & "," & trim(RsCombo("id")))
			                                                            RsCombo.movenext
			                                                            loop
		                                                            end if
		                                                            if (left(sProdutosCupom,1) = ",") then
			                                                            sProdutosCupom = right(sProdutosCupom, len(sProdutosCupom)-1)
		                                                            end if
		                                                            if (right(sProdutosCupom,1) = ",") then
			                                                            sProdutosCupom = left(sProdutosCupom, len(sProdutosCupom)-1)
		                                                            end if
		                                                            set RsCombo = nothing
				
		                                                            Session("ProdutosDesconto") = sProdutosCupom
		                                                        else
                                                                    Session("ProdutosDesconto") = sProdutosCupom
                                                                end if

				                                                if(sProdutosCupom = "" or isnull(sProdutosCupom)) then 
					                                                sProdutosCupom = Session("ProdutosDesconto")
				                                                end if
				
				                                                ' UNICA COMPRA ********************
				                                                if(sTipoDesconto = "1") then 
					                                                if(sPedidoUtilizado = "" or isnull(sPedidoUtilizado)) then
						                                                boolValidado = true
					                                                else
						                                                boolValidado = false
					                                                end if
				                                                else
					                                                ' MUILTI COMPRA ********************
					                                                ' boolValidado = true
					
					                                                sProdutosCupom2  		= (trim(sProdutosCupomFuncoes) & ",")
					                                                sArrayProdutosCupom 	= split(sIdProdutoCesta, ",")
					                                                sArrayProdutosCupom3 	= split(sProdutosCupom2, ",")
					                                                boolValidado 			= false
					
					                                                if(ubound(sArrayProdutosCupom) > 0) then 
						                                                ' LISTANDO TODOS OS PRODUTOS DO CUPOM ******************************
						                                                for lContArray = 0 to ubound(sArrayProdutosCupom)
							                                                if(sArrayProdutosCupom(lContArray) <> "") then 
								                                                if(InStr(1, sProdutosCupom2, sArrayProdutosCupom(lContArray)) > 0) then
									                                                boolValidado = true
									                                                exit for
								                                                end if
							                                                end if
						                                                next
					                                                end if
				                                                end if
				
				                                                if(sProdutosCupom <> "" AND not isnull(sProdutosCupom)) then
					                                                sProdutosCupom  		= (trim(sProdutosCupom) & ",")
					                                                sArrayProdutosCupom 	= split(sIdProdutoCesta, ",")
					                                                boolValidado = false
	
					                                                ' LISTANDO TODOS OS PRODUTOS DO CUPOM ******************************
					                                                for lContArray = 0 to ubound(sArrayProdutosCupom)
						                                                if(sArrayProdutosCupom(lContArray) <> "") then 
							                                                if(InStr(1, sProdutosCupom, sArrayProdutosCupom(lContArray)) > 0) then
								                                                boolValidado = true
								                                                exit for
							                                                end if
						                                                end if
					                                                next
				                                                end if
				
                                                                if(boolValidado) then

                                                                    if(sTipoDescontoValor = "R") then
			                                                            sValorPresente 	        =   RetornaCasasDecimais(trim(Recupera_Campos_Db("TB_CUPONS", "'" & Replace_Caracteres_Especiais(request("presente")) & "'", "Cupom", "Valor")),2)
                                                                        sValorCumpomDescricao   =   "O seu cupom de <span class=""titulo_texto_medio_laranja"">R$"& sValorPresente &" </span> j&aacute; est&aacute; calculado"
		                                                            else
			                                                            sValorPresente 	= RetornaCasasDecimais((cdbl(trim(Recupera_Campos_Db("TB_CUPONS", "'" & Replace_Caracteres_Especiais(request("presente")) & "'", "Cupom", "Valor")))/ 100) * cdbl(sMontaTotal),2)
                                                                        sValorCumpomDescricao   =   "O seu cupom de <span class=""titulo_texto_medio_laranja"">"& trim(Recupera_Campos_Db("TB_CUPONS", "'" & Replace_Caracteres_Especiais(request("presente")) & "'", "Cupom", "Valor")) & "%</span> j&aacute; est&aacute; calculado"
		                                                            end if
					                                                'ESTA SEÇÃO SO É PREENCHIDO AQUI POR MOTIVO DO SUBTOTAL+FRETE
					                                                Session("ValorPresente")    =   sValorPresente
                                    
                                    
                                                                    ' MONTA O TOTAL - COM O DESCONTO DE CUPOM NA CESTA =================================
	                                                                sMontaTotal = RetornaCasasDecimais(cdbl(sMontaTotal) - cdbl(sTotalDescontoCesta),2)

                                                                    ' QUANDO O VALE-PRESENTE EH MAIOR Q A COMPRA TODA
					                                                if(cdbl(sValorPresente) > cdbl(sMontaTotal + Calcula_Frete("1", sProvinciaCad, sMontaTotal))) then 
						                                                sValePresenteSaldo 	= RetornaCasasDecimais(cdbl(sValorPresente) - cdbl(sMontaTotal + Calcula_Frete("1", sProvinciaCad, sMontaTotal)),2)
                                                                        sMemsagemCredito    = "Você ainda possui R$ "& sValePresenteSaldo &" de crédito em compras.\nDeseja mesmo finalizar o pedido?"
                                        
                                                                        sMontaTotal 		= 0
					                                                else
                                                                        sMontaTotal = RetornaCasasDecimais((cdbl(sMontaTotal) - cdbl(sValorPresente)),2)
						                                                if(sMontaTotal < 0) then sMontaTotal = 0
					                                                end if

					                                                if(Session("CupomGet") = "") then 
						                                                sMemsagemAjuda  = "O código do seu vale presente/cupom de desconto também deve ser informado na página de finalização do pedido."
					                                                end if
				                                                end if
			                                                end if
			                                                set RsVale = nothing
			                                                call Fecha_Conexao()
			
			                                                ' DIGITOU CUPOM NO CARRINHO PARA RECALCULAR =============================== **********************

			                                                sValorDescontadoImposto 			= ((cdbl(ValorImposto) / 100) * cdbl(sMontaTotal))
			                                                sValorDescontadoImposto 			= replace(replace(RetornaCasasDecimais(sValorDescontadoImposto,2),",",""),".","")
			                                                Session("ValorDescontadoImposto") 	= sValorDescontadoImposto

			                                                sValorFrete 						= Calcula_Frete("1", sProvinciaCad, sMontaTotal)

			                                                if (session("sCheckedAgendamento")) then sValorFrete	=	sValorFrete + Session("sAcrescimoAgendamento")
			                
					                                        sValorRefrigerado 					= Calcula_Refrigerado(sTotalKiloCongelado)
					                                        Session("ValorRefrigerado") 		= sValorRefrigerado
		
			                                                Session("TotalCompraSemFrete")  	= RetornaCasasDecimais((cdbl(sMontaTotal)),2)
                                                            Session("ValorEmbrulho") 			= cdbl(sValorEmbrulho)

			                                                sMontaTotal 	  			= (cdbl(sMontaTotal) + cdbl(sValorFrete) + cdbl(sValorRefrigerado))
                                                            sMontaTotal 	  			= RetornaCasasDecimais(sMontaTotal,2)
		                                                else
                            
                            
                                                            ' MONTA O TOTAL - COM O DESCONTO DE CUPOM NA CESTA =================================
	                                                        sMontaTotal = RetornaCasasDecimais(cdbl(sMontaTotal) - cdbl(sTotalDescontoCesta),2)

                                                            ' NAO DIGITOU CUPOM NO CARRINHO PARA RECALCULAR =============================== ******************
			                                                sValorDescontadoImposto 			= ((cdbl(ValorImposto) / 100) * cdbl(sMontaTotal))
			                                                sValorDescontadoImposto 			= replace(replace(RetornaCasasDecimais(sValorDescontadoImposto,2),",",""),".","")
			                                                Session("ValorDescontadoImposto") 	= sValorDescontadoImposto
			                                                sValorFrete 						= Calcula_Frete("1", sProvinciaCad, sMontaTotal)
						
			                                                if (session("sCheckedAgendamento")) then sValorFrete    = sValorFrete + Session("sAcrescimoAgendamento")

					                                        sValorRefrigerado 			= Calcula_Refrigerado(sTotalKiloCongelado)
					                                        Session("ValorRefrigerado") = sValorRefrigerado

			                                                Session("TotalCompraSemFrete")  	= RetornaCasasDecimais((cdbl(sMontaTotal)),2)
                                                            Session("ValorEmbrulho") 			= cdbl(sValorEmbrulho)

			                                                sMontaTotal 	  			= (cdbl(sMontaTotal) + cdbl(sValorFrete) + cdbl(sValorRefrigerado))
                                                            sMontaTotal 	  			= RetornaCasasDecimais(sMontaTotal,2)
			
		                                                end if
	                                                else

                                                        ' MONTA O TOTAL - COM O DESCONTO DE CUPOM NA CESTA =================================
	                                                    sMontaTotal = RetornaCasasDecimais(cdbl(sMontaTotal) - cdbl(sTotalDescontoCesta),2)

	                                                    ' ENTROU CUPONADO NO GET =================================================== **********************
                                                        if(sMontaTotal < 0) then sMontaTotal = 0
		
		                                                sValorDescontadoImposto 			= ((cdbl(ValorImposto) / 100) * cdbl(sMontaTotal))
		                                                sValorDescontadoImposto 			= replace(replace(RetornaCasasDecimais(sValorDescontadoImposto,2),",",""),".","")
		                                                Session("ValorDescontadoImposto") 	= sValorDescontadoImposto
		                
                                                        sValorFrete 						= Calcula_Frete("1", sProvinciaCad, sMontaTotal)
	
				                                        sValorRefrigerado 				    = Calcula_Refrigerado(sTotalKiloCongelado)
				                                        Session("ValorRefrigerado") 	    = sValorRefrigerado
		               


                                                        if(ucase(Recupera_Campos_Db("TB_CUPONS", Session("CupomGet"), "Cupom", "TipoDescontoValor")) = "R") then 
			                                                sValorPresente 	        =   RetornaCasasDecimais(trim(Recupera_Campos_Db("TB_CUPONS", "'" & Replace_Caracteres_Especiais(request("presente")) & "'", "Cupom", "Valor")),2)
                                                            sValorCumpomDescricao   =   "O seu cupom de <span class=""titulo_texto_medio_laranja"">R$"& sValorPresente &" </span> j&aacute; est&aacute; calculado"
		                                                else
			                                                sValorPresente 	= RetornaCasasDecimais((cdbl(trim(Recupera_Campos_Db("TB_CUPONS", "'" & Replace_Caracteres_Especiais(request("presente")) & "'", "Cupom", "Valor")))/ 100) * cdbl(sMontaTotal),2)
                                                            sValorCumpomDescricao   =   "O seu cupom de <span class=""titulo_texto_medio_laranja"">"& trim(Recupera_Campos_Db("TB_CUPONS", "'" & Replace_Caracteres_Especiais(request("presente")) & "'", "Cupom", "Valor")) & "%</span> j&aacute; est&aacute; calculado"
		                                                end if

                                    
                                                        ' QUANDO O VALE-PRESENTE EH MAIOR Q A COMPRA TODA
				                                        if(cdbl(sValorPresente) > cdbl(sMontaTotal + Calcula_Frete("1", sProvinciaCad, sMontaTotal))) then 
					                                        sValePresenteSaldo 	= RetornaCasasDecimais(cdbl(sValorPresente) - cdbl(sMontaTotal + Calcula_Frete("1", sProvinciaCad, sMontaTotal)),2)
                                                            sMemsagemCredito  = "Você ainda possui R$ "& sValePresenteSaldo &" de crédito em compras.\nDeseja mesmo finalizar o pedido?"
                                        
                                                            sMontaTotal 		= 0
				                                        else
					                                        if(sMontaTotal < 0) then sMontaTotal = 0
				                                        end if

			                                            sValorFrete 						= Calcula_Frete("1", sProvinciaCad, sMontaTotal)

			                                            if (session("sCheckedAgendamento")) then sValorFrete	=	sValorFrete + Session("sAcrescimoAgendamento")
			                
				                                        sValorRefrigerado 					= Calcula_Refrigerado(sTotalKiloCongelado)
				                                        Session("ValorRefrigerado") 		= sValorRefrigerado
		
			                                            Session("TotalCompraSemFrete")  	= RetornaCasasDecimais((cdbl(sMontaTotal)),2)
                                                        Session("ValorEmbrulho") 			= cdbl(sValorEmbrulho)

			                                            sMontaTotal 	  			= (cdbl(sMontaTotal) + cdbl(sValorFrete) + cdbl(sValorRefrigerado))
                                                        sMontaTotal 	  			= RetornaCasasDecimais(sMontaTotal,2)
	                                                end if
 

                                                    %>

                                      </tbody>

		                                 <tbody class="">
                                        <tr >
                                          <td class="camposCEPcarrinho" colspan="2" >

                                              <div class="camposCEPcarrinho">
                                                    <div class="camposCEPcarrinho1">

                                                        Frete: <input type="text" name="cepd" id="cepd" size="15" maxlength="8" onkeypress="return VerificarEnter(event);" onKeyUp="this.value=this.value.replace(/\D/g,'')" 
                                                                        value="<%if (Session("sCepAgendamento") <> "") then : response.write Session("sCepAgendamento") else  response.write sCepDigitado end if %>" style="width: 86px; height: 34px;color: red;">

                                                        <%if(Session("CupomGet") = "") then %>
                                                            <%if(Replace_Caracteres_Especiais(request("presente")) <> "") then %>
                                                                <button class="btn-search" type="button" onclick="return calculafretedetalhes('<%= replace(RetornaCasasDecimais(cdbl(Session("SubTotalCompra")) - cdbl(sValorPresente), 2),",",".")%>','','CESTA','', 'capital', 'cepd')" style="height: 44px;width: 37px;"><i class="fa fa-search"></i></button>
                                                            <%else%>
                                                                <button class="btn-search" type="button" onclick="return calculafretedetalhes('<%= replace(replace(RetornaCasasDecimais(cdbl(Session("SubTotalCompra")),2),".",""),",",".") %>','','CESTA','', 'capital', 'cepd')" style="height: 44px;width: 37px;"><i class="fa fa-search"></i></button>
                                                            <%end if%>
                                                        <%else%>
                                                            <button class="btn-search" type="button" onclick="return calculafretedetalhes('<%=  replace(replace(RetornaCasasDecimais(cdbl(Session("SubTotalCompra")),2),".",""),",",".") %>','','CESTA','', 'capital', 'cepd')" style="height: 44px;width: 37px;"><i class="fa fa-search"></i></button>
                                                        <%end if%>

                                                        <img src="/img/zoomloader.gif" alt="CEP carregando" id="carregarcep"  />
                                                        &nbsp;&nbsp;&nbsp;
                                                    </div>
                                    
                                              </div>

                                          </td>

                                          <td class="camposCEPcarrinho" >

                                              <div class="camposCEPcarrinho">
                                                    <div class="camposCEPcarrinho1">
                                                        Cupom: <input type="text" name="cupomdesconto" id="cupomdesconto" size="15" maxlength="15" value="<%= request("cupomdesconto") %>" style="width: 105px; height: 34px;color: red;">
                                                        <button id="btenviarcupom" class="btn-search" type="button" onclick="return calculadescontodetalhes('<%= replace(replace(RetornaCasasDecimais(cdbl(Session("SubTotalCompra")),2),".",""),",",".") %>','','CESTA','', 'cupomdesconto', 'cupomvalor')" style="height: 44px;width: 37px;"><i class="fa fa-search"></i></button>
                                                        <img src="/img/zoomloader.gif" alt="CEP carregando" id="carregarcupom"  style="display:none" />
                                                        <img src="/img/zoomloader.gif" alt="CEP carregando" id="LoadCupom" style="display: none"  />
                                                        &nbsp;&nbsp;&nbsp;
                                                    </div>                                  
                                              </div>
                                          </td>


                                          <td colspan="2" style="width: 240px; text-align: right;"><b>Soma dos produto(s):</b></td>
                                          <td>R$<%= formatNumber(iSubTotal, 2) %></td>
                                        </tr>

                                        <tr class="">
                                          <td colspan="3" rowspan="3"><div id="capital"  class="capital"></div></td>

                                            <% if(sValorFrete <> "0" and sValorFrete <> "") then  %>
                                              <td colspan="2" style="text-align: right;"><b>Frete:</b></td>

                                              <td >                             
                                                    <%if(cdbl(sValorFrete) = 0 ) then %>
                                                        <FONT id="FONT1">R$0,00</FONT>
					                                <%ELSE%>
                                                        R$<%= RetornaCasasDecimais(sValorFrete, 2) %>

						                                <%
						                                    session("fretecesta") = 0
						                                    session("fretecesta") = RetornaCasasDecimais(sValorFrete, 2)
						                                %>
					                                <%END IF%>
                                              </td>
                                            <% end if %>
                                        </tr>

                                        <tr  class="">
                                          <td colspan="2" style="text-align: right;"><b>Desconto Cupom:</b></td>
                                          <td >R$ <span id="cupomvalor">0,00</span></td>
                                        </tr>

                                        <tr  class="">
                                          <td colspan="2" style="text-align: right;"></td>
                                          <td >
                                              <strong class="extra-bold cor-5" style="font-size: 24px;"><span id="totalajax">R$<%= RetornaCasasDecimais(sMontaTotal, 2) %></span></strong>
                                              <%Session("sMontaTotal") = RetornaCasasDecimais(sMontaTotal,2) %>
                                          </td>
                                        </tr>

                                      </tbody>

                                        <%  Session("TotalCompra") = sMontaTotal
                                            For i = 1 to iCount
	                                            Response.Write( "<input type=""hidden""  name=""Quantity" & CStr(i) & """ value=" & ARYshoppingcart(C_Qtd, i) &">" )
                                            Next
                                        %>

                                    <% else 
                                        LinhasCarrinhoMobile = LinhasCarrinhoMobile & "Não há produtos no carrinho de compras!"
                                        %>
                                                                        
                                        <br /><br /><center><b>Não há produtos no carrinho de compras!</b></center><br /><br />

                                    <% end if %>


                                    </table>

                                  </div>
                                  <div class="cart_navigation desktop"> 
                                      <a class="continue-btn AlinharMobileCont" href="\"><i class="fa fa-arrow-left"> </i>&nbsp; Continuar Comprando</a> 

                                      <% If iCount > 0 Then %>
                                            <p id="labelmudarcep" style="display:none; float: right;">[Altere o CEP para continuar]</p>
                                            <input name="finalizar" id="btcontinuar1" value="FINALIZAR PEDIDO"  type="image" src="/img/btcomprar.JPG" class="checkout-btn2" <%if (sMemsagemCredito <> "") then response.write "onclick=""return confirm('"& sMemsagemCredito &"')""" end if%>>
                                      <% end if %>
                                  </div>

                                </div>

							    <div class="col-sm-12 mobile">
								    <div class="jumbotron">
                                        <%= LinhasCarrinhoMobile %>
								    </div>
							    </div>

                                <div class="camposCEPcarrinho mobile">
                                    <div class="camposCEPcarrinho1">
                                        Frete: <input type="text" name="cepd" id="cepdM" size="15" maxlength="8" onkeypress="return VerificarEnter(event);" onKeyUp="this.value=this.value.replace(/\D/g,'')" 
                                                        value="<%if (Session("sCepAgendamento") <> "") then : response.write Session("sCepAgendamento") else  response.write sCepDigitado end if %>" style="width: 86px; height: 34px;color: red;">

                                        <%if(Session("CupomGet") = "") then %>
                                            <%if(Replace_Caracteres_Especiais(request("presente")) <> "") then %>
                                                <button class="btn-search" type="button" onclick="return calculafretedetalhes('<%= replace(RetornaCasasDecimais(cdbl(Session("SubTotalCompra")) - cdbl(sValorPresente), 2),",",".")%>','','CESTA','', 'capitalM', 'cepdM')" style="height: 44px;width: 37px;"><i class="fa fa-search"></i></button>
                                            <%else%>
                                                <button class="btn-search" type="button" onclick="return calculafretedetalhes('<%= replace(replace(RetornaCasasDecimais(cdbl(Session("SubTotalCompra")),2),".",""),",",".") %>','','CESTA','', 'capitalM', 'cepdM')" style="height: 44px;width: 37px;"><i class="fa fa-search"></i></button>
                                            <%end if%>
                                        <%else%>
                                            <button class="btn-search" type="button" onclick="return calculafretedetalhes('<%=  replace(replace(RetornaCasasDecimais(cdbl(Session("SubTotalCompra")),2),".",""),",",".") %>','','CESTA','', 'capitalM', 'cepdM')" style="height: 44px;width: 37px;"><i class="fa fa-search"></i></button>
                                        <%end if%>

                                        <img src="/img/zoomloader.gif" alt="CEP carregando" id="carregarcepM"  />
                                        &nbsp;&nbsp;&nbsp;

                                        <br /><br />

                                        Cupom: <input type="text" name="cupomdesconto" id="cupomdescontoM" size="15" maxlength="15" value="<%= request("cupomdesconto") %>" style="width: 105px; height: 34px;color: red;">
                                        <button id="btenviarcupomM" class="btn-search" type="button" onclick="return calculadescontodetalhes('<%= replace(replace(RetornaCasasDecimais(cdbl(Session("SubTotalCompra")),2),".",""),",",".") %>','','CESTA','', 'cupomdescontoM', 'cupomvalorM')" style="height: 44px;width: 37px;"><i class="fa fa-search"></i></button>
                                        <img src="/img/zoomloader.gif" alt="CEP carregando" id="carregarcupomM"  style="display:none" />
                                        <img src="/img/zoomloader.gif" alt="CEP carregando" id="LoadCupomM" style="display: none"  />
                                        &nbsp;&nbsp;&nbsp;

                                        <br /><br /><div id="capitalM"  class="capital" style="width: 100%;margin: 4px 0 0 22px;"></div>
                                    </div>
                                    <br />
                                </div>


                                <% If iCount > 0 Then %>
							        <div class="col-sm-12 text-right mobile">
								        <h5 class="extra-bold cor-5">Sub-Total:  <span class="font-1" style="font-size: 20px;">R$ <%= formatNumber(iSubTotal, 2) %></span>  </h5>
                                        <h5 class="extra-bold cor-5">Desconto:  <span class="font-1" style="font-size: 20px;" id="cupomvalorM">R$ 0,00</span>  </h5>
								        <h2 class="extra-bold cor-5">Total:  <span class="font-1">R$ <%= RetornaCasasDecimais(sMontaTotal, 2) %></span>  </h2> 
								        <p><br>
                                              <% If iCount > 0 Then %>
                                                    <input name="finalizarM" id="btcontinuar2" value="FINALIZAR PEDIDO"  type="image" src="/img/btcomprar.JPG" class="checkout-btn2" <%if (sMemsagemCredito <> "") then response.write "onclick=""return confirm('"& sMemsagemCredito &"')""" end if%>>
                                              <% end if %>
								        </p>
							        </div>
                                <% end if %>
                        </form>

					</div>	

				</div>
			</div>
		</div>
	</div>
    </div>

    <!--#include virtual= "footer.asp"-->

    <script type="text/javascript" src="/js/ajax.js"></script>

    <script type="text/javascript">
        <% if (sCepDigitado <> "" OR Session("sCepAgendamento") <> "") then %>
            calculafretedetalhes('<%= replace(replace(RetornaCasasDecimais(cdbl(Session("SubTotalCompra")),2),".",""),",",".") %>', '', 'CESTA', '', 'capital', 'cepd');
        <% else %>
            $("#carregarcep").hide();
            $("#carregarcepM").hide();
        <% end if %>

        <% if (Session("cupomdesconto") <> "") then %>
                calculadescontodetalhes('<%= replace(replace(RetornaCasasDecimais(cdbl(Session("SubTotalCompra")) + cdbl(sValorFrete),2),".",""),",",".") %>', '', 'CESTA', '', 'cupomdesconto', 'cupomvalor');
                calculadescontodetalhes('<%= replace(replace(RetornaCasasDecimais(cdbl(Session("SubTotalCompra")) + cdbl(sValorFrete),2),".",""),",",".") %>', '', 'CESTA', '', 'cupomdesconto', 'cupomvalorM');
        <% end if %>

    </script>


    <%if (sMemsagemAjuda <> "") then%>
        <script>alert('<%= sMemsagemAjuda %>');</script>
    <%end if%>

    <%if(Replace_Caracteres_Especiais(request("presente")) <> "" and Session("CupomGet") = "") then%>
        <%if(not boolValidado) then%>
            <script>alert('Cupom inválido!');</script>
        <%end if%>
    <%end if%>

    <%
    ' Redireciona para a proxima pagina.
    If (Replace_Caracteres_Especiais(lcase(Request("finalizar.x"))) <> "") Then
	    Response.Clear
	    Response.Redirect (sPathHttps & "login")
    End If

    If (Replace_Caracteres_Especiais(lcase(Request("finalizarM.x"))) <> "") Then
	    Response.Clear
	    Response.Redirect (sPathHttps & "login")
    End If

    %>


</body>
</html>
