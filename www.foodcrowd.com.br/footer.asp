
<script src="/js/funcoes.js" type="text/javascript" ></script>
<script src="/js/Validacao.js" type="text/javascript" ></script>

<footer class="footer">

    <div class="container">
        <div class="row">

            <div class="col-sm-6" style="text-align: center;">
                <a href="/"><img src="/img/logo2.png" alt="<%= Application("NomeLoja") %>"></a>
                <p style=""><%= Application("TextoSobreemprsa") %></p>
            </div>

            <div class="col-sm-2">
                <ul class="lista">
                    <li><a href="/quemsomos">SOBRE </a></li>
                    <li><a href="/produtores">PRODUTORES</a></li>
                    <li><a href="/receitas">RECEITAS</a></li>
                    <li><a href="/produtos">PRODUTOS</a></li>
                    <li><a href="/contato">FALE CONOSCO</a></li>
                </ul>
            </div>

            <div class="col-sm-4">
                <ul class="borderLeftGreen lista">
				    <li><a href="/carrinho"><div class="seta-ico"></div>Meu Carrinho</a></li>
				    <li><a href="/meuspedidos"><div class="seta-ico"></div>Meus Pedidos</a></li>
				    <li><a href="/politicadevolucao"><div class="seta-ico"></div>Trocas e Devoluções</a></li>
				    <li><a href="/politicacancelamento"><div class="seta-ico"></div>Politica de Cancelamento</a></li>
				    <li><a href="/politicaprivacidade"><div class="seta-ico"></div>Política de Privacidade</a></li>
                </ul>
            </div>

            <div class="col-sm-12 ">
                <div class="row copyright ">
                    <div class="col-sm-12">
                        <hr style="border-color: #88a349; ">
                    </div>
                    <div class="col-xs-8">
                        <p >Copyright ©<%= year(date()) %> <a href="#"> <%= Application("Nome") %> </a>. All Rights Reserved.</p>
                    </div>
                    <div class="col-xs-4 text-right redes-sociais">
                        <a href="https://www.facebook.com/foodcrowdbr/"         target="_blank"><i class="fa fa-facebook-f"></i></a>
                        <a href="https://www.instagram.com/foodcrowdbr/?hl=en"  target="_blank"><i class="fa fa-instagram"></i></a>
                        <a href="https://br.pinterest.com/foodcrowd/"           target="_blank"><i class="fa fa-pinterest"></i></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</footer>


<script type="text/javascript">

        var reEmail1 = /^[\w!#$%&'*+\/=?^`{|}~-]+(\.[\w!#$%&'*+\/=?^`{|}~-]+)*@(([\w-]+\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
        var reEmail2 = /^[\w-]+(\.[\w-]+)*@(([\w-]{2,63}\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
        var reEmail3 = /^[\w-]+(\.[\w-]+)*@(([A-Za-z\d][A-Za-z\d-]{0,61}[A-Za-z\d]\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
        var reEmail = reEmail3;
 
        function ValidaEmail(campoemail)
        {
            eval("reEmail = reEmail");
            if (reEmail.test(campoemail)) {
                return true;
            } else if (campoemail != null && campoemail != "") {
                return false;
            }
        } 


    function validaNews(objValidar, objValidarF) {
        if (objValidar.value == '') {
            alert('Campo obrigatório!');
            objValidar.focus();
            return false;
        } else {
            objValidarF.submit();
        }
    }

   function validabusca() {
        if ($("#busca").val() == '') {
            alert('Por favor digite o texto para busca!');
            $("#busca").focus();
            return false;
        } else {
            if ($("#busca").val().length < 3) {
                alert('Por favor digite o texto para busca, mínimo de 3 letras e números!');
                $("#busca").focus();
                return false;
            } else {
                document.frmBus.submit();
            }           
        }
    }


   function validabuscaR() {
        if ($("#buscaR").val() == '') {
            alert('Por favor digite o texto para busca!');
            $("#buscaR").focus();
            return false;
        } else {
            if ($("#buscaR").val().length < 3) {
                alert('Por favor digite o texto para busca, mínimo de 3 letras e números!');
                $("#buscaR").focus();
                return false;
            } else {
                document.frmBusReceita.submit();
            }           
        }
    }
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131305992-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag() { dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-131305992-1');
    gtag('set', 'userId', 'UA-131305992-1');
</script>


<script type="text/javascript">
    window.smartlook||(function(d) {
    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
    c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', '88b93b1d83f0d086bae1fe44a7d06d48313bdbaa');
</script>



<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '845366192461802');
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" width="1" 
src="https://www.facebook.com/tr?id=845366192461802&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

