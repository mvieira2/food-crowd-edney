<!--#include virtual= "/hidden/funcoes.asp"-->

<%
dim sClassHeader: sClassHeader = "bg-1"
dim Prbusca

Prbusca = Replace_Caracteres_Especiais(trim(request("buscaR"))) 


' ====================================================================
function Monta_Carrossel_Post()
	dim sSQl, Rs, sConteudo, sCliente, sCidade, sNomeProduto, sData, sTitulo, sComentario, sAvaliacao, sclass

	call Abre_Conexao()

    sConteudo = ""
	
	sSQl = "Select * " & _
		    " from  " & _
				" tb_publicacoes (NOLOCK) "             & _
			"where "                                    & _
				" cdcliente       = "& sCdCliente       & _
				" AND Ativo       = 1             "     & _
                " AND isnull(Imagem,'') <> ''     "

        if(Prbusca <> "") then 
        
            if(InStr(Prbusca, " ") > 0) then 
                sArrayValoresId_TipoR   = split(Prbusca, " ")

                sSQlaUX = " AND ("

                for lContArray = 0 to ubound(sArrayValoresId_TipoR)
                    if(sArrayValoresId_TipoR(lContArray) <> "" ) then 
                        sSQlaUX = sSQlaUX & " lower(Nome) like '%"& lcase(sArrayValoresId_TipoR(lContArray)) &"%' OR"
                    end if
                next

                for lContArray = 0 to ubound(sArrayValoresId_TipoR)
                    if(sArrayValoresId_TipoR(lContArray) <> "" ) then 
                        sSQlaUX = sSQlaUX & " lower(Convert(varchar(8000), DESCRICAO)) like '%"& lcase(sArrayValoresId_TipoR(lContArray)) &"%' OR"
                    end if
                next

                for lContArray = 0 to ubound(sArrayValoresId_TipoR)
                    if(sArrayValoresId_TipoR(lContArray) <> "" ) then 
                        sSQlaUX = sSQlaUX & " lower(Nome_2) like '%"& lcase(sArrayValoresId_TipoR(lContArray)) &"%' OR"
                    end if
                next

                sSQlaUX = left(sSQlaUX, LEN(sSQlaUX)-3)
                sSQlaUX = sSQlaUX & ")"

                sSQl = sSQl & sSQlaUX
            else
                sSQl = sSQl & " AND (   (lower(Nome) like '%"& lcase(Prbusca) & "%') OR " & _ 
                                        " (lower(Nome_2) like '%"& lcase(Prbusca) & "%') OR " & _ 
                              "         (lower(Convert(varchar(8000), DESCRICAO)) like '%"& lcase(Prbusca) & "%') ) "
            end if
    end if

    sSQl = sSQl & " order by datacad desc "

    'response.Write sSQl
    set Rs = oConn.execute(sSQl)
	
	if(not Rs.Eof) then
        lCont = 1

		Do While (Not Rs.Eof)
            sIdP	    = trim(Rs("ID"))
			sDescricao  = trim(Rs("Nome"))
            sDestaque   = trim(Rs("Nome_2"))
            sImagem	    = trim(Rs("Imagem"))
    
            sConteudo = sConteudo & "<div class=""col-sm-4"">" & vbcrlf 
	            sConteudo = sConteudo & "<div class=""box-receitas"">" & vbcrlf 
		            sConteudo = sConteudo & "<a href=""/detalhe-receita/"& TrataSEO(trim(Rs("Nome"))) &"/"& sIdP &""" title="""& sDescricao &"""><img class=""img-rounded"" src="""& sLinkImagem &"EcommerceNew/upload/Publicacoes/"& sImagem &"""  title="""& sDescricao &""" alt="""& sDescricao &"""  ></a>" & vbcrlf 
		            sConteudo = sConteudo & "<h3 class=""cor-4 semi-bold shape5 text-uppercase"" style=""font-size: 19px;"">"& sDescricao &"</h3>" & vbcrlf 
		            sConteudo = sConteudo & "<div class=""text-justify""><p>"& sDestaque &"</p></div>" & vbcrlf 
		            sConteudo = sConteudo & "<div class=""text-right""><a class=""botao-4 btn text-uppercase"" href=""/detalhe-receita/"& TrataSEO(trim(Rs("Nome"))) &"/"& sIdP &""" title="""& sDescricao &""">Ler receita</a></div>" & vbcrlf 			
	            sConteudo = sConteudo & "</div>" & vbcrlf 
            sConteudo = sConteudo & "</div>" & vbcrlf 


            lCont = (lCont + 1)
		rs.movenext
		loop
	end if
	set Rs = nothing

	call Fecha_Conexao()

    Monta_Carrossel_Post = sConteudo
end function



' ====================================================================
function Monta_Titulos_Receitas()
	dim sSQl, Rs, sConteudo, sCliente, sCidade, sNomeProduto, sData, sTitulo, sComentario, sAvaliacao, sclass

	call Abre_Conexao()

    sConteudo = ""
	
	sSQl = "Select ID, Nome " & _
		    " from  " & _
				" tb_publicacoes (NOLOCK) "             & _
			"where "                                    & _
				" cdcliente       = "& sCdCliente       & _
				" AND Ativo       = 1             "     & _
                " AND isnull(Imagem,'') <> ''     "     & _
            " order by Nome "
    set Rs = oConn.execute(sSQl)
	
	if(not Rs.Eof) then
        lCont = 1

		Do While (Not Rs.Eof)
            sIdP	    = trim(Rs("ID"))
			sDescricao  = trim(Rs("Nome"))

            sConteudo = sConteudo & "<li><a href=""/detalhe-receita/"& TrataSEO(trim(Rs("Nome"))) &"/"& sIdP &""" title="""& sDescricao &""">"& sDescricao &"</a></li>" & vbcrlf 

            lCont = (lCont + 1)

		rs.movenext
		loop
	end if
	set Rs = nothing

	call Fecha_Conexao()

    Monta_Titulos_Receitas = sConteudo
end function

%>


<!DOCTYPE html>
<html lang="en" dir="ltr">
    <!--#include virtual= "head.asp"-->

    <body>
        <!--#include virtual= "header.asp"-->

        <div class=" desktop-margin-top-90" >
	        <div class="margin-60-0 ">
    		    <div class="container">
			        <div class="row ">
    				    <div class="col-sm-12">
					        <h1 class="cor-4 extra-bold "><span class="shape6">RECEITAS</span></h1>

					        <ul class="list-inline filtro-links">
    						    <li><a href="/receitas">Todas</a></li>
                                <%= Monta_Titulos_Receitas() %>

                                <br /><br />

                                <form action="<%= Request.ServerVariables("Script_Name")%>"  method="post" name="frmBusReceita" onsubmit="return validabuscaR()" id="frmBusReceita">
                                    <div class="input-group">
                                      <input type="text" class="form-control" placeholder="O que procura ?" name="buscaR" id="buscaR"  value="<%= Prbusca %>">
                                      <button class="btn-search" type="button" onclick="javascript: validabuscaR();" style="float: right;position: absolute;height: 35px;margin: 6px 0 0 -98px;"><i class="fa fa-search"></i></button>
                                    </div>
                                </form>

					        </ul>
				        </div>
			
                        <%= Monta_Carrossel_Post() %>

				    </div>
			    </div>
		    </div>
	    </div>

        <!--#include virtual= "footer.asp"-->
    </body>

</html>
