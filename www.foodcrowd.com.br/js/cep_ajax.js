
function preenche_endereco(cep, prNome) {

    //alert(cep);

    if ($.trim(cep) != '') {
		
	    if(cep.length == 8) {

            var rootPath = '/ajax/cep_ajax.asp?formato=javascript&cep=' + cep;

            $.getScript(rootPath, function () {

                if (resultadoCEP["retorno"] != "") {

                    if (resultadoCEP["logradouro"] != "") {			
                        $('#endereco').attr('readonly', true);
                        $("#endereco").val(unescape(resultadoCEP["logradouro"]));
                        $("#enderecoH").val(unescape(resultadoCEP["logradouro"]));
                    }
                    else {
                        $('#endereco').attr('readonly', false);
                        $('#endereco').attr('disabled', false);
	                }

                    if (resultadoCEP["bairro"] != "") {
                        $('#bairro').attr('readonly', true);
                        $("#bairro").val(unescape(resultadoCEP["bairro"]));
                        $("#bairroH").val(unescape(resultadoCEP["bairro"]));
                    }
                    else {
                        $('#bairro').attr('readonly', false);
                        $('#bairro').attr('disabled', false);
                    }

                    if (resultadoCEP["cidade"] != "") {
                        $("#cidade").val(unescape(resultadoCEP["cidade"]));
                        $("#cidadeH").val(unescape(resultadoCEP["cidade"]));
                    } else {
                        $('#cidade').attr('readonly', false);
                    }

                    if (resultadoCEP["uf"] != "") {
                        $("#provinciaH").val(unescape(resultadoCEP["uf"]));
                        $("#provinciaLegenda").val(unescape(resultadoCEP["uf"]));
                    }
                    else {
                        $('#provincia').attr('readonly', false);
                        $('#provincia').attr('disabled', false);
                        $('#provinciaLegenda').attr('disabled', false);
                        $('#provinciaLegenda').attr('readonly', false);
                    }

                    if (resultadoCEP["uflegenda"] != "") {
					    $("#provinciaLegenda").val(unescape(resultadoCEP["uflegenda"]));
                    }

                    if (resultadoCEP["numero"] != "") {
				        $('#numero').attr('readonly', false);
				        $('#numero').attr('disabled', false);
                    }
                }
            });
        }

	    else {
             alert("CEP Inválido (1)!");
            return false;
        }

    }
}