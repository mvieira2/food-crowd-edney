﻿
function calculadescontodetalhes(prvalor, prpeso, prtipo, prProduto, prNomeCampo, prCampoValor) {

    var prCupom = $("#" + prNomeCampo).val();
   
    $("#btenviarcupom").hide();
    $("#LoadCupom").show();

    $("#btenviarcupomM").hide();
    $("#LoadCupomM").show();

    $("#" + prCampoValor).load('/hidden/calculo_cupom_detalhes.asp', { tipo: '1', cupom: prCupom, valor: prvalor });

    if (prCupom == '') {
        $("#LoadCupom").hide();
        $("#cupomdesconto").val('');
        $("#cupomdesconto").focus();
        $("#btenviarcupom").show();

        $("#LoadCupomM").hide();
        $("#cupomdescontoM").val('');
        $("#cupomdescontoM").focus();
        $("#btenviarcupomM").show();
        return false;

    } else {
        //alert(prCupom);

        var rootPath = '/ajax/cupom_ajax.asp?cupom=' + prCupom;

        $.getScript(rootPath, function (boolvalidado) {

            if (boolvalidado == '1') {
                $("#" + prCampoValor).load('/hidden/calculo_cupom_detalhes.asp', { tipo: '1', cupom: prCupom, valor: prvalor });
                $("#totalajax").load('/hidden/calculo_cupom_detalhes.asp', { tipo: '2', cupom: prCupom, valor: prvalor });
            } else {
                //$("#totalajax").load('/hidden/calculo_cupom_detalhes.asp', { tipo: '2', cupom: prCupom, valor: prvalor });

                alert('CUPOM inválido!');
                $("#cupomdesconto").val('');
                $("#cupomdesconto").focus();

                $("#cupomdescontoM").val('');
                $("#cupomdescontoM").focus();
            }

            $("#LoadCupom").hide();
            $("#btenviarcupom").show();

            $("#LoadCupomM").hide();
            $("#btenviarcupomM").show();
        });

    }
}

function calculafretedetalhes(prvalor, prpeso, prtipo, prProduto, prNomeDiv, prNomeCampo) {

    var prcep = $("#" + prNomeCampo).val();

    $("#carregarcep").show();
    $("#carregarcepM").show();
    $("#" + prNomeDiv).hide();

    
    if (prcep != '' && prcep != undefined) {
        var prcontacep = prcep.length;
    } else {
        $("#carregarcep").hide();
        $("#carregarcepM").hide();
    }

    if (prcep == '' || prcep == undefined) {
        $("#carregarcep").hide();
        $("#carregarcepM").hide();

        $("#" + prNomeDiv).hide(); 
        $("#" + prNomeDiv).html('');

        //alert('Favor digite o CEP');
        return false;

    } else {

        if (prcontacep > 7) {

            //alert($("#cepd").val());
            var rootPath = '/ajax/cep_ajax.asp?formato=javascript&cep=' + prcep;

            $.getScript(rootPath, function () {
                if (resultadoCEP["resultado"]) {

                    if (prtipo == 'DETALHES') {
                        $("#" + prNomeDiv).load('/hidden/calculo_frete_detalhes.asp', { produto: prProduto, tipo: prtipo, cepAG: prcep, valor: prvalor, peso: prpeso, PaginaPedido: '0', tiposaida: '' });
                    } else {
                        if (prtipo == 'PEDIDOSAG') {
                            $("#" + prNomeDiv).load('/hidden/calculo_frete_detalhes.asp', { produto: prProduto, tipo: prtipo, cepAG: prcep, valor: prvalor, peso: prpeso, PaginaPedido: '1', tiposaida: '' });
                        } else {

                            //alert(resultadoCEP["cidade"]);

                            $("#" + prNomeDiv).load('/hidden/calculo_frete_detalhes.asp', { tipo: prtipo, cepAG: prcep, valor: prvalor, PaginaPedido: '1', tiposaida: '', CidadeFrete: resultadoCEP["cidade"], EstadoFrete: resultadoCEP["uflegenda"] }, function () {
                                $("#" + prNomeDiv).show();
                                $("#carregarcep").hide();
                                $("#carregarcepM").hide();
                            });

                            $("#freteajax").load('/hidden/calculo_frete_detalhes.asp', { tipo: prtipo, cepAG: prcep, valor: prvalor, PaginaPedido: '1', tiposaida: 'frete' }, function () {
                                $("#" + prNomeDiv).show();
                                $("#carregarcep").hide();
                                $("#carregarcepM").hide();
                            });

                            $("#totalajax").load('/hidden/calculo_frete_detalhes.asp', { tipo: prtipo, cepAG: prcep, valor: prvalor, PaginaPedido: '1', tiposaida: 'total' }, function () {
                                $("#" + prNomeDiv).show();
                                $("#carregarcep").hide();
                                $("#carregarcepM").hide();
                            });

                            $("#subfrete").load('/hidden/calculo_frete_detalhes.asp', { tipo: prtipo, cepAG: prcep, valor: prvalor, PaginaPedido: '1', tiposaida: 'frete' }, function () {
                                $("#" + prNomeDiv).show();
                                $("#carregarcep").hide();
                                $("#carregarcepM").hide();
                            });

                            //$("#freteajax").load('/hidden/calculo_frete_detalhes.asp', { tipo: prtipo, cepAG: prcep, valor: prvalor, PaginaPedido: '1', tiposaida: 'frete' });
                            //$("#totalajax").load('/hidden/calculo_frete_detalhes.asp', { tipo: prtipo, cepAG: prcep, valor: prvalor, PaginaPedido: '1', tiposaida: 'total' });
                            //$("#subfrete").load('/hidden/calculo_frete_detalhes.asp', { tipo: prtipo, cepAG: prcep, valor: prvalor, PaginaPedido: '1', tiposaida: 'frete' });
                            //$("#" + prNomeDiv).load('/hidden/calculo_frete_detalhes.asp', { tipo: prtipo, cepAG: prcep, valor: prvalor, PaginaPedido: '1', tiposaida: '', CidadeFrete: resultadoCEP["cidade"], EstadoFrete: resultadoCEP["uflegenda"] } );
                            // $("#subfrete").load('/hidden/calculo_frete_detalhes.asp', { tipo: prtipo, cepAG: prcep, valor: prvalor, PaginaPedido: '1', tiposaida: 'subfrete' });
                        }
                    }

                    //$("#" + prNomeDiv).show();
                    //$("#carregarcep").hide();

                } else {
                    alert("Cep não encontrado (1)");
                }
            });

            //$("#carregarcep").hide();
            
        } else {
            alert('O cep digitado é inválido!(1)');
            $("#carregarcep").hide();
            $("#carregarcepM").hide();
        }
    }
}

function calculafretePedido(prCep, prvalor) {
    //	alert(prCep);
    //	alert(prvalor);
    if (prCep == '') {
        alert('Favor digite o CEP');
        return false
    } else {
        $("#capital").load('/hidden/calculo_frete_detalhes.asp', { cepAG: prCep, valor: prvalor, PaginaPedido: '1' });
        //	$("#capital").load('/hidden/calculo_frete_detalhes.asp',{cepAG:prCep,valor:prvalor});
    }
}

function mostra_Imagem(sProduto, sTipo) {
    $(document).ready(function () {
        $("#img_grande").load('/ajax/busca_foto_ajax.asp', { produto: sProduto, tipo: sTipo });
    });
}

function mostra_variacao_cor(prProduto, prValor) {
    var prCor = $('#selectcor_2').find('option').filter(':selected').val();

    if (prValor == '') {
        $("#select").load('/ajax/busca_variacao_ajax.asp', { produto: prProduto, tamanho: prValor, id_tipo: 'tamanhoT' });
        $("#selectcor_2").load('/ajax/busca_variacao_ajax.asp', { produto: prProduto, cor: prValor, id_tipo: 'corT' });
    } else {
        $("#selectcor_2").load('/ajax/busca_variacao_ajax.asp', { produto: prProduto, tamanho: prValor, id_tipo: 'tamanho' });
    }
}

function mostra_variacao_tamanho(prProduto, prValor) {
    var prTamanho = $('#select').find('option').filter(':selected').val();

    if (prValor == '') {
        $("#select").load('/ajax/busca_variacao_ajax.asp', { produto: prProduto, tamanho: prValor, id_tipo: 'tamanhoT' });
        $("#selectcor_2").load('/ajax/busca_variacao_ajax.asp', { produto: prProduto, cor: prValor, id_tipo: 'corT' });

    } else {
        if (prTamanho == '') {
            //alert(prValor);
            $("#select").load('/ajax/busca_variacao_ajax.asp', { produto: prProduto, cor: prValor, id_tipo: 'cor' });
        }
    }
}

function mostra_variacao(prTipo, prProduto, prValor) {

    if (prValor == '') {
        alert('1')

        $("#select").load('/ajax/busca_variacao_ajax.asp', { produto: prProduto, tamanho: prValor, id_tipo: 'tamanhoT' });
        $("#selectcor_2").load('/ajax/busca_variacao_ajax.asp', { produto: prProduto, cor: prValor, id_tipo: 'corT' });

    } else {
        alert('2')

        if (prTipo == 'T') {
            $("#select").change(
			    function () {
			        alert('T' + prValor);

			        $("#selectcor_2").load('/ajax/busca_variacao_ajax.asp', { produto: prProduto, tamanho: prValor, id_tipo: 'tamanho' });
			    }
			);
        }

        if (prTipo == 'C') {
            $("#selectcor_2").change(
			    function () {
			        alert('C' + prValor);

			        $("#select").load('/ajax/busca_variacao_ajax.asp', { produto: prProduto, cor: prValor, id_tipo: 'cor' });
			    }
			);
        }
    }
}

function mostrapaginacaoAnt(prCat, prSub, prFab, prPagA, prVitrine, prOrigem) {
    switch (prVitrine) {
        case "L":
            $.ajax({
                url: "/vitrine_lancamento.asp",
                data: "cat=" + prCat + "&sub=" + prSub + "&Id_Fabricante=" + prFab + "&Pagina=" + prPagA + "&origem=" + prOrigem,
                async: true,
                success: function (data) {
                    $("#div_lancamentos").html(data)
                }
            });
            break
        case "O":
            $.ajax({
                url: "/vitrine_ofertas.asp",
                data: "cat=" + prCat + "&sub=" + prSub + "&Id_Fabricante=" + prFab + "&Pagina=" + prPagA + "&origem=" + prOrigem,
                async: true,
                success: function (data) {
                    $("#div_ofertas").html(data)
                }
            });
            break
        case "M":
            $.ajax({
                url: "/vitrine_maisvendido.asp",
                data: "cat=" + prCat + "&sub=" + prSub + "&Id_Fabricante=" + prFab + "&Pagina=" + prPagA + "&origem=" + prOrigem,
                async: true,
                success: function (data) {
                    $("#div_vendidos").html(data)
                }
            });
            break
        case "T":
            $.ajax({
                url: "/vitrine_todos.asp",
                data: "cat=" + prCat + "&sub=" + prSub + "&Id_Fabricante=" + prFab + "&Pagina=" + prPagA + "&origem=" + prOrigem,
                async: true,
                success: function (data) {
                    $("#div_todos").html(data)
                }
            });
    }
}

function loading(prDiv) {
    $("#" + prDiv).html('<div id="loading" style="width:99%;height:200px;margin:0px;padding:0px;top:0;left:0;z-index:5000; background:url(/img/loading.gif) no-repeat center center ">&nbsp;</div>');
    return;
}

/*function (prCat,prSub,prFab,prPagA,prVitrine,prOrigem){
switch (prVitrine) {
case "L":
loading('div_lancamentos');
$("#div_lancamentos").load('/vitrine_lancamento.asp',{cat:prCat, sub:prSub, Id_Fabricante:prFab,Pagina:prPagA,origem:prOrigem});
break
case "O":
loading('div_ofertas');
$("#div_ofertas").load('/vitrine_ofertas.asp',{cat:prCat, sub:prSub, Id_Fabricante:prFab,Pagina:prPagA});
break
case "M":
loading('div_vendidos');
$("#div_vendidos").load('/vitrine_maisvendido.asp',{cat:prCat, sub:prSub, Id_Fabricante:prFab,Pagina:prPagA});
break
case "T":
loading('div_todos');
$("#div_todos").load('/vitrine_todos.asp',{cat:prCat, sub:prSub, Id_Fabricante:prFab,Pagina:prPagA});
}
	
}*/