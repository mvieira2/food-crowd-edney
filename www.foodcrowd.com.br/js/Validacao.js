
/// <sumary>
/// 	OBJETIVO: Mascara em Campo

/// EXEMPLO: "CPF"	    - <input id="TxtCpf"   type="text" onkeyup="SetMask(this,event,'###.###.###-##')" />
///	EXEMPLO: "Dinheiro" - <input id="TxtValor" type="text" onkeyup="SetMask(this,event,'###.###.###,##',true,{pre:'R$'})" />
///	EXEMPLO: "Graus"    - <input id="txtG"     type="text" onkeyup="SetMask(this,event,'###,#',true,{pre:'',pos:'�'})" />

///  	 @param w - O elemento que ser� aplicado (normalmente this).
///  	 @param e - O evento para capturar a tecla e cancelar o backspace.
///  	 @param m - A m�scara a ser aplicada.
///  	 @param r - Se a m�scara deve ser aplicada da direita para a esquerda. Veja Exemplos.
///  	 @param a - Colcar Algum Sinal Tipo "R$" ,�
///  	 @ret

/// </sumary>

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function SetMask(w, e, m, r, a) {

    if (!e) var e = window.event
    if (e.keyCode) code = e.keyCode;
    else if (e.which) code = e.which;

    var txt = (!r) ? w.value.replace(/[^\d]+/gi, '') : w.value.replace(/[^\d]+/gi, '').reverse();
    var mask = (!r) ? m : m.reverse();
    var pre = (a) ? a.pre : "";
    var pos = (a) ? a.pos : "";
    var ret = "";

    if (code == 9 || code == 8 || txt.length == mask.replace(/[^#]+/g, '').length) return false;

    for (var x = 0, y = 0, z = mask.length; x < z && y < txt.length; ) {
        if (mask.charAt(x) != '#') {
            ret += mask.charAt(x); x++;
        } else {
            ret += txt.charAt(y); y++; x++;
        }
    }

    ret = (!r) ? ret : ret.reverse()
    w.value = pre + ret + pos;
}

String.prototype.reverse = function () {
    return this.split('').reverse().join('');
};


/// <sumary>
/// 	OBJETIVO: Validar CPF

/// EXEMPLO: "CPF" - <input id="TxtCpf" type="text" onblur="ValidaCpf(this)" />
/// </sumary>

function ValidaCpf(strCPF) {
   var Soma; var Resto; Soma = 0; if (strCPF == "00000000000") return false; for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i); Resto = (Soma * 10) % 11; if ((Resto == 10) || (Resto == 11)) Resto = 0; if (Resto != parseInt(strCPF.substring(9, 10)) ) return false; Soma = 0; for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i); Resto = (Soma * 10) % 11; if ((Resto == 10) || (Resto == 11)) Resto = 0; if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false; return true;
}

/// <sumary>
/// 	OBJETIVO: Validar CNPJ

/// EXEMPLO: "CNPJ" - <input id="TxtCnpj" type="text" onblur="ValidaCnpj(this)" />
/// </sumary>

function validaCnpj(Objcnpj) {

    var cnpj = Objcnpj.value;

    exp = /\.|\-|\//g
    cnpj = cnpj.toString().replace(exp, "");


    var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;

    digitos_iguais = 1;

    if (cnpj.length < 14 && cnpj.length < 15) {
        alert("Cnpj inv�lido");
        return false;
    }

    for (i = 0; i < cnpj.length - 1; i++)
        if (cnpj.charAt(i) != cnpj.charAt(i + 1)) {
            digitos_iguais = 0;
            break;
        }

    if (!digitos_iguais) {

        tamanho = cnpj.length - 2
        numeros = cnpj.substring(0, tamanho);
        digitos = cnpj.substring(tamanho);
        soma = 0;
        pos = tamanho - 7;

        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }

        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

        if (resultado != digitos.charAt(0)) {
            alert("Cnpj inv�lido");
            return;
        }

        tamanho = tamanho + 1;
        numeros = cnpj.substring(0, tamanho);
        soma = 0;
        pos = tamanho - 7;

        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }

        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

        if (resultado != digitos.charAt(1)) {
            alert("Cnpj inv�lido");
            return;
        }

        return true;
    }
    else {
        alert("Cnpj inv�lido");
        return;
    }

}

/// <sumary>
/// 	OBJETIVO: Validar Data

/// EXEMPLO: "Data" - <input id="TxtData" type="text" onblur="ValidaData(this)" />
/// </sumary>

function ValidaData(data) {
    if (obj.length > 0 || obj.value != "" || obj.value != "  /  /    ") {
        exp = /\d{2}\/\d{2}\/\d{4}/

        if (!exp.test(data.value)) {

            data.value = "";
            alert('Data Invalida!');
            data.select();
        }
    }
    else {
        alert('O Campo est� vazio!');
    }
}

/// <sumary>
/// 	OBJETIVO: Validar Cep

/// EXEMPLO: "Data" - <input id="TxtCep" type="text" onblur="ValidaCep(this)" />
/// </sumary>

function ValidaCep(cep) {

    if (cep.length > 0 || cep.value != "" || cep.value != "  .   -   ") {

        exp = /\d{2}\.\d{3}\-\d{3}/

        if (!exp.test(cep.value)) {
            alert('Numero de Cep Invalido!');
        }
    }
    else {
        alert('O Campo est� vazio!');
    }
}


/// <sumary>
/// 	OBJETIVO: Validar Telefone

/// EXEMPLO: "Telefone" - <input id="TxtTel" type="text" onblur="ValidaTelefone(this)" />
/// </sumary>

function ValidaTelefone(tel) {

    exp = /\(\d{2}\)\ \d{4}\-\d{4}/

    if (!exp.test(tel.value)) {
        alert('Numero de Telefone Invalido!');
    }
}


/// <sumary>
/// 	OBJETIVO: Validar Doc

/// EXEMPLO: "Doc" - <input id="TxtDoc" type="text" onblur="ValidaDocs(this)" />
/// </sumary>

function ValidaDocs(doc) {

    exp = /^[a-zA-Z0-9-_\.]+\.(pdf|txt|doc|xls|ppt)$/

    if (!exp.test(doc.value)) {
        alert('Documento Invalido!');
    }
}

/// <sumary>
/// 	OBJETIVO: Validar Img

/// EXEMPLO: "Img" - <input id="TxtImg" type="text" onblur="ValidaImg(this)" />
/// </sumary>

function ValidaImg(img) {

    exp = /^[a-zA-Z0-9-_\.]+\.(jpg|gif|png)$/

    if (!exp.test(img.value)) {
        alert('Imagem Invalida!');
    }
}

/// <sumary>
/// 	OBJETIVO: Validar IP

/// EXEMPLO: "IP" - <input id="TxtIP" type="text" onblur="ValidaIp(this)" />
/// </sumary>

function ValidaIp(ip) {

    exp = /^((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})$/

    if (!exp.test(ip.value)) {
        alert('Endere�o IP Invalido!');
    }
}

/// <sumary>
/// 	OBJETIVO: Validar DataHora

/// EXEMPLO: "DataHora" - <input id="TxtDataHora" type="text" onblur="ValidaDataHora(this)" />
/// </sumary>

function ValidaDataHora(data) {

    exp = /^((0[1-9]|[12]\d)\/(0[1-9]|1[0-2])|30\/(0[13-9]|1[0-2])|31\/(0[13578]|1[02]))\/\d{4} \d{2}:\d{2}$/

    if (!exp.test(data.value)) {
        alert('Data Invalida!');
    }
}

/// <sumary>
/// 	OBJETIVO: Validar Senha

/// EXEMPLO: "Senha" - <input id="TxtSenha" type="text" onblur="ValidarSenha(txtsenha1,txtsenha2)" />
/// </sumary>

function ValidarSenha(obj1, obj2) {
    if (obj2 != obj1) {
        alert("Senhas n�o conferem")
    }
}

/// <sumary>
/// 	OBJETIVO: Validar Vazio

/// EXEMPLO: "Vazio" - <input id="TxtNome" type="text" onblur="ValidaVazio(this)" />
/// </sumary>

function ValidaVazio(obj) {
    if (obj.length > 0) {
        alert('Campo Vazio');
    }
}

/// <sumary>
/// 	OBJETIVO: Validar Tamanho

/// EXEMPLO: "Tamanho" - <input id="TxtTamanho" type="text" onblur="ValidaTamanho(this)" />
/// </sumary>

function ValidaTamanho(obj, size) {

    if (obj.length < size) {
        alert('Tamanho muito pequeno');
    }

}

/// <sumary>
/// 	OBJETIVO: Validar Numeros/Letras (Sem caracter Especial)

/// EXEMPLO: "Tamanho" - <input id="TxtSenha" type="text" onblur="ValidaCaracter(this)" />
/// </sumary>

function ValidaCaracter(objcampo) {

    var campo = objcampo.value;

    var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";

    for (var i = 0; i < campo.length; i++) {

        if (iChars.indexOf(campo.charAt(i)) != -1) {
            alert("O campo possui caracteres especiais, por favor, preencha novamente!");
            return false;
        }
    }
}

/// <sumary>
/// 	OBJETIVO: Retirar caracter

/// EXEMPLO: "Retira caracter" - RetiraCaracter(horario, ':');
/// </sumary>

function RetiraCaracter(string, caracter) {
    var i = 0;
    var final = '';

    while (i < string.length) {

        if (string.charAt(i) == caracter) {

            final += string.substr(0, i);
            string = string.substr(i + 1, string.length - (i + 1));
            i = 0;
        }
        else {
            i++;
        }
    }
    return final + string;
}

/// <sumary>
/// 	OBJETIVO: So Permite Numero, Ponto e V�rgula

/// EXEMPLO: "Retira caracter" - <input type="text" id="TxExemplo" onkeypress="return isDecimal();" />
/// </sumary>

/// <returns>
///		Retorna True caso o valor da tecla pressionada seja um n�mero, ponto ou v�rgula.
///		Retorna False para qualquer outra tecla pressionada.
///	</returns>

function isDecimal() {

    var intKeyId = event.keyCode;

    switch (intKeyId) {
        case 48: // 0 (zero)
            return true;
            break;
        case 49: // 1 (um)
            return true;
            break;
        case 50: // 2 (dois)
            return true;
            break;
        case 51: // 3 (tr�s)
            return true;
            break;
        case 52: // 4 (quatro)
            return true;
            break;
        case 53: // 5 (cinco)
            return true;
            break;
        case 54: // 6 (seis)
            return true;
            break;
        case 55: // 7 (sete)
            return true;
            break;
        case 56: // 8 (oito)
            return true;
            break;
        case 57: // 9 (nove)
            return true;
            break;
        case 44: // , (v�rgula)
            return true;
            break;
        case 46: // . (ponto)
            return true;
            break;
        default:
            return false;
            break;
    }
}

/// <sumary>
/// 	OBJETIVO: So Permite Numero

/// EXEMPLO: "Retira caracter" - <input type="text" id="TxExemplo" onkeypress="return isNumeric(event);" />
/// </sumary>

/// <returns>
///		Retorna True caso o valor da tecla pressionada seja um n�mero, ponto ou v�rgula.
///		Retorna False para qualquer outra tecla pressionada.
///	</returns>

function isNumeric(e) {

    var tecla = (window.event) ? event.keyCode : e.which;

    if ((tecla > 47 && tecla < 58)) {

        return true;

    }
    else {

        if (tecla != 8) return false;
        else return true;

    }

}

/// <sumary>
/// 	OBJETIVO: Retirar caracter

/// EXEMPLO: "Ponto Por V�rgula" - <input type="text" id="TxtExemplo" onkeyup="PontoParaVirgula(this);" />
/// </sumary>

function PontoParaVirgula(strId) {

    var strValor = strId.value;

    document.getElementById(strId).value = strValor.replace('.', ',');
}

/// <sumary>
/// 	OBJETIVO: Abre Pagina PopUp

/// EXEMPLO:  <input type="button" onclick="abrePopUp('http://terra.com.br',800,600,true)"  value="Botao" />
/// </sumary>

function abrePopUp(pstrpagina, largura, altura, barra_rolagem) //barra_rolagem ('yes' ou 'no')
{
    // Vari�vel de par�metros
    var strAtributos = "";
    // Largura da janela
    strAtributos = strAtributos + "width=" + largura + ",";
    // Altura da janela
    strAtributos = strAtributos + "height=" + altura + ",";
    // Centraliza a janela
    strAtributos = strAtributos + "top=" + (screen.height - altura) / 2 + ",";
    strAtributos = strAtributos + "left=" + (screen.width - largura) / 2 + ",";
    // Permitir redimencionamento
    strAtributos = strAtributos + "resizable=no,";
    // Barra de rolagem
    strAtributos = strAtributos + "scrollbars=" + barra_rolagem + ",";
    // Barra de status
    strAtributos = strAtributos + "status=no,";
    // Barra de endere�o
    strAtributos = strAtributos + "location=no,";
    // Barra de ferramentas
    strAtributos = strAtributos + "toolbar=no,";
    // Barra de menu
    strAtributos = strAtributos + "menubar=no,";
    // Exibindo efetivamente a janela, seguindo os par�metros
    window.open(pstrpagina, '', strAtributos);
}

/// <sumary>
/// 	OBJETIVO: Abre Pagina Modal

/// EXEMPLO:  <input type="button" onclick="abreModal('http://terra.com.br',800,600)"  value="Botao" />
/// </sumary>

function abreModal(pstrpagina, largura, altura) {

    var strAtributos = "";
    // Largura da janela
    strAtributos = strAtributos + "dialogWidth:" + largura + "px;";
    // Altura da janela
    strAtributos = strAtributos + "dialogHeight:" + altura + "px;";
    // Centralizar a janela
    strAtributos = strAtributos + "center:yes;";
    // Ocultar o dialogo
    strAtributos = strAtributos + "dialogHide:yes;";
    // Bot�o ajuda
    strAtributos = strAtributos + "help:no;";
    // Permitir redimencionamento
    strAtributos = strAtributos + "resizable:no;";
    // Barra de rolagem
    strAtributos = strAtributos + "scroll:no;";
    // Barra de status
    strAtributos = strAtributos + "status:no;";
    // Exibindo efetivamente a janela, seguindo os par�metros
    window.showModalDialog(pstrpagina, 0, strAtributos);
}

/// <sumary>
/// 	OBJETIVO: Adicionar Zeros a Direita

/// EXEMPLO:  zerosDireita('230',7);
/// </sumary>

function zerosDireita(fld, intLen) {

    var strTexto = fld.value;
    var i = 0;

    // Caso a string j� esteja no tamanho certo, n�o faz nada.
    if (strTexto.length == intLen) return false;

    for (i = strTexto.length; i < intLen; i++) {
        strTexto = strTexto + '0';
    }

    fld.value = strTexto;
    return false;
}

/// <sumary>
/// 	OBJETIVO: Adicionar Zeros a Direita

/// EXEMPLO:  exibeOcultaArea(DivTeste);
/// </sumary>
function exibeOcultaArea(strIdArea) {

    if (document.getElementById) {
        style2 = document.getElementById(strIdArea).style;
        style2.display = style2.display == "none" ? "block" : "none";
    }
    else if (document.all) {
        style2 = document.all[strIdArea].style;
        style2.display = style2.display == "none" ? "block" : "none";
    }
    else if (document.layers) {
        style2 = document.layers[strIdArea].style;
        style2.display = style2.display == "none" ? "block" : "none";
    }
}

/// <sumary>
/// 	OBJETIVO: Limpa Campo

/// EXEMPLO:  limparCampo(this);
/// </sumary>

function limparCampo(fld) {
    fld.value = "";
}

/// <sumary>
/// 	OBJETIVO: Pega a versao Do Browser

/// EXEMPLO:  MsieVersion();
/// </sumary>

function MsieVersion() {
    var ua = window.navigator.userAgent
    var msie = ua.indexOf("MSIE ")

    if (msie > 0)      // If Internet Explorer, return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)))
    else                 // If another browser, return 0
        return 0

}

/// <sumary>
/// 	OBJETIVO: Verifica Enter

/// EXEMPLO:  VerificaEnter(event);
/// </sumary>

function VerificaEnter(evento) {
    var keyCodigo;
    keyCodigo = evento.keyCode;

    if (keyCodigo == 13) {
        return true;
    }
}