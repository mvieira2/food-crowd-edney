<!--#include virtual= "/hidden/funcoes.asp"-->

<%
dim sClassHeader: sClassHeader = "bg-1"
dim sConteudoInst, sTitulo
dim sSecao,  sAtivo(10)

sSecao 	= Replace_Caracteres_Especiais(trim(request("idtipo")))
    
if (sSecao <> "") then
	sTitulo 	        = ucase(trim(Recupera_Campos_Db("TB_INSTITUCIONAL_CATEGORIAS", sSecao, "ID", "Categoria")))
	sConteudoInst 	    = trim(Recupera_Campos_Db("TB_INSTITUCIONAL", sSecao, "Id_Categoria", "Conteudo"))
	if(sConteudoInst    = "" or isnull(sConteudoInst)) then sConteudo = "<center><strong>Desculpe-nos.</strong> N&atilde;o foi encontrada nenhuma ocorr&ecirc;ncia para a sua busca.<br >Por favor, tente novamente usando outra palavra-chave.</center>"
end if

%>

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <!--#include virtual= "head.asp"-->

    <body>
        <!--#include virtual= "header.asp"-->

        <div class=" desktop-margin-top-90" >
            <div class="margin-60-0 ">
                <div class="container">
			        <div class="row ">

				        <div class="col-sm-12">
					        <h1 class="cor-4 extra-bold "><span class="shape6"><%= sTitulo %></span></h1>
                            <br /><br />

                            <p><%= sConteudoInst %></p>

				        </div>
				
				    </div>
			    </div>
		    </div>
	    </div>

        <!--#include virtual= "footer.asp"-->
    </body>

</html>
