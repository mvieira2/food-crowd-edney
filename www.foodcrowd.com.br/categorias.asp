﻿<!--#include virtual= "/hidden/funcoes.asp"-->

<%
dim sIdCat, sNomeCat, boolPromocaoOut, boolPromocao, busca, sIdCatSub, sOrdenarTabela
dim sBannerPagina, sBannerPagina2, boolAcessorio, catBusca, sIdSubCat, prTipo1, CodReferenciaR, Id_TipoR, Id_SubCategoriaR

dim sClassHeader: sClassHeader = "bg-1"

    
sIdCat          = Replace_Caracteres_Especiais(trim(request("idcat"))) 
sIdSubCat       = Replace_Caracteres_Especiais(trim(request("idsubcat"))) 
busca           = Replace_Caracteres_Especiais(trim(request("busca"))) 
sOrdenarTabela  = Replace_Caracteres_Especiais(trim(request("OrdenarTabela"))) 
boolPromocaoOut = (Replace_Caracteres_Especiais(trim(request("destaque"))) = "1")
boolPromocao    = (Replace_Caracteres_Especiais(trim(request("promocao"))) = "1")
CodReferenciaR  = Replace_Caracteres_Especiais(trim(request("CodReferencia"))) 
Id_TipoR        = Replace_Caracteres_Especiais(trim(request("Id_tipo"))) 
Id_SubCategoriaR= Replace_Caracteres_Especiais(trim(request("Id_SubCategoria"))) 

call Abre_Conexao()

if(sIdCat <> "") then 
    sNomeCat            = trim(Recupera_Campos_Db_oConn("TB_CATEGORIAS", sIdCat, "id", "Categoria"))
    sTitleCat           = trim(Recupera_Campos_Db_oConn("TB_CATEGORIAS", sIdCat, "id", "Title"))
    PalavrasChavesCat   = trim(Recupera_Campos_Db_oConn("TB_CATEGORIAS", sIdCat, "id", "PalavrasChaves"))
    Categoria_DescCat   = trim(Recupera_Campos_Db_oConn("TB_CATEGORIAS", sIdCat, "id", "Categoria_Desc"))
end if

if(sIdSubCat <> "") then 
    sIdCatSub           = trim(Recupera_Campos_Db_oConn("TB_SubCATEGORIA", sIdSubCat, "id", "Id_Categoria"))
    sNomeCatSub         = trim(Recupera_Campos_Db_oConn("TB_SubCATEGORIA", sIdSubCat, "id", "SubCategoria"))
    SubCategoria_DescSub= trim(Recupera_Campos_Db_oConn("TB_SubCATEGORIA", sIdSubCat, "id", "SubCategoria_Desc"))
    sTitleSub           = trim(Recupera_Campos_Db_oConn("TB_SubCATEGORIA", sIdSubCat, "id", "Title"))
    PalavrasChavesSub   = trim(Recupera_Campos_Db_oConn("TB_SubCATEGORIA", sIdSubCat, "id", "PalavrasChaves"))
end if

if(boolPromocaoOut)     then sNomeCat       = "Produtos em Destaque"
if(boolPromocao)        then sNomeCat       = "Produtos em Promoção"
if(sTitleCatSub = "")   then sTitleCatSub   = sNomeCatSub


prTipo1 = Monta_Carrossel_Home(sIdCat, catBusca, busca, boolPromocaoOut, boolPromocao)


' ====================================================================
function Monta_Carrossel_Home(PrsIdCat, PrcatBusca, Prbusca, PrboolPromocaoOut, PrboolPromocao)
	dim sSQl, Rs, sConteudo, sCliente, sCidade, sNomeProduto, sData, sTitulo, sComentario, sAvaliacao, sclass

	sSQl = "Select  " & _
                " TB_PRODUTOS.*, TB_CATEGORIAS.Id, TB_CATEGORIAS.Categoria, TB_CATEGORIAS.Categoria_Jp, " & _
                " TB_PRODUTOS.ID AS IDP, TB_PRODUTOS.Preco_Consumidor as Preco " & _
		    " from  " & _
				" TB_CATEGORIAS (NOLOCK)"  & _
                " INNER JOIN TB_PRODUTOS (NOLOCK) ON TB_CATEGORIAS.ID = TB_PRODUTOS.ID_CATEGORIA " & _
			" where " & _
                " TB_PRODUTOS.cdcliente     = "& sCdCliente & _
                " AND TB_PRODUTOS.Ativo     = 1 " & _
				" AND TB_CATEGORIAS.Ativo   = 1 "

    if(Prbusca <> "")       then 

        if(InStr(Prbusca, " ") > 0) then 
            sArrayValoresId_TipoR   = split(Prbusca, " ")

            sSQlaUX = " AND ("

            for lContArray = 0 to ubound(sArrayValoresId_TipoR)
                if(sArrayValoresId_TipoR(lContArray) <> "" ) then 
                    sSQlaUX = sSQlaUX & " lower(TB_PRODUTOS.Descricao) like '%"& lcase(sArrayValoresId_TipoR(lContArray)) &"%' OR"
                end if
            next

            for lContArray = 0 to ubound(sArrayValoresId_TipoR)
                if(sArrayValoresId_TipoR(lContArray) <> "" ) then 
                    sSQlaUX = sSQlaUX & " lower(Convert(nvarchar(max), TB_PRODUTOS.DescricaoC)) like '%"& lcase(sArrayValoresId_TipoR(lContArray)) &"%' OR"
                end if
            next

            for lContArray = 0 to ubound(sArrayValoresId_TipoR)
                if(sArrayValoresId_TipoR(lContArray) <> "" ) then 
                    sSQlaUX = sSQlaUX & " lower(Convert(nvarchar(max), TB_PRODUTOS.DescricaoBreve)) like '%"& lcase(sArrayValoresId_TipoR(lContArray)) &"%' OR"
                end if
            next

            for lContArray = 0 to ubound(sArrayValoresId_TipoR)
                if(sArrayValoresId_TipoR(lContArray) <> "" ) then 
                    sSQlaUX = sSQlaUX & " lower(TB_PRODUTOS.CodReferencia) like '%"& lcase(sArrayValoresId_TipoR(lContArray)) &"%' OR"
                end if
            next

            sSQlaUX = left(sSQlaUX, LEN(sSQlaUX)-3)
            sSQlaUX = sSQlaUX & ")"

            sSQl = sSQl & sSQlaUX
        else
            sSQl = sSQl & " AND (   (lower(TB_PRODUTOS.DESCRICAO) like '%"& lcase(Prbusca) & "%') OR " & _ 
                          "         (lower(TB_PRODUTOS.CodReferencia) like '%"& lcase(Prbusca) & "%') OR " & _ 
                          "         (lower(Convert(nvarchar(max), TB_PRODUTOS.DescricaoBreve)) like '%"& lcase(Prbusca) & "%') OR " & _ 
                          "         (lower(Convert(nvarchar(max), TB_PRODUTOS.DescricaoC)) like '%"& lcase(Prbusca) & "%') )"
        end if

        call Gravar_Palavra_Chave(Prbusca)
    end if

    if(sIdSubCat <> "")     then sSQl = sSQl & " AND TB_PRODUTOS.ID_SubCategoria = "& sIdSubCat
    if(PrsIdCat <> "")      then sSQl = sSQl & " AND TB_PRODUTOS.ID_Categoria = "& PrsIdCat
    if(PrcatBusca <> "")    then sSQl = sSQl & " AND TB_PRODUTOS.ID_SubCategoria = "& PrcatBusca
    if(PrboolPromocaoOut)   then sSQl = sSQl & " AND TB_PRODUTOS.lancamento    =  1"
    if(PrboolPromocao)      then sSQl = sSQl & " AND TB_PRODUTOS.promocao    =  1"
    
    if(CodReferenciaR <> "")       then 
        sSQl = sSQl & " AND CONVERT(VARCHAR(500), TB_PRODUTOS.Fabricante) in ('"& replace(lcase(CodReferenciaR), ",", "','") &"') "
    end if

    if(Id_SubCategoriaR <> "")       then 
        sSQl = sSQl & " AND TB_PRODUTOS.Id_SubCategoria in ('"& replace(lcase(Id_SubCategoriaR), ",", "','") &"') "
    end if

    if(Id_TipoR <> "")       then 
        if(InStr(Id_TipoR, ",") > 0) then 
            sArrayValoresId_TipoR   = split(Id_TipoR, ",")

            sSQlaUX = " AND ("

            for lContArray = 0 to ubound(sArrayValoresId_TipoR)
                if(sArrayValoresId_TipoR(lContArray) <> "" ) then 
                    sSQlaUX = sSQlaUX & " TB_PRODUTOS.Id_Tipos like '%"& sArrayValoresId_TipoR(lContArray) &"%' OR"
                end if
            next

            sSQlaUX = left(sSQlaUX, LEN(sSQlaUX)-3)
            sSQlaUX = sSQlaUX & ")"

            sSQl = sSQl & sSQlaUX
        else
            sSQl = sSQl & " AND TB_PRODUTOS.Id_Tipos like '%"& Id_TipoR &"%' "
        end if
    end if

    if(sOrdenarTabela <> "") then 
        sSQl = sSQl & " order by TB_PRODUTOS."& sOrdenarTabela
    else
        sSQl = sSQl & " order by TB_PRODUTOS.Preco_Consumidor "
    end if

    'response.Write sSQl
    'response.End
    set Rs = oConn.execute(sSQl)
	
    sclass      = ""
    sConteudo   = ""

	if(not Rs.Eof) then
        lCont = 1

		Do While (Not Rs.Eof)
            sIdP	            = trim(Rs("IDP"))
			sDescricao          = CortaLen(trim(Rs("DESCRICAO")), 50)
            sDestaque           = trim(Rs("destaque"))
            sImagem	            = trim(Rs("Imagem"))
            sPreco              = formatNumber(trim(Rs("Preco")),2)
            sPrecoPrazo         = formatNumber(trim(Rs("Preco_Aprazo")),2)
            sParcelamento       = trim(Rs("Qte_Parcelamento"))
            sDescontoTipo       = trim(Rs("DescontoTipo"))
			sImagem             = (Alterar_Tamanho_Imagem("m", "", "", trim(Rs("Imagem")), 267, 267))
            BoolDataPromocao    = false

			if(trim(Rs("DataIniPromo")) <> "" and not isnull(trim(Rs("DataIniPromo"))) )  then 
				BoolDataPromocao = ( (cdate(trim(Rs("DataIniPromo"))) <= Date) and (cDate(trim(Rs("DataFimPromo"))) >= Date) )
			end if

            if(BoolDataPromocao) then 
                Session("d" & sIdProd) = cdbl(trim(Rs("DescontoValor")))
                
                if(sDescontoTipo = "R") then 
                    sPrecofinal         = formatNumber((cdbl(sPreco) - cdbl(Session("d" & sIdProd)) ),2)   
                    sPrecofinalPrazo    = formatNumber((cdbl(sPrecoPrazo) - cdbl(Session("d" & sIdProd)) ),2) 
                else
                    sPrecofinal         = formatNumber((cdbl(sPreco) - ((cdbl(Session("d" & sIdProd))/100) * cdbl(sPreco))),2)   
                    sPrecofinalPrazo    = formatNumber((cdbl(sPrecofinalPrazo) - ((cdbl(Session("d" & sIdProd))/100) * cdbl(sPrecofinalPrazo))),2)   
                end if
            else
                if(Session("dc" & sIdProd) > 0) then                
                    sPrecofinal         = formatNumber((cdbl(sPreco) - cdbl(Session("dc" & sIdProd)) ),2)   
                    sPrecofinalPrazo    = formatNumber((cdbl(sPrecoPrazo) - cdbl(Session("dc" & sIdProd)) ),2) 
                    BoolDataPromocao    = true
                else
                    sPrecofinal             = sPreco
                    sPrecofinalPrazo        = sPrecoPrazo
                end if
            end if 

            boolPRodutoNovo = false
            if((day(trim(Rs("datacad"))) = day(date)) and (month(trim(Rs("datacad"))) = month(date)) and (year(trim(Rs("datacad"))) = year(date))) then boolPRodutoNovo = true


            sConteudo = sConteudo & "<div class=""col-sm-4"">" & vbcrlf
	            sConteudo = sConteudo & "<div class=""box-receitas"">" & vbcrlf
		            sConteudo = sConteudo & "<a title="""& sDescricao &""" href=""/detalhe-produto/"& TrataSEO(trim(Rs("DESCRICAO"))) &"/"& sIdP &"""><img class=""img-rounded"" src="""& sImagem &""" alt="""& sDescricao &"""></a>" & vbcrlf
		            sConteudo = sConteudo & "<h3 class=""cor-4 semi-bold shape5 text-uppercase"" style=""font-size: 18px;""><a title="""& sDescricao &""" href=""/detalhe-produto/"& TrataSEO(trim(Rs("DESCRICAO"))) &"/"& sIdP &""">"& sDescricao &"</a></h3>" & vbcrlf
		            sConteudo = sConteudo & "<div class=""text-justify"" style=""min-height: 45px;""><p>"& sDestaque &"</p></div>" & vbcrlf
		            sConteudo = sConteudo & "<div style=""text-align: center;""><h2 class=""font-3 cor-1"" style=""font-size: 33px;"">R$ "& sPrecofinal &"</h2> <a class=""botao-4 btn text-uppercase"" href=""/detalhe-produto/"& TrataSEO(trim(Rs("DESCRICAO"))) &"/"& sIdP &""">Comprar</a></div>		" & vbcrlf					
	            sConteudo = sConteudo & "</div>" & vbcrlf
            sConteudo = sConteudo & "</div>" & vbcrlf

           
            if(CodReferenciaR = "") then sFiltros1 = sFiltros1 & trim(Rs("Fabricante")) & "," 

            if(Id_TipoR = "")       then 
                if(InStr(sFiltros2, trim(Rs("Id_Tipos"))) = 0) then 
                    sFiltros2 = sFiltros2 & trim(Rs("Id_Tipos")) & "," 
                end if
            end if

            if(Id_SubCategoriaR = "") then sFiltros3 = sFiltros3 & trim(Rs("Id_SubCategoria")) & "," 

		rs.movenext
		loop
	end if
	set Rs = nothing

	call Fecha_Conexao()
	
    if(CodReferenciaR = "" )    then Session("sFiltros1") = sFiltros1
    if(Id_TipoR = "" )          then Session("sFiltros2") = sFiltros2
    if(Id_SubCategoriaR = "" )  then Session("sFiltros3") = sFiltros3

    Monta_Carrossel_Home = sConteudo
end function



' ================================================================================
function MontarFiltrosArray(prTipo, prValores) 

    if(prValores = "") then 
        exit function 
    end if

    dim sArrayValores, sArrayValores2, lContArray, sConteudoArray, sArrayValoresAux, checked, sNomeOPC

    sArrayValores   = split(prValores, ",")
    sArrayValores2  = BubbleSort(sArrayValores)

    for lContArray = 0 to ubound(sArrayValores2)

        if(sArrayValores2(lContArray) <> "" ) then 

            if(InStr("," & sArrayValoresAux, "," & sArrayValores2(lContArray) & ",") = 0 ) then 
                checked = ""

                if(lcase(prTipo) = "codreferencia") then 
                    if(InStr(lcase(trim(CodReferenciaR)), lcase(trim(FormataNome(Trim(sArrayValores2(lContArray)))))) > 0 ) then 
                        checked = " checked "
                    end if

                    sNomeOPC    = "opcboxfiltro1"
                    sValorNome  = sArrayValores2(lContArray)
                end if

                if(lcase(prTipo) = "id_tipos") then 
                    if(InStr(lcase(trim(Id_TipoR)), lcase(trim(FormataNome(Trim(sArrayValores2(lContArray)))))) > 0 ) then 
                        checked = " checked "
                    end if

                    sNomeOPC    = "opcboxfiltro2"
                    sValorNome  = trim(Recupera_Campos_Db("TB_TIPOS", sArrayValores2(lContArray), "id", "Descricao"))
                end if

                if(lcase(prTipo) = "id_subcategoria") then 
                    if(InStr(lcase(trim(Id_SubCategoriaR)), lcase(trim(FormataNome(Trim(sArrayValores2(lContArray)))))) > 0 ) then 
                        checked = " checked "
                    end if

                    sNomeOPC    = "opcboxfiltro3"
                    sValorNome  = trim(Recupera_Campos_Db("TB_SUBCATEGORIA", sArrayValores2(lContArray), "id", "SubCategoria"))
                end if


	            sConteudoArray = sConteudoArray & "<li> " & _
	                                                    " <input style=""display: initial;"" class=""button"" name="""& sNomeOPC &""" "& checked &" type=""checkbox"" value="""& FormataNome(Trim(sArrayValores2(lContArray))) &""" onclick=""EnviaFiltro('"& prTipo &"', this.value, this);""> " & _
	                                                    " <label for=""jtv1""> "& FormataNome(sValorNome) &"</label> " & _
	                                                "</li>" & vbcrlf

            end if

            sArrayValoresAux = sArrayValoresAux & trim(sArrayValores2(lContArray)) & ","
        end if
    next

    MontarFiltrosArray = sConteudoArray
end function 


' ========================================================================
Function BubbleSort(arrInt)
    for i = UBound(arrInt) - 1 To 0 Step -1
        for j= 0 to i
            if arrInt(j)>arrInt(j+1) then
                temp = arrInt(j+1)
                arrInt(j+1) = arrInt(j)
                arrInt(j) = temp
            end if
        next
    next

    BubbleSort = arrInt
end function

%>


<!DOCTYPE html>
<html lang="en" dir="ltr">
    <!--#include virtual= "head.asp"-->

    <script type="text/javascript">
        function OrdenarTabela(prValor){
            location.href = '/categorias.asp?idcat=<%= sIdCat %>&idsubcat=<%= sIdSubCat %>&busca=<%= busca %>&destaque=<%= Replace_Caracteres_Especiais(trim(request("destaque"))) %>&outlet=<%= Replace_Caracteres_Especiais(trim(request("outlet"))) %>&OrdenarTabela=' + prValor;
        }

        function EnviaFiltro(prTipo, prvalor, obj) {
            if (obj.checked == false) prvalor = '';

            var aChk1 = document.getElementsByName("opcboxfiltro1"); 
            var ValoresMarcados1 = '';

            for (var i=0; i < aChk1.length;i++){ 
                if (aChk1[i].checked == true) ValoresMarcados1 = ValoresMarcados1 + aChk1[i].value + ','
            }
            ValoresMarcados1 = Left(ValoresMarcados1, ValoresMarcados1.length-1);

            var aChk2 = document.getElementsByName("opcboxfiltro2"); 
            var ValoresMarcados2 = '';

            for (var i=0; i < aChk2.length;i++){ 
                if (aChk2[i].checked == true) ValoresMarcados2 = ValoresMarcados2 + aChk2[i].value + ','
            }
            ValoresMarcados2 = Left(ValoresMarcados2, ValoresMarcados2.length-1);

            var aChk3 = document.getElementsByName("opcboxfiltro3"); 
            var ValoresMarcados3 = '';

            for (var i=0; i < aChk3.length;i++){ 
                if (aChk3[i].checked == true) ValoresMarcados3 = ValoresMarcados3 + aChk3[i].value + ','
            }
            ValoresMarcados3 = Left(ValoresMarcados3, ValoresMarcados3.length-1);

            location.href = '/categorias.asp?Id_SubCategoria='+ ValoresMarcados3 +'&CodReferencia='+ ValoresMarcados1 +'&Id_tipo='+ ValoresMarcados2 +'&idcat=<%= sIdCat %>&idsubcat=<%= sIdSubCat %>&busca=<%= busca %>&destaque=<%= Replace_Caracteres_Especiais(trim(request("destaque"))) %>&outlet=<%= Replace_Caracteres_Especiais(trim(request("outlet"))) %>&OrdenarTabela=<%= sOrdenarTabela %>';
        }

        function Left(str, n){
	        if (n <= 0)
	            return "";
	        else if (n > String(str).length)
	            return str;
	        else
	            return String(str).substring(0,n);
        }

    </script>


    <body>
        <!--#include virtual= "header.asp"-->

        <div class=" desktop-margin-top-90" >
            <div class="margin-60-0 ">
                <div class="container">
			        <div class="row ">

				        <div class="col-sm-12">
					        <h1 class="cor-4 extra-bold "><span class="shape6">Escolha sua experiência 
                                                                <% if(busca <> "") then  %>
                                                                        [<%= busca %>]
                                                                <% end if %>
					                                      </span></h1>

                              <ul class="list-inline filtro-links">
                                <li > <a title="Go to Home Page" href="/">Home</a><span>&raquo;</span></li>

                                <% if(sIdCat <> "") then  %>
                                        <li > <a title="Go to Home Page" href="/categoria/<%= sNomeCat %>/<%= sIdCat %>"><%= sNomeCat %></a><span>&raquo;</span></li>
                                <% end if %>

                                  <% if(sIdCatSub <> "") then  %>
                                        <li class=""> <a title="Go to Home Page" href="/subcategoria/<%= sIdCatSub %>/<%= sNomeCatSub %>"><%= sNomeCatSub %></a><span>&raquo;</span></li>
                                  <% end if %>

                                  <% if(busca <> "") then  %>
                                        <li class=""> <a title="Go to Home Page" href="/"><%= busca %></a></li>
                                  <% end if %>

                                    <br /><br />

                                    <form action="<%= Request.ServerVariables("Script_Name")%>"  method="post" name="frmBusReceita" id="frmBusReceita">
                                        <div class="input-group">
                                          <input type="text" class="form-control" placeholder="O que procura ?" name="busca" id="buscaR"  value="<%= busca %>">
                                          <button class="btn-search" type="button" onclick="javascript: validabuscaR();" style="float: right;position: absolute;height: 35px;margin: 6px 0 0 -98px;"><i class="fa fa-search"></i></button>
                                        </div>
                                    </form>

                              </ul>
				        </div>
					
                        <% if(prTipo1 <> "") then  %>
                            <%= prTipo1 %>
                        <% else %>
                            <p>&nbsp;</p>
                            <p style="text-align:center"><img src="/img/nadaencontrado.JPG" style="width: 286px;height: auto;"></p>
                        <% end if %>
				

				    </div>
			    </div>
		    </div>
	    </div>

        <!--#include virtual= "footer.asp"-->
    </body>

</html>
