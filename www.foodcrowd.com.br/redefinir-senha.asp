﻿<!--#include virtual= "/hidden/funcoes.asp"-->

<%
if(Session("Id") = "") then response.redirect("/login.asp?ReturnUrl=arearestrita")


' VARIAVEIS ================================
Dim boolenviado, boolGravado, sMsgJsFun, sOnload
Dim objCriptografia
Set objCriptografia = New Criptografia

boolenviado 	= (Replace_Caracteres_Especiais(replace(trim(request("enviado")),"'","")) = "ok")
boolGravado 	= false
sOnload 		= "onload=""javascript: document.forma0.login.focus();"""

if(boolenviado) 		then call Executar()
if(Session("Id") = "") 	then sOnload = ""

' ==========================================
sub Executar()
    dim sSql, Rs, sCorpo_Msg
	dim sSenha, sSenhaN, sSenhaNC

    if(trim(request("senhan")) = "") then exit sub

	call abre_conexao()

	' sSenha 	        = objCriptografia.QuickEncrypt(Replace_Caracteres_Especiais(replace(trim(request("senha")),"'","")))
	sSenhaN         = objCriptografia.QuickEncrypt(Replace_Caracteres_Especiais(replace(trim(request("senhan")),"'","")))

    sSql = "Select top 1 * from TB_CLIENTES (nolock) where cdcliente = "& sCdCliente &" and id = "& Session("Id") &" and ativo = 1 "
    set Rs = oConn.execute(sSql)

    if(not rs.eof) then
		sCorpo_Msg = sCorpo_Msg & "<html>"
		sCorpo_Msg = sCorpo_Msg & "<head>"
		sCorpo_Msg = sCorpo_Msg & "<meta http-equiv=""Content-Type"" content=""text/html; charset=iso-8859-1"" >"
		sCorpo_Msg = sCorpo_Msg & "<title>"& Application("NomeLoja") &"</title>"
		sCorpo_Msg = sCorpo_Msg & "</head>"
		sCorpo_Msg = sCorpo_Msg & "<body>"
		sCorpo_Msg = sCorpo_Msg & "<div id=""wrap"">"
		sCorpo_Msg = sCorpo_Msg & "<div id=""header"">"
		sCorpo_Msg = sCorpo_Msg & "<h1><a href="""& Application("URLdaLoja") &"""><img src="""& Application("URLdaLoja") &"images/top-header-logo.png"" alt="""& ucase(Application("Nome")) &""" border=""0""></a></h1>"
		sCorpo_Msg = sCorpo_Msg & "</div>"
		sCorpo_Msg = sCorpo_Msg & "    <div id=""content"" class=""indique"">"
		sCorpo_Msg = sCorpo_Msg & "    	<p><strong>"& trim(rs("nome")) &" "& trim(rs("SobreNome")) &", </strong></p>"
		sCorpo_Msg = sCorpo_Msg & "    	<p>Conforme solicitação, sua senha para acessar a <strong>"& Application("Nome") &"</strong> foi alterada.</p>"
		sCorpo_Msg = sCorpo_Msg & "    	<p>Caso você não tenha solicitado a alteração, por favor entre em contato com o <strong>Serviço de Atendimento ao Cliente</strong> no<br> telefone SP: (11) 4161-7930 (Das 9h às 18h) ou envie um e-mail  para <a href=""mailto:"& Application("MailEnvioNewsletter") &""" style=""color:#000;font-weight:bold;"" >"& Application("MailEnvioNewsletter") &"</a>.</p><br>"
		sCorpo_Msg = sCorpo_Msg & "    	<p>Nosso horário de atendimento é de segunda a sexta-feira das 9h às 21h e aos sábados das 10h às 16h (exceto feriados).</p><br>"
		sCorpo_Msg = sCorpo_Msg & "        Atenciosamente,<br>"
		sCorpo_Msg = sCorpo_Msg & "        Equipe <b>"& Application("NomeLoja") &"</b><br><br>"
		sCorpo_Msg = sCorpo_Msg & "        <p>Este é um e-mail automático, não é necessário respondê-lo.<br>"
		sCorpo_Msg = sCorpo_Msg & "        Em caso de dúvida, envie um e-mail para a nossa <a href=""mailto:"& Application("MailEnvioNewsletter") &""" style=""color:#000;font-weight:bold;"" >Central Atendimento</a></p>"
		sCorpo_Msg = sCorpo_Msg & "    </div>"
		sCorpo_Msg = sCorpo_Msg & "  </div>"
		sCorpo_Msg = sCorpo_Msg & "</body>"
		sCorpo_Msg = sCorpo_Msg & "</html>"
		
		oConn.execute("Update TB_CLIENTES set senha = '"& sSenhaN &"' where id = "& Session("Id"))
		
        call Envia_Email(Session("Login"), Application("Nome")	& "<sistema01@ominic.com.br>", Application("NomeLoja"), "Alteração de Senha", sCorpo_Msg)

		sMsgJsFun 	= "Senha alterada com sucesso!"
		boolGravado = true
	else
		sMsgJsFun       = "Senha atual invalida!"
		boolGravado     = true
	end if
    set Rs = nothing

	call fecha_conexao()
end sub
Set objCriptografia = Nothing
%>


<!DOCTYPE html>
<html lang="en" dir="ltr">
    <!--#include virtual= "head.asp"-->


    <script language="JavaScript" type="text/JavaScript">
        <!--
    <%if(boolGravado) then%>
        alert('<%= sMsgJsFun %>');
        location.href = '/arearestrita';
    <%end if%>


    function valida() {
        var objForm = document.forma;
	
        if(objForm.senhan.value != '' && objForm.senhanc.value != '') {
            if(objForm.senhan.value != objForm.senhanc.value) {
                alert('Campo senha e confirma senha estao invalidos!');
                objForm.senhan.focus();
                return false;
            } 
            else {    
                if(objForm.senhan.value.length < 5) {
                    alert('Campo senha tem que ser maior que 6 e menor que 10!');
                    objForm.senhan.focus();
                    return false;
                }
            }
            } else {
                alert('Campo senha e confirma senha estão invalidos!');
                objForm.senhan.focus();
                return false;

    }
    }        
    //-->
    </script>
    

    <script type="text/javascript">
        $(document).ready(function() {
            $("#senhan").focus();
        });
    </script>

<body>

	<!--#include virtual= "header.asp"-->

    <div class=" desktop-margin-top-90" >
	    <div class="margin-60-0 ">
		    <div class="container">
			    <div class="row desktop-margin-90 ">
				    <div class="col-sm-12">
					    <div class="row">
						    <div class="col-sm-12"><h2 class="cor-4 semi-bold "><span class="shape6">ALTERAR SENHA</span></h2></div>

                            <div class="col-sm-12">
                                <p>[<a href="/arearestrita">voltar Minha conta</a>]</p>

                                <p>&nbsp;</p>

                                <form id="frmMinhaConta" method="post" action="<%= Request.ServerVariables("Script_Name")%>" onSubmit="javascript: return valida();" name="forma">
                                <input type="hidden" name="enviado" value="ok">

                                  <div class="col-sm-2 form-group" >
                                    <label>Senha nova:</label>
                                    <div class="input-text"><input type="password"  required="required" placeholder="Senha nova" name="senhan" id="senhan" class="form-control"></div>
                                  </div>

                                  <div class="col-sm-2 form-group" >
                                    <label>Confirmar Senha:</label>
                                    <div class="input-text"><input type="password"  required="required" name="senhanc" id="senhanc" placeholder="Senha nova confirmar" class="form-control"></div>
                                  </div>

                                    <p>&nbsp;</p>
                                    <p>&nbsp;</p>
                                    <p>&nbsp;</p>

                                  <div class="col-xs-12 form-group">
                                    <div class="submit-text"><button type="submit" class="btn botao-5">ALTERAR SENHA</button></div>
                                  </div>

                                </form>

						    </div>
					    </div>	
				    </div>
			    </div>
		    </div>
	    </div>
    </div>

    <!--#include virtual= "footer.asp"-->

</body>
</html>
